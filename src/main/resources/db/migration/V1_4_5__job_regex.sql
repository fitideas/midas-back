ALTER TABLE dbo.JOB_SETTINGS DROP COLUMN FILE_PREFIX;
ALTER TABLE dbo.JOB_SETTINGS DROP COLUMN FILE_SUFFIX;
ALTER TABLE dbo.JOB_SETTINGS ADD FILE_NAME_REGEX varchar(255) NULL;
GO

DELETE FROM JOB_SETTINGS;

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'anulacionPagoJob', N'30 * * * * ?', N'^ANULA_2017[0-9]{4}\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'autoAmortizadoJob', N'30 * * * * ?', N'^AUTO_2017[0-9]{4}\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'cambioEstadoJob', N'30 * * * * ?', N'^CambioEstados_[0-9]{8}\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'cargueUtilizacionJob', N'30 * * * * ?', N'^CARGA_2017[0-9]\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'carteraJob', N'30 * * * * ?', N'^CarteraSrvFinanciero_[0-9]{8}\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'carteraJobT', N'30 * * * * ?', N'^CarteraSrvFinanciero_[0-9]{8}-T\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'condonacionJob', N'30 * * * * ?', N'^Condonaciones_[0-9]{8}\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'facturacionJob', N'30 * * * * ?', N'^FacturacionCartera_[0-9]{8}\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'movimientoSaldoJob', N'30 * * * * ?', N'^MovimientosSaldos_[0-9]{8}\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'pagoJob', N'30 * * * * ?', N'^PAGOS_[0-9]{14}\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'recaudoJob', N'30 * * * * ?', N'^RecaudoXcargo_[0-9]{14}\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'refinanciacionJob', N'30 * * * * ?', N'^RefinanciacionesDiarias_[0-9]{8}\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'trasladosJob', N'30 * * * * ?', N'^TrasladosRealizados_[0-9]{8}\.txt$')
GO

INSERT dbo.JOB_SETTINGS(JOB_NAME, CRON, FILE_NAME_REGEX) VALUES (N'deudaJob', N'30 * * * * ?', N'^CarteraExigible_[0-9]{8}\.txt$')
GO
