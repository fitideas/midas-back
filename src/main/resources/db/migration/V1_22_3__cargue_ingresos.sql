CREATE TABLE INGRESO(
                        ID BIGINT IDENTITY(0,1) NOT NULL,
                        SERVICIO_NUMERO VARCHAR(55),
                        MUNICIPIO VARCHAR(20),
                        FECHA_DOCUMENTO DATE,
                        TASA_INTERES_CORRIENTE NUMERIC (5,2),
                        FECHA_CAUSACION DATE,
                        TASA_INTERES_MORA NUMERIC(5,2),
                        CODIGO_CARGO VARCHAR(100),
                        MONTO NUMERIC(18,4),
                        SIGNO VARCHAR(2),
                        CUENTA_CONTABLE VARCHAR(18),
                        CONSTRAINT [PK_INGRESO] PRIMARY KEY CLUSTERED
                            (
                            [ID] ASC
                            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
    GO

ALTER TABLE INGRESO
    WITH CHECK ADD CONSTRAINT [FK_INGRESO_SEGURO] FOREIGN KEY ([SERVICIO_NUMERO])
    REFERENCES [dbo].[SERVICIO] ([NUMERO])
    GO

ALTER TABLE INGRESO
    WITH CHECK ADD CONSTRAINT [FK_INGRESO_MUNICIPIO] FOREIGN KEY ([MUNICIPIO])
    REFERENCES [dbo].[MUNICIPIO] ([CODIGO])
    GO

    INSERT INTO JOB_SETTINGS (JOB_NAME, CRON, FILE_NAME_REGEX)
    VALUES ('ingresoJob', '30 * * * * ?', '^Ingresos_[0-9]{8}\.txt$')
    GO
