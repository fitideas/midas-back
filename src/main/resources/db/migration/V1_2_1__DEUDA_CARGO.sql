CREATE TABLE dbo.DEUDA_CARGO
(
    ID_DEUDA_CARGO            INT                                              NOT NULL IDENTITY,
    CUENTA_ID                 DECIMAL(18) NULL,
    SUCURSAL_ID               INT                                              NULL,
    CODIGO_SUBPRODUCTO_ID     varchar(4),
    CARGO_ID                  varchar(6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    SERVICIO_ID               varchar(55) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    FECHA_DOCUMENTO           DATETIME                                         NOT NULL,
    FECHA_PRIMER_VENCIMIENTO  DATETIME                                         NOT NULL,
    FECHA_SEGUNDO_VENCIMIENTO DATETIME                                         NOT NULL,
    FECHA_REPORTE             DATETIME                                         NOT NULL,
    SALDO                     DECIMAL(18, 4)                                   NOT NULL,
    ANTIGUEDAD                int,
    CONSTRAINT PK_ID_DEUDA_CARGO PRIMARY KEY (ID_DEUDA_CARGO),
    CONSTRAINT FK_DEUDA_CARGO_SUBPRODUCTO FOREIGN KEY (CODIGO_SUBPRODUCTO_ID) REFERENCES dbo.PRODUCTO (CODIGO),
    CONSTRAINT FK_DEUDA_CARGO_CUENTA FOREIGN KEY (CUENTA_ID) REFERENCES dbo.CUENTA (NUMERO),
    CONSTRAINT FK_DEUDA_CARGO_SERVICIO FOREIGN KEY (SERVICIO_ID) REFERENCES dbo.SERVICIO (NUMERO),
    CONSTRAINT FK_DEUDA_CARGO_SUCURSAL FOREIGN KEY (SUCURSAL_ID) REFERENCES dbo.SUCURSAL (NUMERO),
    CONSTRAINT FK_DEUDA_CARGO_CARGO FOREIGN KEY (CARGO_ID) REFERENCES dbo.CARGO (ID),
);
go
INSERT INTO dbo.JOB_SETTINGS (JOB_NAME, CRON, FILE_PREFIX, FILE_SUFFIX)
VALUES (N'deudaJob', N'45 * * * * ?', N'CarteraExigible_', N'');