ALTER TABLE dbo.TIPO_DOCUMENTO ADD HOMOLOGACION_CYBER VARCHAR(100) NULL;
GO
UPDATE dbo.TIPO_DOCUMENTO SET HOMOLOGACION_CYBER ='CC' WHERE CODIGO ='1'
UPDATE dbo.TIPO_DOCUMENTO SET HOMOLOGACION_CYBER ='TE' WHERE CODIGO ='2'
UPDATE dbo.TIPO_DOCUMENTO SET HOMOLOGACION_CYBER ='CE' WHERE CODIGO ='3'
UPDATE dbo.TIPO_DOCUMENTO SET HOMOLOGACION_CYBER ='PP' WHERE CODIGO ='4'
UPDATE dbo.TIPO_DOCUMENTO SET HOMOLOGACION_CYBER ='NT' WHERE CODIGO ='5'
