CREATE TABLE dbo.HISTORIAL_RECAUDO_RESUMEN
(
    ID                 bigint IDENTITY (1,1) NOT NULL,
    CONTRATO_NUMERO    [varchar](55)         NOT NULL,
    FECHA_PAGO         date                  NOT NULL,
    FECHA_PROCESO_PAGO date                  NOT NULL,
    VALOR_PAGADO       decimal(18, 4)        NULL,
    CAPITAL            decimal(18, 4)        NULL,
    INTERES_CORRIENTE  decimal(18, 4)        NULL,
    INTERES_MORA       decimal(18, 4)        NULL,
    CUOTA_MANEJO       decimal(18, 4)        NULL,
    HONORARIO_COBRANZA decimal(18, 4)        NULL,
    SEGURO_OBLIGATORIO decimal(18, 4)        NULL,
    SEGURO_VOLUNTARIO  decimal(18, 4)        NULL,
    COMISIONES         decimal(18, 4)        NULL,
    CONSTRAINT PK_HISTORIAL_RECAUDO_RESUMEN PRIMARY KEY (ID),
    CONSTRAINT FK_HISTORIAL_RECAUDO_RESUMEN_CONTRATO FOREIGN KEY (CONTRATO_NUMERO) REFERENCES dbo.CONTRATO (NUMERO)
);
