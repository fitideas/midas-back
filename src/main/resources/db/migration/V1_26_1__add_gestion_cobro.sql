ALTER TABLE dbo.CLIENTE
    ADD CONSTRAINT CLIENTE_UN UNIQUE (NUMERO_IDENTIFICACION, TIPO_IDENTIFICACION);

CREATE TABLE dbo.GESTION_COBRO_RESPONSE
(
    ID          varchar(4)   NOT NULL,
    DESCRIPCION varchar(256) NULL,
    CONSTRAINT GESTION_COBRO_RESPONSE_PK PRIMARY KEY (ID)
);

INSERT INTO dbo.GESTION_COBRO_RESPONSE (ID, DESCRIPCION)
VALUES ('SC', 'Sin compromiso'),
       ('PG', N'Posible Negociación'),
       ('S7', N'Negociación'),
       ('PP', 'Promesa de pago'),
       ('RP', 'Recordatorio de pago'),
       ('AP', 'Acuerdo de pago'),
       ('YP', N'Ya pagó'),
       ('BC', 'Call back'),
       ('NA','No informado'),
       ('HU','Pendiente'),
       ('DI','pendiente');

CREATE TABLE dbo.GESTION_COBRO_ACTION
(
    ID          varchar(4)   NOT NULL,
    DESCRIPCION varchar(256) NULL,
    CONSTRAINT GESTION_COBRO_ACTION_PK PRIMARY KEY (ID)
);

INSERT INTO dbo.GESTION_COBRO_ACTION (ID, DESCRIPCION)
VALUES ('VT', 'Visita Titular'),
       ('VR', 'Visita Responsable'),
       ('VC', 'Visita Codeudor'),
       ('LT', 'Outbound titular'),
       ('LR', 'Outbound tercero'),
       ('L9', 'Outbound codeudor'),
       ('I3', 'Inbound encargado'),
       ('I1', 'Inbound titular');


CREATE TABLE dbo.GESTION_COBRO_ACTION_RESPONSE
(
    ACTION_ID   varchar(4) NOT NULL,
    RESPONSE_ID varchar(4) NOT NULL,
    GASTOS_COBRANZA bit DEFAULT 0 NOT NULL ,
    HONORARIOS_CASA_COBRANZA bit DEFAULT 0 NOT NULL ,
    VISITAS_CASA_COBRANZA bit DEFAULT 0 NOT NULL,
    CONSTRAINT GESTION_COBRO_ACTION_RESPONSE_PK PRIMARY KEY (ACTION_ID,RESPONSE_ID),
    CONSTRAINT GESTION_COBRO_ACTION_RESPONSE_ACTION_FK FOREIGN KEY (ACTION_ID) REFERENCES dbo.GESTION_COBRO_ACTION (ID),
    CONSTRAINT GESTION_COBRO_ACTION_RESPONSE_RESPONSE_FK_2 FOREIGN KEY (RESPONSE_ID) REFERENCES dbo.GESTION_COBRO_RESPONSE (ID)
);

INSERT INTO dbo.GESTION_COBRO_ACTION_RESPONSE (ACTION_ID, RESPONSE_ID,GASTOS_COBRANZA)
VALUES ('VT', 'SC ',1), ('VT', 'PG ',1), ('VT', 'S7 ',1), ('VT', 'PP ',1), ('VT', 'RP ',1), ('VT', 'AP ',1), ('VT', 'YP ',1), ('VT', 'BC ',0),
       ('VR', 'SC ',1), ('VR', 'PG ',1), ('VR', 'S7 ',1), ('VR', 'PP ',1), ('VR', 'RP ',1), ('VR', 'AP ',1), ('VR', 'YP ',1), ('VR', 'BC ',0),
       ('VC', 'SC ',1), ('VC', 'PG ',1), ('VC', 'S7 ',1), ('VC', 'PP ',1), ('VC', 'RP ',1), ('VC', 'AP ',1), ('VC', 'YP ',1), ('VC', 'BC ',0),
       ('LT', 'SC ',1), ('LT', 'PG ',1), ('LT', 'S7 ',1), ('LT', 'PP ',1), ('LT', 'RP ',1), ('LT', 'AP ',1), ('LT', 'YP ',1), ('LT', 'BC ',1),
       ('LR', 'SC ',1), ('LR', 'PG ',1), ('LR', 'S7 ',1), ('LR', 'PP ',1), ('LR', 'RP ',1), ('LR', 'AP ',1), ('LR', 'YP ',1), ('LR', 'BC ',1),
       ('L9', 'SC ',1), ('L9', 'PG ',1), ('L9', 'S7 ',1), ('L9', 'PP ',1), ('L9', 'RP ',1), ('L9', 'AP ',1), ('L9', 'YP ',1), ('L9', 'BC ',1),
       ('I3', 'SC ',1), ('I3', 'PG ',1), ('I3', 'S7 ',1), ('I3', 'PP ',1), ('I3', 'RP ',1), ('I3', 'AP ',1), ('I3', 'YP ',1), ('I3', 'BC ',1),
       ('I1', 'SC ',1), ('I1', 'PG ',1), ('I1', 'S7 ',1), ('I1', 'PP ',1), ('I1', 'RP ',1), ('I1', 'AP ',1), ('I1', 'YP ',1), ('I1', 'BC ',1);


CREATE TABLE dbo.CH_GESTCYBER
(
    ID             bigint IDENTITY (0,1) NOT NULL,
    HISTORY_DATE   datetime              NOT NULL,
    TIPO_DOCUMENTO varchar(6)            NOT NULL,
    IDENTITY_CODE  decimal(15, 0)        NOT NULL,
    ACTION_ID      varchar(4)            NOT NULL,
    RESPONSE_ID    varchar(4)            NOT NULL,
    DESCRIP        varchar(256)          NOT NULL,
    TELEFONO       varchar(15)           NOT NULL,
    USUARIO        varchar(256)          NOT NULL,
    GRUPO          varchar(4)            NOT NULL,
    CUENTA         varchar(55)           NOT NULL,
    DMAGENCY       varchar(100)          NOT NULL,
    OBSERVATIONS   varchar(256)          NOT NULL,
    CONSTRAINT CH_GESTCYBER_PK PRIMARY KEY (ID),
    CONSTRAINT CH_GESTCYBER_UN UNIQUE (HISTORY_DATE,ACTION_ID,TIPO_DOCUMENTO,IDENTITY_CODE),
    CONSTRAINT CH_GESTCYBER_CLIENTE_FK FOREIGN KEY (IDENTITY_CODE, TIPO_DOCUMENTO) REFERENCES dbo.CLIENTE (NUMERO_IDENTIFICACION, TIPO_IDENTIFICACION),
    CONSTRAINT CH_GESTCYBER_ACTION_FK FOREIGN KEY (ACTION_ID) REFERENCES dbo.GESTION_COBRO_ACTION (ID),
    CONSTRAINT CH_GESTCYBER_RESPONSE_FK FOREIGN KEY (RESPONSE_ID) REFERENCES dbo.GESTION_COBRO_RESPONSE (ID),
    CONSTRAINT CH_GESTCYBER_CONTRATO_FK FOREIGN KEY (CUENTA) REFERENCES dbo.CONTRATO (NUMERO)
);
INSERT INTO dbo.ATTRIBUTES VALUES ('CAMPO_TIPO_DOCUMENTO_GESTION_COBRO','TIPO DOCUMENTO')