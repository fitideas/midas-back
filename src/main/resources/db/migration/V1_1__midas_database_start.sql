/****** Object:  Table [dbo].[CARGO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CARGO]
(
    [ID]              [varchar](6)   NOT NULL,
    [NOMBRE]          [varchar](255) NULL,
    [Cargo_Colpatria] [bit]          NULL,
    CONSTRAINT [PK_CARGO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CATEGORIA_CUENTA]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CATEGORIA_CUENTA]
(
    [ID]     [varchar](20)  NOT NULL,
    [NOMBRE] [varchar](255) NULL,
    CONSTRAINT [PK_CATEGORIA_CUENTA] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CICLO_FACTURACION]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CICLO_FACTURACION]
(
    [ID]     [int]          NOT NULL,
    [NOMBRE] [varchar](255) NULL,
    CONSTRAINT [PK_CICLO_FACTURACION] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CLASE_SERVICIO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CLASE_SERVICIO]
(
    [ID]     [varchar](20)  NOT NULL,
    [NOMBRE] [varchar](255) NULL,
    CONSTRAINT [PK_CLASE_SERVICIO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CLIENTE]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CLIENTE]
(
    [NUMERO_IDENTIFICACION] decimal(15)    NOT NULL,
    [NOMBRE]                [varchar](255) NULL,
    [TIPO_IDENTIFICACION]   [varchar](6)   NULL,
    [CUENTA_NUMERO]         [decimal](18)  NULL,
    CONSTRAINT [PK_CLIENTE] PRIMARY KEY CLUSTERED
        (
         [NUMERO_IDENTIFICACION] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CONTRATO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONTRATO]
(
    [NUMERO]             [varchar](55)  NOT NULL,
    [ESTADO]             [varchar](255) NULL,
    [FACTURACION]        [varchar](2)   NULL,
    [SUBPRODUCTO_CODIGO] [varchar](4)   NULL,
    [CLIENTE_NUMERO_ID]  decimal(15)    NULL,
    CONSTRAINT [PK_CONTRATO] PRIMARY KEY CLUSTERED
        (
         [NUMERO] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CUENTA]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUENTA]
(
    [NUMERO]              [decimal](18)  NOT NULL,
    [CICLO_FACTURACION]   [int]          NULL,
    [DIRECCION]           [varchar](255) NULL,
    [ESTRATO]             [int]          NULL,
    [LOCALIZACION]        [varchar](55)  NULL,
    [MUNICIPIO]           [varchar](55)  NULL,
    [SUCURSAL_NUMERO]     [int]          NULL,
    [CATEGORIA_CUENTA_ID] [varchar](20)  NULL,
    CONSTRAINT [PK_CUENTA] PRIMARY KEY CLUSTERED
        (
         [NUMERO] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EMAIL_SETTINGS]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EMAIL_SETTINGS]
(
    [EMAIL] [varchar](40) NOT NULL,
    CONSTRAINT [PK_EMAIL_SETTINGS] PRIMARY KEY CLUSTERED
        (
         [EMAIL] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FORMA_PAGO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FORMA_PAGO]
(
    [ID]     [varchar](55)    NOT NULL,
    [NOMBRE] [varbinary](255) NULL,
    CONSTRAINT [PK_FORMA_PAGO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_ANULACION_PAGO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_ANULACION_PAGO]
(
    [ID]                      [bigint] IDENTITY (1,1) NOT NULL,
    [NUMERO_DOCUMENTO_PAGADO] [varchar](30)           NULL,
    [FECHA_INGRESO]           [date]                  NULL,
    [FECHA_PAGO]              [date]                  NULL,
    [FECHA_VENCIMIENTO]       [date]                  NULL,
    [SERVICIO_NUMERO]         [varchar](55)           NULL,
    [CARGO_ID]                [varchar](6)            NULL,
    [MONTO]                   [decimal](18, 4)        NULL,
    [MONTO_PARTICIPACION]     [decimal](18, 4)        NULL,
    CONSTRAINT [PK_HISTORIAL_ANULACION_PAGO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_ARCHIVO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_ARCHIVO]
(
    [NOMBRE_ARCHIVO]      [varchar](255) NOT NULL,
    [ESTADO]              [varchar](255) NULL,
    [INTENTOS]            [int]          NULL,
    [ULTIMA_MODIFICACION] [datetime]     NULL,
    [LOG]                 [int]          NULL,
    CONSTRAINT [PK_HISTORIAL_ARCHIVO] PRIMARY KEY CLUSTERED
        (
         [NOMBRE_ARCHIVO] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_AUTOMORTIZADO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_AUTOMORTIZADO]
(
    [ID]                      [bigint] IDENTITY (1,1) NOT NULL,
    [SERVICIO_NUMERO]         [varchar](55)           NULL,
    [NUMERO_DOCUMENTO_PAGADO] [varchar](30)           NULL,
    [FECHA_INGRESO]           [date]                  NULL,
    [FECHA_PAGO]              [date]                  NULL,
    [FECHA_VENCIMIENTO]       [date]                  NULL,
    [FECHA_POSTEO]            [date]                  NULL,
    [FECHA_EFECTIVA]          [date]                  NULL,
    [CARGO_ID]                [varchar](6)            NULL,
    [MONTO]                   [decimal](18, 4)        NULL,
    [MONTO_PARTICIPACION]     [decimal](18, 4)        NULL,
    CONSTRAINT [PK_HISTORIAL_AUTOMORTIZADO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_CAMBIO_ESTADO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_CAMBIO_ESTADO]
(
    [ID]                       [bigint] IDENTITY (1,1) NOT NULL,
    [FECHA]                    [date]                  NULL,
    [SERVICIO_ESTADO_ANTERIOR] [varchar](21)           NULL,
    [SERVICIO_ESTADO_NUEVO]    [varchar](21)           NULL,
    [OBSERVACION]              [varchar](255)          NULL,
    [SERVICIO_NUMERO]          [varchar](55)           NULL,
    [USUARIO_ID]               [varchar](15)           NULL,
    CONSTRAINT [PK_HISTORIAL_CAMBIO_ESTADO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_CARGUE_UTILIZACION]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_CARGUE_UTILIZACION]
(
    [ID]                            [bigint] IDENTITY (1,1) NOT NULL,
    [SERVICIO_NUMERO]               [varchar](55)           NULL,
    [CIUDAD_ORIGEN]                 [varchar](255)          NULL,
    [FECHA_POSTEO]                  [date]                  NULL,
    [FECHA_EFECTIVA]                [date]                  NULL,
    [VALOR]                         [decimal](18, 4)        NULL,
    [VALOR_DESCUENTO]               [decimal](18, 4)        NULL,
    [VALOR_FINANCIADO]              [decimal](18, 4)        NULL,
    [TASA]                          [decimal](18, 4)        NULL,
    [NUMERO_AUTORIZACION]           [varchar](30)           NULL,
    [SUBPRODUCTO_CODIGO]            [varchar](4)            NOT NULL,
    [CLIENTE_NUMERO_IDENTIFICACION] decimal(15)             NULL,
    CONSTRAINT [PK_HISTORIAL_CARGUE_UTILIZACION] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_CARTERA]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_CARTERA]
(
    [ID]                                           [bigint] IDENTITY (1,1) NOT NULL,
    [CAPITAL_IMPAGO]                               [decimal](18, 4)        NULL,
    [DIAS_ATRASO]                                  [int]                   NULL,
    [PRIMERA_ALTURA_MORA]                          [int]                   NULL,
    [SEGUNDA_ALTURA_MORA]                          [int]                   NULL,
    [FECHA_PRIMER_VENCIMIENTO]                     [date]                  NULL,
    [FECHA_REPORTE]                                [date]                  NULL,
    [FECHA_CIERRE]                                 [date]                  NULL,
    [FECHA_SEGUNDO_VENCIMIENTO]                    [date]                  NULL,
    [INTERES_CAUSADO_NO_FACTURADO]                 [decimal](18, 4)        NULL,
    [INTERES_CORRIENTE_ORDEN_CAUSADO_NO_FACTURADO] [decimal](18, 4)        NULL,
    [INTERES_FACTURADO_NO_AFECTO]                  [decimal](18, 4)        NULL,
    [INTERES_IMPAGO]                               [decimal](18, 4)        NULL,
    [INTERES_MORA]                                 [decimal](18, 4)        NULL,
    [INTERES_MORA_CAUSADO_NO_FACTURADO]            [decimal](18, 4)        NULL,
    [INTERES_MORA_ORDEN_CAUSADO_NO_FACTURADO]      [decimal](18, 4)        NULL,
    [INTERES_NO_AFECTO_POR_FACTURAR]               [decimal](18, 4)        NULL,
    [INTERES_ORDEN_CORRIENTE_IMPAGO]               [decimal](18, 4)        NULL,
    [INTERES_ORDEN_MORA_IMPAGO]                    [decimal](18, 4)        NULL,
    [MORA_PRIMER_VENCIMIENTO]                      [decimal](18, 4)        NULL,
    [MOTIVO_FINALIZACION]                          [varchar](255)          NULL,
    [TOTAL_INTERES_CORRIENTE_CAUSADO]              [decimal](18, 4)        NULL,
    [TOTAL_INTERES_CORRIENTE_FACTURADO_IMPAGO]     [decimal](18, 4)        NULL,
    [TOTAL_INTERES_MORA_CAUSADO]                   [decimal](18, 4)        NULL,
    [TOTAL_INTERES_MORA_FACTURADO_IMPAGO]          [decimal](18, 4)        NULL,
    [SERVICIO_NUMERO]                              [varchar](55)           NULL,
    [DESCRIPCION]                                  [varchar](255)          NULL,
    [FECHA_TRASLADO]                               [date]                  NULL,
    [CODIGO_SEGURO_OBLIGATORIO]                    [varchar](21)           NULL,
    CONSTRAINT [PK_HISTORIAL_CARTERA] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_CARTERA_RESUMEN]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_CARTERA_RESUMEN]
(
    [ID]                                                [bigint] IDENTITY (1,1) NOT NULL,
    [CONTRATO_NUMERO]                                   [varchar](55)           NULL,
    [SUMA_CAPITAL_IMPAGO]                               [decimal](18, 4)        NULL,
    [DIAS_ATRASO_MAXIMO]                                [int]                   NULL,
    [FECHA_PRIMER_VENCIMIENTO_MINIMO]                   [date]                  NULL,
    [FECHA_REPORTE]                                     [date]                  NULL,
    [FECHA_SEGUNDO_VENCIMIENTO_MINIMO]                  [date]                  NULL,
    [SUMA_INTERES_CAUSADO_NO_FACTURADO]                 [decimal](18, 4)        NULL,
    [SUMA_INTERES_CORRIENTE_ORDEN_CAUSADO_NO_FACTURADO] [decimal](18, 4)        NULL,
    [SUMA_INTERES_FACTURADO_NO_AFECTO]                  [decimal](18, 4)        NULL,
    [SUMA_INTERES_IMPAGO]                               [decimal](18, 4)        NULL,
    [SUMA_INTERES_MORA]                                 [decimal](18, 4)        NULL,
    [SUMA_INTERES_MORA_CAUSADO_NO_FACTURADO]            [decimal](18, 4)        NULL,
    [SUMA_INTERES_MORA_ORDEN_CAUSADO_NO_FACTURADO]      [decimal](18, 4)        NULL,
    [SUMA_INTERES_NO_AFECTO_POR_FACTURAR]               [decimal](18, 4)        NULL,
    [SUMA_INTERES_ORDEN_CORRIENTE_IMPAGO]               [decimal](18, 4)        NULL,
    [SUMA_INTERES_ORDEN_MORA_IMPAGO]                    [decimal](18, 4)        NULL,
    [MORA_PRIMER_VENCIMIENTO_MAXIMO]                    [decimal](18, 4)        NULL,
    [SUMA_TOTAL_INTERES_CORRIENTE_CAUSADO]              [decimal](18, 4)        NULL,
    [SUMA_TOTAL_INTERES_CORRIENTE_FACTURADO_IMPAGO]     [decimal](18, 4)        NULL,
    [SUMA_TOTAL_INTERES_MORA_CAUSADO]                   [decimal](18, 4)        NULL,
    [SUMA_TOTAL_INTERES_MORA_FACTURADO_IMPAGO]          [decimal](18, 4)        NULL,
    [CUOTAS_PACTADAS_MINIMO]                            [int]                   NULL,
    [CUOTAS_PACTADAS_MAXIMO]                            [int]                   NULL,
    [CUOTAS_POR_FACTURAR_MINIMO]                        [int]                   NULL,
    [CUOTAS_POR_FACTURAR_MAXIMO]                        [int]                   NULL,
    [CANTIDAD_SERVICIOS]                                [int]                   NULL,
    [TASA_PONDERADA]                                    [decimal](18, 4)        NULL,
    [TASA_MAXIMA_PACTADA]                               [decimal](18, 4)        NULL,
    [SUMA_SALDO_CAPITAL_POR_FACTURAR]                   [decimal](18, 4)        NULL,
    [SUMA_VALOR_CUOTA]                                  [decimal](18, 4)        NULL,
    [SUMA_VALOR_COMPRA]                                 [decimal](18, 4)        NULL,
    [SUMA_SALDO_DEUDA]                                  [decimal](18, 4)        NULL,
    CONSTRAINT [PK_HISTORIAL_CARTERA_RESUMEN] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_CONDONACION]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_CONDONACION]
(
    [ID]                    [bigint] IDENTITY (0,1) NOT NULL,
    [TIPO_PRODUCTO_ID]      [varchar](4)            NULL,
    [TIPO_NEGOCIACION]      [int]                   NULL,
    [NEGOCIACION_ID]        [bigint]                NULL,
    [ESTADO_NEGOCIACION]    [varchar](50)           NULL,
    [NUMERO_CONTRATO]       [varchar](55)           NULL,
    [FECHA_NEGOCIACION]     [date]                  NULL,
    [FECHA_PAGO]            [date]                  NULL,
    [FECHA_APLICA]          [date]                  NULL,
    [FECHA_RECHAZO]         [date]                  NULL,
    [FECHA_VENCIMIENTO]     [date]                  NULL,
    [VALOR_PAGO]            [numeric](18, 5)        NULL,
    [MOTIVO_RECHAZO]        [varchar](50)           NULL,
    [DESCRIPCION]           [varchar](255)          NULL,
    [CAPITAL_CONDONA]       [numeric](18, 5)        NULL,
    [SALDO_CAPITAL_CONDONA] [numeric](18, 5)        NULL,
    [USUARIO_ID]            [varchar](15)           NULL,
    CONSTRAINT [PK_CONDONACION] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_FACTURACION]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_FACTURACION]
(
    [ID]              [bigint] IDENTITY (0,1) NOT NULL,
    [VALOR_FACT]      [numeric](18, 5)        NULL,
    [CARGO_ID]        [varchar](10)           NULL,
    [DESCRIPCION]     [varchar](255)          NULL,
    [FECHA_DOC]       [date]                  NULL,
    [FECHA_PR]        [date]                  NULL,
    [TOT_DOCUMENTO]   [numeric](18, 5)        NULL,
    [SALDO]           [numeric](18, 5)        NULL,
    [SERVICIO_NUMERO] [varchar](15)           NULL,
    CONSTRAINT [PK_HISTORIAL_FACTURACION] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_MOVIMIENTO_SALDO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_MOVIMIENTO_SALDO]
(
    [ID]                       [bigint] IDENTITY (1,1) NOT NULL,
    [CONTRATO_NUMERO]          [varchar](55)           NOT NULL,
    [VALOR]                    [decimal](18, 4)        NOT NULL,
    [ESTADO]                   [varchar](21)           NOT NULL,
    [TIPO_MOVIMIENTO]          [varchar](255)          NOT NULL,
    [CARGO_ID]                 [varchar](6)            NOT NULL,
    [NUMERO_COMPRA]            [varchar](30)           NOT NULL,
    [FECHA_REALIZACION_AJUSTE] [date]                  NOT NULL,
    [FECHA_APROBACION_AJUSTE]  [date]                  NOT NULL,
    [USUARIO_CREADOR_ID]       [varchar](15)           NOT NULL,
    [USUARIO_APROBADOR_ID]     [varchar](15)           NOT NULL,
    [OBSERVACION]              [varchar](255)          NULL,
    [NEGOCIACION_NUMERO]       [bigint]                NOT NULL,
    CONSTRAINT [PK_HISTORIAL_MOVIMIENTO_SALDO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_PAGO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_PAGO]
(
    [ID]                      [bigint] IDENTITY (1,1) NOT NULL,
    [NUMERO_DOCUMENTO_PAGADO] [varchar](255)          NULL,
    [FECHA_INGRESO]           [date]                  NULL,
    [FECHA_PAGO]              [date]                  NULL,
    [FECHA_VENCIMIENTO]       [date]                  NULL,
    [CODIGO_CARGO_COLPATRIA]  [varchar](255)          NULL,
    [CODIGO_CARGO]            [varchar](255)          NULL,
    [DESCRIPCION_CARGO]       [varchar](255)          NULL,
    [MONTO]                   [decimal](18, 4)        NULL,
    [SERVICIO_NUMERO]         [varchar](15)           NULL,
    [SUCURSAL_NUMERO]         [int]                   NULL,
    [IDENTIFICACION_CLIENTE]  decimal(15)             NULL,
    [CODIGO_SUBPRODUCTO]      [varchar](4)            NULL,
    [CUENTA_CONTABLE]         [varchar](50)           NULL,
    [MONTO_PARTICIPACION]     [numeric](18, 5)        NULL,
    CONSTRAINT [PK_PAGO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_RECAUDO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_RECAUDO]
(
    [ID]                      [bigint] IDENTITY (1,1) NOT NULL,
    [FECHA_REPORTE]           [datetime]              NULL,
    [FECHA_INGRESO_PAGO]      [date]                  NULL,
    [FECHA_PAGO]              [date]                  NULL,
    [FECHA_PROCESO_PAGO]      [date]                  NULL,
    [CODIGO_RECAUDO_CARGO]    [bigint]                NULL,
    [FORMA_PAGO_ID]           [varchar](55)           NULL,
    [CARGO_ID]                [varchar](6)            NULL,
    [DESCRIPCION]             [varchar](255)          NULL,
    [ID_PAGO]                 [bigint]                NULL,
    [TIPO_CONSUMO]            [varchar](10)           NULL,
    [FECHA_FACTURACION]       [date]                  NULL,
    [SUCURSAL_RECAUDO]        [varchar](12)           NULL,
    [COD_ENTIDAD_RECAUDADORA] [varchar](55)           NULL,
    [SERVICIO]                [varchar](55)           NULL,
    CONSTRAINT [PK_HISTORIAL_RECAUDO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_REFINANCIACION]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_REFINANCIACION]
(
    [ID]                                        [bigint] IDENTITY (1,1) NOT NULL,
    [SERVICIO_NUMERO]                           [varchar](55)           NULL,
    [VALOR_PAGO]                                [decimal](18, 4)        NULL,
    [CUOTA_INICIAL]                             [decimal](18, 4)        NULL,
    [DESCUENTO_CUOTA_INICIAL]                   [varchar](30)           NULL,
    [FECHA_ACT]                                 [date]                  NULL,
    [FECHA_APLI]                                [date]                  NULL,
    [FECHA_RECHAZO]                             [date]                  NULL,
    [MOTIVO_RECHAZO]                            [varchar](50)           NULL,
    [ID_FIN_NUEVA]                              [int]                   NULL,
    [ID_FIN_ORIGINAL]                           [int]                   NULL,
    [CANTIDAD_CUOTAS_ANTERIORES]                [int]                   NULL,
    [CANTIDAD_CUOTAS_DESPUES_DE_REFINANCIACION] [int]                   NULL,
    [CUOTA_MENSUAL_DESPUES]                     [decimal](18, 4)        NULL,
    [TASA_ANTERIOR]                             [decimal](18, 4)        NULL,
    [TASA_NUEVA]                                [decimal](18, 4)        NULL,
    [TASA_CAMBIO_PLAZO]                         [varchar](30)           NULL,
    [USUARIO_ID]                                [varchar](15)           NULL,
    [CUOTA_MENSUAL_ANTERIOR]                    [decimal](18, 4)        NULL,
    [NEGOCIACION_ID]                            [bigint]                NULL,
    CONSTRAINT [PK_HISTORIAL_REFINANCIACION] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORIAL_TRASLADO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORIAL_TRASLADO]
(
    [ID]                     [bigint] IDENTITY (1,1) NOT NULL,
    [FECHA]                  [date]                  NULL,
    [OBSERVACION]            [varchar](500)          NULL,
    [CONTRATO_NUMERO]        [varchar](55)           NULL,
    [CUENTA_ANTERIOR_NUMERO] [varchar](18)           NULL,
    [CUENTA_ACTUAL_NUMERO]   [varchar](18)           NULL,
    [USUARIO_ID]             [varchar](15)           NULL,
    [CICLO_ANTERIOR]         [int]                   NULL,
    [CICLO_ACTUAL]           [int]                   NULL,
    CONSTRAINT [PK_HISTORIAL_TRASLADO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JOB_SETTINGS]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JOB_SETTINGS]
(
    [JOB_NAME]    [varchar](255) NOT NULL,
    [CRON]        [varchar](255) NOT NULL,
    [FILE_PREFIX] [varchar](255) NULL,
    [FILE_SUFFIX] [varchar](255) NULL,
    CONSTRAINT [PK_JOB_CRON] PRIMARY KEY CLUSTERED
        (
         [JOB_NAME] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NEGOCIACION]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NEGOCIACION]
(
    [ID]     [bigint]       NOT NULL,
    [FECHA]  [datetime]     NULL,
    [ESTADO] [varchar](255) NULL,
    CONSTRAINT [PK_NEGOCIACION] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PRODUCTO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUCTO]
(
    [CODIGO]               [varchar](4)   NOT NULL,
    [NOMBRE]               [varchar](255) NULL,
    [PRODUCTO_PADRE]       [varchar](4)   NULL,
    [TIPO_PRODUCTO_CODIGO] [varchar](4)   NULL,
    CONSTRAINT [PK_PRODUCTO] PRIMARY KEY CLUSTERED
        (
         [CODIGO] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RECAUDO_CARGO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RECAUDO_CARGO]
(
    [CODIGO]                [bigint] IDENTITY (1,1) NOT NULL,
    [FECHA_DOCUMENTO]       [date]                  NULL,
    [MEDIO_PAGO]            [int]                   NULL,
    [MONTO]                 [decimal](12, 4)        NULL,
    [NUMERO_DOCUMENTO_PAGO] [bigint]                NULL,
    [OFICINA]               [varchar](255)          NULL,
    [SERVICIO]              [varchar](55)           NULL,
    [TIPO_PAGO]             [varchar](20)           NULL,
    [VALOR_PAGO]            [decimal](12, 4)        NULL,
    [CLIENTE_NUMERO_ID]     decimal(15)             NULL,
    [NRO_DOCUMENTO]         [varchar](55)           NULL,
    CONSTRAINT [PK_RECAUDO_CARGO] PRIMARY KEY CLUSTERED
        (
         [CODIGO] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SERVICIO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SERVICIO]
(
    [NUMERO]                     [varchar](55)    NOT NULL,
    [CUOTAS_PACTADAS]            [int]            NULL,
    [CUOTAS_POR_FACTURAR]        [int]            NULL,
    [ESTADO]                     [varchar](21)    NULL,
    [FECHA_COMPRA]               [date]           NULL,
    [FECHA_CREACION]             [date]           NULL,
    [SALDO_CAPITAL_POR_FACTURAR] [float]          NULL,
    [SALDO_DEUDA]                [decimal](18, 4) NULL,
    [SOCIO_DE_NEGOCIO]           [varchar](255)   NULL,
    [TASA]                       [decimal](18, 4) NULL,
    [VALOR_COMPRA]               [decimal](18, 4) NULL,
    [VALOR_CUOTA]                [decimal](18, 4) NULL,
    [CONTRATO_NUMERO]            [varchar](55)    NULL,
    [TIPO_TARJETA_ID]            [varchar](255)   NULL,
    [TIPO_CONSUMO]               [int]            NULL,
    [CLASE_SERVICIO_ID]          [varchar](20)    NULL,
    CONSTRAINT [PK_SERVICIO] PRIMARY KEY CLUSTERED
        (
         [NUMERO] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SUCURSAL]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SUCURSAL]
(
    [NUMERO] [int]          NOT NULL,
    [NOMBRE] [varchar](255) NULL,
    CONSTRAINT [PK_SUCURSAL] PRIMARY KEY CLUSTERED
        (
         [NUMERO] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIPO_CONSUMO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIPO_CONSUMO]
(
    [ID]     [int]          NOT NULL,
    [NOMBRE] [varchar](255) NULL,
    CONSTRAINT [PK_TIPO_CONSUMO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIPO_DOCUMENTO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIPO_DOCUMENTO]
(
    [CODIGO] [varchar](6)   NOT NULL,
    [NOMBRE] [varchar](255) NULL,
    CONSTRAINT [PK_TIPO_DOCUMENTO] PRIMARY KEY CLUSTERED
        (
         [CODIGO] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIPO_NEGOCIACION]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIPO_NEGOCIACION]
(
    [ID_TIPO_NEGOCIACION]          [int]          NOT NULL,
    [DESCRIPCION_TIPO_NEGOCIACION] [varchar](255) NULL,
    CONSTRAINT [PK_TIPO_NEGOCIACION] PRIMARY KEY CLUSTERED
        (
         [ID_TIPO_NEGOCIACION] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIPO_PRODUCTO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIPO_PRODUCTO]
(
    [CODIGO] [varchar](4)   NOT NULL,
    [NOMBRE] [varchar](255) NULL,
    CONSTRAINT [PK_TIPO_PRODUCTO] PRIMARY KEY CLUSTERED
        (
         [CODIGO] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TIPO_TARJETA]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIPO_TARJETA]
(
    [ID]     [varchar](255) NOT NULL,
    [NOMBRE] [varchar](255) NULL,
    CONSTRAINT [PK_TIPO_TARJETA] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[USUARIO]    Script Date: 8/03/2022 10:09:43 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[USUARIO]
(
    [ID]        [varchar](15)  NOT NULL,
    [APELLIDOS] [varchar](255) NULL,
    [AREA]      [varchar](40)  NULL,
    [NOMBRES]   [varchar](255) NULL,
    CONSTRAINT [PK_USUARIO] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[CATEGORIA_CUENTA] ([ID], [NOMBRE])
VALUES (0, NULL)
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (1, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (2, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (3, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (4, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (5, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (6, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (7, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (8, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (9, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (10, NULL)
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (11, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (12, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (13, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (14, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (15, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (16, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (17, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (18, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (19, N'')
GO
INSERT [dbo].[CICLO_FACTURACION] ([ID], [NOMBRE])
VALUES (20, N'')
GO
INSERT [dbo].[CLASE_SERVICIO] ([ID], [NOMBRE])
VALUES (N'2', NULL)
GO
INSERT [dbo].[CLASE_SERVICIO] ([ID], [NOMBRE])
VALUES (N'3', NULL)
GO
INSERT [dbo].[CLASE_SERVICIO] ([ID], [NOMBRE])
VALUES (N'CompraCartera', NULL)
GO
INSERT [dbo].[EMAIL_SETTINGS] ([EMAIL])
VALUES (N'diego.bustamante@fitideas.co')
GO
INSERT [dbo].[EMAIL_SETTINGS] ([EMAIL])
VALUES (N'esteban.pena@fitideas.co')
GO
INSERT [dbo].[EMAIL_SETTINGS] ([EMAIL])
VALUES (N'jaen.echavarria@fitideas.co')
GO
INSERT [dbo].[EMAIL_SETTINGS] ([EMAIL])
VALUES (N'jesus.jaimes@fitideas.co')
GO
INSERT [dbo].[EMAIL_SETTINGS] ([EMAIL])
VALUES (N'ricardo.calvo@fitideas.co')
GO
INSERT [dbo].[FORMA_PAGO] ([ID], [NOMBRE])
VALUES (7, NULL)
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'anulacionPagoJob', N'30 * * * * ?', N'ANULA_', N'')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'autoAmortizadoJob', N'30 * * * * ?', N'AUTO_', N'')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'cambioEstadoJob', N'30 * * * * ?', N'AUDIT_WKF_', N'')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'cargueUtilizacionJob', N'30 * * * * ?', N'CARGA_', N'')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'carteraJob', N'0 * * * * ?', N'CarteraSrvFinanciero_', N'')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'carteraJobT', N'0 * * * * ?', N'CarteraSrvFinanciero_', N'-T')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'condonacionJob', N'30 * * * * ?', N'Condonaciones_', N'')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'facturacionJob', N'30 * * * * ?', N'FACT_', N'')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'movimientoSaldoJob', N'30 * * * * ?', N'CASOS_MOVIMIENTOS_SALDO_', N'')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'pagoJob', N'/5 * * * * ?', N'PAGOS_', N'')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'recaudoJob', N'30 * * * * ?', N'RECAUDOXCARGO_', N'')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'refinanciacionJob', N'30 * * * * ?', N'REFINANCIACIONES_', N'')
GO
INSERT [dbo].[JOB_SETTINGS] ([JOB_NAME], [CRON], [FILE_PREFIX], [FILE_SUFFIX])
VALUES (N'trasladosJob', N'30 * * * * ?', N'Traslados_servi_financ_', N'')
GO
INSERT [dbo].[PRODUCTO] ([CODIGO], [NOMBRE], [PRODUCTO_PADRE], [TIPO_PRODUCTO_CODIGO])
VALUES (1, N'SEMILLA', NULL, 1)
GO
INSERT [dbo].[PRODUCTO] ([CODIGO], [NOMBRE], [PRODUCTO_PADRE], [TIPO_PRODUCTO_CODIGO])
VALUES (2, N'PRIVADA', 1, 1)
GO
INSERT [dbo].[PRODUCTO] ([CODIGO], [NOMBRE], [PRODUCTO_PADRE], [TIPO_PRODUCTO_CODIGO])
VALUES (3, N'MASTERCARD', 1, 1)
GO
INSERT [dbo].[PRODUCTO] ([CODIGO], [NOMBRE], [PRODUCTO_PADRE], [TIPO_PRODUCTO_CODIGO])
VALUES (4, N'PRESTAMO PERSONAL', 2, 1)
GO
INSERT [dbo].[PRODUCTO] ([CODIGO], [NOMBRE], [PRODUCTO_PADRE], [TIPO_PRODUCTO_CODIGO])
VALUES (5, N'PUBLICA', NULL, 1)
GO
INSERT [dbo].[SUCURSAL] ([NUMERO], [NOMBRE])
VALUES (1000, NULL)
GO
INSERT [dbo].[TIPO_CONSUMO] ([ID], [NOMBRE])
VALUES (0, N'COMPRAS')
GO
INSERT [dbo].[TIPO_CONSUMO] ([ID], [NOMBRE])
VALUES (1, N'PRESTAMOS')
GO
INSERT [dbo].[TIPO_CONSUMO] ([ID], [NOMBRE])
VALUES (2, N'VENTAS')
GO
INSERT [dbo].[TIPO_DOCUMENTO] ([CODIGO], [NOMBRE])
VALUES (0, NULL)
GO
INSERT [dbo].[TIPO_DOCUMENTO] ([CODIGO], [NOMBRE])
VALUES (1, NULL)
GO
INSERT [dbo].[TIPO_DOCUMENTO] ([CODIGO], [NOMBRE])
VALUES (2, NULL)
GO
INSERT [dbo].[TIPO_DOCUMENTO] ([CODIGO], [NOMBRE])
VALUES (3, NULL)
GO
INSERT [dbo].[TIPO_DOCUMENTO] ([CODIGO], [NOMBRE])
VALUES (4, NULL)
GO
INSERT [dbo].[TIPO_DOCUMENTO] ([CODIGO], [NOMBRE])
VALUES (5, NULL)
GO
INSERT [dbo].[TIPO_NEGOCIACION] ([ID_TIPO_NEGOCIACION], [DESCRIPCION_TIPO_NEGOCIACION])
VALUES (1, N'TIPO NEGOCIACION 1')
GO
INSERT [dbo].[TIPO_NEGOCIACION] ([ID_TIPO_NEGOCIACION], [DESCRIPCION_TIPO_NEGOCIACION])
VALUES (2, N'TIPO NEGOCIACION 2')
GO
INSERT [dbo].[TIPO_NEGOCIACION] ([ID_TIPO_NEGOCIACION], [DESCRIPCION_TIPO_NEGOCIACION])
VALUES (3, N'TIPO NEGOCIACION 3')
GO
INSERT [dbo].[TIPO_PRODUCTO] ([CODIGO], [NOMBRE])
VALUES (1, N'CONSUMO')
GO
INSERT [dbo].[TIPO_TARJETA] ([ID], [NOMBRE])
VALUES (N'1', NULL)
GO
INSERT [dbo].[TIPO_TARJETA] ([ID], [NOMBRE])
VALUES (N'2', NULL)
GO
INSERT [dbo].[TIPO_TARJETA] ([ID], [NOMBRE])
VALUES (N'3', NULL)
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'1014254440', N'Bustamante', N'1274', N'Leonardo')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4567', N'A', N'1234', N'CARLOS')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4568', N'AB', N'1235', N'YOVANNI')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4569', N'A', N'1236', N'ALFREDO')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4570', N'AB', N'1237', N'FREDY')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4571', N'A', N'1238', N'JORGE')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4572', N'AB', N'1239', N'ELIZABETH')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4573', N'A', N'1240', N'JULIO')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4574', N'AB', N'1241', N'DIANA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4575', N'A', N'1242', N'LUZ')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4576', N'AB', N'1243', N'PATRICIA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4577', N'A', N'1244', N'CARLOS')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4578', N'AB', N'1245', N'ELSA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4579', N'A', N'1246', N'EMILCE')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4580', N'AB', N'1247', N'MYRIAM')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4581', N'A', N'1248', N'DANIEL')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4582', N'AB', N'1249', N'PAULA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4583', N'A', N'1250', N'CARMEN')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4584', N'AB', N'1251', N'ANA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4585', N'A', N'1252', N'GLORIA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4586', N'AB', N'1253', N'MARIA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4587', N'A', N'1254', N'YADIRA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4588', N'AB', N'1255', N'MYRIAM')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4589', N'A', N'1256', N'GLADYS')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4590', N'AB', N'1257', N'HENRY')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4591', N'A', N'1258', N'NYDYA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4592', N'AB', N'1259', N'JINNETH')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4593', N'A', N'1260', N'JOSE')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4594', N'AB', N'1261', N'GISELA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4595', N'A', N'1262', N'CLAUDIA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4596', N'AB', N'1263', N'MIREYA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4597', N'A', N'1264', N'LUZ')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4598', N'AB', N'1265', N'ANA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4599', N'A', N'1266', N'MAURICIO')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4600', N'AB', N'1267', N'ANA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4601', N'A', N'1268', N'GLORIA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4602', N'AB', N'1269', N'NUBIA')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4603', N'A', N'1270', N'ANGIE')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4604', N'AB', N'1271', N'INGRID')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4605', N'A', N'1272', N'KAROLL')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4606', N'AB', N'1273', N'HECTOR')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4607', N'A', N'1274', N'OLICES')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4608', N'AB', N'1275', N'JULIO')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4609', N'A', N'1276', N'BEATRIZ')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4610', N'AB', N'1277', N'JESUS')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4611', N'A', N'1278', N'MARY')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4612', N'AB', N'1279', N'AYDEE')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4613', N'A', N'1280', N'ANGEL')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4614', N'AB', N'1281', N'FANNY')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'4615', N'A', N'1282', N'ORLANDO')
GO
INSERT [dbo].[USUARIO] ([ID], [APELLIDOS], [AREA], [NOMBRES])
VALUES (N'co999999', NULL, N'PRUEBA', N'PRUEBA')
GO
ALTER TABLE [dbo].[CARGO]
    ADD DEFAULT ('false') FOR [Cargo_Colpatria]
GO
ALTER TABLE [dbo].[CLIENTE]
    WITH CHECK ADD CONSTRAINT [FK_CLIENTE_CUENTA] FOREIGN KEY ([CUENTA_NUMERO])
        REFERENCES [dbo].[CUENTA] ([NUMERO])
GO
ALTER TABLE [dbo].[CLIENTE]
    CHECK CONSTRAINT [FK_CLIENTE_CUENTA]
GO
ALTER TABLE [dbo].[CLIENTE]
    WITH CHECK ADD CONSTRAINT [FK_CLIENTE_TIPO_DOCUMENTO] FOREIGN KEY ([TIPO_IDENTIFICACION])
        REFERENCES [dbo].[TIPO_DOCUMENTO] ([CODIGO])
GO
ALTER TABLE [dbo].[CLIENTE]
    CHECK CONSTRAINT [FK_CLIENTE_TIPO_DOCUMENTO]
GO
ALTER TABLE [dbo].[CONTRATO]
    WITH CHECK ADD CONSTRAINT [FK_CONTRATO_CLIENTE] FOREIGN KEY ([CLIENTE_NUMERO_ID])
        REFERENCES [dbo].[CLIENTE] ([NUMERO_IDENTIFICACION])
GO
ALTER TABLE [dbo].[CONTRATO]
    CHECK CONSTRAINT [FK_CONTRATO_CLIENTE]
GO
ALTER TABLE [dbo].[CONTRATO]
    WITH CHECK ADD CONSTRAINT [FK_CONTRATO_PRODUCTO] FOREIGN KEY ([SUBPRODUCTO_CODIGO])
        REFERENCES [dbo].[PRODUCTO] ([CODIGO])
GO
ALTER TABLE [dbo].[CONTRATO]
    CHECK CONSTRAINT [FK_CONTRATO_PRODUCTO]
GO
ALTER TABLE [dbo].[CUENTA]
    WITH CHECK ADD CONSTRAINT [FK_CUENTA_CATEGORIA_CUENTA] FOREIGN KEY ([CATEGORIA_CUENTA_ID])
        REFERENCES [dbo].[CATEGORIA_CUENTA] ([ID])
GO
ALTER TABLE [dbo].[CUENTA]
    CHECK CONSTRAINT [FK_CUENTA_CATEGORIA_CUENTA]
GO
ALTER TABLE [dbo].[CUENTA]
    WITH CHECK ADD CONSTRAINT [FK_CUENTA_SUCURSAL] FOREIGN KEY ([SUCURSAL_NUMERO])
        REFERENCES [dbo].[SUCURSAL] ([NUMERO])
GO
ALTER TABLE [dbo].[CUENTA]
    CHECK CONSTRAINT [FK_CUENTA_SUCURSAL]
GO
ALTER TABLE [dbo].[HISTORIAL_ANULACION_PAGO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_ANULACION_PAGO_CARGO] FOREIGN KEY ([CARGO_ID])
        REFERENCES [dbo].[CARGO] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_ANULACION_PAGO]
    CHECK CONSTRAINT [FK_HISTORIAL_ANULACION_PAGO_CARGO]
GO
ALTER TABLE [dbo].[HISTORIAL_ANULACION_PAGO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_ANULACION_PAGO_SERVICIO] FOREIGN KEY ([SERVICIO_NUMERO])
        REFERENCES [dbo].[SERVICIO] ([NUMERO])
GO
ALTER TABLE [dbo].[HISTORIAL_ANULACION_PAGO]
    CHECK CONSTRAINT [FK_HISTORIAL_ANULACION_PAGO_SERVICIO]
GO
ALTER TABLE [dbo].[HISTORIAL_AUTOMORTIZADO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_AUTOMORTIZADO_CARGO] FOREIGN KEY ([CARGO_ID])
        REFERENCES [dbo].[CARGO] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_AUTOMORTIZADO]
    CHECK CONSTRAINT [FK_HISTORIAL_AUTOMORTIZADO_CARGO]
GO
ALTER TABLE [dbo].[HISTORIAL_AUTOMORTIZADO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_AUTOMORTIZADO_SERVICIO] FOREIGN KEY ([SERVICIO_NUMERO])
        REFERENCES [dbo].[SERVICIO] ([NUMERO])
GO
ALTER TABLE [dbo].[HISTORIAL_AUTOMORTIZADO]
    CHECK CONSTRAINT [FK_HISTORIAL_AUTOMORTIZADO_SERVICIO]
GO
ALTER TABLE [dbo].[HISTORIAL_CAMBIO_ESTADO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_CAMBIO_ESTADO_SERVICIO] FOREIGN KEY ([SERVICIO_NUMERO])
        REFERENCES [dbo].[SERVICIO] ([NUMERO])
GO
ALTER TABLE [dbo].[HISTORIAL_CAMBIO_ESTADO]
    CHECK CONSTRAINT [FK_HISTORIAL_CAMBIO_ESTADO_SERVICIO]
GO
ALTER TABLE [dbo].[HISTORIAL_CAMBIO_ESTADO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_CAMBIO_ESTADO_USUARIO] FOREIGN KEY ([USUARIO_ID])
        REFERENCES [dbo].[USUARIO] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_CAMBIO_ESTADO]
    CHECK CONSTRAINT [FK_HISTORIAL_CAMBIO_ESTADO_USUARIO]
GO
ALTER TABLE [dbo].[HISTORIAL_CARGUE_UTILIZACION]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_CARGUE_UTILIZACION_CLIENTE] FOREIGN KEY ([CLIENTE_NUMERO_IDENTIFICACION])
        REFERENCES [dbo].[CLIENTE] ([NUMERO_IDENTIFICACION])
GO
ALTER TABLE [dbo].[HISTORIAL_CARGUE_UTILIZACION]
    CHECK CONSTRAINT [FK_HISTORIAL_CARGUE_UTILIZACION_CLIENTE]
GO
ALTER TABLE [dbo].[HISTORIAL_CARGUE_UTILIZACION]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_CARGUE_UTILIZACION_SERVICIO] FOREIGN KEY ([SERVICIO_NUMERO])
        REFERENCES [dbo].[SERVICIO] ([NUMERO])
GO
ALTER TABLE [dbo].[HISTORIAL_CARGUE_UTILIZACION]
    CHECK CONSTRAINT [FK_HISTORIAL_CARGUE_UTILIZACION_SERVICIO]
GO
ALTER TABLE [dbo].[HISTORIAL_CARGUE_UTILIZACION]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_CARGUE_UTILIZACION_SUBPRODUCTO] FOREIGN KEY ([SUBPRODUCTO_CODIGO])
        REFERENCES [dbo].[PRODUCTO] ([CODIGO])
GO
ALTER TABLE [dbo].[HISTORIAL_CARGUE_UTILIZACION]
    CHECK CONSTRAINT [FK_HISTORIAL_CARGUE_UTILIZACION_SUBPRODUCTO]
GO
ALTER TABLE [dbo].[HISTORIAL_CARTERA]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_CARTERA_SERVICIO] FOREIGN KEY ([SERVICIO_NUMERO])
        REFERENCES [dbo].[SERVICIO] ([NUMERO])
GO
ALTER TABLE [dbo].[HISTORIAL_CARTERA]
    CHECK CONSTRAINT [FK_HISTORIAL_CARTERA_SERVICIO]
GO
ALTER TABLE [dbo].[HISTORIAL_CARTERA_RESUMEN]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_CARTERA_RESUMEN_CONTRATO] FOREIGN KEY ([CONTRATO_NUMERO])
        REFERENCES [dbo].[CONTRATO] ([NUMERO])
GO
ALTER TABLE [dbo].[HISTORIAL_CARTERA_RESUMEN]
    CHECK CONSTRAINT [FK_HISTORIAL_CARTERA_RESUMEN_CONTRATO]
GO
ALTER TABLE [dbo].[HISTORIAL_CONDONACION]
    WITH CHECK ADD CONSTRAINT [FK_CONDONACION_CONTRATO] FOREIGN KEY ([NUMERO_CONTRATO])
        REFERENCES [dbo].[CONTRATO] ([NUMERO])
GO
ALTER TABLE [dbo].[HISTORIAL_CONDONACION]
    CHECK CONSTRAINT [FK_CONDONACION_CONTRATO]
GO
ALTER TABLE [dbo].[HISTORIAL_CONDONACION]
    WITH CHECK ADD CONSTRAINT [FK_CONDONACION_NEGOCIACION] FOREIGN KEY ([NEGOCIACION_ID])
        REFERENCES [dbo].[NEGOCIACION] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_CONDONACION]
    CHECK CONSTRAINT [FK_CONDONACION_NEGOCIACION]
GO
ALTER TABLE [dbo].[HISTORIAL_CONDONACION]
    WITH CHECK ADD CONSTRAINT [FK_CONDONACION_TIPO_NEGOCIACION] FOREIGN KEY ([TIPO_NEGOCIACION])
        REFERENCES [dbo].[TIPO_NEGOCIACION] ([ID_TIPO_NEGOCIACION])
GO
ALTER TABLE [dbo].[HISTORIAL_CONDONACION]
    CHECK CONSTRAINT [FK_CONDONACION_TIPO_NEGOCIACION]
GO
ALTER TABLE [dbo].[HISTORIAL_CONDONACION]
    WITH CHECK ADD CONSTRAINT [FK_CONDONACION_TIPO_PRODUCTO] FOREIGN KEY ([TIPO_PRODUCTO_ID])
        REFERENCES [dbo].[TIPO_PRODUCTO] ([CODIGO])
GO
ALTER TABLE [dbo].[HISTORIAL_CONDONACION]
    CHECK CONSTRAINT [FK_CONDONACION_TIPO_PRODUCTO]
GO
ALTER TABLE [dbo].[HISTORIAL_CONDONACION]
    WITH CHECK ADD CONSTRAINT [FK_CONDONACION_USUARIO] FOREIGN KEY ([USUARIO_ID])
        REFERENCES [dbo].[USUARIO] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_CONDONACION]
    CHECK CONSTRAINT [FK_CONDONACION_USUARIO]
GO
ALTER TABLE [dbo].[HISTORIAL_MOVIMIENTO_SALDO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_MOVIMIENTO_SALDO_CARGO] FOREIGN KEY ([CARGO_ID])
        REFERENCES [dbo].[CARGO] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_MOVIMIENTO_SALDO]
    CHECK CONSTRAINT [FK_HISTORIAL_MOVIMIENTO_SALDO_CARGO]
GO
ALTER TABLE [dbo].[HISTORIAL_MOVIMIENTO_SALDO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_MOVIMIENTO_SALDO_CONTRATO] FOREIGN KEY ([CONTRATO_NUMERO])
        REFERENCES [dbo].[CONTRATO] ([NUMERO])
GO
ALTER TABLE [dbo].[HISTORIAL_MOVIMIENTO_SALDO]
    CHECK CONSTRAINT [FK_HISTORIAL_MOVIMIENTO_SALDO_CONTRATO]
GO
ALTER TABLE [dbo].[HISTORIAL_MOVIMIENTO_SALDO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_MOVIMIENTO_SALDO_NEGOCIACION] FOREIGN KEY ([NEGOCIACION_NUMERO])
        REFERENCES [dbo].[NEGOCIACION] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_MOVIMIENTO_SALDO]
    CHECK CONSTRAINT [FK_HISTORIAL_MOVIMIENTO_SALDO_NEGOCIACION]
GO
ALTER TABLE [dbo].[HISTORIAL_MOVIMIENTO_SALDO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_MOVIMIENTO_SALDO_USUARIO] FOREIGN KEY ([USUARIO_CREADOR_ID])
        REFERENCES [dbo].[USUARIO] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_MOVIMIENTO_SALDO]
    CHECK CONSTRAINT [FK_HISTORIAL_MOVIMIENTO_SALDO_USUARIO]
GO
ALTER TABLE [dbo].[HISTORIAL_MOVIMIENTO_SALDO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_MOVIMIENTO_SALDO_USUARIO2] FOREIGN KEY ([USUARIO_APROBADOR_ID])
        REFERENCES [dbo].[USUARIO] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_MOVIMIENTO_SALDO]
    CHECK CONSTRAINT [FK_HISTORIAL_MOVIMIENTO_SALDO_USUARIO2]
GO
ALTER TABLE [dbo].[HISTORIAL_PAGO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_PAGO_CLIENTE] FOREIGN KEY ([IDENTIFICACION_CLIENTE])
        REFERENCES [dbo].[CLIENTE] ([NUMERO_IDENTIFICACION])
GO
ALTER TABLE [dbo].[HISTORIAL_PAGO]
    CHECK CONSTRAINT [FK_HISTORIAL_PAGO_CLIENTE]
GO
ALTER TABLE [dbo].[HISTORIAL_PAGO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_PAGO_PRODUCTO] FOREIGN KEY ([CODIGO_SUBPRODUCTO])
        REFERENCES [dbo].[PRODUCTO] ([CODIGO])
GO
ALTER TABLE [dbo].[HISTORIAL_PAGO]
    CHECK CONSTRAINT [FK_HISTORIAL_PAGO_PRODUCTO]
GO
ALTER TABLE [dbo].[HISTORIAL_PAGO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_PAGO_SUCURSAL] FOREIGN KEY ([SUCURSAL_NUMERO])
        REFERENCES [dbo].[SUCURSAL] ([NUMERO])
GO
ALTER TABLE [dbo].[HISTORIAL_PAGO]
    CHECK CONSTRAINT [FK_HISTORIAL_PAGO_SUCURSAL]
GO
ALTER TABLE [dbo].[HISTORIAL_RECAUDO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_RECAUDO_CARGO] FOREIGN KEY ([CARGO_ID])
        REFERENCES [dbo].[CARGO] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_RECAUDO]
    CHECK CONSTRAINT [FK_HISTORIAL_RECAUDO_CARGO]
GO
ALTER TABLE [dbo].[HISTORIAL_RECAUDO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_RECAUDO_FORMA_PAGO] FOREIGN KEY ([FORMA_PAGO_ID])
        REFERENCES [dbo].[FORMA_PAGO] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_RECAUDO]
    CHECK CONSTRAINT [FK_HISTORIAL_RECAUDO_FORMA_PAGO]
GO
ALTER TABLE [dbo].[HISTORIAL_RECAUDO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_RECAUDO_RECAUDO_CARGO] FOREIGN KEY ([CODIGO_RECAUDO_CARGO])
        REFERENCES [dbo].[RECAUDO_CARGO] ([CODIGO])
GO
ALTER TABLE [dbo].[HISTORIAL_RECAUDO]
    CHECK CONSTRAINT [FK_HISTORIAL_RECAUDO_RECAUDO_CARGO]
GO
ALTER TABLE [dbo].[HISTORIAL_REFINANCIACION]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_REFINANCIACION_NEGOCIACION] FOREIGN KEY ([NEGOCIACION_ID])
        REFERENCES [dbo].[NEGOCIACION] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_REFINANCIACION]
    CHECK CONSTRAINT [FK_HISTORIAL_REFINANCIACION_NEGOCIACION]
GO
ALTER TABLE [dbo].[HISTORIAL_REFINANCIACION]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_REFINANCIACION_SERVICIO] FOREIGN KEY ([SERVICIO_NUMERO])
        REFERENCES [dbo].[SERVICIO] ([NUMERO])
GO
ALTER TABLE [dbo].[HISTORIAL_REFINANCIACION]
    CHECK CONSTRAINT [FK_HISTORIAL_REFINANCIACION_SERVICIO]
GO
ALTER TABLE [dbo].[HISTORIAL_REFINANCIACION]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_REFINANCIACION_USUARIO1] FOREIGN KEY ([USUARIO_ID])
        REFERENCES [dbo].[USUARIO] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_REFINANCIACION]
    CHECK CONSTRAINT [FK_HISTORIAL_REFINANCIACION_USUARIO1]
GO
ALTER TABLE [dbo].[HISTORIAL_TRASLADO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_TRASLADO_CONTRATO] FOREIGN KEY ([CONTRATO_NUMERO])
        REFERENCES [dbo].[CONTRATO] ([NUMERO])
GO
ALTER TABLE [dbo].[HISTORIAL_TRASLADO]
    CHECK CONSTRAINT [FK_HISTORIAL_TRASLADO_CONTRATO]
GO
ALTER TABLE [dbo].[HISTORIAL_TRASLADO]
    WITH CHECK ADD CONSTRAINT [FK_HISTORIAL_TRASLADO_USUARIO] FOREIGN KEY ([USUARIO_ID])
        REFERENCES [dbo].[USUARIO] ([ID])
GO
ALTER TABLE [dbo].[HISTORIAL_TRASLADO]
    CHECK CONSTRAINT [FK_HISTORIAL_TRASLADO_USUARIO]
GO
ALTER TABLE [dbo].[PRODUCTO]
    WITH CHECK ADD CONSTRAINT [FK_PRODUCTO_PRODUCTO] FOREIGN KEY ([TIPO_PRODUCTO_CODIGO])
        REFERENCES [dbo].[PRODUCTO] ([CODIGO])
GO
ALTER TABLE [dbo].[PRODUCTO]
    CHECK CONSTRAINT [FK_PRODUCTO_PRODUCTO]
GO
ALTER TABLE [dbo].[PRODUCTO]
    WITH CHECK ADD CONSTRAINT [FK_PRODUCTO_TIPO_PRODUCTO] FOREIGN KEY ([TIPO_PRODUCTO_CODIGO])
        REFERENCES [dbo].[TIPO_PRODUCTO] ([CODIGO])
GO
ALTER TABLE [dbo].[PRODUCTO]
    CHECK CONSTRAINT [FK_PRODUCTO_TIPO_PRODUCTO]
GO
ALTER TABLE [dbo].[RECAUDO_CARGO]
    WITH CHECK ADD CONSTRAINT [FK_RECAUDO_CARGO_CLIENTE] FOREIGN KEY ([CLIENTE_NUMERO_ID])
        REFERENCES [dbo].[CLIENTE] ([NUMERO_IDENTIFICACION])
GO
ALTER TABLE [dbo].[RECAUDO_CARGO]
    CHECK CONSTRAINT [FK_RECAUDO_CARGO_CLIENTE]
GO
ALTER TABLE [dbo].[RECAUDO_CARGO]
    WITH CHECK ADD CONSTRAINT [FK_RECAUDO_CARGO_SERVICIO] FOREIGN KEY ([SERVICIO])
        REFERENCES [dbo].[SERVICIO] ([NUMERO])
GO
ALTER TABLE [dbo].[RECAUDO_CARGO]
    CHECK CONSTRAINT [FK_RECAUDO_CARGO_SERVICIO]
GO
ALTER TABLE [dbo].[SERVICIO]
    WITH CHECK ADD CONSTRAINT [FK_SERVICIO_CLASE_SERVICIO] FOREIGN KEY ([CLASE_SERVICIO_ID])
        REFERENCES [dbo].[CLASE_SERVICIO] ([ID])
GO
ALTER TABLE [dbo].[SERVICIO]
    CHECK CONSTRAINT [FK_SERVICIO_CLASE_SERVICIO]
GO
ALTER TABLE [dbo].[SERVICIO]
    WITH CHECK ADD CONSTRAINT [FK_SERVICIO_CONTRATO] FOREIGN KEY ([CONTRATO_NUMERO])
        REFERENCES [dbo].[CONTRATO] ([NUMERO])
GO
ALTER TABLE [dbo].[SERVICIO]
    CHECK CONSTRAINT [FK_SERVICIO_CONTRATO]
GO
ALTER TABLE [dbo].[SERVICIO]
    WITH CHECK ADD CONSTRAINT [FK_SERVICIO_TIPO_TARJETA] FOREIGN KEY ([TIPO_TARJETA_ID])
        REFERENCES [dbo].[TIPO_TARJETA] ([ID])
GO
ALTER TABLE [dbo].[SERVICIO]
    CHECK CONSTRAINT [FK_SERVICIO_TIPO_TARJETA]
GO


CREATE TABLE dbo.DEPARTAMENTO
(
    CODIGO varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
    NOMBRE varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    CONSTRAINT PK_DEPARTAMENTO PRIMARY KEY (CODIGO)
);
GO
CREATE TABLE dbo.MUNICIPIO
(
    CODIGO              varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
    NOMBRE              varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    DEPARTAMENTO_CODIGO varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
    CONSTRAINT PK_MUNICIPIO PRIMARY KEY (CODIGO),
    CONSTRAINT FK_MUNICIPIO_DEPARTAMENTO FOREIGN KEY (DEPARTAMENTO_CODIGO) REFERENCES dbo.DEPARTAMENTO (CODIGO)
);
GO
CREATE TABLE dbo.LOCALIDAD
(
    CODIGO           varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
    NOMBRE           varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    MUNICIPIO_CODIGO varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
    CONSTRAINT PK_LOCALIDAD PRIMARY KEY (CODIGO),
    CONSTRAINT FK_LOCALIDAD_MUNICIPIO FOREIGN KEY (MUNICIPIO_CODIGO) REFERENCES dbo.MUNICIPIO (CODIGO)
);
GO
CREATE TABLE dbo.BARRIO
(
    CODIGO           varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
    NOMBRE           varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    LOCALIDAD_CODIGO varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
    CONSTRAINT PK_BARRIO PRIMARY KEY (CODIGO),
    CONSTRAINT FK_BARRIO_LOCALIDAD FOREIGN KEY (LOCALIDAD_CODIGO) REFERENCES dbo.LOCALIDAD (CODIGO)
);
GO
CREATE TABLE dbo.LECTURA
(
    ID              bigint IDENTITY (1,1)                             NOT NULL,
    MANZANA         varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    SUCURSAL_NUMERO int                                               NOT NULL,
    ZONA            varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    CICLO           int                                               NOT NULL,
    GRUPO           varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
    BARRIO_CODIGO   varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
    LOCALIZACION    varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    CONSTRAINT PK_LECTURA PRIMARY KEY (ID),
    CONSTRAINT FK_LECTURA_BARRIO FOREIGN KEY (BARRIO_CODIGO) REFERENCES dbo.BARRIO (CODIGO),
    CONSTRAINT FK_LECTURA_SUCURSAL FOREIGN KEY (SUCURSAL_NUMERO) REFERENCES dbo.SUCURSAL (NUMERO)
);
GO
CREATE TABLE dbo.REPARTO
(
    ID              bigint IDENTITY (1,1)                             NOT NULL,
    DIRECCION       varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    MANZANA         varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    SUCURSAL_NUMERO int                                               NOT NULL,
    ZONA            varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    CICLO           int                                               NOT NULL,
    GRUPO           varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
    CONSTRAINT PK_REPARTO PRIMARY KEY (ID),
    CONSTRAINT FK_REPARTO_SUCURSAL FOREIGN KEY (SUCURSAL_NUMERO) REFERENCES dbo.SUCURSAL (NUMERO)
);
GO
CREATE TABLE dbo.HISTORIAL_MAESTRO_CLIENTE
(
    ID                              bigint IDENTITY (1,1)                             NOT NULL,
    CLIENTE_NUMERO_ID               decimal(15)                                       NULL,
    SERVICIO_NUMERO                 varchar(55) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
    LECTURA_ID                      bigint                                            NOT NULL,
    REPARTO_ID                      bigint                                            NOT NULL,
    ESTADO_SERVICIO_ELECTRICO       varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    CODIGO_ACTIVIDAD_ECONOMICA      varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    DESCRIPCION_ACTIVIDAD_ECONOMICA varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    CODIGO_INTERNO                  varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    CONSTRAINT PK_HISTORIAL_MAESTRO_CLIENTE PRIMARY KEY (ID),
    CONSTRAINT FK_HISTORIAL_MAESTRO_CLIENTE_SERVICIO FOREIGN KEY (SERVICIO_NUMERO) REFERENCES dbo.SERVICIO (NUMERO),
    CONSTRAINT FK_HISTORIAL_MAESTRO_CLIENTE_LECTURA FOREIGN KEY (LECTURA_ID) REFERENCES dbo.LECTURA (ID),
    CONSTRAINT FK_HISTORIAL_MAESTRO_CLIENTE_REPARTO FOREIGN KEY (REPARTO_ID) REFERENCES dbo.REPARTO (ID),
    CONSTRAINT FK_HISTORIAL_MAESTRO_CLIENTE_CLIENTE FOREIGN KEY (CLIENTE_NUMERO_ID) REFERENCES dbo.CLIENTE (NUMERO_IDENTIFICACION)
);
GO
INSERT INTO dbo.DEPARTAMENTO (CODIGO, NOMBRE)
VALUES (N'1', N'VALLE');
GO
INSERT INTO dbo.MUNICIPIO (CODIGO, NOMBRE, DEPARTAMENTO_CODIGO)
VALUES (N'403', N'TULUA', N'1');
GO
INSERT INTO dbo.LOCALIDAD (CODIGO, NOMBRE, MUNICIPIO_CODIGO)
VALUES (N'113', N'AGUACLARA', N'403');
GO

INSERT INTO dbo.JOB_SETTINGS (JOB_NAME, CRON, FILE_PREFIX, FILE_SUFFIX)
VALUES (N'maestroClienteJob', N'45 * * * * ?', N'MCLIENTES_', N'');
GO