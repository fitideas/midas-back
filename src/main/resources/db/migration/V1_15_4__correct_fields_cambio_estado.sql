ALTER TABLE dbo.HISTORIAL_CAMBIO_ESTADO ALTER COLUMN SERVICIO_ESTADO_ANTERIOR varchar(30);
ALTER TABLE dbo.HISTORIAL_CAMBIO_ESTADO ALTER COLUMN SERVICIO_ESTADO_NUEVO varchar(30);
ALTER TABLE dbo.HISTORIAL_CAMBIO_ESTADO ALTER COLUMN OBSERVACION varchar(500);
