CREATE TABLE [dbo].[HISTORIAL_CARTERA_TOTAL]
(
    [ID]                                           [bigint] IDENTITY (1,1) NOT NULL,
    [CAPITAL_IMPAGO]                               [decimal](18, 4)        NULL,
    [DIAS_ATRASO]                                  [int]                   NULL,
    [FECHA_PRIMER_VENCIMIENTO]                     [date]                  NULL,
    [FECHA_REPORTE]                                [date]                  NULL,
    [FECHA_SEGUNDO_VENCIMIENTO]                    [date]                  NULL,
    [INTERES_CAUSADO_NO_FACTURADO]                 [decimal](18, 4)        NULL,
    [INTERES_CORRIENTE_ORDEN_CAUSADO_NO_FACTURADO] [decimal](18, 4)        NULL,
    [INTERES_FACTURADO_NO_AFECTO]                  [decimal](18, 4)        NULL,
    [INTERES_IMPAGO]                               [decimal](18, 4)        NULL,
    [INTERES_MORA]                                 [decimal](18, 4)        NULL,
    [INTERES_MORA_CAUSADO_NO_FACTURADO]            [decimal](18, 4)        NULL,
    [INTERES_MORA_ORDEN_CAUSADO_NO_FACTURADO]      [decimal](18, 4)        NULL,
    [INTERES_NO_AFECTO_POR_FACTURAR]               [decimal](18, 4)        NULL,
    [INTERES_ORDEN_CORRIENTE_IMPAGO]               [decimal](18, 4)        NULL,
    [INTERES_ORDEN_MORA_IMPAGO]                    [decimal](18, 4)        NULL,
    [MORA_PRIMER_VENCIMIENTO]                      [decimal](18, 4)        NULL,
    [MOTIVO_FINALIZACION]                          [varchar](255)          NULL,
    [TOTAL_INTERES_CORRIENTE_CAUSADO]              [decimal](18, 4)        NULL,
    [TOTAL_INTERES_CORRIENTE_FACTURADO_IMPAGO]     [decimal](18, 4)        NULL,
    [TOTAL_INTERES_MORA_CAUSADO]                   [decimal](18, 4)        NULL,
    [TOTAL_INTERES_MORA_FACTURADO_IMPAGO]          [decimal](18, 4)        NULL,
    [SERVICIO_NUMERO]                              [varchar](55)           NULL,
    [DESCRIPCION]                                  [varchar](255)          NULL,
    [FECHA_TRASLADO]                               [date]                  NULL,
    [CODIGO_SEGURO_OBLIGATORIO]                    [varchar](21)           NULL,
    CONSTRAINT [PK_HISTORIAL_CARTERA_TOTAL] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

EXEC sys.sp_rename N'dbo.HISTORIAL_CARTERA', N'HISTORIAL_CARTERA_LIGHT', 'OBJECT';
