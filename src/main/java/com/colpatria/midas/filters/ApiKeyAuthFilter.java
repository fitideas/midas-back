package com.colpatria.midas.filters;

/*
 *
 * Libraries
 *
*/

import javax.servlet.http.HttpServletRequest;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

/**
 * Configuration class for the request security
*/
public class ApiKeyAuthFilter extends AbstractPreAuthenticatedProcessingFilter {
    
    /**
     * Api key header name
    */
    private String apiKeyHeaderName;

    /**
     * Class constructor
     * @param apiKeyHeaderName is the api key header name
    */
    public ApiKeyAuthFilter( String apiKeyHeaderName ) {
        this.apiKeyHeaderName = apiKeyHeaderName;
    }

    /**
     * Method that extracts the api key from the request header
     * @param request is the api key header name
    */
    @Override
    protected Object getPreAuthenticatedPrincipal( HttpServletRequest request ) {
        return request.getHeader( this.apiKeyHeaderName );
    }

    /**
     * Method that extracts the credentials from the request header
     * @param request is the api key header name
    */
    @Override
    protected Object getPreAuthenticatedCredentials( HttpServletRequest request ) {
        return "N/A";
    }
}
