package com.colpatria.midas.model;

/*
 *
 * Librerías
 *
*/

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 *
 * Clase
 *
*/

@Entity
@Table(name = "TIPO_CONSUMO")
public class TipoConsumo implements Serializable {

    /*
     *
     * Atríbutos
     *
    */

    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NOMBRE")
    private String nombre;

    /*
     *
     * Métodos
     *
    */

    /**
	 * Compare both objects
	 * @param tipoConsumo is the new contract object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( TipoConsumo tipoConsumo ) {
        return tipoConsumo != null && this.id.equals(tipoConsumo.getId());
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
