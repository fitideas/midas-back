package com.colpatria.midas.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Model of CUENTA table in the database where the objects and methods are declared
 */
@Entity
@Table(name = "CUENTA")
public class Cuenta implements Serializable {

	/**
	 * Double that refers to the number of account
	 */
	@Id
	@Column(name = "NUMERO")
	private Double numero;

	/**
	 * Integer that refers to the stratum
	 */
	@Column(name = "ESTRATO")
	private Integer estrato;

	/**
	 * String that refers to the address
	 */
	@Column(name = "DIRECCION")
	private String direccion;

	/**
	 * String that refers to the city
	 */
	@Column(name = "MUNICIPIO")
	private Long municipio;

	/**
	 * String that refers to location
	 */
	@Column(name = "LOCALIZACION")
	private String  localizacion;

	/**
	 * Variable thar refers to the many-to-one relation between SUCURSAL and CUENTA
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SUCURSAL_NUMERO", referencedColumnName = "NUMERO" )
	private Sucursal sucursal;

	/**
	 * Variable thar refers to the one-to-one relation between CLIENTE and CUENTA
	 */
	@OneToOne(mappedBy = "cuenta", cascade = CascadeType.ALL)
	private Cliente cliente;

	/**
	 * Integer that refers to the cycle
	 */
	@Column(name = "CICLO_FACTURACION")
	private Integer cicloFacturacion;

	/**
	 * Variable thar refers to the many-to-one relation between CATEGORIA and CUENTA
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CATEGORIA_CUENTA_ID", referencedColumnName = "ID" )
	private CategoriaCuenta categoriaCuenta;

	/**
	 * Compare both objects
	 * @param cuenta is the new account object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( Cuenta cuenta ) {
		if( cuenta == null || !this.numero.equals( cuenta.getNumero() ) ) {
			return false;
		}
		return true;
	}

	/**
	 * Get the number account of a class
	 * @return String that refers to the number of account
	 */
	public Double getNumero() {
		return numero;
	}

	/**
	 * Set the number account of a class
	 * @param numero String that refers to the number of account
	 */
	public void setNumero(Double numero) {
		this.numero = numero;
	}

	/**
	 * Get the stratum of a class
	 * @return Integer that refers to the stratum
	 */
	public Integer getEstrato() {
		return estrato;
	}

	/**
	 * Set the stratum of a class
	 * @param estrato Integer that refers to the stratum
	 */
	public void setEstrato(Integer estrato) {
		this.estrato = estrato;
	}

	/**
	 * Get the address of a class
	 * @return String that refers to the address
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * Set the address of a class
	 * @param direccion String that refers to the address
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * Get the city of a class
	 * @return String that refers to the city
	 */
	public Long getMunicipio() {
		return municipio;
	}

	/**
	 * Set the city of a class
	 * @param municipio String that refers to the city
	 */
	public void setMunicipio(Long municipio) {
		this.municipio = municipio;
	}

	/**
	 * Get the location of a class
	 * @return String that refers to location
	 */
	public String getLocalizacion() {
		return localizacion;
	}

	/**
	 * Set the location of a class
	 * @param localizacion String that refers to location
	 */
	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}

	/**
	 * Get SUCURSAL-CUENTA relation of a class
	 * @return Variable thar refers to the many-to-one relation between SUCURSAL and CUENTA
	 */
	public Sucursal getSucursal() {
		return sucursal;
	}

	/**
	 * Set SUCURSAL-CUENTA relation of a class
	 * @param sucursal Variable thar refers to the many-to-one relation between SUCURSAL and CUENTA
	 */
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * Get the CILO_FACTURACION-CUENTA relation of a class
	 * @return Variable thar refers to the many-to-one relation between CICLO_FACTURACION and CUENTA
	 */
	public Integer getCicloFacturacion() {
		return this.cicloFacturacion;
	}

	/**
	 * Set the CILO_FACTURACION-CUENTA relation of a class
	 * @param cicloFacturacion Variable thar refers to the many-to-one relation between CICLO_FACTURACION and CUENTA
	 */
	public void setCicloFacturacion(Integer cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}

	/**
	 * Get the CATEGORIA-CUENTA relation of a class
	 * @return Variable thar refers to the many-to-one relation between CATEGORIA and CUENTA
	 */
	public CategoriaCuenta getCategoria() {
		return this.categoriaCuenta;
	}

	/**
	 * Set the CATEGORIA-CUENTA relation of a class
	 * @param categoriaCuenta Variable thar refers to the many-to-one relation between CATEGORIA and CUENTA
	 */
	public void setCategoria(CategoriaCuenta categoriaCuenta) {
		this.categoriaCuenta = categoriaCuenta;
	}

	/**
	 * Get the CLIENTE-CUENTA relation of a class
	 * @return Variable thar refers to the ONE-to-one relation between CLIENTE and CUENTA
	 */
	public Cliente getCliente() {
		return cliente;
	}
	/**
	 * Set the CLIENTE-CUENTA relation of a class
	 * @param cliente Variable thar refers to the one-to-one relation between CLIENTE and CUENTA
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}