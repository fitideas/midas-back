package com.colpatria.midas.model;

/*
 *
 * Libraries
 *
*/

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import javax.persistence.*;

/*
 *
 * Class
 *
*/

@Entity
@Table(name="HISTORIAL_REFINANCIACION")
public class HistorialRefinanciacion implements Serializable {

    /**
     * BigInteger that specifies the record ID.
    */
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

    /**
     * Servicio that specifies the relation between SERVICE and HISTORIAL_REFINANCIACION.
    */
    @ManyToOne
	@JoinColumn(name="SERVICIO_NUMERO", referencedColumnName = "NUMERO")
	private Servicio servicio;

    /**
     * Usuario that specifies the relation between USUARIO and HISTORIAL_REFINANCIACION.
    */
	@ManyToOne
	@JoinColumn(name="USUARIO_ID", referencedColumnName = "ID")
	private Usuario usuario;

    /**
     * Negociacion that specifies the relation between NEGOCIACION and HISTORIAL_REFINANCIACION.
    */
    @ManyToOne
	@JoinColumn(name="NEGOCIACION_ID", referencedColumnName = "ID")
	private Negociacion negociacion;

    /**
     * Double that specifies the payment value.
    */
    @Column(name = "VALOR_PAGO")
    private Double valorPago;

    /**
     * Double that specifies the initial fee.
    */
    @Column(name = "CUOTA_INICIAL")
    private Double cuotaInicial;

    /**
     * Double specifying the whether there is a discount.
    */
    @Column(name = "DESCUENTO_CUOTA_INICIAL")
    private Double descuentoCuotaInicial;

    /**
     * LocalDate that specifies the activation date.
    */
    @Column(name = "FECHA_ACT")
    private LocalDate fechaAct;

    /**
     * LocalDate that specifies the application date.
    */
    @Column(name = "FECHA_APLI")
    private LocalDate fechaApli;

    /**
     * LocalDate that specifies the rejection date.
    */
    @Column(name = "FECHA_RECHAZO")
    private LocalDate fechaRechazo;

    /**
     * String that specifies the reason for rejection.
    */
    @Column(name = "MOTIVO_RECHAZO")
    private String motivoRechazo;

    /**
     * Integer that specifies the number of previous installments.
    */
    @Column(name = "CANTIDAD_CUOTAS_ANTERIORES")
    private Integer cantidadCuotasAnteriores;

    /**
     * Integer that specifies the previous monthly payment.
    */
    @Column(name = "CUOTA_MENSUAL_ANTERIOR")
    private Double cuotaMensualAnterior;

    /**
     * Integer that specifies the number of installments after.
    */
    @Column(name = "CANTIDAD_CUOTAS_DESPUES_DE_REFINANCIACION")
    private Integer cantidadCuotasDespuesDeRefinanciacion;

    /**
     * Integer that specifies the monthly fee after.
    */
    @Column(name = "CUOTA_MENSUAL_DESPUES")
    private Double cuotaMensualDespues;

    /**
     * Integer that specifies the previous rate.
    */
    @Column(name = "TASA_ANTERIOR")
    private Double tasaAnterior;

    /**
     * Integer that specifies the new rate.
    */
    @Column(name = "TASA_NUEVA")
    private Double tasaNueva;

    /**
     * Double that specifies whether a rate exists.
    */
    @Column(name = "TASA_CAMBIO_PLAZO")
    private Double tasaCambioPlazo;

    /**
     * Get the record id.
     * @return the record id.
    */
    public BigInteger getId() {
        return id;
    }

    /**
	 * Set the record id
	 * @param id is the record id
	*/
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
     * Get the number of previous installments.
     * @return the number of previous installments.
    */
    public Integer getCantidadCuotasAnteriores() {
        return cantidadCuotasAnteriores;
    }

    /**
	 * Set the number of previous installments.
	 * @param cantidadCuotasAnteriores is the number of previous installments.
	*/
    public void setCantidadCuotasAnteriores(Integer cantidadCuotasAnteriores) {
        this.cantidadCuotasAnteriores = cantidadCuotasAnteriores;
    }

    /**
     * Get the number of installments after.
     * @return the number of installments after.
    */
    public Integer getCantidadCuotasDespuesDeRefinanciacion() {
        return cantidadCuotasDespuesDeRefinanciacion;
    }

    /**
	 * Set the number of installments after.
	 * @param cantidadCuotasDespuesDeRefinanciacion is the number of installments after.
	*/
    public void setCantidadCuotasDespuesDeRefinanciacion(Integer cantidadCuotasDespuesDeRefinanciacion) {
        this.cantidadCuotasDespuesDeRefinanciacion = cantidadCuotasDespuesDeRefinanciacion;
    }

    /**
     * Get the service.
     * @return the service.
    */
    public Servicio getServicio() {
        return this.servicio;
    }

    /**
	 * Set the service.
	 * @param servicio is the service.
	*/
    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    /**
     * Get the user.
     * @return the user.
    */
    public Usuario getUsuario() {
        return this.usuario;
    }

    /**
	 * Set the user.
	 * @param usuario is the user.
	*/
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * Get the payment value.
     * @return the payment value.
    */
    public Double getValorPago() {
        return this.valorPago;
    }

    /**
	 * Set the payment value.
	 * @param valorPago is the payment value.
	*/
    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    /**
     * Get the initial fee.
     * @return the initial fee.
    */
    public Double getCuotaInicial() {
        return this.cuotaInicial;
    }

    /**
	 * Set the initial fee.
	 * @param cuotaInicial is the initial fee.
	*/
    public void setCuotaInicial(Double cuotaInicial) {
        this.cuotaInicial = cuotaInicial;
    }

    /**
     * Get the field that determines if a discount exists.
     * @return the field that determines if a discount exists.
    */
    public Double getDescuentoCuotaInicial() {
        return this.descuentoCuotaInicial;
    }

    /**
	 * Set if there is a discount.
	 * @param descuentoCuotaInicial is the field that determines if a discount exists.
	*/
    public void setDescuentoCuotaInicial(Double descuentoCuotaInicial) {
        this.descuentoCuotaInicial = descuentoCuotaInicial;
    }

    /**
     * Get the activation date.
     * @return the activation date.
    */
    public LocalDate getFechaAct() {
        return this.fechaAct;
    }

    /**
	 * Set the activation date.
	 * @param fechaAct is the activation date.
	*/
    public void setFechaAct(LocalDate fechaAct) {
        this.fechaAct = fechaAct;
    }

    /**
     * Get the application date.
     * @return the application date.
    */
    public LocalDate getFechaApli() {
        return this.fechaApli;
    }

    /**
	 * Set the application date.
	 * @param fechaApli is the application date.
	*/
    public void setFechaApli(LocalDate fechaApli) {
        this.fechaApli = fechaApli;
    }

    /**
     * Get the rejection date.
     * @return the rejection date.
    */
    public LocalDate getFechaRechazo() {
        return this.fechaRechazo;
    }

    /**
	 * Set the rejection date.
	 * @param fechaRechazo is the rejection date.
	*/
    public void setFechaRechazo(LocalDate fechaRechazo) {
        this.fechaRechazo = fechaRechazo;
    }

    /**
     * Get the reason for rejection.
     * @return the reason for rejection.
    */
    public String getMotivoRechazo() {
        return this.motivoRechazo;
    }

    /**
	 * Set the reason for rejection.
	 * @param motivoRechazo is the reason for rejection.
	*/
    public void setMotivoRechazo(String motivoRechazo) {
        this.motivoRechazo = motivoRechazo;
    }

    /**
     * Get the monthly fee after.
     * @return the monthly fee after.
    */
    public Double getCuotaMensualDespues() {
        return this.cuotaMensualDespues;
    }

    /**
	 * Set the monthly fee after.
	 * @param cuotaMensualDespues is the monthly fee after.
	*/
    public void setCuotaMensualDespues(Double cuotaMensualDespues) {
        this.cuotaMensualDespues = cuotaMensualDespues;
    }

    /**
     * Get the previous rate.
     * @return the previous rate.
    */
    public Double getTasaAnterior() {
        return this.tasaAnterior;
    }

    /**
	 * Set the previous rate.
	 * @param tasaAnterior is the previous rate.
	*/
    public void setTasaAnterior(Double tasaAnterior) {
        this.tasaAnterior = tasaAnterior;
    }

    /**
     * Get the new rate.
     * @return the new rate.
    */
    public Double getTasaNueva() {
        return this.tasaNueva;
    }

    /**
	 * Set the new rate.
	 * @param tasaNueva is the new rate.
	*/
    public void setTasaNueva(Double tasaNueva) {
        this.tasaNueva = tasaNueva;
    }

    /**
     * Get the field that determines if a rate exists.
     * @return the field that determines if a rate exists.
    */
    public Double getTasaCambioPlazo() {
        return this.tasaCambioPlazo;
    }

    /**
	 * Set if there is a rate.
	 * @param tasaCambioPlazo is the field that determines if a rate exists.
	*/
    public void setTasaCambioPlazo(Double tasaCambioPlazo) {
        this.tasaCambioPlazo = tasaCambioPlazo;
    }

    /**
     * Get the previous monthly payment.
     * @return the previous monthly payment.
    */
    public Double getCuotaMensualAnterior() {
        return cuotaMensualAnterior;
    }

    /**
	 * Set the previous monthly payment.
	 * @param cuotaMensualAnterior is the previous monthly payment.
	*/
    public void setCuotaMensualAnterior(Double cuotaMensualAnterior) {
        this.cuotaMensualAnterior = cuotaMensualAnterior;
    }

    /**
     * Get the negociation object.
     * @return the negociation object.
    */
    public Negociacion getNegociacion() {
        return negociacion;
    }

    /**
	 * Set the negociation.
	 * @param negociacion is a negociation object.
	*/
    public void setNegociacion(Negociacion negociacion) {
        this.negociacion = negociacion;
    }

}