package com.colpatria.midas.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "HISTORIAL_CARTERA_LIGHT")
public class HistorialCarteraLight extends HistorialCartera {

    public HistorialCarteraLight(){}

    public HistorialCarteraLight(HistorialCartera historialCartera){
        this.setId(historialCartera.getId());
        this.setServicio(historialCartera.getServicio());
        this.setMotivoFinalizacion(historialCartera.getMotivoFinalizacion());
        this.setDiasAtraso(historialCartera.getDiasAtraso());
        this.setFechaPrimerVencimiento(historialCartera.getFechaPrimerVencimiento());
        this.setFechaSegundoVencimiento(historialCartera.getFechaSegundoVencimiento());
        this.setMoraPrimerVencimiento(historialCartera.getMoraPrimerVencimiento());
        this.setCapitalImpago(historialCartera.getCapitalImpago());
        this.setInteresImpago(historialCartera.getInteresImpago());
        this.setInteresOrdenCorrienteImpago(historialCartera.getInteresOrdenCorrienteImpago());
        this.setInteresMora(historialCartera.getInteresMora());
        this.setInteresOrdenMoraImpago(historialCartera.getInteresOrdenMoraImpago());
        this.setInteresCausadoNoFacturado(historialCartera.getInteresCausadoNoFacturado());
        this.setInteresCorrienteOrdenCausadoNoFacturado(historialCartera.getInteresCorrienteOrdenCausadoNoFacturado());
        this.setInteresMoraCausadoNoFacturado(historialCartera.getInteresMoraCausadoNoFacturado());
        this.setInteresMoraOrdenCausadoNoFacturado(historialCartera.getInteresMoraOrdenCausadoNoFacturado());
        this.setTotalInteresCorrienteFacturadoImpago(historialCartera.getTotalInteresCorrienteFacturadoImpago());
        this.setTotalInteresCorrienteCausado(historialCartera.getTotalInteresCorrienteCausado());
        this.setTotalInteresMoraFacturadoImpago(historialCartera.getTotalInteresMoraFacturadoImpago());
        this.setTotalInteresMoraCausado(historialCartera.getTotalInteresMoraCausado());
        this.setInteresFacturadoNoAfecto(historialCartera.getInteresFacturadoNoAfecto());
        this.setInteresNoAfectoPorFacturar(historialCartera.getInteresNoAfectoPorFacturar());
        this.setFechaReporte(historialCartera.getFechaReporte());
        this.setDescripcion(historialCartera.getDescripcion());
    }

}
