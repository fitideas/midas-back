package com.colpatria.midas.model;

import ch.qos.logback.core.net.server.Client;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.*;

@Entity
@Table(name = "HISTORIAL_PAGO")
public class HistorialPago implements Serializable {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

    @Column(name = "SERVICIO_NUMERO")
    private String servicio;

    @Column(name = "SUCURSAL_NUMERO")
    private Long sucursal;

    @Column(name = "NUMERO_DOCUMENTO_PAGADO")
    private String numeroDocumentoPagado;

    @Column(name = "FECHA_INGRESO")
    private LocalDate fechaIngreso;

    @Column(name = "FECHA_PAGO")
    private LocalDate fechaPago;

    @Column(name = "FECHA_VENCIMIENTO")
    private LocalDate fechaVencimiento;

    @Column(name = "CODIGO_CARGO_COLPATRIA")
    private String codigoCargoColpatria;

    @Column(name = "CODIGO_CARGO")
    private String codigoCargo;

    @Column(name = "DESCRIPCION_CARGO")
    private String descripcionCargo;

    @Column(name = "CUENTA_CONTABLE")
    private String cuentaContable;

    @Column(name = "MONTO")
    private Double monto;

    @Column(name = "MONTO_PARTICIPACION")
    private Double montoParticipacion;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "IDENTIFICACION_CLIENTE", referencedColumnName = "NUMERO_IDENTIFICACION")
    private Cliente cliente;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CODIGO_SUBPRODUCTO", referencedColumnName = "CODIGO")
    private Producto subProducto;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getCodigoCargo() {
        return codigoCargo;
    }

    public void setCodigoCargo(String codigoCargo) {
        this.codigoCargo = codigoCargo;
    }

    public String getCodigoCargoColpatria() {
        return codigoCargoColpatria;
    }

    public void setCodigoCargoColpatria(String codigoCargoColpatria) {
        this.codigoCargoColpatria = codigoCargoColpatria;
    }

    public String getDescripcionCargo() {
        return descripcionCargo;
    }

    public void setDescripcionCargo(String descripcionCargo) {
        this.descripcionCargo = descripcionCargo;
    }

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getNumeroDocumentoPagado() {
        return numeroDocumentoPagado;
    }

    public void setNumeroDocumentoPagado(String numeroDocumentoPagado) {
        this.numeroDocumentoPagado = numeroDocumentoPagado;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public Long getSucursal() {
        return sucursal;
    }

    public void setSucursal(Long sucursal) {
        this.sucursal = sucursal;
    }

    public Double getMontoParticipacion() {
        return montoParticipacion;
    }

    public void setMontoParticipacion(Double montoParticipacion) {
        this.montoParticipacion = montoParticipacion;
    }

    public String getCuentaContable() {
        return cuentaContable;
    }

    public void setCuentaContable(String cuentaContable) {
        this.cuentaContable = cuentaContable;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Producto getSubProducto() {
        return subProducto;
    }

    public void setSubProducto(Producto subProducto) {
        this.subProducto = subProducto;
    }
}
