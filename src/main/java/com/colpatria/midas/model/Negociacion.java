package com.colpatria.midas.model;

/*
 *
 * Librerías
 *
*/

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;

/*
 *
 * Clase
 *
*/

@Entity
@Table(name = "NEGOCIACION" )
public class Negociacion implements Serializable {

    /*
     *
     * Atríbutos
     *
    */

    @Id
    @Column( name = "ID" )
    private String id;

    @Column( name = "FECHA" )
    private LocalDate fecha;

    @Column( name = "ESTADO" )
    private String estado;

    /*
     *
     * Métodos
     *
    */

    public Negociacion() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
