package com.colpatria.midas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class define the objects and methods that make up the table EMAIL_SETTINGS
 * @author Esteban Peña
 * @version 1.0
 */
@Entity
@Table(name = "EMAIL_SETTINGS")
public class EmailSettings {

    /**
     * Recipient email
     */
    @Id
    @Column(name = "EMAIL")
    private String email;

    /**
     * Email getter
     * @return Recipient Email
     * @since 1.0
     */
    public String getEmail() { return this.email; }

    /**
     * Email setter
     * @param email Recipient Email
     * @since 1.0
     */
    public void setEmail(String email) { this.email = email; }
}