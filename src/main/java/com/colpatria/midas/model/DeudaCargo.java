package com.colpatria.midas.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.access.prepost.PreInvocationAuthorizationAdviceVoter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "DEUDA_CARGO")
public class DeudaCargo {

    /**
     * Integer that refers to the category account ID
     */
    @Getter
    @Id
    @Column(name = "ID_DEUDA_CARGO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDeudaCargo;

    /**
     * String that refers to the account number in the condonation
     */
    @Getter
    @Setter
    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn(name = "CUENTA_ID")
    private Cuenta cuenta;

    /**
     * Property that refers to sucursal
     */
    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SUCURSAL_ID", referencedColumnName = "NUMERO" )
    private Sucursal sucursal;

    /**
     * Property that refers to subproducto
     */
    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CODIGO_SUBPRODUCTO_ID", referencedColumnName = "CODIGO")
    private Producto subProducto;

    /**
     * Property that refers to servicio
     */
    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SERVICIO_ID", referencedColumnName = "NUMERO")
    private Servicio servicio;

    /**
     * Property that refers to cargo
     */
    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CARGO_ID", referencedColumnName = "ID")
    private Cargo cargo;

    /**
     * Property that set and get fecha documento
     */
    @Getter
    @Setter
    @Column(name = "FECHA_DOCUMENTO")
    private LocalDate fechaDocumento;

    /**
     * Property that set and get fecha primer vencimiento
     */
    @Getter
    @Setter
    @Column(name = "FECHA_PRIMER_VENCIMIENTO")
    private LocalDate fechaPrimerVencimiento;

    /**
     * Property that set and get fecha segundo vencimiento
     */
    @Getter
    @Setter
    @Column(name = "FECHA_SEGUNDO_VENCIMIENTO")
    private LocalDate fechaSegundoVencimiento;

    /**
     * Property that set and get fecha reporte
     */
    @Getter
    @Setter
    @Column(name = "FECHA_REPORTE")
    private LocalDate fechaReporte;

    /**
     * Property that set and get saldo
     */
    @Getter
    @Setter
    @Column(name = "SALDO")
    private double saldo;

    /**
     * Property that set and get antiguedad
     */
    @Getter
    @Setter
    @Column(name = "ANTIGUEDAD")
    private int antiguedad;

    /**
     * Property that set and get descripcion
     */
    @Setter
    @Getter
    @Column(name = "DESCRIPCION")
    private String descripcion;

    /**
     * Property that set and get NRO DOCUMENTO
     */
    @Getter
    @Setter
    @Column(name = "NRO_DOCUMENTO_FACTURACION")
    private String nroDocumentoFacturacion;
}
