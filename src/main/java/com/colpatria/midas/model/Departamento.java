package com.colpatria.midas.model;

/*
 *
 * Libraries
 *
*/

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

/**
 * Entity that represents the database model.
*/
@Entity
@Table(name = "DEPARTAMENTO")
public class Departamento {

    /**
     * String that specifies the state code.
    */
    @Id
    @Column(name = "CODIGO")
	private String codigo;

    /**
     * String that specifies the state code.
    */
    @Column(name = "NOMBRE")
	private String nombre;

    /**
	 * Compare both objects
	 * @param departamento is the new account object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( Departamento departamento ) {
		if( departamento == null || !this.codigo.equals( departamento.getCodigo() ) ) {
			return false;
		}
		return true;
	}

    /**
     * Get the state code.
     * @return the state code.
    */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Set the state code.
     * @param codigo is the state code.
    */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Get the state name.
     * @return the state name.
    */
    public String getNombre() {
        return nombre;
    }

    /**
     * Set the state name.
     * @param codigo is the state name.
    */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
