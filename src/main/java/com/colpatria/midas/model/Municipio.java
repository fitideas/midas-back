package com.colpatria.midas.model;

/*
 *
 * Libraries
 *
*/

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Column;

/**
 * Entity that represents the database model.
*/
@Entity
@Table(name = "MUNICIPIO")
public class Municipio {

    /**
     * String that specifies the state code.
    */
    @Id
    @Column(name = "CODIGO")
	private Long codigo;

    /**
     * String that specifies the state code.
    */
    @Column(name = "NOMBRE")
	private String nombre;

    /**
	 * Variable DEPARTAMENTO that specifies one to one relation between MUNICIPIO and DEPARTAMENTO
	 */
	@ManyToOne
	@JoinColumn(name = "DEPARTAMENTO_CODIGO", referencedColumnName = "CODIGO")
	private Departamento departamento;

    /**
	 * Compare both objects
	 * @param municipio is the new account object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( Municipio municipio ) {
		if( municipio == null || !this.codigo.equals( municipio.getCodigo() ) ) {
			return false;
		}
		return true;
	}

    /**
     * Get the municipality code.
     * @return the municipality code.
    */
    public Long getCodigo() {
        return codigo;
    }

    /**
     * Set the municipality code.
     * @param codigo is the municipality code.
    */
    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    /**
     * Get the municipality name.
     * @return the municipality name.
    */
    public String getNombre() {
        return nombre;
    }

    /**
     * Set the municipality name.
     * @param codigo is the municipality name.
    */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Get the state.
     * @return the state.
    */
    public Departamento getDepartamento() {
        return departamento;
    }

    /**
     * Set the state.
     * @param codigo is the state.
    */
    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }
    
}
