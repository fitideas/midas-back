package com.colpatria.midas.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;

@Entity
@Table(name="HISTORIAL_RECAUDO_RESUMEN")
public class HistorialRecaudoResumen implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CONTRATO_NUMERO", referencedColumnName = "NUMERO")
    private Contrato contrato;

    @Column(name = "FECHA_PAGO")
    private LocalDate fechaPago;

    @Column(name = "FECHA_PROCESO_PAGO")
    private LocalDate fechaProcesoPago;

    @Column(name="VALOR_PAGADO")
    private Double valorPagado;

    @Column(name="CAPITAL")
    private Double capital;

    @Column(name="INTERES_CORRIENTE")
    private Double interesCorriente;

    @Column(name="INTERES_MORA")
    private Double interesMora;

    @Column(name="CUOTA_MANEJO")
    private Double cuotaManejo;

    @Column(name="HONORARIO_COBRANZA")
    private Double honorarioCobranza;

    @Column(name="SEGURO_OBLIGATORIO")
    private Double seguroObligatorio;

    @Column(name="SEGURO_VOLUNTARIO")
    private Double seguroVoluntario;

    @Column(name="COMISIONES")
    private Double comisiones;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public LocalDate getFechaProcesoPago() {
        return fechaProcesoPago;
    }

    public void setFechaProcesoPago(LocalDate fechaProcesoPago) {
        this.fechaProcesoPago = fechaProcesoPago;
    }

    public Double getValorPagado() {
        return valorPagado;
    }

    public void setValorPagado(Double valorPagado) {
        this.valorPagado = valorPagado;
    }

    public Double getCapital() {
        return capital;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public Double getInteresCorriente() {
        return interesCorriente;
    }

    public void setInteresCorriente(Double interesCorriente) {
        this.interesCorriente = interesCorriente;
    }

    public Double getInteresMora() {
        return interesMora;
    }

    public void setInteresMora(Double interesMora) {
        this.interesMora = interesMora;
    }

    public Double getCuotaManejo() {
        return cuotaManejo;
    }

    public void setCuotaManejo(Double cuotaManejo) {
        this.cuotaManejo = cuotaManejo;
    }

    public Double getHonorarioCobranza() {
        return honorarioCobranza;
    }

    public void setHonorarioCobranza(Double honorarioCobranza) {
        this.honorarioCobranza = honorarioCobranza;
    }

    public Double getSeguroObligatorio() {
        return seguroObligatorio;
    }

    public void setSeguroObligatorio(Double seguroObligatorio) {
        this.seguroObligatorio = seguroObligatorio;
    }

    public Double getSeguroVoluntario() {
        return seguroVoluntario;
    }

    public void setSeguroVoluntario(Double seguroVoluntario) {
        this.seguroVoluntario = seguroVoluntario;
    }

    public Double getComisiones() {
        return comisiones;
    }

    public void setComisiones(Double comisiones) {
        this.comisiones = comisiones;
    }

}
