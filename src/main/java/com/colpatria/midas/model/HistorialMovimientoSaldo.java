package com.colpatria.midas.model;

/*
 *
 * Librerías
 *
*/

import java.time.LocalDate;
import javax.persistence.*;
import java.math.BigInteger;

/*
 *
 * Clase
 *
*/

@Entity
@Table( name="HISTORIAL_MOVIMIENTO_SALDO" )
public class HistorialMovimientoSaldo {

    /*
	 *
	 * Atríbutos
	 *
	*/

	/**
     * BigInteger that specifies the record ID.
    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
	private BigInteger id;

	/**
	 * Transaction number
	*/
	@ManyToOne
	@JoinColumn( name = "NEGOCIACION_NUMERO", referencedColumnName = "ID" )
    private Negociacion negociacion;

	/**
	 * Contract number
	*/
	@ManyToOne
	@JoinColumn( name = "CONTRATO_NUMERO", referencedColumnName = "NUMERO" )
	private Contrato contrato;

	/**
	 * Value
	*/
	@Column( name = "VALOR" )
    private Double valor;

	/**
	 * Movement type
	*/
	@Column( name = "TIPO_MOVIMIENTO" )
    private String tipoMovimiento;

	/**
	 * Position
	*/
	@ManyToOne
	@JoinColumn( name = "CARGO_ID", referencedColumnName = "ID" )
    private Cargo cargo;

	/**
	 * Purchase number
	*/
	@Column( name = "NUMERO_COMPRA" )
    private String numeroCompra;

	/**
	 * Adjustment date
	*/
	@Column( name = "FECHA_REALIZACION_AJUSTE" )
    private LocalDate fechaRealizacionAjuste;

	/**
	 * Adjustment approval date
	*/
	@Column( name = "FECHA_APROBACION_AJUSTE" )
    private LocalDate fechaAprobacionAjuste;

	/**
	 * Creator user
	*/
	@ManyToOne
	@JoinColumn( name = "USUARIO_CREADOR_ID", referencedColumnName = "ID" )
	private Usuario usuarioCreador;

	/**
	 * Approving user
	*/
	@ManyToOne
	@JoinColumn( name = "USUARIO_APROBADOR_ID", referencedColumnName = "ID" )
	private Usuario usuarioAprobador;

	/**
	 * Observation
	*/
	@Column( name = "OBSERVACION" )
    private String observacion;

	/**
	 * Estado that refers to the Service status
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ESTADO_ID", referencedColumnName = "ID")
	private Estado estado;

	/*
     *
     * Métodos
     *
    */

	/**
     * This is the class contructutor
    */
	public HistorialMovimientoSaldo() {
	}

	/**
	 * Get the contract
	 * @return the contract
	*/
	public Contrato getContrato() {
		return contrato;
	}

	/**
	 * Set the contract
	 * @param contrato is the movement contract
	*/
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	/**
	 * Get the value
	 * @return the value
	*/
	public Double getValor() {
		return valor;
	}

	/**
	 * Set the value
	 * @param valor is the value
	*/
	public void setValor(Double valor) {
		this.valor = valor;
	}

	/**
     * Get the movement type.
     * @return the movement type.
    */
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	/**
     * Set the movement type.
     * @param tipoMovimiento is the movement type.
    */
	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	/**
     * Get the purchase number.
     * @return the purchase number.
    */
	public String getNumeroCompra() {
		return numeroCompra;
	}

	/**
     * Set the purchase number.
     * @param numeroCompra is the purchase number.
    */
	public void setNumeroCompra(String numeroCompra) {
		this.numeroCompra = numeroCompra;
	}

	/**
     * Get the adjustment approval date.
     * @return the adjustment approval date.
    */
	public LocalDate getFechaAprobacionAjuste() {
		return fechaAprobacionAjuste;
	}

	/**
     * Set the adjustment approval date.
     * @param fechaAprobacionAjuste is the adjustment approval date.
    */
	public void setFechaAprobacionAjuste(LocalDate fechaAprobacionAjuste) {
		this.fechaAprobacionAjuste = fechaAprobacionAjuste;
	}

	/**
     * Get the creator user.
     * @return the creator user.
    */
	public Usuario getUsuarioCreador() {
		return usuarioCreador;
	}

	/**
     * Set the creator user.
     * @param usuarioCreador is the creator user.
    */
	public void setUsuarioCreador(Usuario usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}

	/**
     * Get the approving user.
     * @return the approving user.
    */
	public Usuario getUsuarioAprobador() {
		return usuarioAprobador;
	}

	/**
     * Set the approving user.
     * @param usuarioAprobador is the approving user.
    */
	public void setUsuarioAprobador(Usuario usuarioAprobador) {
		this.usuarioAprobador = usuarioAprobador;
	}

	/**
     * Get the observation.
     * @return the movement observation.
    */
	public String getObservacion() {
		return observacion;
	}

	/**
     * Set the observation.
     * @param observacion is the movement observation.
    */
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	/**
     * Get the adjustment date.
     * @return the adjustment date.
    */
	public LocalDate getFechaRealizacionAjuste() {
		return fechaRealizacionAjuste;
	}

	/**
     * Set the adjustment date.
     * @param fechaRealizacionAjuste is the adjustment date.
    */
	public void setFechaRealizacionAjuste(LocalDate fechaRealizacionAjuste) {
		this.fechaRealizacionAjuste = fechaRealizacionAjuste;
	}

	/**
     * Get status.
     * @return the movement status.
    */
	public Estado getEstado() {
		return estado;
	}

    /**
     * Set status.
     * @param estado is the movement status.
    */
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	/**
     * Get the position.
     * @return Cargo.
    */
	public Cargo getCargo() {
		return cargo;
	}

	/**
     * Set the position.
     * @param cargo is the movement position.
    */
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	/**
     * Get the history id.
     * @return the history id.
    */
	public BigInteger getId() {
		return id;
	}

	/**
     * Set the history id.
     * @param id is the history id.
    */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
     * Get the transaction.
     * @return the transaction.
    */
	public Negociacion getNegociacion() {
		return negociacion;
	}

	/**
     * Set the transaction.
     * @param negociacion is the transaction.
    */
	public void setNegociacion(Negociacion negociacion) {
		this.negociacion = negociacion;
	}
	    
}