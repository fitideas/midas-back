package com.colpatria.midas.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Tipo Reclamo Entity
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "TIPO_RECLAMO")
public class TipoReclamo {
    /**
     * reclamation type code
     */
    @Id
    @Column(name = "ID", nullable = false, length = 1)
    private String id;
    /**
     * Reclamation type name
     */
    @Column(name = "DESCRIPCION", nullable = false)
    private String descripcion;
    /**
     * Priority of the reclamation type
     */
    @Column(name = "PRIORIDAD", nullable = false)
    private Integer prioridad;

}