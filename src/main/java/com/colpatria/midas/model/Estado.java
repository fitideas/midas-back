package com.colpatria.midas.model;

/*
 *
 * Libraries
 *
*/

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

/**
 * Entity that represents the database model.
*/
@Entity
@Table(name = "ESTADO")
public class Estado {

    /**
     * String that specifies the service state code.
    */
    @Id
    @Column(name = "ID")
	private String id;

    /**
     * String that specifies the service state name.
    */
    @Column(name = "NOMBRE")
	private String nombre;

    /**
     * Get the state service code.
     * @return the state service code.
    */
    public String getId() {
        return id;
    }

    /**
     * Set the state service code.
     * @param id is the state service code.
    */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get the state service name.
     * @return the state service name.
    */
    public String getNombre() {
        return nombre;
    }

    /**
     * Set the state service name.
     * @param codigo is the state service name.
    */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
