package com.colpatria.midas.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDate;

/**
 * Info finalizacion entity
 */
@Entity
@Table(name="INFO_FINALIZACION")
public class InfoFinalizacion {

    /**
     * BigInteger that specifies the record ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private BigInteger id;

    /**
     * Servicio that specifies the relation between SERVICE and Info finalizacion.
     */
    @ManyToOne
    @JoinColumn(name="NRO_SERVICIO", referencedColumnName = "NUMERO")
    @Getter
    @Setter
    private Servicio servicio;

    /**
     * Servicio that specifies the relation between USUARIO and Info finalizacion.
     */
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="USUARIO", referencedColumnName = "ID")
    @Getter
    @Setter
    private Usuario usuario;

    /**
     * String that specifies 'Estado anterior'.
     */
    @Column(name="ESTADO_ANTERIOR")
    @Getter
    @Setter
    private String estadoAnterior;

    /**
     * String that specifies 'Estado Nuevo'.
     */
    @Column(name="ESTADO_NUEVO")
    @Getter
    @Setter
    private String estadoNuevo;

    /**
     * String that specifies 'Estado Nuevo'.
     */
    @Column(name="OBSERVACIONES")
    @Getter
    @Setter
    private String observaciones;

    /**
     * Double that specifies the cargo concepto.
     */
    @Getter
    @Setter
    @Column(name="CARGO_CONCEPTO")
    private Double cargoConcepto;

    /**
     * Double that specifies the cargo capital.
     */
    @Getter
    @Setter
    @Column(name="CARGO_CAPITAL")
    private Double cargoCapital;

    /**
     * Double that specifies the cargo capital.
     */
    @Getter
    @Setter
    @Column(name="FECHA_CAMBIO")
    private LocalDate fechaCambio;

    /**
     * Double that specifies the cargo interes corriente.
     */
    @Getter
    @Setter
    @Column(name="CARGO_INTERES_CORRIENTE")
    private Double cargoInteresCorriente;

    /**
     * Double that specifies the cargo interes mora.
     */
    @Getter
    @Setter
    @Column(name="CARGO_INTERES_MORA")
    private Double cargoInteresMora;

    /**
     * Double that specifies the cargo interes mora.
     */
    @Getter
    @Setter
    @Column(name="DEUDA_PENDIENTE_FACTURAR")
    private Double deudaPendienteFacturar;
}
