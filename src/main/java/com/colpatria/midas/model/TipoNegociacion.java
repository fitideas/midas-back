package com.colpatria.midas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "TIPO_NEGOCIACION")
@Entity
public class TipoNegociacion {

    /**
     * Integer that refers to the identifier of the  TipoNegociacion entity
     */
    @Id
    @Column(name = "ID_TIPO_NEGOCIACION", nullable = false)
    private Integer id;

    /**
     * String that refers to the description in the TipoNegociacion entity
     */
    @Column(name = "DESCRIPCION_TIPO_NEGOCIACION")
    private String descripcionTipoNegociacion;

    /**
     * Gets the String that refers to the description in the TipoNegociacion entity
     * @return a String that refers to the description in the TipoNegociacion entity
     */
    public String getDescripcionTipoNegociacion() {
        return descripcionTipoNegociacion;
    }

    /**
     * Sets the String that refers to the description in the TipoNegociacion entity
     * @param descripcionTipoNegociacion a String that refers to the description in the TipoNegociacion entity
     */
    public void setDescripcionTipoNegociacion(String descripcionTipoNegociacion) {
        this.descripcionTipoNegociacion = descripcionTipoNegociacion;
    }

    /**
     * Gets an Integer that refers to the identifier in the TipoNegociacion Entity
     * @return an Integer that refers to the identifier in the TipoNegociacion Entity
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets an Integer that refers to the identifier in the TipoNegociacion Entity
     * @param id an Integer that refers to the identifier in the TipoNegociacion Entity
     */
    public void setId(Integer id) {
        this.id = id;
    }
}