package com.colpatria.midas.model;

/*
 *
 * Libraries
 *
*/

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Column;

/**
 * Entity that represents the database model.
*/
@Entity
@Table(name = "LOCALIDAD")
public class Localidad {

    /**
     * String that specifies the location code.
    */
    @Id
    @Column(name = "CODIGO")
	private String codigo;

    /**
     * String that specifies the location code.
    */
    @Column(name = "NOMBRE")
	private String nombre;

    /**
	 * Variable MUNICIPIO that specifies one to one relation between LOCALIDAD and MUNICIPIO
	 */
	@ManyToOne
	@JoinColumn(name = "MUNICIPIO_CODIGO", referencedColumnName = "CODIGO")
	private Municipio municipio;

    /**
	 * Compare both objects
	 * @param localidad is the new account object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( Localidad localidad ) {
		if( localidad == null || !this.codigo.equals( localidad.getCodigo() ) ) {
			return false;
		}
		return true;
	}

    /**
     * Get the location code.
     * @return the location code.
    */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Set the location code.
     * @param codigo is the location code.
    */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Get the location name.
     * @return the location name.
    */
    public String getNombre() {
        return nombre;
    }

    /**
     * Set the location name.
     * @param codigo is the location name.
    */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Get the municipality.
     * @return the municipality.
    */
    public Municipio getMunicipio() {
        return municipio;
    }

    /**
     * Set the municipality.
     * @param codigo is the municipality.
    */
    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }
    
}
