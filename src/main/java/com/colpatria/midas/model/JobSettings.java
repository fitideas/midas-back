package com.colpatria.midas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model of the JOB_SETTING table on the database where the methods for each Job schedule are registered
 */
@Entity
@Table(name = "JOB_SETTINGS")
public class JobSettings {

    /**
     * String that refers to the job name
     */
    @Id
    @Column(name = "JOB_NAME")
    private String jobName;

    /**
     * String that refers job cron expression
     */
    @Column(name = "CRON")
    private String cron;

    /**
     * String that refers to the file name regex of a file to load in the job
     */
    @Column(name = "FILE_NAME_REGEX")
    private String fileNameRegex;



    /**
     * Get the job name of a class
     * @return String that refers to the job name
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * Set the job name of a class
     * @param jobName String that refers to the job name
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    /**
     * Get the cron of a class
     * @return String that refers to the hour when the job is executed
     */
    public String getCron() {
        return cron;
    }

    /**
     * Set the cron of a class
     * @param cron String that refers to the hour when the job is executed
     */
    public void setCron(String cron) {
        this.cron = cron;
    }


    /**
     * Get the Job File Name Regex
     * @return String that refers to file name regex of a file to load in the job
     */
    public String getFileNameRegex() {
        return fileNameRegex;
    }

    /**
     * Set the Job File Name Regex
     * @param fileNameRegex String that refers to the file name regex of a file to load in the job
     */
    public void setFileNameRegex(String fileNameRegex) {
        this.fileNameRegex = fileNameRegex;
    }
}
