package com.colpatria.midas.model;

import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Model of the HISTORIAL_PERIODO_GRACIA table on the database where the methods and objects are declared
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "HISTORIAL_PERIODO_GRACIA")
public class HistorialPeriodoGracia {

    /**
     * Big integer that refers to the ID of a record
     */
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    /**
     * Service variable that refers to the many-to-one relation between SERVICE and HISTORIAL_PERIODO_GRACIA
     */
    @ManyToOne
	@JoinColumn(name="SERVICIO_NUMERO", referencedColumnName = "NUMERO")
    private Servicio servicio;

    /**
     * Usuario variable that refers to the many-to-one relation between USUARIO and HISTORIAL_PERIODO_GRACIA
     */
    @ManyToOne
	@JoinColumn(name="USUARIO_ID", referencedColumnName = "ID")
    private Usuario usuario;

    /**
     * Negociacion that refers to the entity Negociacion associated with the HISTORIAL_PERIODO_GRACIA
     */
    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "NEGOCIACION_ID")
    private Negociacion negociacion;

    /**
     * LocalDate that specifies the application date.
    */
	@Column(name="FECHA_APLICACION")
	private LocalDate fechaAplicacion;

    /**
     * LocalDate that specifies the 'inicio gracia' date.
    */
	@Column(name="FECHA_INICIO_GRACIA")
	private LocalDate fechaInicioGracia;

    /**
     * LocalDate that specifies the 'fin gracia' date.
    */
	@Column(name="FECHA_FIN_GRACIA")
	private LocalDate fechaFinGracia;

    /**
     * LocalDate that specifies the 'desiste gracia' date.
    */
	@Column(name="FECHA_DESISTE_GRACIA")
	private LocalDate fechaDesistegracia;

    /**
     * Integer that refers to the amount of previous installments.
     */
    @Column(name="CANTIDAD_CUOTAS_ANTERIORES")
    private Integer cantidadCuotasAnteriores;

    /**
     * Integer that refers to the amount 'dias mora'.
     */
    @Column(name="DIAS_MORA")
    private Integer diasMora;

    /**
     * String that specifies the description.
     */
    @Column(name="DESCRIPCION")
    private String descripcion;

    /**
     * Double that specifies the 'valor facturado'.
     */
    @Column(name="VALOR_FACTURADO")
    private Double valorFacturado;
    
}
