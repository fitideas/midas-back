package com.colpatria.midas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.time.LocalDate;

/**
 * Model of SERVICO table in the database where the objects and methods are declared
 */
@Entity
@Table(name = "SERVICIO")
public class Servicio implements Serializable {

	/**
	 * String that refers to Service number
	 */
	@Id
	@Column(name = "NUMERO")
	private String numero;

	/**
	 * Estado that refers to the Service status
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ESTADO_ID", referencedColumnName = "ID")
	private Estado estado;

	/**
	 * String that refers to the debt balance
	 */
	@Column(name = "SALDO_DEUDA")
	private Double saldoDeuda;

	/**
	 * String that refers to the service class
	*/
	@Column(name = "CLASE")
	private String clase;

	/**
	 * Double that refers to the capital balance to be billed
	 */
	@Column(name = "SALDO_CAPITAL_POR_FACTURAR")
	private Double saldoCapitalPorFacturar;

	/**
	 * Integer that refers to numbers of fees paid
	 */
	@Column(name = "CUOTAS_PACTADAS")
	private Integer cuotasPactadas;

	/**
	 * Integer that refers to the numbers of fees to be billed
	 */
	@Column(name = "CUOTAS_POR_FACTURAR")
	private Integer cuotasPorFacturar;

	/**
	 * Double that refers to the fee value
	 */
	@Column(name = "VALOR_CUOTA")
	private Double valorCuota;

	/**
	 * Double that refers to the purchase value
	 */
	@Column(name = "VALOR_COMPRA")
	private Double valorCompra;

	/**
	 * LocalDate that refers to the creation date
	 */
	@Column(name = "FECHA_CREACION")
	private LocalDate fechaCreacion;

	/**
	 * Double that refers to the interest rate
	 */
	@Column(name = "TASA")
	private Double tasa;

	/**
	 * LocalDate that refers to the purchase date
	 */
	@Column(name = "FECHA_COMPRA")
	private LocalDate fechaCompra;

	/**
	 * Variable that refers to the many-to-one relation between SERVICIO and TIPO_CONSUMO
	 */
	@Column(name = "TIPO_CONSUMO")
	private Integer tipoConsumo;

	/**
	 * String that refers to the bussness partner
	 */
	@Column(name="SOCIO_DE_NEGOCIO")
	private String socioDeNegocio;

	/**
	 * Variable that refers to the many-to-one relation between SERVICIO and CONTRATO
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CONTRATO_NUMERO", referencedColumnName = "NUMERO")
	private Contrato contrato;

	/**
	 * Vatiable that refers to the many-to-one relation between SERVICIO and TIPO_TARJETA
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "TIPO_TARJETA_ID", referencedColumnName = "ID")
	private TipoTarjeta tarjeta;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Double getSaldoDeuda() {
		return saldoDeuda;
	}

	public void setSaldoDeuda(Double saldoDeuda) {
		this.saldoDeuda = saldoDeuda;
	}

	public Double getSaldoCapitalPorFacturar() {
		return saldoCapitalPorFacturar;
	}

	public void setSaldoCapitalPorFacturar(Double saldoCapitalPorFacturar) {
		this.saldoCapitalPorFacturar = saldoCapitalPorFacturar;
	}

	public Integer getCuotasPactadas() {
		return cuotasPactadas;
	}

	public void setCuotasPactadas(Integer cuotasPactadas) {
		this.cuotasPactadas = cuotasPactadas;
	}

	public Integer getCuotasPorFacturar() {
		return cuotasPorFacturar;
	}

	public void setCuotasPorFacturar(Integer cuotasPorFacturar) {
		this.cuotasPorFacturar = cuotasPorFacturar;
	}

	public Double getValorCuota() {
		return valorCuota;
	}

	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	public Double getValorCompra() {
		return valorCompra;
	}

	public void setValorCompra(Double valorCompra) {
		this.valorCompra = valorCompra;
	}

	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Double getTasa() {
		return tasa;
	}

	public void setTasa(Double tasa) {
		this.tasa = tasa;
	}

	public LocalDate getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(LocalDate fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public String getSocioDeNegocio() {
		return socioDeNegocio;
	}  

	public void setSocioDeNegocio(String socioDeNegocio) {
		this.socioDeNegocio = socioDeNegocio;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public TipoTarjeta getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(TipoTarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getClaseServicio() {
		return clase;
	}

	public void setClaseServicio(String claseServicio) {
		this.clase = claseServicio;
	}

	public Integer getTipoConsumo() {
		return tipoConsumo;
	}

	public void setTipoConsumo(Integer tipoConsumo) {
		this.tipoConsumo = tipoConsumo;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

}