package com.colpatria.midas.model;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDate;

/*
 * Model class for 'historial facturacion concepto resumen resumen'
*/
@Entity
@Table( name = "HISTORIAL_FACTURACION_CONCEPTO_RESUMEN" )
public class HistorialFacturacionConceptoResumen {
    /**
     * BigInteger that specifies the record ID.
    */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	/**
     * 'servicio' associated with 'historial facturacion concepto resumen'
     */
    @ManyToOne
	@JoinColumn( name = "CONTRATO_NUMERO", referencedColumnName = "NUMERO" )
	private Contrato contrato;

	/**
     * LocalDate that specifies the document date.
    */
	@Column(name="FECHA_DOCUMENTO")
	private LocalDate fechaDocumento;

	/**
     * LocalDate that specifies the process date.
    */
	@Column(name="FECHA_PROCESO")
	private LocalDate fechaProceso;

    /**
     * 'totalDocumento' associated with 'historial facturacion concepto resumen'
     */
    @Column( name = "SUMA_TOTAL_DOCUMENTO" )
	private Double sumaTotalDocumento;

    /**
     * 'capital' associated with 'historial facturacion concepto resumen'
     */
    @Column( name = "CAPITAL" )
	private Double capital;

    /**
     * 'interesCorriente' associated with 'historial facturacion concepto resumen'
     */
    @Column( name = "INTERES_CORRIENTE" )
	private Double interesCorriente;

    /**
     * 'interesMora' associated with 'historial facturacion concepto resumen'
     */
    @Column( name = "INTERES_MORA" )
	private Double interesMora;

    /**
     * 'cuotaManejo' associated with 'historial facturacion concepto resumen'
     */
    @Column( name = "CUOTA_MANEJO" )
	private Double cuotaManejo;

    /**
     * 'honorarioCobranza' associated with 'historial facturacion concepto resumen'
     */
    @Column( name = "HONORARIO_COBRANZA" )
	private Double honorarioCobranza;

    /**
     * 'seguroObligatorio' associated with 'historial facturacion concepto resumen'
     */
    @Column( name = "SEGURO_OBLIGATORIO" )
	private Double seguroObligatorio;

    /**
     * 'seguroVoluntario' associated with 'historial facturacion concepto resumen'
     */
    @Column( name = "SEGURO_VOLUNTARIO" )
	private Double seguroVoluntario;

    /**
     * 'comisiones' associated with 'historial facturacion concepto resumen'
     */
    @Column( name = "COMISIONES" )
	private Double comisiones;

    /**
     * 'nuevosCobros' associated with 'historial facturacion concepto resumen'
     */
    @Column( name = "NUEVOS_COBROS" )
	private Double nuevosCobros;

    /**
     * Get the ID of a class
     * @return Big integer that refers to the ID of a record
     */
    public BigInteger getId() {
        return this.id;
    }

    /**
     * Set the ID of a class
     * @param id Big integer that refers to the ID of a record
     */
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
     * Get the CONTRATO for the HISTORIAL_FACTURACION_CONCEPTO_RESUMEN
     * @return Variable thar refers to the many-to-one relation between CONTRATO and HISTORIAL_FACTURACION_CONCEPTO_RESUMEN
     */
    public Contrato getContrato() {
        return this.contrato;
    }

    /**
     * Set the CONTRATO for the HISTORIAL_FACTURACION_CONCEPTO_RESUMEN
     * @param contrato Variable thar refers to the many-to-one relation between CONTRATO and HISTORIAL_FACTURACION_CONCEPTO_RESUMEN
     */
    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    /**
     * Get the document date of 'historial facturacion concepto resumen'
     * @return Variable that contains the document date
     */
    public LocalDate getFechaDocumento() {
        return this.fechaDocumento;
    }

    /**
     * Set the document date of 'historial facturacion concepto resumen'
     * @param fechaDocumento Variable that contains the document date
     */
    public void setFechaDocumento(LocalDate fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    /**
     * Get the process date of 'historial facturacion concepto resumen'
     * @return Variable that contains the process date
     */
    public LocalDate getFechaProceso() {
        return this.fechaProceso;
    }

    /**
     * Set the process date of 'historial facturacion concepto resumen'
     * @param fechaProceso Variable that contains the process date
     */
    public void setFechaProceso(LocalDate fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    /**
     * Get the document total of 'historial facturacion concepto resumen'
     * @return Double that refers to the document total of a record
     */
    public Double getSumaTotalDocumento() {
        return this.sumaTotalDocumento;
    }

    /**
     * Set the document total of 'historial facturacion concepto resumen'
     * @param sumaTotalDocumento Double that refers to the document total of a record
     */
    public void setSumaTotalDocumento(Double sumaTotalDocumento) {
        this.sumaTotalDocumento = sumaTotalDocumento;
    }

    /**
     * Get the 'capital' of 'historial facturacion concepto resumen'
     * @return Double that refers to the 'capital' of a record
     */
    public Double getCapital() {
        return this.capital;
    }

    /**
     * Set the 'capital' of 'historial facturacion concepto resumen'
     * @param capital Double that refers to the 'capital' of a record
     */
    public void setCapital(Double capital) {
        this.capital = capital;
    }

    /**
     * Get the 'interesCorriente' of 'historial facturacion concepto resumen'
     * @return Double that refers to the 'interesCorriente' of a record
     */
    public Double getInteresCorriente() {
        return this.interesCorriente;
    }

    /**
     * Set the 'interesCorriente' of 'historial facturacion concepto resumen'
     * @param interesCorriente Double that refers to the 'interesCorriente' of a record
     */
    public void setInteresCorriente(Double interesCorriente) {
        this.interesCorriente = interesCorriente;
    }

    /**
     * Get the 'interesMora' of 'historial facturacion concepto resumen'
     * @return Double that refers to the 'interesMora' of a record
     */
    public Double getInteresMora() {
        return this.interesMora;
    }

    /**
     * Set the 'interesMora' of 'historial facturacion concepto resumen'
     * @param interesMora Double that refers to the'interesMora' of a record
     */
    public void setInteresMora(Double interesMora) {
        this.interesMora = interesMora;
    }

    /**
     * Get the 'cuotaManejo' of 'historial facturacion concepto resumen'
     * @return Double that refers to the 'cuotaManejo' of a record
     */
    public Double getCuotaManejo() {
        return this.cuotaManejo;
    }

    /**
     * Set the 'cuotaManejo' of 'historial facturacion concepto resumen'
     * @param cuotaManejo Double that refers to the 'cuotaManejo' of a record
     */
    public void setCuotaManejo(Double cuotaManejo) {
        this.cuotaManejo = cuotaManejo;
    }

    /**
     * Get the 'honorarioCobranza' of 'historial facturacion concepto resumen'
     * @return Double that refers to the 'honorarioCobranza' of a record
     */
    public Double getHonorarioCobranza() {
        return this.honorarioCobranza;
    }

    /**
     * Set the 'honorarioCobranza' of 'historial facturacion concepto resumen'
     * @param honorarioCobranza Double that refers to the 'honorarioCobranza' of a record
     */
    public void setHonorarioCobranza(Double honorarioCobranza) {
        this.honorarioCobranza = honorarioCobranza;
    }

    /**
     * Get the 'seguroObligatorio' of 'historial facturacion concepto resumen'
     * @return Double that refers to the 'seguroObligatorio' of a record
     */
    public Double getSeguroObligatorio() {
        return this.seguroObligatorio;
    }

    /**
     * Set the 'seguroObligatorio' of 'historial facturacion concepto resumen'
     * @param seguroObligatorio Double that refers to the 'seguroObligatorio' of a record
     */
    public void setSeguroObligatorio(Double seguroObligatorio) {
        this.seguroObligatorio = seguroObligatorio;
    }

    /**
     * Get the 'seguroVoluntario' of 'historial facturacion concepto resumen'
     * @return Double that refers to the 'seguroVoluntario' of a record
     */
    public Double getSeguroVoluntario() {
        return this.seguroVoluntario;
    }

    /**
     * Set the 'seguroVoluntario' of 'historial facturacion concepto resumen'
     * @param seguroVoluntario Double that refers to the 'seguroVoluntario' of a record
     */
    public void setSeguroVoluntario(Double seguroVoluntario) {
        this.seguroVoluntario = seguroVoluntario;
    }

    /**
     * Get the 'comisiones' of 'historial facturacion concepto resumen'
     * @return Double that refers to the 'comisiones' of a record
     */
    public Double getComisiones() {
        return this.comisiones;
    }

    /**
     * Set the 'comisiones' of 'historial facturacion concepto resumen'
     * @param comisiones Double that refers to the 'comisiones' of a record
     */
    public void setComisiones(Double comisiones) {
        this.comisiones = comisiones;
    }

    /**
     * Get the 'nuevosCobros' of 'historial facturacion concepto resumen'
     * @return Double that refers to the 'nuevosCobros' of a record
     */
    public Double getNuevosCobros() {
        return this.nuevosCobros;
    }

    /**
     * Set the 'nuevosCobros' of 'historial facturacion concepto resumen'
     * @param nuevosCobros Double that refers to the 'nuevosCobros' of a record
     */
    public void setNuevosCobros(Double nuevosCobros) {
        this.nuevosCobros = nuevosCobros;
    }


}
