package com.colpatria.midas.model;

import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="GASTOS_COBRANZA")
public class GastoCobranza {

    /**
     * BigInteger that specifies the record ID.
    */
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @OneToOne
	@JoinColumn(name="CARTERA_RESUMEN_ID", referencedColumnName = "ID")
    private HistorialCarteraResumen historialCarteraResumen;

    @OneToOne
	@JoinColumn(name="CARTA_MOROSA_ID", referencedColumnName = "ID")
    private HistorialCartaMorosa historialCartaMorosa;

    @Column(name="CAUSAL")
    private String causal;

    @Column(name="FECHA_ANTERIOR")
	private LocalDate fechaAnterior;
    
}
