package com.colpatria.midas.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 * The type Gestion cobro action response id.
 * for the composite key of the table
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Embeddable
public class GestionCobroActionResponseId implements Serializable {
    /**
     * The constant serialVersionUID.
     */
    private static final long serialVersionUID = 4082881157563197390L;
    /**
     * The Action id.
     */
    @Column(name = "ACTION_ID", nullable = false, length = 4)
    private String actionId;

    /**
     * The Response id.
     */
    @Column(name = "RESPONSE_ID", nullable = false, length = 4)
    private String responseId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        GestionCobroActionResponseId entity = (GestionCobroActionResponseId) o;
        return Objects.equals(this.actionId, entity.actionId) &&
                Objects.equals(this.responseId, entity.responseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actionId, responseId);
    }

}