package com.colpatria.midas.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * The type Gestion cobro action response.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "GESTION_COBRO_ACTION_RESPONSE")
public class GestionCobroActionResponse {
    /**
     * The ID
     */
    @EmbeddedId
    private GestionCobroActionResponseId id;

    /**
     * The Action
     */
    @MapsId("actionId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ACTION_ID", nullable = false)
    private GestionCobroAction action;

    /**
     * The Response.
     */
    @MapsId("responseId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "RESPONSE_ID", nullable = false)
    private GestionCobroResponse response;

    /**
     * flags if it is valid for Gastos cobranza.
     */
    @Column(name = "GASTOS_COBRANZA", nullable = false)
    private Boolean gastosCobranza = false;

    /**
     * flags if it is valid for Honorarios casa cobranza.
     */
    @Column(name = "HONORARIOS_CASA_COBRANZA", nullable = false)
    private Boolean honorariosCasaCobranza = false;

    /**
     * flags if it is valid for Visitas casa cobranza.
     */
    @Column(name = "VISITAS_CASA_COBRANZA", nullable = false)
    private Boolean visitasCasaCobranza = false;

}