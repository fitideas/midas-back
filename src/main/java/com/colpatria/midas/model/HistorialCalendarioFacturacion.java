package com.colpatria.midas.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="HISTORIAL_CALENDARIO_FACTURACION")
public class HistorialCalendarioFacturacion {
    /**
     * Represents the autogenerated primary key
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    /**
     * Version of the Schedule
     */
    private String version;
    /**
     * date of Schedule upload
     */
    private LocalDateTime fecha;
    /**
     * user who uploaded the Schedule
     */
    @OneToOne
    @JoinColumn(name = "usuario")
    private Usuario usuario;

}
