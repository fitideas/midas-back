package com.colpatria.midas.model;
/*
 *
 * Librerías
 *
*/

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDate;

/*
 *
 * Clase
 *
*/

@Entity
@Table( name = "HISTORIAL_CARGUE_UTILIZACION" )
public class HistorialCargueUtilizacion {

    /*
     *
     * Atríbutos
     *
    */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @ManyToOne
	@JoinColumn( name = "SERVICIO_NUMERO", referencedColumnName = "NUMERO" )
	private Servicio servicio;

    @Column( name = "CIUDAD_ORIGEN" )
	private String ciudadOrigen;

    @Column( name = "FECHA_POSTEO" )
	private LocalDate fechaPosteo;

    @Column( name = "FECHA_EFECTIVA" )
	private LocalDate fechaEfectiva;

    @Column( name = "VALOR" )
	private Double valor;

    @Column( name = "VALOR_DESCUENTO" )
	private Double valorDescuento;

    @Column( name = "VALOR_FINANCIADO" )
	private Double valorFinanciado;

    @Column( name = "TASA" )
	private Double tasa;
    
    @Column( name = "NUMERO_AUTORIZACION" )
	private String numeroAutorizacion;

    @ManyToOne
    @JoinColumn(name = "CLIENTE_NUMERO_IDENTIFICACION", referencedColumnName = "NUMERO_IDENTIFICACION")
    private Cliente cliente;

    @ManyToOne
    @JoinColumn(name = "SUBPRODUCTO_CODIGO", referencedColumnName = "CODIGO")
    private Producto subProducto;

    /*
     *
     * Métodos
     *
    */

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public String getCiudadOrigen() {
        return ciudadOrigen;
    }

    public void setCiudadOrigen(String ciudadOrigen) {
        this.ciudadOrigen = ciudadOrigen;
    }

    public LocalDate getFechaPosteo() {
        return fechaPosteo;
    }

    public void setFechaPosteo(LocalDate fechaPosteo) {
        this.fechaPosteo = fechaPosteo;
    }

    public LocalDate getFechaEfectiva() {
        return fechaEfectiva;
    }

    public void setFechaEfectiva(LocalDate fechaEfectiva) {
        this.fechaEfectiva = fechaEfectiva;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getValorDescuento() {
        return valorDescuento;
    }

    public void setValorDescuento(Double valorDescuento) {
        this.valorDescuento = valorDescuento;
    }

    public Double getValorFinanciado() {
        return valorFinanciado;
    }

    public void setValorFinanciado(Double valorFinanciado) {
        this.valorFinanciado = valorFinanciado;
    }

    public Double getTasa() {
        return tasa;
    }

    public void setTasa(Double tasa) {
        this.tasa = tasa;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public Producto getSubProducto() {
        return this.subProducto;
    }

    public void setSubProducto(Producto subProducto) {
        this.subProducto = subProducto;
    }


    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

}
