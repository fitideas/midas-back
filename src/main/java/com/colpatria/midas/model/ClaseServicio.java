package com.colpatria.midas.model;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model of CLASE_SERVICIO table in the database where the objects and methods are declared
 */
@Entity
@Table(name = "CLASE_SERVICIO")
public class ClaseServicio implements Serializable {

    /**
     * String that refers to the service class ID
     */
    @Id
    @Column(name = "ID")
    private String id;

    /**
     * String that refers to the service class name
     */
    @Column(name = "NOMBRE")
    private String nombre;

    /**
	 * Compare both objects
	 * @param claseServicio is the new contract object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( ClaseServicio claseServicio ) {
		if( claseServicio == null || !this.id.equals( claseServicio.getId() ) ) {
			return false;
		}
		return true;
	}

    /**
     * Get the service name of a class
     * @return String that refers to the service class name
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Set the service name of a class
     * @param nombre String that refers to the service class name
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Get the service ID of a class
     * @return String that refers to the service class ID
     */
    public String getId() {
        return id;
    }

    /**
     * Set the service ID of a class
     * @param id String that refers to the service class ID
     */
    public void setId(String id) {
        this.id = id;
    }

}
