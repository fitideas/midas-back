package com.colpatria.midas.model;

/*
 *
 * Libraries
 *
*/

import java.math.BigInteger;
import java.time.LocalDate;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity that represents the database model.
*/
@Entity
@Table( name="HISTORIAL_RELACION_CLIENTE_SUBPRODUCTO" )
public class HistorialRelacionClienteSubproducto {

    /**
     * BigInteger that specifies the record ID.
    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
	private BigInteger id;

    /**
     * String that specifies the client type.
    */
	@Column(name="TIPO_CLIENTE")
	private String tipoCliente; 

    /**
     * String that specifies the usage quota value.
    */
	@Column(name="VALOR_CUOTA_UTILIZACION")
	private Double valorCuotaUtilizacion; 

    /**
     * String that specifies the mandatory insurance code.
    */
	@Column(name="CODIGO_SEGURO_OBLIGATORIO")
	private String codigoSeguroObligatorio; 

    /**
     * String that specifies the mandatory insurance code.
    */
	@Column(name="FECHA_ORIGEN_RELACION")
	private LocalDate fechaOrigenRelacion; 

    /**
     * String that specifies the relationship cancellation date.
    */
	@Column(name="FECHA_CANCELACION_RELACION")
	private LocalDate fechaCancelacionRelacion; 

    /**
     * String that specifies the upgrade.
    */
	@Column(name="UPGRADE")
	private Boolean upgrade; 

    /**
     * String that specifies the upgrade date.
    */
	@Column(name="FECHA_UPGRADE")
	private LocalDate fechaUpgrade;
    
    /**
     * String that specifies the punishment start date.
    */
	@Column(name="FECHA_INICIAL_CASTIGO")
	private LocalDate fechaInicialCastigo;

    /**
     * String that specifies the punishment end date.
    */
	@Column(name="FECHA_FIN_CASTIGO")
	private LocalDate fechaFinCastigo;

    /**
     * String that specifies the invoice Submission initial date.
    */
	@Column(name="FECHA_INICIAL_ENVIO_FACTURA")
	private LocalDate fechaInicialEnvioFactura;

    /**
     * String that specifies the end date of sending the invoice.
    */
	@Column(name="FECHA_FIN_ENVIO_FACTURA")
	private LocalDate fechaFinEnvioFactura;

    /**
     * String that specifies the reason for not sending invoice.
    */
	@Column(name="MOTIVO_NO_ENVIO_FACTURA")
	private String motivoNoEnvioFactura;

    /**
	 * Variable that refers to the many-to-one relation between HISTORIAL_RELACION_CLIENTE_SUBPRODUCTO and CONTRATO
	*/
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CONTRATO_NUMERO", referencedColumnName = "NUMERO")
    private Contrato contrato;
    
    /**
     * String that specifies the generation date.
    */
	@Column(name="FECHA_GENERACION")
	private LocalDate fechaGeneracion;

    /**
     * Get the record id.
     * @return the record id.
    */
    public BigInteger getId() {
        return id;
    }

    /**
	 * Set the record id
	 * @param id is the record id
	*/
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
	 * Get the client type.
	 * @return the client type.
	*/
    public String getTipoCliente() {
        return tipoCliente;
    }

    /**
	 * Set the client type.
	 * @param tipoCliente is the client type.
	*/
    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    /**
	 * Get the usage quota value.
	 * @return the usage quota value.
	*/
    public Double getValorCuotaUtilizacion() {
        return valorCuotaUtilizacion;
    }

    /**
	 * Set the usage quota value.
	 * @param valorCuotaUtilizacion is the usage quota value.
	*/
    public void setValorCuotaUtilizacion(Double valorCuotaUtilizacion) {
        this.valorCuotaUtilizacion = valorCuotaUtilizacion;
    }

    /**
	 * Get the mandatory insurance code.
	 * @return the mandatory insurance code.
	*/
    public String getCodigoSeguroObligatorio() {
        return codigoSeguroObligatorio;
    }

    /**
	 * Set the mandatory insurance code.
	 * @param codigoSeguroObligatorio is the mandatory insurance code.
	*/
    public void setCodigoSeguroObligatorio(String codigoSeguroObligatorio) {
        this.codigoSeguroObligatorio = codigoSeguroObligatorio;
    }

    /**
	 * Get the relationship origin date.
	 * @return the relationship origin date.
	*/
    public LocalDate getFechaOrigenRelacion() {
        return fechaOrigenRelacion;
    }

    /**
	 * Set the relationship origin date.
	 * @param fechaOrigenRelacion is the relationship origin date.
	*/
    public void setFechaOrigenRelacion(LocalDate fechaOrigenRelacion) {
        this.fechaOrigenRelacion = fechaOrigenRelacion;
    }

    /**
	 * Get the relationship cancellation date.
	 * @return the relationship cancellation date.
	*/
    public LocalDate getFechaCancelacionRelacion() {
        return fechaCancelacionRelacion;
    }

    /**
	 * Set the relationship cancellation date.
	 * @param fechaCancelacionRelacion is the relationship cancellation date.
	*/
    public void setFechaCancelacionRelacion(LocalDate fechaCancelacionRelacion) {
        this.fechaCancelacionRelacion = fechaCancelacionRelacion;
    }

    /**
	 * Get the upgrade.
	 * @return the upgrade.
	*/
    public Boolean getUpgrade() {
        return upgrade;
    }

    /**
	 * Set the upgrade.
	 * @param upgrade is the upgrade.
	*/
    public void setUpgrade(Boolean upgrade) {
        this.upgrade = upgrade;
    }

    /**
	 * Get the upgrade date.
	 * @return the upgrade date.
	*/
    public LocalDate getFechaUpgrade() {
        return fechaUpgrade;
    }

    /**
	 * Set the upgrade date.
	 * @param fechaUpgrade is the upgrade date.
	*/
    public void setFechaUpgrade(LocalDate fechaUpgrade) {
        this.fechaUpgrade = fechaUpgrade;
    }

    /**
	 * Get the punishment start date.
	 * @return the punishment start date.
	*/
    public LocalDate getFechaInicialCastigo() {
        return fechaInicialCastigo;
    }

    /**
	 * Set the punishment start date.
	 * @param fechaInicialCastigo is the punishment start date.
	*/
    public void setFechaInicialCastigo(LocalDate fechaInicialCastigo) {
        this.fechaInicialCastigo = fechaInicialCastigo;
    }

    /**
	 * Get the punishment end date.
	 * @return the punishment end date.
	*/
    public LocalDate getFechaFinCastigo() {
        return fechaFinCastigo;
    }

    /**
	 * Set the punishment end date.
	 * @param fechaFinCastigo is the punishment end date.
	*/
    public void setFechaFinCastigo(LocalDate fechaFinCastigo) {
        this.fechaFinCastigo = fechaFinCastigo;
    }

    /**
	 * Get the invoice Submission initial date.
	 * @return the invoice Submission initial date.
	*/
    public LocalDate getFechaInicialEnvioFactura() {
        return fechaInicialEnvioFactura;
    }

    /**
	 * Set the invoice Submission initial date.
	 * @param fechaInicialEnvioFactura is the invoice Submission initial date.
	*/
    public void setFechaInicialEnvioFactura(LocalDate fechaInicialEnvioFactura) {
        this.fechaInicialEnvioFactura = fechaInicialEnvioFactura;
    }

    /**
	 * Get the end date of sending the invoice.
	 * @return the end date of sending the invoice.
	*/
    public LocalDate getFechaFinEnvioFactura() {
        return fechaFinEnvioFactura;
    }

    /**
	 * Set the end date of sending the invoice.
	 * @param fechaFinEnvioFactura is the end date of sending the invoice.
	*/
    public void setFechaFinEnvioFactura(LocalDate fechaFinEnvioFactura) {
        this.fechaFinEnvioFactura = fechaFinEnvioFactura;
    }

    /**
	 * Get the reason for not sending invoice.
	 * @return the reason for not sending invoice.
	*/
    public String getMotivoNoEnvioFactura() {
        return motivoNoEnvioFactura;
    }

    /**
	 * Set the reason for not sending invoice.
	 * @param motivoNoEnvioFactura is the reason for not sending invoice.
	*/
    public void setMotivoNoEnvioFactura(String motivoNoEnvioFactura) {
        this.motivoNoEnvioFactura = motivoNoEnvioFactura;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public LocalDate getFechaGeneracion() {
        return fechaGeneracion;
    }

    public void setFechaGeneracion(LocalDate fechaGeneracion) {
        this.fechaGeneracion = fechaGeneracion;
    }

    
    
}
