package com.colpatria.midas.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

@Entity
@Table(name="RECAUDO_CARGO")
public class RecaudoCargo implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CODIGO")
	private String codigo;

	@Column(name = "VALOR_PAGO")
	private Double valorPago;

	@Column(name = "MONTO")
	private Double monto;

	@Column(name = "OFICINA")
	private Long oficina;

	@Column(name = "FECHA_DOCUMENTO")
	private LocalDate fechaDocumento;

	@Column(name = "NRO_DOCUMENTO")
	private String nroDocumento;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MEDIO_PAGO", referencedColumnName = "ID")
	private FormaPago medioPago;

	@Column(name = "TIPO_PAGO")
	private Integer tipoPago;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SERVICIO", referencedColumnName = "NUMERO")
	private Servicio servicio;

	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CLIENTE_NUMERO_ID", referencedColumnName = "NUMERO_IDENTIFICACION")
    private Cliente cliente;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Double getValorPago() {
		return valorPago;
	}

	public void setValorPago(Double valorPago) {
		this.valorPago = valorPago;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Long getOficina() {
		return oficina;
	}

	public void setOficina(Long oficina) {
		this.oficina = oficina;
	}

	public LocalDate getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(LocalDate fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public FormaPago getMedioPago() {
		return medioPago;
	}

	public void setMedioPago(FormaPago medioPago) {
		this.medioPago = medioPago;
	}

	public Integer getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(Integer tipoPago) {
		this.tipoPago = tipoPago;
	}

	public Servicio getServicio() {
		return servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}