package com.colpatria.midas.model;

/*
 *
 * Libraries
 *
*/

import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.CascadeType;

/**
 * Entity that represents the database model.
*/
@Entity
@Table(name = "LECTURA")
public class Lectura {

    /**
     * BigInteger that specifies the entity ID.
    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
	private BigInteger id;

    /**
	 * String that specifies the apple.
	*/
	@Column( name = "MANZANA" )
    private String manzana;

    /**
	 * Sucursal that specifies the relation between LECTURA and SUCURSAL.
	*/
	@ManyToOne
	@JoinColumn( name = "SUCURSAL_NUMERO", referencedColumnName = "NUMERO" )
	private Sucursal sucursal;

    /**
	 * String that specifies the zone.
	*/
	@Column( name = "ZONA" )
    private String zona;

    /**
	 * Integer that specifies the cycle.
	*/
	@Column( name = "CICLO" )
    private Integer ciclo;

    /**
	 * String that specifies the group.
	*/
	@Column( name = "GRUPO" )
    private String grupo;

    /**
	 * Barrio that specifies the relation between LECTURA and BARRIO.
	*/
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn( name = "BARRIO_CODIGO", referencedColumnName = "CODIGO" )
	private Barrio barrio;
    
    /**
	 * String that specifies the location.
	*/
	@Column( name = "LOCALIZACION" )
    private String localizacion;

    /**
     * Get the entity ID.
     * @return the entity ID.
    */
    public BigInteger getId() {
        return id;
    }

    /**
     * Set the entity ID.
     * @param id is the entity ID.
    */
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
     * Get the apple.
     * @return the apple.
    */
    public String getManzana() {
        return manzana;
    }

    /**
     * Set the apple.
     * @param manzana is the apple.
    */
    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    /**
     * Get the relation between LECTURA and SUCURSAL.
     * @return the relation between LECTURA and SUCURSAL.
    */
    public Sucursal getSucursal() {
        return sucursal;
    }

    /**
     * Set the relation between LECTURA and SUCURSAL.
     * @param sucursal is the relation between LECTURA and SUCURSAL.
    */
    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * Get the zone.
     * @return the zone.
    */
    public String getZona() {
        return zona;
    }

    /**
     * Set the zone.
     * @param zona is the zone.
    */
    public void setZona(String zona) {
        this.zona = zona;
    }

    /**
     * Get the cycle.
     * @return the cycle.
    */
    public Integer getCiclo() {
        return ciclo;
    }

    /**
     * Set the cycle.
     * @param ciclo is the cycle.
    */
    public void setCiclo(Integer ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * Get the group.
     * @return the group.
    */
    public String getGrupo() {
        return grupo;
    }

    /**
     * Set the group.
     * @param grupo is the group.
    */
    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    /**
     * Get the relation between LECTURA and BARRIO.
     * @return the relation between LECTURA and BARRIO.
    */
    public Barrio getBarrio() {
        return barrio;
    }

    /**
     * Set the relation between LECTURA and BARRIO.
     * @param barrio is the relation between LECTURA and BARRIO.
    */
    public void setBarrio(Barrio barrio) {
        this.barrio = barrio;
    }

    /**
     * Get the location.
     * @return the location.
    */
    public String getLocalizacion() {
        return localizacion;
    }

    /**
     * Set the location.
     * @param localizacion is the location.
    */
    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }    
    
}
