package com.colpatria.midas.model;

/*
 *
 * Libraries
 *
*/

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.*;
import java.time.LocalDate;

/*
 *
 * Class
 *
*/
@MappedSuperclass
public class 	HistorialCartera implements Serializable {

	/**
     * BigInteger that specifies the record ID.
    */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	/**
     * Servicio that specifies the relation between SERVICE and HISTORIAL_REFINANCIACION.
    */
	@ManyToOne
	@JoinColumn(name="SERVICIO_NUMERO", referencedColumnName = "NUMERO")
	private Servicio servicio;

	/**
     * String that specifies the reason for termination.
    */
	@Column(name="MOTIVO_FINALIZACION")
	private String motivoFinalizacion; 

	/**
     * Integer that specifies the number of days late.
    */
	@Column(name="DIAS_ATRASO")
	private Integer diasAtraso;

	/**
     * LocalDate that specifies the first expiration date.
    */
	@Column(name="FECHA_PRIMER_VENCIMIENTO")
	private LocalDate fechaPrimerVencimiento;

	/**
     * LocalDate that specifies the second expiration date.
    */
	@Column(name="FECHA_SEGUNDO_VENCIMIENTO")
	private LocalDate fechaSegundoVencimiento;

	/**
     * Integer that specifies the first delinquent maturity.
    */
	@Column(name="MORA_PRIMER_VENCIMIENTO")
	private Integer moraPrimerVencimiento;

	/**
     * Double that specifies the unpaid money.
    */
	@Column(name="CAPITAL_IMPAGO")
	private Double capitalImpago;

	/**
     * Double that specifies the unpaid interest.
    */
	@Column(name="INTERES_IMPAGO")
    private Double interesImpago;

	/**
     * Double that specifies the unpaid current order interest.
    */
	@Column(name="INTERES_ORDEN_CORRIENTE_IMPAGO")
	private Double interesOrdenCorrienteImpago;

	/**
     * Double that specifies the delinquent interest.
    */
	@Column(name="INTERES_MORA")
    private Double interesMora;

	/**
     * Double that specifies the interest unpaid delinquent order.
    */
	@Column(name="INTERES_ORDEN_MORA_IMPAGO")
	private Double interesOrdenMoraImpago;

	/**
     * Double that specifies the unbilled accrued interest.
    */
	@Column(name="INTERES_CAUSADO_NO_FACTURADO")
	private Double interesCausadoNoFacturado;

	/**
     * Double that specifies the current interest order caused not billed.
    */
	@Column(name="INTERES_CORRIENTE_ORDEN_CAUSADO_NO_FACTURADO")
	private Double interesCorrienteOrdenCausadoNoFacturado;

	/**
     * Double that specifies the current interest order caused not billed.
    */
	@Column(name="INTERES_MORA_CAUSADO_NO_FACTURADO")
	private Double interesMoraCausadoNoFacturado;

	/**
     * Double variable that refers to the delayed order caused unbilled interests.
    */
	@Column(name="INTERES_MORA_ORDEN_CAUSADO_NO_FACTURADO")
	private Double interesMoraOrdenCausadoNoFacturado;

	/**
     * Double variable that refers to the total current billed unpaid interest.
    */
	@Column(name="TOTAL_INTERES_CORRIENTE_FACTURADO_IMPAGO")
	private Double totalInteresCorrienteFacturadoImpago;

	/**
     * Double variable that refers to the total caused current interest.
    */
	@Column(name="TOTAL_INTERES_CORRIENTE_CAUSADO")
	private Double totalInteresCorrienteCausado;

	/**
     * Double variable that refers to the total delayed billed unpaid interest.
    */
	@Column(name="TOTAL_INTERES_MORA_FACTURADO_IMPAGO")
	private Double totalInteresMoraFacturadoImpago;

	/**
     * Double variable that refers to the total caused delay interest.
    */
	@Column(name="TOTAL_INTERES_MORA_CAUSADO")
	private Double totalInteresMoraCausado;

	/**
     * Double variable that refers to the unaffected billed interest.
    */
	@Column(name="INTERES_FACTURADO_NO_AFECTO")
	private Double interesFacturadoNoAfecto;

	/**
     * Double variable that refers to the unaffected interest for bill.
    */
	@Column(name="INTERES_NO_AFECTO_POR_FACTURAR")
	private Double interesNoAfectoPorFacturar;

	/**
	 * Primera Altura Mora Value
	 */
	@Column(name = "PRIMERA_ALTURA_MORA")
	private Integer primeraAlturaMora;

	/**
	 * Segunda Altura Mora Value
	 */
	@Column(name = "SEGUNDA_ALTURA_MORA")
	private Integer segundaAlturaMora;
	
	/**
     * LocalDate variable that refers to the report date.
    */
	@Column(name="FECHA_REPORTE")
	private LocalDate fechaReporte;

	/**
	 * LocalDate variable that refers to the final date.
	 */
	@Column(name="FECHA_CIERRE")
	private LocalDate fechaCierre;

	/**
     * String that specifies to the description.
    */
	@Column(name="DESCRIPCION")
	private String descripcion;

	/**
     * Get the record id.
     * @return the record id.
    */
	public BigInteger getId() {
		return id;
	}

	/**
	 * Set the record id.
	 * @param id is the record id.
	*/
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
     * Get the service.
     * @return the service.
    */
	public Servicio getServicio() {
		return servicio;
	}

	/**
	 * Set the service.
	 * @param servicio is the service.
	*/
	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

	/**
     * Get the reason for termination.
     * @return the reason for termination.
    */
	public String getMotivoFinalizacion() {
		return motivoFinalizacion;
	}

	/**
	 * Set the reason for termination.
	 * @param motivoFinalizacion is the reason for termination.
	*/
	public void setMotivoFinalizacion(String motivoFinalizacion) {
		this.motivoFinalizacion = motivoFinalizacion;
	}

	/**
     * Get the number of days late.
     * @return the number of days late.
    */
	public Integer getDiasAtraso() {
		return diasAtraso;
	}

	/**
	 * Set the number of days late.
	 * @param diasAtraso is the number of days late.
	*/
	public void setDiasAtraso(Integer diasAtraso) {
		this.diasAtraso = diasAtraso;
	}

	/**
     * Get the first expiration date.
     * @return the first expiration date.
    */
	public LocalDate getFechaPrimerVencimiento() {
		return fechaPrimerVencimiento;
	}

	/**
	 * Set the first expiration date.
	 * @param fechaPrimerVencimiento is the first expiration date.
	*/
	public void setFechaPrimerVencimiento(LocalDate fechaPrimerVencimiento) {
		this.fechaPrimerVencimiento = fechaPrimerVencimiento;
	}

	/**
     * Get the second expiration date.
     * @return the second expiration date.
    */
	public LocalDate getFechaSegundoVencimiento() {
		return fechaSegundoVencimiento;
	}

	/**
	 * Set the second expiration date.
	 * @param fechaSegundoVencimiento is the second expiration date.
	*/
	public void setFechaSegundoVencimiento(LocalDate fechaSegundoVencimiento) {
		this.fechaSegundoVencimiento = fechaSegundoVencimiento;
	}

	/**
     * Get the report date.
     * @return the report date.
    */
	public LocalDate getFechaReporte() {
		return fechaReporte;
	}

	/**
	 * Set the report date.
	 * @param fechaReporte is the report date.
	*/
	public void setFechaReporte(LocalDate fechaReporte) {
		this.fechaReporte = fechaReporte;
	}

	public LocalDate getFechaCierre() {
		return fechaCierre;
	}

	public void setFechaCierre(LocalDate fechaCierre) {
		this.fechaCierre = fechaCierre;
	}

	public Integer getPrimeraAlturaMora() {
		return primeraAlturaMora;
	}

	public void setPrimeraAlturaMora(Integer primeraAlturaMora) {
		this.primeraAlturaMora = primeraAlturaMora;
	}

	public Integer getSegundaAlturaMora() {
		return segundaAlturaMora;
	}

	public void setSegundaAlturaMora(Integer segundaAlturaMora) {
		this.segundaAlturaMora = segundaAlturaMora;
	}

	/**
     * Get the description.
     * @return the description.
    */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Set the description.
	 * @param descripcion is the description.
	*/
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
     * Get the first delinquent maturity.
     * @return the first delinquent maturity.
    */
	public Integer getMoraPrimerVencimiento() {
		return moraPrimerVencimiento;
	}

	/**
	 * Set the first delinquent maturity.
	 * @param moraPrimerVencimiento is the first delinquent maturity.
	*/
	public void setMoraPrimerVencimiento(Integer moraPrimerVencimiento) {
		this.moraPrimerVencimiento = moraPrimerVencimiento;
	}

	/**
     * Get the unpaid money.
     * @return the unpaid money.
    */
	public Double getCapitalImpago() {
		return capitalImpago;
	}

	/**
	 * Set the unpaid money.
	 * @param capitalImpago is the unpaid money.
	*/
	public void setCapitalImpago(Double capitalImpago) {
		this.capitalImpago = capitalImpago;
	}

	/**
     * Get the unpaid interest.
     * @return the unpaid interest.
    */
	public Double getInteresImpago() {
		return interesImpago;
	}

	/**
	 * Set the unpaid interest.
	 * @param interesImpago is the unpaid interest.
	*/
	public void setInteresImpago(Double interesImpago) {
		this.interesImpago = interesImpago;
	}

	/**
     * Get the unpaid current order interest.
     * @return the unpaid current order interest.
    */
	public Double getInteresOrdenCorrienteImpago() {
		return interesOrdenCorrienteImpago;
	}

	/**
	 * Set the unpaid current order interest.
	 * @param interesOrdenCorrienteImpago is the unpaid current order interest.
	*/
	public void setInteresOrdenCorrienteImpago(Double interesOrdenCorrienteImpago) {
		this.interesOrdenCorrienteImpago = interesOrdenCorrienteImpago;
	}

	/**
     * Get the delinquent interest.
     * @return the delinquent interest.
    */
	public Double getInteresMora() {
		return interesMora;
	}

	/**
	 * Set the delinquent interest.
	 * @param interesMora is the delinquent interest.
	*/
	public void setInteresMora(Double interesMora) {
		this.interesMora = interesMora;
	}

	/**
     * Get the interest unpaid delinquent order.
     * @return the interest unpaid delinquent order.
    */
	public Double getInteresOrdenMoraImpago() {
		return interesOrdenMoraImpago;
	}

	/**
	 * Set the interest unpaid delinquent order.
	 * @param interesOrdenMoraImpago is the interest unpaid delinquent order.
	*/
	public void setInteresOrdenMoraImpago(Double interesOrdenMoraImpago) {
		this.interesOrdenMoraImpago = interesOrdenMoraImpago;
	}

	/**
     * Get the unbilled accrued interest.
     * @return the unbilled accrued interest.
    */
	public Double getInteresCausadoNoFacturado() {
		return interesCausadoNoFacturado;
	}

	/**
	 * Set the unbilled accrued interest.
	 * @param interesCausadoNoFacturado is the unbilled accrued interest.
	*/
	public void setInteresCausadoNoFacturado(Double interesCausadoNoFacturado) {
		this.interesCausadoNoFacturado = interesCausadoNoFacturado;
	}

	/**
     * Get the current interest order caused not billed.
     * @return the current interest order caused not billed.
    */
	public Double getInteresCorrienteOrdenCausadoNoFacturado() {
		return interesCorrienteOrdenCausadoNoFacturado;
	}

	/**
	 * Set the current interest order caused not billed.
	 * @param interesCorrienteOrdenCausadoNoFacturado is the current interest order caused not billed.
	*/
	public void setInteresCorrienteOrdenCausadoNoFacturado(Double interesCorrienteOrdenCausadoNoFacturado) {
		this.interesCorrienteOrdenCausadoNoFacturado = interesCorrienteOrdenCausadoNoFacturado;
	}

	/**
     * Get the current interest order caused not billed.
     * @return the current interest order caused not billed.
    */
	public Double getInteresMoraCausadoNoFacturado() {
		return interesMoraCausadoNoFacturado;
	}

	/**
	 * Set the current interest order caused not billed.
	 * @param interesMoraCausadoNoFacturado is the current interest order caused not billed.
	*/
	public void setInteresMoraCausadoNoFacturado(Double interesMoraCausadoNoFacturado) {
		this.interesMoraCausadoNoFacturado = interesMoraCausadoNoFacturado;
	}

	/**
     * Get the delayed order caused unbilled interests.
     * @return the delayed order caused unbilled interests.
    */
	public Double getInteresMoraOrdenCausadoNoFacturado() {
		return interesMoraOrdenCausadoNoFacturado;
	}

	/**
	 * Set the delayed order caused unbilled interests.
	 * @param interesMoraOrdenCausadoNoFacturado is the delayed order caused unbilled interests.
	*/
	public void setInteresMoraOrdenCausadoNoFacturado(Double interesMoraOrdenCausadoNoFacturado) {
		this.interesMoraOrdenCausadoNoFacturado = interesMoraOrdenCausadoNoFacturado;
	}

	/**
     * Get the total current billed unpaid interest.
     * @return the total current billed unpaid interest.
    */
	public Double getTotalInteresCorrienteFacturadoImpago() {
		return totalInteresCorrienteFacturadoImpago;
	}

	/**
	 * Set the total current billed unpaid interest.
	 * @param totalInteresCorrienteFacturadoImpago is the total current billed unpaid interest.
	*/
	public void setTotalInteresCorrienteFacturadoImpago(Double totalInteresCorrienteFacturadoImpago) {
		this.totalInteresCorrienteFacturadoImpago = totalInteresCorrienteFacturadoImpago;
	}

	/**
     * Get the total caused current interest.
     * @return the total caused current interest.
    */
	public Double getTotalInteresCorrienteCausado() {
		return totalInteresCorrienteCausado;
	}

	/**
	 * Set the total caused current interest.
	 * @param totalInteresCorrienteCausado is the total caused current interest.
	*/
	public void setTotalInteresCorrienteCausado(Double totalInteresCorrienteCausado) {
		this.totalInteresCorrienteCausado = totalInteresCorrienteCausado;
	}

	/**
     * Get the total delayed billed unpaid interest.
     * @return the total delayed billed unpaid interest.
    */
	public Double getTotalInteresMoraFacturadoImpago() {
		return totalInteresMoraFacturadoImpago;
	}

	/**
	 * Set the total delayed billed unpaid interest.
	 * @param totalInteresMoraFacturadoImpago is the total delayed billed unpaid interest.
	*/
	public void setTotalInteresMoraFacturadoImpago(Double totalInteresMoraFacturadoImpago) {
		this.totalInteresMoraFacturadoImpago = totalInteresMoraFacturadoImpago;
	}

	/**
     * Get the total caused delay interest.
     * @return the total caused delay interest.
    */
	public Double getTotalInteresMoraCausado() {
		return totalInteresMoraCausado;
	}

	/**
	 * Set the total caused delay interest.
	 * @param totalInteresMoraCausado is the total caused delay interest.
	*/
	public void setTotalInteresMoraCausado(Double totalInteresMoraCausado) {
		this.totalInteresMoraCausado = totalInteresMoraCausado;
	}

	/**
     * Get the unaffected billed interest.
     * @return the unaffected billed interest.
    */
	public Double getInteresFacturadoNoAfecto() {
		return interesFacturadoNoAfecto;
	}

	/**
	 * Set the unaffected billed interest.
	 * @param interesFacturadoNoAfecto is the unaffected billed interest.
	*/
	public void setInteresFacturadoNoAfecto(Double interesFacturadoNoAfecto) {
		this.interesFacturadoNoAfecto = interesFacturadoNoAfecto;
	}

	/**
     * Get the unaffected interest for bill.
     * @return the unaffected interest for bill.
    */
	public Double getInteresNoAfectoPorFacturar() {
		return interesNoAfectoPorFacturar;
	}

	/**
	 * Set the unaffected interest for bill.
	 * @param interesNoAfectoPorFacturar is the unaffected interest for bill.
	*/
	public void setInteresNoAfectoPorFacturar(Double interesNoAfectoPorFacturar) {
		this.interesNoAfectoPorFacturar = interesNoAfectoPorFacturar;
	}

}