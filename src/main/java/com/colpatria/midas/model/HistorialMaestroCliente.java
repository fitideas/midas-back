package com.colpatria.midas.model;

/*
 *
 * Libraries
 *
*/

import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity that represents the database model.
*/
@Entity
@Table(name = "HISTORIAL_MAESTRO_CLIENTE")
public class HistorialMaestroCliente {

    /**
     * BigInteger that specifies the entity ID.
    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
	private BigInteger id;

    /**
     * Variable that refers to the many-to-one relation between CLIENTE and CONTRATO
     */
    @ManyToOne
    @JoinColumn(name = "CLIENTE_NUMERO_ID", referencedColumnName = "NUMERO_IDENTIFICACION")
    private Cliente cliente;

    /**
     * Servicio that specifies the relation between SERVICE and HISTORIAL_MAESTRO_CLIENTE.
    */
	@ManyToOne
	@JoinColumn(name="SERVICIO_NUMERO", referencedColumnName = "NUMERO")
	private Servicio servicio;

    /**
     * Lectura that specifies the relation between LECTURA and HISTORIAL_MAESTRO_CLIENTE.
    */
	@ManyToOne
	@JoinColumn(name="LECTURA_ID", referencedColumnName = "ID")
	private Lectura lectura;

    /**
     * Reparto that specifies the relation between REPARTO and HISTORIAL_MAESTRO_CLIENTE.
    */
	@ManyToOne
	@JoinColumn(name="REPARTO_ID", referencedColumnName = "ID")
	private Reparto reparto;

    /**
     * String that specifies the electrical service status.
    */
	@Column(name="ESTADO_SERVICIO_ELECTRICO")
	private String estadoServicioElectrico; 

    /**
     * String that specifies the economic activity code.
    */
	@Column(name="CODIGO_ACTIVIDAD_ECONOMICA")
	private String codigoActividadEconomica; 

    /**
     * String that specifies the internal code.
    */
	@Column(name="CODIGO_INTERNO")
	private String codigoInterno; 

    /**
     * String that specifies the economic activity description.
    */
	@Column(name="DESCRIPCION_ACTIVIDAD_ECONOMICA")
	private String descripcionActividadEconomica;


    /**
     * Get the entity ID.
     * @return the entity ID.
    */
    public BigInteger getId() {
        return id;
    }

    /**
     * Set the entity ID.
     * @param id is the entity ID.
    */
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
     * Get the relation between SERVICE and HISTORIAL_MAESTRO_CLIENTE.
     * @return the relation between SERVICE and HISTORIAL_MAESTRO_CLIENTE.
    */
    public Servicio getServicio() {
        return servicio;
    }

    /**
     * Set the relation between SERVICE and HISTORIAL_MAESTRO_CLIENTE.
     * @param servicio is the relation between SERVICE and HISTORIAL_MAESTRO_CLIENTE.
    */
    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    /**
     * Get the relation between LECTURA and HISTORIAL_MAESTRO_CLIENTE.
     * @return the relation between LECTURA and HISTORIAL_MAESTRO_CLIENTE.
    */
    public Lectura getLectura() {
        return lectura;
    }

    /**
     * Set the relation between LECTURA and HISTORIAL_MAESTRO_CLIENTE.
     * @param lectura is the relation between LECTURA and HISTORIAL_MAESTRO_CLIENTE.
    */
    public void setLectura(Lectura lectura) {
        this.lectura = lectura;
    }

    /**
     * Get the relation between REPARTO and HISTORIAL_MAESTRO_CLIENTE.
     * @return the relation between REPARTO and HISTORIAL_MAESTRO_CLIENTE.
    */
    public Reparto getReparto() {
        return reparto;
    }

    /**
     * Set the relation between REPARTO and HISTORIAL_MAESTRO_CLIENTE.
     * @param reparto is the relation between REPARTO and HISTORIAL_MAESTRO_CLIENTE.
    */
    public void setReparto(Reparto reparto) {
        this.reparto = reparto;
    }

    /**
     * Get the electrical service status.
     * @return the electrical service status.
    */
    public String getEstadoServicioElectrico() {
        return estadoServicioElectrico;
    }

    /**
     * Set the electrical service status.
     * @param estadoServicioElectrico is the electrical service status.
    */
    public void setEstadoServicioElectrico(String estadoServicioElectrico) {
        this.estadoServicioElectrico = estadoServicioElectrico;
    }

    /**
     * Get the economic activity code.
     * @return the economic activity code.
    */
    public String getCodigoActividadEconomica() {
        return codigoActividadEconomica;
    }

    /**
     * Set the economic activity code.
     * @param codigoActividadEconomica is the economic activity code.
    */
    public void setCodigoActividadEconomica(String codigoActividadEconomica) {
        this.codigoActividadEconomica = codigoActividadEconomica;
    }

    /**
     * Get the internal code.
     * @return the internal code.
    */
    public String getCodigoInterno() {
        return codigoInterno;
    }

    /**
     * Set the internal code.
     * @param codigoInterno is the internal code.
    */
    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    /**
     * Get the economic activity description.
     * @return the economic activity description.
    */
    public String getDescripcionActividadEconomica() {
        return descripcionActividadEconomica;
    }

    /**
     * Set the economic activity description.
     * @param descripcionActividadEconomica is the economic activity description.
    */
    public void setDescripcionActividadEconomica(String descripcionActividadEconomica) {
        this.descripcionActividadEconomica = descripcionActividadEconomica;
    } 

    /**
     * Get the CLIENTE-CONTRATO relation of a class
     * @return Variable that refers to the many-to-one relation between CLIENTE and CONTRATO
    */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * Set the CLIENTE-CONTRATO relation of a class
     * @param cliente Variable that refers to the many-to-one relation between CLIENTE and CONTRATO
    */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
}
