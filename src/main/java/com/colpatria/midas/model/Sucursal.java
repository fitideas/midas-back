package com.colpatria.midas.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SUCURSAL")
public class Sucursal implements Serializable {

    @Id
	@Column(name = "NUMERO")
	private Integer numero;

    @Column(name = "NOMBRE")
    private String nombre;

    /**
	 * Compare both objects
	 * @param sucursal is the new billing cycle object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( Sucursal sucursal ) {
		if( sucursal == null || this.numero != sucursal.getNumero() ) {
			return false;
		}
		return true;
	}
    
    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
