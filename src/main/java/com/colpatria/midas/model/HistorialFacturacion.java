package com.colpatria.midas.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;

@Entity
@Table(name = "HISTORIAL_FACTURACION")
public class HistorialFacturacion implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private BigInteger id;

    @Column(name = "VALOR_FACT")
    private Double valorFact;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CARGO_ID", referencedColumnName = "ID")
    private Cargo cargo;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "FECHA_DOC")
    private LocalDate fechaDoc;

    @Column(name = "FECHA_PR")
    private LocalDate fechaPr;

    @Column(name = "TOT_DOCUMENTO")
    private Double totalDoc;

    @Column(name = "SALDO")
    private Double saldo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SERVICIO_NUMERO", referencedColumnName = "NUMERO")
    private Servicio servicio;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Double getValorFact() {
        return valorFact;
    }

    public void setValorFact(Double valorFact) {
        this.valorFact = valorFact;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LocalDate getFechaDoc() {
        return fechaDoc;
    }

    public void setFechaDoc(LocalDate fechaDoc) {
        this.fechaDoc = fechaDoc;
    }

    public LocalDate getFechaPr() {
        return fechaPr;
    }

    public void setFechaPr(LocalDate fechaPr) {
        this.fechaPr = fechaPr;
    }

    public Double getTotalDoc() {
        return totalDoc;
    }

    public void setTotalDoc(Double totalDoc) {
        this.totalDoc = totalDoc;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }
}
