package com.colpatria.midas.model;

/*
 *
 * Libraries
 *
*/

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Column;

/**
 * Entity that represents the database model.
*/
@Entity
@Table(name = "BARRIO")
public class Barrio {

    /**
     * String that specifies the neighborhood code.
    */
    @Id
    @Column(name = "CODIGO")
	private String codigo;

    /**
     * String that specifies the neighborhood code.
    */
    @Column(name = "NOMBRE")
	private String nombre;

    /**
	 * Variable LOCALIDAD_CODIGO that specifies one to one relation between BARRIO and LOCALIDAD_CODIGO
	 */
	@ManyToOne
    @JoinColumn(name="LOCALIDAD_CODIGO", referencedColumnName = "CODIGO")
	private Localidad localidad;

    /**
     * Get the neighborhood code.
     * @return the neighborhood code.
    */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Set the neighborhood code.
     * @param codigo is the neighborhood code.
    */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Get the neighborhood name.
     * @return the neighborhood name.
    */
    public String getNombre() {
        return nombre;
    }

    /**
     * Set the neighborhood name.
     * @param codigo is the neighborhood name.
    */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Get the location.
     * @return the location.
    */
    public Localidad getLocalidad() {
        return localidad;
    }

    /**
     * Set the location.
     * @param codigo is the location.
    */
    public void setLocalidad(Localidad localidad) {
        this.localidad = localidad;
    }    
    
}
