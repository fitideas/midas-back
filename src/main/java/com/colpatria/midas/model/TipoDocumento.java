package com.colpatria.midas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Tipo Producto Entity
 */
@Entity
@Table(name = "TIPO_DOCUMENTO")
public class TipoDocumento implements Serializable {

    /**
     * Document type code
     */
    @Id
    @Column(name = "CODIGO")
    private String codigo;

    /**
     * Document type name
     */
    @Column(name = "NOMBRE")
    private String nombre;
    /**
     * name of document type in cyber files
     */
    @Column(name = "HOMOLOGACION_CYBER", length = 100)
    private String homologacionCyber;

    /**
	 * Compare both objects
	 * @param tipoDocumento is the new account object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( TipoDocumento tipoDocumento ) {
		if( tipoDocumento == null || this.codigo != tipoDocumento.getCodigo() ) {
			return false;
		}
		return true;
	}

    public String getCodigo() {
        return this.codigo;
    }

    /**
     * Set the document type ID of a class
     * @param id String that refers to the document type ID
     */
    public void setCodigo(String id) {
        this.codigo = id;
    }

    /**
     * Get the document type name of a class
     * @return String that refers to the document type name
     */
    public String getNombre() {
        return this.nombre;
    }

    /**
     * Set the document type name of a class
     * @param nombre String that refers to the document type name
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * get name of document type in cyber file
     * @return name of document type in cyber file
     */
    public String getHomologacionCyber() {
        return homologacionCyber;
    }
    /**
     * set name of document type in cyber file
     */
    public void setHomologacionCyber(String homologacionCyber) {
        this.homologacionCyber = homologacionCyber;
    }
}
