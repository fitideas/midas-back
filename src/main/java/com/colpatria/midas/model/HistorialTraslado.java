package com.colpatria.midas.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.*;

@Entity
@Table(name="HISTORIAL_TRASLADO")
public class HistorialTraslado implements Serializable {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

    @ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="CONTRATO_NUMERO", referencedColumnName = "NUMERO")
	private Contrato contrato;
    
    @ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="USUARIO_ID", referencedColumnName = "ID")
	private Usuario usuario;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="CUENTA_ANTERIOR_NUMERO", referencedColumnName = "NUMERO")
	private Cuenta cuentaAnterior;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="CUENTA_ACTUAL_NUMERO", referencedColumnName = "NUMERO")
	private Cuenta cuentaActual;

    @Column(name = "FECHA")
    private LocalDate fecha;

    @Column(name = "OBSERVACION")
    private String observacion;

    @Column(name="CICLO_ANTERIOR")
    private Integer cicloAnterior;

    @Column(name="CICLO_ACTUAL")
    private Integer cicloActual;

    public BigInteger getId() {
        return this.id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Contrato getContrato() {
        return this.contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Cuenta getCuentaAnterior() {
        return this.cuentaAnterior;
    }

    public void setCuentaAnterior(Cuenta cuentaAnterior) {
        this.cuentaAnterior = cuentaAnterior;
    }

    public Cuenta getCuentaActual() {
        return this.cuentaActual;
    }

    public void setCuentaActual(Cuenta cuentaActual) {
        this.cuentaActual = cuentaActual;
    }

    public LocalDate getFecha() {
        return this.fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getObservacion() {
        return this.observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getCicloAnterior() {
        return cicloAnterior;
    }

    public void setCicloAnterior(Integer cicloAnterior) {
        this.cicloAnterior = cicloAnterior;
    }

    public Integer getCicloActual() {
        return cicloActual;
    }

    public void setCicloActual(Integer cicloActual) {
        this.cicloActual = cicloActual;
    }
}
