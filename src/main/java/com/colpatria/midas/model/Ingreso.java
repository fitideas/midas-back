package com.colpatria.midas.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Table(name = "INGRESO")
@Entity
@Getter
@Setter
public class Ingreso {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    /**
     * References the Entity Service
     */
    @ManyToOne(cascade=CascadeType.MERGE)
    @JoinColumn(name = "SERVICIO_NUMERO")
    private Servicio servicioNumero;

    /**
     * References the Municipio Entity
     */
    @ManyToOne
    @JoinColumn(name = "MUNICIPIO")
    private Municipio municipio;

    /**
     * References the CodigoCargo Entity
     */
    @ManyToOne
    @JoinColumn(name = "CODIGO_CARGO")
    private Cargo codigoCargo;

    /**
     * References the fechaDocumento
     */
    @Column(name = "FECHA_DOCUMENTO")
    private LocalDate fechaDocumento;

    /**
     * References the tasa Interes corriente information
     */
    @Column(name = "TASA_INTERES_CORRIENTE", precision = 5, scale = 2)
    private BigDecimal tasaInteresCorriente;

    /**
     * References the Fecha Causacion information
     */
    @Column(name = "FECHA_CAUSACION")
    private LocalDate fechaCausacion;

    /**
     * REferences the Tasa interes mora information
     */
    @Column(name = "TASA_INTERES_MORA", precision = 5, scale = 2)
    private BigDecimal tasaInteresMora;

    /**
     * References the monto information
     */
    @Column(name = "MONTO", precision = 18, scale = 4)
    private BigDecimal monto;

    /**
     * References the signo information
     */
    @Column(name = "SIGNO", length = 2)
    private String signo;

    /**
     * References the cuenta contable information
     */
    @Column(name = "CUENTA_CONTABLE", length = 18)
    private String cuentaContable;

}