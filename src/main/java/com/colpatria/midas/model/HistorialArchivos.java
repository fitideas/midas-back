package com.colpatria.midas.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Model of the HISTORIAL_ARCHIVO table on the database where the methods and objects are declared
 */
@Entity
@Table(name = "HISTORIAL_ARCHIVO")
public class HistorialArchivos implements Serializable {

    /**
     * String that refers to the file name
     */
    @Id
    @Column(name = "NOMBRE_ARCHIVO")
    private String nombreArchivo;

    /**
     * Status that refers to the state of the file
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "ESTADO")
    private Status estado;

    /**
     * Integer that refers to the number of logs
     */
    @Column(name = "LOG")
    private Integer log;

    /**
     * Integer that refers to the amount of tries
     */
    @Column(name = "INTENTOS")
    private Integer intentos;

    /**
     * LocalDate referring to the last update
    */
    @Column(name = "ULTIMA_MODIFICACION")
    private LocalDateTime ultimaModificacion;

    /**
     * Get the file name of a class
     * @return String that refers to the file name
     */
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    /**
     * Set the file name of a class
     * @param nombreArchivo String that refers to the file name
     */
    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    /**
     * Get the state of a class
     * @return Status that refers to the state of the file
     */
    public Status getEstado() {
        return estado;
    }

    /**
     * Set the state of a class
     * @param estado Status that refers to the state of the file
     */
    public void setEstado(Status estado) {
        this.estado = estado;
    }

    /**
     * Get the amount of logs of a class
     * @return Integer that refers to the number of logs
     */
    public Integer getLog() {
        return this.log;
    }

    /**
     * Set the amount of logs of a class
     * @param log Integer that refers to the number of logs
     */
    public void setLog(Integer log) {
        this.log = log;
    }

    /**
     * Get the amount of tries of a class
     * @return Integer that refers to the amount of tries
     */
    public Integer getIntentos() {
        return intentos;
    }

    /**
     * Set the amount of tries of a class
     * @param intentos Integer that refers to the amount of tries
     */
    public void setIntentos(Integer intentos) {
        this.intentos = intentos;
    }

    /**
     * Get the last update.
     * @return LocalDate referring to the last update.
    */
    public LocalDateTime getUltimaModificacion() {
        return ultimaModificacion;
    }

    /**
     * Set the last update.
     * @param ultimaModificacion LocalDate referring to the last update.
    */
    public void setUltimaModificacion(LocalDateTime ultimaModificacion) {
        this.ultimaModificacion = ultimaModificacion;
    }

    /**
     * Enum method for the status
     */
    public enum Status {
        PENDIENTE, AGENDADO, PROCESANDO, REINTENTADO, FINALIZADO, ERROR
    }

}
