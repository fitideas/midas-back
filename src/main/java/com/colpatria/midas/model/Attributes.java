package com.colpatria.midas.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Midas Attributes
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ATTRIBUTES")
public class Attributes {

    @Id
    @Column(name = "K")
    private String key;

    @Column(name = "V")
    private String value;

}
