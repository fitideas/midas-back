package com.colpatria.midas.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Table(name = "HISTORIAL_CONDONACION")
@Entity
public class HistorialCondonacion {
    /**
     * Long that refers to the ID number of the Condonation
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    /**
     * TipoNegociacion that refers to the entity TipoNegociacion associated with the condonation
     */
    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "TIPO_NEGOCIACION" )
    private TipoNegociacion tipoNegociacion;

    /**
     * Negociacion that refers to the entity Negociacion associated with the condonation
     */
    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "NEGOCIACION_ID")
    private Negociacion negociacion;

    /**
     * String that refers to the contract number in the condonation
     */
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SERVICIO_NUMERO")
    private Servicio servicio;

    /**
     * LocalDate that refers to the Payment date
     */
    @Column(name = "FECHA_PAGO")
    private LocalDate fechaPago;

    /**
     * LocalDate that refers to the Application date
     */
    @Column(name = "FECHA_APLICA")
    private LocalDate fechaAplica;

    /**
     * LocalDate that refers to the rejection date
     */
    @Column(name = "FECHA_RECHAZO")
    private LocalDate fechaRechazo;

    /**
     * LocalDate that refers to the due date
     */
    @Column(name = "FECHA_VENCIMIENTO")
    private LocalDate fechaVencimiento;

    /**
     * BigDecimal that refers to the payment ammount
     */
    @Column(name = "VALOR_PAGO", precision = 18, scale = 5)
    private BigDecimal valorPago;

    /**
     * String that refers to the rejection reason in the condonation
     */
    @Column(name = "MOTIVO_RECHAZO", length = 50)
    private String motivoRechazo;

    /**
     * String that refers to the description in the condonation
     */
    @Column(name = "DESCRIPCION")
    private String descripcion;

    /**
     * BigDecimal that refers to the service number in the condonation
     */
    @Column(name = "CAPITAL_CONDONA", precision = 18, scale = 5)
    private BigDecimal capitalCondona;

    /**
     * Bigdecimal that refers to the capital balance in the condonation
     */
    @Column(name = "SALDO_CAPITAL_CONDONA", precision = 18, scale = 5)
    private BigDecimal saldoCapitalCondona;

    /**
     * Usuario that refers to the entity Usuario related to the condonation
     */
    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn(name = "USUARIO_ID")
    private Usuario usuario;

    /**
     * Gets the Usario entity associated with the condonation
     * @return Variable that refers to the one-to-many relation between CONDONACION and USUARIO
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * Sets the Usario entity associated with the condonation
     * @param usuario Variable of Type Usuario that refers to the one-to-many relation between CONDONACION and USUARIO
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * Gets the principal balance in the condonation
     * @return Variable of Type BigDecimal that refers to principal balance in the condonation
     */
    public BigDecimal getSaldoCapitalCondona() {
        return saldoCapitalCondona;
    }

    /**
     * Sets the principal balance in the condonation
     * @param saldoCapitalCondona Variable of Type BigDecimal that refers to principal balance in the condonation
     */
    public void setSaldoCapitalCondona(BigDecimal saldoCapitalCondona) {
        this.saldoCapitalCondona = saldoCapitalCondona;
    }

    /**
     * Gets the general balance in the condonation
     * @return Variable of Type BigDecimal that refers to capital in the condonation
     */
    public BigDecimal getCapitalCondona() {
        return capitalCondona;
    }

    /**
     * Sets the general balance in the condonation
     * @return Variable of Type BigDecimal that refers to capital in the condonation
     */
    public void setCapitalCondona(BigDecimal capitalCondona) {
        this.capitalCondona = capitalCondona;
    }

    /**
     * Gets the description in the condonation
     * @return Variable of Type String that refers to description in the condonation
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the general description in the condonation
     * @return Variable of Type String that refers to description in the condonation
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Gets the payment ammount
     * @return Variable of Type BigDecimal that refers to the payment ammount in the condonation
     */
    public BigDecimal getValorPago() {
        return valorPago;
    }

    /**
     * Sets the payment ammount of the condonation
     * @param valorPago Variable of Type BigDecimal that refers to the payment ammount in the condonation
     */
    public void setValorPago(BigDecimal valorPago) {
        this.valorPago = valorPago;
    }

    /**
     * Gets the due date of the condonation
     * @return Variable of Type localDate that refers to the fechaVencimiento in the condonation
     */
    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * Sets the due date of the condonation
     * @param fechaVencimiento Variable of Type localDate that refers to the fechaVencimiento in the condonation
     */
    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * Gets the rejection date of the condonation
     * @return Variable of Type localDate that refers to the fechaRechazo in the condonation
     */
    public LocalDate getFechaRechazo() {
        return fechaRechazo;
    }

    /**
     * Sets the rejection date of the condonation
     * @param fechaRechazo Variable of Type localDate that refers to the fechaRechazo in the condonation
     */
    public void setFechaRechazo(LocalDate fechaRechazo) {
        this.fechaRechazo = fechaRechazo;
    }

    /**
     * Gets the application Date
     * @return Variable of Type localDate that refers to the fechaAplica field in the condonation
     */
    public LocalDate getFechaAplica() {
        return fechaAplica;
    }

    /**
     * Sets the application Date
     * @param fechaAplica Variable of Type localDate that refers to the fechaAplica field in the condonation
     */
    public void setFechaAplica(LocalDate fechaAplica) {
        this.fechaAplica = fechaAplica;
    }

    /**
     * Gets the application Date
     * @return Variable of Type localDate that refers to the fechaPago field in the condonation
     */
    public LocalDate getFechaPago() {
        return fechaPago;
    }

    /**
     * Sets the payment Date
     * @param fechaPago Variable of Type localDate that refers to the fechaPago field in the condonation
     */
    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * Gets the contract number
     * @return Variable of Type String that refers to the numeroContrato field in the condonation
     */
    public Servicio getServicio() {
        return servicio;
    }

    /**
     * Sets the contract number
     * @return Variable of Type String that refers to the numeroContrato field in the condonation
     */
    public void setServicio(Servicio numeroServicio) {
        this.servicio = numeroServicio;
    }


     /**
     * Gets the negotiation
     * @return Variable of Type Negociacion that refers to the negociacion field in the condonation
     */
    public Negociacion getNegociacion() {
        return negociacion;
    }

    /**
     * Sets the negotiation
     * @param negociacion Variable of Type Negociacion that refers to the negociacion field in the condonation
     */
    public void setNegociacion(Negociacion negociacion) {
        this.negociacion = negociacion;
    }

    /**
     * Gets the negotiation type
     * @return Variable of Type TipoNegociacion that refers to the tipoNegociacion field in the condonation
     */
    public TipoNegociacion getTipoNegociacion() {
        return tipoNegociacion;
    }

    /**
     * Sets the negotiation type
     * @return Variable of Type TipoNegociacion that refers to the tipoNegociacion field in the condonation
     */
    public void setTipoNegociacion(TipoNegociacion tipoNegociacion) {
        this.tipoNegociacion = tipoNegociacion;
    }

    /**
     * Gets the product type
     * @return Variable of Type TipoProducto that refers to the tipoProducto field in the condonation
     */
    /*public TipoProducto getTipoProducto() {
        return tipoProducto;
    }*/

    /**
     * Sets the product type
     * @param tipoProducto Variable of Type TipoProducto that refers to the tipoProducto field in the condonation
     */
    /*public void setTipoProducto(TipoProducto tipoProducto) {
        this.tipoProducto = tipoProducto;
    }*/

    /**
     * Gets the record Id
     * @return Variable of Type Long that refers to the id in the condonation entity
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the Rejection cause
     * @return Variable of Type String that refers to the motivoRechazo field in the condonation
     */
    public String getMotivoRechazo() {
        return motivoRechazo;
    }

    /**
     * Sets the Rejection casuse
     * @param motivoRechazo Variable of Type String that refers to the motivoRechazo field in the condonation
     */
    public void setMotivoRechazo(String motivoRechazo) {
        this.motivoRechazo = motivoRechazo;
    }

    /**
     * Sets the record Id
     * @param id Variable of Type Long that refers to the id in the condonation entity
     */
    public void setId(Long id) {
        this.id = id;
    }
}