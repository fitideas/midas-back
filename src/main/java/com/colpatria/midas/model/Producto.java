package com.colpatria.midas.model;

/*
 *
 * Libraries
 *
*/

import java.io.Serializable;
import javax.persistence.*;
import com.colpatria.midas.exceptions.ElementNotFoundInCatalogueException;

/*
 *
 * Class
 *
*/

@Entity
@Table(name = "PRODUCTO")
public class Producto implements Serializable {

	/*
     *
     * Attributes
     *
    */

	@Id
	@Column(name = "CODIGO")
	private String codigo;

	@Column(name = "NOMBRE")
	private String nombre;

	@ManyToOne
	@JoinColumn(name = "TIPO_PRODUCTO_CODIGO", referencedColumnName = "CODIGO")
	private TipoProducto tipo;

	@ManyToOne
	@JoinColumn(name = "PRODUCTO_PADRE", referencedColumnName = "CODIGO")
	private Producto productoPadre;

	/*
     *
     * Methods
     *
    */

	public void validateProduct( Producto producto ) throws Exception {
		if( producto == null ) {
			throw new ElementNotFoundInCatalogueException( "Producto", codigo.toString() );
		}
		if( !equals( producto ) ) {
			throw new ElementNotFoundInCatalogueException( "Producto", codigo.toString() );
		}
	}

	/**
	 * Compare both objects
	 * @param producto is the new product object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( Producto producto ) {		
		if( producto == null || this.codigo != producto.getCodigo() ){
			return false;
		}
		return true;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoProducto getTipo() {
		return tipo;
	}

	public void setTipo(TipoProducto tipo) {
		this.tipo = tipo;
	}

	public Producto getProductoPadre() {
		return productoPadre;
	}

	public void setProductoPadre(Producto productoPadre) {
		this.productoPadre = productoPadre;
	}

}