package com.colpatria.midas.model;

/*
*
* Libraries
*
*/

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Entity that represents the database model.
*/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "HISTORIAL_CARTA_MOROSA")
public class    HistorialCartaMorosa {

    /**
     * BigInteger that specifies the record ID.
    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private BigInteger id;

    /**
     * Integer that specifies the month.
    */
    @Column(name = "MES", nullable = false)
    private Integer mes;

    /**
	 * Variable that refers to the many-to-one relation between HISTORIAL_CARTA_MOROSA and CLIENTE
	*/
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CLIENTE_IDENTIFICACION_NUMERO")
    private Cliente cliente;

    /**
	 * Variable that refers to the many-to-one relation between HISTORIAL_CARTA_MOROSA and CONTRATO
	*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTRATO_NUMERO")
    private Contrato contrato;

    /**
     * Integer that specifies the days late.
    */
    @Column(name = "DIAS_ATRASO", nullable = false)
    private Integer diasAtraso;

    /**
     * String that specifies the history state.
    */
    @Column(name = "ESTADO", nullable = false, length = 30)
    private String estado;

    /**
     * BigDecimal that specifies the total balance.
    */
    @Column(name = "SALDO_TOTAL", nullable = false, precision = 38)
    private BigDecimal saldoTotal;

    /**
     * BigDecimal that specifies the immediate payment.
    */
    @Column(name = "PAGO_INMEDIATO", nullable = false, precision = 38)
    private BigDecimal pagoInmediato;

    /**
     * BigDecimal that specifies the interests.
    */
    @Column(name = "INTERESES", nullable = false, precision = 38)
    private BigDecimal intereses;

    /**
     * BigDecimal that specifies the interests.
    */
    @Column(name = "INTERESES_MORA", nullable = false, precision = 38)
    private BigDecimal interesesMora;

    /**
     * BigDecimal that specifies the collection expenses.
    */
    @Column(name = "GASTOS_COBRANZA", nullable = false, precision = 38)
    private BigDecimal gastosCobranza;

    /**
     * LocalDate that specifies the expiration date.
    */
    @Column(name = "FECHA_VENCIMIENTO", nullable = false)
    private LocalDate fechaVencimiento;

    /**
     * LocalDate that specifies the cutoff date.
    */
    @Column(name = "CORTE", nullable = false)
    private LocalDate corte;

    /**
     * Boolean that specifies if the history has deleted.
    */
    @Column(name = "FECHA_ELIMINADO")
    private LocalDateTime fechaEliminado ;

    /**
     * LocalDate that specifies the generation date.
    */
    @Column(name = "FECHA_GENERACION", nullable = false)
    private LocalDate fechaGeneracion;
    /**
     * user who uploaded the Schedule
     */
    @OneToOne
    @JoinColumn(name = "USUARIO_ELIMINACION")
    private Usuario usuarioEliminacion;
}