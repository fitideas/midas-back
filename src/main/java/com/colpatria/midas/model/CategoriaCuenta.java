package com.colpatria.midas.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Model of the CATEGORIA_CUENTA table on the database where the methods and objects are declared
 */
@Entity
@Table(name = "CATEGORIA_CUENTA")
public class CategoriaCuenta implements Serializable {

    /**
     * Integer that refers to the category account ID
     */
    @Id
	@Column(name = "ID")
    private String id;

    /**
     * String that refers to the account category name
     */
    @Column(name = "NOMBRE")
    private String nombre;

    /**
	 * Compare both objects
	 * @param categoriaCuenta is the new account object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( CategoriaCuenta categoriaCuenta ) {
		if( categoriaCuenta == null || this.id != categoriaCuenta.getId() ) {
			return false;
		}
		return true;
	}

    /**
     * Get the ID of a class.
     * @return Integer that refers to the category account ID
     */
    public String getId() {
        return this.id;
    }

    /**
     * Set the ID of a class.
     * @param id Integer that refers to the category account ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get the name category of a class
     * @return String that refers to the account category name
     */
    public String getNombre() {
        return this.nombre;
    }

    /**
     * Set the name category of a class
     * @param nombre String that refers to the account category name
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
}
