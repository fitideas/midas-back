package com.colpatria.midas.model;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDate;

/**
 * Model of the HISTORIAL_ANULACION_PAGO table on the database where the methods and objects are declared
 */
@Entity
@Table( name = "HISTORIAL_ANULACION_PAGO" )
public class HistorialAnulacionPago {

    /**
     * BigInteger that refers to the HISTORIAL_ANULACION_PAGO ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    /**
     * String that refers to the paid document number
     */
    @Column( name = "NUMERO_DOCUMENTO_PAGADO" )
	private String numeroDocumentoPagado;

    /**
     * Variable that contains the entrance date
     */
    @Column( name = "FECHA_INGRESO" )
	private LocalDate fechaIngreso;

    /**
     * Variable that contains the payment date
     */
    @Column( name = "FECHA_PAGO" )
	private LocalDate fechaPago;

    /**
     * Variable that contains the expiration date
     */
    @Column( name = "FECHA_VENCIMIENTO" )
	private LocalDate fechaVencimiento;

    /**
     * Variable thar refers to the many-to-one relation between SERVICIO and HISTORIAL_ANULACION_PAGO
     */
    @ManyToOne
	@JoinColumn( name = "SERVICIO_NUMERO", referencedColumnName = "NUMERO" )
	private Servicio servicio;

    /**
     * Variable thar refers to the many-to-one relation between CARGO and HISTORIAL_ANULACION_PAGO
     */
    @ManyToOne
	@JoinColumn( name = "CARGO_ID", referencedColumnName = "ID" )
	private Cargo cargo;

    /**
     * Double that refers to the amount of money
     */
    @Column( name = "MONTO" )
	private Double monto;

    /**
     * Double that refers to the participation amount of money
     */
    @Column( name = "MONTO_PARTICIPACION" )
	private Double montoParticipacion;

    /**
     * Empty constructor of the class
     */
    public HistorialAnulacionPago() {
    }

    /**
     * Get the ID of a class
     * @return BigInteger that refers to the HISTORIAL_ANULACION_PAGO ID
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Set the ID of a class
     * @param id BigInteger that refers to the HISTORIAL_ANULACION_PAGO ID
     */
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
     * Get the paid  document number of a class
     * @return String that refers to the paid document number
     */
    public String getNumeroDocumentoPagado() {
        return numeroDocumentoPagado;
    }

    /**
     * Set the paid  document number of a class
     * @param numeroDocumentoPagado String that refers to the paid document number
     */
    public void setNumeroDocumentoPagado(String numeroDocumentoPagado) {
        this.numeroDocumentoPagado = numeroDocumentoPagado;
    }

    /**
     * Get the entrance date of a class
     * @return Variable that contains the entrance date
     */
    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    /**
     * Set the entrance date of a class
     * @param fechaIngreso Variable that contains the entrance date
     */
    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    /**
     * Get the payment date of a class
     * @return Variable that contains the payment date
     */
    public LocalDate getFechaPago() {
        return fechaPago;
    }

    /**
     * Set the payment date of a class
     * @param fechaPago Variable that contains the payment date
     */
    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * Get the expiration date of a class
     * @return Variable that contains the expiration date
     */
    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * Set the expiration date of a class
     * @param fechaVencimiento Variable that contains the expiration date
     */
    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * Get the SERVICIO and HISTORIAL_ANULACION_PAGO relation of a class
     * @return Variable thar refers to the many-to-one relation between SERVICIO and HISTORIAL_ANULACION_PAGO
     */
    public Servicio getServicio() {
        return servicio;
    }

    /**
     * Set the SERVICIO and HISTORIAL_ANULACION_PAGO relation of a class
     * @param servicio Variable thar refers to the many-to-one relation between SERVICIO and HISTORIAL_ANULACION_PAGO
     */
    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    /**
     * Get the CARGO and HISTORIAL_ANULACION_PAGO relation of a class
     * @return Variable thar refers to the many-to-one relation between CARGO and HISTORIAL_ANULACION_PAGO
     */
    public Cargo getCargo() {
        return cargo;
    }

    /**
     * Set the CARGO and HISTORIAL_ANULACION_PAGO relation of a class
     * @param cargo Variable thar refers to the many-to-one relation between CARGO and HISTORIAL_ANULACION_PAGO
     */
    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    /**
     * Get the amount of money of a class
     * @return Double that refers to the amount of money
     */
    public Double getMonto() {
        return monto;
    }

    /**
     * Set the amount of money of a class
     * @param monto Double that refers to the amount of money
     */
    public void setMonto(Double monto) {
        this.monto = monto;
    }

    /**
     * Get the participation amount of money of a class
     * @return Double that refers to the participation amount of money
     */
    public Double getMontoParticipacion() {
        return montoParticipacion;
    }

    /**
     * Set the participation amount of money of a class
     * @param montoParticipacion Double that refers to the participation amount of money
     */
    public void setMontoParticipacion(Double montoParticipacion) {
        this.montoParticipacion = montoParticipacion;
    }

}
