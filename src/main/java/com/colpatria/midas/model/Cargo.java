package com.colpatria.midas.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Model of CARGO table where the objects and methods are declared
 */
@Entity
@Table(name = "CARGO")
public class Cargo implements Serializable {

    /**
     * String that refers to the job title ID
     */
    @Id
    @Column(name = "ID")
    private String id;

    /**
     * String that refers to the job title name
     */
    @Column(name = "NOMBRE")
    private String nombre;

    /**
     * Identify cargo of colpatria
     */
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TIPO_CARGO", referencedColumnName = "CODIGO" )
    private TipoCargo tipoCargo;

    /**
     * Identify cargo of colpatria
     */
    @Column(name = "Cargo_Colpatria")
    private Boolean cargoColpatria;


    /**
     * Get the position ID of a class 
     * @return String that refers to the position ID  
     */
    public String getId() {
        return this.id;
    }

    /**
     * Set the position ID of a class 
     * @param id String that refers to the position ID  
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get the position name of a class
     * @return String that refers to the job title name
     */
    public String getNombre() {
        return this.nombre;
    }

    /**
     * Set the position name of a class
     * @param nombre String that refers to the job title name
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoCargo getTipoCargo() {
        return tipoCargo;
    }

    public void setTipoCargo(TipoCargo tipoCargo) {
        this.tipoCargo = tipoCargo;
    }

    public Boolean getCargoColpatria() {
        return cargoColpatria;
    }

    public void setCargoColpatria(Boolean cargoColpatria) {
        this.cargoColpatria = cargoColpatria;
    }
}
