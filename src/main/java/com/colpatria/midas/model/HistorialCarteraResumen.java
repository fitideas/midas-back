package com.colpatria.midas.model;

import java.math.BigInteger;
import java.time.LocalDate;
import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="HISTORIAL_CARTERA_RESUMEN")
public class HistorialCarteraResumen implements Serializable {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

	@ManyToOne
	@JoinColumn(name="CONTRATO_NUMERO", referencedColumnName = "NUMERO")
	private Contrato contrato;

    @Column(name="SUMA_CAPITAL_IMPAGO")
	private Double sumaCapitalImpago;

    @Column(name="DIAS_ATRASO_MAXIMO")
	private Integer diasAtrasoMaximo;

    @Column(name="FECHA_PRIMER_VENCIMIENTO_MINIMO")
	private LocalDate fechaPrimerVencimientoMinimo;

    @Column(name="FECHA_SEGUNDO_VENCIMIENTO_MINIMO")
	private LocalDate fechaSegundoVencimientoMinimo;

    @Column(name="FECHA_REPORTE")
	private LocalDate fechaReporte;

    @Column(name="SUMA_INTERES_CAUSADO_NO_FACTURADO")
	private Double  sumaInteresCausadoNoFacturado;

    @Column(name="SUMA_INTERES_CORRIENTE_ORDEN_CAUSADO_NO_FACTURADO")
	private Double sumaInteresCorrienteOrdenCausadoNoFacturado;

    @Column(name="SUMA_INTERES_FACTURADO_NO_AFECTO")
	private Double sumaInteresFacturadoNoAfecto;

    @Column(name="SUMA_INTERES_IMPAGO")
    private Double sumaInteresImpago;

    @Column(name="SUMA_INTERES_MORA")
    private Double sumaInteresMora;

    @Column(name="SUMA_INTERES_MORA_CAUSADO_NO_FACTURADO")
	private Double sumaInteresMoraCausadoNoFacturado;

    @Column(name="SUMA_INTERES_MORA_ORDEN_CAUSADO_NO_FACTURADO")
	private Double sumaInteresMoraOrdenCausadoNoFacturado;

    @Column(name="SUMA_INTERES_NO_AFECTO_POR_FACTURAR")
	private Double sumaInteresNoAfectoPorFacturar;

    @Column(name="SUMA_INTERES_ORDEN_CORRIENTE_IMPAGO")
	private Double sumaInteresOrdenCorrienteImpago;

    @Column(name="SUMA_INTERES_ORDEN_MORA_IMPAGO")
	private Double sumaInteresOrdenMoraImpago;

    @Column(name="MORA_PRIMER_VENCIMIENTO_MAXIMO")
	private Integer moraPrimerVencimientoMaximo;

    @Column(name="SUMA_TOTAL_INTERES_CORRIENTE_CAUSADO")
	private Double sumaTotalInteresCorrienteCausado;

    @Column(name="SUMA_TOTAL_INTERES_CORRIENTE_FACTURADO_IMPAGO")
	private Double sumaTotalInteresCorrienteFacturadoImpago;

    @Column(name="SUMA_TOTAL_INTERES_MORA_FACTURADO_IMPAGO")
	private Double sumaTotalInteresMoraFacturadoImpago;

    @Column(name="SUMA_TOTAL_INTERES_MORA_CAUSADO")
	private Double sumaTotalInteresMoraCausado;
	
    @Column(name="CUOTAS_PACTADAS_MINIMO")
    private Integer cuotasPactadasMinimo;

    @Column(name="CUOTAS_PACTADAS_MAXIMO")
    private Integer cuotasPactadasMaximo;

    @Column(name="CUOTAS_POR_FACTURAR_MINIMO")
    private Integer cuotasPorFacturarMinimo;

    @Column(name="CUOTAS_POR_FACTURAR_MAXIMO")
    private Integer cuotasPorFacturarMaximo;

    @Column(name="CANTIDAD_SERVICIOS")
    private Integer cantidadServicios;

    @Column(name="TASA_PONDERADA")
    private Double tasaPonderada;

    @Column(name="TASA_MAXIMA_PACTADA")
    private Double tasaMaximaPactada;

    @Column(name="TASA_MINIMA_PACTADA")
    private Double tasaMinimaPactada;

    @Column(name="SUMA_SALDO_DEUDA")
    private Double sumaSaldoDeuda;

    @Column(name = "SUMA_SALDO_CAPITAL_POR_FACTURAR")
	private Double sumaSaldoCapitalPorFacturar;

    @Column(name = "SUMA_VALOR_CUOTA")
	private Double sumaValorCuota;

	@Column(name = "SUMA_VALOR_COMPRA")
	private Double sumaValorCompra;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Integer getCantidadServicios() {
        return cantidadServicios;
    }

    public void setCantidadServicios(Integer cantidadServicios) {
        this.cantidadServicios = cantidadServicios;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Integer getCuotasPactadasMaximo() {
        return cuotasPactadasMaximo;
    }

    public void setCuotasPactadasMaximo(Integer cuotasPactadasMaximo) {
        this.cuotasPactadasMaximo = cuotasPactadasMaximo;
    }

    public Integer getCuotasPactadasMinimo() {
        return cuotasPactadasMinimo;
    }

    public void setCuotasPactadasMinimo(Integer cuotasPactadasMinimo) {
        this.cuotasPactadasMinimo = cuotasPactadasMinimo;
    }

    public Integer getCuotasPorFacturarMaximo() {
        return cuotasPorFacturarMaximo;
    }

    public void setCuotasPorFacturarMaximo(Integer cuotasPorFacturarMaximo) {
        this.cuotasPorFacturarMaximo = cuotasPorFacturarMaximo;
    }

    public Integer getCuotasPorFacturarMinimo() {
        return cuotasPorFacturarMinimo;
    }

    public void setCuotasPorFacturarMinimo(Integer cuotasPorFacturarMinimo) {
        this.cuotasPorFacturarMinimo = cuotasPorFacturarMinimo;
    }

    public Integer getDiasAtrasoMaximo() {
        return diasAtrasoMaximo;
    }

    public void setDiasAtrasoMaximo(Integer diasAtrasoMaximo) {
        this.diasAtrasoMaximo = diasAtrasoMaximo;
    }

    public LocalDate getFechaPrimerVencimientoMinimo() {
        return fechaPrimerVencimientoMinimo;
    }

    public void setFechaPrimerVencimientoMinimo(LocalDate fechaPrimerVencimientoMinimo) {
        this.fechaPrimerVencimientoMinimo = fechaPrimerVencimientoMinimo;
    }

    public LocalDate getFechaReporte() {
        return fechaReporte;
    }

    public void setFechaReporte(LocalDate fechaReporte) {
        this.fechaReporte = fechaReporte;
    }

    public LocalDate getFechaSegundoVencimientoMinimo() {
        return fechaSegundoVencimientoMinimo;
    }

    public void setFechaSegundoVencimientoMinimo(LocalDate fechaSegundoVencimientoMinimo) {
        this.fechaSegundoVencimientoMinimo = fechaSegundoVencimientoMinimo;
    }

    public Double getTasaMaximaPactada() {
        return tasaMaximaPactada;
    }

    public void setTasaMaximaPactada(Double tasaMaximaPactada) {
        this.tasaMaximaPactada = tasaMaximaPactada;
    }

    public Double getTasaMinimaPactada() {
        return tasaMinimaPactada;
    }

    public void setTasaMinimaPactada(Double tasaMinimaPactada) {
        this.tasaMinimaPactada = tasaMinimaPactada;
    }

    public Double getTasaPonderada() {
        return tasaPonderada;
    }

    public void setTasaPonderada(Double tasaPonderada) {
        this.tasaPonderada = tasaPonderada;
    }

    public Double getSumaCapitalImpago() {
        return sumaCapitalImpago;
    }

    public void setSumaCapitalImpago(Double sumaCapitalImpago) {
        this.sumaCapitalImpago = sumaCapitalImpago;
    }

    public Double getSumaInteresCausadoNoFacturado() {
        return sumaInteresCausadoNoFacturado;
    }

    public void setSumaInteresCausadoNoFacturado(Double sumaInteresCausadoNoFacturado) {
        this.sumaInteresCausadoNoFacturado = sumaInteresCausadoNoFacturado;
    }

    public Double getSumaInteresCorrienteOrdenCausadoNoFacturado() {
        return sumaInteresCorrienteOrdenCausadoNoFacturado;
    }

    public void setSumaInteresCorrienteOrdenCausadoNoFacturado(Double sumaInteresCorrienteOrdenCausadoNoFacturado) {
        this.sumaInteresCorrienteOrdenCausadoNoFacturado = sumaInteresCorrienteOrdenCausadoNoFacturado;
    }

    public Double getSumaInteresFacturadoNoAfecto() {
        return sumaInteresFacturadoNoAfecto;
    }

    public void setSumaInteresFacturadoNoAfecto(Double sumaInteresFacturadoNoAfecto) {
        this.sumaInteresFacturadoNoAfecto = sumaInteresFacturadoNoAfecto;
    }

    public Double getSumaInteresImpago() {
        return sumaInteresImpago;
    }

    public void setSumaInteresImpago(Double sumaInteresImpago) {
        this.sumaInteresImpago = sumaInteresImpago;
    }

    public Double getSumaInteresMora() {
        return sumaInteresMora;
    }

    public void setSumaInteresMora(Double sumaInteresMora) {
        this.sumaInteresMora = sumaInteresMora;
    }

    public Double getSumaInteresMoraCausadoNoFacturado() {
        return sumaInteresMoraCausadoNoFacturado;
    }

    public void setSumaInteresMoraCausadoNoFacturado(Double sumaInteresMoraCausadoNoFacturado) {
        this.sumaInteresMoraCausadoNoFacturado = sumaInteresMoraCausadoNoFacturado;
    }

    public Double getSumaInteresMoraOrdenCausadoNoFacturado() {
        return sumaInteresMoraOrdenCausadoNoFacturado;
    }

    public void setSumaInteresMoraOrdenCausadoNoFacturado(Double sumaInteresMoraOrdenCausadoNoFacturado) {
        this.sumaInteresMoraOrdenCausadoNoFacturado = sumaInteresMoraOrdenCausadoNoFacturado;
    }

    public Double getSumaInteresNoAfectoPorFacturar() {
        return sumaInteresNoAfectoPorFacturar;
    }

    public void setSumaInteresNoAfectoPorFacturar(Double sumaInteresNoAfectoPorFacturar) {
        this.sumaInteresNoAfectoPorFacturar = sumaInteresNoAfectoPorFacturar;
    }

    public Double getSumaInteresOrdenCorrienteImpago() {
        return sumaInteresOrdenCorrienteImpago;
    }

    public void setSumaInteresOrdenCorrienteImpago(Double sumaInteresOrdenCorrienteImpago) {
        this.sumaInteresOrdenCorrienteImpago = sumaInteresOrdenCorrienteImpago;
    }

    public Double getSumaInteresOrdenMoraImpago() {
        return sumaInteresOrdenMoraImpago;
    }

    public void setSumaInteresOrdenMoraImpago(Double sumaInteresOrdenMoraImpago) {
        this.sumaInteresOrdenMoraImpago = sumaInteresOrdenMoraImpago;
    }

    public Integer getMoraPrimerVencimientoMaximo() {
        return moraPrimerVencimientoMaximo;
    }

    public void setMoraPrimerVencimientoMaximo(Integer moraPrimerVencimientoMaximo) {
        this.moraPrimerVencimientoMaximo = moraPrimerVencimientoMaximo;
    }

    public Double getSumaTotalInteresCorrienteCausado() {
        return sumaTotalInteresCorrienteCausado;
    }

    public void setSumaTotalInteresCorrienteCausado(Double sumaTotalInteresCorrienteCausado) {
        this.sumaTotalInteresCorrienteCausado = sumaTotalInteresCorrienteCausado;
    }

    public Double getSumaTotalInteresCorrienteFacturadoImpago() {
        return sumaTotalInteresCorrienteFacturadoImpago;
    }

    public void setSumaTotalInteresCorrienteFacturadoImpago(Double sumaTotalInteresCorrienteFacturadoImpago) {
        this.sumaTotalInteresCorrienteFacturadoImpago = sumaTotalInteresCorrienteFacturadoImpago;
    }

    public Double getSumaTotalInteresMoraFacturadoImpago() {
        return sumaTotalInteresMoraFacturadoImpago;
    }

    public void setSumaTotalInteresMoraFacturadoImpago(Double sumaTotalInteresMoraFacturadoImpago) {
        this.sumaTotalInteresMoraFacturadoImpago = sumaTotalInteresMoraFacturadoImpago;
    }

    public Double getSumaTotalInteresMoraCausado() {
        return sumaTotalInteresMoraCausado;
    }

    public void setSumaTotalInteresMoraCausado(Double sumaTotalInteresMoraCausado) {
        this.sumaTotalInteresMoraCausado = sumaTotalInteresMoraCausado;
    }

    public Double getSumaSaldoDeuda() {
        return sumaSaldoDeuda;
    }

    public void setSumaSaldoDeuda(Double sumaSaldoDeuda) {
        this.sumaSaldoDeuda = sumaSaldoDeuda;
    }

    public Double getSumaSaldoCapitalPorFacturar() {
        return sumaSaldoCapitalPorFacturar;
    }

    public void setSumaSaldoCapitalPorFacturar(Double sumaSaldoCapitalPorFacturar) {
        this.sumaSaldoCapitalPorFacturar = sumaSaldoCapitalPorFacturar;
    }

    public Double getSumaValorCuota() {
        return sumaValorCuota;
    }

    public void setSumaValorCuota(Double sumaValorCuota) {
        this.sumaValorCuota = sumaValorCuota;
    }

    public Double getSumaValorCompra() {
        return sumaValorCompra;
    }

    public void setSumaValorCompra(Double sumaValorCompra) {
        this.sumaValorCompra = sumaValorCompra;
    }
    
    
}
