package com.colpatria.midas.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Entity that represents a response for Gestion cobro.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "GESTION_COBRO_RESPONSE")
public class GestionCobroResponse {
    /**
     * The ID.
     */
    @Id
    @Column(name = "ID", nullable = false, length = 4)
    private String id;

    /**
     * The Description.
     */
    @Column(name = "DESCRIPCION", length = 256)
    private String descripcion;

}