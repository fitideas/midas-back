package com.colpatria.midas.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TIPO_PRODUCTO")
public class TipoProducto implements Serializable {

    @Id
    @Column(name = "CODIGO")
    private String codigo;

    @Column(name = "NOMBRE")
    private String nombre;

    /**
	 * Compare both objects
	 * @param tipoProducto is the new product object to compare
	 * @return a boolean variable that tell if they are the same
	*/
    public boolean equals( TipoProducto tipoProducto ) {
        if( tipoProducto == null || this.codigo != tipoProducto.getCodigo() ) {
            return false;
        }
        return true;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
