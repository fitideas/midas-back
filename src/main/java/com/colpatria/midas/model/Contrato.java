package com.colpatria.midas.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Model of CLIENTE table in the database where the objects and methods are declared
 */
@Entity
@Table(name = "CONTRATO")
public class Contrato implements Serializable {

    /**
     * String that refers to the contract number
     */
    @Id
    @Column(name = "NUMERO")
    private String numero;

    /**
     * String that refers to the contract state
     */
    @Column(name = "ESTADO")
    private String estado;

    /**
     * String that refers to the contract billing
     */
    @Column(name = "FACTURACION")
    private Boolean facturacion;

    /**
     * Variable that refers to the one-to-one relation between CLIENTE and CONTRATO
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CLIENTE_NUMERO_ID", referencedColumnName = "NUMERO_IDENTIFICACION")
    private Cliente cliente;

    /**
     * Variable that refers to the many-to-one relation between SUBPRODUCTO and CONTRATO
     */
    @ManyToOne
    @JoinColumn(name = "SUBPRODUCTO_CODIGO", referencedColumnName = "CODIGO")
    private Producto subProducto;

    /**
     * Variable that refers to the one-to-many relation between SERVICIO and CONTRATO
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contrato")
	private List<Servicio> servicios;

    /**
	 * Compare both objects
	 * @param contrato is the new contract object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( Contrato contrato ) {
		if( contrato == null || !this.numero.equals( contrato.getNumero() ) ) {
			return false;
		}
		return true;
	}

    /**
     * Get the contract number of the class
     * @return BigInteger that refers to the contract number
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Set the contract number of the class
     * @param numero BigInteger that refers to the contract number
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Get the contract state of the class
     * @return String that refers to the contract state
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Set the contract state of the class
     * @param estado String that refers to the contract state
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Get the contract billing of the class
     * @return String that refers to the billing of the contract
     */
    public Boolean getFacturacion() {
        return facturacion;
    }

    /**
     * Set the contract billing of the class
     * @param facturacion String that refers to the billing of the contract
     */
    public void setFacturacion(Boolean facturacion) {
        this.facturacion = facturacion;
    }

    /**
     * Get the CLIENTE-CONTRATO relation of a class
     * @return Variable that refers to the many-to-one relation between CLIENTE and CONTRATO
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * Set the CLIENTE-CONTRATO relation of a class
     * @param cliente Variable that refers to the many-to-one relation between CLIENTE and CONTRATO
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * Get the SUBPRODUCTO-CONTRATO relation of a class
     * @return Variable that refers to the many-to-one relation between SUBPRODUCTO_CODIGO and CONTRATO
     */
    public Producto getSubProducto() {
        return subProducto;
    }

    /**
     * Set the SUBPRODUCTO-CONTRATO relation of a class
     * @param subProducto Variable that refers to the many-to-one relation between SUBPRODUCTO_CODIGO and CONTRATO
     */
    public void setSubProducto(Producto subProducto) {
        this.subProducto = subProducto;
    }

    /**
     * Get the SEVICIO-CONTRATO relation of a class
     * @return Variable that refers to the one-to-many relation between SERVICIO and CONTRATO
     */
    public List<Servicio> getServicios() {
        return servicios;
    }

    /**
     * Set the SEVICIO-CONTRATO relation of a class
     * @param servicios Variable that refers to the one-to-many relation between SERVICIO and CONTRATO
     */
    public void setServicios(List<Servicio> servicios) {
        this.servicios = servicios;
    }

}
