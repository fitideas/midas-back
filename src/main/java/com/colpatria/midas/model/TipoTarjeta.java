package com.colpatria.midas.model;

/*
 *
 * Librerías
 *
*/

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 *
 * Clase
 *
*/

@Entity
@Table(name = "TIPO_TARJETA")
public class TipoTarjeta implements Serializable {

    /*
     *
     * Atríbutos
     *
    */

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "NOMBRE")
    private String nombre;

    /*
     *
     * Métodos
     *
    */

    /**
	 * Compare both objects
	 * @param tipoTarjeta is the new contract object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( TipoTarjeta tipoTarjeta ) {
		if( tipoTarjeta == null || !this.id.equals( tipoTarjeta.getId() ) ) {
			return false;
		}
		return true;
	}

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
