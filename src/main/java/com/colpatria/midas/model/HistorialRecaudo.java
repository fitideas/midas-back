package com.colpatria.midas.model;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "HISTORIAL_RECAUDO")
public class HistorialRecaudo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @Column(name="FECHA_REPORTE")
    private LocalDateTime fechaReporte;

    @Column(name = "FECHA_PAGO")
    private LocalDate fechaPago;

    @Column(name = "FECHA_PROCESO_PAGO")
    private LocalDate fechaProcesoPago;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CODIGO_RECAUDO_CARGO", referencedColumnName = "CODIGO")
    private RecaudoCargo recaudoCargo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CARGO_ID", referencedColumnName = "ID")
    private Cargo cargo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FORMA_PAGO_ID", referencedColumnName = "ID")
    private FormaPago formaPago;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "TIPO_CONSUMO")
    private String tipoConsumo;

    @Column(name = "FECHA_FACTURACION")
    private LocalDate fechaFacturacion;

    @Column(name = "SUCURSAL_RECAUDO")
    private Integer sucursalRecaudo;

    @Column(name = "COD_ENTIDAD_RECAUDADORA")
    private Integer codEntidadRecaudadora;

    @ManyToOne
    @JoinColumn(name = "RESUMEN", referencedColumnName = "ID")
    private HistorialRecaudoResumen resumen;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public LocalDateTime getFechaReporte() {
        return fechaReporte;
    }

    public void setFechaReporte(LocalDateTime fechaReporte) {
        this.fechaReporte = fechaReporte;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public LocalDate getFechaProcesoPago() {
        return fechaProcesoPago;
    }

    public void setFechaProcesoPago(LocalDate fechaProcesoPago) {
        this.fechaProcesoPago = fechaProcesoPago;
    }

    public RecaudoCargo getRecaudoCargo() {
        return recaudoCargo;
    }

    public void setRecaudoCargo(RecaudoCargo recaudoCargo) {
        this.recaudoCargo = recaudoCargo;
    }

    public Cargo getCargo() {
        return this.cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public FormaPago getFormaPago() {
        return this.formaPago;
    }

    public void setFormaPago(FormaPago formaPago) {
        this.formaPago = formaPago;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoConsumo() {
        return tipoConsumo;
    }

    public void setTipoConsumo(String tipoConsumo) {
        this.tipoConsumo = tipoConsumo;
    }

    public LocalDate getFechaFacturacion() {
        return fechaFacturacion;
    }

    public void setFechaFacturacion(LocalDate fechaFacturacion) {
        this.fechaFacturacion = fechaFacturacion;
    }

    public Integer getSucursalRecaudo() {
        return sucursalRecaudo;
    }

    public void setSucursalRecaudo(Integer sucursalRecaudo) {
        this.sucursalRecaudo = sucursalRecaudo;
    }

    public Integer getCodEntidadRecaudadora() {
        return codEntidadRecaudadora;
    }

    public void setCodEntidadRecaudadora(Integer codEntidadRecaudadora) {
        this.codEntidadRecaudadora = codEntidadRecaudadora;
    }

    public HistorialRecaudoResumen getResumen() {
        return resumen;
    }

    public void setResumen(HistorialRecaudoResumen resumen) {
        this.resumen = resumen;
    }
}
