package com.colpatria.midas.model;

import com.colpatria.midas.dto.CasasDeCobranzaProbeQueryDto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Model of CLIENTE table in the database where the objects and methods are declared
 */
@Entity
@Table(name = "CLIENTE")
public class Cliente implements Serializable {

	/**
	 * Double that refers to the ID number of the client
	 */
	@Id
	@Column(name="NUMERO_IDENTIFICACION")
	private Double numeroIdentificacion;

	/**
	 * Variable TipoProducto that specifies one to one relation between CLIENTE and TipoDocumento
	 */
	@ManyToOne
	@JoinColumn(name = "TIPO_IDENTIFICACION", referencedColumnName = "CODIGO")
	private TipoDocumento tipoDocumento;

	/**
	 * String that refers to the client name
	 */
	@Column(name="NOMBRE")
	private String nombre;

	/**
	 * Variable CUENTA that specifies one to one relation between CLIENTE and CUENTA
	 */
	@OneToOne (cascade = CascadeType.ALL)
	@JoinColumn(name = "CUENTA_NUMERO", referencedColumnName = "NUMERO")
	private Cuenta cuenta;

	/**
	 * Variable thar refers to the one-to-one relation between CLIENTE and CONTRATO
	 */
	@OneToOne(mappedBy = "cliente", cascade = CascadeType.ALL)
	private Contrato contrato;

	public boolean equalsClient(Cliente cliente ) {
		return !this.numeroIdentificacion.equals(cliente.getNumeroIdentificacion());
	}

	/**
	 * Get the ID number of a class
	 * @return String that refers to the ID number of the client
	 */
	public Double getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	/**
	 * Set the ID number of a class
	 * @param numeroIdentificacion String that refers to the ID number of the client
	 */
	public void setNumeroIdentificacion(Double numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	/**
	 * Compare both objects
	 * @param cliente is the new client object to compare
	 * @return a boolean variable that tell if they are the same
	*/
	public boolean equals( Cliente cliente ) {
		if( cliente == null || !this.numeroIdentificacion.equals( cliente.getNumeroIdentificacion() )  ) {
			return false;
		}
		return true;
	}

	/**
	 * Get the name of a class
	 * @return String that refers to the client name
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Set the name of a class
	 * @param nombre String that refers to the client name
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Get the CUENTA-CLIENTE relation of a class
	 * @return Variable CUENTA that specifies one to one relation between CLIENTE and CUENTA
	 */
	public Cuenta getCuenta() {
		return cuenta;
	}

	/**
	 * Set the CUENTA-CLIENTE relation of a class
	 * @param cuenta Variable CUENTA that specifies one to one relation between CLIENTE and CUENTA
	 */
	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * Get the ID type of a class
	 * @return Integer that refers to the ID type of the client
	 */
	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Set the ID type of a class
	 * @param tipoIdentificacion Integer that refers to the ID type of the client
	 */
	public void setTipoDocumento(TipoDocumento tipoIdentificacion) {
		this.tipoDocumento = tipoIdentificacion;
	}

	/**
	 * Get the CONTRATO-CLIENTE relation of a class
	 * @return Variable CONTRATO that specifies one to one relation between CLIENTE and CONTRATO
	 */
	public Contrato getContrato() {
		return contrato;
	}

	/**
	 * Set the CONTRATO-CLIENTE relation of a class
	 * @param contrato Variable CONTRATO that specifies one to one relation between CLIENTE and CONTRATO
	 */
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
}