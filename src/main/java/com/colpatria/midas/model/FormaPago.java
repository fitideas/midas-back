package com.colpatria.midas.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model of the FORMA_PAGO table on the database where the methods and objects are declared
 */
@Entity
@Table(name = "FORMA_PAGO")
public class FormaPago implements Serializable {

    /**
     * Integer that specifies the payment method ID
     */
    @Id
	@Column(name = "ID")
    private String id;

    /**
     * Integer that specifies the payment method name
     */
    @Column(name = "NOMBRE")
    private String nombre;

    /**
     * Get the payment method ID
     * @return Integer that specifies the payment method ID
     */
    public String getId() {
        return this.id;
    }

    /**
     * Set the payment method ID
     * @param id Integer that specifies the payment method ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get tha payment method name
     * @return Integer that specifies the payment method name
     */
    public String getNombre() {
        return this.nombre;
    }

    /**
     * Set tha payment method name
     * @param nombre Integer that specifies the payment method name
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


}
