package com.colpatria.midas.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.*;

/**
 * Model of the HISTORIAL_CAMBIO_ESTADO table on the database where the methods and objects are declared
 */
@Entity
@Table(name = "HISTORIAL_CAMBIO_ESTADO")
public class HistorialCambioEstado implements Serializable {

    /**
     * Big integer that refers to the ID of a record
     */
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    /**
     * String that refers to the previous state of the transaction
     */
    @Column(name = "SERVICIO_ESTADO_ANTERIOR")
    private String servicioEstadoAnterior;

    /**
     * String that refers to the afterwards state of the transaction
     */
    @Column(name = "SERVICIO_ESTADO_NUEVO")
    private String servicioEstadoNuevo;

    /**
     * Date that refers to the transaction date
     */
    @Column(name = "FECHA")
    private LocalDate fecha;

    /**
     * String that refers to the observations of a class
     */
    @Column(name = "OBSERVACION")
    private String observacion;

    /**
     * Service variable that refers to the many-to-one relation between SERVICE and HISTORIAL_CAMBIO_ESTADO
     */
    @ManyToOne
	@JoinColumn(name="SERVICIO_NUMERO", referencedColumnName = "NUMERO")
    private Servicio servicio;

    /**
     * Usuario variable that refers to the many-to-one relation between USUARIO and HISTORIAL_CAMBIO_ESTADO
     */
    @ManyToOne
	@JoinColumn(name="USUARIO_ID", referencedColumnName = "ID")
    private Usuario usuario;

    /**
     * Get the ID of a class
     * @return Big integer that refers to the ID of a record
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Set the ID of a class
     * @param id Big integer that refers to the ID of a record
     */
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
     * Get the observation of a class
     * @return String that refers to the observations of a class
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * Set the observation of a class
     * @param observacion String that refers to the observations of a class
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * Get the SERVICE-HISTORIAL_CAMBIO_ESTADO relation of a class
     * @return Service variable that refers to the many-to-one relation between SERVICE and HISTORIAL_CAMBIO_ESTADO
     */
    public Servicio getServicio() {
        return servicio;
    }

    /**
     * Set the SERVICE-HISTORIAL_CAMBIO_ESTADO relation of a class
     * @param servicio Service variable that refers to the many-to-one relation between SERVICE and HISTORIAL_CAMBIO_ESTADO
     */
    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    /**
     * Get the previous state of the transaction of a class
     * @return String that refers to the previous state of the transaction
     */
    public String getServicioEstadoAnterior() {
        return servicioEstadoAnterior;
    }

    /**
     * Set the previous state of the transaction of a class
     * @param servicioEstadoAnterior String that refers to the previous state of the transaction
     */
    public void setServicioEstadoAnterior(String servicioEstadoAnterior) {
        this.servicioEstadoAnterior = servicioEstadoAnterior;
    }

    /**
     * Get the afterwards state of the transaction of a class
     * @return String that refers to the afterwards state of the transaction
     */
    public String getServicioEstadoNuevo() {
        return servicioEstadoNuevo;
    }

    /**
     * Set the afterwards state of the transaction of a class
     * @param servicioEstadoNuevo String that refers to the afterwards state of the transaction
     */
    public void setServicioEstadoNuevo(String servicioEstadoNuevo) {
        this.servicioEstadoNuevo = servicioEstadoNuevo;
    }

    /**
     * Get the USUARIO-HISTORIAL_CAMBIO_ESTADO relation of a class
     * @return Usuario variable that refers to the many-to-one relation between USUARIO and HISTORIAL_CAMBIO_ESTADO
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * Set the USUARIO-HISTORIAL_CAMBIO_ESTADO relation of a class
     * @param usuario Usuario variable that refers to the many-to-one relation between USUARIO and HISTORIAL_CAMBIO_ESTADO
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * Get the transaction date of a class
     * @return Date that refers to the transaction date
     */
    public LocalDate getFecha() {
        return this.fecha;
    }

    /**
     * Set the transaction date of a class
     * @param fecha Date that refers to the transaction date
     */
    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

}
