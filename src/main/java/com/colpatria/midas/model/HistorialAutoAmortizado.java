package com.colpatria.midas.model;

/*
 *
 * Librerías
 *
*/

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDate;

/*
 * Model class for 'historial auto amortizados'
*/
@Entity
@Table( name = "HISTORIAL_AUTOMORTIZADO" )
public class HistorialAutoAmortizado {

    /**
     * Identifier for 'historial auto amortizados'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    /**
     * 'servicio' associated with 'historial auto amortizados'
     */
    @ManyToOne
	@JoinColumn( name = "SERVICIO_NUMERO", referencedColumnName = "NUMERO" )
	private Servicio servicio;

    /**
     * 'numero documento' of bill associated with 'historial auto amortizados'
     */
    @Column( name = "NUMERO_DOCUMENTO_PAGADO" )
	private String numeroDocumentoPagado;

    /**
     * 'fecha ingreso' associated with 'historial auto amortizados'
     */
    @Column( name = "FECHA_INGRESO" )
	private LocalDate fechaIngreso;

    /**
     * 'fecha pago' associated with 'historial auto amortizados'
     */
    @Column( name = "FECHA_PAGO" )
	private LocalDate fechaPago;

    /**
     * 'fecha vencimiento' associated with 'historial auto amortizados'
     */
    @Column( name = "FECHA_VENCIMIENTO" )
	private LocalDate fechaVencimiento;

    /**
     * 'fecha posteo' associated with 'historial auto amortizados'
     */
    @Column( name = "FECHA_POSTEO" )
	private LocalDate fechaPosteo;

    /**
     * 'fecha efectiva' associated with 'historial auto amortizados'
     */
    @Column( name = "FECHA_EFECTIVA" )
	private LocalDate fechaEfectiva;

    /**
     * 'cargo' associated with 'historial auto amortizados'
     */
    @ManyToOne
	@JoinColumn( name = "CARGO_ID", referencedColumnName = "ID" )
	private Cargo cargo;

    /**
     * 'monto' associated with 'historial auto amortizados'
     */
    @Column( name = "MONTO" )
	private Double monto;

    /**
     * 'monto participacion' associated with 'historial auto amortizados'
     */
    @Column( name = "MONTO_PARTICIPACION" )
	private Double montoParticipacion;

    /**
     * Empty constructor
     */
    public HistorialAutoAmortizado() {
    }


    /**
     *
     * Getters & Setters
     *
     */

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public String getNumeroDocumentoPagado() {
        return numeroDocumentoPagado;
    }

    public void setNumeroDocumentoPagado(String numeroDocumentoPagado) {
        this.numeroDocumentoPagado = numeroDocumentoPagado;
    }

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public LocalDate getFechaPosteo() {
        return fechaPosteo;
    }

    public void setFechaPosteo(LocalDate fechaPosteo) {
        this.fechaPosteo = fechaPosteo;
    }

    public LocalDate getFechaEfectiva() {
        return fechaEfectiva;
    }

    public void setFechaEfectiva(LocalDate fechaEfectiva) {
        this.fechaEfectiva = fechaEfectiva;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Double getMontoParticipacion() {
        return montoParticipacion;
    }

    public void setMontoParticipacion(Double montoParticipacion) {
        this.montoParticipacion = montoParticipacion;
    } 
    
}
