package com.colpatria.midas.model;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDate;

/*
 * Model class for 'historial facturacion concepto'
*/
@Entity
@Table( name = "HISTORIAL_FACTURACION_CONCEPTO" )
public class HistorialFacturacionConcepto {

    /**
     * BigInteger that specifies the record ID.
    */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	/**
     * 'servicio' associated with 'historial facturacion concepto'
     */
    @ManyToOne
	@JoinColumn( name = "SERVICIO_NUMERO", referencedColumnName = "NUMERO" )
	private Servicio servicio;

    /**
     * 'cargo' associated with 'historial facturacion concepto'
     */
    @ManyToOne
	@JoinColumn( name = "CARGO_ID", referencedColumnName = "ID" )
	private Cargo cargo;

    /**
     * Double variable that refers to the description of 'facturacion concepto'.
    */
	@Column(name="DESCRIPCION")
	private String descripcion;

    /**
     * 'valorFactura' associated with 'historial facturacion concepto'
     */
    @Column( name = "VALOR_FACTURA" )
	private Double valorFactura;

	/**
     * LocalDate that specifies the document date.
    */
	@Column(name="FECHA_DOCUMENTO")
	private LocalDate fechaDocumento;

	/**
     * LocalDate that specifies the process date.
    */
	@Column(name="FECHA_PROCESO")
	private LocalDate fechaProceso;

    /**
     * 'totalDocumento' associated with 'historial facturacion concepto'
     */
    @Column( name = "TOTAL_DOCUMENTO" )
	private Double totalDocumento;

    /**
     * 'saldo' associated with 'historial facturacion concepto'
     */
    @Column( name = "SALDO" )
	private Double saldo;


    /**
     * Get the ID of a class
     * @return Big integer that refers to the ID of a record
     */
    public BigInteger getId() {
        return this.id;
    }

    /**
     * Set the ID of a class
     * @param id Big integer that refers to the ID of a record
     */
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
     * Get the SERVICIO for the HISTORIAL_FACTURACION_CONCEPTO
     * @return Variable thar refers to the many-to-one relation between SERVICIO and HISTORIAL_FACTURACION_CONCEPTO
     */
    public Servicio getServicio() {
        return this.servicio;
    }

    /**
     * Set the SERVICIO for the HISTORIAL_FACTURACION_CONCEPTO
     * @param servicio Variable thar refers to the many-to-one relation between SERVICIO and HISTORIAL_FACTURACION_CONCEPTO
     */
    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    /**
     * Get the CARGO for the HISTORIAL_FACTURACION_CONCEPTO
     * @return Variable thar refers to the many-to-one relation between CARGO and HISTORIAL_FACTURACION_CONCEPTO
     */
    public Cargo getCargo() {
        return this.cargo;
    }

    /**
     * Set the CARGO for the HISTORIAL_FACTURACION_CONCEPTO
     * @param cargo Variable thar refers to the many-to-one relation between CARGO and HISTORIAL_FACTURACION_CONCEPTO
     */
    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    /**
     * Get the descriptiion of 'historial facturacion concepto'
     * @return String that refers to the description of a record
     */
    public String getDescripsion() {
        return this.descripcion;
    }

    /**
     * Set the description of 'historial facturacion concepto'
     * @param descripcion String that refers to the description of a record
     */
    public void setDescripsion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Get the value of billing
     * @return Double that refers to the value of billing
     */
    public Double getValorFactura() {
        return this.valorFactura;
    }

    /**
     * Set the value of billing
     * @param valorFactura Double that refers to the value of billing
     */
    public void setValorFactura(Double valorFactura) {
        this.valorFactura = valorFactura;
    }

    /**
     * Get the document date of 'historial facturacion concepto'
     * @return Variable that contains the document date
     */
    public LocalDate getFechaDocumento() {
        return this.fechaDocumento;
    }

    /**
     * Set the document date of 'historial facturacion concepto'
     * @param fechaDocumento Variable that contains the document date
     */
    public void setFechaDocumento(LocalDate fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    /**
     * Get the process date of 'historial facturacion concepto'
     * @return Variable that contains the process date
     */
    public LocalDate getFechaProceso() {
        return this.fechaProceso;
    }

    /**
     * Set the process date of 'historial facturacion concepto'
     * @param fechaProceso Variable that contains the process date
     */
    public void setFechaProceso(LocalDate fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    /**
     * Get the document total of 'historial facturacion concepto'
     * @return Double that refers to the document total of a record
     */
    public Double getTotalDocumento() {
        return this.totalDocumento;
    }

    /**
     * Set the document total of 'historial facturacion concepto'
     * @param totalDocumento Double that refers to the document total of a record
     */
    public void setTotalDocumento(Double totalDocumento) {
        this.totalDocumento = totalDocumento;
    }

    /**
     * Get the balance of 'historial facturacion concepto'
     * @return Double that refers to the balance of a record
     */
    public Double getSaldo() {
        return this.saldo;
    }

    /**
     * Set the balance of 'historial facturacion concepto'
     * @param saldo Double that refers to the balance of a record
     */
    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }


    
}
