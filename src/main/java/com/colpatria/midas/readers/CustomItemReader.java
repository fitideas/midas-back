package com.colpatria.midas.readers;

import com.colpatria.midas.batch.DeudaCargoConfig;
import com.colpatria.midas.exceptions.IncompatibleLogException;
import com.colpatria.midas.exceptions.InvalidStructureException;
import com.colpatria.midas.model.HistorialArchivos;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.utils.JobFlagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.BufferedReaderFactory;
import org.springframework.batch.item.file.DefaultBufferedReaderFactory;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.cache.CacheManager;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.util.Arrays;


/**
 * @param <T> CustomItemReader to use in all process to validate structure
 */
public class CustomItemReader <T> extends FlatFileItemReader<T> {

    /**
     * Add logger to CustomItemReader
     */
    private static final Logger logger = LoggerFactory.getLogger(CustomItemReader.class);

    /**
     * process to validate
     */
    private final String process;

    /**
     * Columns to validate
     */
    private final String[] columns;

    /**
     * Resource with file to process
     */
    private final Resource resource;

    /**
     * Encoding to read file
     */
    private static final String ENCODING = DEFAULT_CHARSET;

    /**
     * The email service to send notification
     */
    private final EmailService emailService;

    /**
     * cache manager to use data.
     */
    private final CacheManager cacheManager;

    /**
     * File history repository
     */
    private final HistorialArchivos file;

    /**
     * buffer to read file
     */
    private static final BufferedReaderFactory bufferedReaderFactory = new DefaultBufferedReaderFactory();


    /**
     * @param columns Columns to validate
     * @param process Process to execute
     * @param resource with file
     * @param emailService to send notification
     * @param cacheManager
     * @param file is history service
     */
    public CustomItemReader(String[] columns, String process, Resource resource, EmailService emailService, CacheManager cacheManager, HistorialArchivos file) {
        this.columns = columns;
        this.emailService=emailService;
        this.resource = resource;
        this.process = process;
        this.cacheManager = cacheManager;
        this.file = file;
        setLineMapper(new DefaultLineMapper<>());
        setLinesToSkip(0);
        setName(process);
        setResource(resource);
    }

    /**
     * @throws Exception Exception with fail
     */
    @Override
    protected void doOpen() throws Exception {
        try(BufferedReader r = bufferedReaderFactory.create(this.resource, ENCODING)){
            String[] line = r.readLine().split("\\|");
            if (line.length != columns.length || !Arrays.equals(line, columns)) {
                throw new InvalidStructureException(resource.getFilename(), this.process);
            }
            long linesInFile = r.lines().count();
            if (linesInFile != this.file.getLog().longValue()){
                throw new IncompatibleLogException(resource.getFilename());
            }
        }
        JobFlagUtils.ValidateJobExecution(cacheManager, logger, this.process);
        super.doOpen();
    }
}
