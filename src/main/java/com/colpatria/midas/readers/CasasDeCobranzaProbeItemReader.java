package com.colpatria.midas.readers;

import com.colpatria.midas.dto.CasasDeCobranzaProbeQueryDto;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.HistorialCarteraLight;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.CalendarioFacturacionService;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.support.AbstractItemCountingItemStreamItemReader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Casas de cobranza probe custom item reader
 */
public class CasasDeCobranzaProbeItemReader extends AbstractItemCountingItemStreamItemReader<CasasDeCobranzaProbeQueryDto> {

    private static final Logger logger = LoggerFactory.getLogger(CasasDeCobranzaProbeItemReader.class);

    @Setter
    private HistorialCarteraLightRepository historialCarteraLightRepository;

    @Setter
    private HistorialCarteraResumenRepository historialCarteraResumenRepository;

    @Setter
    private HistorialRecaudoRepository historialRecaudoRepository;

    @Setter
    private CalendarioFacturacionRepository calendarioFacturacionRepository;

    @Setter
    private CalendarioFacturacionService calendarioFacturacionService;

    @Setter
    private HistorialRecaudoResumenRepository historialRecaudoResumenRepository;

    @Setter
    private ReclamacionRepository reclamacionRepository;

    @Setter
    private HistorialRefinanciacionRepository historialRefinanciacionRepository;

    @Setter
    private NegociacionRepository negociacionRepository;

    @Setter
    private LocalDate lastLoad;

    private List<CasasDeCobranzaProbeQueryDto> results;
    private Sort sort;
    @Setter
    private int pageSize = 10;

    private volatile int page = 0;

    private volatile int current = 0;

    private final Object lock = new Object();

    protected List<CasasDeCobranzaProbeQueryDto> doPageRead() throws Exception {
        List<CasasDeCobranzaProbeQueryDto> items = new ArrayList<>();
        Pageable pageRequest = PageRequest.of(this.page, this.pageSize, this.sort);
        Page<HistorialCarteraLight> carteras = this.historialCarteraLightRepository.getHistorialCarteraLightsByFechaReporte(this.lastLoad, pageRequest);
        for (HistorialCarteraLight cartera : carteras) {
            CasasDeCobranzaProbeQueryDto dto = new CasasDeCobranzaProbeQueryDto();
            dto.setHistorialCarteraLight(cartera);
            dto.setHistorialCarteraResumen(this.historialCarteraResumenRepository.getResumenByContratoAndFechaReporte(cartera.getServicio().getContrato(), cartera.getFechaReporte()));
            dto.setHistorialRecaudo(this.historialRecaudoRepository.findTopByRecaudoCargoServicioContrato(cartera.getServicio().getContrato()));
            dto.setCalendarioFacturacion(this.calendarioFacturacionRepository.getCalendarioFacturacionByCicloAndFechaFacturacionAfterAndVersion(cartera.getServicio().getContrato().getCliente().getCuenta().getCicloFacturacion(), cartera.getFechaReporte(), this.calendarioFacturacionService.getLastVersion()));
            if(dto.getHistorialRecaudo() != null){
                dto.setHistorialRecaudoResumen(this.historialRecaudoResumenRepository.getHistorialRecaudoResumenByContratoAndFechaPagoAndFechaProcesoPago(cartera.getServicio().getContrato(), dto.getHistorialRecaudo().getFechaPago(), dto.getHistorialRecaudo().getFechaProcesoPago()));
            }
            Cliente cliente = cartera.getServicio().getContrato().getCliente();
            dto.setReclamacion(this.reclamacionRepository.findByTipoDocumentoAndNumeroDocumentoAndVigenteTrue(cliente.getTipoDocumento(),cliente.getNumeroIdentificacion()).orElse(null));
            //dto.setNegociacion(); TODO: PENDIENTE NEGOCIACION
            dto.setNumeroPagosFechaCierre(this.historialRecaudoRepository.countByFechaReporteAfterAndRecaudoCargoServicioContrato(/*cartera.getFechaCierre().atStartOfDay()*/cartera.getFechaReporte().atStartOfDay(), cartera.getServicio().getContrato()));
            dto.setNumeroRefinanciaciones(this.historialRefinanciacionRepository.countByServicioContratoCliente(cartera.getServicio().getContrato().getCliente()));
            dto.setNumeroCarterasCierreMes(this.historialCarteraLightRepository.countHistorialCarteraLightByFechaReporteAfter(LocalDate.now().minusMonths(1).with(TemporalAdjusters.lastDayOfMonth())));
            items.add(dto);

        }
        return items;
    }

    @Nullable
    @Override
    protected CasasDeCobranzaProbeQueryDto doRead() throws Exception {
        synchronized (this.lock) {
            boolean nextPageNeeded = (this.results != null && this.current >= this.results.size());
            if (this.results == null || nextPageNeeded) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Reading page {}", this.page);
                }
                this.results = this.doPageRead();
                this.page++;
                if (this.results.isEmpty()) {
                    return null;
                }
                if (nextPageNeeded) {
                    this.current = 0;
                }
            }
            if (this.current < this.results.size()) {
                CasasDeCobranzaProbeQueryDto curLine = this.results.get(this.current);
                this.current++;
                return curLine;
            } else {
                return null;
            }
        }
    }

    @Override
    protected void doOpen() throws Exception {
    }

    @Override
    protected void doClose() throws Exception {
        synchronized (this.lock) {
            this.current = 0;
            this.page = 0;
            this.results = null;
        }
    }

    private Sort convertToSort(Map<String, Sort.Direction> sorts) {
        List<Sort.Order> sortValues = new ArrayList<>();
        for (Map.Entry<String, Sort.Direction> curSort : sorts.entrySet()) {
            sortValues.add(new Sort.Order(curSort.getValue(), curSort.getKey()));
        }
        return Sort.by(sortValues);
    }

    public void setSort(Map<String, Sort.Direction> sorts) {
        this.sort = this.convertToSort(sorts);
    }
}
