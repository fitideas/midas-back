package com.colpatria.midas.tasklets;

import com.colpatria.midas.model.HistorialArchivos;
import com.colpatria.midas.model.JobSettings;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.JobSettingsService;
import com.colpatria.midas.services.JobSchedulerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Master Tasklet
 */
public class MasterTasklet implements Tasklet {

    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(MasterTasklet.class);

    /**
     * Job Scheduler Service
     */
    private final JobSchedulerService jobSchedulerService;

    /**
     * File History Service
     */
    private final FileHistoryService fileHistoryService;

    /**
     * Job Cron Service
     */
    private final JobSettingsService jobSettingsService;


    /**
     * Constructor
     *
     * @param jobSchedulerService Job Scheduler Service Reference
     * @param fileHistoryService  File History Service Reference
     * @param jobSettingsService  Job Cron Service Reference
     */
    public MasterTasklet(JobSchedulerService jobSchedulerService, FileHistoryService fileHistoryService, JobSettingsService jobSettingsService) {
        this.jobSchedulerService = jobSchedulerService;
        this.fileHistoryService = fileHistoryService;
        this.jobSettingsService = jobSettingsService;
    }

    /**
     * Execute method for MasterTasklet
     *
     * @param stepContribution Step Contribution of current Step
     * @param chunkContext     Chunk Context
     * @return Status of Execution (Finished or Continuable)
     */
    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {
        Set<HistorialArchivos> files = new HashSet<>();
        files.addAll(this.fileHistoryService.getPendingFiles());
        files.addAll(this.fileHistoryService.getRetryFiles());
        for (HistorialArchivos file : files) {
            if (file.getLog() != null) {
                String jobName = this.getJobName(file);
                if (jobName != null) {
                    String cron = this.jobSettingsService.getCronByJobName(jobName);
                    boolean wasScheduled = this.jobSchedulerService.scheduleLoadJob(cron, jobName, file).isSuccess();
                    if (wasScheduled) {
                        this.fileHistoryService.setStatusToHistorialArchivo(file, HistorialArchivos.Status.AGENDADO);
                        log.info("The job '{}' has been scheduled", jobName);
                    } else {
                        this.fileHistoryService.setStatusToHistorialArchivo(file, HistorialArchivos.Status.ERROR);
                    }
                }
            }
        }
        return RepeatStatus.FINISHED;
    }

    /**
     * Get Job name given a HistorialArchivos
     *
     * @param file HistorialArchivo reference
     * @return Job Name
     */
    private String getJobName(HistorialArchivos file) {
        List<JobSettings> jobSettings = this.jobSettingsService.getAllJobsSettings();
        for (JobSettings settings : jobSettings) {
            Pattern p = Pattern.compile(settings.getFileNameRegex());
            Matcher m = p.matcher(file.getNombreArchivo());
            if (m.matches()) {
                return settings.getJobName();
            }
        }
        return null;
    }

}
