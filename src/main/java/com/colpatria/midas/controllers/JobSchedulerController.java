package com.colpatria.midas.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.colpatria.midas.payload.ScheduleRequest;
import com.colpatria.midas.payload.ScheduleResponse;
import com.colpatria.midas.services.JobSchedulerService;

import java.util.List;
import java.util.Objects;

/**
 * Job Scheduler Controller
 */
@Tag(name = "Job Scheduler")
@RestController
public class JobSchedulerController {

    /**
     * Job Scheduler Service
     */
    private final JobSchedulerService jobSchedulerService;

    /**
     * Constructor.
     *
     * @param jobSchedulerService Job Scheduler Service bean reference
     */
    @Autowired
    public JobSchedulerController(JobSchedulerService jobSchedulerService) {
        this.jobSchedulerService = jobSchedulerService;
    }

    /**
     * Schedule a Job
     *
     * @param scheduleRequest Schedule Request Body
     * @return Schedule Response
     */
    @PostMapping("/schedule")
    public ResponseEntity<ScheduleResponse> scheduleJob(@RequestBody ScheduleRequest scheduleRequest) {
        if (Objects.equals(scheduleRequest.getJobName(), "masterJob")) {
            ScheduleResponse response = this.jobSchedulerService.scheduleMasterJob(scheduleRequest.getCron());
            return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST).body(response);
        } return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

    /**
     * Delete a Scheduled Job
     *
     * @param scheduleRequest Job id
     * @return Schedule Response
     */
    @DeleteMapping("/schedule/{scheduleRequest}")
    public ResponseEntity<ScheduleResponse> deleteScheduledJob(@PathVariable String scheduleRequest) {
        return this.jobSchedulerService.deleteScheduledJob(scheduleRequest);
    }

    /**
     * Get all scheduled Jobs
     *
     * @return Schedule Reponse
     */
    @GetMapping("/schedule")
    public ResponseEntity<List<ScheduleResponse>> getScheduledJob() {
        return this.jobSchedulerService.getScheduledJob();
    }

}
