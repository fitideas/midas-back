package com.colpatria.midas.controllers.web.catalog;

import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.catalogue.DocumentTypeService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Document type")
@RestController
@RequestMapping("/catalogue/documentType")
public class DocumentTypeController {

    @Autowired
    private DocumentTypeService documentTypeService;

    @GetMapping
    public ResponseEntity findAllDocumentType() {
        GenericResponse response;
        try{
            response = new GenericResponse(this.documentTypeService.findAllDocumentType(), null);
            return  ResponseEntity.status(HttpStatus.OK).body(response);
        } catch( Exception exc ) {
            response = new GenericResponse(null,  exc.getMessage());
            return  ResponseEntity.status(HttpStatus.OK).body(response);
        }
    }
}
