package com.colpatria.midas.controllers.web;

import com.colpatria.midas.services.ReportGeneratorService;
import com.colpatria.midas.utils.DateUtils;
import io.micrometer.core.lang.Nullable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Objects;

/**
 * File Controller
 */
@Tag(name = "File")
@RestController
public class FileController {

    /**
     * Report Generator Service
     */
    private final ReportGeneratorService reportGeneratorService;

    /**
     * Constructor. Initialize beans
     * @param reportGeneratorService Report Generator Service
     */
    @Autowired
    public FileController(ReportGeneratorService reportGeneratorService) {
        this.reportGeneratorService = reportGeneratorService;
    }

    @GetMapping("/schedule/file")
    public ResponseEntity<Resource> download(
            @RequestParam Integer ext,
            @RequestParam Integer typeQuery,
            @Nullable
            @RequestParam String numeroContrato,
            @Nullable
            @RequestParam Double numeroSuministro,
            @Nullable
            @RequestParam String tipoDocumento,
            @Nullable
            @RequestParam Double numeroDocumento,
            @Nullable
            @RequestParam String fechaInicial,
            @Nullable
            @RequestParam String fechaFinal,
            @Nullable
            @RequestParam Integer SubProducto,
            @Nullable
            @RequestParam String numeroServicio,
            @RequestParam boolean verMas) throws IOException {
        // Variables
        Resource  resource = null;

        LocalDate localDateInicial = DateUtils.stringToLocalDate(fechaInicial);
        LocalDate localDateFinal = DateUtils.stringToLocalDate(fechaFinal);

        if(Objects.equals(ext, 1)){
            resource = this.reportGeneratorService.generateExcelReport(typeQuery, tipoDocumento, numeroDocumento, numeroContrato, numeroSuministro, localDateInicial, localDateFinal, numeroServicio, SubProducto, verMas);
        }else if(Objects.equals(ext, 2)) {
            resource = this.reportGeneratorService.generateFlatFileReport(typeQuery, tipoDocumento, numeroDocumento, numeroContrato, numeroSuministro, localDateInicial, localDateFinal, numeroServicio, SubProducto, verMas);
        }else{
            return ResponseEntity.badRequest().body(null);
        }
        if(resource == null){
            return ResponseEntity.noContent().build();
        }
        Path path = resource.getFile().toPath();
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(path))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);

    }

}
