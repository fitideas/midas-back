package com.colpatria.midas.controllers.web;

import java.io.Serializable;
import java.util.Map;
import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.FacturacionConceptoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.micrometer.core.lang.Nullable;

/**
 * Facturacion por Concepto Controller
*/
@RestController
@CrossOrigin(origins = "*")
public class FacturacionConceptoController {

    /**
     * Facturacion Concepto Service
     */
    private final FacturacionConceptoService facturacionConceptoService;

    /**
     * Class constructor
     * @param facturacionConceptoService is the service for the 'facturacion concepto'
    */
    @Autowired
    public FacturacionConceptoController (FacturacionConceptoService facturacionConceptoService){
        this.facturacionConceptoService = facturacionConceptoService;
    }

    /**
     * Get all cartera data
     * @param params is the request's params
     * @return Schedule Reponse
    */
    @GetMapping( "/facturacion" )
    @ResponseBody
    public ResponseEntity<GenericResponse<Map<String, Serializable>>> getFacturacion(
        @Nullable @RequestParam String tipoDocumento,
        @Nullable @RequestParam Double numeroDocumento,
        @Nullable @RequestParam Double numeroSuministro,
        @Nullable @RequestParam String numeroContrato,
        @Nullable @RequestParam Integer SubProducto,
        @Nullable @RequestParam String fechaInicial,
        @Nullable @RequestParam String fechaFinal,
        @Nullable @RequestParam String numeroServicio,
        @Nullable @RequestParam Integer limit,
        @Nullable @RequestParam Integer offset
    )  {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(new GenericResponse(facturacionConceptoService.getFacturacion(
                tipoDocumento, 
                numeroDocumento, 
                numeroContrato, 
                numeroSuministro, 
                SubProducto,
                fechaInicial, 
                fechaFinal,
                numeroServicio,
                limit,
                offset
                ), "succes"));
        }
        catch (InvalidQueryException exception){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GenericResponse(null, exception.getMessage()));
        }
        catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.OK).body(new GenericResponse(null, ex.getMessage()));
        }
    }
    
}
