package com.colpatria.midas.controllers.web;

import com.colpatria.midas.dto.web.UploadResponseDto;
import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.GestionCobroService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * GestionCobro Controller
 */
@RestController
@RequestMapping("gestionCobro")
public class GestionCobroController {
    /**
     * GestionCobro Service
     */
    private final GestionCobroService gestionCobroService;

    /**
     * Class constructor
     * @param gestionCobroService gestionCobro Service
     */
    public GestionCobroController(GestionCobroService gestionCobroService) {
        this.gestionCobroService = gestionCobroService;
    }

    /**
     * Endpoint for uploading GestionCobro from cyber file
     * @param file the cyber Excel file with gestion cobro
     * @return the report of the upload
     */
    @PostMapping(path="upload",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<GenericResponse<UploadResponseDto>> uploadGestionCobro(@RequestPart MultipartFile file){
        try {
            return ResponseEntity.ok(new GenericResponse<>(gestionCobroService.uploadGestionCobro(file.getInputStream()),null));
        } catch (IOException e) {
            return ResponseEntity.badRequest().body(new GenericResponse<>("Error al cargar el archivo"));
        }
    }
}
