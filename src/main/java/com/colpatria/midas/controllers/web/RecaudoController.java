package com.colpatria.midas.controllers.web;

import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.RecaudoService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * Recaudo Controller
 */
@Tag(name = "Historico Recaudo")
@RestController
@CrossOrigin(origins = "*")
public class RecaudoController {

    /**
     * Recaudo Service Bean Reference
     */
    private final RecaudoService recaudoService;

    /**
     * Contructor. Inject the Beans
     * @param recaudoService Recaudo Service Bean Reference
     */
    @Autowired
    public RecaudoController(RecaudoService recaudoService) {
        this.recaudoService = recaudoService;
    }

    /**
     * Controller. Return a list of Historial Recaudo that checks the given filter
     * @param numeroContrato Numero contrato to filter
     * @param numeroCuenta Numero cuenta to filter
     * @param tipoDocumento Tipo documento to filter
     * @param numeroDocumento numero Documento to filter
     * @param fechaInicial fecha inicial to filter
     * @param fechaFinal fecha final to filter
     * @param numeroServicio numero servicio to filter
     * @param limit limit of the query
     * @param offset offset of the query
     * @return Response Entity with  the result of the query
     */
    @GetMapping("/recaudo/historico")
    public ResponseEntity getRecaudo(
            @RequestParam(value = "numeroContrato", required = false) String numeroContrato,
            @RequestParam(value = "numeroSuministro", required = false) Double numeroCuenta,
            @RequestParam(value = "SubProducto", required = false) Integer subProducto,
            @RequestParam(value = "tipoDocumento", required = false) String tipoDocumento,
            @RequestParam(value = "numeroDocumento", required = false) Double numeroDocumento,
            @RequestParam(value = "fechaInicial", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaInicial,
            @RequestParam(value = "fechaFinal", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaFinal,
            @RequestParam(value = "numeroServicio", required = false) String numeroServicio,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "offset", required = false) Integer offset
    ) {
        try {
            Map<String, Serializable> result = this.recaudoService.findHistoricoRecaudoByPage(
                    tipoDocumento,
                    numeroDocumento,
                    numeroContrato,
                    numeroCuenta,
                    subProducto,
                    fechaInicial,
                    fechaFinal,
                    numeroServicio,
                    limit,
                    offset
            );
            return ResponseEntity.status(HttpStatus.OK).body(new GenericResponse(result, null));
        } catch (InvalidQueryException exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GenericResponse(null, exception.getMessage()));
        }
    }


}
