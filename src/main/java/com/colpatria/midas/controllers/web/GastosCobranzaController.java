package com.colpatria.midas.controllers.web;

import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.repositories.HistorialCarteraLightRepository;
import com.colpatria.midas.services.GastosCobranzaService;
import com.colpatria.midas.utils.DateUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.*;

/**
 * GastosCobranza Controller
 */
@RestController
@CrossOrigin(origins = "*")
public class GastosCobranzaController {

    /**
     * GastosCobranza Service
     */
    private final GastosCobranzaService gastoCobranzaService;

    /**
     * JobLocator bean reference
     */
    private final JobLocator jobLocator;

    /**
     * JobLauncher bean reference
     */
    private JobLauncher jobLauncher;


    /**
     * Class Contructor
     *
     * @param gastoCobranzaService dependency injection GastosCobranza Service
     * @param jobLocator Job Locator bean reference
     * @param asyncJobLauncher Async Job Launcher bean reference
     */
    @Autowired
    private GastosCobranzaController(GastosCobranzaService gastoCobranzaService, JobLocator jobLocator, JobLauncher jobLauncher) {
        this.gastoCobranzaService = gastoCobranzaService;
        this.jobLocator = jobLocator;
        this.jobLauncher = jobLauncher;
    } 

    /**
     * @return response entity with the requested data
     */
    @GetMapping("/generar-gastos-cobranza")
    public ResponseEntity scheduleJob(){
        /*ScheduleResponse response = this.gastoCobranzaService.scheduleJob("gastoCobranzaJob");
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST).body(response);*/
        try {
            Job job = this.jobLocator.getJob("gastoCobranzaJob");
            JobParameters params = new JobParametersBuilder()
                    .addDate("date", new Date())
                    .toJobParameters();
            this.jobLauncher.run(job, params);
            Integer counter = this.gastoCobranzaService.getCountRegistrosAfecto();
            return ResponseEntity.ok(new GenericResponse<>(counter, "success"));
        } catch (NoSuchJobException | JobExecutionAlreadyRunningException | JobParametersInvalidException |
                 JobInstanceAlreadyCompleteException | JobRestartException ignored) {
            return ResponseEntity.internalServerError().body(new GenericResponse<>(null, "Ha ocurrido un durante la generación de información"));
        }
        
    }

}
