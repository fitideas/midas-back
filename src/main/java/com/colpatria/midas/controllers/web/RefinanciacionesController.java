package com.colpatria.midas.controllers.web;

import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.RefinanciacionService;
import io.micrometer.core.lang.Nullable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Refinanaciaciones controller conteins endpoints to find data
 */
@RestController
@Tag(name = "Refinanciaciones")
public class RefinanciacionesController {

    /**
     * refinanciacionService
     */
    private final RefinanciacionService refinanciacionService;

    /**
     * @param refinanciacionService DI of refinanciacion Service
     */
    @Autowired
    public RefinanciacionesController(RefinanciacionService refinanciacionService) {
        this.refinanciacionService = refinanciacionService;
    }

    /**
     * Get used to find data of 'Refinanciaciones'
     *
     * @param numeroContrato  Numero de contrato in query params
     * @param numeroCuenta    Numero de cuenta in query params
     * @param tipoDocumento   Tipo de documento in query params
     * @param numeroDocumento numero de documento in query params
     * @param fechaInicial    fecha inicial in query params
     * @param fechaFinal      fecha final in query params
     * @param numeroServicio  numero de servicio in query params
     * @return
     */
    @GetMapping("/refinanciaciones")
    public ResponseEntity<GenericResponse> getRefinanciaciones(
            @Nullable
            @RequestParam String numeroContrato,
            @Nullable
            @RequestParam Double numeroSuministro,
            @Nullable
            @RequestParam String tipoDocumento,
            @Nullable
            @RequestParam Double numeroDocumento,
            @Nullable
            @RequestParam String fechaInicial,
            @Nullable
            @RequestParam String fechaFinal,
            @Nullable
            @RequestParam String numeroServicio,
            @Nullable
            @RequestParam Integer SubProducto,
            @RequestParam int limit,
            @RequestParam int offset
    ) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(new GenericResponse(refinanciacionService.getRefinanciaciones(
                    numeroContrato,
                    numeroSuministro,
                    tipoDocumento,
                    numeroDocumento,
                    fechaInicial,
                    fechaFinal,
                    numeroServicio,
                    SubProducto,
                    offset,
                    limit
            ), null));
        }
        catch (InvalidQueryException exception){
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body( new GenericResponse( null, exception.getMessage() ) );
        }
        catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.OK).body(new GenericResponse(null, ex.getMessage()));
        }
    }
}
