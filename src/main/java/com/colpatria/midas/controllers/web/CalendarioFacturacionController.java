package com.colpatria.midas.controllers.web;

import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.CalendarioFacturacionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Billing Schedule Controller
 */
@RestController
@RequestMapping("calendarioFacturacion")
public class CalendarioFacturacionController {
    /**
     * Billing Schedule Service
     */
    private final CalendarioFacturacionService calendarioFacturacionService;

    /**
     * Class constructor
     * @param calendarioFacturacionService Billing Schedule Service
     */
    public CalendarioFacturacionController(CalendarioFacturacionService calendarioFacturacionService) {
        this.calendarioFacturacionService = calendarioFacturacionService;
    }

    /**
     * Controller for Billing Schedule Upload
     *
     * @param file    the Excel file to upload
     * @param version the version of the Schedule
     * @return ok if the upload is successful
     */
    @PostMapping("upload")
    public ResponseEntity<GenericResponse<Boolean>> uploadCalendar(@RequestParam MultipartFile file,@RequestParam String version){
        if(calendarioFacturacionService.uploadNewVersion(file,version)){
            return ResponseEntity.ok(new GenericResponse<>(true,null));
        }
        return ResponseEntity.badRequest().body(null);
    }

    /**
     * Validates if a Schedule can be uploaded
     *
     * @param version the version of the Schedule
     * @return true if the schedule can be uploaded
     */
    @GetMapping("validate")
    public ResponseEntity<GenericResponse<Boolean>> validateUpload(@RequestParam String version){
        return ResponseEntity.ok(new GenericResponse<>(calendarioFacturacionService.validateUpload(version),null));
    }

    /**
     * retrieves the last version of the calendar uploaded
     * @return the last version String
     */
    @GetMapping("last")
    public ResponseEntity<GenericResponse<String>> getLastVersion(){
        return ResponseEntity.ok(new GenericResponse<>(calendarioFacturacionService.getLastVersion(),null));
    }


}
