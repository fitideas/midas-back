package com.colpatria.midas.controllers.web;

import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.repositories.HistorialCarteraLightRepository;
import com.colpatria.midas.utils.DateUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.*;

/**
 * Casas de Cobranza Controller
 */
@Tag(name = "Casas De Cobranza")
@RestController
@CrossOrigin(origins = "*")
public class CasasDeCobranzaController {

    /**
     * Historial Cartera Light bean reference
     */
    private final HistorialCarteraLightRepository historialCarteraLightRepository;


    /**
     * JobLocator bean reference
     */
    private final JobLocator jobLocator;

    /**
     * JobLauncher bean reference
     */
    private JobLauncher asyncJobLauncher;


    /**
     * Class Contructor
     *
     * @param historialCarteraLightRepository Historial Cartera Light Repository bean reference
     * @param jobLocator                      Job Locator bean reference
     * @param asyncJobLauncher                Async Job Launcher bean reference
     */
    @Autowired
    private CasasDeCobranzaController(HistorialCarteraLightRepository historialCarteraLightRepository, JobLocator jobLocator, JobLauncher asyncJobLauncher) {
        this.historialCarteraLightRepository = historialCarteraLightRepository;
        this.jobLocator = jobLocator;
        this.asyncJobLauncher = asyncJobLauncher;
    }

    @GetMapping("/getLastCarteraDate")
    public ResponseEntity<GenericResponse<LocalDate>> getLastFechaCartera() {
        String date = this.historialCarteraLightRepository.getLastLoad();
        if(date != null){
            return ResponseEntity.ok(new GenericResponse<>(DateUtils.stringToLocalDate(date),null));
        }else{
            return new ResponseEntity<>(new GenericResponse<>(null,"No se ha realizado el cargue de la cartera por primera vez"), HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping("/casas-de-cobranza/")
    public ResponseEntity generateCasasDeCobranza() {
        try {
            Job job = this.jobLocator.getJob("casasDeCobranza");
            JobParameters params = new JobParametersBuilder()
                    .addDate("date", new Date())
                    .toJobParameters();
            this.asyncJobLauncher.run(job, params);
            return ResponseEntity.ok(new GenericResponse<>(null, "Generación en curso, los resultados se mostrarán por correo electronico"));
        } catch (NoSuchJobException | JobExecutionAlreadyRunningException | JobParametersInvalidException |
                 JobInstanceAlreadyCompleteException | JobRestartException ignored) {
            return ResponseEntity.internalServerError().body(new GenericResponse<>(null, "Ha ocurrido un error disparando la generación de información"));
        }
    }

}
