package com.colpatria.midas.controllers.web;

import com.colpatria.midas.dto.web.PagedResponseDto;
import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.CambioEstadoService;
import io.micrometer.core.lang.Nullable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

/**
 *  Cambio Estado Controller
 */
@Tag(name = "Cambio Estado")
@RequestMapping("cambioEstado")
@RestController
public class CambioEstadoController {
    /**
     * Cambio Estado Service
     */
    final CambioEstadoService cambioEstadoService;

    /**
     * Class Constructor
     * @param cambioEstadoService Cambio Estado Service
     */
    public CambioEstadoController(CambioEstadoService cambioEstadoService) {
        this.cambioEstadoService = cambioEstadoService;
    }

    /**
     * Get negotiation history
     *
     * @param numeroSuministro    account number
     * @param numeroDocumento document number
     * @param numeroContrato  contract number
     * @param tipoDocumento   document type
     * @param fechaInicial    initial date
     * @param fechaFinal      final date
     * @param numeroServicio  service number
     * @param limit           limit of registers
     * @param offset          offset of registers
     * @return the negotiation history
     */
    @GetMapping
    public ResponseEntity<GenericResponse<PagedResponseDto>> getHistorico(
            @Nullable
            @RequestParam Double numeroSuministro,
            @Nullable
            @RequestParam Integer SubProducto,
            @Nullable
            @RequestParam Double numeroDocumento,
            @Nullable
            @RequestParam String numeroContrato,
            @Nullable
            @RequestParam String tipoDocumento,
            @Nullable
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaInicial,
            @Nullable
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaFinal,
            @Nullable
            @RequestParam String numeroServicio,
            @Nullable
            @RequestParam Integer limit,
            @Nullable
            @RequestParam Integer offset
    ) {
        try {
        PagedResponseDto result = cambioEstadoService.findHistoricoCambioCarteraPaged(
                tipoDocumento,
                numeroDocumento,
                numeroContrato,
                SubProducto,
                numeroSuministro,
                fechaInicial,
                fechaFinal,
                numeroServicio,
                limit,
                offset
        );
        return ResponseEntity.ok(new GenericResponse<>(result, null));
        } catch (InvalidQueryException exc) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GenericResponse<>(null, exc.getMessage()));
        }
    }

}
