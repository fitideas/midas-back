package com.colpatria.midas.controllers.web;

/*
*
* Libraries
*
*/

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Map;
import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.HistoricoCarteraService;
import com.colpatria.midas.utils.HttpParameterUtil;

/**
 * Cartera Controller
*/
@Tag(name = "Historico Cartera")
@RestController
public class HistoricoCarteraController {

    /**
     * Cartera total Service
     */
    private final HistoricoCarteraService historicoCarteraTotalService;
    
    /**
     * Class constructor.
     * @param historicoCarteraTotalService is catera total service
    */
    public HistoricoCarteraController( HistoricoCarteraService historicoCarteraTotalService ) {
        this.historicoCarteraTotalService = historicoCarteraTotalService;
    }

    /**
     * Get all cartera data
     * @param params is the request's params
     * @return Schedule Reponse
    */
    @GetMapping( "/cartera/historico" )
    public ResponseEntity getCartera( @RequestParam Map<String, Serializable> params ) {
        // Variables
        Map<String, Serializable> result;
        Double                    numeroCuenta;
        Double                    numeroDocumento;
        String                    numeroContrato;
        String                    tipoDocumento;
        LocalDate                 fechaInicial;
        LocalDate                 fechaFinal;
        String                    numeroServicio;
        Integer                   limit;
        Integer                   offset;
        // Code
        numeroContrato  = HttpParameterUtil.getString( params , "numeroContrato" );    
        numeroCuenta    = HttpParameterUtil.getDouble( params , "numeroSuministro" );
        tipoDocumento   = HttpParameterUtil.getString( params , "tipoDocumento" );
        numeroDocumento = HttpParameterUtil.getDouble( params , "numeroDocumento" );
        fechaInicial    = HttpParameterUtil.getDate( params , "fechaInicial" );
        fechaFinal      = HttpParameterUtil.getDate( params , "fechaFinal" );
        numeroServicio  = HttpParameterUtil.getString( params, "numeroServicio" );
        limit           = HttpParameterUtil.getInteger( params , "limit" );
        offset          = HttpParameterUtil.getInteger( params , "offset" );
        try {
            result = historicoCarteraTotalService.findHistoricoCarteraByPage( 
                tipoDocumento, 
                numeroDocumento, 
                numeroContrato, 
                numeroCuenta, 
                fechaInicial, 
                fechaFinal,
                numeroServicio,
                limit,
                offset 
            );
            return ResponseEntity.status( HttpStatus.OK ).body( new GenericResponse( result, null ) );            
        } catch( InvalidQueryException exc ) {
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body( new GenericResponse( null, exc.getMessage() ) );            
        }
    }

}
