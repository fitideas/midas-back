package com.colpatria.midas.controllers.web;

/*
*
* Libraries
*
*/

import com.colpatria.midas.dto.web.DeleteResponseDto;
import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.CartaMorosaService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Carta morosa Controller
*/
@Tag(name = "Carta morosa")
@RestController
@RequestMapping("cartaMorosa")
public class CartaMorosaController {

    /**
     * Carta morosa service
     */
    private final CartaMorosaService cartaMorosaService;
    
    /**
     * Class constructor.
     * @param cartaMorosaService is carta morosa service
    */
    public CartaMorosaController( CartaMorosaService cartaMorosaService ) {
        this.cartaMorosaService = cartaMorosaService;
    }
    
    /**
     * Get all cartera data
     * @param params is the request's params
     * @return Schedule Reponse
    */
    @GetMapping( "generar" )
    public ResponseEntity generateCartaMorosa( @RequestParam List<String> estados, @RequestParam int ciclo ) throws IOException {
        Resource resource;
        try {
            resource = cartaMorosaService.generateCartasMorosas( estados, ciclo );
        } catch( InvalidQueryException exc ) {
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body( new GenericResponse( null, exc.getMessage() ) );
        }
        if(resource == null){
            return ResponseEntity.noContent().build();
        }
        Path path = resource.getFile().toPath();
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(path))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    /**
     * Endpoint for deleting CartasMorosos
     * @param file plain text file with a list of CartasMorosos to delete
     * @return a report of the processed and deleted items
     */
    @DeleteMapping(path="delete",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<GenericResponse<DeleteResponseDto>> deleteCartas(@RequestPart MultipartFile file){
        try {
            return ResponseEntity.ok(new GenericResponse<>(cartaMorosaService.deleteCartas(file.getInputStream()),null));
        }catch(InvalidObjectException e){
            return ResponseEntity.badRequest().body(new GenericResponse<>(e.getMessage()));
        }catch (IOException e) {
            return ResponseEntity.badRequest().body(new GenericResponse<>("Fallo al subir el archivo"));
        }
    }
}
