package com.colpatria.midas.controllers.web;

import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.ReclamacionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Reclamación Controller
 */
@RestController()
@RequestMapping("reclamacion")
public class ReclamacionController {
    /**
     * Reclamación service
     */
    final ReclamacionService reclamacionService;

    /**
     * Class constructor
     * @param reclamacionService Reclamación service
     */
    public ReclamacionController(ReclamacionService reclamacionService) {
        this.reclamacionService = reclamacionService;
    }

    /**
     * Endpoint for uploading new reclamations
     * @param file the file with the list of reclamations
     * @return the number of reclamations uploaded
     */
    @PostMapping("upload")
    public ResponseEntity<GenericResponse<Integer>> upload(@RequestParam MultipartFile file){
        try {
            int result = reclamacionService.uploadReclamaciones(file.getInputStream());
            if( result > 0 ){
                return ResponseEntity.ok(new GenericResponse<>(result,""));
            }else {
                return ResponseEntity.badRequest().body(new GenericResponse<>(null,"Error al procesar el archivo"));
            }
        } catch (IOException ignored) {
            return ResponseEntity.badRequest().body(new GenericResponse<>(null,"Error al procesar el archivo"));
        }
    }
}
