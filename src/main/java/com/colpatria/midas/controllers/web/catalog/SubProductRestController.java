package com.colpatria.midas.controllers.web.catalog;

import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.catalogue.ProductService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Subproducts")
@RestController
@RequestMapping("/catalogue/subProduct")
public class SubProductRestController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity findAllProducts() {
        GenericResponse response;
        try{
            response = new GenericResponse(this.productService.findAllSubProducts(), null);
            return  ResponseEntity.status(HttpStatus.OK).body(response);
        } catch( Exception exc ) {
            response = new GenericResponse(null,  exc.getMessage());
            return  ResponseEntity.status(HttpStatus.OK).body(response);
        }
    }
}
