package com.colpatria.midas.controllers.web;

import com.colpatria.midas.services.ReportGetterService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;

/**
 * ReportGetter Controller
 */
@RestController
@RequestMapping("reports")
public class ReportGetterController {
    /**
     * Report getter service
     */
    private final ReportGetterService reportGetterService;

    /**
     * class constructor
     * @param reportGetterService report getter service
     */
    public ReportGetterController(ReportGetterService reportGetterService) {
        this.reportGetterService = reportGetterService;
    }

    /**
     * Endpoint for downloading a report file
     * @param name the name of the file to download
     * @return the file
     * @throws IOException if the transfer fails
     */
    @GetMapping("download")
    public ResponseEntity<Resource> download(@RequestParam String name) throws IOException {
        Resource resource = reportGetterService.getLogFile(name);
        if(resource.exists()){
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(resource.getFile().toPath()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

}
