package com.colpatria.midas.controllers.web;

/*
 *
 * Libraries
 *
*/

import com.colpatria.midas.dto.web.CarteraActualDto;
import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.utils.HttpParameterUtil;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.colpatria.midas.services.CarteraService;
import com.colpatria.midas.utils.DateUtils;

/**
 * Cartera Controller
*/

@Tag(name = "Cartera")
@RestController
@CrossOrigin(origins = "*")
public class CarteraController {

    /**
     * Cartera Service
     */
    private final CarteraService carteraService;
    
    /**
     * Class constructor.
     * @param carteraService is catera service
    */
    public CarteraController( CarteraService carteraService ) {
        this.carteraService = carteraService;
    }

    /**
     * Get all cartera data
     * @param params is the request's params
     * @return Schedule Reponse
    */
    @GetMapping("/cartera/carteraLigth")
    public ResponseEntity getCartera(@RequestParam Map<String, Serializable> params ) {
        // Variables
        Map<String, Serializable> result;
        String numeroContrato;
        Double numeroSuministro;
        String tipoDocumento;
        Double numeroDocumento;
        String numeroServicio;
        Integer subProducto;
        Integer limit;
        Integer offset;
        LocalDate fechaInicial;
        LocalDate fechaFinal;

        // Code
        numeroContrato   = HttpParameterUtil.getString( params , "numeroContrato" );
        numeroSuministro = HttpParameterUtil.getDouble( params , "numeroSuministro" );
        tipoDocumento   = HttpParameterUtil.getString( params , "tipoDocumento" );
        numeroDocumento = HttpParameterUtil.getDouble( params , "numeroDocumento" );
        fechaInicial    = HttpParameterUtil.getDate( params , "fechaInicial" );
        fechaFinal      = HttpParameterUtil.getDate( params , "fechaFinal" );
        numeroServicio  = HttpParameterUtil.getString( params, "numeroServicio" );
        subProducto     =   HttpParameterUtil.getInteger( params, "SubProducto" );
        limit           = HttpParameterUtil.getInteger( params , "limit" );
        offset          = HttpParameterUtil.getInteger( params , "offset" );
        try{
            result = this.carteraService.findAllCarteraByfilterPaged(
                    tipoDocumento,
                    numeroDocumento,
                    numeroContrato,
                    numeroSuministro,
                    numeroServicio,
                    subProducto,
                    limit,
                    offset
            );
            return ResponseEntity.status( HttpStatus.OK ).body( new GenericResponse( result, null ) );
        }catch (InvalidQueryException exception){
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body( new GenericResponse( null, exception.getMessage() ) );
        }catch (Exception ex){
            ex.printStackTrace();
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body( new GenericResponse( null, ex.getMessage() ) );
        }
    }

}
