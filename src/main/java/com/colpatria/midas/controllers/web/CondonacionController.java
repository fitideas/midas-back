package com.colpatria.midas.controllers.web;

import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.CondonacionService;
import com.colpatria.midas.utils.HttpParameterUtil;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Map;

@Tag(name = "Condonacion")
@RestController
@CrossOrigin(origins = "*")
public class CondonacionController {
    /**
     * References the Condonacion service Object
     */
    private final CondonacionService condonacionService;

    /**
     * Constructor which enables DI
     * @param condonacionService
     */
    @Autowired
    public CondonacionController(CondonacionService condonacionService){
        this.condonacionService=condonacionService;
    }

    /**
     * This method returns the ResponseEntity according to the query parameters from the fronend
     * @param params
     * @return
     */
    @GetMapping("/condonacion/condonaciones")
    public ResponseEntity<GenericResponse<Map<String, Serializable>>> getCondonacion(@RequestParam Map<String, Serializable> params ){
        // Variables
        Map<String, Serializable> result;
        String numeroContrato;
        Double numeroSuministro;
        String tipoDocumento;
        Double numeroDocumento;
        Integer subProducto;
        String numeroServicio;
        Integer limit;
        Integer offset;
        LocalDate fechaInicial;
        LocalDate fechaFinal;

        // Code
        numeroContrato   = HttpParameterUtil.getString( params , "numeroContrato" );
        numeroSuministro = HttpParameterUtil.getDouble( params , "numeroSuministro" );
        tipoDocumento   = HttpParameterUtil.getString( params , "tipoDocumento" );
        numeroDocumento = HttpParameterUtil.getDouble( params , "numeroDocumento" );
        numeroServicio =  HttpParameterUtil.getString( params , "numeroServicio" );
        fechaInicial    = HttpParameterUtil.getDate( params , "fechaInicial" );
        fechaFinal      = HttpParameterUtil.getDate( params , "fechaFinal" );
        subProducto  = HttpParameterUtil.getInteger( params, "SubProducto" );
        limit           = HttpParameterUtil.getInteger( params , "limit" );
        offset          = HttpParameterUtil.getInteger( params , "offset" );
        try{
            result = this.condonacionService.findAllCondonacionByfiterPaged(
                    tipoDocumento,
                    numeroDocumento,
                    numeroContrato,
                    numeroSuministro,
                    numeroServicio,
                    subProducto,
                    fechaInicial,
                    fechaFinal,
                    limit,
                    offset
            );
            return ResponseEntity.status( HttpStatus.OK ).body( new GenericResponse<>( result, null ) );
        }catch (InvalidQueryException exception){
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body( new GenericResponse<>( null, exception.getMessage() ) );
        }catch (Exception ex){
            ex.printStackTrace();
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body( new GenericResponse<>( null, ex.getMessage() ) );
        }
    }

}
