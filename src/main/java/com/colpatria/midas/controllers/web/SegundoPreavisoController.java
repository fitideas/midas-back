package com.colpatria.midas.controllers.web;

import com.colpatria.midas.services.ReportGeneratorService;
import com.colpatria.midas.services.SegundoPreavisoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Segundo preaviso Controller
 */
@RestController
@CrossOrigin(origins = "*")
public class SegundoPreavisoController {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(SegundoPreavisoController.class);

    /**
     * Segundo Preaviso Service Bean reference
     */
    private final SegundoPreavisoService segundoPreavisoService;

    /**
     * Report Generator Service
     */
    private final ReportGeneratorService reportGeneratorService;

    /**
     * Constructor
     *
     * @param segundoPreavisoService Segundo preaviso service bean reference
     * @param reportGeneratorService Report Generator service bean reference
     */
    @Autowired
    public SegundoPreavisoController(SegundoPreavisoService segundoPreavisoService, ReportGeneratorService reportGeneratorService) {
        this.segundoPreavisoService = segundoPreavisoService;
        this.reportGeneratorService = reportGeneratorService;
    }

    /**
     * Controller, Return Segundo Preaviso Report
     * @return Responce Entity
     */
    @GetMapping("/segundoPreaviso")
    public ResponseEntity segundoPreaviso(@RequestParam("ext") Integer ext) throws IOException{
        List<Map<String, Serializable>> list = this.segundoPreavisoService.getSegundoPreaviso();
        if (list.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        Resource resource;
        if(Objects.equals(ext, 1)){
            resource = this.reportGeneratorService.generateExcelReport(list, "segundo_preaviso");
        }else{
            resource = this.reportGeneratorService.generateFlatFileReport(list, "segundo_preaviso");
        }
        Path path = resource.getFile().toPath();
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(path))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);



    }
}
