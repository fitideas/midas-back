package com.colpatria.midas.controllers.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.HistorialCarteraResumen;
import com.colpatria.midas.payload.GenericResponse;
import com.colpatria.midas.services.TrasladoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import io.micrometer.core.lang.Nullable;

/**
 * Traslado Controller
*/
@RestController
@CrossOrigin(origins = "*")
public class TrasladoController {

    /**
     * Traslado Service
     */
    private final TrasladoService trasladoService;

    /**
     * Class constructor
     * @param trasladoService is the service for the 'traslado'
    */
    @Autowired
    public TrasladoController (TrasladoService trasladoService){
        this.trasladoService = trasladoService;
    }

    /**
     * Get all cartera data
     * @param params is the request's params
     * @return Schedule Reponse
    */
    @GetMapping( "/traslado" )
    @ResponseBody
    public ResponseEntity<GenericResponse<Map<String, Serializable>>> getTraslado(
        @Nullable @RequestParam String tipoDocumento,
        @Nullable @RequestParam Double numeroDocumento,
        @Nullable @RequestParam Double numeroSuministro,
        @Nullable @RequestParam String numeroContrato,
        @Nullable @RequestParam String fechaInicial,
        @Nullable @RequestParam String fechaFinal,
        @Nullable @RequestParam String numeroServicio,
        @Nullable @RequestParam Integer limit,
        @Nullable @RequestParam Integer offset,
        @Nullable @RequestParam Integer SubProducto
    )  {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(new GenericResponse(trasladoService.getTraslado(
                tipoDocumento, 
                numeroDocumento, 
                numeroContrato, 
                numeroSuministro, 
                SubProducto,
                fechaInicial, 
                fechaFinal,
                numeroServicio,
                limit,
                offset
                ), "succes"));
        }
        catch (InvalidQueryException exception){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GenericResponse(null, exception.getMessage()));
        }
        catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.OK).body(new GenericResponse(null, ex.getMessage()));
        }
    }

    
}
