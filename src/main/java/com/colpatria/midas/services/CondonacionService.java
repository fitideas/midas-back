package com.colpatria.midas.services;

import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.HistorialCondonacionRepository;
import com.colpatria.midas.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CondonacionService extends HistoryService{


    private final HistorialCondonacionRepository repository;

    /**
     * Class constructor
     * @param historialCondonacionRepository historialCondonacion Repository
     */
    @Autowired
    public CondonacionService(HistorialCondonacionRepository historialCondonacionRepository){
        this.repository = historialCondonacionRepository;

    }

    /**
     * Returns the Mapped HistorialCondonacion information according to the query parameters
     * @param tipoDocumento query param
     * @param numeroDocumento query param
     * @param numeroContrato query param
     * @param numeroCuenta query param
     * @param subProducto query param
     * @param limit query param
     * @param offset query param
     * @return query result paginated
     * @throws InvalidQueryException invalid pagination, document filter or date range filter
     */
    public Map<String, Serializable> findAllCondonacionByfiterPaged(String tipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroCuenta, String numeroServicio, Integer subProducto, LocalDate fechaInicial, LocalDate fechaFinal, Integer limit, Integer offset) throws InvalidQueryException{
        Pageable pageable;
        Specification<HistorialCondonacion> specification;
        Page<HistorialCondonacion> entities;
        if(limit==null||offset==null){
            throw new InvalidQueryException( "Datos de paginación inválidos" );
        }
        if ((tipoDocumento!=null && numeroDocumento==null)||(tipoDocumento==null&&numeroDocumento!=null))
            throw new InvalidQueryException( "Tipo y documento inválidos" );
        if( fechaInicial!=null &&fechaFinal!=null && fechaInicial.isAfter( fechaFinal ) ) {
            throw new InvalidQueryException( "La fecha inicial es mayor que la fecha final" );
        }
        if (tipoDocumento==null && numeroDocumento==null && numeroContrato==null && numeroCuenta==null && numeroServicio==null)
            throw new InvalidQueryException( "Criterios de búsqueda vacios" );
        pageable= PageRequest.of(offset,limit);
        specification = this.getSpecification(tipoDocumento,numeroDocumento,numeroContrato,numeroCuenta,subProducto,numeroServicio,fechaInicial,fechaFinal);
        entities = this.repository.findAll(specification,pageable);
        HashMap<String, Serializable> result =  new HashMap<>();
        result.put( "number_of_pages", entities.getTotalPages() );
        result.put( "total_elements", entities.getTotalElements() );
        result.put( "content", ( Serializable )mapData( entities.toList() ) );
        return result;
    }

    /**
     * Return all HistorialCondonacion entities according to the query parameters
     * @param tipoDocumento Query parameter
     * @param numeroDocumento Query parameter
     * @param numeroContrato Query parameter
     * @param numeroSuministro Query Parameter
     * @param subProducto Query Parameter
     * @param numeroServicio Query Parameter
     * @param fechaInicial Query Parameter
     * @param fechaFinal Query Parameter
     * @return all HistorialCondonacion entities according to the query parameters
     */
    public List<Map<String, Serializable>> findAllCondonacionByfiter(String tipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroSuministro, Integer subProducto, String numeroServicio, LocalDate fechaInicial, LocalDate fechaFinal){
        Specification<HistorialCondonacion> specification;
        specification = this.getSpecification(tipoDocumento,numeroDocumento,numeroContrato,numeroSuministro,subProducto,numeroServicio,fechaInicial,fechaFinal);
        List<HistorialCondonacion> entities = this.repository.findAll(specification);
        return entities.stream().map( this::mapEntity ).collect( Collectors.toList() );
    }

    /**
     * Returns an Example object built from query parameters
     * @param tipoDocumento query parameter
     * @param numeroDocumento query parameter
     * @param numeroContrato query parameter
     * @param numeroCuenta query parameter
     * @param subProducto query parameter
     * @return Example object built from query parameters
     */
    private Example<HistorialCondonacion> getExampleByFilter(String tipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroCuenta, String subProducto) {
        HistorialCondonacion condonacion = new HistorialCondonacion();
        Cliente cliente = new Cliente();
        Contrato contrato = new Contrato();
        Producto producto = new Producto();
        Cuenta cuenta = new Cuenta();
        if(tipoDocumento!=null){
            TipoDocumento tipoDocumento_ =  new TipoDocumento();
            tipoDocumento_.setCodigo(tipoDocumento);
            cliente.setTipoDocumento(tipoDocumento_);
            cliente.setNumeroIdentificacion(numeroDocumento);
            contrato.setCliente(cliente);
        }
        if (numeroCuenta!=null){
            cuenta.setNumero(numeroCuenta);
            cliente.setCuenta(cuenta);
            contrato.setCliente(cliente);
        }
        if (numeroContrato!=null) {
            contrato.setNumero(numeroContrato);
        }
        if (subProducto!=null){
            producto.setCodigo(subProducto);
        }
        contrato.setSubProducto(producto);
        return Example.of(condonacion);
    }

    /**
     * Private method to map from a list of HistorialCondonacion entities to a List of Mapped Data
     * @param input List of HistorialCondonacion objects
     * @return List of Mapped Data
     */
    private List<Map<String, Serializable>> mapData(
            List <HistorialCondonacion> input){
        return input.stream().map( this::mapEntity ).collect( Collectors.toList() );
    }

    /**
     * Map entity HistorialCondonacion data
     * @param item is the HistorialCondonacion Item data
     * @return a the data mapped
     */
    private LinkedHashMap<String, Serializable> mapEntity(HistorialCondonacion item/*, LocalDate fechaInicial, LocalDate fechaFinal */) {
        return new LinkedHashMap<String, Serializable>() { {
            put( "id", UuidUtil.generate() );
            put("tipo_identificacion",item.getServicio().getContrato().getCliente().getTipoDocumento().getNombre());
            put("numero_identificacion",String.format("%.0f",item.getServicio().getContrato().getCliente().getNumeroIdentificacion()));
            put("numero_servicio",item.getServicio().getNumero());
            put("numero_contrato",item.getServicio().getContrato().getNumero());
            put("subproducto",item.getServicio().getContrato().getSubProducto().getNombre());
            put("fecha_negociacion", item.getNegociacion().getFecha());
            put("fecha_aplicacion", item.getFechaAplica());
            put("fecha_rechazo",item.getFechaRechazo());
            put("fecha_limite_pago",item.getFechaVencimiento());
            put("estado_condonacion",item.getNegociacion().getEstado());
            put("usuario",item.getUsuario().getNombres());
            put("area_modificacion",item.getUsuario().getArea());
            put("tipo_condonacion",item.getTipoNegociacion().getDescripcionTipoNegociacion());
            put("capital_condonado",String.format("%.0f",item.getCapitalCondona()));
            put("interes_corriente",null); // validar
            put("interes_moratorio",null); // validar
            put("subproducto",item.getServicio().getContrato().getSubProducto().getNombre());
            put("id_negociacion",item.getNegociacion().getId());
            put("valor_pago",String.format("%.0f",item.getValorPago()));
            put("cuotas",null);//Validar
            put("Seguros",null); // Validar
            put("gastos_cobranza",null); //Validar
        } };
    }

    /**
     * Create an objet typed specification in order to create the query conditions
     * @param tipoDocumento Document type in the query
     * @param numeroDocumento Document number in the query
     * @param numeroContrato Contract number in the query
     * @param numeroCuenta Account number in the query
     * @param subproducto product code in the query
     * @param numeroServicio service number in the query
     * @param fechaInicial start date in the query
     * @param fechaFinal en date in the query
     * @return specification
     */
    private Specification<HistorialCondonacion> getSpecification(String tipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroCuenta, Integer subproducto, String numeroServicio, LocalDate fechaInicial, LocalDate fechaFinal){
        return (root, query, criteriaBuilder) ->{
            List<Predicate> predicates;
            predicates =  new ArrayList<>();
            predicates.add(historialQueryPredicates(
                    root,
                    criteriaBuilder,
                    tipoDocumento,
                    numeroDocumento,
                    subproducto,
                    numeroContrato,
                    numeroCuenta,
                    numeroServicio
            ));
            if (fechaInicial != null)
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("fechaAplica"), fechaInicial));

            if (fechaFinal != null)
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("fechaAplica"), fechaFinal));
            query.orderBy(criteriaBuilder.desc(root.get("fechaAplica")));
            Predicate[] p = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(p));
        };
    }

}
