package com.colpatria.midas.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

/**
 * Report getter service
 */
@Service
public class ReportGetterService {
    /**
     * Path of the reports folder
     */
    @Value("${midas.jobs.path.reports}")
    private String reportPath;

    /**
     * Method for retrieving a report file
     * @param name the name of the report
     * @return the file
     */
    public Resource getLogFile(String name){
        return new PathResource(reportPath + name);
    }
}
