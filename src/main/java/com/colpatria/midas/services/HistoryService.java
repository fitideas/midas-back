package com.colpatria.midas.services;

import com.colpatria.midas.model.*;
import org.hibernate.metamodel.model.domain.internal.SingularAttributeImpl;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * General History service with query specification for filters
 */
public class HistoryService {
    /**
     * Create a query specification for a history
     * @param root root pointing to the target class of the query
     * @param criteriaBuilder builder class for predicate
     * @param codigoTipoDocumento is the document type code
     * @param numeroDocumento is the document number
     * @param numeroContrato is the contract number
     * @param numeroCuenta is the account number
     * @param numeroServicio is the service number
     * @return a query specification
     */
    protected Predicate historialQueryPredicates(
            Root<?> root,
            CriteriaBuilder criteriaBuilder,
            String   codigoTipoDocumento,
            Double    numeroDocumento,
            Integer subProducto,
            String      numeroContrato,
            Double    numeroCuenta,
            String    numeroServicio
    ){
        List<Predicate> predicates = new ArrayList<>();
        Join<?, Contrato> contrato;
        Join<?, Servicio> servicio;
        // configuration depending on the class containing Contrato or Service
        if( Arrays.stream(root.getModel().getAttributes().toArray())
                .anyMatch(f -> ((SingularAttributeImpl<?, ?>) f).getName().equals("contrato"))){
            contrato = root.join( "contrato", JoinType.INNER );
            servicio = contrato.join( "servicios", JoinType.INNER );
        }else{
            servicio = root.join( "servicio", JoinType.INNER );
            contrato = servicio.join( "contrato", JoinType.INNER );
        }
        // filters
        if( numeroContrato != null ) {
            predicates.add( criteriaBuilder.equal( contrato.get( "numero" ) , numeroContrato ) );
        }
        if( numeroServicio != null ) {
            predicates.add( criteriaBuilder.equal( servicio.get( "numero" ) , numeroServicio ) );
        }
        if(subProducto!=null){
            Join<Contrato, Producto> producto = contrato.join("subProducto",JoinType.INNER);
            predicates.add( criteriaBuilder.equal( producto.get( "codigo" ) , subProducto ) );
        }
        Join<Contrato, Cliente> cliente = contrato.join( "cliente", JoinType.INNER );
        if( numeroDocumento != null && codigoTipoDocumento != null ) {
            Join<Cliente, TipoDocumento> tipoDocumento = cliente.join( "tipoDocumento", JoinType.INNER );
            predicates.add( criteriaBuilder.equal( cliente.get( "numeroIdentificacion" ) , numeroDocumento ) );
            predicates.add( criteriaBuilder.equal( tipoDocumento.get( "codigo" ) , codigoTipoDocumento ) );
        }
        if( numeroCuenta != null ) {
            Join<Cliente, Cuenta> cuenta = cliente.join( "cuenta", JoinType.INNER );
            predicates.add( criteriaBuilder.equal( cuenta.get( "numero" ) , numeroCuenta ) );
        }
        //join filter criteria
        Predicate[] p = new Predicate[predicates.size()];
        return criteriaBuilder.and( predicates.toArray( p ) );
    }
}
