package com.colpatria.midas.services;

import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.HistorialFacturacionConceptoResumen;
import com.colpatria.midas.repositories.HistorialFacturacionConceptoRepository;
import com.colpatria.midas.repositories.HistorialFacturacionConceptoResumenRepository;
import com.colpatria.midas.utils.DateUtils;
import com.colpatria.midas.utils.UuidUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.Serializable;
import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.colpatria.midas.model.*;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;

/**
 * Facturacion Concepto Service
 */
@Service
public class FacturacionConceptoService {

    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(FacturacionConceptoService.class);

    /**
     * respository of historialFacturacionResumen
     */
    private final HistorialFacturacionConceptoResumenRepository historialFacturacionConceptoResumenRepository;

    /**
     * respository of historialFacturacion
     */
    private final HistorialFacturacionConceptoRepository historialFacturacionConceptoRepository;
    
    /**
     * String constant for message 'Criterios de búsqueda vacios'
     */
    private static final String MESSAGE_CRITERIOS_VACIOS = "Criterios de búsqueda vacios";

    /**
     * @param historialFacturacionConceptoResumenRepository DI of historial of facturacionConcepto
     */
    @Autowired
    public FacturacionConceptoService(HistorialFacturacionConceptoResumenRepository historialFacturacionConceptoResumenRepository, HistorialFacturacionConceptoRepository historialFacturacionConceptoRepository){
        this.historialFacturacionConceptoResumenRepository = historialFacturacionConceptoResumenRepository;
        this.historialFacturacionConceptoRepository = historialFacturacionConceptoRepository;
    }

    /**
     * Get all 'facturacionConcepto' data
     * @param numeroContrato 'numero de contrato' to search data
     * @param numeroCuenta 'numero de cuenta' to search data
     * @param subproducto 'subproducto' to search data
     * @param tipoDocumento 'tipo de documento' to search data
     * @param numeroDocumento 'numero de documento' to search data
     * @param fechaInicial 'fecha inicial' to search data
     * @param fechaFinal 'fecha final' to search data
     * @param numeroServicio 'numero de servicio' to search data
     * @param offset 'offset' to search data
     * @param limit 'limit' to search data
     * @return the data of the object in the form of a map
     * @throws InvalidQueryException
    */
    public Map<String, Serializable> getFacturacion(
        String tipoDocumento, 
        Double numeroDocumento,
        String numeroContrato, 
        Double numeroCuenta,
        Integer subproducto,
        String fechaInicial, 
        String fechaFinal, 
        String numeroServicio, 
        Integer limit, 
        Integer offset
    ) throws InvalidQueryException {

        Map<String, Serializable> result;
        Pageable pageable;
        Specification<HistorialFacturacionConceptoResumen> specification;
        LocalDate fechaInicialDate = DateUtils.stringToLocalDate(fechaInicial);
        LocalDate fechaFinalDate = DateUtils.stringToLocalDate(fechaFinal);

        if ((tipoDocumento != null && numeroDocumento == null) || (tipoDocumento == null && numeroDocumento != null))
            throw new InvalidQueryException("Tipo y documento inválidos");

        if (limit == null || offset == null) {
            throw new InvalidQueryException("Los índices de paginación son inválidos");
        }

        if(fechaInicialDate != null && !areAnyNotNulls(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, numeroServicio)) {
            throw new InvalidQueryException(MESSAGE_CRITERIOS_VACIOS);
        }

        if(fechaFinalDate != null && !areAnyNotNulls(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, numeroServicio)) {
            throw new InvalidQueryException(MESSAGE_CRITERIOS_VACIOS);
        }

        if(fechaInicialDate != null && fechaFinalDate != null && fechaInicialDate.isAfter(fechaFinalDate)) {
            throw new InvalidQueryException("La fecha inicial es mayor que la fecha final");
        }

        if(subproducto != null && !areAnyNotNulls(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, numeroServicio)) {
            throw new InvalidQueryException(MESSAGE_CRITERIOS_VACIOS);
        }

        if(!areAnyNotNulls(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta,  numeroServicio)) {
            throw new InvalidQueryException(MESSAGE_CRITERIOS_VACIOS);
        }

        specification = createSpecificationsResumen(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, subproducto, fechaInicialDate, fechaFinalDate, numeroServicio);
        pageable = PageRequest.of(offset, limit, Sort.by("fechaProceso").descending());
        result = findByPage(specification, pageable);
        return result;

    }

    /**
     * @param numeroContrato 'numero de contrato' to search data
     * @param numeroCuenta 'numero de cuenta' to search data
     * @param tipoDocumento 'tipo de documento' to search data
     * @param numeroDocumento 'numero de documento' to search data
     * @param fechaInicial 'fecha inicial' to search data
     * @param fechaFinal 'fecha final' to search data
     * @return boolean that indicates if one or more fields are not null
     * @throws InvalidQueryException
    */
    public boolean areAnyNotNulls(
        String tipoDocumento, 
        Double numeroDocumento,
        String numeroContrato, 
        Double numeroCuenta,
        String numeroServicio
    ) {
        if(tipoDocumento != null || numeroDocumento != null || numeroContrato != null || numeroCuenta != null || numeroServicio != null){
            return true;
        }
        return false;
    }

    /**
     * Create a query specification
     * @param codigoTipoDocumento is the document type code
     * @param numeroDocumento is the document number
     * @param numeroContrato is the contract number
     * @param numeroCuenta is the account number
     * @param subproducto 'subproducto' to search data
     * @param fechaInicial is the starting date
     * @param fechaFinal is the end date
     * @param numeroServicio is the service number
     * @return a query specification
    */
    private Specification<HistorialFacturacionConceptoResumen> createSpecificationsResumen(
        String codigoTipoDocumento,
        Double numeroDocumento, 
        String numeroContrato, 
        Double numeroCuenta, 
        Integer subproducto,
        LocalDate fechaInicial, 
        LocalDate fechaFinal,
        String numeroServicio
    ) {
        return ( root, query, criteriaBuilder ) -> {

            List<Predicate> predicates;
            Join<HistorialFacturacionConceptoResumen, Contrato> contrato;
            Join<Contrato, Cliente> cliente;
            Join<Cliente, Cuenta> cuenta;
            Join<Cliente, TipoDocumento> tipoDocumento;
            Join<Contrato, Servicio> servicio;

            predicates = new ArrayList<>();
            contrato = root.join("contrato", JoinType.INNER);
            if(numeroContrato != null) {
                predicates.add(criteriaBuilder.equal(contrato.get("numero"), numeroContrato));
            }
            if( numeroServicio != null ) {
                servicio = contrato.join( "servicios", JoinType.INNER );
                predicates.add( criteriaBuilder.equal( servicio.get("numero") , numeroServicio));
            }
            cliente = contrato.join("cliente", JoinType.INNER);
            if(numeroDocumento != null && codigoTipoDocumento != null) {
                tipoDocumento = cliente.join("tipoDocumento", JoinType.INNER);
                predicates.add(criteriaBuilder.equal(cliente.get("numeroIdentificacion"), numeroDocumento));
                predicates.add(criteriaBuilder.equal(tipoDocumento.get("codigo"), codigoTipoDocumento));
            }
            if(numeroCuenta != null) {
                cuenta = cliente.join("cuenta", JoinType.INNER);
                predicates.add( criteriaBuilder.equal(cuenta.get("numero"), numeroCuenta));
            }
            if (subproducto != null) {
                predicates.add(criteriaBuilder.equal(contrato.get("subProducto"), subproducto));
            }
            if(fechaInicial != null){
                predicates.add( criteriaBuilder.greaterThanOrEqualTo(root.get("fechaProceso"), fechaInicial));
            }
            if(fechaFinal != null){
                predicates.add( criteriaBuilder.lessThanOrEqualTo(root.get("fechaProceso"), fechaFinal));
            }
            Predicate[] p = new Predicate[predicates.size()];
            return criteriaBuilder.and( predicates.toArray(p));
        };
    }

    /**
     * Create a query specification
     * @param codigoTipoDocumento is the document type code
     * @param numeroDocumento is the document number
     * @param numeroContrato is the contract number
     * @param numeroCuenta is the account number
     * @param subproducto 'subproducto' to search data
     * @param fechaInicial is the starting date
     * @param fechaFinal is the end date
     * @param numeroServicio is the service number
     * @return a query specification
    */
    private Specification<HistorialFacturacionConcepto> createSpecifications(
        String codigoTipoDocumento,
        Double numeroDocumento, 
        String numeroContrato, 
        Double numeroCuenta, 
        Integer subproducto,
        LocalDate fechaInicial, 
        LocalDate fechaFinal,
        String numeroServicio
    ) {
        return ( root, query, criteriaBuilder ) -> {

            List<Predicate> predicates;
            Join<HistorialFacturacionConcepto, Servicio> servicio;
            Join<Contrato, Cliente> cliente;
            Join<Cliente, Cuenta> cuenta;
            Join<Cliente, TipoDocumento> tipoDocumento;
            Join<Servicio, Contrato> contrato;

            predicates = new ArrayList<>();

            servicio = root.join("servicio", JoinType.INNER);
            if(numeroServicio != null) {
                predicates.add(criteriaBuilder.equal(servicio.get("numero"), numeroServicio));
            }
            if(numeroContrato != null) {
                contrato = servicio.join( "contrato", JoinType.INNER );
                predicates.add( criteriaBuilder.equal( contrato.get("numero") , numeroContrato));
            }
            cliente = servicio.join("contrato", JoinType.INNER).join("cliente", JoinType.INNER);
            if(numeroDocumento != null && codigoTipoDocumento != null) {
                tipoDocumento = cliente.join("tipoDocumento", JoinType.INNER);
                predicates.add(criteriaBuilder.equal(cliente.get("numeroIdentificacion"), numeroDocumento));
                predicates.add(criteriaBuilder.equal(tipoDocumento.get("codigo"), codigoTipoDocumento));
            }
            if(numeroCuenta != null) {
                cuenta = cliente.join("cuenta", JoinType.INNER);
                predicates.add( criteriaBuilder.equal(cuenta.get("numero"), numeroCuenta));
            }
            if (subproducto != null) {
                predicates.add(criteriaBuilder.equal(servicio.get("contrato").get("subProducto"), subproducto));
            }
            if(fechaInicial != null){
                predicates.add( criteriaBuilder.greaterThanOrEqualTo(root.get("fechaProceso"), fechaInicial));
            }
            if(fechaFinal != null){
                predicates.add( criteriaBuilder.lessThanOrEqualTo(root.get("fechaProceso"), fechaFinal));
            }
            Predicate[] p = new Predicate[predicates.size()];
            return criteriaBuilder.and( predicates.toArray(p));
        };
    }

    /**
     * Find all the data in the database
     *
     * @param specification is the query specification
     * @param pageable is the pageable object
     * @return a list with the data
     */
    private Map<String, Serializable> findByPage(Specification<HistorialFacturacionConceptoResumen> specification, Pageable pageable) {
        Page<HistorialFacturacionConceptoResumen> entities;
        entities = historialFacturacionConceptoResumenRepository.findAll(specification, pageable);
        return new HashMap<String, Serializable>() {
            {
                put("number_of_pages", entities.getTotalPages());
                put( "total_elements", entities.getTotalElements() );
                put("content", (Serializable) mapDataResumen(entities.toList()));
            }
        };
    }


    /**
     * Map database data
     * @param entities is the history summary data
     * @return a list with the data mapped
    */
    private List<Map<String,Serializable>> mapDataResumen(List<HistorialFacturacionConceptoResumen> entities) {
        return entities.stream().map(this::mapEntityResumen).collect(Collectors.toList());
    }

    /**
     * Map database data
     * @param entities is the history summary data
     * @return a list with the data mapped
    */
    private List<Map<String,Serializable>> mapData(List<HistorialFacturacionConcepto> entities) {
        return entities.stream().map(this::mapEntity).collect(Collectors.toList());
    }

    /**
     * Map entity data
     * @param entity is the history summary data
     * @return the data mapped
    */
    private LinkedHashMap<String,Serializable> mapEntityResumen(HistorialFacturacionConceptoResumen entity) {
        LinkedHashMap<String,Serializable> fields = new LinkedHashMap<>();
        fields.put("id", UuidUtil.generate());
        fields.put("tipo_documento", entity.getContrato().getCliente().getTipoDocumento().getNombre());
        fields.put("numero_identificacion", entity.getContrato().getCliente().getNumeroIdentificacion().longValue());
        fields.put("numero_suministro", entity.getContrato().getCliente().getCuenta().getNumero());
        fields.put("numero_contrato", entity.getContrato().getNumero());
        fields.put("subproducto", entity.getContrato().getSubProducto().getNombre());
        fields.put("fecha_documento", entity.getFechaDocumento());
        fields.put("fecha_contable", entity.getFechaProceso());
        fields.put("capital", Math.round(entity.getCapital()));
        fields.put("interes_corriente", Math.round(entity.getInteresCorriente()));
        fields.put("interes_mora", Math.round(entity.getInteresMora()));
        fields.put("cuota_manejo", Math.round(entity.getCuotaManejo()));
        fields.put("gastos_cobranza", Math.round(entity.getHonorarioCobranza()));
        fields.put("seguro_obligatorio", Math.round(entity.getSeguroObligatorio()));
        fields.put("seguro_voluntario", Math.round(entity.getSeguroVoluntario()));
        fields.put("nuevos_cobros", Math.round(entity.getNuevosCobros()));
        return fields;
    }

    /**
     * Map entity data
     * @param entity is the history summary data
     * @return the data mapped
    */
    private LinkedHashMap<String,Serializable> mapEntity(HistorialFacturacionConcepto entity) {
        LinkedHashMap<String,Serializable> fields = new LinkedHashMap<>();
        fields.put("id", UuidUtil.generate());
        fields.put("tipo_documento", entity.getServicio().getContrato().getCliente().getTipoDocumento().getNombre());
        fields.put("numero_identificacion", entity.getServicio().getContrato().getCliente().getNumeroIdentificacion().longValue());
        fields.put("numero_suministro", entity.getServicio().getContrato().getCliente().getCuenta().getNumero());
        fields.put("numero_contrato", entity.getServicio().getContrato().getNumero());
        fields.put("subproducto", entity.getServicio().getContrato().getSubProducto().getNombre());
        fields.put("numero_servicio", entity.getServicio().getNumero());
        fields.put("fecha_documento", entity.getFechaDocumento());
        fields.put("fecha_contable", entity.getFechaProceso());
        fields.put("valor_factura", Math.round(entity.getValorFactura()));
        fields.put("cargo_codigo", entity.getCargo().getId());
        fields.put("cargo_nombre", entity.getCargo().getNombre());
        fields.put("descripcion", entity.getDescripsion());
        return fields;
    }

    /**
     * Get refinanciaciones without pagination
     * @param numeroContrato 'numero de contrato' to search data
     * @param numeroCuenta 'numero de cuenta' to search data
     * @param subproducto 'subproducto' to search data
     * @param tipoDocumento 'tipo de documento' to search data
     * @param numeroDocumento 'numero de documento' to search data
     * @param fechaInicial 'fecha inicial' to search data
     * @param fechaFinal 'fecha final' to search data
     * @param numeroServicio 'numero de servicio' to search data
     * @return data serializable
     */
    public List<Map<String, Serializable>> getAllFacturaciones(
        String numeroContrato,
        Double numeroCuenta,
        String tipoDocumento,
        Double numeroDocumento,
        LocalDate fechaInicial,
        LocalDate fechaFinal,
        String numeroServicio, 
        Integer subproducto,
        boolean verMas
    ){
        List<Map<String, Serializable>> result;

        if(verMas){
            Specification<HistorialFacturacionConcepto> specification;
            specification = createSpecifications(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, subproducto, fechaInicial, fechaFinal, numeroServicio);
            result = findAll(specification);
        }else{
            Specification<HistorialFacturacionConceptoResumen> specification;
            specification = createSpecificationsResumen(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, subproducto, fechaInicial, fechaFinal, numeroServicio);
            result = findAllResumen(specification);
        }
        return result;
    }

    /**
     * Find all the data in the database     *
     * @param specification is the query specification
     * @return a list with the data
     */
    private List<Map<String, Serializable>> findAllResumen(Specification<HistorialFacturacionConceptoResumen> specification) {
        List<HistorialFacturacionConceptoResumen> entities;
        entities = historialFacturacionConceptoResumenRepository.findAll(specification);
        return mapDataResumen(entities);
    }

    /**
     * Find all the data in the database     *
     * @param specification is the query specification
     * @return a list with the data
     */
    private List<Map<String, Serializable>> findAll(Specification<HistorialFacturacionConcepto> specification) {
        List<HistorialFacturacionConcepto> entities;
        entities = historialFacturacionConceptoRepository.findAll(specification);
        return mapData(entities);
    }
    
}


