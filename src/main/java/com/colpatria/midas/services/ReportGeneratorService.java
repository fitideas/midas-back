package com.colpatria.midas.services;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Report Generator Service
 * Take a Query and produce a file report
 * Formats supported: xlsx and txt
 */
@Service
public class ReportGeneratorService {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ReportGeneratorService.class);

    /**
     * Path to save the generated reports
     */
    @Value("${midas.jobs.path.reports}")
    private String reportPath;

    /**
     * Field Separator
     */
    private final String flatFileSeparator = "|";

    /**
     * Historico Cartera Service bean reference
     */
    private final HistoricoCarteraService historicoCarteraService;

    /**
     * Cartera Service bean reference
     */
    private final CarteraService carteraLigthService;

    /**
     * Historico Cartera Service bean reference
     */
    private final CambioEstadoService cambioEstadoService;

    /**
     * RefinanciacionService bean reference
     */
    private final RefinanciacionService refinanciacionService;

    /**
     * RefinanciacionService bean reference
     */
    private final CondonacionService condonacionService;

    /**
     * FacturacionConceptoService bean reference
     */
    private final FacturacionConceptoService facturacionConceptoService;

    /**
     * Recaudo Service bean reference
     */
    private final RecaudoService recaudoService;

    /**
     * Traslado Service bean reference
     */
    private final TrasladoService trasladoService;

    /**
     * Segundo Preaviso Service bean reference
     */
    private final SegundoPreavisoService segundoPreavisoService;

    /**
     * Constructor. Initialize beans
     *
     * @param historicoCarteraService Historico Cartera Service Bean Reference
     * @param cambioEstadoService     Cambio Estado Service Bean Reference
     * @param refinanciacionService   Refinanciacion bean reference
     * @param segundoPreavisoService  Segundo Preaviso bean reference
     */
    @Autowired
    public ReportGeneratorService(HistoricoCarteraService historicoCarteraService, CarteraService carteraLigthService, RefinanciacionService refinanciacionService, CondonacionService condonacionService, FacturacionConceptoService facturacionConceptoService, RecaudoService recaudoService, CambioEstadoService cambioEstadoService, TrasladoService trasladoService, SegundoPreavisoService segundoPreavisoService) {
        this.historicoCarteraService = historicoCarteraService;
        this.carteraLigthService = carteraLigthService;
        this.cambioEstadoService = cambioEstadoService;
        this.refinanciacionService = refinanciacionService;
        this.condonacionService = condonacionService;
        this.facturacionConceptoService = facturacionConceptoService;
        this.recaudoService = recaudoService;
        this.trasladoService = trasladoService;
        this.segundoPreavisoService = segundoPreavisoService;
    }

    /**
     * Generate a Report in XSLX format and return the corresponding Resource reference
     *
     * @return Resource reference
     */
    public Resource generateExcelReport(Integer typeQuery, String tipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroCuenta, LocalDate fechaInicial, LocalDate fechaFinal, String numeroServicio, Integer SubProducto, boolean verMas){
        List<Map<String, Serializable>> registers = this.getRegistersByTypeQueryAndFilter(typeQuery, tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, fechaInicial, fechaFinal, numeroServicio, SubProducto, verMas);
        String filename = Paths.get(this.reportPath).resolve("typeQuery_" + typeQuery + "__" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy_hh-mm-ss")) + ".xlsx").toString();
        try (XSSFWorkbook book = new XSSFWorkbook()) {
            XSSFSheet sheet = book.createSheet();
            String[] headerLabels = registers.get(0).keySet().toArray(new String[0]);
            XSSFRow header = sheet.createRow(0);
            for (int i = 0; i < headerLabels.length; i++) {
                header.createCell(i).setCellValue(headerLabels[i]);
            }
            for (int i = 0; i < registers.size(); i++) {
                XSSFRow row = sheet.createRow(i + 1);
                String[] keys = registers.get(i).keySet().toArray(new String[0]);
                for (int j = 0; j < keys.length; j++) {
                    if (registers.get(i).get(keys[j]) != null)
                        row.createCell(j).setCellValue(registers.get(i).get(keys[j]).toString());
                    else
                        row.createCell(j).setCellValue("");
                }
            }
            FileOutputStream out = new FileOutputStream(filename);
            book.write(out);
            out.close();
            return new UrlResource(Paths.get(filename).toUri());
        } catch (IOException exception) {
            logger.error("Error saving the xslx report. {}", exception.getMessage());
            return null;
        }catch (IndexOutOfBoundsException exception){
            logger.warn("Empty report.");
            return null;
        }
    }

    /**
     * Create an Excel Report given a List of Map String, Serializable
     * @param registers List of attributes to save
     * @return File Resource
     */
    public Resource generateExcelReport(List<Map<String, Serializable>> registers, String filePrefix){
        String filename = Paths.get(this.reportPath).resolve(filePrefix + "__" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy_hh-mm-ss")) + ".xlsx").toString();
        try (XSSFWorkbook book = new XSSFWorkbook()) {
            XSSFSheet sheet = book.createSheet();
            String[] headerLabels = registers.get(0).keySet().toArray(new String[0]);
            XSSFRow header = sheet.createRow(0);
            for (int i = 0; i < headerLabels.length; i++) {
                header.createCell(i).setCellValue(headerLabels[i]);
            }
            for (int i = 0; i < registers.size(); i++) {
                XSSFRow row = sheet.createRow(i + 1);
                String[] keys = registers.get(i).keySet().toArray(new String[0]);
                for (int j = 0; j < keys.length; j++) {
                    if (registers.get(i).get(keys[j]) != null)
                        row.createCell(j).setCellValue(registers.get(i).get(keys[j]).toString());
                    else
                        row.createCell(j).setCellValue("");
                }
            }
            FileOutputStream out = new FileOutputStream(filename);
            book.write(out);
            out.close();
            return new UrlResource(Paths.get(filename).toUri());
        } catch (IOException exception) {
            logger.error("Error saving the xslx report. {}", exception.getMessage());
            return null;
        }catch (IndexOutOfBoundsException exception){
            logger.warn("Empty report.");
            return null;
        }
    }

    /**
     * Create an Excel Report given a List of Map String, Serializable and save it in a given path
     * @param registers List of attributes to save
     * @return File Resource
     */
    public Resource generateExcelReport(List<Map<String, Serializable>> registers, String filePrefix, String path){
        String filename = Paths.get(path).resolve(filePrefix + "__" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy_hh-mm-ss")) + ".xlsx").toString();
        try (XSSFWorkbook book = new XSSFWorkbook()) {
            XSSFSheet sheet = book.createSheet();
            String[] headerLabels = registers.get(0).keySet().toArray(new String[0]);
            XSSFRow header = sheet.createRow(0);
            for (int i = 0; i < headerLabels.length; i++) {
                header.createCell(i).setCellValue(headerLabels[i]);
            }
            for (int i = 0; i < registers.size(); i++) {
                XSSFRow row = sheet.createRow(i + 1);
                String[] keys = registers.get(i).keySet().toArray(new String[0]);
                for (int j = 0; j < keys.length; j++) {
                    if (registers.get(i).get(keys[j]) != null)
                        row.createCell(j).setCellValue(registers.get(i).get(keys[j]).toString());
                    else
                        row.createCell(j).setCellValue("");
                }
            }
            FileOutputStream out = new FileOutputStream(filename);
            book.write(out);
            out.close();
            return new UrlResource(Paths.get(filename).toUri());
        } catch (IOException exception) {
            logger.error("Error saving the xslx report. {}", exception.getMessage());
            return null;
        }catch (IndexOutOfBoundsException exception){
            logger.warn("Empty report.");
            return null;
        }
    }

    /**
     * Generate a Report in TXT format and return the corresponding Resource reference
     *
     * @return Resource reference
     */
    public Resource generateFlatFileReport(Integer typeQuery, String tipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroCuenta, LocalDate fechaInicial, LocalDate fechaFinal, String numeroServicio, Integer subproducto, boolean verMas){
        List<Map<String, Serializable>> registers = this.getRegistersByTypeQueryAndFilter(typeQuery, tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, fechaInicial, fechaFinal, numeroServicio, subproducto, verMas);
        String filename = Paths.get(this.reportPath).resolve("typeQuery_" + typeQuery + "__" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy_hh-mm-ss")) + ".txt").toString();
        StringBuilder builder = new StringBuilder();
        try (FileOutputStream out = new FileOutputStream(filename)) {
            String[] headerLabels = registers.get(0).keySet().toArray(new String[0]);
            for (String headerLabel : headerLabels) {
                builder.append(headerLabel);
                builder.append(this.flatFileSeparator);
            }
            builder.setLength(builder.length() - 1);
            builder.append("\n");
            for(Map<String, Serializable> register : registers){
               for(Serializable value : register.values()){
                   if (value != null)
                        builder.append(value);
                   builder.append(this.flatFileSeparator);
               }
                builder.setLength(builder.length() - 1);
                builder.append("\n");
            }
            out.write(builder.toString().getBytes(StandardCharsets.UTF_8));
            return new UrlResource(Paths.get(filename).toUri());
        } catch (IOException exception) {
            logger.error("Error saving the txt report. {}", exception.getMessage());
            return null;
        }catch (IndexOutOfBoundsException exception){
            logger.warn("Empty report.");
            return null;
        }

    }

    /**
     * Create a flat file Report given a List of Map String, Serializable
     * @param registers List of attributes to save
     * @return File Resource
     */
    public Resource generateFlatFileReport(List<Map<String, Serializable>> registers, String filePrefix){
        String filename = Paths.get(this.reportPath).resolve(filePrefix + "__" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy_hh-mm-ss")) + ".txt").toString();
        StringBuilder builder = new StringBuilder();
        try (FileOutputStream out = new FileOutputStream(filename)) {
            String[] headerLabels = registers.get(0).keySet().toArray(new String[0]);
            for (String headerLabel : headerLabels) {
                builder.append(headerLabel);
                builder.append(this.flatFileSeparator);
            }
            builder.setLength(builder.length() - 1);
            builder.append("\n");
            for (Map<String, Serializable> register : registers) {
                for (Serializable value : register.values()) {
                    if (value != null)
                        builder.append(value.toString());
                    builder.append(this.flatFileSeparator);
                }
                builder.setLength(builder.length() - 1);
                builder.append("\n");
            }
            out.write(builder.toString().getBytes(StandardCharsets.UTF_8));
            return new UrlResource(Paths.get(filename).toUri());
        } catch (IOException exception) {
            logger.error("Error saving the txt report. {}", exception.getMessage());
            return null;
        }catch (IndexOutOfBoundsException exception){
            logger.warn("Empty report.");
            return null;
        }
    }

    /**
     * Create a flat file Report given a List of Map String, Serializable and save it in a given path
     * @param registers List of attributes to save
     * @return File Resource
     */
    public Resource generateFlatFileReport(List<Map<String, Serializable>> registers, String filePrefix, String path){
        String filename = Paths.get(path).resolve(filePrefix + "__" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy_hh-mm-ss")) + ".txt").toString();
        StringBuilder builder = new StringBuilder();
        try (FileOutputStream out = new FileOutputStream(filename)) {
            String[] headerLabels = registers.get(0).keySet().toArray(new String[0]);
            for (String headerLabel : headerLabels) {
                builder.append(headerLabel);
                builder.append(this.flatFileSeparator);
            }
            builder.setLength(builder.length() - 1);
            builder.append("\n");
            for (Map<String, Serializable> register : registers) {
                for (Serializable value : register.values()) {
                    if (value != null)
                        builder.append(value.toString());
                    builder.append(this.flatFileSeparator);
                }
                builder.setLength(builder.length() - 1);
                builder.append("\n");
            }
            out.write(builder.toString().getBytes(StandardCharsets.UTF_8));
            return new UrlResource(Paths.get(filename).toUri());
        } catch (IOException exception) {
            logger.error("Error saving the txt report. {}", exception.getMessage());
            return null;
        }
    }

    private List<Map<String, Serializable>> getRegistersByTypeQueryAndFilter(Integer typeQuery, String tipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroCuenta, LocalDate fechaInicial, LocalDate fechaFinal, String numeroServicio, Integer subproducto, boolean verMas){
        switch (typeQuery){
            case 1: 
            {
                return this.historicoCarteraService.findAllHistoricoCartera(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, fechaInicial, fechaFinal, numeroServicio );
            }
            case 2: 
            {
                return this.carteraLigthService.findAllCarteraLigthByfilter(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, numeroServicio, subproducto);
            }
            case 3: {
                return this.refinanciacionService.getAllRefinanciaciones(numeroContrato, numeroCuenta, tipoDocumento, numeroDocumento, fechaInicial, fechaFinal, numeroServicio, subproducto);
            }
            case 4: {
                return this.condonacionService.findAllCondonacionByfiter(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, subproducto, numeroServicio, fechaInicial, fechaFinal);
            }
            case 5:
            {
                return this.facturacionConceptoService.getAllFacturaciones(numeroContrato, numeroCuenta, tipoDocumento, numeroDocumento, fechaInicial, fechaFinal, numeroServicio, subproducto, verMas);
            }
            case 6: 
            {
                return this.recaudoService.findAllHistoricoRecaudo(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, subproducto, fechaInicial, fechaFinal, numeroServicio, verMas);
            }
            case 7: {
                return this.cambioEstadoService.findHistoricoCambioEstado(tipoDocumento, numeroDocumento, subproducto, numeroContrato, numeroCuenta, fechaInicial, fechaFinal, numeroServicio);
            }
            case 8:
            {
                return this.trasladoService.getAllTraslados(numeroContrato, numeroCuenta, tipoDocumento, numeroDocumento, fechaInicial, fechaFinal, subproducto);
            }
            case 9: {
                return this.segundoPreavisoService.getSegundoPreaviso();
            }
            default: {
                logger.error("The type query {} is currently not supported.", typeQuery);
                return Collections.emptyList();
            }
        }
    }

}
