package com.colpatria.midas.services;

import com.colpatria.midas.model.HistorialCarteraLight;
import com.colpatria.midas.model.HistorialCarteraResumen;
import com.colpatria.midas.repositories.AttributesRepository;
import com.colpatria.midas.repositories.HistorialCarteraLightRepository;
import com.colpatria.midas.repositories.HistorialCarteraResumenRepository;
import com.colpatria.midas.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Segundo Preaviso Service
 */
@Service
public class SegundoPreavisoService {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(SegundoPreavisoService.class);

    /**
     * Historial Cartera Total Repository Bean Reference
     */
    private final HistorialCarteraResumenRepository historialCarteraResumenRepository;

    /**
     * Attribues Repository Bean Reference
     */
    private final AttributesRepository attributesRepository;


    /**
     * Contructor
     *
     * @param historialCarteraResumenRepository Historial Cartera Resumen Repository Bean Reference
     * @param attributesRepository            Attributes Repository Bean Reference
     */
    @Autowired
    public SegundoPreavisoService(HistorialCarteraResumenRepository historialCarteraResumenRepository, AttributesRepository attributesRepository) {
        this.historialCarteraResumenRepository = historialCarteraResumenRepository;
        this.attributesRepository = attributesRepository;
    }

    /**
     * Get all historial de recaudo
     *
     * @return List of Segundo preaviso
     */
    public List<Map<String, Serializable>> getSegundoPreaviso() {
        List<Map<String, Serializable>> resumenes = new ArrayList<>();
        resumenes.addAll(this.firstFilter());
        resumenes.addAll(this.secondFilter());
        return resumenes;
    }

    /**
     * Get elements corresponding to the first filter
     *
     * @return Elements of first filter
     */
    private List<Map<String, Serializable>> firstFilter() {
        Double maxFee = Double.parseDouble(this.attributesRepository.getAttributesValueByKey("SMMLV_SEGUNDO_PREAVISO")) * Double.parseDouble(this.attributesRepository.getAttributesValueByKey("SEGUNDO_PREAVISO_SMMLV_PORCENTAJE_SEGUNDO_PREAVISO"));
        int minDays = Integer.parseInt(this.attributesRepository.getAttributesValueByKey("DIAS_MORA_LIMITE_INFERIOR_SEGUNDO_PREAVISO"));
        int maxDays = Integer.parseInt(this.attributesRepository.getAttributesValueByKey("DIAS_MORA_LIMITE_SUPERIOR_SEGUNDO_PREAVISO"));
        return this.historialCarteraResumenRepository.findHistorialCarteraResumenBySumaSaldoDeudaLessThanAndMoraPrimerVencimientoMaximoBetween(maxFee, minDays, maxDays).stream().map(this::mapHistorialCarteraResumen).collect(Collectors.toList());
    }

    /**
     * Get elements corresponding to the second filter
     *
     * @return Elements of second filter
     */
    private List<Map<String, Serializable>> secondFilter() {
        List<String> lastTwoDates = this.historialCarteraResumenRepository.getLastTwoLoads();
        if (lastTwoDates.size() != 2) {
            logger.warn("Second filter not applied, there is not enough loads");
            return new ArrayList<>();
        }
        List<HistorialCarteraResumen> currentLoad = this.historialCarteraResumenRepository.getByFechaReporte(DateUtils.stringToLocalDate(lastTwoDates.get(0)));
        List<HistorialCarteraResumen> previousLoad = this.historialCarteraResumenRepository.getByFechaReporte(DateUtils.stringToLocalDate(lastTwoDates.get(1)));
        List<HistorialCarteraResumen> list = new ArrayList<>();
        for (HistorialCarteraResumen current : currentLoad) {
            for (HistorialCarteraResumen previous : previousLoad) {
                if (Objects.equals(previous.getContrato().getNumero(), current.getContrato().getNumero()) && current.getMoraPrimerVencimientoMaximo() - previous.getMoraPrimerVencimientoMaximo() > 30) {
                    list.add(current);
                }
            }
        }
        return list.stream().map(this::mapHistorialCarteraResumen).collect(Collectors.toList());
    }

    /**
     * Map Historial Cartera Resumen to output format
     *
     * @param resumen Element to map
     * @return Map with output
     */
    public Map<String, Serializable> mapHistorialCarteraResumen(HistorialCarteraResumen resumen) {
        Map<String, Serializable> map = new LinkedHashMap<>();
        map.put("tipo_documento", resumen.getContrato().getCliente().getTipoDocumento().getNombre());
        map.put("numero_documento", String.format("%.0f", resumen.getContrato().getCliente().getNumeroIdentificacion()));
        map.put("obligacion", resumen.getContrato().getNumero());
        map.put("dias_mora", resumen.getMoraPrimerVencimientoMaximo());
        map.put("capital", String.format("%.0f", resumen.getSumaSaldoCapitalPorFacturar() + resumen.getSumaCapitalImpago()));
        return map;
    }

}
