package com.colpatria.midas.services;

import org.springframework.stereotype.Service;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import com.colpatria.midas.payload.ScheduleResponse;
import com.colpatria.midas.quartz.QuartzJobLauncher;
import com.colpatria.midas.repositories.GastoCobranzaRepository;

/**
 * GastosCobranza Service
 */
@Service
public class GastosCobranzaService {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(GastosCobranzaService.class);

    /**
     * GastosCobranza Repository
     */
    private final GastoCobranzaRepository gastoCobranzaRepository;

    /**
     * Job Launcher Bean
     */
    private final JobLauncher jobLauncher;

    /**
     * Job Locator Bean
     */
    private final JobLocator jobLocator;

    /**
     * Quartz Scheduler Bean
     */
    private final Scheduler scheduler;


    /**
     * Constructor
     *
     * @param scheduler   Scheduler bean reference
     * @param jobLauncher Job Launcher bean reference
     * @param jobLocator  Job Locator bean reference
     * @param gastoCobranzaRepository DI for Gastos Cobranza Repository
     */
    @Autowired
    public GastosCobranzaService(Scheduler scheduler, JobLauncher jobLauncher, JobLocator jobLocator, GastoCobranzaRepository gastoCobranzaRepository) {
        this.scheduler = scheduler; 
        this.jobLauncher = jobLauncher; 
        this.jobLocator = jobLocator;
        this.gastoCobranzaRepository = gastoCobranzaRepository;
    }

    /**
     * Schedule a unique execution of a Load Job
     *
     * @param jobName Job Name
     * @return Schedule Response
     */
    public ScheduleResponse scheduleJob(String jobName) {
        try {
            JobDetail jobDetail = this.buildJobDetail(jobName);
            Trigger trigger = this.buildJobTrigger(jobDetail);
            this.scheduler.scheduleJob(jobDetail, trigger);
            return new ScheduleResponse(true, jobDetail.getKey().getName(), jobDetail.getKey().getGroup(), "Job " +
                    "Scheduled Successfully!");
        } catch (SchedulerException se) {
            logger.error("A Job couldn't be scheduled: ", se);
            return new ScheduleResponse(false, "Error: " + se.getMessage());
        }
    }

    /**
     * Build a Job Detail without parameters
     *
     * @param jobName Job Name
     */
    private JobDetail buildJobDetail(String jobName) {
        JobDataMap jobDataMap = new JobDataMap(); jobDataMap.put("jobName", jobName);
        jobDataMap.put("jobLauncher", this.jobLauncher); jobDataMap.put("jobLocator", this.jobLocator);
        return JobBuilder.newJob(QuartzJobLauncher.class).setJobData(jobDataMap).storeDurably().build();
    }

    /**
     * Build a Non-Repetitive Trigger for a job given a Cron expression
     * The date of execution is computed for the next Valid Time of the cron expression
     *
     * @param jobDetail Job Detail
     * @return Trigger
     */
    private Trigger buildJobTrigger(JobDetail jobDetail) {
        try {
            return TriggerBuilder.newTrigger().forJob(jobDetail).startNow().build();
        } catch (Exception e) {
            logger.error("Error found '{}'", e.getMessage()); 
            return null;
        }
    }

    /**
     * 
     * @return the count for gastos cobranza afectos
     */
    public Integer getCountRegistrosAfecto(){
        return this.gastoCobranzaRepository.getCountRegistrosAfecto();
    }
}
