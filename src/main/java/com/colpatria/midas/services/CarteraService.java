package com.colpatria.midas.services;

/*
 *
 * Libraries
 *
*/

import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.HistorialCarteraLightRepository;
import com.colpatria.midas.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Cartera Service
*/
@Service
public class CarteraService {

    /**
     * LocalDate which refers to the starting date.
    */
    private LocalDate fechaInicial;

    /**
     * LocalDate which refers to the end date.
    */
    private LocalDate fechaFinal;

    /**
     * Refers to the  HistorialCarteraLightRepository used to access data
     */
    @Autowired
    private HistorialCarteraLightRepository myRepository;

    /**
     * Class constructor.
    */
    public CarteraService(){
    }

    /**
     * Set the starting date.
     * @param fechaInicial is the starting date.
    */
    public void setFechaInicial(LocalDate fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    /**
     * Set the end date.
     * @param fechaFinal is the end date.
    */
    public void setFechaFinal(LocalDate fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    /**
     * Private method to map from HistorialCarteraLiggth to CarteraActualDto
     * @param input List of HistorialCarteraLight objects
     * @return
     */
    private  List<Map<String, Serializable>> mapData(
            List <HistorialCarteraLight> input){
        return input.stream().map( this::mapEntity ).collect( Collectors.toList() );
    }

    /**
     * public method to find all carteraLight using filters ang pagination
     * @param tipoDocumento refers to the tipoDocumento Filter. Used with numerodocumento
     * @param numeroDocumento refers to the numeroDocumento Filter. Used with tipoDocumento
     * @param numeroContrato refers to the numeroContrato Filter.
     * @param numeroSuministro refers to the numeroSuministro Filter.
     * @param numeroServicio refers to the numeroServicio Filter.
     * @param limit referts to the number of records per page
     * @param offset refers to the current page to be obtained
     * @return
     * @throws InvalidQueryException
     */
    public Map<String, Serializable> findAllCarteraByfilterPaged(String tipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroSuministro, String numeroServicio, Integer subProducto, Integer limit, Integer offset) throws InvalidQueryException {
        Map<String, Serializable> result;
        Pageable pageable;
        Example<HistorialCarteraLight> example;
        Page<HistorialCarteraLight> entities;
        if (limit == null || offset == null){
            throw new InvalidQueryException( "Datos de paginación inválidos" );
        }
        if ((tipoDocumento!=null && numeroDocumento==null)||(tipoDocumento==null&&numeroDocumento!=null))
            throw new InvalidQueryException( "Tipo y documento inválidos" );
        if (tipoDocumento==null && numeroDocumento==null && numeroContrato==null && numeroSuministro==null && numeroServicio==null)
            throw new InvalidQueryException( "Criterios de búsqueda vacios" );

        pageable=PageRequest.of(offset.intValue(), limit.intValue());
        example = this.getExampleByFilter(tipoDocumento,numeroDocumento,numeroContrato,numeroSuministro,numeroServicio, subProducto);
        entities = this.myRepository.findAll(example,pageable);
        return new HashMap<String, Serializable>() { {
            put( "number_of_pages", entities.getTotalPages() );
            put( "total_elements", entities.getTotalElements() );
            put( "content", ( Serializable )mapData( entities.toList() ) );
        } };
    }

    /**
     * Returns an Example object built from query parameters
     * @param tipoDocumento query parameter
     * @param numeroDocumento query parameter
     * @param numeroContrato query parameter
     * @param numeroCuenta query parameter
     * @param numeroServicio query parameter
     * @return
     */
    private Example<HistorialCarteraLight> getExampleByFilter(
            String   tipoDocumento,
            Double    numeroDocumento,
            String      numeroContrato,
            Double    numeroCuenta,
            String    numeroServicio,
            Integer subProducto

    ) {
        HistorialCarteraLight historial = new HistorialCarteraLight();
        Cliente cliente = new Cliente();
        Contrato contrato = new Contrato();
        Servicio servicio = new Servicio();
        Cuenta cuenta = new Cuenta();
        if(tipoDocumento!=null){
            TipoDocumento tipoDocumento_ =  new TipoDocumento();
            tipoDocumento_.setCodigo(tipoDocumento);
            cliente.setTipoDocumento(tipoDocumento_);
            cliente.setNumeroIdentificacion(numeroDocumento);
            contrato.setCliente(cliente);
        }
        if (numeroCuenta!=null){
            cuenta.setNumero(numeroCuenta);
            cliente.setCuenta(cuenta);
            contrato.setCliente(cliente);
        }
        if (numeroContrato!=null) {
            contrato.setNumero(numeroContrato);
        }
        if (numeroServicio!=null){
            servicio.setNumero(numeroServicio);
        }
        if (subProducto!=null){
            Producto producto = new Producto();
            producto.setCodigo(subProducto.toString());
            contrato.setSubProducto(producto);
        }
        servicio.setContrato(contrato);
        historial.setServicio(servicio);
        return Example.of(historial);
    }

    /**
     * public method to find all carteraLight using filters ang NO pagination
     * @param tipoDocumento refers to the tipoDocumento Filter. Used with numerodocumento
     * @param numeroDocumento refers to the numeroDocumento Filter. Used with tipoDocumento
     * @param numeroContrato refers to the numeroContrato Filter.
     * @param numeroCuenta refers to the numeroSuministro/numeroCuenta Filter.
     * @param numeroServicio refers to the numeroServicio Filter.
     * @return
     */
    public List<Map<String, Serializable>> findAllCarteraLigthByfilter(String tipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroCuenta, String numeroServicio, Integer subProducto) {
        Example<HistorialCarteraLight> example;
        example = this.getExampleByFilter(tipoDocumento,numeroDocumento,numeroContrato,numeroCuenta,numeroServicio, subProducto);
        List<HistorialCarteraLight> entities = this.myRepository.findAll(example);
        return entities.stream().map( this::mapEntity ).collect( Collectors.toList() );
    }

    /**
     * Map entity HistorialCarteraLight data
     * @param item is the HistorialCarteraLigth Item data
     * @return a the data mapped
     */
    private LinkedHashMap<String, Serializable> mapEntity(HistorialCarteraLight item ) {
        return new LinkedHashMap<String, Serializable>() { {
            put( "id", UuidUtil.generate() );
            put("tipo_identificacion",item.getServicio().getContrato().getCliente().getTipoDocumento().getNombre());
            put("numero_identificacion",String.format("%.0f",item.getServicio().getContrato().getCliente().getNumeroIdentificacion()));
            put("numero_servicio",item.getServicio().getNumero());
            put("numero_contrato",item.getServicio().getContrato().getNumero());
            put("subproducto",item.getServicio().getContrato().getSubProducto().getNombre());
            put("nombre_cliente", item.getServicio().getContrato().getCliente().getNombre());
            put("intereses_corrientes", String.format("%,.2f", item.getTotalInteresCorrienteCausado()));
            put("intereses_mora",String.format("%,.2f", item.getInteresMora()));
            put("cuota_manejo",String.format("%,.2f", Double.valueOf(35000)));
            put("gastos_cobranzas",String.format("%,.2f", Double.valueOf(1500000)));
            put("seguros",String.format("%,.2f", Double.valueOf(250000)));
            put("numero_compra",item.getServicio().getNumero());
            put("nuevos_cobros",String.format("%,.2f", Double.valueOf(3500000)));
            put("dias_mora",item.getDiasAtraso());
            put("estado_compra",item.getServicio().getEstado().getNombre());
        } };
    }
}
