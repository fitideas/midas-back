package com.colpatria.midas.services;

import com.colpatria.midas.dto.web.UploadResponseDto;
import com.colpatria.midas.model.GestionCobro;
import com.colpatria.midas.repositories.*;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.StreamSupport;

/**
 * GestionCobro Service
 */
@Service
public class GestionCobroService {
    /**
     * GestionCobro Repository
     */
    private final GestionCobroRepository gestionCobroRepository;
    /**
     * GestionCobroAction Repository
     */
    private final GestionCobroActionRepository gestionCobroActionRepository;
    /**
     * GestionCobroResponse Repository
     */
    private final GestionCobroResponseRepository gestionCobroResponseRepository;
    /**
     * Attributes Repository
     */
    private final AttributesRepository attributesRepository;
    /**
     * Cliente Repository
     */
    private final ClienteRepository clienteRepository;
    /**
     * Contrato Repository
     */
    private final ContratoRepository contratoRepository;
    /**
     * Path to save the generated reports
     */
    @Value("${midas.jobs.path.reports}")
    private String reportPath;

    /**
     * Class constructor
     * @param gestionCobroRepository gestionCobro Repository
     * @param gestionCobroActionRepository gestionCobroAction Repository
     * @param gestionCobroResponseRepository gestionCobroResponse Repository
     * @param attributesRepository attributes Repository
     * @param clienteRepository cliente Repository
     * @param contratoRepository contrato Repository
     */
    public GestionCobroService(GestionCobroRepository gestionCobroRepository,
                               GestionCobroActionRepository gestionCobroActionRepository,
                               GestionCobroResponseRepository gestionCobroResponseRepository,
                               AttributesRepository attributesRepository,
                               ClienteRepository clienteRepository,
                               ContratoRepository contratoRepository) {
        this.gestionCobroRepository = gestionCobroRepository;
        this.gestionCobroActionRepository = gestionCobroActionRepository;
        this.gestionCobroResponseRepository = gestionCobroResponseRepository;
        this.attributesRepository = attributesRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository = contratoRepository;
    }

    /**
     * Method for uploading GestionCobro from cyber
     * @param file the file to upload
     * @return the report of the upload
     * @throws IOException if there is an error with the stream
     */
    public UploadResponseDto uploadGestionCobro(InputStream file) throws IOException {
        String campoTipoDocumento = attributesRepository.getAttributesValueByKey("CAMPO_TIPO_DOCUMENTO_GESTION_COBRO");
        UploadResponseDto result = new UploadResponseDto(0,0,0,null);
        Map<String, Integer> columns = new HashMap<>();
        List<String> errors= new ArrayList<>();
        String headers = "NUMERO_FILA|ERROR";
        errors.add(headers);
        try (Workbook workbook = WorkbookFactory.create(file)){
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> it = sheet.rowIterator();
            it.next().forEach(cell-> columns.put(cell.getStringCellValue(), cell.getColumnIndex()));
            it.forEachRemaining(row->{
                if(StreamSupport.stream(row.spliterator(), false).count() <2) return;
                DataFormatter formatter = new DataFormatter();
                try{
                    gestionCobroRepository.save( new GestionCobro(
                            null,
                            row.getCell(columns.get("HISTORY_DATE")).getLocalDateTimeCellValue(),
                            clienteRepository.getByNumeroIdentificacionAndTipoDocumentoHomologacionCyber(
                                    row.getCell(columns.get("IDENTITY_CODE")).getNumericCellValue(),
                                    formatter.formatCellValue(row.getCell(columns.get(campoTipoDocumento)))
                            ).orElseThrow(NullPointerException::new),
                            gestionCobroActionRepository.findById(row.getCell(columns.get("ACTION_ID")).getStringCellValue()).orElseThrow(NoSuchFieldError::new),
                            gestionCobroResponseRepository.findById(row.getCell(columns.get("RESPONSE_ID")).getStringCellValue()).orElseThrow(IllegalArgumentException::new),
                            formatter.formatCellValue(row.getCell(columns.get("DESCRIP"))),
                            formatter.formatCellValue(row.getCell(columns.get("TELEFONO"))),
                            formatter.formatCellValue(row.getCell(columns.get("USUARIO"))),
                            formatter.formatCellValue(row.getCell(columns.get("GRUPO"))),
                            contratoRepository.findById(formatter.formatCellValue(row.getCell(columns.get("CUENTA")))).orElseThrow(NoSuchFieldException::new),
                            formatter.formatCellValue(row.getCell(columns.get("DMAGENCY"))),
                            formatter.formatCellValue(row.getCell(columns.get("OBSERVATIONS")))
                    ));
                    result.increaseCargados();
                }catch (DataIntegrityViolationException e){
                    result.increaseDuplicados();
                    errors.add((row.getRowNum()+1)+"|Registro duplicado|");
                }catch (NullPointerException ignored){
                    result.increaseFallidos();
                    errors.add((row.getRowNum()+1)+"|Cliente invalido|");
                }catch (NoSuchFieldException ignored){
                    result.increaseFallidos();
                    errors.add((row.getRowNum()+1)+"|Cuenta invalida|");
                }catch (NoSuchFieldError ignored){
                    result.increaseFallidos();
                    errors.add((row.getRowNum()+1)+"|Campo ACTION_ID invalido|");
                }catch (IllegalArgumentException e){
                    result.increaseFallidos();
                    errors.add((row.getRowNum()+1)+"|Campo RESPONSE_ID invalido|");
                }
            });
            if(result.getDuplicados()>0||result.getFallidos()>0){
                String fileName = "Log_gestion_cobro_errors_"+ LocalDateTime.now().toString().replace(":","-")+".txt";
                try(BufferedWriter writer =new BufferedWriter(new FileWriter(reportPath+fileName))) {
                    for(String line:errors){
                        writer.write(line);
                        writer.newLine();
                    }
                }
                result.setArchivoLog(fileName);
            }
        }
        return result;
    }
}
