package com.colpatria.midas.services;

import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.HistorialTraslado;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.HistorialCarteraResumenRepository;
import com.colpatria.midas.repositories.HistorialTrasladoRepository;
import com.colpatria.midas.utils.DateUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.colpatria.midas.model.*;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import com.colpatria.midas.model.HistorialFacturacionConceptoResumen;
import com.colpatria.midas.utils.UuidUtil;
import java.util.LinkedHashMap;
import org.springframework.data.domain.Sort;

/**
 * Traslado Service
 */
@Service
public class TrasladoService {

    /**
     * respository of historialTraslado
     */
    private final HistorialTrasladoRepository historialTrasladoRepository;

    /**
     * respository of HistorialCarteraResumen
     */
    private final HistorialCarteraResumenRepository historialCarteraResumenRepository;

    private final ContratoRepository contratoRepository;

    /**
     * String constant for message 'Criterios de búsqueda vacios'
     */
    private static final String MESSAGE_CRITERIOS_VACIOS = "Criterios de búsqueda vacios";

    /**
     * @param historialTrasladoRepository DI of historial of traslados
     */
    @Autowired
    public TrasladoService(HistorialTrasladoRepository historialTrasladoRepository, HistorialCarteraResumenRepository historialCarteraResumenRepository, ContratoRepository contratoRepository){
        this.historialTrasladoRepository = historialTrasladoRepository;
        this.historialCarteraResumenRepository = historialCarteraResumenRepository;
        this.contratoRepository = contratoRepository;
    }

    /**
     * Get all 'traslados' data
     * @param numeroContrato 'numero de contrato' to search data
     * @param numeroCuenta 'numero de cuenta' to search data
     * @param tipoDocumento 'tipo de documento' to search data
     * @param numeroDocumento 'numero de documento' to search data
     * @param fechaInicial 'fecha inicial' to search data
     * @param fechaFinal 'fecha final' to search data
     * @param numeroServicio 'numero de servicio' to search data
     * @param offset 'offset' to search data
     * @param limit 'limit' to search data
     * @return the data of the object in the form of a map
     * @throws InvalidQueryException
    */
    public Map<String, Serializable> getTraslado(
        String tipoDocumento, 
        Double numeroDocumento,
        String numeroContrato, 
        Double numeroCuenta,
        Integer subproducto,
        String fechaInicial, 
        String fechaFinal, 
        String numeroServicio, 
        Integer limit, 
        Integer offset
    ) throws InvalidQueryException {

        Map<String, Serializable> result;
        Pageable pageable;
        Specification<HistorialTraslado> specification;
        LocalDate fechaInicialDate = DateUtils.stringToLocalDate(fechaInicial);
        LocalDate fechaFinalDate = DateUtils.stringToLocalDate(fechaFinal);

        if ((tipoDocumento != null && numeroDocumento == null) || (tipoDocumento == null && numeroDocumento != null))
            throw new InvalidQueryException("Tipo y documento inválidos");

        if (limit == null || offset == null) {
            throw new InvalidQueryException("Los índices de paginación son inválidos");
        }

        if(fechaInicialDate != null && !areAnyNotNulls(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta)) {
            throw new InvalidQueryException(MESSAGE_CRITERIOS_VACIOS);
        }

        if(fechaFinalDate != null && !areAnyNotNulls(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta)) {
            throw new InvalidQueryException(MESSAGE_CRITERIOS_VACIOS);
        }

        if(fechaInicialDate != null && fechaFinalDate != null && fechaInicialDate.isAfter(fechaFinalDate)) {
            throw new InvalidQueryException("La fecha inicial es mayor que la fecha final");
        }

        if(subproducto != null && !areAnyNotNulls(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta)) {
            throw new InvalidQueryException(MESSAGE_CRITERIOS_VACIOS);
        }

        if(numeroServicio != null && !areAnyNotNulls(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta)) {
            throw new InvalidQueryException("Servicio no disponible para la consulta");
        }

        if(!areAnyNotNulls(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta)) {
            throw new InvalidQueryException(MESSAGE_CRITERIOS_VACIOS);
        }

        specification = createSpecifications(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, subproducto, fechaInicialDate, fechaFinalDate);
        pageable = PageRequest.of(offset, limit, Sort.by("fecha").descending());
        result = findByPage(specification, pageable);
        return result;

    }

    /**
     * @param numeroContrato 'numero de contrato' to search data
     * @param numeroCuenta 'numero de cuenta' to search data
     * @param tipoDocumento 'tipo de documento' to search data
     * @param numeroDocumento 'numero de documento' to search data
     * @param fechaInicial 'fecha inicial' to search data
     * @param fechaFinal 'fecha final' to search data
     * @return boolean that indicates if one or more fields are not null
     * @throws InvalidQueryException
    */
    public boolean areAnyNotNulls(
        String tipoDocumento, 
        Double numeroDocumento,
        String numeroContrato, 
        Double numeroCuenta
    ) {
        if(tipoDocumento != null || numeroDocumento != null || numeroContrato != null || numeroCuenta != null){
            return true;
        }
        return false;
    }

    /**
     * Get all 'facturacionConcepto' data
     * @param numeroContrato 'numero de contrato' to search data
     * @param numeroCuenta 'numero de cuenta' to search data
     * @param tipoDocumento 'tipo de documento' to search data
     * @param numeroDocumento 'numero de documento' to search data
     * @param fechaInicial 'fecha inicial' to search data
     * @param fechaFinal 'fecha final' to search data
     * @return boolean that indicates if one or more fields are not null
     * @throws InvalidQueryException
    */
    public boolean areAnyNotNulls(
        String tipoDocumento, 
        Double numeroDocumento,
        String numeroContrato, 
        Double numeroCuenta,
        LocalDate fechaInicial, 
        LocalDate fechaFinal
    ) {
        if(tipoDocumento != null || numeroDocumento != null || numeroContrato != null || numeroCuenta != null || fechaInicial != null || fechaFinal != null){
            return true;
        }
        return false;
    }

    /**
     * Create a query specification
     * @param codigoTipoDocumento is the document type code
     * @param numeroDocumento is the document number
     * @param numeroContrato is the contract number
     * @param numeroCuenta is the account number
     * @param subproducto 'subproducto' to search data
     * @param fechaInicial is the starting date
     * @param fechaFinal is the end date
     * @return a query specification
    */
    private Specification<HistorialTraslado> createSpecifications(
        String codigoTipoDocumento,
        Double numeroDocumento, 
        String numeroContrato, 
        Double numeroCuenta, 
        Integer subproducto,
        LocalDate fechaInicial, 
        LocalDate fechaFinal
    ) {
        return ( root, query, criteriaBuilder ) -> {

            List<Predicate> predicates;
            Join<HistorialFacturacionConceptoResumen, Contrato> contrato;
            Join<Contrato, Cliente> cliente;
            Join<Cliente, Cuenta> cuenta;
            Join<Cliente, TipoDocumento> tipoDocumento;

            predicates = new ArrayList<>();
            contrato = root.join("contrato", JoinType.INNER);
            if(numeroContrato != null) {
                predicates.add(criteriaBuilder.equal(contrato.get("numero"), numeroContrato));
            }
            cliente = contrato.join("cliente", JoinType.INNER);
            if(numeroDocumento != null && codigoTipoDocumento != null) {
                tipoDocumento = cliente.join("tipoDocumento", JoinType.INNER);
                predicates.add(criteriaBuilder.equal(cliente.get("numeroIdentificacion"), numeroDocumento));
                predicates.add(criteriaBuilder.equal(tipoDocumento.get("codigo"), codigoTipoDocumento));
            }
            if(numeroCuenta != null) {
                cuenta = cliente.join("cuenta", JoinType.INNER);
                predicates.add( criteriaBuilder.equal(cuenta.get("numero"), numeroCuenta));
            }
            if (subproducto != null) {
                predicates.add(criteriaBuilder.equal(contrato.get("subProducto"), subproducto));
            }
            if(fechaInicial != null){
                predicates.add( criteriaBuilder.greaterThanOrEqualTo(root.get("fecha"), fechaInicial));
            }
            if(fechaFinal != null){
                predicates.add( criteriaBuilder.lessThanOrEqualTo(root.get("fecha"), fechaFinal));
            }
            Predicate[] p = new Predicate[predicates.size()];
            return criteriaBuilder.and( predicates.toArray(p));
        };
    }

    /**
     * Find all the data in the database
     *
     * @param specification is the query specification
     * @param pageable is the pageable object
     * @return a list with the data
     */
    private Map<String, Serializable> findByPage(Specification<HistorialTraslado> specification, Pageable pageable) {
        Page<HistorialTraslado> entities;
        entities = historialTrasladoRepository.findAll(specification, pageable);
        return new HashMap<String, Serializable>() {
            {
                put("number_of_pages", entities.getTotalPages());
                put( "total_elements", entities.getTotalElements() );
                put("content", (Serializable) mapData(entities.toList()));
            }
        };
    }


    /**
     * Map database data
     * @param entities is the history summary data
     * @return a list with the data mapped
    */
    private List<Map<String,Serializable>> mapData(List<HistorialTraslado> entities) {
        return entities.stream().map(this::mapEntity).collect(Collectors.toList());
    }

    /**
     * Map entity data
     * @param entity is the history summary data
     * @return the data mapped
    */
    private LinkedHashMap<String,Serializable> mapEntity(HistorialTraslado trasladoEntity) {
        HistorialCarteraResumen carteraEntity = this.historialCarteraResumenRepository.getLastResumenByContrato(trasladoEntity.getContrato().getNumero());
        LinkedHashMap<String,Serializable> fields = new LinkedHashMap<>();
        fields.put("id", UuidUtil.generate());
        fields.put("tipo_documento", trasladoEntity.getContrato().getCliente().getTipoDocumento().getNombre());
        fields.put("numero_identificacion", trasladoEntity.getContrato().getCliente().getNumeroIdentificacion().longValue());
        fields.put("numero_suministro", trasladoEntity.getContrato().getCliente().getCuenta().getNumero().longValue());
        fields.put("numero_contrato", trasladoEntity.getContrato().getNumero());
        fields.put("subproducto", trasladoEntity.getContrato().getSubProducto().getNombre());
        fields.put("cuenta_anterior", trasladoEntity.getCuentaAnterior().getNumero().longValue());
        fields.put("cuenta_actual", trasladoEntity.getCuentaActual().getNumero().longValue());
        fields.put("fecha_traslado", trasladoEntity.getFecha());
        fields.put("usuario", trasladoEntity.getUsuario().getNombres());
        fields.put("ciclo_anterior", trasladoEntity.getCicloAnterior());
        fields.put("ciclo_nuevo", trasladoEntity.getCicloActual());
        fields.put("numero_cuotas_facturadas", "");
        fields.put("numero_cuotas_actual", "");
        fields.put("capital", "");
        fields.put("capital_vencido", "");
        fields.put("saldo_capital", "");
        fields.put("interes_corriente", "");
        fields.put("interes_moratorio", "");
        fields.put("gastos_cobranza", "");
        fields.put("cuota_manejo", "");
        fields.put("seguros", "");
        if(carteraEntity != null){
            fields.put("numero_cuotas_facturadas", carteraEntity.getCuotasPactadasMaximo() - carteraEntity.getCuotasPorFacturarMaximo());
            fields.put("numero_cuotas_actual", carteraEntity.getCuotasPactadasMaximo());
            fields.put("capital", String.format(Locale.GERMAN, "%,d", Math.round(carteraEntity.getSumaCapitalImpago() + carteraEntity.getSumaSaldoCapitalPorFacturar())));
            fields.put("capital_vencido", String.format(Locale.GERMAN, "%,d", Math.round(carteraEntity.getSumaCapitalImpago())));
            fields.put("saldo_capital", String.format(Locale.GERMAN, "%,d", Math.round(carteraEntity.getSumaSaldoCapitalPorFacturar())));
            fields.put("interes_corriente", String.format(Locale.GERMAN, "%,d", Math.round(carteraEntity.getSumaInteresImpago() + carteraEntity.getSumaInteresOrdenCorrienteImpago() + carteraEntity.getSumaInteresFacturadoNoAfecto() + carteraEntity.getSumaInteresNoAfectoPorFacturar())));
            fields.put("interes_moratorio", String.format(Locale.GERMAN, "%,d", Math.round(carteraEntity.getSumaInteresMora() + carteraEntity.getSumaInteresOrdenMoraImpago())));
        }
        
        return fields;
    }

    /**
     * Get refinanciaciones without pagination
     * @param numeroContrato 'numero de contrato' to search data
     * @param numeroCuenta 'numero de cuenta' to search data
     * @param subproducto 'subproducto' to search data
     * @param tipoDocumento 'tipo de documento' to search data
     * @param numeroDocumento 'numero de documento' to search data
     * @param fechaInicial 'fecha inicial' to search data
     * @param fechaFinal 'fecha final' to search data
     * @param numeroServicio 'numero de servicio' to search data
     * @return data serializable
     */
    public List<Map<String, Serializable>> getAllTraslados(
        String numeroContrato,
        Double numeroCuenta,
        String tipoDocumento,
        Double numeroDocumento,
        LocalDate fechaInicial,
        LocalDate fechaFinal, 
        Integer subproducto
    ){
        List<Map<String, Serializable>> result;
        Specification<HistorialTraslado> specification;

        specification = createSpecifications(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, subproducto, fechaInicial, fechaFinal);

        result = findAll(specification);
        return result;
    }

    /**
     * Find all the data in the database     *
     * @param specification is the query specification
     * @return a list with the data
     */
    private List<Map<String, Serializable>> findAll(Specification<HistorialTraslado> specification) {
        List<HistorialTraslado> entities;
        entities = historialTrasladoRepository.findAll(specification);
        return mapData(entities);
    }
    
}
