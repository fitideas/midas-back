package com.colpatria.midas.services;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.dto.LogDto;
import com.colpatria.midas.utils.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.*;
import java.time.LocalDate;

/*
 *
 * Clase
 *
*/

@Service
public class LogReaderService {

    /*
     *
     * Constantes
     *
    */

    private final String simboloSeparador = "\\|";

    /*
     *
     * Atríbutos
     *
    */

    @Value( "${midas.jobs.path.logs}" )
    private String ruta;

    /*
     *
     * Métodos
     *
    */

    public LogReaderService() {
    }

    public LogDto read( String rutaArchivo ) {
        // Variables
        LogDto         log;
        File           file;
        BufferedReader bufferedReader;
        String         linea;
        String[]       datos;
        int            cantidadRegistros;
        String         nombreArchivo;
        LocalDate      fecha;
        String         rutaLog;
        int            numeroFila;
        // Código
        nombreArchivo = FileUtils.obtenerNombre( rutaArchivo );
        rutaLog       = ruta + "/" + crearNombreLog( nombreArchivo );
        file          = new File( rutaLog );
        try {
            bufferedReader = new BufferedReader( new FileReader( file ) );
            numeroFila     = 0;
            datos          = new String[2];
            while( ( linea = bufferedReader.readLine() ) != null ) {
                if( numeroFila == 1 ) {
                    datos = linea.split( simboloSeparador );
                }
                numeroFila++;
            }
            cantidadRegistros = Integer.parseInt( datos[0] );
            fecha             = FileUtils.extraerFecha( nombreArchivo );
            log = new LogDto( cantidadRegistros, nombreArchivo, fecha );
            return log;
        } catch( FileNotFoundException e ) {
            return null;
        } catch( IOException e ) {
            return null;
        }
    }

    private String crearNombreLog( String nombreArchivo ) {
        // Variables
        String nombreLog;
        // Código
        nombreLog = "Log_" + nombreArchivo;
        return nombreLog;
    }

}
