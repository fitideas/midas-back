package com.colpatria.midas.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.text.MessageFormat;

import com.colpatria.midas.model.HistorialArchivos;
import com.colpatria.midas.payload.ScheduleResponse;
import com.colpatria.midas.quartz.QuartzJobLauncher;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Job Scheduler Service
 */
@Service
public class JobSchedulerService {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(JobSchedulerService.class);

    /**
     * Job Launcher Bean
     */
    private final JobLauncher jobLauncher;

    /**
     * Job Locator Bean
     */
    private final JobLocator jobLocator;

    /**
     * Quartz Scheduler Bean
     */
    private final Scheduler scheduler;


    /**
     * Constructor
     *
     * @param scheduler   Scheduler bean reference
     * @param jobLauncher Job Launcher bean reference
     * @param jobLocator  Job Locator bean reference
     */
    @Autowired
    public JobSchedulerService(Scheduler scheduler, JobLauncher jobLauncher, JobLocator jobLocator) {
        this.scheduler = scheduler; this.jobLauncher = jobLauncher; this.jobLocator = jobLocator;
    }

    /**
     * Schedule the Master Job
     *
     * @param cron Master Job cron
     * @return Schedule Response
     */
    public ScheduleResponse scheduleMasterJob(String cron) {
        try {
            JobDetail masterJobDetail = this.buildJobDetail("masterJob");
            Trigger trigger = this.buildRepetitiveJobTrigger(masterJobDetail, cron);
            this.scheduler.scheduleJob(masterJobDetail, trigger);
            logger.info("The master job has been scheduled");
            return new ScheduleResponse(true, masterJobDetail.getKey().getName(), masterJobDetail.getKey().getGroup()
                    , "The master job has been scheduled successfully");
        } catch (SchedulerException se) {
            logger.error("The master job couldn't be scheduled: ", se);
            return new ScheduleResponse(false, "Error: " + se.getMessage());
        }
    }

    /**
     * Schedule a unique execution of a Load Job
     *
     * @param cron    Cron Expression
     * @param jobName Job Name
     * @param file    File to Process
     * @return Schedule Response
     */
    public ScheduleResponse scheduleLoadJob(String cron, String jobName, HistorialArchivos file) {
        try {
            JobDetail jobDetail = this.buildJobDetail(jobName, file);
            Trigger trigger = this.buildNonRepetitiveJobTrigger(jobDetail, cron);
            this.scheduler.scheduleJob(jobDetail, trigger);
            return new ScheduleResponse(true, jobDetail.getKey().getName(), jobDetail.getKey().getGroup(), "Job " +
                    "Scheduled Successfully!");
        } catch (SchedulerException se) {
            logger.error("A Job couldn't be scheduled: ", se);
            return new ScheduleResponse(false, "Error: " + se.getMessage());
        }
    }

    /**
     * Build a Job Detail without parameters
     *
     * @param jobName Job Name
     * @return Job Detail
     */
    private JobDetail buildJobDetail(String jobName) {
        JobDataMap jobDataMap = new JobDataMap(); jobDataMap.put("jobName", jobName);
        jobDataMap.put("jobLauncher", this.jobLauncher); jobDataMap.put("jobLocator", this.jobLocator);
        return JobBuilder.newJob(QuartzJobLauncher.class).setJobData(jobDataMap).storeDurably().build();
    }

    /**
     * Build a Job Detail given a Historial Archivo
     *
     * @param jobName Job name
     * @param file    Historial Archivo
     * @return Job Detail
     */
    private JobDetail buildJobDetail(String jobName, HistorialArchivos file) {
        JobDataMap jobDataMap = new JobDataMap(); jobDataMap.put("jobName", jobName);
        jobDataMap.put("jobLauncher", this.jobLauncher); jobDataMap.put("jobLocator", this.jobLocator);
        jobDataMap.put("file", file.getNombreArchivo());
        return JobBuilder.newJob(QuartzJobLauncher.class).withIdentity(MessageFormat.format("{0}_{1}", jobName,
                UUID.randomUUID())).setJobData(jobDataMap).storeDurably().build();
    }

    /**
     * Build a Repetitive Trigger for a job given a Cron expression
     *
     * @param jobDetail Job Detail
     * @param cron      Cron Expression
     * @return Trigger
     */
    private Trigger buildRepetitiveJobTrigger(JobDetail jobDetail, String cron) {
        return TriggerBuilder.newTrigger().forJob(jobDetail).withSchedule(CronScheduleBuilder.cronSchedule(cron)).build();
    }

    /**
     * Build a Non-Repetitive Trigger for a job given a Cron expression
     * The date of execution is computed for the next Valid Time of the cron expression
     *
     * @param jobDetail Job Detail
     * @param cron      Cron Expression
     * @return Trigger
     */
    private Trigger buildNonRepetitiveJobTrigger(JobDetail jobDetail, String cron) {
        try {
            CronExpression cronExpression = new CronExpression(cron);
            return TriggerBuilder.newTrigger().forJob(jobDetail).startAt(cronExpression.getNextValidTimeAfter(new Date())).build();
        } catch (ParseException pe) {
            logger.error("The Cron Expression '{}' is invalid: {}", cron, pe.getMessage()); return null;
        }
    }

    /**
     * Delete a scheduled Job
     *
     * @param scheduleRequest Schedule Request
     * @return Response Entity
     */
    public ResponseEntity<ScheduleResponse> deleteScheduledJob(String scheduleRequest) {
        JobKey job1Key = JobKey.jobKey(scheduleRequest); try {
            boolean response = scheduler.deleteJob(job1Key);
            return response ? ResponseEntity.status(HttpStatus.OK).body(new ScheduleResponse(true, scheduleRequest,
                    null, "Job " + "Deleted Successfully!")) :
                    ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ScheduleResponse(false, scheduleRequest,
                            null, "The job does not exist!"));
        } catch (SchedulerException se) {
            logger.error("Error deleting job", se);
            ScheduleResponse scheduleEmailResponse = new ScheduleResponse(false, MessageFormat.format("Error: {0}",
                    se.getMessage()));
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(scheduleEmailResponse);
        }
    }

    /**
     * Get All Scheduled Jobs
     *
     * @return Response Entity
     */
    public ResponseEntity<List<ScheduleResponse>> getScheduledJob() {
        GroupMatcher<JobKey> matcher = GroupMatcher.anyJobGroup(); try {
            List<ScheduleResponse> listResponse = new ArrayList<>(); for (JobKey item : scheduler.getJobKeys(matcher)) {
                listResponse.add(new ScheduleResponse(item.getName(), item.getGroup()));
            } return ResponseEntity.status(HttpStatus.OK).body(listResponse);
        } catch (SchedulerException se) {
            logger.error("Error getScheduledJob job", se);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ArrayList<>());
        }
    }

}
