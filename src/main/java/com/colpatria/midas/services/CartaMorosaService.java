package com.colpatria.midas.services;

/*
 *
 * Libraries
 *
*/

import com.colpatria.midas.dto.web.DeleteResponseDto;
import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.utils.DateUtils;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Cartera Service
*/
@Service
public class CartaMorosaService {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(CartaMorosaService.class);

    /**
     * Historial carta morosa repository
    */
    private final HistorialCartaMorosaRepository historialCartaMorosaRepository;

    /**
     * Historial cartera resumen repository
    */
    private final HistorialCarteraResumenRepository historialCarteraResumenRepository;

    /**
     * Contract repository to read and write to the database
     */
    private final ContratoRepository contratoRepository;

    /**
     * Client repository to read and write to the database
    */
	private final ClienteRepository clienteRepository;

    /**
     * Usuario Repository
     */
    final UsuarioRepository usuarioRepository;

    /**
     * Usuario Repository
     */
    final CalendarioFacturacionRepository calendarioFacturacionRepository;

    /**
     * History repository to read and write to the database
    */
    private final HistorialRelacionClienteSubproductoRepository historialRelacionClienteSubproductoRepository;

    /**
     * Path to save the generated reports
     */
    @Value("${midas.jobs.path.reports}")
    private String reportPath;

    /**
     * Class constructor.
     * @param historialCartaMorosaRepository is the historial carta morosa repository
     * @param historialCarteraResumen is the historial cartera resumen repository
     * @param contratoRepository is the contract repository
     * @param clienteRepository is the client repository
     * @param usuarioRepository is the user repository
     * @param calendarioFacturacionRepository is the celandario facturacion repository
     * @param calendarioFacturacionRepository is the celandario facturacion repository
    */
    @Autowired
    public CartaMorosaService( 
        HistorialCartaMorosaRepository                historialCartaMorosaRepository, 
        HistorialCarteraResumenRepository             historialCarteraResumenRepository,
        ContratoRepository                            contratoRepository,
        ClienteRepository                             clienteRepository,
        UsuarioRepository                             usuarioRepository,
        CalendarioFacturacionRepository               calendarioFacturacionRepository,
        HistorialRelacionClienteSubproductoRepository historialRelacionClienteSubproductoRepository
    ){
        this.historialCartaMorosaRepository                = historialCartaMorosaRepository;
        this.historialCarteraResumenRepository             = historialCarteraResumenRepository;
        this.contratoRepository                            = contratoRepository;
        this.clienteRepository                             = clienteRepository;
        this.usuarioRepository                             = usuarioRepository;
        this.calendarioFacturacionRepository               = calendarioFacturacionRepository;
        this.historialRelacionClienteSubproductoRepository = historialRelacionClienteSubproductoRepository;
    }

    /**
     * Search all cartera data
     * @param estados is a list of the contract states
     * @param ciclo is the billing cycle
     * @return a list with the data
    */
    public Resource generateCartasMorosas( List<String> estados, Integer ciclo ) throws InvalidQueryException {
        // Variables
        List<HistorialCarteraResumen> entities;
        List<HistorialCartaMorosa>    cartasMorosas;
        /*long                          numberOfCards;
        LocalDate                     dateNow;*/
        Resource                      report;
        // Code
        /*dateNow = LocalDate.now();
        numberOfCards = historialCartaMorosaRepository.countByCicloAndPeriodo( ciclo, dateNow.getYear(), dateNow.getMonthValue() );
        if( numberOfCards > 0 ) {
            throw new InvalidQueryException( "Las cartas ya fueron generadas" );
        }*/
        entities      = filterCartera( estados, ciclo );
        cartasMorosas = processCartasMorosas( entities, ciclo );
        if( cartasMorosas.isEmpty() ) {
            return null;
        }  
        report = generateReport( cartasMorosas );
        try {
            logger.info( "La generacion de cartas morosas se detuvo durante 10 segundos" );
            Thread.sleep(10000 );
        } catch( InterruptedException e ) {
            
        }   
        validateConsistency( entities, estados, ciclo );
        saveCartasMorosas( cartasMorosas );   
        return report;
    }

    /**
     * Method to filter portfolio data
     * @param entities is the history summary data
     * @param estados is a list of the contract states
     * @param ciclo is the billing cycle
    */
    private void validateConsistency( List<HistorialCarteraResumen> oldEntities, List<String> estados, Integer ciclo ) throws InvalidQueryException {
        // Variables
        List<HistorialCarteraResumen> entities;
        // Code
        entities = filterCartera( estados, ciclo );
        if( entities.size() != oldEntities.size() ) {
            throw new InvalidQueryException( "Se encontró una inconsistencia del archivo en relación a la foto de cartera" );
        }
    }

    private boolean isContractContained( List<Contrato> contratos, Contrato contrato ) {
        // Variables
        boolean isContained;
        // Code
        isContained = false;
        for( Contrato item : contratos ) {
            if( item.getNumero().equals( contrato.getNumero() ) ) {
                isContained = true;
                break;
            }
        }
        return isContained;
    }

    /**
     * Method to filter portfolio data
     * @param estados is a list of the contract states
     * @param ciclo is the billing cycle
     * @return a list with bank wallet data
    */
    private List<HistorialCarteraResumen> filterCartera( List<String> estados, Integer ciclo ) {
        // Variables
        List<Cliente>                             clientes;
        List<Contrato>                            contratos;
        List<Contrato>                            contratosSatisfechos;
        List<HistorialCarteraResumen>             carteraResumen;
        List<HistorialCarteraResumen>             carteraResumenSatisfechas;
        long                                      maximoDiasMora;
        List<Contrato>                            contratosFiltrados;
        List<Integer>                             estadosFacturacion;
        boolean                                   isPunished;
        List<HistorialRelacionClienteSubproducto> relacionesClientesSubproductos;
        String                                    ultimaFechaGeneracionString;
        String                                    ultimaFechaReporteString;
        LocalDate                                 ultimaFechaGeneracion;
        LocalDate                                 ultimaFechaReporte;
        List<HistorialCartaMorosa>                cartasMorosas;
        LocalDate                                 dateNow;
        // Code
        clientes = clienteRepository.getByCiclo( ciclo );
        contratos = new ArrayList<>();
        carteraResumen = new ArrayList<>();
        estadosFacturacion = new ArrayList<>();
        isPunished = false;
        dateNow = LocalDate.now();
        for( String estado : estados ) {
            if( estado.equals( "SI FACTURA" ) ) {
                estadosFacturacion.add( 1 );
            } else if( estado.equals( "NO FACTURA" ) ) {
                estadosFacturacion.add( 0 );
            } else if( estado.equals( "CASTIGADOS" ) ) {
                isPunished = true;
            }
        }
        ultimaFechaGeneracionString = this.historialRelacionClienteSubproductoRepository.getLastLoad();
        if( ultimaFechaGeneracionString == null ) {
            ultimaFechaGeneracion = LocalDate.now();
        } else {
            ultimaFechaGeneracion = DateUtils.stringToLocalDate( ultimaFechaGeneracionString );
        }
        for( Cliente cliente : clientes ) {
            cartasMorosas = historialCartaMorosaRepository.getByCicloAndPeriodoAndCliente( ciclo, dateNow.getYear(), dateNow.getMonthValue(), cliente.getNumeroIdentificacion() );
            if( cartasMorosas.isEmpty() ) {
                contratosSatisfechos = contratoRepository.getByClienteAndEstadosFacturacion( cliente.getNumeroIdentificacion(), estadosFacturacion );            
                contratos.addAll( contratosSatisfechos );
                if( isPunished ) {
                    relacionesClientesSubproductos = historialRelacionClienteSubproductoRepository.getByCastigoAndClienteAndFechaGeneracion( 
                        cliente.getNumeroIdentificacion(), 
                        ultimaFechaGeneracion
                    );
                    for( HistorialRelacionClienteSubproducto relacionClienteSubproducto : relacionesClientesSubproductos ) {      
                        if( !isContractContained( contratos, relacionClienteSubproducto.getContrato() ) ) {
                            contratos.add( relacionClienteSubproducto.getContrato() );
                        }
                    }
                }
            }
        }        
        contratosFiltrados = new ArrayList<>();
        for( Contrato contrato : contratos ) {
            try {
                maximoDiasMora = historialCarteraResumenRepository.getMaxDiasMoraByCliente( contrato.getCliente().getNumeroIdentificacion() );
                if( maximoDiasMora > 0 ) {
                    contratosFiltrados.add( contrato );
                }
            } catch( NullPointerException exc ) {
            }
        }
        ultimaFechaReporteString = historialCarteraResumenRepository.getLastLoad();
        if( ultimaFechaReporteString == null ){
            ultimaFechaReporte = LocalDate.now();
        } else {
            ultimaFechaReporte = DateUtils.stringToLocalDate( ultimaFechaReporteString );
        }
        for( Contrato contrato : contratosFiltrados ) {
            carteraResumenSatisfechas = historialCarteraResumenRepository.getByContratoNumeroAndFechaReporte( 
                contrato.getNumero(), 
                ultimaFechaReporte
            );
            if( carteraResumenSatisfechas != null ) {
                carteraResumen.addAll( carteraResumenSatisfechas );
            }
        }
        return carteraResumen;
    }

    /**
     * Map database data
     * @param entities is the history summary data
     * @return a list with the data mapped
    */
    private List<Map<String, Serializable>> mapData( List<HistorialCartaMorosa> entities ) {
        return entities.stream().map( this::mapEntity ).collect( Collectors.toList() );
    }

    /**
     * Map entity data
     * @param entity is the history summary data
     * @return a the data mapped
    */
    private LinkedHashMap<String, Serializable> mapEntity( HistorialCartaMorosa entity ) {
        return new LinkedHashMap<String, Serializable>() { {
            put( "mes", entity.getMes() != 0 ? entity.getMes() : "" );
            put( "nombre", entity.getCliente().getNombre() );
            put( "dirección", entity.getCliente().getCuenta().getDireccion() );
            put( "municipio", entity.getCliente().getCuenta().getMunicipio() );
            put( "localiza", entity.getCliente().getCuenta().getLocalizacion() );
            put( "numero_cli", entity.getCliente().getCuenta().getNumero().longValue() );
            put( "dias_atras", entity.getDiasAtraso() );
            put( "cedula", entity.getCliente().getNumeroIdentificacion().longValue() );
            put( "estado", entity.getEstado() );
            put( "ciclo", entity.getCliente().getCuenta().getCicloFacturacion() );
            put( "saldo_tota", entity.getSaldoTotal().longValue() );
            put( "pago_inmed", entity.getPagoInmediato().longValue() );
            put( "intereses", entity.getIntereses().longValue() );
            put( "intmora", entity.getInteresesMora().longValue() );
            put( "gcobran", entity.getGastosCobranza().longValue() );
            put( "sucursal", entity.getCliente().getCuenta().getSucursal().getNumero() );
            put( "fecha_vto", !entity.getFechaVencimiento().equals(LocalDate.parse("2018-02-27")) ? entity.getFechaVencimiento() : "" );
            put( "corte", entity.getCorte() );
        } };
    }

    /**
     * Method for process delinquent letters
     * @param entities is the history summary data
     * @param ciclo is the billing cycle
     * @return a list of delinquent letters
    */
    private List<HistorialCartaMorosa> processCartasMorosas( List<HistorialCarteraResumen> entities, int ciclo ) throws InvalidQueryException {
        // Variables
        List<HistorialCartaMorosa>          cartasMorosas;
        HistorialCartaMorosa                cartaMorosa;
        List<HistorialCarteraResumen>       carterasAsociadas;
        double                              sumaSaldoTotal;
        double                              sumaPagoInmediado;
        double                              sumaIntereses;
        double                              sumaInteresesMora;
        double                              sumaGastosCobranza;
        String                              estado;
        int                                 maximoDiasAtraso;
        boolean                             isActive;
        boolean                             isPunished;
        String                              ultimaFechaGeneracionString;
        LocalDate                           ultimaFechaGeneracion;
        HistorialRelacionClienteSubproducto relacionClienteSubproducto;
        // Code
        ultimaFechaGeneracionString = this.historialRelacionClienteSubproductoRepository.getLastLoad();
        if( ultimaFechaGeneracionString == null ) {
            ultimaFechaGeneracion = LocalDate.now();
        } else {
            ultimaFechaGeneracion = DateUtils.stringToLocalDate( ultimaFechaGeneracionString );
        }
        cartasMorosas = new ArrayList<>();
        for( HistorialCarteraResumen entity : entities ) {     
            sumaSaldoTotal     = 0;
            sumaPagoInmediado  = 0;
            sumaIntereses      = 0;
            sumaInteresesMora  = 0;
            sumaGastosCobranza = 0;
            sumaGastosCobranza = 0;
            maximoDiasAtraso   = 0;
            isActive           = true;
            isPunished         = false;
            carterasAsociadas  = this.historialCarteraResumenRepository.getByCliente( entity.getContrato().getCliente().getNumeroIdentificacion() );
            for( HistorialCarteraResumen carteraAsociada : carterasAsociadas ) {
                sumaSaldoTotal += getSaldoTotal( carteraAsociada );
                sumaPagoInmediado += getPagoInmediato( carteraAsociada );
                sumaIntereses += getIntereses( carteraAsociada );
                sumaInteresesMora += getInteresesMora( carteraAsociada );
                sumaGastosCobranza += 0.0;
                if( carteraAsociada.getDiasAtrasoMaximo() > maximoDiasAtraso ) {
                    maximoDiasAtraso = carteraAsociada.getDiasAtrasoMaximo();
                }
                relacionClienteSubproducto = this.historialRelacionClienteSubproductoRepository.getByFechaInicialCastigoNotNullAndFechaFinCastigoNullAndContratoNumeroAndFechaGeneracion( 
                    carteraAsociada.getContrato().getNumero(), 
                    ultimaFechaGeneracion
                );
                if( relacionClienteSubproducto != null ) {
                    isPunished = true;
                }
                if( !carteraAsociada.getContrato().getFacturacion() ) {
                    isActive = false;
                }
            }
            estado = "Castigado";
            if( !isPunished ) {
                estado = ( isActive ) ? "Activo" : "Ilocalizado";
            }            
            cartaMorosa = new HistorialCartaMorosa();  
            cartaMorosa = getMes( ciclo, entity.getContrato().getCliente().getCuenta().getSucursal().getNumero(), cartaMorosa );
            cartaMorosa.setContrato( entity.getContrato() );
            cartaMorosa.setCliente( entity.getContrato().getCliente() );
            cartaMorosa.setCorte( entity.getFechaReporte() );
            cartaMorosa.setFechaGeneracion( LocalDate.now() );
            cartaMorosa.setEstado( estado );
            cartaMorosa.setDiasAtraso( maximoDiasAtraso );
            cartaMorosa.setSaldoTotal( BigDecimal.valueOf( sumaSaldoTotal ) );
            cartaMorosa.setPagoInmediato( BigDecimal.valueOf( sumaPagoInmediado ) );
            cartaMorosa.setIntereses( BigDecimal.valueOf( sumaIntereses ) );
            cartaMorosa.setInteresesMora( BigDecimal.valueOf( sumaInteresesMora ) );
            cartaMorosa.setGastosCobranza( BigDecimal.valueOf( sumaGastosCobranza ) );            
            cartasMorosas.add( cartaMorosa );
        }
        return cartasMorosas;
    }

    /**
     * Method that saves delinquent letters
     * @param cartasMorosas is a list of delinquent letters
    */
    @Transactional
    private void saveCartasMorosas( List<HistorialCartaMorosa> cartasMorosas ) {
        // Variables
        Cliente  cliente;
        Contrato contrato;
        // Code
        for( HistorialCartaMorosa cartaMorosa : cartasMorosas ) {
            cliente  = this.clienteRepository.save( cartaMorosa.getCliente() );
            contrato = this.contratoRepository.save( cartaMorosa.getContrato() );
            cartaMorosa.setCliente( cliente );
            cartaMorosa.setContrato( contrato );
            this.historialCartaMorosaRepository.save( cartaMorosa );
        }
    }

    /**
     * Method that formats the file name
     * @param cartasMorosas is a list of delinquent letters
     * @return the name of the formatted file
    */
    public String formatReportFilePath() {
        // Variables
        DateTimeFormatter dateFormatter;
        String            fileName;
        // Code
        dateFormatter = DateTimeFormatter.ofPattern( "dd-MM-yyyy_hh-mm-ss" );
        fileName      = "cartasMorosas_" + LocalDateTime.now().format( dateFormatter ) + ".xlsx";
        return Paths.get( this.reportPath ).resolve( fileName ).toString();
    }

    /**
     * Generate a Report in XSLX format and return the corresponding Resource reference
     * @param entities is the history summary data
     * @return Resource reference
     */
    public Resource generateReport( List<HistorialCartaMorosa> entities ) throws InvalidQueryException {
        List<Map<String, Serializable>> registers = this.mapData( entities );
        String filePath = this.formatReportFilePath();
        try( XSSFWorkbook book = new XSSFWorkbook() ){
            XSSFSheet sheet = book.createSheet();
            String[] headerLabels = registers.get(0).keySet().toArray(new String[0]);
            XSSFRow header = sheet.createRow(0);
            for(int i = 0; i < headerLabels.length; i++){
                header.createCell(i).setCellValue(headerLabels[i]);
            }
            for(int i = 0; i < registers.size(); i++){
                XSSFRow row = sheet.createRow(i + 1);
                String[] keys = registers.get(i).keySet().toArray(new String[0]);
                for(int j = 0; j < keys.length; j++){
                    if(registers.get(i).get(keys[j]) != null )
                        row.createCell(j).setCellValue(registers.get(i).get(keys[j]).toString());
                    else
                        row.createCell(j).setCellValue("");
                }
            }
            FileOutputStream out = new FileOutputStream(filePath);
            book.write(out);
            out.close();
            return new UrlResource( Paths.get( filePath ).toUri() );
        } catch (IOException exception){
            logger.error("Error saving the xslx report. {}", exception.getMessage());
            throw new InvalidQueryException( "Error mientras se generaba las cartas" );
        }
    }

    /**
     * Method for calculate the month from period
     * @param ciclo is the billing cycle
     * @return the month from period
     */
    private HistorialCartaMorosa getMes( int ciclo, int sucursal, HistorialCartaMorosa cartaMorosa ) {
        // Variables
        LocalDate             fechaVencimiento;
        CalendarioFacturacion calendarioFacturacion;
        String                calendarLatestVersion;
        // Code
        fechaVencimiento      = LocalDate.now();
        calendarLatestVersion = calendarioFacturacionRepository.getLastVersionByCicloAndFechaVencimientoAndSucursal( ciclo, fechaVencimiento, sucursal );
        calendarioFacturacion = calendarioFacturacionRepository.getByCicloAndFechaVencimientoAndSucursalAndVersion( ciclo, fechaVencimiento, sucursal, calendarLatestVersion );
        if( calendarioFacturacion != null ) {
            cartaMorosa.setMes( calendarioFacturacion.getPeriodo().getMonthValue() );
            cartaMorosa.setFechaVencimiento( calendarioFacturacion.getPrimerVencimiento() );
        } else {
            cartaMorosa.setMes( 0 );
            cartaMorosa.setFechaVencimiento( LocalDate.parse("2018-02-27") );
        }
        return cartaMorosa;
    }

    /**
     * Method for calculate the total balance
     * @param entity is a summary portfolio record
     * @return the total balance
     */
    private Double getSaldoTotal( HistorialCarteraResumen entity ) {
        return entity.getSumaCapitalImpago() + entity.getSumaInteresImpago() + 
            entity.getSumaInteresOrdenCorrienteImpago() + entity.getSumaInteresMora() +
            entity.getSumaInteresOrdenMoraImpago() + entity.getSumaInteresFacturadoNoAfecto() + 
            entity.getSumaInteresNoAfectoPorFacturar() + entity.getSumaSaldoCapitalPorFacturar();
    }

    /**
     * Method for calculate the immediate payment
     * @param entity is a summary portfolio record
     * @return the immediate payment
     */
    private Double getPagoInmediato( HistorialCarteraResumen entity ) {
        return entity.getSumaCapitalImpago() + entity.getSumaInteresImpago() + 
            entity.getSumaInteresOrdenCorrienteImpago() + entity.getSumaInteresMora() +
            entity.getSumaInteresOrdenMoraImpago() + entity.getSumaInteresFacturadoNoAfecto();
    }

    /**
     * Method for calculate the interest
     * @param entity is a summary portfolio record
     * @return the interest
     */
    private Double getIntereses( HistorialCarteraResumen entity ) {
        return entity.getSumaInteresImpago() + entity.getSumaInteresOrdenCorrienteImpago() + 
            entity.getSumaInteresMora() + entity.getSumaInteresOrdenMoraImpago() +
            entity.getSumaInteresFacturadoNoAfecto();
    }

    /**
     * Method for calculate the interest on arrears
     * @param entity is a summary portfolio record
     * @return the interest on arrears
     */
    private Double getInteresesMora( HistorialCarteraResumen entity ) {
        return entity.getSumaInteresMora() + entity.getSumaInteresOrdenMoraImpago();
    }

    /**
     * Method for deleting cartasMorosos
     * @param file file with the entries to delete
     * @return a report of the deleting process
     */
    public DeleteResponseDto deleteCartas(InputStream file) throws IOException{
        int deleted = 0;
        int failed = 0;
        if(!usuarioRepository.existsById("1")){
            usuarioRepository.save(new Usuario("1","area","nombres"));
        }
        Scanner scanner = new Scanner(file);
        if(!Objects.equals(scanner.nextLine(), "FECHA GENERACION|TIPO DOCUMENTO|NUMERO DOCUMENTO"))
            throw new InvalidObjectException("Error en la estructura del archivo");
        List<String> errors= new ArrayList<>();
        String headers = "FECHA GENERACION|TIPO DOCUMENTO|NUMERO DOCUMENTO|ERROR|";
        errors.add(headers);
        while(scanner.hasNext()){
            String[] values = scanner.nextLine().split("\\|");
            try{
                LocalDate fechaGeneracion = LocalDate.parse(values[0]);
                Double cedula = Double.parseDouble(values[2]);
                Cliente cliente = clienteRepository.getByNumeroIdentificacionAndTipoDocumentoCodigo(cedula,values[1]).orElseThrow(NoSuchElementException::new);
                Optional<HistorialCartaMorosa> cartaMorosaOptional = historialCartaMorosaRepository.findByClienteAndFechaGeneracionAndFechaEliminadoNull(cliente, fechaGeneracion);
                if(cartaMorosaOptional.isPresent()){
                    HistorialCartaMorosa cartaMorosa = cartaMorosaOptional.get();
                    cartaMorosa.setFechaEliminado(LocalDateTime.now());
                    cartaMorosa.setUsuarioEliminacion(usuarioRepository.getById("1"));
                    historialCartaMorosaRepository.save(cartaMorosa);
                    deleted++;
                    logger.info("Carta a Moroso con tipo documento: {} y numero documento {} y fecha: {} Eliminada por usuario: {}",
                            values[1],
                            values[2],
                            values[0],
                            usuarioRepository.getById("1").getId());
                }else{
                    errors.add(values[0]+"|"+values[1]+"|"+values[2]+"|carta no encontrada|");

                    logger.error("Error al encontrar carta a eliminar tipo documento: {}, numero documento: {} y fecha de generación: {}",
                            values[1],
                            values[2],
                            values[0]);
                    failed++;
                }
            }
            catch (DateTimeException|NumberFormatException|NoSuchElementException e){
                if(e instanceof DateTimeException){
                    errors.add(values[0]+"|"+values[1]+"|"+values[2]+"|Error de formato en la fecha|");
                    logger.error("Error de formato en el campo fecha de la carta a eliminar con tipo documento: {}, numero documento: {} y fecha de generación: {}",
                            values[1],
                            values[2],
                            values[0]);
                }else if(e instanceof NumberFormatException){
                    errors.add(values[0]+"|"+values[1]+"|"+values[2]+"|Error de formato en el documento del cliente|");
                    logger.error("Error de formato en el documento del cliente de la carta a eliminar con tipo documento: {}, numero documento: {} y fecha de generación: {}",
                            values[1],
                            values[2],
                            values[0]);
                }else{
                    errors.add(values[0]+"|"+values[1]+"|"+values[2]+"|Error documento de cliente inexistente|");
                    logger.error("Error documento de cliente inexistente de la carta a eliminar con tipo documento: {}, numero documento: {} y fecha de generación: {}",
                            values[1],
                            values[2],
                            values[0]);
                }
                failed++;
            }
        }
        if(failed>0){
            String fileName = "Log_Carta_Morosa_errors_"+ LocalDateTime.now().toString().replace(":","-")+".txt";
            try(BufferedWriter writer =new BufferedWriter(new FileWriter(reportPath+fileName))) {
                for(String line:errors){
                    writer.write(line);
                    writer.newLine();
                }
            }
            return new DeleteResponseDto(deleted,failed,fileName);
        }
        return new DeleteResponseDto(deleted,failed,null);
    }
}
