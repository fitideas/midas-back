package com.colpatria.midas.services;

import com.colpatria.midas.dto.web.PagedResponseDto;
import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.model.HistorialCambioEstado;
import com.colpatria.midas.repositories.PagedHistorialCambioEstadoRepository;
import com.colpatria.midas.utils.UuidUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Cambio Estado Service
 */
@Service
public class CambioEstadoService extends HistoryService{
    /**
     * Paged HistorialCambioEstado Repository
     */
    private final PagedHistorialCambioEstadoRepository pagedCambioEstadoRepository;

    /**
     * Class constructor
     * @param pagedCambioEstadoRepository Paged HistorialCambioEstado Repository
     */
    public CambioEstadoService(PagedHistorialCambioEstadoRepository pagedCambioEstadoRepository) {
        this.pagedCambioEstadoRepository = pagedCambioEstadoRepository;
    }

    /**
     * public method to find all HistoricoCambioEstado using filters ang pagination
     * @param tipoDocumento refers to the tipoDocumento Filter. Used with numerodocumento
     * @param numeroDocumento refers to the numeroDocumento Filter. Used with tipoDocumento
     * @param numeroContrato refers to the numeroContrato Filter.
     * @param numeroServicio refers to the numeroServicio Filter.
     * @param subProducto refers to the subProduct Filter
     * @param numeroCuenta refers to the numeroCuenta Filter
     * @param fechaInicial refers to the initial date Filter
     * @param fechaFinal refers to the final date Filter
     * @return All the registers
     */
    public List<Map<String,Serializable>> findHistoricoCambioEstado(
            String    tipoDocumento,
            Double    numeroDocumento,
            Integer    subProducto,
            String    numeroContrato,
            Double    numeroCuenta,
            LocalDate fechaInicial,
            LocalDate fechaFinal,
            String    numeroServicio
    ){
        List<HistorialCambioEstado> result= pagedCambioEstadoRepository.findAll(
                specifyQuery(
                        tipoDocumento,
                        subProducto,
                        numeroDocumento,
                        numeroContrato,
                        numeroCuenta,
                        fechaInicial,
                        fechaFinal,
                        numeroServicio
                )
        );
        return result.stream().map(this::mapEntity).collect(
                Collectors.toList());
    }

    /**
     * public method to find all HistoricoCambioEstado using filters ang pagination
     * @param tipoDocumento refers to the tipoDocumento Filter. Used with numerodocumento
     * @param numeroDocumento refers to the numeroDocumento Filter. Used with tipoDocumento
     * @param numeroContrato refers to the numeroContrato Filter.
     * @param numeroServicio refers to the numeroServicio Filter.
     * @param subProducto refers to the subProduct Filter
     * @param numeroCuenta refers to the numeroCuenta Filter
     * @param fechaInicial refers to the initial date Filter
     * @param fechaFinal refers to the final date Filter
     * @param limit amount of registers
     * @param offset offset of registers
     * @return All the registers with the pagination configuration
     * @throws InvalidQueryException if the pagination configuration is null or fechaInicial is after fechaFinal
     */
    public PagedResponseDto findHistoricoCambioCarteraPaged(
            String   tipoDocumento,
            Double    numeroDocumento,
            String      numeroContrato,
            Integer   subProducto,
            Double    numeroCuenta,
            LocalDate fechaInicial,
            LocalDate fechaFinal,
            String    numeroServicio,
            Integer   limit,
            Integer   offset
    ) throws InvalidQueryException {
        if (limit == null || offset == null){
            throw new InvalidQueryException( "Datos de paginación inválidos" );
        }
        if ((tipoDocumento != null && numeroDocumento == null)||(tipoDocumento == null && numeroDocumento != null)) {
            throw new InvalidQueryException("Tipo y documento inválidos");
        }
        if (tipoDocumento == null && numeroContrato == null && numeroCuenta == null && numeroServicio == null) {
            throw new InvalidQueryException("Criterios de búsqueda vacios");
        }
        if(fechaInicial != null && fechaFinal != null && fechaInicial.isAfter(fechaFinal)){
            throw new InvalidQueryException("La fecha inicial debe ser anterior a la fecha final");
        }
        Pageable pageable = PageRequest.of( offset, limit );
        Page<HistorialCambioEstado> result= pagedCambioEstadoRepository.findAll(
                specifyQuery(
                        tipoDocumento,
                        subProducto,
                        numeroDocumento,
                        numeroContrato,
                        numeroCuenta,
                        fechaInicial,
                        fechaFinal,
                        numeroServicio
                ),pageable
        );
        List<Map<String,Serializable>> data = result.stream().map(this::mapEntity).collect(
                Collectors.toList());
        return new PagedResponseDto(result.getTotalPages(), (Serializable) data);
    }
    /**
     * Map entity data
     * @param entity is the history summary data
     * @return a the data mapped
     */
    private Map<String, Serializable> mapEntity(HistorialCambioEstado entity ) {
        Map<String, Serializable> map = new LinkedHashMap<>();
        map.put( "id", UuidUtil.generate() );
        map.put( "cedula_del_titular", String.format("%.0f", entity.getServicio().getContrato().getCliente().getNumeroIdentificacion()) );
        map.put( "numero_de_servicio_o_compra", entity.getServicio().getNumero() );
        map.put( "subproducto", entity.getServicio().getContrato().getSubProducto().getNombre() );
        map.put( "estado_anterior", entity.getServicioEstadoAnterior() );
        map.put( "estado_actual", entity.getServicioEstadoNuevo() );
        map.put( "fecha_de_cambio_de_estado", entity.getFecha() );
        map.put( "usuario", entity.getUsuario().getId() );
        map.put( "area", entity.getUsuario().getArea() );
        map.put( "saldo_capital", entity.getServicio().getSaldoCapitalPorFacturar() );
        map.put( "capital", 0 );
        map.put( "capital_vencido", 0 );
        map.put( "interes", 5 );
        map.put( "interes_moratorio", 0 );
        map.put( "cuotas_de_manejo", 0 );
        map.put( "seguros", 0 );
        map.put( "gastos_de_cobranza", 0 );
        return map;
    }

    /**
     * Create a query specification
     * @param codigoTipoDocumento is the document type code
     * @param numeroDocumento is the document number
     * @param numeroContrato is the contract number
     * @param numeroCuenta is the account number
     * @param fechaInicial is the starting date
     * @param fechaFinal is the end date
     * @param numeroServicio is the service number
     * @return a query specification
     */
    private Specification<HistorialCambioEstado> specifyQuery(
            String   codigoTipoDocumento,
            Integer   subProducto,
            Double    numeroDocumento,
            String      numeroContrato,
            Double    numeroCuenta,
            LocalDate fechaInicial,
            LocalDate fechaFinal,
            String    numeroServicio
    ) {
        return ( root, query, criteriaBuilder ) -> {
            Predicate predicates = historialQueryPredicates(
                    root,
                    criteriaBuilder,
                    codigoTipoDocumento,
                    numeroDocumento,
                    subProducto,
                    numeroContrato,
                    numeroCuenta,
                    numeroServicio
            );
            if(fechaInicial!=null){
                predicates = criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo( root.get( "fecha" ), fechaInicial ),predicates);
            }
            if(fechaFinal!=null){
                predicates = criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo( root.get( "fecha" ), fechaFinal ),predicates);
            }
            return criteriaBuilder.and(predicates);
        };
    }

}
