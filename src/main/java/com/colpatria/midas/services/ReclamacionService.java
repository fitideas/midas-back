package com.colpatria.midas.services;

import com.colpatria.midas.model.Reclamacion;
import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.model.Usuario;
import com.colpatria.midas.repositories.ReclamacionRepository;
import com.colpatria.midas.repositories.TipoDocumentoRepository;
import com.colpatria.midas.repositories.TipoReclamoRepository;
import com.colpatria.midas.repositories.UsuarioRepository;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * Reclamación service
 */
@Service
public class ReclamacionService {
    /**
     * Reclamación repository
     */
    final ReclamacionRepository reclamacionRepository;
    /**
     * Cliente repository
     */
    final TipoDocumentoRepository tipoDocumentoRepository;
    /**
     * Usuario repository
     */
    final UsuarioRepository usuarioRepository;
    /**
     * tipoReclamacion repository
     */
    final TipoReclamoRepository tipoReclamoRepository;
    /**
     * Class Constructor
     * @param reclamacionRepository Reclamación repository
     * @param tipoDocumentoRepository Cliente repository
     * @param usuarioRepository Usuario repository
     * @param tipoReclamoRepository tipoReclamacion repository
     */
    public ReclamacionService(ReclamacionRepository reclamacionRepository,
                              TipoDocumentoRepository tipoDocumentoRepository,
                              UsuarioRepository usuarioRepository, TipoReclamoRepository tipoReclamoRepository) {
        this.reclamacionRepository = reclamacionRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.usuarioRepository = usuarioRepository;
        this.tipoReclamoRepository = tipoReclamoRepository;
    }

    /**
     * Method for uploading new reclamations
     * @param inputStream the file with the reclamations
     * @return teh number of reclamations uploaded
     */
    @Transactional
    public Integer uploadReclamaciones(InputStream inputStream){
        if(!usuarioRepository.existsById("1")){
            usuarioRepository.save(new Usuario("1","area","nombres"));
        }
        int registros = 0;
        try( Workbook workbook = WorkbookFactory.create(inputStream)) {
            ArrayList<Reclamacion> entries = new ArrayList<>();
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> it = sheet.rowIterator();
            for (it.next(); it.hasNext(); ) {
                Row row = it.next();
                if(StreamSupport.stream(row.spliterator(), false).count() <2)continue;
                Double numeroDocumento = row.getCell(1).getNumericCellValue();
                TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(String.format("%.0f",row.getCell(0).getNumericCellValue())).orElseThrow(NullPointerException::new);
                Optional<Reclamacion> collision =entries.stream().filter(reclamacion -> reclamacion.getTipoDocumento().equals(tipoDocumento)&&reclamacion.getNumeroDocumento().equals(numeroDocumento)&&reclamacion.getVigente()).findAny();
                if(collision.isPresent()){
                    if(collision.get().getTipoReclamo().getPrioridad()>tipoReclamoRepository.findById(row.getCell(2).getStringCellValue()).orElseThrow(NullPointerException::new).getPrioridad()){
                        entries.remove(collision.get());
                    }else{
                        registros++;
                        continue;
                    }
                }
                entries.add(new Reclamacion(
                    null,
                    tipoDocumento,
                    numeroDocumento,
                    LocalDateTime.now(),
                    usuarioRepository.getById("1"),
                    true,
                    tipoReclamoRepository.findById(row.getCell(2).getStringCellValue()).orElseThrow(NullPointerException::new)
                ));
                registros++;
            }
            List<Reclamacion> anteriores = reclamacionRepository.getAllByVigenteTrue();
            anteriores.forEach(reclamacion -> reclamacion.setVigente(false));
            reclamacionRepository.saveAll(anteriores);
            reclamacionRepository.saveAll(entries);
            return registros;
        } catch (IOException|NullPointerException ignored) {
            return 0;
        }
    }
}
