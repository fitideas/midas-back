package com.colpatria.midas.services.catalogue;

import com.colpatria.midas.model.Producto;
import com.colpatria.midas.repositories.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductoRepository productRepository;

    public ProductService(){
    }

    public List<Map<String, Serializable>> findAllSubProducts() {
        Collection<Producto> entities = this.productRepository.getSubProductList();
        /*List<ProductoDto> productos = new ArrayList<ProductoDto>();
        for (Producto entity : entities){
            TipoProductoDto tipoProductoDto = new TipoProductoDto(entity.getTipo().getCodigo(),entity.getTipo().getNombre());
            TipoProductoDto tipoProductoPadreDto = new TipoProductoDto(entity.getProductoPadre().getTipo().getCodigo(),entity.getProductoPadre().getTipo().getNombre());
            ProductoDto padre = new ProductoDto(tipoProductoPadreDto,entity.getProductoPadre().getCodigo(),entity.getProductoPadre().getNombre(),null);
            ProductoDto productoDto = new ProductoDto(tipoProductoDto, entity.getCodigo(), entity.getNombre(),padre);
            productos.add(productoDto);
        }
        return productos;*/
        
        return entities.stream().map( entity -> new HashMap<String, Serializable>() { {
            put( "codigo", entity.getCodigo() );
            put( "nombre", entity.getNombre() );
            put( "tipoProducto", new HashMap<String, Serializable>(){ {
                put( "codigo", entity.getTipo().getCodigo() );
                put( "nombre", entity.getTipo().getNombre() );
            } } );
            put( "productoPadre", new HashMap<String, Serializable>(){ {
                put( "codigo", entity.getProductoPadre().getCodigo() );
                put( "nombre", entity.getProductoPadre().getNombre() );
                put( "tipoProducto", new HashMap<String, Serializable>(){ {
                    put( "codigo", entity.getProductoPadre().getTipo().getCodigo() );
                    put( "nombre", entity.getProductoPadre().getTipo().getNombre() );
                } } );
            } } );
        } } ).collect( Collectors.toList() );
        
    }
}
