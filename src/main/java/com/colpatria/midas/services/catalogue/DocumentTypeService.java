package com.colpatria.midas.services.catalogue;

import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.repositories.TipoDocumentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DocumentTypeService {

    @Autowired
    private TipoDocumentoRepository tipoDocumentoRepository;

    public DocumentTypeService(){

    }

    public List<Map<String, Serializable>> findAllDocumentType() {

        List<TipoDocumento> entities = this.tipoDocumentoRepository.findAll();

        return entities.stream().map( entity -> new HashMap<String, Serializable>() { {
            put( "codigo", entity.getCodigo() );
            put( "nombre", entity.getNombre() );
        } } ).collect( Collectors.toList() );

        /*List<TipoDocumentoDto> dtos = new ArrayList<TipoDocumentoDto>();
        for(TipoDocumento entity : entities){
            TipoDocumentoDto dto =  new TipoDocumentoDto(entity.getCodigo(), entity.getNombre());
            dtos.add(dto);
        }
        return dtos;*/
    }
}
