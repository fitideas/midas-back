package com.colpatria.midas.services;

import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.HistorialCarteraResumenRepository;
import com.colpatria.midas.repositories.HistorialRefinanciacionRepository;
import com.colpatria.midas.utils.DateUtils;
import com.colpatria.midas.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Refinanciacion Service
 */
@Service
public class RefinanciacionService {

    /**
     * respository of historialRefinanciacion
     */
    private final HistorialRefinanciacionRepository historialRefinanciacionRepository;

    /**
     * respository of historialCarteraResumen
     */
    private final HistorialCarteraResumenRepository historialCarteraResumenRepository;


    /**
     * @param historialRefinanciacionRepository DI of historial of refinanciacion
     * @param historialCarteraResumen
     */
    @Autowired
    public RefinanciacionService(HistorialRefinanciacionRepository historialRefinanciacionRepository, HistorialCarteraResumenRepository historialCarteraResumen) {
        this.historialRefinanciacionRepository = historialRefinanciacionRepository;
        this.historialCarteraResumenRepository = historialCarteraResumen;
    }


    /**
     * Get refinanciaciones with pagination
     *
     * @param numeroContrato  'numero de contrato' to search data
     * @param numeroCuenta    'numero de cuenta' to search data
     * @param tipoDocumento   'tipo de documento' to search data
     * @param numeroDocumento 'numero de documento' to search data
     * @param fechaInicial    'fecha inicial' to search data
     * @param fechaFinal      'fecha final' to search data
     * @param numeroServicio  'numero de servicio' to search data
     * @param offset          'offset' to search data
     * @param limit           'limit' to search data
     * @return data serializable
     * @throws InvalidQueryException Error founded in filters
     */
    public Map<String, Serializable> getRefinanciaciones(String numeroContrato,
                                                         Double numeroCuenta,
                                                         String tipoDocumento,
                                                         Double numeroDocumento,
                                                         String fechaInicial,
                                                         String fechaFinal,
                                                         String numeroServicio,
                                                         Integer Subproducto,
                                                         Integer offset,
                                                         Integer limit) throws InvalidQueryException {
        Map<String, Serializable> result;
        Pageable pageable;
        Specification<HistorialRefinanciacion> specification;

        LocalDate fechaInicialDate = DateUtils.stringToLocalDate(fechaInicial);
        LocalDate fechaFinalDate = DateUtils.stringToLocalDate(fechaFinal);

        if (tipoDocumento == null &&
                numeroDocumento == null &&
                numeroServicio == null &&
                numeroContrato == null &&
                numeroCuenta == null)
            throw new InvalidQueryException("Criteriós de búsqueda vacios.");

        if ((tipoDocumento != null && numeroDocumento == null) || (tipoDocumento == null && numeroDocumento != null))
            throw new InvalidQueryException("Tipo y documento inválidos");

        if (limit == null || offset == null) {
            throw new InvalidQueryException("Los índices de paginación son inválidos");
        }

        if (fechaInicialDate != null && fechaFinalDate != null && fechaInicialDate.isAfter(fechaFinalDate)) {
            throw new InvalidQueryException("La fecha inicial es mayor que la fecha final");
        }

        if (fechaInicialDate != null && fechaInicialDate.isAfter(LocalDate.now())) {
            throw new InvalidQueryException("La fecha inicial no puede ser mayor a la actual");
        }

        if (fechaFinalDate != null && fechaFinalDate != null && fechaFinalDate.isAfter(LocalDate.now())) {
            throw new InvalidQueryException("La fecha final no puede ser mayor a la actual");
        }

        specification = isBetweenTheReportingDates(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, fechaInicialDate, fechaFinalDate, numeroServicio, Subproducto);
        pageable = PageRequest.of(offset, limit, Sort.by(Sort.Order.desc("negociacion.fecha")));
        result = findByPage(specification, pageable);
        return result;
    }


    /**
     * Get refinanciaciones without pagination
     *
     * @param numeroContrato  'numero de contrato' to search data
     * @param numeroCuenta    'numero de cuenta' to search data
     * @param tipoDocumento   'tipo de documento' to search data
     * @param numeroDocumento 'numero de documento' to search data
     * @param fechaInicial    'fecha inicial' to search data
     * @param fechaFinal      'fecha final' to search data
     * @param numeroServicio  'numero de servicio' to search data
     * @return data serializable
     */
    public List<Map<String, Serializable>> getAllRefinanciaciones(String numeroContrato,
                                                                  Double numeroCuenta,
                                                                  String tipoDocumento,
                                                                  Double numeroDocumento,
                                                                  LocalDate fechaInicial,
                                                                  LocalDate fechaFinal,
                                                                  String numeroServicio,
                                                                  Integer subproducto) {
        List<Map<String, Serializable>> result;
        Specification<HistorialRefinanciacion> specification;

        specification = isBetweenTheReportingDates(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, fechaInicial, fechaFinal, numeroServicio, subproducto);

        result = findAll(specification);
        return result;
    }


    /**
     * Create a query specification
     *
     * @param codigoTipoDocumento is the document type code
     * @param numeroDocumento     is the document number
     * @param numeroContrato      is the contract number
     * @param numeroCuenta        is the account number
     * @param fechaInicial        is the starting date
     * @param fechaFinal          is the end date
     * @param numeroServicio      is the service number
     * @return a query specification
     */
    private Specification<HistorialRefinanciacion> isBetweenTheReportingDates(
            String codigoTipoDocumento,
            Double numeroDocumento,
            String numeroContrato,
            Double numeroCuenta,
            LocalDate fechaInicial,
            LocalDate fechaFinal,
            String numeroServicio,
            Integer Subproducto
    ) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates;

            Join<HistorialRefinanciacion, Servicio> servicio;
            Join<HistorialRefinanciacion, Negociacion> negociacion;
            Join<Servicio, Contrato> contrato;
            Join<Contrato, Cliente> cliente;
            Join<Cliente, TipoDocumento> tipoDocumento;
            Join<Cliente, Cuenta> cuenta;

            predicates = new ArrayList<>();
            servicio = root.join("servicio", JoinType.INNER);
            negociacion = root.join("negociacion", JoinType.INNER);
            contrato = servicio.join("contrato", JoinType.INNER);
            contrato.join("subProducto", JoinType.INNER);
            cliente = contrato.join("cliente", JoinType.INNER);
            cuenta = cliente.join("cuenta", JoinType.INNER);
            tipoDocumento = cliente.join("tipoDocumento", JoinType.INNER);
            if (numeroServicio != null)
                predicates.add(criteriaBuilder.equal(servicio.get("numero"), numeroServicio));

            if (Subproducto != null)
                predicates.add(criteriaBuilder.equal(contrato.get("subProducto"), Subproducto));

            if (numeroContrato != null)
                predicates.add(criteriaBuilder.equal(contrato.get("numero"), numeroContrato));

            if (numeroDocumento != null && codigoTipoDocumento != null) {

                predicates.add(criteriaBuilder.equal(cliente.get("numeroIdentificacion"), numeroDocumento));
                predicates.add(criteriaBuilder.equal(tipoDocumento.get("codigo"), codigoTipoDocumento));
            }

            if (numeroCuenta != null)
                predicates.add(criteriaBuilder.equal(cuenta.get("numero"), numeroCuenta));

            if (fechaInicial != null)
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(negociacion.get("fecha"), fechaInicial));

            if (fechaFinal != null)
                predicates.add(criteriaBuilder.lessThanOrEqualTo(negociacion.get("fecha"), fechaFinal));


            Predicate[] p = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(p));
        };
    }

    /**
     * Find all the data in the database
     *
     * @param specification is the query specification
     * @param pageable      is the pageable object
     * @return a list with the data
     */
    private Map<String, Serializable> findByPage(Specification<HistorialRefinanciacion> specification, Pageable pageable) {
        // Variables
        Page<HistorialRefinanciacion> entities;
        // Code
        entities = historialRefinanciacionRepository.findAll(specification, pageable);
        return new HashMap<String, Serializable>() {
            {
                put("number_of_pages", entities.getTotalPages());
                put("total_elements", entities.getTotalElements());
                put("content", (Serializable) mapData(entities.toList()));
            }
        };
    }

    /**
     * Find all the data in the database     *
     *
     * @param specification is the query specification
     * @return a list with the data
     */
    private List<Map<String, Serializable>> findAll(Specification<HistorialRefinanciacion> specification) {
        // Variables
        List<HistorialRefinanciacion> entities;
        // Code
        entities = historialRefinanciacionRepository.findAll(specification);
        return mapData(entities);
    }

    /**
     * Map database data
     *
     * @param entities is the history summary data
     * @return a list with the data mapped
     */
    private List<Map<String, Serializable>> mapData(List<HistorialRefinanciacion> entities) {
        return entities.stream().map(this::mapEntity).collect(Collectors.toList());
    }

    /**
     * Map entity data
     *
     * @param entity is the history summary data
     * @return a the data mapped
     */
    private LinkedHashMap<String, Serializable> mapEntity(HistorialRefinanciacion entity) {
        List<HistorialCarteraResumen> historialRefinanciacionList = historialCarteraResumenRepository.getByContrato(entity.getServicio().getContrato().getNumero(), entity.getFechaApli());
        HistorialCarteraResumen historialRefinanciacion = historialRefinanciacionList.stream().findFirst().orElse(null);
        return new LinkedHashMap<String, Serializable>() {
            {
                put("id", UuidUtil.generate());
                put("Tipo_documento", entity.getServicio().getContrato().getCliente().getTipoDocumento().getNombre());
                put("Numero_documento", entity.getServicio().getContrato().getCliente().getNumeroIdentificacion().longValue());
                put("Numero_suministro", entity.getServicio().getContrato().getCliente().getCuenta().getNumero().longValue());
                put("Numero_contrato", entity.getServicio().getContrato().getNumero());
                put("subproducto", entity.getServicio().getContrato().getSubProducto().getNombre());
                put("fecha_negociacion", entity.getNegociacion().getFecha());
                put("fecha_aplicacion", entity.getFechaApli());
                put("numero_compra_modificado", entity.getServicio().getNumero());
                put("tasa_interes_actual", entity.getTasaNueva());
                put("tasa_interes_anterior", entity.getTasaAnterior());
                put("valor_cuota", entity.getValorPago().longValue());
                put("valor_cuota_anterior", entity.getCuotaMensualAnterior().longValue());
                put("valor_cuota_nueva", entity.getCuotaMensualDespues().longValue());
                put("plazo_anterior", entity.getCantidadCuotasAnteriores());
                put("nuevo_plazo", entity.getCantidadCuotasDespuesDeRefinanciacion());
                put("tipo_modificacion", "");
                put("estado_negociacion", entity.getNegociacion().getEstado());
                put("subproducto", entity.getServicio().getContrato().getSubProducto().getNombre());
                put("id_negociacion", entity.getNegociacion().getId());
                put("capital_vencido", historialRefinanciacion == null ? "" : historialRefinanciacion.getSumaCapitalImpago().longValue());
                put("saldo_capital", historialRefinanciacion == null ? "" : historialRefinanciacion.getSumaSaldoCapitalPorFacturar().longValue());
                put("rol_modificado", entity.getUsuario().getNombres());
                put("area_modificacion", entity.getUsuario().getArea());
            }
        };
    }
}
