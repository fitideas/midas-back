package com.colpatria.midas.services;

/*
 *
 * Libraries
 *
*/

import org.springframework.stereotype.Service;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.Cuenta;
import com.colpatria.midas.model.HistorialCarteraResumen;
import com.colpatria.midas.model.Servicio;
import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.repositories.HistorialCarteraResumenRepository;
import com.colpatria.midas.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

/**
 * Cartera Service
*/
@Service
public class HistoricoCarteraService {

    /**
     * Historial cartera resumen repository
    */
    private final HistorialCarteraResumenRepository historialCarteraResumenRepository;

    /**
     * Class constructor.
     * @param historialCarteraResumenRepository is the historial cartera resumen repository
    */
    @Autowired
    public HistoricoCarteraService( HistorialCarteraResumenRepository historialCarteraResumenRepository ){
        this.historialCarteraResumenRepository = historialCarteraResumenRepository;
    }

    /**
     * Search all cartera data
     * @param tipoDocumento is the document type code
     * @param numeroDocumento is the document number
     * @param numeroContrato is the contract number
     * @param numeroCuenta is the account number
     * @param fechaInicial is the starting date
     * @param fechaFinal is the end date
     * @param numeroServicio is the service number
     * @return a list with the data
    */
    public List<Map<String, Serializable>> findAllHistoricoCartera(
        String    tipoDocumento,
        Double    numeroDocumento, 
        String    numeroContrato, 
        Double    numeroCuenta, 
        LocalDate fechaInicial, 
        LocalDate fechaFinal,
        String    numeroServicio
     ) {
        // Variables
        List<Map<String, Serializable>>        result;
        Specification<HistorialCarteraResumen> specification;
        // Code
        specification = isBetweenTheReportingDates( 
            tipoDocumento, 
            numeroDocumento, 
            numeroContrato, 
            numeroCuenta, 
            fechaInicial,
            fechaFinal,
            numeroServicio
        );
        result = findAll( specification );
        return result;
    }

    /**
     * Search all cartera data
     * @param tipoDocumento is the document type code
     * @param numeroDocumento is the document number
     * @param numeroContrato is the contract number
     * @param numeroCuenta is the account number
     * @param fechaInicial is the starting date
     * @param fechaFinal is the end date
     * @param numeroServicio is the service number
     * @param limit is the number of records
     * @param offset is the page number
     * @return a list with the data
    */
    public Map<String, Serializable> findHistoricoCarteraByPage(
        String    tipoDocumento,
        Double    numeroDocumento, 
        String    numeroContrato, 
        Double    numeroCuenta, 
        LocalDate fechaInicial, 
        LocalDate fechaFinal,
        String    numeroServicio,
        Integer   limit,
        Integer   offset
     ) throws InvalidQueryException {
        // Variables
        Map<String, Serializable>              result;
        Pageable                               pageable;
        Specification<HistorialCarteraResumen> specification;
        // Code
        if( limit == null || offset == null ) {
            throw new InvalidQueryException( "Los índices de paginación son inválidos" );
        }
        if( !areAnyNotNulls( tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, numeroServicio ) ) {
            throw new InvalidQueryException( "Criterios de búsqueda vacios" );
        }
        if( fechaInicial != null && fechaFinal != null && fechaInicial.isAfter( fechaFinal ) ) {
            throw new InvalidQueryException( "La fecha inicial es mayor que la fecha final" );
        }
        pageable = PageRequest.of( offset.intValue(), limit.intValue() );
        specification = isBetweenTheReportingDates( 
            tipoDocumento, 
            numeroDocumento, 
            numeroContrato, 
            numeroCuenta, 
            fechaInicial, 
            fechaFinal, 
            numeroServicio
        );
        result = findByPage( specification, pageable );
        return result;
    }

    /**
     * Get all 'facturacionConcepto' data
     * @param numeroContrato 'numero de contrato' to search data
     * @param numeroCuenta 'numero de cuenta' to search data
     * @param tipoDocumento 'tipo de documento' to search data
     * @param numeroDocumento 'numero de documento' to search data
     * @param fechaInicial 'fecha inicial' to search data
     * @param fechaFinal 'fecha final' to search data
     * @return boolean that indicates if one or more fields are not null
     * @throws InvalidQueryException
    */
    public boolean areAnyNotNulls(
        String tipoDocumento, 
        Double numeroDocumento,
        String numeroContrato, 
        Double numeroCuenta,
        String numeroServicio
    ) {
        if( ( tipoDocumento != null && numeroDocumento != null ) || 
            numeroContrato != null || numeroCuenta != null || numeroServicio != null 
        ) {
            return true;
        }
        return false;
    }

    /**
     * Map database data
     * @param entities is the history summary data
     * @return a list with the data mapped
    */
    private List<Map<String, Serializable>> mapData( List<HistorialCarteraResumen> entities ) {
        return entities.stream().map( this::mapEntity ).collect( Collectors.toList() );
    }

    /**
     * Map entity data
     * @param entity is the history summary data
     * @return a the data mapped
    */
    private LinkedHashMap<String, Serializable> mapEntity( HistorialCarteraResumen entity ) {
        return new LinkedHashMap<String, Serializable>() { {
            put( "id", UuidUtil.generate() );
            put( "tipo_documento", entity.getContrato().getCliente().getTipoDocumento().getNombre() );
            put( "numero_identificacion_cliente", entity.getContrato().getCliente().getNumeroIdentificacion().longValue() );
            put( "numero_suministro", entity.getContrato().getCliente().getCuenta().getNumero().longValue() );
            put( "numero_contrato", entity.getContrato().getNumero() );
            put( "subproducto", entity.getContrato().getSubProducto().getNombre() );
            put( "nombre_cliente", entity.getContrato().getCliente().getNombre() );
            put( "capital_impago", entity.getSumaCapitalImpago().longValue() );
            put( "saldo_capital_por_facturar", entity.getSumaSaldoCapitalPorFacturar().longValue() );
            put( "interes_corriente_orden_causado_no_facturado", entity.getSumaInteresCorrienteOrdenCausadoNoFacturado().longValue() );
            put( "interes_orden_corriente_impago", entity.getSumaInteresOrdenCorrienteImpago().longValue() );
            put( "total_interes_corriente_causado", entity.getSumaTotalInteresCorrienteCausado().longValue() );
            put( "interes_mora", entity.getSumaInteresMora().longValue() );
            put( "interes_mora_causado_no_facturado", entity.getSumaInteresMoraCausadoNoFacturado().longValue() );
            put( "interes_mora_orden_causado_no_facturado", entity.getSumaInteresMoraOrdenCausadoNoFacturado().longValue() );
            put( "interes_orden_mora_impago", entity.getSumaInteresOrdenMoraImpago().longValue() );
            put( "total_interes_mora_causado", entity.getSumaTotalInteresMoraCausado().longValue() );
            put( "total_interes_mora_facturado_impago", entity.getSumaTotalInteresMoraFacturadoImpago().longValue() );
            put( "dias_atraso_maximo", entity.getDiasAtrasoMaximo() );
            put( "mora_primer_vencimiento_maximo", entity.getMoraPrimerVencimientoMaximo() );
            put( "estado_contrato", entity.getContrato().getEstado() );
            put( "asignacion_casa_cobranzas", "" );            
        } };
    }

    /**
     * Find all the data in the database
     * @param specification is the query specification
     * @return a list with the data
    */
    private List<Map<String, Serializable>> findAll( Specification<HistorialCarteraResumen> specification ) {
        // Variables
        List<HistorialCarteraResumen> entities;
        // Code
        entities = historialCarteraResumenRepository.findAll( specification );
        return mapData( entities );
    }

    /**
     * Find all the data in the database
     * @param specification is the query specification
     * @param pageable is the pageable object
     * @return a list with the data
    */
    private Map<String, Serializable> findByPage( Specification<HistorialCarteraResumen> specification, Pageable pageable ) {
        // Variables
        Page<HistorialCarteraResumen> entities;
        // Code
        entities = historialCarteraResumenRepository.findAll( specification, pageable );
        return new HashMap<String, Serializable>() { {
            put( "number_of_pages", entities.getTotalPages() );
            put( "total_elements", entities.getTotalElements() );
            put( "content", ( Serializable )mapData( entities.toList() ) );
        } };
    }

    /**
     * Create a query specification
     * @param codigoTipoDocumento is the document type code
     * @param numeroDocumento is the document number
     * @param numeroContrato is the contract number
     * @param numeroCuenta is the account number
     * @param fechaInicial is the starting date
     * @param fechaFinal is the end date
     * @param numeroServicio is the service number
     * @return a query specification
    */
    private Specification<HistorialCarteraResumen> isBetweenTheReportingDates(
        String    codigoTipoDocumento,
        Double    numeroDocumento, 
        String    numeroContrato, 
        Double    numeroCuenta, 
        LocalDate fechaInicial, 
        LocalDate fechaFinal,
        String    numeroServicio
    ) {
        return ( root, query, criteriaBuilder ) -> {
            // Variables
            List<Predicate>                         predicates;
            Join<HistorialCarteraResumen, Contrato> contrato;
            Join<Contrato, Cliente>                 cliente;
            Join<Cliente, Cuenta>                   cuenta;
            Join<Cliente, TipoDocumento>            tipoDocumento;
            Join<Contrato, Servicio>                servicio;
            // Code
            predicates = new ArrayList<>();
            contrato   = root.join( "contrato", JoinType.INNER );
            if( numeroContrato != null ) {
                predicates.add( criteriaBuilder.equal( contrato.get( "numero" ) , numeroContrato ) );
            }
            if( numeroServicio != null ) {
                servicio = contrato.join( "servicios", JoinType.INNER );
                predicates.add( criteriaBuilder.equal( servicio.get( "numero" ) , numeroServicio ) );
            }
            cliente = contrato.join( "cliente", JoinType.INNER );
            if( numeroDocumento != null && codigoTipoDocumento != null ) {
                tipoDocumento = cliente.join( "tipoDocumento", JoinType.INNER );
                predicates.add( criteriaBuilder.equal( cliente.get( "numeroIdentificacion" ) , numeroDocumento ) );
                predicates.add( criteriaBuilder.equal( tipoDocumento.get( "codigo" ) , codigoTipoDocumento ) );
            }
            if( numeroCuenta != null ) {
                cuenta = cliente.join( "cuenta", JoinType.INNER );
                predicates.add( criteriaBuilder.equal( cuenta.get( "numero" ) , numeroCuenta ) );
            }
            if( fechaFinal != null && fechaInicial != null ) {
                predicates.add( criteriaBuilder.greaterThanOrEqualTo( root.get( "fechaReporte" ), fechaInicial ) );
                predicates.add( criteriaBuilder.lessThanOrEqualTo( root.get( "fechaReporte" ), fechaFinal ) );
            }
            Predicate[] p = new Predicate[predicates.size()];
            return criteriaBuilder.and( predicates.toArray( p ) );
        };
    }

}