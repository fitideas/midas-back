package com.colpatria.midas.services;

import com.colpatria.midas.exceptions.InvalidQueryException;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.HistorialRecaudoRepository;
import com.colpatria.midas.repositories.HistorialRecaudoResumenRepository;
import com.colpatria.midas.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Recaudo Service
 */
@Service
public class RecaudoService {

    /**
     * Historial Recaudo Repository bean reference
     */
    private final HistorialRecaudoRepository historialRecaudoRepository;

    /**
     * Historial Recaudo Repository bean reference
     */
    private final HistorialRecaudoResumenRepository historialRecaudoResumenRepository;


    /**
     * Constructor.
     * @param historialRecaudoRepository Historial recaudo bean reference
     */
    @Autowired
    public RecaudoService(HistorialRecaudoRepository historialRecaudoRepository, HistorialRecaudoResumenRepository historialRecaudoResumenRepository) {
        this.historialRecaudoRepository = historialRecaudoRepository;
        this.historialRecaudoResumenRepository = historialRecaudoResumenRepository;
    }

    public List<Map<String, Serializable>> findAllHistoricoRecaudo(String tipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroCuenta, Integer subProducto, LocalDate fechaInicial, LocalDate fechaFinal, String numeroServicio, boolean verMas) {
        if(verMas){
            Specification<HistorialRecaudo> specification = this.isBetweenTheReportingDates(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, subProducto, fechaInicial, fechaFinal, numeroServicio);
            return this.findAll(specification);
        }else{
            Specification<HistorialRecaudoResumen> specification = this.isBetweenTheReportingDatesResumen(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, subProducto, fechaInicial, fechaFinal, numeroServicio);
            return this.findAllResumen(specification);
        }

    }

    public Map<String, Serializable> findHistoricoRecaudoByPage(String tipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroCuenta, Integer subProducto, LocalDate fechaInicial, LocalDate fechaFinal, String numeroServicio, Integer limit, Integer offset) throws InvalidQueryException {
        if (limit == null || offset == null){
            throw new InvalidQueryException( "Datos de paginación inválidos" );
        }
        if ((tipoDocumento != null && numeroDocumento == null)||(tipoDocumento == null && numeroDocumento != null)) {
            throw new InvalidQueryException("Tipo y documento inválidos");
        }
        if (tipoDocumento == null && numeroContrato == null && numeroCuenta == null && numeroServicio == null) {
            throw new InvalidQueryException("Criterios de búsqueda vacios");
        }
        if(fechaInicial != null && fechaFinal != null && fechaInicial.isAfter(fechaFinal)){
            throw new InvalidQueryException("La fecha inicial debe ser anterior a la fecha final");
        }
        Pageable pageable = PageRequest.of(offset, limit, Sort.by(Sort.Order.desc("fechaPago")));
        Specification<HistorialRecaudoResumen> specification = this.isBetweenTheReportingDatesResumen(tipoDocumento, numeroDocumento, numeroContrato, numeroCuenta, subProducto, fechaInicial, fechaFinal, numeroServicio);
        return this.findByPage(specification, pageable);
    }

    private List<Map<String, Serializable>> mapData(List<HistorialRecaudo> entities) {
        return entities.stream().map(this::mapEntity).collect(Collectors.toList());
    }

    private List<Map<String, Serializable>> mapDataResumen(List<HistorialRecaudoResumen> entities) {
        return entities.stream().map(this::mapEntityResumen).collect(Collectors.toList());
    }

    private Map<String, Serializable> mapEntity(HistorialRecaudo entity) {
        Map<String, Serializable> map = new LinkedHashMap<>();
        map.put("id", UuidUtil.generate());
        map.put("tipo_documento",  entity.getResumen().getContrato().getCliente().getTipoDocumento().getNombre());
        map.put("numero_documento", String.format("%.0f", entity.getResumen().getContrato().getCliente().getNumeroIdentificacion()));
        map.put("numero_suministro", String.format("%.0f", entity.getResumen().getContrato().getCliente().getCuenta().getNumero()));
        map.put("numero_contrato", entity.getResumen().getContrato().getNumero());
        map.put("subproducto", entity.getResumen().getContrato().getSubProducto().getNombre());
        map.put("fecha_de_pago", entity.getFechaPago());
        map.put("fecha_proceso_pago", entity.getFechaProcesoPago());
        map.put("tipo_cargo", entity.getCargo().getTipoCargo().getNombre());
        map.put("valor", String.format("%.0f", entity.getRecaudoCargo().getMonto()));
        return map;
    }

    private Map<String, Serializable> mapEntityResumen(HistorialRecaudoResumen entity) {
        Map<String, Serializable> map = new LinkedHashMap<>();
        map.put("id", UuidUtil.generate());
        map.put("tipo_documento", entity.getContrato().getCliente().getTipoDocumento().getNombre());
        map.put("numero_documento", String.format("%.0f", entity.getContrato().getCliente().getNumeroIdentificacion()));
        map.put("numero_suministro", String.format("%.0f", entity.getContrato().getCliente().getCuenta().getNumero()));
        map.put("numero_contrato", entity.getContrato().getNumero());
        map.put("subproducto", entity.getContrato().getSubProducto().getNombre());
        map.put("fecha_de_pago", entity.getFechaPago());
        map.put("fecha_proceso_pago", entity.getFechaProcesoPago());
        map.put("capital", entity.getCapital().intValue());
        map.put("intereses_corrientes", entity.getInteresCorriente().intValue());
        map.put("intereses_de_mora", entity.getInteresMora().intValue());
        map.put("cuota_de_manejo", entity.getCuotaManejo().intValue());
        map.put("gastos_de_cobranza",  entity.getHonorarioCobranza().intValue());
        map.put("seguro_obligatorio", entity.getSeguroObligatorio().intValue());
        map.put("seguro_voluntario", entity.getSeguroVoluntario().intValue());
        return map;
    }

    private List<Map<String, Serializable>> findAll(Specification<HistorialRecaudo> specification) {
        return this.mapData(this.historialRecaudoRepository.findAll(specification));
    }

    private List<Map<String, Serializable>> findAllResumen(Specification<HistorialRecaudoResumen> specification) {
        return this.mapDataResumen(this.historialRecaudoResumenRepository.findAll(specification));
    }

    private Map<String, Serializable> findByPage(Specification<HistorialRecaudoResumen> specification, Pageable pageable) {
        Page<HistorialRecaudoResumen> entities = this.historialRecaudoResumenRepository.findAll(specification, pageable);
        Map<String, Serializable> map = new HashMap<>();
        map.put("number_of_pages", entities.getTotalPages());
        map.put("total_elements", entities.getTotalElements());
        map.put("content", (Serializable) mapDataResumen(entities.toList()));
        return map;
    }

    private Specification<HistorialRecaudo> isBetweenTheReportingDates(String codigoTipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroCuenta, Integer subProducto, LocalDate fechaInicial, LocalDate fechaFinal, String numeroServicio) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            Join<HistorialRecaudo, RecaudoCargo> recaudoCargo = root.join("recaudoCargo", JoinType.INNER);
            Join<RecaudoCargo, Servicio> servicio = recaudoCargo.join("servicio", JoinType.INNER);
            Join<Servicio, Contrato> contrato = servicio.join("contrato", JoinType.INNER);
            Join<Contrato, Cliente> cliente = contrato.join("cliente", JoinType.INNER);
            Join<Cliente, Cuenta> cuenta = cliente.join("cuenta", JoinType.INNER);
            Join<Cliente, TipoDocumento> tipoDocumento = cliente.join("tipoDocumento", JoinType.INNER);
            if (numeroServicio != null) {
                predicates.add(criteriaBuilder.equal(servicio.get("numero"), numeroServicio));
            }
            if (numeroContrato != null) {
                predicates.add(criteriaBuilder.equal(contrato.get("numero"), numeroContrato));
            }
            if (numeroDocumento != null && codigoTipoDocumento != null) {
                predicates.add(criteriaBuilder.equal(cliente.get("numeroIdentificacion"), numeroDocumento));
                predicates.add(criteriaBuilder.equal(tipoDocumento.get("codigo"), codigoTipoDocumento));
            }
            if (numeroCuenta != null) {
                predicates.add(criteriaBuilder.equal(cuenta.get("numero"), numeroCuenta));
            }
            if (subProducto != null) {
                predicates.add(criteriaBuilder.equal(contrato.get("subProducto"), subProducto));
            }
            if (fechaInicial != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("fechaProcesoPago"), fechaInicial));
            }
            if (fechaFinal != null) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("fechaProcesoPago"), fechaFinal));
            }
            query.distinct(true);
            Predicate[] p = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(p));
        };
    }

    private Specification<HistorialRecaudoResumen> isBetweenTheReportingDatesResumen(String codigoTipoDocumento, Double numeroDocumento, String numeroContrato, Double numeroCuenta, Integer subProducto, LocalDate fechaInicial, LocalDate fechaFinal, String numeroServicio) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            Join<HistorialRecaudoResumen, Contrato> contrato = root.join("contrato", JoinType.INNER);
            Join<Contrato, Servicio> servicio = contrato.join("servicios", JoinType.INNER);
            Join<Contrato, Cliente> cliente = contrato.join("cliente", JoinType.INNER);
            Join<Cliente, Cuenta> cuenta = cliente.join("cuenta", JoinType.INNER);
            Join<Cliente, TipoDocumento> tipoDocumento = cliente.join("tipoDocumento", JoinType.INNER);
            if (numeroServicio != null) {
                predicates.add(criteriaBuilder.equal(servicio.get("numero"), numeroServicio));
            }
            if (numeroContrato != null) {
                predicates.add(criteriaBuilder.equal(contrato.get("numero"), numeroContrato));
            }
            if (numeroDocumento != null && codigoTipoDocumento != null) {
                predicates.add(criteriaBuilder.equal(cliente.get("numeroIdentificacion"), numeroDocumento));
                predicates.add(criteriaBuilder.equal(tipoDocumento.get("codigo"), codigoTipoDocumento));
            }
            if (numeroCuenta != null) {
                predicates.add(criteriaBuilder.equal(cuenta.get("numero"), numeroCuenta));
            }
            if (subProducto != null) {
                predicates.add(criteriaBuilder.equal(contrato.get("subProducto"), subProducto));
            }
            if (fechaInicial != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("fechaProcesoPago"), fechaInicial));
            }
            if (fechaFinal != null) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("fechaProcesoPago"), fechaFinal));
            }
            query.distinct(true);
            Predicate[] p = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(p));
        };
    }

}
