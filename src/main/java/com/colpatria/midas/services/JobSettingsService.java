package com.colpatria.midas.services;

import com.colpatria.midas.model.JobSettings;
import com.colpatria.midas.repositories.JobSettingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Job Cron Service
 */
@Service
public class JobSettingsService {

    /**
     * Job Cron Repository
     */
    private final JobSettingsRepository jobSettingsRepository;

    /**
     * Constructor
     *
     * @param jobSettingsRepository Job Cron Repository Bean Reference
     */
    @Autowired
    public JobSettingsService(JobSettingsRepository jobSettingsRepository) {
        this.jobSettingsRepository = jobSettingsRepository;
    }

    /**
     * Get the Cron expression of a Job
     *
     * @param jobName Job name
     * @return Cron expression
     */
    public String getCronByJobName(String jobName) {
        return this.jobSettingsRepository.getJobSettingsByJobName(jobName).getCron();
    }

    public List<JobSettings> getAllJobsSettings(){
        return this.jobSettingsRepository.findAll();
    }

}
