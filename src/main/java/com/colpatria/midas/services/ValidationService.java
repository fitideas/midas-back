package com.colpatria.midas.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import com.colpatria.midas.utils.DateUtils;
import com.colpatria.midas.utils.NumberUtils;
/**
 * Validation service
 */
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ValidationService {
    /**
     * Logger que se implementa para informar en la interfaz acerca de procesos.
     */
    private static final Logger logger = LoggerFactory.getLogger(ValidationService.class);
    /**
     * Saves the state of error of the files being read.
     */
    private boolean hasError = false;
    /**
     * indicates if the line of the file has an error.
     */
    private boolean lineHasError;
    /**
     * Strings that saves the report of errors found.
     */
    private StringBuilder registerReport;
    /**
     * Counters that allows you to know the line of the file that is being read.
     */
    private Long registerCounter = 1L;
    /**
     * @return return register report
     */
    public StringBuilder getRegisterReport() {
        return registerReport;
    }
    /**
     * @return state of validation service
     */
    public boolean getHasError() {
        return hasError;
    }
    /**
     * @return register counter
     */
    public Long getRegisterCounter(){
        return registerCounter;
    }
    /**
     * @return a boolean variable which indicates if the line of the file has an error.
     */
    public boolean isLineError() {
        return lineHasError;
    }
    /**
     * Constructor of method
     */
    public ValidationService() {
        this.hasError = false;
        this.registerReport = new StringBuilder("");
        this.registerCounter = 1L;
    }
    /**
     * @param value     field with data
     * @param valueName column to verify
     * @param fileName  file name to process
     * @param <T>       Type of process
     */
    public <T> void verifyEmpty(T value, String valueName, String fileName) {
        if (value == null || value.getClass().isAssignableFrom(String.class) && value.toString().isEmpty()) {
            registerReport(valueName, fileName, this.registerCounter, "Esta vacio");
        }
    }

    public <T> boolean isEmptyField(T value, String valueName, String fileName){
        if (value == null || value.getClass().isAssignableFrom(String.class) && value.toString().isEmpty()){
            registerReport(valueName, fileName, this.registerCounter, "Esta vacio");
            return true;
        }
        return false;
    }


    /**
     * validate catalog
     *
     * @param value     field with data
     * @param valueName column to verify
     * @param fileName  file name to process
     * @param <T>       Type of process
     */
    public <T> void verifyCatalog(T value, String valueName, String fileName) {
        if (value == null) {
            registerReport(valueName, fileName, this.registerCounter, "Catalogo no Existe");
        }
    }
    /**
     * validate catalogue
     * @param catalogue field with data
     * @param catalogueName column to verify
     * @param catalogueId is the catalogue id
     * @param fileName file name to process
     * @param <T> is the type of process
     */
    public <T> void validateCatalogue( T catalogue, String catalogueName, String catalogueId, String fileName ) {
        if( catalogue == null ) {
            registerReport( catalogueName, fileName, this.registerCounter, "Catálogo " + catalogueId + " no existe" );
        }
    }
    /**
     * @param parentId Parent id to evaluate
     * @param childId  child to evaluate
     * @param parent   parent string
     * @param child    child name
     * @param fileName file name to process
     * @param <T>      Type of process
     */
    public <T> void verifyParent(T parentId, T childId, String parent, String child, String fileName) {
        if (parentId != childId) {
            registerReport(child, fileName, this.registerCounter, "No pertenece al catalogo, " + parent);
        }
    }
    /**
     * validate parent
     * @param child field with child data
     * @param parent field with parent data
     * @param childName column to verify
     * @param childId is the catalogue id
     * @param fileName file name to process
     * @param <T> is the type of process
     */
    public <T> void validateParent( T child, T parent, String childName, String childId, String parentName, String fileName ) {
        if( !child.equals( parent ) ) {
            registerReport( childName, fileName, this.registerCounter, childName + " " + childId + " no pertenece a " + parentName );
        }
    }
    /**
     * validate child
     * @param child field with child data
     * @param parent field with parent data
     * @param childName column to verify
     * @param parentName is the catalogue id
     * @param fileName file name to process
     * @param <T> is the type of process
     */
    public <T> void validateChild( T child, T parent, String childName, String parentName, String fileName ) {
        if( !child.equals( parent ) ) {
            registerReport( childName, fileName, this.registerCounter, "El '" + childName + "' no pertenece a '" + parentName + "'");
        }
    }
    /**
     * validate integer type
     * @param value field with data
     * @param columnName column to verify
     * @param fileName file name to process
     */
    public void validateInteger( String value, String columnName, String fileName ) {
        if( !isEmptyField( value, columnName, fileName ) && !NumberUtils.isInteger( value ) ) {
            registerReport( columnName, fileName, this.registerCounter, "Valor númerico invalido");
        }
    }
    /**
     * validate integer type with possible zero value
     * @param value field with data
     * @param columnName column to verify
     * @param fileName file name to process
     */
    public void validateIntegerWithZero( String value, String columnName, String fileName ) {
        if( !isEmptyField( value, columnName, fileName ) && !isZero( value, columnName, fileName ) && 
            !NumberUtils.isInteger( value ) 
        ) {
            registerReport( columnName, fileName, this.registerCounter, "Valor númerico invalido");
        }
    }
    /**
     * validate double type
     * @param value field with data
     * @param columnName column to verify
     * @param fileName file name to process
     */
    public void validateDouble( String value, String columnName, String fileName ) {
        if( !isEmptyField( value, columnName, fileName ) && !NumberUtils.isDouble( value ) ) {
            registerReport( columnName, fileName, this.registerCounter, "Valor décimal invalido");
        }
    }
    /**
     * validate Long type
     * @param value field with data
     * @param columnName column to verify
     * @param fileName file name to process
     */
    public void validateLong( String value, String columnName, String fileName ) {
        try {
            Long.parseLong(value);
        } catch (Exception e) {
            registerReport(columnName, fileName, this.registerCounter, "Valor númerico invalido");
        }
    }
    /**
     * validate date
     * @param value field with data
     * @param columnName column to verify
     * @param fileName file name to process
     */
    public void validateDate( String value, String columnName, String fileName ) {
        if( !DateUtils.isValidDate(value) ) {
            registerReport( columnName, fileName, this.registerCounter, "Fecha invalida");
        }
    }
    /**
     * validate date
     * @param date Date string
     * @param columnName column with message
     * @param fileName to process
     */
    public void verifyDate(String date, String columnName, String fileName)
    {
        try {
            LocalDate.parse(date);
        } catch (Exception e) {
            registerReport(columnName, fileName, this.registerCounter, "Fecha invalida");
        }
    }
    /**
     * @param number Number string
     * @param columnName column with message
     * @param fileName to process
     */
    public  void verifyNumber(String number, String columnName, String fileName)
    {
        try {
            Double.parseDouble(number);
        } catch (Exception e) {
            registerReport(columnName, fileName, this.registerCounter, "Numero invalido");
        }
    }
    /**
     * validate that a value is not zero
     * @param value field with data
     * @param columnName column to verify
     * @param fileName file name to process
     */
    public boolean isZero( String value, String columnName, String fileName ) {
        try{
            if( Double.parseDouble( value ) <= 0 ){
                registerReport( columnName, fileName, this.registerCounter, "Valor es cero" );
                return true;
            }
        } catch( NumberFormatException exc ) {
            
        }
        return false;        
    }
    /**
     * validate that a value is not zero
     * @param value field with data
     * @param columnName column to verify
     * @param fileName file name to process
     */
    public void verifyZero( String value, String columnName, String fileName ) {
        if( ( NumberUtils.isDouble(value) && Double.parseDouble(value) <= 0) ){
            registerReport(columnName, fileName, this.registerCounter, "Valor es cero");
        }
    }
    /**
     * method to set new line
     */
    public void increaseRegisterCounter() {
        registerCounter++;
    }
    /**
     * Reset all values
     */
    public void resetValues(){
        this.hasError = false;
        this.registerReport = new StringBuilder();
        this.registerCounter = 1L;
    }
    /**
     * Clean all errors
     */
    public void cleanLineErrors(){
        this.lineHasError = false;
    }
    /**
     * Register error in string builder
     *
     * @param valueName       column to write
     * @param fileName        file name to process
     * @param registerCounter line with problem
     * @param error           error message
     */
    private void registerReport(String valueName, String fileName, Long registerCounter, String error) {
        registerReport.append("Error en el campo '").append(valueName).append("' en la linea '").append(registerCounter).append("' del archivo '").append(fileName).append("': ").append(error).append("\n");
        hasError     = true;
        lineHasError = true;
    }
}