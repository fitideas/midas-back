package com.colpatria.midas.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.colpatria.midas.model.EmailSettings;
import com.colpatria.midas.repositories.EmailSettingsRepository;

/**
 * Clase que tiene como funcionalidad el envio del correo informando
 * de procesos y/o errores.
 * @author Ricardo Calvo
 */
@Service
public class EmailService{

    /**
     * Logger que se implementa para informar en la interfaz acerca de procesos.
     */
    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    /**
     * Variable que toma el template para el envio del mail.
     */
    private final TemplateEngine templateEngine;

    /**
     * Variable que se implementa para crear un nuevo mensaje dependiendo
     * del metodo/proceso.
     */
    private final JavaMailSender mailSender;

    /**
     * Variable que implementa la tabla EMAIL_SETTINGS de la db.
     */
    private final EmailSettingsRepository emailSettingsRepository;

    /**
     * Variable tipo String Array que define los destinatarios del
     * mail.
     */
    private String[] to;

    /**
     * Variable que se implementa para que, a traves del archivo
     * application-dev.properties, se tome el correo remitente del
     * mail.
     */
    @Value("${midas.email.from}")
    private String from;

    /**
     * Variable que se implementa para que, a traves del archivo
     * application-dev.properties, se tome la entidad remitente del
     * mail.
     */
    @Value("${midas.email.personal}")
    private String personal;

    /**
     * Constructor que parametriza las variables templateEngine,
     * mailSender e emailSettingsRepository. Ademas, se implementa
     * el metodo para obtener los mail destinatarios de la db.
     * @param templateEngine Objeto que contiene el template para
     *                       el cuerpo del mail.
     * @param mailSender Objeto que contiene el mensaje a enviar
     *                   en el mail.
     * @param emailSettingsRepository Objeto que implementa la
     *                                tabla EMAIL_SETTINGS de la
     *                                db.
     */
    @Autowired
    public EmailService(TemplateEngine templateEngine, JavaMailSender mailSender, EmailSettingsRepository emailSettingsRepository) {
        this.templateEngine = templateEngine;
        this.mailSender = mailSender;
        this.emailSettingsRepository = emailSettingsRepository;
        getEmails();
    }

    /**
     * Metodo que toma todos los emails de la tabla EMAIL_SETTINGS de la
     * db y los asigna a la variable "to" implementando una variable
     * auxiliar "aux".
     */
    public void getEmails(){
        List<EmailSettings> emails = emailSettingsRepository.findAll();
        List<String> aux = new ArrayList<>();
        for(EmailSettings item:emails){ aux.add(item.getEmail()); }
        to = aux.stream().toArray(String[]::new);
    }

    /**
     * Metodo que se implementa para informar el cargue satisfactorio de un proceso
     * tomando todos los objetos para enviar el respectivo mail.
     * @param filename File name of processed file
     */
    public void sendFinishProcess(String filename) {
        try {
            MimeMessage message = this.mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setFrom(this.from);
            helper.setTo(this.to);
            helper.setSubject("Finalización de proceso");
            Context context = new Context();
            context.setVariable("header", "El proceso de cargue a terminado satisfactoriamente.");
            context.setVariable("body", "Archivo cargado: '"+ filename +"'");
            String process = this.templateEngine.process("mail.html", context);
            helper.setText(process, true);
            this.mailSender.send(message);
            logger.info("Se ha enviado el correo satisfactoriamente");
        }catch (MessagingException exception){
            logger.error("Ha ocurrido un error enviando el correo electrónico");
        }
    }

    /**
     * Metodo que se implementa para informar si ha ocurrido un error
     * buscando un catalogo existente y tomando todos los objetos para
     * enviar el respectivo mail.
     * @param catalogue String que contiene el nombre del catalogo
     *                  donde se busco el valor.
     * @param expectedValue String que contiene el valor no
     *                      encontrado.
     */
    public void sendWriteErrorProcess(String catalogue, String expectedValue) {
        try {
            MimeMessage message = this.mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setFrom(this.from);
            helper.setTo(this.to);
            helper.setSubject("Error en proceso");
            Context context = new Context();
            context.setVariable("header", "Ha ocurrido un error en la escritura de la información.");
            context.setVariable("body", "El valor '" + expectedValue + "' No ha sido encontrado en el catálogo '" + catalogue + "'.");
            String process = this.templateEngine.process("mail.html", context);
            helper.setText(process, true);
            this.mailSender.send(message);
            logger.info("Se ha enviado el correo satisfactoriamente");
        }catch (MessagingException exception){
            logger.error("Ha ocurrido un error enviando el correo electrónico");
        }
    }

    public void sendWriteErrorProcess(String entity, String entityId, String historial) {
        try {
            MimeMessage message = this.mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setFrom(this.from);
            helper.setTo(this.to);
            helper.setSubject("Error en proceso");
            Context context = new Context();
            context.setVariable("header", "Ha ocurrido un error en la escritura de la información.");
            context.setVariable("body", "No se ha encontrado el '" + entity + "' con identificador '" + entityId + "' durante el proceso de carga '" +historial+ "'.");
            String process = this.templateEngine.process("mail.html", context);
            helper.setText(process, true);
            this.mailSender.send(message);
            logger.info("Se ha enviado el correo satisfactoriamente");
        }catch (MessagingException exception){
            logger.error("Ha ocurrido un error enviando el correo electrónico");
        }
    }

    public void sendWriteErrorProcess(String messageStr) {
        try {
            MimeMessage message = this.mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setFrom(this.from);
            helper.setTo(this.to);
            helper.setSubject("Error en proceso");
            Context context = new Context();
            context.setVariable("header", "Ha ocurrido un error en la escritura de la información.");
            context.setVariable("body", messageStr);
            String process = this.templateEngine.process("mail.html", context);
            helper.setText(process, true);
            this.mailSender.send(message);
            logger.info("Se ha enviado el correo satisfactoriamente");
        }catch (MessagingException exception){
            logger.error("Ha ocurrido un error enviando el correo electrónico");
        }
    }

    /**
     * Metodo que se implementa para informar si ha ocurrido un error
     * al verificar la cantidad de logs establecidos entre el archivo
     * log y el liquidador.
     * @param filename String que contiene el nombre del liquidador que
     *                 obtuvo el error.
     */
    public void sendLogErrorProcess(String filename) {
        try {
            MimeMessage message = this.mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setFrom(this.from);
            helper.setTo(this.to);
            helper.setSubject("Error en proceso");
            Context context = new Context();
            context.setVariable("header", "Ha ocurrido un error en el proceso de carga de la información.");
            context.setVariable("body", "La cantidad de registros en el archivo '" + filename + "' No coincide con la cantidad enunciada en el Log.");
            String process = this.templateEngine.process("mail.html", context);
            helper.setText(process, true);
            this.mailSender.send(message);
            logger.info("Se ha enviado el correo satisfactoriamente");
        }catch (MessagingException exception){
            logger.error("Ha ocurrido un error enviando el correo electrónico");
        }
    }


    /*public void sendEmail(String to, String subject, String content) {
        try {
            SimpleMailMessage email = new SimpleMailMessage();
            email.setTo(to);
            email.setSubject(subject);
            email.setText(content);
            this.mailSender.send(email);
            logger.info("Se ha enviado el correo satisfactoriamente");
        }catch (Exception exception){
            logger.error("Ha ocurrido un error enviando el correo electrónico");
        }
    }*/

    /**
     * Method that seds the email to notify that a mandatory field is null or empty
     * @param job is the name of the batch process which sends the email (example: "cambioEstado")
     * @param file to be attached that contains the report of errors
     */
    public void sendVerificationErrorReport(String job, File file) {
        try {
            MimeMessage message = this.mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(this.from);
            helper.setTo(this.to);
            helper.setSubject("Reporte de registros fallidos");
            Context context = new Context();
            context.setVariable("header", "Se encontrarron inconsistencias durane durante la ejecucion del proceso de " + job + "\n Ruta con inconsistencias: " + file.getPath());
            String process = this.templateEngine.process("mail.html", context);
            helper.setText(process, true);
            this.mailSender.send(message);
            logger.info("Se ha enviado el correo satisfactoriamente");
        }catch (MessagingException exception){
            logger.error("Ha ocurrido un error enviando el correo electrónico");
        }
    }
    /**
     * Method that sends the email to notify that a file has exceeded the max attempts
     * @param nombreArchivo file name
     */
    public void sendMaxAttemptsExceed(String nombreArchivo) {
        try {
            MimeMessage message = this.mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(this.from);
            helper.setTo(this.to);
            helper.setSubject("Máximo de intentos alcanzados");
            Context context = new Context();
            context.setVariable("header", "Se ha excedido el límite de intentos para el proceso de cargue.");
            context.setVariable("body", "Nombre del archivo: " + nombreArchivo);
            String process = this.templateEngine.process("mail.html", context);
            helper.setText(process, true);
            this.mailSender.send(message);
            logger.info("Se ha enviado el correo satisfactoriamente");
        }catch (MessagingException exception){
            logger.error("Ha ocurrido un error enviando el correo electrónico");
        }
    }


    /**
     *  Method used to send information about Cartera a Casas de Cobranza
     * @param table Table to sum up the information
     **/
    public void sendPortfolioInfo(List<Map<String, Serializable>> table){
        try{
            MimeMessage message = this.mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setFrom(this.from);
            helper.setTo(this.to);
            helper.setSubject("Resultados - Generación de información de casas de cobranza");
            Context context = new Context();
            context.setVariable("table", table);
            String process = this.templateEngine.process("casas-de-cobranza.html", context);
            helper.setText(process, true);
            this.mailSender.send(message);
            logger.info("Se ha enviado el correo satisfactoriamente");
        }catch (MessagingException exception){
            logger.error("Ha ocurrido un error enviando el correo electrónico");
        }
    }
}
