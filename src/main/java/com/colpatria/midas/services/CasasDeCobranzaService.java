package com.colpatria.midas.services;

import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.HistorialCarteraLight;
import com.colpatria.midas.repositories.HistorialCarteraLightRepository;
import com.colpatria.midas.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Casa de cobranza Service
 */
@Service
public class CasasDeCobranzaService {

    /**
     * Email Service bean reference
     */
    private final EmailService emailService;

    /**
     * Historial Cartera Light bean reference
     */
    private final HistorialCarteraLightRepository historialCarteraLightRepository;

    /**
     * Class Constructor
     * @param emailService Email Service bean Reference
     * @param historialCarteraLightRepository Historial Cartera Light Repository
     */
    @Autowired
    public CasasDeCobranzaService(EmailService emailService, HistorialCarteraLightRepository historialCarteraLightRepository){
        this.emailService = emailService;
        this.historialCarteraLightRepository = historialCarteraLightRepository;
    }

    public void generateCasasDeCobranza(){
        String fechaReporte = this.historialCarteraLightRepository.getLastLoad();
        List<HistorialCarteraLight> historiales = this.historialCarteraLightRepository.getHistorialCarteraLightsByFechaReporte(DateUtils.stringToLocalDate(fechaReporte));

        List<Map<String, Serializable>> report = new ArrayList<>();
        // TODO: GENERAR REPORTE
    }

    // maybe should join with next
    private Map<String, Serializable> mapUserData(HistorialCarteraLight historialCarteraLight){
        Cliente cliente = historialCarteraLight.getServicio().getContrato().getCliente();
        Map<String, Serializable> data = new LinkedHashMap<>();
        String[] names = cliente.getNombre().split("");
        data.put("Nombre1", names[0]);
        data.put("Nombre2", names[1]);
        data.put("Apellido1", names[2]);
        data.put("Apellido2", names[3]);
        data.put("Numide", cliente.getNumeroIdentificacion());
        data.put("Tipide", cliente.getTipoDocumento().getCodigo());
        data.put("Teléfono Casa", null);
        data.put("Teléfono Oficina", null);
        data.put("Celular", null);
        data.put("Dirección 1", cliente.getCuenta().getDireccion());
        data.put("Ciudad 1", cliente.getCuenta().getMunicipio());
        data.put("Codigo DANE", null);
        data.put("correo Electrónico", null);

        return data;
    }

    private Map<String, Serializable> mapLastHistory(HistorialCarteraLight historialCarteraLight){
        Map<String, Serializable> map = new LinkedHashMap<>();
        map.put("numide", historialCarteraLight.getServicio().getContrato().getCliente().getNumeroIdentificacion());
        map.put("numbobl",historialCarteraLight.getServicio().getContrato().getNumero());
        map.put("saldo_k", historialCarteraLight.getServicio().getSaldoCapitalPorFacturar());
        map.put("diasmo1", DAYS.between(historialCarteraLight.getFechaSegundoVencimiento(), LocalDate.now()));
        map.put("diasmo2", DAYS.between(historialCarteraLight.getFechaPrimerVencimiento(), LocalDate.now()));
        map.put("edad1", null); //?
        map.put("edad2", null); //?
        map.put("cuomor", null); //faltan datos
        map.put("saldo_venc", historialCarteraLight.getCapitalImpago()+historialCarteraLight.getInteresImpago()); //?
        map.put("fec_redif", null); // ??
        map.put("val_pago", null);
        map.put("valor_cuot", historialCarteraLight.getServicio().getValorCuota());
        map.put("cant_pagos", historialCarteraLight.getServicio().getCuotasPorFacturar()); // ?
        map.put("fec_vence", historialCarteraLight.getFechaSegundoVencimiento());
        map.put("tipo_tarj", historialCarteraLight.getServicio().getContrato().getSubProducto().getNombre());
        map.put("proend", historialCarteraLight.getServicio().getContrato().getSubProducto().getProductoPadre().getNombre());
        map.put("diasmo_pro", null); // preguntar
        map.put("pag_minimo", null);
        map.put("m_libros", null);
        map.put("esp", historialCarteraLight.getServicio().getContrato().getCliente().getCuenta().getMunicipio());
        map.put("tipide", historialCarteraLight.getServicio().getContrato().getCliente().getTipoDocumento().getNombre());
        map.put("numide1", historialCarteraLight.getServicio().getContrato().getCliente().getTipoDocumento().toString()+
                historialCarteraLight.getServicio().getContrato().getCliente().getTipoDocumento().toString());
        map.put("famparada", null); // ??
        map.put("docto_amp", null);
        map.put("tipo_tarje", historialCarteraLight.getServicio().getContrato().getSubProducto().getTipo().getNombre());
        map.put("fec_ult_re", null);
        map.put("saldo_tot", historialCarteraLight.getServicio().getSaldoDeuda());
        map.put("cant_refi", null);
        map.put("tip_modi1", null);
        map.put("fec_modif", null);
        map.put("fec_segu", historialCarteraLight.getFechaSegundoVencimiento());
        map.put("numero_cli", historialCarteraLight.getServicio().getContrato().getCliente().getCuenta().getNumero());
        map.put("fecha_repo", null);
        map.put("indapert", null);
        map.put("cant_apert", null);
        map.put("asiignado", null);
        map.put("fecutil", null);
        map.put("ind_iloc", historialCarteraLight.getServicio().getContrato().getCliente().getContrato().getFacturacion());
        map.put("cant_comp", null);
        map.put("val_comp", null);
        map.put("CFECACMV", null); // f
        map.put("cvlrdes", historialCarteraLight.getServicio().getValorCompra());// ?????
        map.put("INTCTE", historialCarteraLight.getTotalInteresCorrienteCausado());
        map.put("INTMOR", historialCarteraLight.getTotalInteresMoraCausado());
        map.put("INTTOT", historialCarteraLight.getTotalInteresCorrienteCausado()+historialCarteraLight.getTotalInteresMoraCausado());
        map.put("CMANEJO", historialCarteraLight.getServicio().getValorCuota());// def si
        map.put("GCOBRAN", null);
        map.put("CTASAIN", historialCarteraLight.getServicio().getTasa());//def si
        map.put("CICLO", historialCarteraLight.getServicio().getContrato().getCliente().getCuenta().getCicloFacturacion());
        map.put("VALPAG", null);//
        map.put("FECPAG", null);//??
        map.put("Sucursal", historialCarteraLight.getServicio().getContrato().getCliente().getCuenta().getSucursal().getNumero());
        map.put("Fecha", null);
        map.put("plazo", null);
        map.put("Número Obligación Cobranzas", historialCarteraLight.getServicio().getContrato().getNumero()+
                historialCarteraLight.getServicio().getContrato().getCliente().getCuenta().getNumero().toString());
        map.put("Flag Reclamo", null);

        return map;
    }
}
