package com.colpatria.midas.services;

import com.colpatria.midas.model.CalendarioFacturacion;
import com.colpatria.midas.model.HistorialCalendarioFacturacion;
import com.colpatria.midas.model.Usuario;
import com.colpatria.midas.repositories.CalendarioFacturacionRepository;
import com.colpatria.midas.repositories.HistorialCalendarioFacturacionRepository;
import com.colpatria.midas.repositories.SucursalRepository;
import com.colpatria.midas.repositories.UsuarioRepository;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.StreamSupport;

/**
 * Billing Schedule Service
 */
@Service
public class CalendarioFacturacionService {
    /**
     * Billing Schedule Repository
     */
    private final CalendarioFacturacionRepository calendarioFacturacionRepository;
    /**
     * Billing Schedule History Repository
     */
    private final HistorialCalendarioFacturacionRepository historialCalendarioFacturacionRepository;
    /**
     * Usuario repository
     */
    private final UsuarioRepository usuarioRepository;
    /**
     * Sucursal Repository
     */
    private final SucursalRepository sucursalRepository;
    /**
     * Class constructor
     * @param calendarioFacturacionRepository Billing Schedule Repository
     * @param historialCalendarioFacturacionRepository Billing Schedule History Repository
     * @param usuarioRepository Usuario repository
     * @param sucursalRepository Sucursal Repository
     */
    public CalendarioFacturacionService(CalendarioFacturacionRepository calendarioFacturacionRepository,
                                        HistorialCalendarioFacturacionRepository historialCalendarioFacturacionRepository,
                                        UsuarioRepository usuarioRepository,
                                        SucursalRepository sucursalRepository) {
        this.calendarioFacturacionRepository = calendarioFacturacionRepository;
        this.historialCalendarioFacturacionRepository = historialCalendarioFacturacionRepository;
        this.usuarioRepository = usuarioRepository;
        this.sucursalRepository = sucursalRepository;
    }

    /**
     * Get the last uploaded version String
     * @return the String containing the year and version
     */
    public String getLastVersion(){
        HistorialCalendarioFacturacion historialCalendarioFacturacion = historialCalendarioFacturacionRepository.getFirstByOrderByFechaDesc();
        if(historialCalendarioFacturacion != null){
            return historialCalendarioFacturacion.getVersion();
        }else{
            return "";
        }

    }

    /**
     * Validates if the file with this version and period has already been uploaded
     *
     * @param version version of the schedule
     * @return true if no register match the version and period
     */
    public boolean validateUpload(String version){
        return !historialCalendarioFacturacionRepository.existsByVersion(version);
    }

    /**
     * Uploads the Excel file to the database
     *
     * @param file    The inputStream that reprecents the file to upload
     * @param version the version that the file represents
     */
    public boolean uploadNewVersion(MultipartFile file, String version){
        if(!validateUpload(version)){
            return false;
        }
        String format = Arrays.stream(Objects.requireNonNull(file.getOriginalFilename()).split("\\.")).reduce((f,l)->l).orElse("");
        List<CalendarioFacturacion> entries;
        try {
            switch(format){
                case "xlsx":
                    entries = readFromExcel(file.getInputStream(), version);
                    break;
                case "csv":
                    entries = readFromCSV(file.getInputStream(), version);
                    break;
                default:
                    return false;
            }
            if(entries.isEmpty()){
                return false;
            }
            calendarioFacturacionRepository.saveAll(entries);
            if(!usuarioRepository.existsById("1")){
                usuarioRepository.save(new Usuario("1","area","nombres"));
            }
            HistorialCalendarioFacturacion historialCalendarioFacturacion=new HistorialCalendarioFacturacion(
                    null,
                    version,
                    LocalDateTime.now(),
                    usuarioRepository.getById("1")
            );
            historialCalendarioFacturacionRepository.save(historialCalendarioFacturacion);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Reads the Billing Schedule from a CSV file
     * @param version the version of the schedule
     * @return the array with the billing schedule registers
     */
    public List<CalendarioFacturacion> readFromCSV(InputStream file, String version){
        try (
                Reader reader = new InputStreamReader(file);
                CSVParser csvParser = new CSVParser(reader, CSVFormat.Builder.create().setHeader().setSkipHeaderRecord(true).build())
        ) {
            ArrayList<CalendarioFacturacion> entries = new ArrayList<>();
            for (CSVRecord csvRecord : csvParser) {
                entries.add(new CalendarioFacturacion(
                        null,
                        version,
                        LocalDate.parse(csvRecord.get(0)+"01", DateTimeFormatter.ofPattern("yyyyMMdd")),
                        sucursalRepository.getById(Integer.parseInt(csvRecord.get(1))),
                        Integer.parseInt(csvRecord.get(2)),
                        LocalDate.parse(csvRecord.get(3)),
                        LocalDate.parse(csvRecord.get(4)),
                        LocalDate.parse(csvRecord.get(5)),
                        LocalDate.parse(csvRecord.get(6)),
                        LocalDate.parse(csvRecord.get(7)),
                        LocalDate.parse(csvRecord.get(8)),
                        csvRecord.get(9)
                ));
            }
            return entries;
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
    /**
     * Reads the Billing Schedule from a Excel file
     * @param version the version of the schedule
     * @return the array with the billing schedule registers
     */
    public List<CalendarioFacturacion> readFromExcel(InputStream file, String version){
        try (Workbook workbook = WorkbookFactory.create(file)){
            ArrayList<CalendarioFacturacion> entries = new ArrayList<>();
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> it = sheet.rowIterator();
            for (it.next(); it.hasNext(); ) {
                Row row = it.next();
                if(StreamSupport.stream(row.spliterator(), false).count() <2) continue;
                entries.add(new CalendarioFacturacion(
                        null,
                        version,
                        LocalDate.parse(String.format("%.0f",row.getCell(0).getNumericCellValue())+"01", DateTimeFormatter.ofPattern("yyyyMMdd")),
                        sucursalRepository.getById((int) row.getCell(1).getNumericCellValue()),
                        (int) row.getCell(2).getNumericCellValue(),
                        row.getCell(3).getLocalDateTimeCellValue().toLocalDate(),
                        row.getCell(4).getDateCellValue().toInstant()
                                .atZone(ZoneId.systemDefault())
                                .toLocalDate(),
                        row.getCell(5).getDateCellValue().toInstant()
                                .atZone(ZoneId.systemDefault())
                                .toLocalDate(),
                        row.getCell(6).getDateCellValue().toInstant()
                                .atZone(ZoneId.systemDefault())
                                .toLocalDate(),
                        row.getCell(7).getDateCellValue().toInstant()
                                .atZone(ZoneId.systemDefault())
                                .toLocalDate(),
                        row.getCell(8).getDateCellValue().toInstant()
                                .atZone(ZoneId.systemDefault())
                                .toLocalDate(),
                        row.getCell(9).getStringCellValue()
                ));
            }
            return entries;
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}
