package com.colpatria.midas.services;

import com.colpatria.midas.model.HistorialArchivos;
import com.colpatria.midas.repositories.HistorialArchivosRepository;
import com.colpatria.midas.utils.DateUtils;
import com.colpatria.midas.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;

/**
 * Fila History Service
 */
@Service
public class FileHistoryService {

    /**
     * Pending Files, Folder Path
     */
    @Value("${midas.jobs.path.pending}")
    private String pendingPath;

    /**
     * Scheduled Files, Folder Path
     */
    @Value("${midas.jobs.path.scheduled}")
    private String scheduledPath;

    /**
     * Retry Files, Folder Path
     */
    @Value("${midas.jobs.path.retry}")
    private String retryPath;

    /**
     * Completed Files, Folder Path
     */
    @Value("${midas.jobs.path.completed}")
    private String completedPath;

    /**
     * Error Files, Folder Path
     */
    @Value("${midas.jobs.path.error}")
    private String errorPath;

    /**
     * Error Files, Folder Path
     */
    @Value("${midas.jobs.path.logs}")
    private String logPath;

    /**
     * Max Retries per job
     */
    @Value("${midas.jobs.maxretries}")
    private int maxretries;

    /**
     * Historial Archivos Repository
     */
    private final HistorialArchivosRepository historialArchivosRepository;

    /**
     * Constructor
     *
     * @param historialArchivosRepository Historial Archivos bean
     */
    @Autowired
    public FileHistoryService(HistorialArchivosRepository historialArchivosRepository) {
        this.historialArchivosRepository = historialArchivosRepository;
    }

    /**
     * Set a status to a HistorialArchivos and save it into database
     *
     * @param file   Historial Archivos to change
     * @param status status to set
     */
    public void setStatusToHistorialArchivo(HistorialArchivos file, HistorialArchivos.Status status) {
        HistorialArchivos.Status previousStatus = file.getEstado();
        file.setEstado(status);
        file.setUltimaModificacion( DateUtils.getDatetimeNow() );
        this.historialArchivosRepository.save(file);
        FileUtils.moveFile(this.getPathByStatus(previousStatus) + file.getNombreArchivo(), this.getPathByStatus(status) + file.getNombreArchivo());
    }

    /**
     * Get a Set of Pending HistorialArchivos
     *
     * @return Set of Pending HistorialArchivos
     */
    public Set<HistorialArchivos> getPendingFiles() {
        this.loadPendingFilesToDatabase();
        return this.historialArchivosRepository.getHistorialArchivosByEstado(HistorialArchivos.Status.PENDIENTE);
    }

    /**
     * Update the database with unregistered Pending files
     */
    private void loadPendingFilesToDatabase() {
        Set<String> pendingFiles = FileUtils.listFilesInDirectory(this.pendingPath);
        for (String pendingFile : pendingFiles) {
            HistorialArchivos file = this.historialArchivosRepository.getHistorialArchivosByNombreArchivo(pendingFile);
            if (file == null) {
                file = new HistorialArchivos();
                file.setNombreArchivo(pendingFile);
                file.setEstado(HistorialArchivos.Status.PENDIENTE);
                file.setIntentos(0);
            }
            file.setLog(this.getLogData(file.getNombreArchivo()));
            file.setUltimaModificacion( DateUtils.getDatetimeNow() );
            this.historialArchivosRepository.save(file);
        }
    }

    /**
     * Get Number of registers from log
     *
     * @param filename File name
     * @return Value of number of registers from log
     */
    private Integer getLogData(String filename) {
        Set<String> logFiles = FileUtils.listFilesInDirectory(this.logPath);
        for (String logFile : logFiles) {
            if (logFile.endsWith(filename)) {
                try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.logPath + logFile))) {
                    String data = bufferedReader.readLine();
                    data = bufferedReader.readLine();
                    return Integer.parseInt(data.split("\\|")[0]);
                } catch (IOException exception) {
                    return null;
                }
            }
        }
        return null;
    }

    /**
     * Get a Set of Retry HistorialArchivos
     *
     * @return Set of Retry HistorialArchivos
     */
    public Set<HistorialArchivos> getRetryFiles() {
        return this.historialArchivosRepository.getHistorialArchivosByEstado(HistorialArchivos.Status.REINTENTADO);
    }

    /**
     * Get a 'Historial archivos' reference given a file name
     *
     * @param fileName file name of 'Historial archivos' reference
     * @return 'Historial archivos' refernece
     */
    public HistorialArchivos getHistorialArchivosByFileName(String fileName) {
        return this.historialArchivosRepository.getHistorialArchivosByNombreArchivo(fileName);
    }

    /**
     * Process a failure in a File
     * If retries is less than max retries, status will be 'retry'
     * otherwise, status will be 'Error'
     *
     * @param file file to process
     */
    public void processFailureInFile(HistorialArchivos file) {
        file.setIntentos(file.getIntentos() + 1);
        if(file.getIntentos() < this.maxretries){
            this.setStatusToHistorialArchivo(file, HistorialArchivos.Status.REINTENTADO);
        }else {
            this.setStatusToHistorialArchivo(file, HistorialArchivos.Status.ERROR);
        }
    }

    private String getPathByStatus(HistorialArchivos.Status status) {
        switch (status) {
            case PENDIENTE:
                return this.pendingPath;
            case AGENDADO:
                return this.scheduledPath;
            case REINTENTADO:
                return this.retryPath;
            case ERROR:
                return this.errorPath;
            case FINALIZADO:
                return this.completedPath;
            default:
                return null;
        }
    }

    public Set<HistorialArchivos> getFilesByPrefix(String prefix) {
        this.loadPendingFilesToDatabase();
        return this.historialArchivosRepository.getHistorialArchivosByNombreArchivoStartingWith(prefix);
    }

    public void ProcessFileByPath(String path) {
        HistorialArchivos historialArchivo = this.getHistorialArchivosByPath(path);
        historialArchivo.setEstado(HistorialArchivos.Status.FINALIZADO);
        historialArchivo.setUltimaModificacion( DateUtils.getDatetimeNow() );
        this.historialArchivosRepository.save(historialArchivo);
        FileUtils.moveFile(this.scheduledPath + historialArchivo.getNombreArchivo(), this.completedPath + historialArchivo.getNombreArchivo());
    }

    public void ProcessErrorFileByPath(String path) {
        HistorialArchivos historialArchivo = this.getHistorialArchivosByPath(path);
        historialArchivo.setEstado(HistorialArchivos.Status.ERROR);
        historialArchivo.setUltimaModificacion( DateUtils.getDatetimeNow() );
        this.historialArchivosRepository.save(historialArchivo);
        FileUtils.moveFile(this.scheduledPath + historialArchivo.getNombreArchivo(), this.errorPath + historialArchivo.getNombreArchivo());
    }


    private HistorialArchivos getHistorialArchivosByPath(String path) {
        String[] split = path.split("/");
        String filename = split[split.length - 1];
        return this.historialArchivosRepository.getHistorialArchivosByNombreArchivo(filename);
    }

    public String getPathByFileName(String file) {
        HistorialArchivos historialArchivos = this.historialArchivosRepository.getHistorialArchivosByNombreArchivo(file);
        switch (historialArchivos.getEstado()) {
            case PENDIENTE:
                return this.pendingPath + historialArchivos.getNombreArchivo();
            case AGENDADO:
                return this.scheduledPath + historialArchivos.getNombreArchivo();
            case ERROR:
                return this.errorPath + historialArchivos.getNombreArchivo();
            case FINALIZADO:
                return this.completedPath + historialArchivos.getNombreArchivo();
            case REINTENTADO:
                return this.retryPath + historialArchivos.getNombreArchivo();
            default:
                return null;
        }
    }

}
