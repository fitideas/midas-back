package com.colpatria.midas.listeners;

import com.colpatria.midas.dto.CasasDeCobranzaProbeDto;
import com.colpatria.midas.services.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

import java.io.Serializable;
import java.util.*;

/**
 * Report Generator Job Listener
 */
public class ReportGeneratorJobListener extends JobExecutionListenerSupport implements ItemWriteListener<CasasDeCobranzaProbeDto> {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ReportGeneratorJobListener.class);

    /**
     * Email Service bean reference
     */
    private final EmailService emailService;

    private final Map<String, Integer> registros;
    private final Map<String, Double> saldoK;
    private final Map<String, Double> cHMora;

    /**
     * Class constructor
     * @param emailService Email Service bean reference
     */
    public ReportGeneratorJobListener(EmailService emailService) {
        this.emailService = emailService;
        this.registros = new LinkedHashMap<>();
        this.saldoK = new LinkedHashMap<>();
        this.cHMora = new LinkedHashMap<>();
    }

    /**
     * After Job Hook
     * @param jobExecution Job Execution data
     */
    @Override
    public void afterJob(JobExecution jobExecution) {
        if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
            List<Map<String, Serializable>> table = new ArrayList<>();
            Set<String> tarjetas = this.registros.keySet();
            for (String key : tarjetas) {
                Map<String, Serializable> map = new LinkedHashMap<>();
                map.put("tipo_tarje", key);
                map.put("Registros", this.registros.get(key));
                map.put("saldo_k", this.saldoK.get(key));
                map.put("ch_mora", this.cHMora.get(key));
                table.add(map);
            }
            this.emailService.sendPortfolioInfo(table);
        }
    }

    @Override
    public void beforeWrite(List<? extends CasasDeCobranzaProbeDto> list) {

    }

    @Override
    public void afterWrite(List<? extends CasasDeCobranzaProbeDto> list) {
        for(CasasDeCobranzaProbeDto item : list){
            if(!registros.containsKey(item.getTipoTarj())){
                registros.put(item.getTipoTarj(), 1);
                saldoK.put(item.getTipoTarj(), Double.parseDouble(item.getSaldoK()));
                cHMora.put(item.getTipoTarj(), Double.parseDouble(item.getSaldoTot()));
            }else{
                registros.put(item.getTipoTarj(), registros.get(item.getTipoTarj()) + 1);
                saldoK.put(item.getTipoTarj(), saldoK.get(item.getTipoTarj()) + Double.parseDouble(item.getSaldoK()));
                cHMora.put(item.getTipoTarj(), cHMora.get(item.getTipoTarj()) + Double.parseDouble(item.getSaldoTot()));
            }

        }
    }

    @Override
    public void onWriteError(Exception e, List<? extends CasasDeCobranzaProbeDto> list) {

    }
}
