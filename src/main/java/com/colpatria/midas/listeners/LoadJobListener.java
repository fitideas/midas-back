package com.colpatria.midas.listeners;

import com.colpatria.midas.exceptions.IncompatibleLogException;
import com.colpatria.midas.exceptions.InvalidStructureException;
import com.colpatria.midas.model.HistorialArchivos;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Job Listener used in all data load processes
 */
public class LoadJobListener extends JobExecutionListenerSupport {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(LoadJobListener.class);

    /**
     * Email Service
     */
    private final EmailService emailService;

    /**
     * File History Service
     */
    private final FileHistoryService fileHistoryService;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * Log Email Path
     */
    @Value("${midas.jobs.path.logs_email}")
    private String logEmailPath;

    /**
     * Cache manager to use
     */
    private final CacheManager cacheManager;

    /**
     * Constructor
     *  @param emailService       Email Service Bean reference
     * @param fileHistoryService File History Service bean reference
     * @param validationService  Validation Service bean reference
     * @param cacheManager DI of cache manager
     */
    public LoadJobListener(EmailService emailService, FileHistoryService fileHistoryService, ValidationService validationService, CacheManager cacheManager) {
        this.emailService = emailService;
        this.fileHistoryService = fileHistoryService;
        this.validationService = validationService;
        this.cacheManager = cacheManager;
    }

    /**
     * Before Job Execution Resolver
     *
     * @param jobExecution Job Execution reference
     */
    @Override
    public void beforeJob(JobExecution jobExecution) {
        logger.info("The job '{}_{}' has been triggered", jobExecution.getJobInstance().getJobName(), jobExecution.getId());
    }

    /**
     * Before Job Execution Resolver
     *
     * @param jobExecution Job Execution reference
     */
    @Override
    public void afterJob(JobExecution jobExecution) {
        if (this.validationService.getHasError()) {
            String reportName = "report_" + jobExecution.getJobParameters().getString("file");
            File report = new File(this.logEmailPath + reportName);
            jobExecution.setStatus(BatchStatus.FAILED);
            try (FileWriter writer = new FileWriter(report)) {
                writer.write(this.validationService.getRegisterReport().toString());
                writer.flush();
                this.validationService.resetValues();
                this.emailService.sendVerificationErrorReport(jobExecution.getJobInstance().getJobName(), report);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        HistorialArchivos file = this.fileHistoryService.getHistorialArchivosByFileName(jobExecution.getJobParameters().getString("file"));
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            logger.info("The job '{}_{}' has finished successfully", jobExecution.getJobInstance().getJobName(), jobExecution.getId());
            this.fileHistoryService.setStatusToHistorialArchivo(file, HistorialArchivos.Status.FINALIZADO);
            this.emailService.sendFinishProcess(file.getNombreArchivo());
        } else {
            logger.error("The job '{}_{}' couldn't been completed", jobExecution.getJobInstance().getJobName(), jobExecution.getId());
            this.fileHistoryService.processFailureInFile(file);
            List<Throwable> exceptions = jobExecution.getAllFailureExceptions();
            for (Throwable exception : exceptions) {
                if (exception.getCause() instanceof InvalidStructureException) {
                    InvalidStructureException ex = (InvalidStructureException) exception.getCause();
                    this.emailService.sendWriteErrorProcess("Se encontro un error en la estrucutra del archivo " + ex.getFileName());
                } else if (exception.getCause() instanceof IncompatibleLogException) {
                    IncompatibleLogException ex = (IncompatibleLogException) exception.getCause();
                    this.emailService.sendLogErrorProcess(ex.getFileName());
                }
                // TODO: ADD NEXT EXCEPTIONS
            }
            if (file.getEstado() == HistorialArchivos.Status.ERROR) {
                this.emailService.sendMaxAttemptsExceed(file.getNombreArchivo());
            }
        }
        cacheManager.getCacheNames().stream()
                .forEach( cacheName -> cacheManager.getCache( cacheName ).clear() );
    }
}
