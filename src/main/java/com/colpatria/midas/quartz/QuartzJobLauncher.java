package com.colpatria.midas.quartz;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class QuartzJobLauncher extends QuartzJobBean {

	private static final Logger log = LoggerFactory.getLogger(QuartzJobLauncher.class);

	private String jobName;
	private JobLauncher jobLauncher;
	private JobLocator jobLocator;

	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public JobLauncher getJobLauncher() {
		return jobLauncher;
	}
	public void setJobLauncher(JobLauncher jobLauncher) {
		this.jobLauncher = jobLauncher;
	}
	public JobLocator getJobLocator() {
		return jobLocator;
	}
	public void setJobLocator(JobLocator jobLocator) {
		this.jobLocator = jobLocator;
	}

	@Override
	protected void executeInternal(JobExecutionContext context) {
		try {
	 		Job job = this.jobLocator.getJob(this.jobName);
			JobDataMap dataMap = context.getJobDetail().getJobDataMap();
	 		JobExecution jobExecution = this.jobLauncher.run(job, this.buildParameters(dataMap));
	 		log.info("{}_{} job excecution has been completed", job.getName(), jobExecution.getId());
	 	} catch (Exception e) {
 			log.error("Encountered job execution exception!, {} {}", e.getMessage(), e.getStackTrace());
	 	}
	}

	private JobParameters buildParameters(JobDataMap jobDataMap){
		JobParametersBuilder builder = new JobParametersBuilder();
		for(Map.Entry<String, Object> entry : jobDataMap.entrySet()){
			if(Objects.equals(entry.getKey(), "jobName") || Objects.equals(entry.getKey(),"jobLauncher") || Objects.equals(entry.getKey(),"jobLocator")){
				continue;
			}
            if(entry.getValue() instanceof String){
                builder.addString(entry.getKey(), (String) entry.getValue());
            }
		}
		builder.addDate("jobTimeExecution", new Date());
		return builder.toJobParameters();
	}

}
