package com.colpatria.midas.exceptions;

/**
 * Class that interrupts the flow when a query is invalid
*/
public class InvalidQueryException extends Exception {

    /**
     * Constructor of the exception
     * @param message is the exception message
    */
    public InvalidQueryException( String message ) {
        super( message );
    }
    
}
