package com.colpatria.midas.exceptions;

import static org.aspectj.weaver.ResolvedTypeMunger.Parent;

/**
 * Class for implementing when a parent was not found in a history
 */
public class ParentNotFoundException extends Exception{

    /**
     * String that represents the unbelong children
     */
    private String children;

    /**
     * String that represents the parent where the system find the exception
     */
    private String parent;

    /**
     * String that represents the children ID
     */
    private String childrenId;

    /**
     * String that represents the parent ID
     */
    private String parentId;

    /**
     * Constructor of the class where the value is given to the class objects
     * @param children String that represents the unbelong children
     * @param parent String that represents the parent where the system find the exception
     * @param childrenId String that represents the children ID
     * @param parentId String that represents the parent ID
     */
    public ParentNotFoundException(String children, String parent, String childrenId, String parentId){
        super("Entity'" + children + "' with id '"+ childrenId +"' does not belong to  '" + parent +" with id '"+parentId+"'.");

        this.children = children;
        this.parent = parent;
        this.childrenId = childrenId;
        this.parentId = parentId;
    }

    /**
     * Get the children of a class
     * @return String that represents the unbelong children
     */
    public String getChildren() {
        return children;
    }

    /**
     * Set the children of a class
     * @param children String that represents the unbelong children
     */
    public void setChildren(String children) {
        this.children = children;
    }

    /**
     * Get the parent of a class
     * @return String that represents the parent where the system find the exception
     */
    public String getParent() {
        return parent;
    }

    /**
     * Set the parent of a class
     * @param parent String that represents the parent where the system find the exception
     */
    public void setParent(String parent) {
        this.parent = parent;
    }

    /**
     * Get the children ID of a class
     * @return String that represents the children ID
     */
    public String getChildrenId() {
        return childrenId;
    }

    /**
     * Set the children ID of a class
     * @param childrenId String that represents the children ID
     */
    public void setChildrenId(String childrenId) {
        this.childrenId = childrenId;
    }

    /**
     * Get the parent ID of a class
     * @return String that represents the parent ID
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * Set the parent ID of a class
     * @param parentId String that represents the parent ID
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
