package com.colpatria.midas.exceptions;

/**
 * Class for implementing when an entity was not found in a history
 */
public class EntityNotFoundHistoryException extends Exception{

    /**
     * String that refers to the entity unfound.
     */
    private String entity;

    /**
     * String that refers to the entity unfound ID.
     */
    private String entityId;

    /**
     * String that refers to the history where the entity was unfound.
     */
    private String history;


    /**
     * Constructor of the class where the values are given to the class objects
     * @param entity String that refers to the entity unfound.
     * @param entityId String that refers to the entity unfound ID.
     * @param history String that refers to the history where the entity was unfound.
     */
    public EntityNotFoundHistoryException(String entity, String entityId, String history) {
        super("Entity'" + entity + "' with id '"+ entityId +"' has not been found while processing  '" + history +"'.");
        this.entity = entity;
        this.entityId = entityId;
        this.history = history;
    }

    /**
     * Get the entity of a class
     * @return String that refers to the entity unfound.
     */
    public String getEntity() {
        return this.entity;
    }

    /**
     * Set the entity of a class
     * @param entity String that refers to the entity unfound.
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * Get the ID identity of a class.
     * @return String that refers to the entity unfound ID.
     */
    public String getEntityId() {
        return this.entityId;
    }

    /**
     * Set the ID identity of a class.
     * @param entityId String that refers to the entity unfound ID.
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * Get the history of a class
     * @return String that refers to the history where the entity was unfound.
     */
    public String getHistory() {
        return this.history;
    }

    /**
     * Set the history of a class
     * @param history String that refers to the history where the entity was unfound.
     */
    public void setHistory(String history) {
        this.history = history;
    }
    
    
}
