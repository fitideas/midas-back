package com.colpatria.midas.exceptions;

/**
 * Exception to control file structure
 */
public class InvalidStructureException extends Exception{

    /**
     * File name with fail
     */
    private String fileName;

    /**
     * Process with fail
     */
    private String process;

    /**
     * @param fileName with fail
     * @param process with fail
     */
    public InvalidStructureException(String fileName, String process) {
        super("The file '" + fileName + "' has invalid structure for  '"+ process +"'.");
        this.fileName = fileName;
        this.process = process;
    }

    /**
     * @return get file name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName set file name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return get process
     */
    public String getProcess() {
        return process;
    }

    /**
     * @param process set process
     */
    public void setProcess(String process) {
        this.process = process;
    }
}
