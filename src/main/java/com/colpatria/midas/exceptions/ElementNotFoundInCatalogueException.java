package com.colpatria.midas.exceptions;

/**
 * Class for implement ing when an element isn't found
 * in catalog table in the db
 */
public class ElementNotFoundInCatalogueException extends Exception{

    /**
     * String that refers to the catalogue name where the exception occurred.
     */
    private String catalogueName;

    /**
     * String that refers to the expected value that the system was looking for in the catalogue.
     */
    private String expectedValue;

    /**
     * Constructor of the class where the value is given to the class objects
     * @param catalogueName String that refers to the catalogue name where the exception occurred.
     * @param expectedValue String that refers to the expected value that the system was looking for in the catalogue.
     */
    public ElementNotFoundInCatalogueException(String catalogueName, String expectedValue) {
        super("Element '" + expectedValue + "' has not been found in catalogue '" + catalogueName + "'.");
        this.catalogueName = catalogueName; this.expectedValue = expectedValue;
    }

    /**
     * Get the catalogue name of a class.
     * @return String that refers to the catalogue name where the exception occurred.
     */
    public String getCatalogueName() {
        return catalogueName;
    }

    /**
     * Set the catalogue name of a class.
     * @param catalogueName  String that refers to the catalogue name where the exception occurred.
     */
    public void setCatalogueName(String catalogueName) {
        this.catalogueName = catalogueName;
    }

    /**
     * Get the expected value of a class.
     * @return String that refers to the expected value that the system was looking for in the catalogue.
     */
    public String getExpectedValue() {
        return expectedValue;
    }

    /**
     * Set the expected value of a class.
     * @param expectedValue String that refers to the expected value that the system was looking for in the catalogue.
     */
    public void setExpectedValue(String expectedValue) {
        this.expectedValue = expectedValue;
    }
}
