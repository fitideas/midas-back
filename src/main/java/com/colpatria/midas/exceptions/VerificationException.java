package com.colpatria.midas.exceptions;

/**
 * This class is used for the exceptions that are generated when a mandatory field is null or empty in a
 * file that is being read in batch process
 */
public class VerificationException extends Exception {

    /**
     * Represents the line of the file where the mandatory field is null or empty
     */
    private Long errorLine;
    /**
     * Name of the file where the exception was found
     */
    private String fileName;

    /**
     * Constructor of the class where the values are given to the class objects
     * @param errorLine Represents the line of the file where the mandatory field is null or empty
     * @param fileName Name of the file where the exception was found
     */
    public VerificationException(Long errorLine, String fileName) {
        super("Error found in line '" + (errorLine) + "' while reading the file '" + fileName +"'.");
        this.errorLine = errorLine;
        this.fileName = fileName;
    }

    /**
     * Get the error line of a class.
     * @return Represents the line of the file where the mandatory field is null or empty
     */
    public Long getErrorLine() {
        return this.errorLine;
    }

    /**
     * Set the error line of a class.
     * @param errorLine Represents the line of the file where the mandatory field is null or empty
     */
    public void setErrorLine(Long errorLine) {
        this.errorLine = errorLine;
    }

    /**
     * Get the file name of a class.
     * @return Name of the file where the exception was found
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
     * Set the file name of a class.
     * @param fileName Name of the file where the exception was found
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    
}
    
