package com.colpatria.midas.exceptions;

/*
 *
 * Clase
 *
*/

public class IncompatibleLogException extends Exception {

    /*
	 *
	 * Atríbutos
	 *
	*/

    /**
     * File name
    */
    private String fileName;

    /*
     *
     * Métodos
     *
    */

    /**
     * Constructor of the exception
     * @param fileName is the name of the file where the error occurred
    */
    public IncompatibleLogException( String fileName ) {
        this.fileName = fileName;
    }

    /**
     * Get the file name
     * @return file name
    */
    public String getFileName() {
        return fileName;
    }
    
}
