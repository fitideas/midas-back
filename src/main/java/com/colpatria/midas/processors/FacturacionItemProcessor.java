package com.colpatria.midas.processors;

import com.colpatria.midas.dto.FacturacionDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.DateUtils;
import org.springframework.batch.item.ItemProcessor;

import java.math.BigInteger;

/**
 * Facturacion Item Processor
 * Converts objects from FacturacionDto to HistorialFacturacion
 */
public class FacturacionItemProcessor implements ItemProcessor<FacturacionDto, HistorialFacturacion> {

    /**
     * File
     */
    private final String file;

    /**
     * Validation Service
     */
    private ValidationService validationService;

    /**
     * Producto Repository
     */
    private ProductoRepository productoRepository;

    /**
     * Tipo Producto Repository
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Tipo Documento Repository
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Sucursal Repository
     */
    private SucursalRepository sucursalRepository;

    /**
     * Cargo Repository
     */
    private CargoRepository cargoRepository;

    /**
     * Cuenta Repository
     */
    private CuentaRepository cuentaRepository;

    /**
     * Cliente Repository
     */
    private ClienteRepository clienteRepository;

    /**
     * Contrato Repository
     */
    private ContratoRepository contratoRepository;

    /**
     * Servicio Repository
     */
    private ServicioRepository servicioRepository;

    /**
     * Historial Facturacion Repository
     */
    private HistorialFacturacionRepository historialFacturacionRepository;


    /**
     * Constructor
     * @param file File name
     */
    public FacturacionItemProcessor(String file) {
        this.file = file;
    }

    /**
     * Process an item
     * @param facturacion item to process
     * @return item processed
     */
    @Override
    public HistorialFacturacion process(FacturacionDto facturacion) throws Exception {

        // Initial Cleaning
        this.validationService.cleanLineErrors();
        this.validationService.increaseRegisterCounter();

        // Date Fields Validation
        this.validationService.verifyDate(facturacion.getFechaDoc(), "Fecha Documento", this.file);
        this.validationService.verifyDate(facturacion.getFechaPr(), "Fecha PR", this.file);

        // Integer Fields Validation
        this.validationService.validateInteger(facturacion.getCodigoSubproducto(), "Codigo SubProducto", this.file);
        this.validationService.validateInteger(facturacion.getCodigoProducto(), "Codigo Producto", this.file);
        this.validationService.validateInteger(facturacion.getCodigoTipoProducto(), "Codigo Tipo Producto", this.file);
        this.validationService.validateInteger(facturacion.getTipoIdentificacion(), "Codigo Tipo Identificacion", this.file);
        this.validationService.validateInteger(facturacion.getNumeroContrato(), "Numero Contrato", this.file);
        this.validationService.validateInteger(facturacion.getSector(), "Sector", this.file);
        this.validationService.validateInteger(facturacion.getEstrato(), "Estrato", this.file);

        // Long Fields Validation
        this.validationService.validateLong(facturacion.getSucursal(), "Sucursal", this.file);

        // Double Fields Validation
        this.validationService.validateDouble(facturacion.getValorFact(), "Valor Facturacion", this.file);
        this.validationService.validateDouble(facturacion.getSaldo(), "Saldo", this.file);
        this.validationService.validateDouble(facturacion.getTotDocumento(), "Total Documento", this.file);

        // Verification Validation
        if (this.validationService.isLineError()) {
            return null;
        }

        // Catalogue Entities
        Producto subproducto = this.productoRepository.getProductoByCodigo(facturacion.getCodigoSubproducto());
        this.validationService.verifyCatalog(subproducto, "Subproducto", this.file);

        Producto producto = this.productoRepository.getProductoByCodigo(facturacion.getCodigoProducto());
        this.validationService.verifyCatalog(producto, "Producto", this.file);

        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(facturacion.getCodigoTipoProducto());
        this.validationService.verifyCatalog(tipoProducto, "Tipo Producto", this.file);

        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(facturacion.getTipoIdentificacion());
        this.validationService.verifyCatalog(tipoDocumento, "Tipo Documento", this.file);

        Sucursal sucursal = this.sucursalRepository.getSucursalByNumero(Integer.parseInt(facturacion.getSucursal()));
        this.validationService.verifyCatalog(sucursal, "Sucursal", this.file);

        this.validationService.validateIntegerWithZero(facturacion.getSector(), "Sector", this.file);

        Cargo cargo = this.cargoRepository.getCargoById(facturacion.getCargo());
        this.validationService.verifyCatalog(cargo, "Cargo", this.file);

        // Verification Validation
        if (this.validationService.isLineError()) {
            return null;
        }

        // Subproducto - producto relationship Validation
        String codigo = subproducto.getProductoPadre() != null ? subproducto.getProductoPadre().getCodigo() : "";
        this.validationService.verifyParent(producto.getCodigo(), codigo, "Producto", "SubProducto", this.file);
        this.validationService.verifyParent(producto.getTipo().getCodigo(), tipoProducto.getCodigo(), "Producto", "Tipo producto", this.file);

        // Verification Validation
        if (this.validationService.isLineError()) {
            return null;
        }

        // Non-Catalogue Validation
        Cuenta cuenta = this.cuentaRepository.getByNumero(Double.parseDouble(facturacion.getNumeroCuenta()));
        if(cuenta == null){
            cuenta = new Cuenta();
            cuenta.setNumero(Double.parseDouble(facturacion.getNumeroCuenta()));
            cuenta.setEstrato(Integer.parseInt(facturacion.getEstrato()));
            cuenta.setSucursal(sucursal);
            cuenta.setCicloFacturacion(Integer.parseInt(facturacion.getSector()));
        }

        Cliente cliente = this.clienteRepository.getByNumeroIdentificacion(Double.parseDouble(facturacion.getNumeroDocumento()));
        if(cliente == null){
            cliente = new Cliente();
            cliente.setTipoDocumento(tipoDocumento);
            cliente.setNumeroIdentificacion(Double.parseDouble(facturacion.getNumeroDocumento()));
            cliente.setCuenta(cuenta);
        }

        Contrato contrato = this.contratoRepository.getByNumero(facturacion.getNumeroContrato());
        if(contrato == null){
            contrato = new Contrato();
            contrato.setNumero(facturacion.getNumeroContrato());
            contrato.setFacturacion(Boolean.parseBoolean(facturacion.getFacturacion()));
            contrato.setSubProducto(subproducto);
            contrato.setCliente(cliente);
        }

        Servicio servicio = this.servicioRepository.getByNumero(facturacion.getNumeroServicio());
        if(servicio == null){
            servicio = new Servicio();
            servicio.setNumero(facturacion.getNumeroServicio());
            servicio.setContrato(contrato);
        }

        // Historial Facturacion
        HistorialFacturacion historialFacturacion = new HistorialFacturacion();
        historialFacturacion.setDescripcion(facturacion.getDescripcion());
        historialFacturacion.setCargo(cargo);
        historialFacturacion.setFechaDoc(DateUtils.stringToLocalDate(facturacion.getFechaDoc()));
        historialFacturacion.setServicio(servicio);
        historialFacturacion.setFechaPr(DateUtils.stringToLocalDate(facturacion.getFechaPr()));
        historialFacturacion.setValorFact(Double.parseDouble(facturacion.getValorFact()));
        historialFacturacion.setSaldo(Double.parseDouble(facturacion.getSaldo()));
        historialFacturacion.setTotalDoc(Double.parseDouble(facturacion.getTotDocumento()));

        return historialFacturacion;
    }

    /**
     * Validation Service Setter
     * @param validationService Validation Service
     */
    public void setValidationService(ValidationService validationService) {
        this.validationService = validationService;
    }

    /**
     * Producto Repository Setter
     * @param productoRepository Producto Repository
     */
    public void setProductoRepository(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    /**
     * Tipo Producto Repository Setter
     * @param tipoProductoRepository Tipo Producto Repository
     */
    public void setTipoProductoRepository(TipoProductoRepository tipoProductoRepository) {
        this.tipoProductoRepository = tipoProductoRepository;
    }

    /**
     * Tipo Documento Repository Setter
     * @param tipoDocumentoRepository Tipo Documento Repository
     */
    public void setTipoDocumentoRepository(TipoDocumentoRepository tipoDocumentoRepository) {
        this.tipoDocumentoRepository = tipoDocumentoRepository;
    }

    /**
     * Sucursal Repository Setter
     * @param sucursalRepository Sucursal Repository
     */
    public void setSucursalRepository(SucursalRepository sucursalRepository) {
        this.sucursalRepository = sucursalRepository;
    }

    /**
     * Cargo Repository Setter
     * @param cargoRepository Cargo Repository
     */
    public void setCargoRepository(CargoRepository cargoRepository) {
        this.cargoRepository = cargoRepository;
    }

    /**
     * Cuenta Repository Setter
     * @param cuentaRepository Cuenta Repository
     */
    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Cliente Repository Setter
     * @param clienteRepository Cliente Repository
     */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Contrato Repository Setter
     * @param contratoRepository Contrato Repository
     */
    public void setContratoRepository(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    /**
     * Servicio Repository Setter
     * @param servicioRepository Servicio Repository
     */
    public void setServicioRepository(ServicioRepository servicioRepository) {
        this.servicioRepository = servicioRepository;
    }

    /**
     * Historial Facturacion Repository Setter
     * @param historialFacturacionRepository Historial Facturacion Repository
     */
    public void setHistorialFacturacionRepository(HistorialFacturacionRepository historialFacturacionRepository) {
        this.historialFacturacionRepository = historialFacturacionRepository;
    }
}
