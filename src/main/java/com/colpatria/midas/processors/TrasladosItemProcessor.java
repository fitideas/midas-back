package com.colpatria.midas.processors;

import com.colpatria.midas.dto.TrasladosDto;
import com.colpatria.midas.exceptions.ElementNotFoundInCatalogueException;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.DateUtils;
import com.colpatria.midas.utils.NumberUtils;
import org.springframework.batch.item.ItemProcessor;

import java.math.BigInteger;


/**
 * Traslados Item Processor
 * Converts objects from TrasladosDto to Historial Taslado
 */
public class TrasladosItemProcessor implements ItemProcessor<TrasladosDto, HistorialTraslado> {

    /**
     * File
     */
    private final String file;

    /**
     * Validation Service
     */
    private ValidationService validationService;

    /**
     * Producto Repository
     */
    private ProductoRepository productoRepository;

    /**
     * Tipo Producto Repository
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Tipo Documento Repository
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Cuenta Repository
     */
    private CuentaRepository cuentaRepository;

    /**
     * Cliente Repository
     */
    private ClienteRepository clienteRepository;

    /**
     * Contrato Repository
     */
    private ContratoRepository contratoRepository;

    /**
     * Usuario Repository
     */
    private UsuarioRepository usuarioRepository;


    /**
     * Constructor
     *
     * @param file File name
     */
    public TrasladosItemProcessor(String file) {
        this.file = file;
    }

    /**
     * Process an item
     *
     * @param traslado item to process
     * @return Historial Traslado
     */
    @Override
    public HistorialTraslado process(TrasladosDto traslado) throws Exception {

        // Initial Cleaning
        this.validationService.cleanLineErrors();
        this.validationService.increaseRegisterCounter();

        // CODIGO PRODUCTO
        this.validationService.verifyEmpty(traslado.getCodigoProducto(), "Código de producto", this.file);
        Producto producto = this.productoRepository.getProductoByCodigo(traslado.getCodigoProducto());
        if(!traslado.getCodigoProducto().equals(""))
            this.validationService.verifyCatalog(producto, "Código de producto", this.file);

        // NOMBRE PRODUCTO
        // sin validaciones

        // CODIGO TIPO PRODUCTO
        this.validationService.verifyEmpty(traslado.getCodigoTipoProducto(), "Código de tipo de producto", this.file);
        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(traslado.getCodigoTipoProducto());
        if(!traslado.getCodigoTipoProducto().equals(""))
            this.validationService.verifyCatalog(tipoProducto, "Código de tipo de producto", this.file);

        // NOMBRE TIPO PRODUCTO
        // sin validaciones

        // CODIGO SUBPRODUCTO
        this.validationService.verifyEmpty(traslado.getCodigoSubproducto(), "Código de subproducto", this.file);
        Producto subproducto = this.productoRepository.getProductoByCodigo(traslado.getCodigoSubproducto());
        if(!traslado.getCodigoSubproducto().equals(""))
            this.validationService.verifyCatalog(subproducto, "Código de subproducto", this.file);
        if (subproducto != null && producto != null) {
            this.validationService.validateChild(producto, subproducto.getProductoPadre(), "subproducto", subproducto.getCodigo(), this.file);
        }

        // NOMBRE SUBPRODUCTO
        // sin validaciones

        // NUMERO CONTRATO
        this.validationService.verifyNumber(traslado.getNumeroContrato(), "Número de contrato", this.file);

        // TIPO IDENTIFICACIÓN
        this.validationService.verifyEmpty(traslado.getTipoIdentificacion(), "Tipo de identificación", this.file);
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(traslado.getTipoIdentificacion());
        if(!traslado.getTipoIdentificacion().equals(""))
            this.validationService.verifyCatalog(tipoDocumento, "Tipo de identificación", this.file);

        // NUMERO DOCUMENTO IDENTIFICACION
        this.validationService.validateIntegerWithZero(traslado.getNumeroDocumento(), "Número de document", this.file);

        // CUENTA ACTUAL
        this.validationService.validateIntegerWithZero(traslado.getCuentaActual(), "Cuenta actual", this.file);

        // CUENTA ANTERIOR
        this.validationService.validateIntegerWithZero(traslado.getCuentaAnterior(), "Cuenta Anterior", this.file);

        // FECHA TRASLADO
        this.validationService.verifyDate(traslado.getFechaTraslado(), "Fecha de traslado", this.file);

        // USERNAME
        // sin validaciones

        // NOMBRE
        // sin validaciones

        // CICLO ANTERIOR
        this.validationService.validateIntegerWithZero(traslado.getCicloAnterior(), "Ciclo anterior", this.file);


        // CICLO NUEVO
        this.validationService.validateIntegerWithZero(traslado.getCicloNuevo(), "Ciclo nuevo", this.file);

        // OBSERVACION
        // sin validaciones


        // Verification Validation
        if(this.validationService.isLineError()) {
            return null;
        }

        // Non-Catalogue Validation
        Cuenta cuentaAnterior = this.cuentaRepository.getByNumero(Double.parseDouble(traslado.getCuentaAnterior()));
        if (cuentaAnterior == null) {
            cuentaAnterior = new Cuenta();
            cuentaAnterior.setNumero(Double.parseDouble(traslado.getCuentaAnterior()));
        }

        Cuenta cuentaActual = this.cuentaRepository.getByNumero(Double.parseDouble(traslado.getCuentaAnterior()));
        if (cuentaActual == null) {
            cuentaActual = new Cuenta();
            cuentaActual.setNumero(Double.parseDouble(traslado.getCuentaActual()));
        }

        Cliente cliente = this.clienteRepository.getByNumeroIdentificacion(Double.parseDouble(traslado.getNumeroDocumento()));
        if (cliente == null) {
            cliente = new Cliente();
            cliente.setNumeroIdentificacion(Double.parseDouble(traslado.getNumeroDocumento()));
            cliente.setTipoDocumento(tipoDocumento);
            cliente.setCuenta(cuentaActual);
        }

        Contrato contrato = this.contratoRepository.getByNumero(traslado.getNumeroContrato());
        if (contrato == null) {
            contrato = new Contrato();
            contrato.setNumero(traslado.getNumeroContrato());
            contrato.setCliente(cliente);
            contrato.setSubProducto(subproducto);
        }

        Usuario usuario = this.usuarioRepository.getUsuarioById(traslado.getUsername());
        if (usuario == null) {
            usuario = new Usuario();
            usuario.setId(traslado.getUsername());
            usuario.setNombres(traslado.getNombre());
            usuario.setArea(traslado.getArea() + "");
        }

        HistorialTraslado historialTraslado = new HistorialTraslado();
        historialTraslado.setFecha(DateUtils.stringToLocalDate(traslado.getFechaTraslado()));
        historialTraslado.setObservacion(traslado.getObservacion());
        historialTraslado.setCuentaActual(cuentaActual);
        historialTraslado.setCuentaAnterior(cuentaAnterior);
        historialTraslado.setContrato(contrato);
        historialTraslado.setUsuario(usuario);
        historialTraslado.setCicloActual(Integer.parseInt(traslado.getCicloNuevo()));
        historialTraslado.setCicloAnterior(Integer.parseInt(traslado.getCicloAnterior()));

        return historialTraslado;
    }

    /**
     * Validation Service setter
     *
     * @param validationService Validation Service
     */
    public void setValidationService(ValidationService validationService) {
        this.validationService = validationService;
    }

    /**
     * Producto Repository setter
     *
     * @param productoRepository Producto Repository
     */
    public void setProductoRepository(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    /**
     * Tipo Producto Repository setter
     *
     * @param tipoProductoRepository Tipo Producto Repository
     */
    public void setTipoProductoRepository(TipoProductoRepository tipoProductoRepository) {
        this.tipoProductoRepository = tipoProductoRepository;
    }

    /**
     * Tipo Documento Repository Setter
     *
     * @param tipoDocumentoRepository Tipo Documento Repositoryy
     */
    public void setTipoDocumentoRepository(TipoDocumentoRepository tipoDocumentoRepository) {
        this.tipoDocumentoRepository = tipoDocumentoRepository;
    }

    /**
     * Cuenta Repository Setter
     *
     * @param cuentaRepository Cuenta Repository
     */
    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Cliente Repository Setter
     *
     * @param clienteRepository Cliente Repository
     */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Contrato Repository Setter
     *
     * @param contratoRepository Contrato Repository
     */
    public void setContratoRepository(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    /**
     * Usuario Repository Setter
     *
     * @param usuarioRepository Usuario Repository
     */
    public void setUsuarioRepository(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }
}
