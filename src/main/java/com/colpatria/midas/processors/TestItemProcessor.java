package com.colpatria.midas.processors;

import com.colpatria.midas.model.Usuario;

import org.springframework.batch.item.ItemProcessor;

public class TestItemProcessor implements  ItemProcessor<Usuario, Usuario>{

    @Override
    public Usuario process(Usuario item) throws Exception {
        return item;
    }

}
