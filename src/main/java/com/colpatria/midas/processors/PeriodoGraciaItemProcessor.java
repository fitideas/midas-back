package com.colpatria.midas.processors;

import java.time.LocalDate;
import java.util.Optional;
import com.colpatria.midas.dto.PeriodoGraciaDto;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.Cuenta;
import com.colpatria.midas.model.Estado;
import com.colpatria.midas.model.HistorialPeriodoGracia;
import com.colpatria.midas.model.Negociacion;
import com.colpatria.midas.model.Producto;
import com.colpatria.midas.model.Servicio;
import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.model.TipoProducto;
import com.colpatria.midas.model.Usuario;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.CuentaRepository;
import com.colpatria.midas.repositories.EstadoRepository;
import com.colpatria.midas.repositories.NegociacionRepository;
import com.colpatria.midas.repositories.ProductoRepository;
import com.colpatria.midas.repositories.ServicioRepository;
import com.colpatria.midas.repositories.TipoDocumentoRepository;
import com.colpatria.midas.repositories.TipoProductoRepository;
import com.colpatria.midas.repositories.UsuarioRepository;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.DateUtils;
import com.colpatria.midas.utils.NumberUtils;
import org.springframework.batch.item.ItemProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PeriodoGraciaItemProcessor implements ItemProcessor<PeriodoGraciaDto, HistorialPeriodoGracia>{

    /**
     * Logger in processor
     */
    private static final Logger logger = LoggerFactory.getLogger(PeriodoGraciaItemProcessor.class);

    /**
     * Attribute that identifies the file that is being read in the batch process
     */
    private String fileName;

    /**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

	/**
	 * add 'tipoProducto' repository
	 */
    private final TipoProductoRepository tipoProductoRepository;

	/**
	 * add 'producto' repository
	 */
    private final ProductoRepository productoRepository;

	/**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

	/**
	 * add 'contrato' repository
	 */
	private final ContratoRepository contratoRepository;

	/**
	 * add 'Tipo Documento' repository
	 */
	private final TipoDocumentoRepository tipoDocumentoRepository;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;

    /**
	 * add 'Estado' repository
	 */
    private final EstadoRepository estadoRepository;

    /**
	 * add 'Usuario' repository
	 */
    private final UsuarioRepository usuarioRepository;

    /**
	 * add 'Usuario' repository
	 */
    private final NegociacionRepository negociacionRepository;

    /**
	 * add 'Ceunta' repository
	 */
    private final CuentaRepository cuentaRepository;

    /**
     * Constant for String 'Producto'
     */
    private static final String PRODUCTO = "Producto";

    /**
     * Constant for String 'Subproducto'
     */
    private static final String SUBPRODUCTO = "Subproducto";

    /**
     * Constant for String 'TipoProducto'
     */
    private static final String TIPO_PRODUCTO = "TipoProducto";

    /**
     * Constant for String 'TipoDocumento'
     */
    private static final String TIPO_DOCUMENTO = "TipoDocumento";

    /**
     * Constant for String 'cicloFacturacion'
     */
    private static final String CICLO_FACTURACION = "CicloFacturacion";

    /**
     * Constant for String 'NumeroDocumento'
     */
    private static final String NUMERO_DOCUMENTO = "NumeroDocumento";

    /**
     * Constant for String 'NumeroCuenta'
     */
    private static final String CUENTA_NUMERO = "NumeroCuenta";

    /**
     * Constant for String 'NumeroServicio'
     */
    private static final String SERVICIO_NUMERO = "NumeroServicio";

    /**
     * Constant for String 'EstadoNegociacion'
     */
    private static final String ESTADO_NEGOCIACION = "EstadoNegociacion";

    /**
     * Constant for String 'NegociacionID'
     */
    private static final String NEGOCIACION_ID = "NegociacionID";

    /**
     * Constant for String 'ContratoNumero'
     */
    private static final String CONTRATO_NUMERO = "ContratoNumero";

    /**
     * Constant for String 'FechaNegociacion'
     */
    private static final String FECHA_NEGOCIACION = "FechaNegociacion";

    /**
     * Constant for String 'FechaAplicacion'
     */
    private static final String FECHA_APLICACION = "FechaAplicacion";

    /**
     * Constant for String 'FechaInicioGracia'
     */
    private static final String FECHA_INICIO_GRACIA = "FechaInicioGracia";

    /**
     * Constant for String 'FechaFinGracia'
     */
    private static final String FECHA_FIN_GRACIA = "FechaFinGracia";

    /**
     * Constant for String 'FechaDesisteGracia'
     */
    private static final String FECHA_DESISTE_GRACIA = "FechaDesisteGracia";

    /**
     * Constant for String 'DiasMora'
     */
    private static final String DIAS_MORA = "DiasMora";

    /**
     * Constant for String 'CantidadCuotasAnteriores'
     */
    private static final String CUOTAS_AMTERIOSRES = "CantidadCuotasAnteriores";

    /**
     * Constant for String 'ValorFacturado'
     */
    private static final String VALOR_FACT = "ValorFacturado";
    

    /**
	 * @param fileName identifies the file that is being read in the batch process
	 * @param servicioRepository dependency injection of servicioRepository
	 * @param tipoProductoRepository dependency injection of tipoProductoRepository
	 * @param productoRepository dependency injection of productoRepository
	 * @param clienteRepository dependency injection of clienteRepository
	 * @param sucursalRepository dependency injection of sucursalRepository
	 * @param contratoRepository dependency injection of contratoRepository
	 * @param cuentaRepository dependency injection of cuentaRepository
	 * @param tipoDocumentoRepository dependency injection of tipoDocumentoRepository
	 * @param validationService dependency injection of validationService
	 */
	public PeriodoGraciaItemProcessor(
        String fileName,
        ValidationService validationService,
		ServicioRepository servicioRepository,
        TipoDocumentoRepository tipoDocumentoRepository,
		TipoProductoRepository tipoProductoRepository,
		ProductoRepository productoRepository,
        ContratoRepository contratoRepository,
        UsuarioRepository usuarioRepository,
		ClienteRepository clienteRepository,
		NegociacionRepository negociacionRepository,
        EstadoRepository estadoRepository,
        CuentaRepository cuentaRepository
	) {
        this.fileName = fileName;
		this.servicioRepository = servicioRepository;
		this.tipoProductoRepository = tipoProductoRepository;
		this.productoRepository = productoRepository;
		this.clienteRepository = clienteRepository;
		this.contratoRepository = contratoRepository;
		this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.estadoRepository = estadoRepository;
        this.usuarioRepository = usuarioRepository;
        this.negociacionRepository = negociacionRepository;
		this.validationService = validationService;
        this.cuentaRepository = cuentaRepository;
	}

    /**
     * Method that process the items that are being read from the file in the batch process
     * @param item to be process
     * @return the item that has been process
     * @throws Exception
     */
    @Override
    public HistorialPeriodoGracia process(PeriodoGraciaDto item) throws Exception {

        this.validationService.increaseRegisterCounter();
        this.validationService.cleanLineErrors();

        this.validationService.verifyEmpty(item.getProductoCodigo(), PRODUCTO, this.fileName);
        this.validationService.verifyEmpty(item.getProductoTipoCodigo(), TIPO_PRODUCTO, this.fileName);
        this.validationService.verifyEmpty(item.getSubproductoCodigo(), SUBPRODUCTO, this.fileName);
        this.validationService.verifyEmpty(item.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyEmpty(item.getNumeroIdentificacion(), NUMERO_DOCUMENTO, this.fileName);
        this.validationService.verifyEmpty(item.getCuentaNumero(), CUENTA_NUMERO, this.fileName);
        this.validationService.verifyEmpty(item.getCiclo(), CICLO_FACTURACION, this.fileName);
        this.validationService.verifyEmpty(item.getServicioNumero(), SERVICIO_NUMERO, this.fileName);
        this.validationService.verifyEmpty(item.getNegociacionEstado(), ESTADO_NEGOCIACION, this.fileName);
        this.validationService.verifyEmpty(item.getNegociacionId(), NEGOCIACION_ID, this.fileName);
        this.validationService.verifyEmpty(item.getContratoNumero(), CONTRATO_NUMERO, this.fileName);
        this.validationService.verifyEmpty(item.getFechaNegociacion(), FECHA_NEGOCIACION, this.fileName);
        this.validationService.verifyEmpty(item.getFechaAplicacion(), FECHA_APLICACION, this.fileName);
        this.validationService.verifyEmpty(item.getDiasMora(), DIAS_MORA, this.fileName);
        this.validationService.verifyEmpty(item.getCantidadCuotasAnteriores(), CUOTAS_AMTERIOSRES, this.fileName);
        this.validationService.verifyEmpty(item.getValorFacturado(), VALOR_FACT, this.fileName);

        if(this.validationService.isLineError()){
            return null;
        }

        this.validationService.validateInteger(item.getProductoCodigo(), PRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getProductoTipoCodigo(), TIPO_PRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getSubproductoCodigo(), SUBPRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName);
        this.validationService.validateInteger(item.getCiclo(), CICLO_FACTURACION, this.fileName);
        this.validationService.validateInteger(item.getNumeroIdentificacion(), NUMERO_DOCUMENTO, this.fileName);
        this.validationService.validateInteger(item.getCuentaNumero(), CUENTA_NUMERO, this.fileName);
        this.validationService.validateInteger(item.getServicioNumero(), SERVICIO_NUMERO, this.fileName);
        this.validationService.validateInteger(item.getContratoNumero(), CONTRATO_NUMERO, this.fileName);
        this.validationService.validateInteger(item.getNegociacionId(), NEGOCIACION_ID, this.fileName);
        this.validationService.validateInteger(item.getDiasMora(), DIAS_MORA, this.fileName);
        this.validationService.validateInteger(item.getCantidadCuotasAnteriores(), CUOTAS_AMTERIOSRES, this.fileName);

        this.validationService.validateDouble(item.getValorFacturado(), VALOR_FACT, this.fileName);

        this.validationService.validateDate(item.getFechaNegociacion(), FECHA_NEGOCIACION, this.fileName);
        this.validationService.validateDate(item.getFechaAplicacion(), FECHA_APLICACION, this.fileName);
        this.validationService.validateDate(item.getFechaInicioGracia(), FECHA_INICIO_GRACIA, this.fileName);
        this.validationService.validateDate(item.getFechaFinGracia(), FECHA_FIN_GRACIA, this.fileName);
        this.validationService.validateDate(item.getFechaDesistegracia(), FECHA_DESISTE_GRACIA, this.fileName);

        this.validationService.verifyZero(item.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyZero(item.getNumeroIdentificacion(), NUMERO_DOCUMENTO, this.fileName);
        this.validationService.verifyZero(item.getCuentaNumero(), CUENTA_NUMERO, this.fileName);
        this.validationService.verifyZero(item.getCiclo(), CICLO_FACTURACION, this.fileName);
        this.validationService.verifyZero(item.getContratoNumero(), CONTRATO_NUMERO, this.fileName);
        this.validationService.verifyZero(item.getServicioNumero(), SERVICIO_NUMERO, this.fileName);

        if(this.validationService.isLineError()){
            return null;
        }

        String productoTipoCodigo = item.getProductoTipoCodigo();
        String productoCodigo = item.getProductoCodigo();
        String subproductoCodigo = item.getSubproductoCodigo();
        String tipoIdentificacion = item.getTipoIdentificacion();
        Integer ciclo = NumberUtils.isInteger(item.getCiclo()) ? Integer.parseInt(item.getCiclo()) : -1;
        String estadoId = item.getNegociacionEstado();

        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(productoTipoCodigo);
        Producto subproducto = this.productoRepository.getByCodigo(subproductoCodigo);
        Producto producto = this.productoRepository.getProductoByCodigo(productoCodigo);
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(tipoIdentificacion);
        Estado estado = this.estadoRepository.getEstadoById(estadoId);    

        this.validationService.verifyCatalog(tipoProducto, TIPO_PRODUCTO, this.fileName);
        this.validationService.verifyCatalog(subproducto, SUBPRODUCTO, this.fileName);
        this.validationService.verifyCatalog(producto, PRODUCTO, this.fileName);
        this.validationService.verifyCatalog(tipoDocumento, TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyCatalog(estado, ESTADO_NEGOCIACION, this.fileName);

        if(subproducto != null && producto != null){
            String codigoProductoPadre = subproducto.getProductoPadre() != null ? subproducto.getProductoPadre().getCodigo() : "";
            this.validationService.verifyParent(producto.getCodigo(), codigoProductoPadre, PRODUCTO, SUBPRODUCTO, this.fileName);
        }
        if( producto != null && tipoProducto != null ) {
            this.validationService.verifyParent(producto.getTipo().getCodigo(), tipoProducto.getCodigo(), PRODUCTO, TIPO_PRODUCTO, this.fileName);
        }
        
        if(this.validationService.isLineError()){
            return null;
        }
        
        return createHistorialPeriodoGracia(item, ciclo, estado, subproducto, tipoDocumento);
    }

    HistorialPeriodoGracia createHistorialPeriodoGracia (PeriodoGraciaDto item, Integer cicloFacturacion, Estado estado, Producto subproducto, TipoDocumento tipoDocumento){

        HistorialPeriodoGracia historialPeriodoGracia = new HistorialPeriodoGracia();

        Optional<Cuenta> optionalCuenta = cuentaRepository.findById(Double.parseDouble(item.getCuentaNumero()));
        Cuenta cuenta = new Cuenta();
        if(!optionalCuenta.isPresent()){
            cuenta.setNumero(Double.parseDouble(item.getCuentaNumero()));
            cuenta.setCicloFacturacion(cicloFacturacion);
            cuenta.setCicloFacturacion(Integer.parseInt(item.getCiclo()));
        }else{
            cuenta = optionalCuenta.get();
        }

        Optional<Cliente> optionalCliente = clienteRepository.findById(Double.parseDouble(item.getNumeroIdentificacion()));
        Cliente cliente = new Cliente();
        if(!optionalCliente.isPresent()){
            cliente.setNumeroIdentificacion(Double.parseDouble(item.getNumeroIdentificacion()));
            cliente.setTipoDocumento(tipoDocumento);
        }else{
            cliente = optionalCliente.get();
        }
        if(cliente.getCuenta() == null){
            cliente.setCuenta(cuenta);
        }
        
        Optional<Contrato> optionalContrato = contratoRepository.findById(item.getContratoNumero());
        Contrato contrato = new Contrato();
        if(!optionalContrato.isPresent()){
            contrato.setNumero(item.getContratoNumero());
            contrato.setSubProducto(subproducto);
        }else{
            contrato = optionalContrato.get();
        }
        if(contrato.getCliente() == null){
            contrato.setCliente(cliente);
        }
        
        Optional<Servicio> optionalServicio = servicioRepository.findById(item.getServicioNumero());
        Servicio servicio = new Servicio();
        if(!optionalServicio.isPresent()){
            servicio.setNumero(item.getServicioNumero());
        }else{
            servicio = optionalServicio.get();
        }
        if(servicio.getContrato() == null){
            servicio.setContrato(contrato);
        }

        Optional<Negociacion> optionalNegociacion = negociacionRepository.findById(item.getNegociacionId());
        Negociacion negociacion = new Negociacion();
        if(!optionalNegociacion.isPresent()){
            negociacion.setId(item.getNegociacionId());
            negociacion.setEstado(item.getNegociacionEstado());
            negociacion.setFecha(DateUtils.stringToLocalDate(item.getFechaNegociacion()));
        }else{
            negociacion = optionalNegociacion.get();
        }

        Optional<Usuario> optionalUsuario = usuarioRepository.findById(item.getUsuarioCodigo());
        Usuario usuario = new Usuario();
        if(!optionalUsuario.isPresent()){
            usuario.setNombres(item.getUsuarioNombre());
            usuario.setId(item.getUsuarioCodigo());
            usuario.setArea(item.getUsuarioArea());
        }else{
            usuario = optionalUsuario.get();
        }

        historialPeriodoGracia.setCantidadCuotasAnteriores(Integer.parseInt(item.getCantidadCuotasAnteriores()));
        historialPeriodoGracia.setServicio(servicio);
        historialPeriodoGracia.setFechaAplicacion(LocalDate.parse(item.getFechaAplicacion()));
        historialPeriodoGracia.setFechaInicioGracia(LocalDate.parse(item.getFechaInicioGracia()));
        historialPeriodoGracia.setFechaInicioGracia(LocalDate.parse(item.getFechaInicioGracia()));
        historialPeriodoGracia.setFechaFinGracia(LocalDate.parse(item.getFechaFinGracia()));
        historialPeriodoGracia.setFechaDesistegracia(LocalDate.parse(item.getFechaDesistegracia()));
        historialPeriodoGracia.setDescripcion (item.getDescripcion());
        historialPeriodoGracia.setDiasMora(Integer.parseInt(item.getDiasMora()));
        historialPeriodoGracia.setNegociacion(negociacion);
        historialPeriodoGracia.setUsuario(usuario);
        historialPeriodoGracia.setValorFacturado(Double.parseDouble(item.getValorFacturado()));

        return historialPeriodoGracia;
    }
    
}
