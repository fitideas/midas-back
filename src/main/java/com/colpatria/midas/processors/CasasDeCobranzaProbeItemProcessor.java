package com.colpatria.midas.processors;

import com.colpatria.midas.dto.CasasDeCobranzaProbeDto;
import com.colpatria.midas.dto.CasasDeCobranzaProbeQueryDto;
import org.springframework.batch.item.ItemProcessor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Casas de Cobranza Probe Item Processor
 */
public class CasasDeCobranzaProbeItemProcessor implements ItemProcessor<CasasDeCobranzaProbeQueryDto, CasasDeCobranzaProbeDto> {

    /**
     * Process Method.
     *
     * @param item Input Object
     * @return Output Object
     * @throws Exception Exception
     */
    @Override
    public CasasDeCobranzaProbeDto process(CasasDeCobranzaProbeQueryDto item) throws Exception {
        CasasDeCobranzaProbeDto out = new CasasDeCobranzaProbeDto();

        // numide
        out.setNumide(String.format("%.0f", item.getHistorialCarteraLight().getServicio().getContrato().getCliente().getNumeroIdentificacion()));

        // numobl
        out.setNumobl(item.getHistorialCarteraLight().getServicio().getContrato().getNumero());

        // saldo_k
        double saldoK = item.getHistorialCarteraLight().getCapitalImpago() +
                item.getHistorialCarteraLight().getServicio().getSaldoCapitalPorFacturar();
        out.setSaldoK(Double.toString(saldoK));

        // diasmo1
        out.setDiasmo1(item.getHistorialCarteraLight().getDiasAtraso().toString());

        // diasmo2
        out.setDiasmo2(item.getHistorialCarteraLight().getMoraPrimerVencimiento().toString());

        // edad1
        if (item.getHistorialCarteraLight().getPrimeraAlturaMora() != null) {
            out.setEdad1(item.getHistorialCarteraLight().getPrimeraAlturaMora().toString());
        } else {
            out.setEdad1("");
        }

        // edad2
        if (item.getHistorialCarteraLight().getSegundaAlturaMora() != null) {
            out.setEdad2(item.getHistorialCarteraLight().getSegundaAlturaMora().toString());
        } else {
            out.setEdad2("");
        }

        // cuomor
        out.setCuomor(Double.toString(Math.ceil(item.getHistorialCarteraLight().getMoraPrimerVencimiento() / 30.0)));

        // saldo_venc
        double saldoVenc;
        if (item.getHistorialRecaudoResumen() != null && Integer.parseInt(out.getDiasmo2()) > 2) {
            saldoVenc =
                    item.getHistorialCarteraLight().getCapitalImpago() +
                            item.getHistorialCarteraLight().getInteresImpago() +
                            item.getHistorialCarteraLight().getInteresOrdenCorrienteImpago() +
                            item.getHistorialCarteraLight().getInteresMora() +
                            item.getHistorialCarteraLight().getInteresOrdenMoraImpago() +
                            item.getHistorialCarteraLight().getInteresFacturadoNoAfecto() +
                            item.getHistorialRecaudoResumen().getSeguroObligatorio() +
                            item.getHistorialCarteraLight().getServicio().getValorCuota() +
                            item.getHistorialRecaudoResumen().getHonorarioCobranza() +
                            item.getHistorialRecaudoResumen().getComisiones() +
                            item.getHistorialRecaudoResumen().getSeguroVoluntario();
        } else {
            saldoVenc = 0.0;
        }
        out.setSaldoVenc(Double.toString(saldoVenc));

        // fec_redif
        out.setFecRedif(""); //TODO: PENDIENTE DEFINICION

        // val_pago
        double valPago;
        if (item.getHistorialRecaudoResumen() != null) {
            valPago =
                    item.getHistorialRecaudoResumen().getCapital() +
                            item.getHistorialRecaudoResumen().getInteresCorriente() +
                            item.getHistorialRecaudoResumen().getInteresMora() +
                            item.getHistorialRecaudoResumen().getCuotaManejo() +
                            item.getHistorialRecaudoResumen().getHonorarioCobranza() +
                            item.getHistorialRecaudoResumen().getSeguroObligatorio() +
                            item.getHistorialRecaudoResumen().getSeguroVoluntario() +
                            item.getHistorialRecaudoResumen().getComisiones();
        } else {
            valPago = 0.0;
        }
        out.setValPago(Double.toString(valPago));

        // valor_cuot
        out.setValorCuot(item.getHistorialCarteraLight().getServicio().getValorCuota().toString());

        // cant_pagos
        out.setCantPagos(Integer.toString(item.getNumeroPagosFechaCierre()));

        // fec_vence
        out.setFecVence(item.getHistorialCarteraLight().getFechaPrimerVencimiento().toString());

        // tipo_tarj
        out.setTipoTarj(item.getHistorialCarteraLight().getServicio().getContrato().getSubProducto().getNombre());

        // proend
        out.setProend(""); // TODO: PENDIENTE DEFINICION

        // diasmo_pro
        out.setDiasmoPro(
                Long.toString(
                        Integer.parseInt(out.getDiasmo2()) +
                                DAYS.between(
                                        LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()),
                                        item.getHistorialCarteraLight().getFechaReporte()
                                )
                )
        );

        // pag_minimo
        double pagMinimo = item.getHistorialCarteraLight().getCapitalImpago() +
                item.getHistorialCarteraLight().getInteresImpago() +
                item.getHistorialCarteraLight().getInteresOrdenCorrienteImpago() +
                item.getHistorialCarteraLight().getInteresMora() +
                item.getHistorialCarteraLight().getInteresOrdenMoraImpago() +
                item.getHistorialCarteraLight().getInteresFacturadoNoAfecto();
        if (item.getHistorialRecaudoResumen() != null) {
            pagMinimo += item.getHistorialRecaudoResumen().getSeguroObligatorio() +
                    item.getHistorialRecaudoResumen().getHonorarioCobranza() +
                    item.getHistorialRecaudoResumen().getCuotaManejo() +
                    item.getHistorialRecaudoResumen().getSeguroVoluntario() +
                    item.getHistorialRecaudoResumen().getComisiones();
        }
        out.setPagMinimo(Double.toString(pagMinimo));

        // m_libros
        out.setMLibros(" "); // TODO: PENDIENTE DEFINICION CRM

        // esp
        out.setEsp("1");

        // tipide
        out.setTipide(item.getHistorialCarteraLight().getServicio().getContrato().getCliente().getTipoDocumento().getNombre());

        // numide1
        out.setNumide1("0");

        // famparada
        if (item.getHistorialCarteraLight().getServicio().getContrato().getSubProducto().getTipo().getNombre().equals("TARJETA AMPARADA")) {
            out.setFamparada("1");
        } else {
            out.setFamparada("0");
        }

        // docto_amp
        out.setDoctoAmp(""); // TODO: PEDNIENTE DEFINICION CRM

        // tipo_tarje
        out.setTipoTarje(item.getHistorialCarteraLight().getServicio().getContrato().getSubProducto().getProductoPadre().getNombre());

        // fec_ult_re (Ultimo reestructurado)
        out.setFecUltRe(""); // TODO: PENDIENTE DEFINICION

        // saldo_tot
        double saldoTot = item.getHistorialCarteraLight().getCapitalImpago() +
                item.getHistorialCarteraLight().getInteresImpago() +
                item.getHistorialCarteraLight().getInteresOrdenCorrienteImpago() +
                item.getHistorialCarteraLight().getInteresMora() +
                item.getHistorialCarteraLight().getInteresOrdenMoraImpago() +
                item.getHistorialCarteraLight().getInteresFacturadoNoAfecto() +
                item.getHistorialCarteraLight().getInteresCausadoNoFacturado() +
                item.getHistorialCarteraLight().getServicio().getSaldoCapitalPorFacturar() +
                item.getHistorialCarteraLight().getServicio().getValorCuota() +
                item.getHistorialCarteraLight().getInteresCorrienteOrdenCausadoNoFacturado() +
                item.getHistorialCarteraLight().getInteresMoraCausadoNoFacturado() +
                item.getHistorialCarteraLight().getInteresMoraOrdenCausadoNoFacturado();
        out.setSaldoTot(Double.toString(saldoTot));

        // cant_refi
        out.setCantRefi(Integer.toString(item.getNumeroRefinanciaciones()));

        // tip_modi1
//        if (item.getNegociacion() != null) {
//            out.setTipModi1(item.getNegociacion().getEstado());
//        }

        // fec_modif
//        if (item.getNegociacion() != null) {
//            out.setFecModif(item.getNegociacion().getFecha().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
//        }

        // fec_segu
        out.setFecSegu(item.getHistorialCarteraLight().getFechaSegundoVencimiento().toString());

        // numero_cli
        out.setNumeroCli(item.getHistorialCarteraLight().getServicio().getContrato().getCliente().getCuenta().getNumero().toString());

        // fecha_repo
        out.setFechaRepo(item.getHistorialCarteraLight().getFechaReporte().toString());

        // indapert
        out.setIndapert(""); // TODO: PENDIENTE DEFINICION

        // cant_apert
        out.setCantApert(""); // TODO: PENDIENTE DEFINICION

        // asiignado
        out.setAsiignado("");

        // fecutil
        out.setFecutil(item.getHistorialCarteraLight().getServicio().getFechaCompra().format(DateTimeFormatter.ofPattern("yyyyMMdd")));

        // ind_iloc
        if (item.getHistorialCarteraLight().getServicio().getContrato().getFacturacion() != null && item.getHistorialCarteraLight().getServicio().getContrato().getFacturacion()) {
            out.setIndIloc("0");
        } else {
            out.setIndIloc("1");
        }

        // cant_comp
        out.setCantComp(Integer.toString(item.getNumeroCarterasCierreMes()));

        // val_comp
        out.setValComp(Double.toString(item.getHistorialCarteraLight().getServicio().getValorCompra()));

        // CFECACMV
        String fechaCuotaMasVencida;
        if (Integer.parseInt(out.getDiasmo2()) > 0) {
            fechaCuotaMasVencida = item.getHistorialCarteraLight().getFechaReporte().minusDays(Integer.parseInt(out.getDiasmo2())).format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        } else {
            fechaCuotaMasVencida = "";
        }
        out.setFechaCuotaMasVencida(fechaCuotaMasVencida);

        // cvlrdes
        out.setValorDesembolso(Double.toString(item.getHistorialCarteraResumen().getSumaValorCompra()));

        // INTCTE
        double intCte = item.getHistorialCarteraLight().getInteresImpago() +
                item.getHistorialCarteraLight().getInteresOrdenCorrienteImpago();
        out.setIntCte(Double.toString(intCte));

        // INTMOR
        double intMora = item.getHistorialCarteraLight().getInteresMora() +
                item.getHistorialCarteraLight().getInteresOrdenMoraImpago();
        out.setIntMot(Double.toString(intMora));

        // INTTOT
        out.setIntTot(out.getIntCte() + out.getIntMot());

        // CMANEJO
        if(item.getHistorialRecaudoResumen() != null){
            out.setCManejo(item.getHistorialRecaudoResumen().getCuotaManejo().toString());
        }

        // GCOBRAN
        if(item.getHistorialRecaudoResumen() != null) {
            out.setGCobranz(item.getHistorialRecaudoResumen().getHonorarioCobranza().toString());
        }

        // CTASAIN
        out.setCTasain(item.getHistorialCarteraLight().getServicio().getTasa().toString());

        // CICLO
        out.setCiclo(item.getHistorialCarteraLight().getServicio().getContrato().getCliente().getCuenta().getCicloFacturacion().toString());

        // VALPAG
        double valPag;
        if(item.getHistorialRecaudoResumen() != null){
            valPag = item.getHistorialRecaudoResumen().getCapital() +
                    item.getHistorialRecaudoResumen().getInteresCorriente() +
                    item.getHistorialRecaudoResumen().getInteresMora() +
                    item.getHistorialRecaudoResumen().getCuotaManejo() +
                    item.getHistorialRecaudoResumen().getHonorarioCobranza() +
                    item.getHistorialRecaudoResumen().getSeguroObligatorio() +
                    item.getHistorialRecaudoResumen().getSeguroVoluntario() +
                    item.getHistorialRecaudoResumen().getComisiones() +
                    item.getHistorialRecaudo().getRecaudoCargo().getMonto();

        }else{
            valPag = 0.0;
        }
        out.setValPag(Double.toString(valPag));

        // FecPag
        if(item.getHistorialRecaudo() != null) {
            out.setFecPag(item.getHistorialRecaudo().getFechaPago().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        }

        // Sucursal
        out.setSucursal(item.getHistorialCarteraLight().getServicio().getContrato().getCliente().getCuenta().getSucursal().getNombre());

        // Fecha
        String fecha;
        if (item.getCalendarioFacturacion() != null) {
            fecha = item.getCalendarioFacturacion().getFechaFacturacion().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        }else {
            fecha = "";
        }
        out.setFecha(fecha);

        // Plazo
        out.setPlazo(Integer.toString(item.getHistorialCarteraResumen().getCuotasPorFacturarMaximo()));

        // Número Obligación Cobranzas
        out.setNumeroObligacionCobranzas(""); // TODO: PENDIENTE DEFINICION TABLA DE EQUIVALENCIA

        // Flag Reclamo
        String flagReclamo;
        if(item.getReclamacion() != null && item.getReclamacion().getVigente()){
            flagReclamo = "1";
        }else{
            flagReclamo = "0";
        }
        out.setFlagReclamo(flagReclamo);
        return out;
    }
}
