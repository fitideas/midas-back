package com.colpatria.midas.processors;

import com.colpatria.midas.dto.PagosDto;
import com.colpatria.midas.exceptions.ElementNotFoundInCatalogueException;
import com.colpatria.midas.exceptions.ParentNotFoundException;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.DateUtils;
import com.colpatria.midas.utils.VerificationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import java.time.LocalDate;

/**
 * procesador de cada item del job de pagos
 */
public class PagoItemProcessor implements ItemProcessor<PagosDto, HistorialPago> {

    /**
     * Add logger to pagos config
     */
    private static final Logger logger = LoggerFactory.getLogger(PagoItemProcessor.class);

    private String fileName;
    /**
     * Add email service
     */
    private EmailService emailService;

    /**
     * Producto Repository
     */
    private ProductoRepository productoRepository;

    /**
     * Tipo producto repository
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Cliente repository
     */
    private ClienteRepository clienteRepository;

    /**
     * sucursal repository
     */
    private SucursalRepository sucursalRepository;

    /**
     * Historial pago repository
     */
    private HistorialPagoRepository historialPagoRepository;

    /**
     * Tipo Documento repository
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * validation service
     */
    private ValidationService validationService;

    /**
     * cargoRepository
     */
    private CargoRepository cargoRepository;

    /**
     * @param fileName to process
     * @param emailService DI of email helper
     * @param productoRepository DI producto repository
     * @param tipoProductoRepository DI Tipo producto repository
     * @param clienteRepository DI cliente Repository
     * @param sucursalRepository DI sucursal Repository
     * @param historialPagoRepository DI historial Pago Repository
     * @param tipoDocumentoRepository DI tipo Documento Repository
     * @param validationService DI validationService
     * @param cargoRepository DI cargoRepository
     */
    public PagoItemProcessor(String fileName,
                             EmailService emailService,
                             ProductoRepository productoRepository,
                             TipoProductoRepository tipoProductoRepository,
                             ClienteRepository clienteRepository,
                             SucursalRepository sucursalRepository,
                             HistorialPagoRepository historialPagoRepository,
                             TipoDocumentoRepository tipoDocumentoRepository,
                             ValidationService validationService,
                             CargoRepository cargoRepository) {
        this.fileName = fileName;
        this.productoRepository = productoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.clienteRepository = clienteRepository;
        this.sucursalRepository = sucursalRepository;
        this.historialPagoRepository = historialPagoRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.emailService = emailService;
        this.validationService = validationService;
        this.cargoRepository = cargoRepository;
    }

    @Override
    public HistorialPago process(PagosDto pago) throws Exception {

        this.validationService.increaseRegisterCounter();
        this.validationService.cleanLineErrors();

        this.validationService.verifyDate(pago.getFechaPago(), "Fecha de pago", this.fileName);
        this.validationService.verifyDate(pago.getFechaIngreso(), "Fecha de ingreso", this.fileName);
        this.validationService.verifyDate(pago.getFechaVencimiento(), "Fecha de vencimiento", this.fileName);

        this.validationService.verifyNumber(pago.getMonto(), "Monto", this.fileName);
        this.validationService.verifyNumber(pago.getMontoParticipacion(), "Monto de participacion", this.fileName);
        this.validationService.verifyNumber(pago.getSucursal(), "Sucursal ", this.fileName);
        this.validationService.verifyNumber(pago.getCodigoProducto(), "Codigo Producto", this.fileName);
        this.validationService.verifyNumber(pago.getCodigoSubproducto(), "Codigo SubProducto", this.fileName);
        this.validationService.verifyNumber(pago.getCodigoTipoProducto(), "Codigo TipoProducto", this.fileName);
        this.validationService.verifyNumber(pago.getTipoDocumentoIdentidad(), "TipoDocumentoIdentidad", this.fileName);

        if(this.validationService.isLineError()){
            return null;
        }

        Producto subproducto = productoRepository.getByCodigo(pago.getCodigoSubproducto());
        Sucursal sucursal = sucursalRepository.getSucursalByNumero(Integer.parseInt(pago.getSucursal()));
        Producto producto = this.productoRepository.getProductoByCodigo(pago.getCodigoProducto());
        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(pago.getCodigoTipoProducto());
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(pago.getTipoDocumentoIdentidad());
        Cargo cargo = this.cargoRepository.getCargoById(pago.getCodCargo());
        Cargo cargoColpatria = this.cargoRepository.getCargoByIdAndCargoColpatria(pago.getCodCargoColpatria(), true);
        this.validationService.verifyCatalog(subproducto, "Subproducto", this.fileName);
        this.validationService.verifyCatalog(sucursal, "Sucursal", this.fileName);
        this.validationService.verifyCatalog(producto, "Producto", this.fileName);
        this.validationService.verifyCatalog(tipoProducto, "TipoProducto", this.fileName);
        this.validationService.verifyCatalog(tipoDocumento, "TipoDocumento", this.fileName);
        this.validationService.verifyCatalog(cargo, "Cargo", this.fileName);
        this.validationService.verifyCatalog(cargoColpatria, "Cargo Colpatria", this.fileName);

        String codigo = subproducto.getProductoPadre() != null ? subproducto.getProductoPadre().getCodigo() : "";
        this.validationService.verifyParent(producto.getCodigo(),
                codigo,
                "Producto",
                "SubProducto",
                this.fileName);

        this.validationService.verifyParent(
                producto.getTipo().getCodigo(),
                tipoProducto.getCodigo(),
                "Producto",
                "Tipo producto",
                this.fileName);

        if(this.validationService.isLineError()){
            return null;
        }

        Cliente cliente = this.clienteRepository.getByNumeroIdentificacion(Double.parseDouble(pago.getNroDocumentoTitular()));
        if (cliente == null) {
            cliente = new Cliente();
            cliente.setNumeroIdentificacion(Double.parseDouble(pago.getNroDocumentoTitular()));
            cliente.setTipoDocumento(tipoDocumento);
            cliente.setNombre(String.format("%s %s %s", pago.getNombreCliente(), pago.getApellidoPaterno(), pago.getApellidoMaterno()));
        }

        HistorialPago historialPago = this.historialPagoRepository.getByServicio(pago.getNumeroServicio());
        if (historialPago == null) {
            historialPago = new HistorialPago();
            historialPago.setServicio(pago.getNumeroServicio());
        }

        historialPago.setNumeroDocumentoPagado(pago.getNroDocPago());
        historialPago.setFechaIngreso(LocalDate.parse(pago.getFechaIngreso()));
        historialPago.setFechaPago(LocalDate.parse(pago.getFechaPago()));
        historialPago.setFechaVencimiento(LocalDate.parse(pago.getFechaVencimiento()));
        historialPago.setCodigoCargoColpatria(pago.getCodCargoColpatria());
        historialPago.setCodigoCargo(pago.getCodCargo());
        historialPago.setDescripcionCargo(pago.getDescCargo());
        historialPago.setMonto(pago.getSigno().equals("-") ? Double.parseDouble(pago.getMonto()) * (-1) : Double.parseDouble(pago.getMonto()));

        historialPago.setSucursal(Long.parseLong(pago.getSucursal()));
        historialPago.setCliente(cliente);
        historialPago.setSubProducto(subproducto);
        historialPago.setCuentaContable(pago.getCuentaContable());
        historialPago.setMontoParticipacion(Double.parseDouble(pago.getMontoParticipacion()));

        return historialPago;
    }
}
