package com.colpatria.midas.processors;

/*
 *
 * Libraries
 *
 */

import com.colpatria.midas.dto.MovimientoSaldoDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.model.Cargo;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.Estado;
import com.colpatria.midas.model.HistorialMovimientoSaldo;
import com.colpatria.midas.model.Negociacion;
import com.colpatria.midas.model.Producto;
import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.model.TipoProducto;
import com.colpatria.midas.model.Usuario;
import com.colpatria.midas.repositories.CargoRepository;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.EstadoRepository;
import com.colpatria.midas.repositories.NegociacionRepository;
import com.colpatria.midas.repositories.ProductoRepository;
import com.colpatria.midas.repositories.TipoDocumentoRepository;
import com.colpatria.midas.repositories.TipoProductoRepository;
import com.colpatria.midas.repositories.UsuarioRepository;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.DateUtils;
import org.springframework.batch.item.ItemProcessor;


/**
 * Class that is use to transform, filter and verify the items that are being read in a specific batch process
 */
public class MovimientoSaldoItemProcessor implements ItemProcessor<MovimientoSaldoDto, HistorialMovimientoSaldo> {

    /**
     * Constant to represent the product
     */
    private static final String PRODUCTO = "Producto";

    /**
     * Constant to represent the by-product
     */
    private static final String SUBPRODUCTO = "Subproducto";

    /**
     * Constant to represent the client
     */
    private static final String CLIENTE = "Cliente";

    /**
     * Constant to represent the contract
     */
    private static final String CONTRATO = "Contrato";

    /**
     * Contract repository to read and write to the database
     */
    private ContratoRepository contratoRepository;

    /**
     * User repository to read and write to the database
     */
    private UsuarioRepository usuarioRepository;

    /**
     * Position repository to read and write to the database
     */
    private CargoRepository cargoRepository;

    /**
     * Product repository to read and write to the database
     */
    private ProductoRepository productoRepository;

    /**
     * Product type repository to read and write to the database
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Client repository to read and write to the database
     */
    private ClienteRepository clienteRepository;

    /**
     * Document type repository to read and write to the database
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Negociation repository to read and write to the database
     */
    private NegociacionRepository negociacionRepository;

    /**
     * Service state repository to read and write to the database
     */
    private EstadoRepository estadoRepository;

    /**
     * File name
     */
    private String fileName;

    /**
     * validation service
     */
    private final ValidationService validationService;

    /*
     *
     * Methods
     *
     */

    /**
     * Constructor of the processor class
     *
     * @param fileName          identifies the file that is being read in the batch process
     * @param validationService is the validation service
     */
    public MovimientoSaldoItemProcessor(String fileName, ValidationService validationService) {
        this.fileName = fileName;
        this.validationService = validationService;
    }

    /**
     * Method that process the items that are being read from the file in the batch process
     *
     * @param item to be process
     * @return the item that has been process
     * @throws VerificationException in case that a mandatory field in the item is null or empty
     */
    @Override
    public HistorialMovimientoSaldo process(MovimientoSaldoDto item) throws VerificationException {
        // Variables
        HistorialMovimientoSaldo historialMovimientoSaldo;
        // Code
        validationService.cleanLineErrors();
        validationService.increaseRegisterCounter();
        valdateFileFields(item);
        if (this.validationService.isLineError()) {
            return null;
        }
        historialMovimientoSaldo = processHistorialMovimientoSaldo(item);
        if (this.validationService.isLineError()) {
            return null;
        }
        return historialMovimientoSaldo;
    }

    /**
     * Method that verifies the data
     *
     * @param item is the list of items to be written in the database
     */
    private void valdateFileFields(MovimientoSaldoDto item) {
        // Contract
        this.validationService.verifyEmpty(item.getCodigoProducto(), "código producto", this.fileName);
        this.validationService.verifyEmpty(item.getCodigoTipoProducto(), "código tipo producto", this.fileName);
        this.validationService.verifyEmpty(item.getCodigoSubproducto(), "código subproducto", this.fileName);
        this.validationService.validateIntegerWithZero(item.getTipoIdentificacion(), "tipo de identificación", this.fileName);
        this.validationService.validateIntegerWithZero(item.getNumeroIdentificacion(), "número de identificación", fileName);
        this.validationService.validateIntegerWithZero(item.getNumeroContrato(), "número de contrato", this.fileName);
        // Negociation
        this.validationService.verifyEmpty(item.getNumeroTransaccion(), "número de negociación", this.fileName);
        // History
        this.validationService.validateDouble(item.getValor(), "valor", this.fileName);
        this.validationService.verifyEmpty(item.getCodigoCargo(), "cargo", fileName);
        this.validationService.validateIntegerWithZero(item.getNumeroCompra(), "número de compra", this.fileName);
        this.validationService.validateDate(item.getFechaRealizacionAjuste(), "fecha de realización del ajuste", this.fileName);
        this.validationService.validateDate(item.getFechaAprobacionAjuste(), "fecha de aprobación del ajuste", this.fileName);
        this.validationService.verifyEmpty(item.getCodigoUsuarioCreador(), "código de usuario creador", fileName);
        this.validationService.verifyEmpty(item.getCodigoUsuarioAprobador(), "código de usuario aprobador", fileName);

        this.validationService.verifyEmpty(item.getEstado(), "estado del movimiento ", fileName);
    }

    /**
     * Method that writes the history to the database
     *
     * @param item is the list of items to be written in the database
     * @return the history object
     */
    private HistorialMovimientoSaldo processHistorialMovimientoSaldo(MovimientoSaldoDto item) {
        // Variables
        HistorialMovimientoSaldo historialMovimientoSaldo;
        Contrato contrato;
        Usuario usuarioCreador;
        Usuario usuarioAprobador;
        Cargo cargo;
        Negociacion negociacion;
        Estado estado;
        // Código
        usuarioCreador = processUser(item.getCodigoUsuarioCreador());
        usuarioAprobador = processUser(item.getCodigoUsuarioAprobador());
        // Position
        cargo = cargoRepository.getCargoById(item.getCodigoCargo());
        this.validationService.validateCatalogue(cargo, "Cargo", item.getCodigoCargo(), this.fileName);
        // History status
        estado = estadoRepository.getEstadoById( item.getEstado() );
        this.validationService.validateCatalogue(estado, "Estado del movimiento", item.getEstado(), this.fileName);
        // History
        contrato = processContract(item);
        historialMovimientoSaldo = new HistorialMovimientoSaldo();
        negociacion = processNegociation(item);
        historialMovimientoSaldo.setNegociacion(negociacion);
        historialMovimientoSaldo.setContrato(contrato);
        historialMovimientoSaldo.setValor(Double.parseDouble(item.getValor()));
        historialMovimientoSaldo.setEstado(estado);
        historialMovimientoSaldo.setTipoMovimiento(item.getTipoMovimiento());
        historialMovimientoSaldo.setCargo(cargo); // Preguntar esto
        historialMovimientoSaldo.setNumeroCompra(item.getNumeroCompra());
        historialMovimientoSaldo.setFechaRealizacionAjuste(DateUtils.stringToLocalDate(item.getFechaRealizacionAjuste()));
        historialMovimientoSaldo.setFechaAprobacionAjuste(DateUtils.stringToLocalDate(item.getFechaAprobacionAjuste()));
        historialMovimientoSaldo.setUsuarioCreador(usuarioCreador);
        historialMovimientoSaldo.setUsuarioAprobador(usuarioAprobador);
        historialMovimientoSaldo.setObservacion(item.getObservacion());
        return historialMovimientoSaldo;
    }

    /**
     * This method processes the contract
     *
     * @param item is an object that contains the data of the file
     * @return Contrato
     */
    private Contrato processContract(MovimientoSaldoDto item) {
        // Variables
        Producto producto;
        Producto subproducto;
        TipoProducto tipoProducto;
        Cliente cliente;
        Contrato contrato;
        TipoDocumento tipoDocumento;
        // Code
        tipoProducto = tipoProductoRepository.getTipoProductoByCodigo(item.getCodigoTipoProducto());
        this.validationService.validateCatalogue(tipoProducto, "Tipo producto", item.getCodigoTipoProducto(), this.fileName);
        // Product
        producto = productoRepository.getByCodigo(item.getCodigoProducto());
        this.validationService.validateCatalogue(producto, PRODUCTO, item.getCodigoProducto(), this.fileName);
        if (producto != null && tipoProducto != null) {
            this.validationService.validateParent(tipoProducto, producto.getTipo(), "Tipo producto", tipoProducto.getCodigo().toString(), PRODUCTO, this.fileName);
        }
        // Subproduct
        subproducto = productoRepository.getByCodigo(item.getCodigoSubproducto());
        this.validationService.validateCatalogue(subproducto, SUBPRODUCTO, item.getCodigoSubproducto(), this.fileName);
        if (subproducto != null && producto != null) {
            this.validationService.validateChild(producto, subproducto.getProductoPadre(), SUBPRODUCTO, PRODUCTO, this.fileName);
        }
        // Document type
        tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(item.getTipoIdentificacion());
        this.validationService.validateCatalogue(tipoDocumento, "Tipo Documento", item.getTipoIdentificacion(), this.fileName);
        // Client
        cliente = clienteRepository.getByNumeroIdentificacion(Double.parseDouble(item.getNumeroIdentificacion()));
        if (cliente == null) {
            cliente = new Cliente();
            cliente.setNumeroIdentificacion(Double.parseDouble(item.getNumeroIdentificacion()));
            cliente.setTipoDocumento(tipoDocumento);
        }
        // Contract
        contrato = contratoRepository.getByNumero(item.getNumeroContrato());
        if (contrato == null) {
            contrato = new Contrato();
            contrato.setNumero(item.getNumeroContrato());
            contrato.setSubProducto(subproducto);
            contrato.setCliente(cliente);
        } else {
            if (contrato.getCliente() == null) {
                contrato.setCliente( cliente );
            }
        }
        return contrato;
    }

    /**
     * This method processes the negotiation
     *
     * @param item is an object that contains the data of the file
     * @return Negociacion
     */
    private Negociacion processNegociation(MovimientoSaldoDto item) {
        // Variables
        Negociacion negociacion;
        // Código
        negociacion = negociacionRepository.getNegociacionById(item.getNumeroTransaccion());
        if (negociacion == null) {
            negociacion = new Negociacion();
            negociacion.setId(item.getNumeroTransaccion());
        }
        return negociacion;
    }

    /**
     * This method processes the user
     *
     * @param userId is an object that contains the data of the file
     * @return Usuario
     */
    private Usuario processUser(String userId) {
        // Variables
        Usuario usuario;
        // Código
        usuario = usuarioRepository.getUsuarioById(userId);
        if (usuario == null) {
            usuario = new Usuario();
            usuario.setId(userId);
        }
        return usuario;
    }

    /**
     * Set the contract repository
     *
     * @param contratoRepository is the repository object
     */
    public void setContratoRepository(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    /**
     * Set the user repository
     *
     * @param usuarioRepository is the repository object
     */
    public void setUsuarioRepository(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    /**
     * Set the position repository
     *
     * @param cargoRepository is the repository object
     */
    public void setCargoRepository(CargoRepository cargoRepository) {
        this.cargoRepository = cargoRepository;
    }

    /**
     * Set the product repository
     *
     * @param productoRepository is the repository object
     */
    public void setProductoRepository(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    /**
     * Set the product type repository
     *
     * @param tipoProductoRepository is the repository object
     */
    public void setTipoProductoRepository(TipoProductoRepository tipoProductoRepository) {
        this.tipoProductoRepository = tipoProductoRepository;
    }

    /**
     * Set the client repository
     *
     * @param clienteRepository is the repository object
     */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Set the document type repository
     *
     * @param tipoDocumentoRepository is the repository object
     */
    public void setTipoDocumentoRepository(TipoDocumentoRepository tipoDocumentoRepository) {
        this.tipoDocumentoRepository = tipoDocumentoRepository;
    }

    /**
     * Set the repository negotiation history
     *
     * @param negociacionRepository is the repository object
     */
    public void setNegociacionRepository(NegociacionRepository negociacionRepository) {
        this.negociacionRepository = negociacionRepository;
    }

    /**
     * Set the service state repository
     * @param estadoRepository is the repository object
    */
    public void setEstadoRepository(EstadoRepository estadoRepository) {
        this.estadoRepository = estadoRepository;
    }

}