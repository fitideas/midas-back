package com.colpatria.midas.processors;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;
import com.colpatria.midas.dto.AnulacionPagoDto;
import com.colpatria.midas.model.Cargo;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.HistorialAnulacionPago;
import com.colpatria.midas.model.Producto;
import com.colpatria.midas.model.Servicio;
import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.model.TipoProducto;
import com.colpatria.midas.repositories.CargoRepository;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.ProductoRepository;
import com.colpatria.midas.repositories.ServicioRepository;
import com.colpatria.midas.repositories.TipoDocumentoRepository;
import com.colpatria.midas.repositories.TipoProductoRepository;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.NumberUtils;
import org.springframework.batch.item.ItemProcessor;
import java.util.List;

/**
 * Class that is use to transform, filter and verify the items that are being read in a specific batch process
 */
public class AnulacionPagoItemProcessor implements  ItemProcessor<AnulacionPagoDto, HistorialAnulacionPago> {

    /**
     * Attribute that identifies the file that is being read in the batch process
     */
    private final String fileName;

	/**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

	/**
	 * add 'cargo' repository
	 */
    private final CargoRepository cargoRepository;

	/**
	 * add 'tipoProducto' repository
	 */
    private final TipoProductoRepository tipoProductoRepository;

	/**
	 * add 'producto' repository
	 */
    private final ProductoRepository productoRepository;

	/**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

	/**
	 * add 'contrato' repository
	 */
	private final ContratoRepository contratoRepository;

	/**
	 * add 'Tipo Documento' repository
	 */
	private final TipoDocumentoRepository tipoDocumentoRepository;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;

    /**
     * Constant for String 'Producto'
     */
    private static final String PRODUCTO = "Producto";

    /**
     * Constant for String 'Subproducto'
     */
    private static final String SUBPRODUCTO = "Subproducto";

    /**
     * Constant for String 'Cargo'
     */
    private static final String CARGO = "Cargo";

    /**
     * Constant for String 'TipoProducto'
     */
    private static final String TIPO_PRODUCTO = "TipoProducto";

    /**
     * Constant for String 'TipoDocumento'
     */
    private static final String TIPO_DOCUMENTO = "TipoDocumento";

    /**
     * Constant for String 'FechaIngreso'
     */
    private static final String FECHA_INGRESO = "FechaIngreso";

    /**
     * Constant for String 'FechaPago'
     */
    private static final String FECHA_PAGO = "FechaPago";

    /**
     * Constant for String 'FechaVencimiento'
     */
    private static final String FECHA_VENCIMIENTO = "FechaVencimiento";

    /**
     * Constant for String 'Monto'
     */
    private static final String MONTO = "Monto";

    /**
     * Constant for String 'MontoParticipacion'
     */
    private static final String MONTO_PARTICIPACION = "MontoParticipacion";

    /**
     * Constant for String 'Contrato'
     */
    private static final String CONTRATO = "Contrato";

    /**
     * Constructor of the processor class
     * @param fileName identifies the file that is being read in the batch process
     * @param servicioRepository dependency injection of servicioRepository
     * @param cargoRepository dependency injection of cargoRepository
     * @param tipoProductoRepository dependency injection of tipoProductoRepository
     * @param productoRepository dependency injection of productoRepository
     * @param clienteRepository dependency injection of clienteRepository
     * @param contratoRepository dependency injection of contratoRepository
     * @param tipoDocumentoRepository dependency injection of tipoDocumentoRepository
     * @param validationService dependency injection of validationService
     */
    public AnulacionPagoItemProcessor(
        String fileName,
        ValidationService validationService,
        ServicioRepository servicioRepository,
        CargoRepository cargoRepository,
        TipoProductoRepository tipoProductoRepository,
        TipoDocumentoRepository tipoDocumentoRepository,
        ProductoRepository productoRepository,
        ClienteRepository clienteRepository,
        ContratoRepository contratoRepository
    ) {
        this.fileName = fileName;
        this.validationService = validationService;
        this.servicioRepository = servicioRepository;
        this.cargoRepository = cargoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.productoRepository = productoRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository = contratoRepository;
    }

    /**
     * Method that process the items that are being read from the file in the batch process
     * @param item to be process
     * @return the item that has been process
     * @throws Exception in case that a mandatory field in the item is null or empty
     */
    @Override
    public HistorialAnulacionPago process(AnulacionPagoDto item) throws Exception {

        this.validationService.increaseRegisterCounter();
        this.validationService.cleanLineErrors();

        this.validationService.validateInteger(item.getProductoTipoCodigo(), TIPO_PRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getSubproductoCodigo(), SUBPRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getProductoCodigo(), PRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyEmpty(item.getCargoCodigo(), CARGO, this.fileName);

        String productoTipoCodigo = item.getProductoTipoCodigo();
        String productoCodigo = item.getProductoCodigo();
        String subproductoCodigo = item.getSubproductoCodigo();
        String tipoIdentificacion = item.getTipoIdentificacion();

        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(productoTipoCodigo);
        Producto subproducto = productoRepository.getByCodigo(subproductoCodigo);
        Producto producto = this.productoRepository.getProductoByCodigo(productoCodigo);
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(tipoIdentificacion);
        Cargo cargo = this.cargoRepository.getCargoById(item.getCargoCodigo());

        this.validationService.verifyCatalog(tipoProducto, TIPO_PRODUCTO, this.fileName);
        this.validationService.verifyCatalog(subproducto, SUBPRODUCTO, this.fileName);
        this.validationService.verifyCatalog(producto, PRODUCTO, this.fileName);
        this.validationService.verifyCatalog(tipoDocumento, TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyCatalog(cargo, CARGO, this.fileName);
        this.validationService.validateDate(item.getFechaIngreso(), FECHA_INGRESO, this.fileName);
        this.validationService.validateDate(item.getFechaPago(), FECHA_PAGO, this.fileName);
        this.validationService.validateDate(item.getFechaVencimiento(), FECHA_VENCIMIENTO, this.fileName);
        this.validationService.validateDouble(item.getMonto(), MONTO, this.fileName);
        this.validationService.validateDouble(item.getMontoParticipacion(), MONTO_PARTICIPACION, this.fileName);
        this.validationService.validateInteger(item.getContratoNumero(), CONTRATO, this.fileName);

        if(subproducto != null && producto != null){
            String codigoProductoPadre = subproducto.getProductoPadre() != null ? subproducto.getProductoPadre().getCodigo() : "";
            this.validationService.verifyParent(producto.getCodigo(), codigoProductoPadre, PRODUCTO, SUBPRODUCTO, this.fileName);
        }
        if( producto != null && tipoProducto != null ) {
            this.validationService.verifyParent(producto.getTipo().getCodigo(), tipoProducto.getCodigo(), PRODUCTO, TIPO_PRODUCTO, this.fileName);
        }

        if(this.validationService.isLineError()){
            return null;
        }
        
        return createHistorialAnulacionPago(item, subproducto, tipoDocumento, cargo);
    }

    HistorialAnulacionPago createHistorialAnulacionPago (AnulacionPagoDto item, Producto subproducto, TipoDocumento tipoDocumento, Cargo cargo){

        HistorialAnulacionPago historialAnulacionPago = new HistorialAnulacionPago();

        Optional<Cliente> optionalCliente = clienteRepository.findById(Double.parseDouble(item.getNumeroIdentificacion()));
        Cliente cliente = new Cliente();
        if(!optionalCliente.isPresent()){
            cliente.setNumeroIdentificacion(Double.parseDouble(item.getNumeroIdentificacion()));
            cliente.setNombre(item.getClienteNombre());
            cliente.setTipoDocumento(tipoDocumento);
        }else{
            cliente = optionalCliente.get();
        }

        String contratoNumero =  item.getContratoNumero();
        Optional<Contrato> optionalContrato = contratoRepository.findById(contratoNumero);
        Contrato contrato = new Contrato();
        if(!optionalContrato.isPresent()){
            contrato.setNumero(contratoNumero);
            contrato.setSubProducto(subproducto);
        }else{
            contrato = optionalContrato.get();
        }
        if(contrato.getCliente() == null){
            contrato.setCliente(cliente);
        }
        
        Optional<Servicio> optionalServicio = servicioRepository.findById(item.getServicioNumero());
        Servicio servicio = new Servicio();
        if(!optionalServicio.isPresent()){
            servicio.setNumero(item.getServicioNumero());
        }else{
            servicio = optionalServicio.get();
        }
        if(servicio.getContrato() == null){
            servicio.setContrato(contrato);
        }

        historialAnulacionPago.setCargo(cargo);
        historialAnulacionPago.setServicio(servicio);
        historialAnulacionPago.setFechaIngreso(LocalDate.parse(item.getFechaIngreso()));
        historialAnulacionPago.setFechaPago(LocalDate.parse(item.getFechaPago()));
        historialAnulacionPago.setFechaVencimiento(LocalDate.parse(item.getFechaVencimiento()));
        historialAnulacionPago.setMonto(Double.parseDouble(item.getMonto()));
        historialAnulacionPago.setMontoParticipacion(Double.parseDouble(item.getMontoParticipacion()));
        historialAnulacionPago.setNumeroDocumentoPagado(item.getNumeroDocumentoPagado());

        return historialAnulacionPago;
    }
    
}
