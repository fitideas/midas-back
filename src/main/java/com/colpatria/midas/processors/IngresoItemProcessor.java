package com.colpatria.midas.processors;

import com.colpatria.midas.constants.CacheConstants;
import com.colpatria.midas.dto.IngresoDto;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.cache.CacheManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class IngresoItemProcessor implements ItemProcessor<IngresoDto, Ingreso> {

    /**
     * Add logger to pagos config
     */
    private static final Logger logger = LoggerFactory.getLogger(PagoItemProcessor.class);

    /**
     * add servicio repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * Add file name
     */
    private String fileName;
    /**
     * Add email service
     */
    private EmailService emailService;

    /**
     * add product repository
     */
    private ProductoRepository productoRepository;

    /**
     * add product repository
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Tipo Documento repository
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Add client repository
     */
    private ClienteRepository clienteRepository;

    /**
     * Add Condonacion repository
     */
    private IngresoRepository ingresoRepository;

    /**
     * add cuenta repository
     */
    private final MunicipioRepository municipioRepository;

    /**
     * validation service
     */
    private ValidationService validationService;

    /**
     * add contrato repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * add cargo repository
     */
    private final CargoRepository cargoRepository;

    /**
     * Cache Manager in the processor
     */
    private CacheManager cacheManager;

    /**
     * Method that configures the processor for the batch process
     * @param fileName being read
     * @return object of the porcessor class
     */
    public IngresoItemProcessor(String fileName,
                                ServicioRepository servicioRepository,
                                ProductoRepository productoRepository,
                                TipoProductoRepository tipoProductoRepository,
                                TipoDocumentoRepository tipoDocumentoRepository,
                                ClienteRepository clienteRepository,
                                MunicipioRepository municipioRepository,
                                ContratoRepository contratoRepository,
                                CargoRepository cargoRepository,
                                ValidationService validationService,
                                CacheManager cacheManager){
        this.fileName = fileName;
        this.servicioRepository = servicioRepository;
        this.productoRepository = productoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.clienteRepository = clienteRepository;
        this.municipioRepository = municipioRepository;
        this.contratoRepository = contratoRepository;
        this.cargoRepository = cargoRepository;
        this.validationService = validationService;
        this.cacheManager = cacheManager;
    }

    /**
     * Method that process the items that are being read from the file in the batch process
     * @param ingresoDto to be process
     * @return the item that has been process
     * @throws Exception in case that a mandatory field in the item is null or empty
     */
    public Ingreso process(IngresoDto ingresoDto){
        this.validationService.cleanLineErrors();
        this.validationService.increaseRegisterCounter();

        //Date fields validation
        if (!this.validationService.isEmptyField(ingresoDto.getFechaCausacion(), "FechaCausacion", this.fileName))
            this.validationService.validateDate(ingresoDto.getFechaCausacion(), "FechaCausacion", this.fileName);
        this.validationService.validateDate(ingresoDto.getFechaDocumento(), "FechaDocumento", this.fileName);

        //Integer Fields Validation
        if (!this.validationService.isEmptyField(ingresoDto.getCodigoProducto(), "CodigoProducto", this.fileName))
            this.validationService.validateInteger(ingresoDto.getCodigoProducto(), "CodigoProducto", this.fileName);
        if (!this.validationService.isEmptyField(ingresoDto.getCodigoSubproducto(), "CodigoSubproducto", this.fileName))
            this.validationService.validateInteger(ingresoDto.getCodigoSubproducto(), "CodigoSubproducto", this.fileName);
        if (!this.validationService.isEmptyField(ingresoDto.getTipoDocumento(), "TipoDocumento", this.fileName))
            this.validationService.validateInteger(ingresoDto.getTipoDocumento(), "TipoDocumento", this.fileName);

        //Long Fields Validation
        if (!this.validationService.isEmptyField(ingresoDto.getNumeroDocumento(), "NumeroIdentificacion", this.fileName))
            this.validationService.validateLong(ingresoDto.getNumeroDocumento(), "NumeroIdentificacion", this.fileName);
        if (!this.validationService.isEmptyField(ingresoDto.getNumeroServicio(), "NumeroServicio", this.fileName))
            this.validationService.validateLong(ingresoDto.getNumeroServicio(), "NumeroServicio", this.fileName);
        if (!this.validationService.isEmptyField(ingresoDto.getNumeroContrato(), "NumeroContrato", this.fileName))
            this.validationService.validateLong(ingresoDto.getNumeroContrato(), "NumeroContrato", this.fileName);
        this.validationService.validateLong(ingresoDto.getMonto(), "Monto", this.fileName);

        //Double Fields validation
        this.validationService.validateDouble(ingresoDto.getTasaInteresCorriente(),"TasaInteresCorriente",this.fileName);
        this.validationService.validateDouble(ingresoDto.getTasaInteresMora(),"TasaInteresMora",this.fileName);
        this.validationService.isEmptyField(ingresoDto.getCodigoCargo(),"codigoCargo",this.fileName);

        if (this.validationService.isLineError()){
            return null;
        }

        //Catalogue Entities
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(ingresoDto.getTipoDocumento());
        this.validationService.verifyCatalog(tipoDocumento, "TipoDocumento", this.fileName);
        Producto producto = productoRepository.getByCodigo(ingresoDto.getCodigoProducto());
        this.validationService.verifyCatalog(producto, "CodigoProducto", this.fileName);
        Producto subproducto = productoRepository.getByCodigo(ingresoDto.getCodigoSubproducto());
        this.validationService.verifyCatalog(subproducto, "CodigoSubproducto", this.fileName);
        Municipio municipio = null;
        try {
            municipio = municipioRepository.getByCodigo(Long.parseLong(ingresoDto.getMunicipio()));
        }catch (NumberFormatException nex){

        }
        this.validationService.verifyCatalog(municipio, "Municipio", this.fileName);
        Cargo cargo = cargoRepository.getCargoById(ingresoDto.getCodigoCargo());
        this.validationService.verifyCatalog(cargo,"CodigoCargo", this.fileName);

        // Verification Validation
        if (this.validationService.isLineError()) {
            return null;
        }

        // Subproducto - producto relationship Validation
        String codigo = subproducto!=null && subproducto.getProductoPadre() != null ? subproducto.getProductoPadre().getCodigo() : "";
        if (producto!=null)
            this.validationService.verifyParent(producto.getCodigo(), codigo, "Producto", "SubProducto", this.fileName);

        // Verification Validation
        if (this.validationService.isLineError()) {
            return null;
        }
        //Getting Related Entities
        Contrato contrato = null;
        Cliente cliente = null;
        Servicio servicio = null;

        if (cacheManager.getCache(CacheConstants.contrato).get(ingresoDto.getNumeroContrato())!=null)
            contrato = (Contrato) cacheManager.getCache(CacheConstants.contrato).get(ingresoDto.getNumeroContrato()).get();
        else
            contrato=this.contratoRepository.getByNumero(ingresoDto.getNumeroContrato());
        if (contrato!=null && contrato.getCliente()!=null)
            cliente = contrato.getCliente();
        else{
            if (cacheManager.getCache(CacheConstants.cliente).get(ingresoDto.getNumeroDocumento())!=null)
                cliente = (Cliente) cacheManager.getCache(CacheConstants.cliente).get(ingresoDto.getNumeroDocumento()).get();
            else
                cliente = this.clienteRepository.getByNumeroIdentificacion(Double.parseDouble(ingresoDto.getNumeroDocumento()));
        }
        if (cacheManager.getCache(CacheConstants.service).get(ingresoDto.getNumeroServicio())!=null)
            servicio = (Servicio) cacheManager.getCache(CacheConstants.service).get(ingresoDto.getNumeroServicio()).get();
        else
            servicio = this.servicioRepository.getByNumero(ingresoDto.getNumeroServicio());

        //Validate
        if (cliente == null) {
            cliente = new Cliente();
            cliente.setTipoDocumento(tipoDocumento);
            cliente.setNumeroIdentificacion(Double.parseDouble(ingresoDto.getNumeroDocumento()));
        }

        if (contrato==null){
            contrato=new Contrato();
            contrato.setNumero(ingresoDto.getNumeroContrato());
            contrato.setSubProducto(subproducto);
            contrato.setCliente(cliente);
        }

        if (contrato.getCliente()==null)
            contrato.setCliente(cliente);

        if (servicio==null){
            servicio = new Servicio();
            servicio.setNumero(ingresoDto.getNumeroServicio());
            servicio.setContrato(contrato);
        }
        if (servicio.getContrato()==null)
            servicio.setContrato(contrato);
        if (contrato.getServicios()==null || contrato.getServicios().size()==0){
            List<Servicio> servicios = new ArrayList<Servicio>();
            servicios.add(servicio);
            contrato.setServicios(servicios);
        }else{
            contrato.getServicios().add(servicio);
        }

        cacheManager.getCache(CacheConstants.cliente).put(ingresoDto.getNumeroDocumento(), cliente);
        cacheManager.getCache(CacheConstants.contrato).put(ingresoDto.getNumeroContrato(), contrato);
        cacheManager.getCache(CacheConstants.service).put(ingresoDto.getNumeroServicio(), servicio);

        //Ingreso
        Ingreso ingreso = new Ingreso();
        ingreso.setServicioNumero(servicio);
        ingreso.setCodigoCargo(cargo);
        ingreso.setFechaDocumento(DateUtils.stringToLocalDate(ingresoDto.getFechaDocumento()));
        ingreso.setFechaCausacion(DateUtils.stringToLocalDate(ingresoDto.getFechaCausacion()));
        ingreso.setTasaInteresMora(new BigDecimal(ingresoDto.getTasaInteresMora()));
        ingreso.setTasaInteresCorriente(new BigDecimal(ingresoDto.getTasaInteresCorriente()));
        ingreso.setMonto(new BigDecimal(ingresoDto.getMonto()));
        ingreso.setSigno(ingresoDto.getSigno());
        ingreso.setCuentaContable(ingresoDto.getCuentaContable());
        ingreso.setMunicipio(municipio);
        return ingreso;
    }



}
