package com.colpatria.midas.processors;

import java.time.LocalDate;

/*
*
* Libraries
*
*/

import com.colpatria.midas.dto.RelacionClienteSubproductoDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.Cuenta;
import com.colpatria.midas.model.HistorialRelacionClienteSubproducto;
import com.colpatria.midas.model.Producto;
import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.model.TipoProducto;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.CuentaRepository;
import com.colpatria.midas.repositories.ProductoRepository;
import com.colpatria.midas.repositories.TipoDocumentoRepository;
import com.colpatria.midas.repositories.TipoProductoRepository;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.DateUtils;
import org.springframework.batch.item.ItemProcessor;

/**
 * Class that is use to transform, filter and verify the items that are being read in a specific batch process
*/
public class RelacionClienteSubproductoItemProcessor implements ItemProcessor<RelacionClienteSubproductoDto, HistorialRelacionClienteSubproducto> {

    /**
     * Constant to represent the product
     */
    private static final String PRODUCTO = "Producto";

    /**
     * Constant to represent the by-product
     */
    private static final String SUBPRODUCTO = "Subproducto";

    /**
     * File name
    */
    private String fileName;

    /**
     * validation service
     */
    private final ValidationService validationService;

    /**
     * Product repository to read and write to the database
     */
    private ProductoRepository productoRepository;

    /**
     * Product type repository to read and write to the database
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Contract repository to read and write to the database
     */
    private ContratoRepository contratoRepository;

    /**
     * Client repository to read and write to the database
     */
    private ClienteRepository clienteRepository;

    /**
     * Document type repository to read and write to the database
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Banck account repository to read and write to the database
    */
    private CuentaRepository cuentaRepository;

    /**
     * Constructor of the processor class
     * @param fileName identifies the file that is being read in the batch process
     * @param validationService is the validation service
    */
    public RelacionClienteSubproductoItemProcessor( String fileName, ValidationService validationService ) {
        this.fileName          = fileName;
        this.validationService = validationService;
    }

    /**
     * Method that process the items that are being read from the file in the batch process
     * @param item to be process
     * @return the item that has been process
     * @throws VerificationException in case that a mandatory field in the item is null or empty
     */
    @Override
    public HistorialRelacionClienteSubproducto process(RelacionClienteSubproductoDto item) throws VerificationException {
        // Variables
        HistorialRelacionClienteSubproducto historialRelacionClienteSubproducto;
        // Code
        validationService.cleanLineErrors();
        validationService.increaseRegisterCounter();
        valdateFileFields(item);
        if (this.validationService.isLineError()) {
            return null;
        }
        historialRelacionClienteSubproducto = processHistorialRelacionClienteSubproducto(item);
        if (this.validationService.isLineError()) {
            return null;
        }
        return historialRelacionClienteSubproducto;
    }

    /**
     * Method that verifies the data
     *
     * @param item is the list of items to be written in the database
     */
    private void valdateFileFields(RelacionClienteSubproductoDto item) {
        // Contract
        this.validationService.verifyEmpty(item.getCodigoProducto(), "código producto", this.fileName);
        this.validationService.verifyEmpty(item.getCodigoTipoProducto(), "código tipo producto", this.fileName);
        this.validationService.verifyEmpty(item.getCodigoSubproducto(), "código subproducto", this.fileName);
        this.validationService.verifyEmpty(item.getTipoIdentificacion(), "tipo de identificación", this.fileName);
        this.validationService.validateIntegerWithZero(item.getNumeroIdentificacion(), "número de identificación", fileName);
        this.validationService.validateIntegerWithZero(item.getNumeroContrato(), "número de contrato", this.fileName);
        this.validationService.validateIntegerWithZero(item.getNumeroCuenta(), "número de cuenta", this.fileName);
        this.validationService.verifyEmpty(item.getEstado(), "estado del contrato", fileName);
        this.validationService.verifyZero(item.getEstado(), "estado del contrato", fileName);
        // History
        this.validationService.validateDouble(item.getValorCuotaUtilizacion(), "valor de la cuota de utilización", this.fileName);
        this.validationService.validateDate(item.getFechaOrigenRelacion(), "fecha origen de la relación", this.fileName);
        this.validationService.validateDate(item.getFechaCancelacionRelacion(), "fecha cancelación de la relación", this.fileName);
        this.validationService.validateDate(item.getFechaUpgrade(), "fecha de upgrade", this.fileName);
        this.validationService.validateDate(item.getFechaInicialCastigo(), "fecha inicial del castigo", this.fileName);
        this.validationService.validateDate(item.getFechaFinCastigo(), "fecha fin del castigo", this.fileName);
        this.validationService.validateDate(item.getFechaInicialEnvioFactura(), "fecha inicial del envío de la factura", this.fileName);
        this.validationService.validateDate(item.getFechaFinEnvioFactura(), "fecha fin del envío de la factura", this.fileName);
    }

     /**
     * Method that writes the history to the database
     *
     * @param item is the list of items to be written in the database
     * @return the history object
     */
    private HistorialRelacionClienteSubproducto processHistorialRelacionClienteSubproducto(RelacionClienteSubproductoDto item) {
        // Variables
        HistorialRelacionClienteSubproducto historialRelacionClienteSubproducto;
        Contrato contrato;
        // Code
        // History
        contrato = processContract(item);
        historialRelacionClienteSubproducto = new HistorialRelacionClienteSubproducto();
        historialRelacionClienteSubproducto.setTipoCliente(item.getTipoCliente());
        historialRelacionClienteSubproducto.setValorCuotaUtilizacion(Double.parseDouble(item.getValorCuotaUtilizacion()));
        historialRelacionClienteSubproducto.setCodigoSeguroObligatorio(item.getCodigoSeguroObligatorio());
        historialRelacionClienteSubproducto.setFechaOrigenRelacion(DateUtils.stringToLocalDate(item.getFechaOrigenRelacion()));
        historialRelacionClienteSubproducto.setFechaCancelacionRelacion(DateUtils.stringToLocalDate(item.getFechaCancelacionRelacion()));
        historialRelacionClienteSubproducto.setUpgrade(Boolean.parseBoolean(item.getUpgrade()));
        historialRelacionClienteSubproducto.setFechaUpgrade(DateUtils.stringToLocalDate(item.getFechaUpgrade()));
        historialRelacionClienteSubproducto.setFechaInicialCastigo(DateUtils.stringToLocalDate(item.getFechaInicialCastigo()));
        historialRelacionClienteSubproducto.setFechaFinCastigo(DateUtils.stringToLocalDate(item.getFechaFinCastigo()));
        historialRelacionClienteSubproducto.setFechaInicialEnvioFactura(DateUtils.stringToLocalDate(item.getFechaInicialEnvioFactura()));
        historialRelacionClienteSubproducto.setFechaFinEnvioFactura(DateUtils.stringToLocalDate(item.getFechaFinEnvioFactura()));
        historialRelacionClienteSubproducto.setMotivoNoEnvioFactura(item.getMotivoNoEnvioFactura());
        historialRelacionClienteSubproducto.setContrato(contrato);
        historialRelacionClienteSubproducto.setFechaGeneracion(LocalDate.now());
        return historialRelacionClienteSubproducto;
    }

    /**
     * This method processes the contract
     *
     * @param item is an object that contains the data of the file
     * @return Contrato
     */
    private Contrato processContract(RelacionClienteSubproductoDto item) {
        // Variables
        Producto producto;
        Producto subproducto;
        TipoProducto tipoProducto;
        Cliente cliente;
        Contrato contrato;
        TipoDocumento tipoDocumento;
        Cuenta cuenta;
        // Code
        tipoProducto = tipoProductoRepository.getTipoProductoByCodigo(item.getCodigoTipoProducto());
        this.validationService.validateCatalogue(tipoProducto, "Tipo producto", item.getCodigoTipoProducto(), this.fileName);
        // Product
        producto = productoRepository.getByCodigo(item.getCodigoProducto());
        this.validationService.validateCatalogue(producto, PRODUCTO, item.getCodigoProducto(), this.fileName);
        if (producto != null && tipoProducto != null) {
            this.validationService.validateParent(tipoProducto, producto.getTipo(), "Tipo producto", tipoProducto.getCodigo(), PRODUCTO, this.fileName);
        }
        // Subproduct
        subproducto = productoRepository.getByCodigo(item.getCodigoSubproducto());
        this.validationService.validateCatalogue(subproducto, SUBPRODUCTO, item.getCodigoSubproducto(), this.fileName);
        if (subproducto != null && producto != null) {
            this.validationService.validateChild(producto, subproducto.getProductoPadre(), SUBPRODUCTO, PRODUCTO, this.fileName);
        }
        // Account
        cuenta = cuentaRepository.getByNumero( Double.parseDouble( item.getNumeroCuenta() ) );
        if( cuenta == null ) {
            cuenta = new Cuenta();
            cuenta.setNumero( Double.parseDouble( item.getNumeroCuenta() ) );
        }
        // Document type
        tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(item.getTipoIdentificacion());
        this.validationService.validateCatalogue(tipoDocumento, "Tipo Documento", item.getTipoIdentificacion(), this.fileName);
        // Client
        cliente = clienteRepository.getByNumeroIdentificacion(Double.parseDouble(item.getNumeroIdentificacion()));
        if (cliente == null) {
            cliente = new Cliente();
            cliente.setNumeroIdentificacion(Double.parseDouble(item.getNumeroIdentificacion()));
        }
        cliente.setNombre(item.getNombreCliente());
        if(cliente.getTipoDocumento() == null) {
            cliente.setTipoDocumento(tipoDocumento);
        }
        if(cliente.getCuenta() == null) {
            cliente.setCuenta(cuenta);
        }
        // Contract
        contrato = contratoRepository.getByNumero(item.getNumeroContrato());
        if (contrato == null) {
            contrato = new Contrato();
            contrato.setNumero(item.getNumeroContrato());
        }
        contrato.setFacturacion(Boolean.parseBoolean(item.getFacturacion()));
        contrato.setEstado(item.getEstado());
        if(contrato.getSubProducto() == null){
            contrato.setSubProducto(subproducto);
        }
        if(contrato.getCliente() == null){
            contrato.setCliente(cliente);
        }
        return contrato;
    }

    /**
     * Set the product repository
     *
     * @param productoRepository is the repository object
     */
    public void setProductoRepository(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    /**
     * Set the product type repository
     *
     * @param tipoProductoRepository is the repository object
     */
    public void setTipoProductoRepository(TipoProductoRepository tipoProductoRepository) {
        this.tipoProductoRepository = tipoProductoRepository;
    }

    /**
     * Set the contract repository
     *
     * @param contratoRepository is the repository object
     */
    public void setContratoRepository(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    /**
     * Set the client repository
     *
     * @param clienteRepository is the repository object
     */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Set the document type repository
     *
     * @param tipoDocumentoRepository is the repository object
     */
    public void setTipoDocumentoRepository(TipoDocumentoRepository tipoDocumentoRepository) {
        this.tipoDocumentoRepository = tipoDocumentoRepository;
    }

    /**
     * Set the account repository
     * @param cuentaRepository is the repository object
    */
    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }
    
}
