package com.colpatria.midas.processors;

import java.time.LocalDate;
import java.util.Optional;
import com.colpatria.midas.dto.CargueUtilizacionDto;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ProductoRepository;
import com.colpatria.midas.repositories.ServicioRepository;
import com.colpatria.midas.repositories.TipoConsumoRepository;
import com.colpatria.midas.repositories.TipoDocumentoRepository;
import com.colpatria.midas.repositories.TipoProductoRepository;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.NumberUtils;
import org.springframework.batch.item.ItemProcessor;

/**
 * Class that is use to transform, filter and verify the items that are being read in a specific batch process
 */
public class CargueUtilizacionItemProcessor implements  ItemProcessor<CargueUtilizacionDto, HistorialCargueUtilizacion> {

    /**
     * Attribute that identifies the file that is being read in the batch process
     */
    private String fileName;

    /**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

	/**
	 * add 'Tipo Documento' repository
	 */
	private final TipoDocumentoRepository tipoDocumentoRepository;
	
	/*
	 * add 'tipoProducto' repository
	 */
    private final TipoProductoRepository tipoProductoRepository;

	/**
	 * add 'producto' repository
	 */
    private final ProductoRepository productoRepository;

	/**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

    /**
	 * add 'tipo consumo' repository
	 */
	private final TipoConsumoRepository tipoConsumoRepository;

    /**
	 * Validation service
	 */
	private final ValidationService validationService;

    /**
     * Constant for String 'Producto'
     */
    private static final String PRODUCTO = "Producto";

    /**
     * Constant for String 'Subproducto'
     */
    private static final String SUBPRODUCTO = "Subproducto";

    /**
     * Constant for String 'TipoProducto'
     */
    private static final String TIPO_PRODUCTO = "TipoProducto";

    /**
     * Constant for String 'TipoDocumento'
     */
    private static final String TIPO_DOCUMENTO = "TipoDocumento";

    /**
     * Constant for String 'TipoConsumo'
     */
    private static final String TIPO_CONSUMO = "TipoConsumo";

    /**
     * Constant for String 'FechaPosteo'
     */
    private static final String FECHA_POSTEO = "FechaPosteo";

    /**
     * Constant for String 'FechaEfectiva'
     */
    private static final String FECHA_EFECTIVA = "FechaEfectiva";

    /**
     * Constant for String 'Valor'
     */
    private static final String VALOR = "Valor";

    /**
     * Constant for String 'ValorDescuento'
     */
    private static final String VALOR_DESCUENTO = "ValorDescuento";

    /**
     * Constant for String 'ValorFinanciado'
     */
    private static final String VALOR_FINANCIADO = "ValorFinanciado";

    /**
     * Constant for String 'Tasa'
     */
    private static final String TASA = "Tasa";

    /**
     * Constant for String 'NumeroAutorizacion'
     */
    private static final String NUMERO_AUTORIZACION = "NumeroAutorizacion";

    /**
     * Constructor of the processor class
     * @param fileName identifies the file that is being read in the batch process
     * @param validationService dependency injection of validationService
	 * @param servicioRepository dependency injection of servicioRepository
	 * @param tipoProductoRepository dependency injection of tipoProductoRepository
	 * @param productoRepository dependency injection of productoRepository
	 * @param clienteRepository dependency injection of clienteRepository
	 * @param tipoConsumoRepository dependency injection of tipoConsumoRepository
	 * @param tipoDocumentoRepository dependency injection of tipoDocumentoRepository
	 */
    public CargueUtilizacionItemProcessor(
        String fileName,
        ValidationService validationService,
		ServicioRepository servicioRepository,
        TipoDocumentoRepository tipoDocumentoRepository,
        TipoProductoRepository tipoProductoRepository,
        ProductoRepository productoRepository,
        ClienteRepository clienteRepository,
        TipoConsumoRepository tipoConsumoRepository
    ) {
        this.fileName = fileName;
        this.validationService = validationService;
		this.servicioRepository = servicioRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.productoRepository = productoRepository;
        this.clienteRepository = clienteRepository;
        this.tipoConsumoRepository = tipoConsumoRepository;
    }

    /**
     * Method that process the items that are being read from the file in the batch process
     * @param item to be process
     * @return the item that has been process
     * @throws Exception
     */
    @Override
    public HistorialCargueUtilizacion process(CargueUtilizacionDto item) throws Exception {

        this.validationService.increaseRegisterCounter();
        this.validationService.cleanLineErrors();

        this.validationService.validateInteger(item.getProductoTipoCodigo(), TIPO_PRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getSubproductoCodigo(), SUBPRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getProductoCodigo(), PRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyEmpty(item.getTipoConsumo(), TIPO_CONSUMO, this.fileName);

        String productoTipoCodigo = item.getProductoTipoCodigo();
        String productoCodigo = item.getProductoCodigo();
        String subproductoCodigo = item.getSubproductoCodigo();
        String tipoIdentificacion = item.getTipoIdentificacion();

        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(productoTipoCodigo);
        Producto subproducto = productoRepository.getByCodigo(subproductoCodigo);
        Producto producto = this.productoRepository.getProductoByCodigo(productoCodigo);
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(tipoIdentificacion);
        TipoConsumo tipoConsumo = this.tipoConsumoRepository.getTipoConsumoById(Integer.parseInt(item.getTipoConsumo()));

        this.validationService.verifyCatalog(tipoProducto, TIPO_PRODUCTO, this.fileName);
        this.validationService.verifyCatalog(subproducto, SUBPRODUCTO, this.fileName);
        this.validationService.verifyCatalog(producto, PRODUCTO, this.fileName);
        this.validationService.verifyCatalog(tipoDocumento, TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyCatalog(tipoConsumo, TIPO_CONSUMO, this.fileName);
        this.validationService.validateDate(item.getFechaPosteo(), FECHA_POSTEO, this.fileName);
        this.validationService.validateDate(item.getFechaEfectiva(), FECHA_EFECTIVA, this.fileName);
        this.validationService.validateDouble(item.getValor(), VALOR, this.fileName);
        this.validationService.validateDouble(item.getValorDescuento(), VALOR_DESCUENTO, this.fileName);
        this.validationService.validateDouble(item.getValorFinanciado(), VALOR_FINANCIADO, this.fileName);
        this.validationService.validateDouble(item.getTasa(), TASA, this.fileName);
        this.validationService.validateInteger(item.getAutorizacionNumero(), NUMERO_AUTORIZACION, this.fileName);

        if(subproducto != null && producto != null){
            String codigoProductoPadre = subproducto.getProductoPadre() != null ? subproducto.getProductoPadre().getCodigo() : "";
            this.validationService.verifyParent(producto.getCodigo(), codigoProductoPadre, PRODUCTO, SUBPRODUCTO, this.fileName);
        }
        if( producto != null && tipoProducto != null ) {
            this.validationService.verifyParent(producto.getTipo().getCodigo(), tipoProducto.getCodigo(), PRODUCTO, TIPO_PRODUCTO, this.fileName);
        }

        if(this.validationService.isLineError()){
            return null;
        }

        return createHistorialCargueUtilizacion(item, subproducto, tipoDocumento, tipoConsumo);
    }

    HistorialCargueUtilizacion createHistorialCargueUtilizacion (CargueUtilizacionDto item, Producto subproducto, TipoDocumento tipoDocumento, TipoConsumo tipoConsumo){

        HistorialCargueUtilizacion historialCargueUtilizacion = new HistorialCargueUtilizacion();

        Optional<Cliente> optionalCliente = clienteRepository.findById(Double.parseDouble(item.getNumeroIdentificacion()));
        Cliente cliente = new Cliente();
        if (!optionalCliente.isPresent()) {
            cliente.setNumeroIdentificacion(Double.parseDouble(item.getNumeroIdentificacion()));
            cliente.setTipoDocumento(tipoDocumento);
            cliente.setNombre(item.getClienteNombre());
        }else{
            cliente = optionalCliente.get();
        }

        Optional<Servicio> optionalServicio = servicioRepository.findById(item.getServicioNumero());
        Servicio servicio = new Servicio();
        if(!optionalServicio.isPresent()){
            servicio.setNumero(item.getServicioNumero());
            servicio.setTipoConsumo(Integer.parseInt(item.getTipoConsumo()));
            servicio.setTasa(Double.parseDouble(item.getTasa()));
        }else{
            servicio = optionalServicio.get();
        }

        historialCargueUtilizacion.setSubProducto(subproducto);
        historialCargueUtilizacion.setCliente(cliente);
        historialCargueUtilizacion.setServicio(servicio);
        historialCargueUtilizacion.setCiudadOrigen(item.getCiudadOrigen());
        historialCargueUtilizacion.setFechaEfectiva(LocalDate.parse(item.getFechaEfectiva()));
        historialCargueUtilizacion.setFechaPosteo(LocalDate.parse(item.getFechaPosteo()));
        historialCargueUtilizacion.setValor(Double.parseDouble(item.getValor()));
        historialCargueUtilizacion.setValorDescuento(Double.parseDouble(item.getValorDescuento()));
        historialCargueUtilizacion.setValorFinanciado(Double.parseDouble(item.getValorFinanciado()));
        historialCargueUtilizacion.setTasa(Double.parseDouble(item.getTasa()));
        historialCargueUtilizacion.setNumeroAutorizacion(item.getAutorizacionNumero());

        return historialCargueUtilizacion;
    }
    
}

