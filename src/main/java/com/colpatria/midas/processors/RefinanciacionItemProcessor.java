package com.colpatria.midas.processors;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.dto.RefinanciacionDto;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.Cuenta;
import com.colpatria.midas.model.HistorialRefinanciacion;
import com.colpatria.midas.model.Negociacion;
import com.colpatria.midas.model.Producto;
import com.colpatria.midas.model.Servicio;
import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.model.TipoProducto;
import com.colpatria.midas.model.Usuario;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.DateUtils;
import org.springframework.batch.item.ItemProcessor;

/*
 *
 * Clase
 *
*/

public class RefinanciacionItemProcessor implements ItemProcessor<RefinanciacionDto, HistorialRefinanciacion>{

    /**
     * File name
    */
    private String fileName;

    /**
     * Constant to represent the document type
    */
    private static final String TIPO_DOCUMENTO = "TipoDocumento";

    /**
     * Constant to represent the product
    */
    private static final String PRODUCTO = "Producto";

    /**
     * Constant to represent the by-product
    */
    private static final String SUBPRODUCTO = "Subproducto";

    /**
     * Constant to represent the client
    */
    private static final String CLIENTE = "Cliente";

    /**
     * Constant to represent the contract
    */
    private static final String CONTRATO = "Contrato";

    /**
     * Constant to represent the account
    */
    private static final String CUENTA = "Cuenta";

    /**
     * Constant to represent the service
    */
    private static final String SERVICIO = "Servicio";

    /**
     * add product repository
     */
    private ProductoRepository productoRepository;

    /**
     * add product repository
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Tipo Documento repository
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Add client repository
     */
    private ClienteRepository clienteRepository;

    /**
     * Add Negociacion repository
     */
    private NegociacionRepository negociacionRepository;

    /**
     * Add Usuario repository
     */
    private UsuarioRepository usuarioRepository;


    /**
     * add cuenta repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * add contrato repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * add servicio repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * validation service
     */
    private ValidationService validationService;

    /*
     *
     * Métodos
     *
    */

    /**
     * Constructor of the processor class
     * @param fileName identifies the file that is being read in the batch process
     * @param productoRepository identifies the product repository
     * @param tipoProductoRepository identifies the tipoproductorepository
     * @param tipoDocumentoRepository identifies the tipodocumentorepository
     * @param clienteRepository identifies the clienterepository
     * @param negociacionRepository identifies the negociacionRepository
     * @param usuarioRepository identifies de usuarioRepository
     * @param cuentaRepository identifies the cuentaRepository
     * @param contratoRepository identifies the contratoRepository
     * @param servicioRepository identifies the servicioRepository
     * @param validationService identifies the validationservice Service
    */
    public RefinanciacionItemProcessor( String fileName,
                                        ProductoRepository productoRepository,
                                        TipoProductoRepository tipoProductoRepository,
                                        TipoDocumentoRepository tipoDocumentoRepository,
                                        ClienteRepository clienteRepository,
                                        NegociacionRepository negociacionRepository,
                                        UsuarioRepository usuarioRepository,
                                        CuentaRepository cuentaRepository,
                                        ContratoRepository contratoRepository,
                                        ServicioRepository servicioRepository,
                                        ValidationService validationService) {
        this.fileName = fileName;
        this.productoRepository = productoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.clienteRepository = clienteRepository;
        this.negociacionRepository = negociacionRepository;
        this.usuarioRepository = usuarioRepository;
        this.cuentaRepository = cuentaRepository;
        this.contratoRepository = contratoRepository;
        this.servicioRepository = servicioRepository;
        this.validationService = validationService;
    }

    /**
     * Method that process the items that are being read from the file in the batch process
     * @param refinanciaciondto to be process
     * @throws Exception in case that a mandatory field in the refinanciaciondto is null or empty
     * @return the refinanciaciondto that has been process
    */
    @Override
    public HistorialRefinanciacion process( RefinanciacionDto refinanciaciondto ) throws Exception {
        // Variables
        HistorialRefinanciacion historialRefinanciacion;
        // Code
        validationService.cleanLineErrors();
        validationService.increaseRegisterCounter();
        valdateFileFields( refinanciaciondto );                        
        if( this.validationService.isLineError() ){
            return null;
        }        
        historialRefinanciacion = processHistorialRefinanciacion( refinanciaciondto );
        if( this.validationService.isLineError() ){
            return null;
        }
        return historialRefinanciacion;
    }

    /**
     * Method that process the history 
     * @param refinanciaciondto is the list of items to be written in the database
     * @return the history object
    */
    private HistorialRefinanciacion processHistorialRefinanciacion( RefinanciacionDto refinanciaciondto ) {
        // Variables
        Servicio                servicio;
        HistorialRefinanciacion historialRefinanciacion;
        Usuario                 usuario;
        Negociacion             negociacion;
        // Código
        usuario = processUser( refinanciaciondto );
        historialRefinanciacion = new HistorialRefinanciacion();
        negociacion = processNegociation( refinanciaciondto );
        historialRefinanciacion.setNegociacion( negociacion );
        historialRefinanciacion.setValorPago( Double.parseDouble( refinanciaciondto.getValorPago() ) );
        historialRefinanciacion.setCuotaInicial( Double.parseDouble( refinanciaciondto.getCuotaInicial() ) );
        historialRefinanciacion.setDescuentoCuotaInicial( Double.parseDouble( refinanciaciondto.getDescuentoCuotaInicial() ) );
        historialRefinanciacion.setFechaAct( DateUtils.stringToLocalDate( refinanciaciondto.getFechaAct() ) );
        historialRefinanciacion.setFechaApli( DateUtils.stringToLocalDate( refinanciaciondto.getFechaApli() ) );
        historialRefinanciacion.setFechaRechazo( DateUtils.stringToLocalDate( refinanciaciondto.getFechaRechazo() ) );
        historialRefinanciacion.setMotivoRechazo( refinanciaciondto.getMotivoRechazo() );
        servicio = processService( refinanciaciondto );
        historialRefinanciacion.setServicio( servicio );
        historialRefinanciacion.setCantidadCuotasAnteriores( Integer.parseInt( refinanciaciondto.getCantidadCuotasAnteriores() ) );
        historialRefinanciacion.setCuotaMensualAnterior( Double.parseDouble( refinanciaciondto.getCuotaMensualAnterior() ) );
        historialRefinanciacion.setCantidadCuotasDespuesDeRefinanciacion( Integer.parseInt( refinanciaciondto.getCantidadCuotasDespuesRefinanciacion() ) );
        historialRefinanciacion.setCuotaMensualDespues( Double.parseDouble( refinanciaciondto.getCuotaMensualDespues() ) );
        historialRefinanciacion.setTasaAnterior( Double.parseDouble( refinanciaciondto.getTasaAnterior() ) );
        historialRefinanciacion.setTasaNueva( Double.parseDouble( refinanciaciondto.getTasaNueva() ) );
        historialRefinanciacion.setTasaCambioPlazo( Double.parseDouble( refinanciaciondto.getTasaCambioPlaza() ) );
        historialRefinanciacion.setUsuario( usuario );
        return historialRefinanciacion;
    }

    /**
     * Method that verifies the data
     * @param refinanciaciondto is the list of items to be written in the database
    */
    private void valdateFileFields( RefinanciacionDto refinanciaciondto ) {
        // Contract
        this.validationService.verifyEmpty( refinanciaciondto.getCodigoProducto(), "CodigoProducto", this.fileName );
        this.validationService.verifyEmpty( refinanciaciondto.getCodigoTipoProducto(), "CodigoTipoProducto", this.fileName );
        this.validationService.verifyEmpty( refinanciaciondto.getCodigoSubproducto(), "CodigoSubproducto", this.fileName );
        // this.validationService.validateIntegerWithZero( refinanciaciondto.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName );
        this.validationService.validateIntegerWithZero( refinanciaciondto.getNumeroIdentificacion(), "NumeroIdentificacion", this.fileName );
        this.validationService.validateIntegerWithZero( refinanciaciondto.getNumeroContrato(), "NumeroContrato", this.fileName );
        this.validationService.validateInteger( refinanciaciondto.getNumeroCuenta(), "NumeroCuenta", fileName );
        this.validationService.validateIntegerWithZero( refinanciaciondto.getCiclo(), "Ciclo", this.fileName );
        // Negociation
        this.validationService.validateIntegerWithZero( refinanciaciondto.getIdNegociacion(), "IdNegociacion", this.fileName );
        this.validationService.validateDate( refinanciaciondto.getFechaNegociacion(), "FechaNegociacion", this.fileName );
        // History
        this.validationService.validateDouble( refinanciaciondto.getValorPago(), "ValorPago", this.fileName );
        this.validationService.validateDouble( refinanciaciondto.getCuotaInicial(), "CuotaInicial", this.fileName );
        this.validationService.validateDouble( refinanciaciondto.getDescuentoCuotaInicial(), "descuentoCuotaInicial", this.fileName );
        this.validationService.validateDate( refinanciaciondto.getFechaAct(), "FechaAct", this.fileName );
        this.validationService.validateDate( refinanciaciondto.getFechaApli(), "FechaApli", this.fileName );
        this.validationService.validateDate( refinanciaciondto.getFechaRechazo(), "FechaRechazo", this.fileName );
        this.validationService.validateInteger( refinanciaciondto.getCantidadCuotasAnteriores(), "CantidadCuotasAnteriores", this.fileName );
        this.validationService.validateDouble( refinanciaciondto.getCuotaMensualAnterior(), "CuotaMensualAnterior", this.fileName );
        this.validationService.validateInteger( refinanciaciondto.getCantidadCuotasDespuesRefinanciacion(), "CantidadCuotasDespuesRefinanciacion", this.fileName );
        this.validationService.validateDouble( refinanciaciondto.getCuotaMensualDespues(), "CuotaMensualDespues", this.fileName );
        this.validationService.validateDouble( refinanciaciondto.getTasaAnterior(), "TasaAnterior", this.fileName );
        this.validationService.validateDouble( refinanciaciondto.getTasaNueva(), "TasaNueva", this.fileName );
        this.validationService.validateDouble( refinanciaciondto.getTasaCambioPlaza(), "TasaCambioPlaza", fileName );
        // Service
        this.validationService.validateIntegerWithZero( refinanciaciondto.getNumeroServicio(),"NumeroServicio",this.fileName ); 
        // User
        this.validationService.verifyEmpty( refinanciaciondto.getCodigoUsuario(),"CodigoUsuario",this.fileName );         
    }

    /**
     * Method that gets the service from the database
     * @param refinanciaciondto is an object that contains the data of the file
     * @throws Exception when there is an error during the writing process
     * @return a model entity
    */
    private Servicio processService( RefinanciacionDto refinanciaciondto ) {
        // Variables
        Contrato contrato;  
        Servicio servicio;
        // Code
        contrato = processContract( refinanciaciondto );        
        servicio = servicioRepository.getByNumero( refinanciaciondto.getNumeroServicio() );        
        if( servicio == null ) {
            servicio = new Servicio();
            servicio.setNumero( refinanciaciondto.getNumeroServicio() );
            servicio.setContrato( contrato );          
        } else {
            if( servicio.getContrato() == null ) {
                servicio.setContrato( contrato );
            }
        }
        return servicio;
    }

    /**
     * This method process the contract
     * @param refinanciaciondto is an object that contains the data of the file
     * @return Contrato
    */
    private Contrato processContract( RefinanciacionDto refinanciaciondto ) {
        // Variables
        Producto         producto;
        Producto         subproducto;
        TipoProducto     tipoProducto;
        Cliente          cliente;
        Contrato         contrato;
        TipoDocumento    tipoDocumento;
        //Cuenta           cuenta;
        //CicloFacturacion cicloFacturacion;
        // Code
        tipoProducto = tipoProductoRepository.getTipoProductoByCodigo(refinanciaciondto.getCodigoTipoProducto());
        this.validationService.validateCatalogue( tipoProducto, "TipoProducto", refinanciaciondto.getCodigoTipoProducto(), this.fileName );
        // Product
        producto = productoRepository.getByCodigo(refinanciaciondto.getCodigoProducto() );
        this.validationService.validateCatalogue( producto, PRODUCTO, refinanciaciondto.getCodigoProducto(), this.fileName );
        if( producto != null && tipoProducto != null ) {
            this.validationService.validateParent( tipoProducto, producto.getTipo(), "TipoProducto", tipoProducto.getCodigo().toString(), PRODUCTO, this.fileName );
        }
        // Subproduct
        subproducto = productoRepository.getByCodigo(refinanciaciondto.getCodigoSubproducto() );
        this.validationService.validateCatalogue( subproducto, SUBPRODUCTO, refinanciaciondto.getCodigoSubproducto(), this.fileName );
        if( subproducto != null && producto != null ) {
            this.validationService.validateChild( producto, subproducto.getProductoPadre(), SUBPRODUCTO, PRODUCTO, this.fileName );
        }   
        // Billing cycle
        /*cicloFacturacion = cicloFacturacionRepository.getCicloFactuarionById( Integer.parseInt( refinanciaciondto.getCiclo() ) );
        this.validationService.validateCatalogue( cicloFacturacion, "CicloFacturacion", refinanciaciondto.getCiclo(), this.fileName );*/                      
        // Account
        /*cuenta = cuentaRepository.getByNumero( Double.parseDouble( refinanciaciondto.getNumeroCuenta() ) );
        if( cuenta == null ) {
            cuenta = new Cuenta();
            cuenta.setNumero( Double.parseDouble( refinanciaciondto.getNumeroCuenta() ) );
            cuenta.setCicloFacturacion( cicloFacturacion );
        }*/
        // Document type
        tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(refinanciaciondto.getTipoIdentificacion() );
        this.validationService.validateCatalogue( tipoDocumento, TIPO_DOCUMENTO, refinanciaciondto.getTipoIdentificacion(), this.fileName );
        // Client
        cliente = clienteRepository.getByNumeroIdentificacion( Double.parseDouble( refinanciaciondto.getNumeroIdentificacion() ) );
        this.validationService.validateCatalogue( cliente, CLIENTE, refinanciaciondto.getNumeroIdentificacion(), this.fileName );
        /*if( cliente == null ) {
            cliente = new Cliente();
            cliente.setNumeroIdentificacion( Double.parseDouble( refinanciaciondto.getNumeroIdentificacion() ) );
            cliente.setTipoDocumento( tipoDocumento );
            cliente.setCuenta( cuenta );
        } else {
            if( cliente.getCuenta() == null ) {
                cliente.setCuenta( cuenta );
            }
        }*/
        // Contract
        contrato = contratoRepository.getByNumero( refinanciaciondto.getNumeroContrato() );
        if( contrato == null ) {
            contrato = new Contrato();
            contrato.setNumero( refinanciaciondto.getNumeroContrato() );
            contrato.setSubProducto( subproducto );
            contrato.setCliente( cliente );
        } else {  
            if( contrato.getCliente() == null ) {
                contrato.setCliente( cliente );
            }
        }
        return contrato;
    }

    /**
     * This method process the negotiation
     * @param refinanciaciondto is an object that contains the data of the file
     * @return Negociacion
    */
    private Negociacion processNegociation( RefinanciacionDto refinanciaciondto ) {
        // Variables
        Negociacion negociacion;
        // Código
        negociacion = negociacionRepository.getNegociacionById( refinanciaciondto.getIdNegociacion() );
        if( negociacion == null ) {
            negociacion = new Negociacion();
            negociacion.setId( refinanciaciondto.getIdNegociacion() );
            negociacion.setFecha( DateUtils.stringToLocalDate( refinanciaciondto.getFechaNegociacion() ) );
            negociacion.setEstado( refinanciaciondto.getEstadoNegociacion() );
        }
        return negociacion;
    }

    /**
     * This method process the user
     * @param refinanciaciondto is an object that contains the data of the file
     * @return Usuario
    */
    private Usuario processUser( RefinanciacionDto refinanciaciondto  ) {
        // Variables
        Usuario usuario;
        // Código
        usuario = usuarioRepository.getUsuarioById( refinanciaciondto.getCodigoUsuario() );
        if( usuario == null ) {
            usuario = new Usuario();
            usuario.setId( refinanciaciondto.getCodigoUsuario() );
            usuario.setNombres( refinanciaciondto.getNombreUsuario() );
            usuario.setArea( refinanciaciondto.getArea() );
        }
        return usuario;
    }
    
}