package com.colpatria.midas.processors;

/*
 *
 * Libraries
 *
*/

import com.colpatria.midas.services.ValidationService;
import org.springframework.batch.item.ItemProcessor;
import com.colpatria.midas.dto.MaestroClienteDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.model.Barrio;
import com.colpatria.midas.model.CategoriaCuenta;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Cuenta;
import com.colpatria.midas.model.Departamento;
import com.colpatria.midas.model.Estado;
import com.colpatria.midas.model.HistorialMaestroCliente;
import com.colpatria.midas.model.Lectura;
import com.colpatria.midas.model.Localidad;
import com.colpatria.midas.model.Municipio;
import com.colpatria.midas.model.Reparto;
import com.colpatria.midas.model.Servicio;
import com.colpatria.midas.model.Sucursal;
import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.repositories.BarrioRepository;
import com.colpatria.midas.repositories.CategoriaRepository;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.CuentaRepository;
import com.colpatria.midas.repositories.DepartamentoRepository;
import com.colpatria.midas.repositories.EstadoRepository;
import com.colpatria.midas.repositories.LocalidadRepository;
import com.colpatria.midas.repositories.MunicipioRepository;
import com.colpatria.midas.repositories.ServicioRepository;
import com.colpatria.midas.repositories.SucursalRepository;
import com.colpatria.midas.repositories.TipoDocumentoRepository;

/**
 * Class that is use to transform, filter and verify the items that are being read in a specific batch process
*/
public class MaestroClienteItemProcessor implements ItemProcessor<MaestroClienteDto, HistorialMaestroCliente>{


    /**
     * Constant to represent the ​​locality
    */
    private static final String LOCALIDAD = "Localidad";

    /**
     * Constant to represent the township
    */
    private static final String MUNICIPIO = "Municipio";

    /**
     * Constant to represent the client
    */
    private static final String CLIENTE = "Cliente";

    /**
     * Constant to represent the account
    */
    private static final String CUENTA = "Cuenta";

    /**
     * Branch repository to read and write to the database
    */
    private SucursalRepository sucursalRepository;

    /**
     * Service repository to read and write to the database
    */
    private ServicioRepository servicioRepository;

    /**
     * Client repository to read and write to the database
    */
    private ClienteRepository clienteRepository;

    /**
     * Banck account repository to read and write to the database
    */
    private CuentaRepository cuentaRepository;

    /**
     * Banck account repository to read and write to the database
    */
    private CategoriaRepository categoriaRepository;

    /**
     * Document type repository to read and write to the database
    */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Neighborhood repository to read and write to the database
    */
    private BarrioRepository barrioRepository;

    /**
     * ​​Department repository to read and write to the database
    */
    private DepartamentoRepository departamentoRepository;

    /**
     * Township repository to read and write to the database
    */
    private MunicipioRepository municipioRepository;

    /**
     * ​​Locality repository to read and write to the database
    */
    private LocalidadRepository localidadRepository;

    /**
     * Service state repository to read and write to the database
     */
    private EstadoRepository estadoRepository;

    /**
     * File name
    */
    private String fileName;

    /**
     * validation service
     */
    private final ValidationService validationService;

    /*
     *
     * Methods
     *
    */

    /**
     * Constructor of the processor class
     * @param fileName identifies the file that is being read in the batch process
     * @param validationService is the validation service
    */
    public MaestroClienteItemProcessor( String fileName, ValidationService validationService ) {
        this.fileName          = fileName;
        this.validationService = validationService;
    }

    /**
     * Method that process the items that are being read from the file in the batch process
     * @param item to be process     
     * @throws VerificationException in case that a mandatory field in the item is null or empty
     * @return the item that has been process
    */
    @Override
    public HistorialMaestroCliente process( MaestroClienteDto item ) throws VerificationException {
        // Variables
        HistorialMaestroCliente historialMaestroCliente;
        // Code
        validationService.cleanLineErrors();
        validationService.increaseRegisterCounter();
        valdateFileFields( item );                
        if( this.validationService.isLineError() ){
            return null;
        }        
        historialMaestroCliente = processHistorialMaestroCliente( item );
        if( this.validationService.isLineError() ){
            return null;
        }
        return historialMaestroCliente;
    }

     /**
     * Method that verifies the data
     * @param item is the list of items to be written in the database
    */
    private void valdateFileFields( MaestroClienteDto item ) {
        // Client
        this.validationService.verifyEmpty( item.getNumeroCuenta(), "número de cuenta", fileName );        
        this.validationService.validateInteger( item.getTipoIdentificacion(), "tipo de identificación", this.fileName );
        this.validationService.verifyEmpty( item.getNumeroIdentificacion(), "número de identificación", fileName );               
        this.validationService.validateInteger( item.getCodigoCategoria(), "código de la categoría", fileName );      
        this.validationService.validateInteger( item.getEstratoSocioeconomico(), "estrato socioeconómico", this.fileName );
        // Service
        this.validationService.verifyEmpty( item.getNumeroServicio(), "número de servicio", fileName );
        // History
        this.validationService.verifyEmpty( item.getEstadoServicioElectrico() , "estado del servicio electrico", fileName );
        this.validationService.verifyEmpty( item.getCodigoActividadEconomica() , "código de actividad económica", fileName );
        this.validationService.verifyEmpty( item.getDescripcionActividadEconomica() , "descripción de actividad económica", fileName );        
        this.validationService.verifyEmpty( item.getCodigoInterno() , "código interno", fileName );   
        // Delivery
        this.validationService.validateInteger( item.getCicloReparto(), "ciclo de reparto", this.fileName );
        this.validationService.verifyEmpty( item.getDireccionReparto() , "dirección de reparto", fileName );   
        this.validationService.verifyEmpty( item.getManzanaReparto() , "manzana de reparto", fileName );   
        this.validationService.validateInteger( item.getSucursalReparto(), "número de sucursal de reparto", this.fileName );
        this.validationService.verifyEmpty( item.getZonaReparto() , "zona de reparto", fileName );   
        this.validationService.verifyEmpty( item.getGrupoReparto() , "grupo de reparto", fileName );
        // Reading 
        this.validationService.validateInteger( item.getCicloLectura(), "ciclo de lectura", this.fileName );
        this.validationService.verifyEmpty( item.getManzanaLectura() , "manzana de lectura", fileName );   
        this.validationService.validateInteger( item.getSucursalLectura(), "número de sucursal de lectura", this.fileName );
        this.validationService.verifyEmpty( item.getZonaLectura() , "zona de lectura", fileName );   
        this.validationService.verifyEmpty( item.getGrupoLectura() , "grupo de lectura", fileName );
        this.validationService.verifyEmpty( item.getLocalizacionLectura() , "localización de lectura", fileName ); 
        // Neighborhood
        this.validationService.verifyEmpty( item.getCodigoDepartamentoLectura(), "código del departamento de la lectura", fileName );
        this.validationService.verifyEmpty( item.getCodigoMunicipioLectura(), "código del municipio de la lectura", fileName );
        this.validationService.verifyEmpty( item.getCodigoLocalidadLectura(), "código de la localidad de la lectura", fileName );
        this.validationService.verifyEmpty( item.getCodigoBarrioLectura(), "código del barrio de la lectura", fileName );
    }

    /**
     * Method that writes the history to the database
     * @param item is the list of items to be written in the database
     * @return the history object
    */
    private HistorialMaestroCliente processHistorialMaestroCliente( MaestroClienteDto item ) {
        // Variables
        HistorialMaestroCliente historialMaestroCliente;
        Servicio                servicio;
        Lectura                 lectura;
        Reparto                 reparto;
        Cliente                 cliente;
        // Código
        historialMaestroCliente = new HistorialMaestroCliente();
        servicio = processService( item );
        lectura  = processReading( item );
        reparto  = processDelivery( item );
        cliente  = processClient( item );
        historialMaestroCliente.setCliente( cliente );
        historialMaestroCliente.setServicio( servicio );
        historialMaestroCliente.setLectura( lectura );
        historialMaestroCliente.setReparto( reparto );
        historialMaestroCliente.setEstadoServicioElectrico( item.getEstadoServicioElectrico() );
        historialMaestroCliente.setCodigoActividadEconomica( item.getCodigoActividadEconomica() );
        historialMaestroCliente.setDescripcionActividadEconomica( item.getDescripcionActividadEconomica() );
        historialMaestroCliente.setCodigoInterno( item.getCodigoInterno() );
        return historialMaestroCliente;
    }

    /**
     * This method processes the service
     * @param item is an object that contains the data of the file
     * @return Cliente
    */
    private Cliente processClient( MaestroClienteDto item ) {
        // Variables
        Cliente         cliente;
        TipoDocumento   tipoDocumento;
        Cuenta          cuenta;
        CategoriaCuenta categoria;
        // Código
        categoria = categoriaRepository.getCategoriaById(item.getCodigoCategoria() );
        this.validationService.validateCatalogue( categoria, "Categoría", item.getCodigoCategoria(), this.fileName );
        // Account
        cuenta = cuentaRepository.getByNumero( Double.parseDouble( item.getNumeroCuenta() ) );
        if( cuenta == null ) {
            cuenta = new Cuenta();
            cuenta.setNumero( Double.parseDouble( item.getNumeroCuenta() ) );
            cuenta.setCategoria( categoria );
        } else {                  
            if( categoria != null ) {          
                if( cuenta.getCategoria() == null ) {
                    cuenta.setCategoria( categoria );
                }
            }      
        } 
        cuenta.setDireccion( item.getDireccionPredio() );
        cuenta.setEstrato( Integer.parseInt( item.getEstratoSocioeconomico() ) );
        // Document type
        tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo( item.getTipoIdentificacion() );
        this.validationService.validateCatalogue( tipoDocumento, "Tipo Documento", item.getTipoIdentificacion(), this.fileName );
        // Client
        cliente = clienteRepository.getByNumeroIdentificacion( Double.parseDouble( item.getNumeroIdentificacion() ) );
        if( cliente == null ) {
            cliente = new Cliente();
            cliente.setNumeroIdentificacion( Double.parseDouble( item.getNumeroIdentificacion() ) );
            cliente.setTipoDocumento( tipoDocumento );
            cliente.setCuenta( cuenta );
        } else {
            if( tipoDocumento != null ) {
                if( cliente.getTipoDocumento() == null ) {
                    cliente.setTipoDocumento( tipoDocumento );
                }
            }
            if( cliente.getCuenta() == null ) {
                cliente.setCuenta( cuenta );
            }
        }        
        cliente.setNombre( item.getNombreCliente() + "  " + item.getApellidosCliente() );
        return cliente;
    }

    /**
     * This method processes the service
     * @param item is an object that contains the data of the file
     * @return Servicio
    */
    private Servicio processService( MaestroClienteDto item ) {
        // Variables
        Servicio servicio;
        Estado   estado;
        // Código
        estado = estadoRepository.getEstadoById( item.getEstadoServicio() );
        this.validationService.validateCatalogue(estado, "Estado de servicio", item.getEstadoServicio(), this.fileName);
        // Service
        servicio = servicioRepository.getByNumero( item.getNumeroServicio() );
        if( servicio == null ) {
            servicio = new Servicio();            
            servicio.setNumero( item.getNumeroServicio() );
        }        
        servicio.setEstado( estado );
        return servicio;
    }

    /**
     * This method processes the delivery
     * @param item is an object that contains the data of the file
     * @return Reparto
    */
    private Reparto processDelivery( MaestroClienteDto item ) {
        // Variables
        Reparto  reparto;
        Sucursal sucursal;
        // Código
        sucursal = sucursalRepository.getSucursalByNumero( Integer.parseInt( item.getSucursalReparto() ) );
        this.validationService.validateCatalogue( sucursal, "Sucursal", item.getSucursalReparto(), this.fileName );
        // Delivery
        reparto = new Reparto();
        reparto.setDireccion( item.getDireccionReparto() );
        reparto.setManzana( item.getManzanaReparto() );
        reparto.setSucursal( sucursal );
        reparto.setZona( item.getZonaReparto() );
        reparto.setCiclo( Integer.parseInt( item.getCicloReparto() ) );
        reparto.setGrupo( item.getGrupoLectura() );
        return reparto;
    }

    /**
     * This method processes the reading
     * @param item is an object that contains the data of the file
     * @return Lectura
    */
    private Lectura processReading( MaestroClienteDto item ) {
        // Variables
        Lectura  lectura;
        Sucursal sucursal;
        Barrio   barrio;
        // Código
        sucursal = sucursalRepository.getSucursalByNumero( Integer.parseInt( item.getSucursalLectura() ) );
        this.validationService.validateCatalogue( sucursal, "Sucursal", item.getSucursalLectura(), this.fileName );
        // Neighborhood
        barrio = processNeighborhood( item );
        // Reading
        lectura = new Lectura();
        lectura.setManzana( item.getManzanaLectura() );
        lectura.setSucursal( sucursal );
        lectura.setZona( item.getZonaLectura() );
        lectura.setCiclo( Integer.parseInt( item.getCicloLectura() ) );
        lectura.setGrupo( item.getGrupoLectura() );
        lectura.setBarrio( barrio );
        lectura.setLocalizacion( item.getLocalizacionLectura() );
        return lectura;
    }

    /**
     * This method processes the neighborhood
     * @param item is an object that contains the data of the file
     * @return Barrio
    */
    private Barrio processNeighborhood( MaestroClienteDto item ) {
        // Variables
        Barrio       barrio;
        Departamento departamento;
        Municipio    municipio;
        Localidad    localidad;
        // Código
        departamento = departamentoRepository.getByCodigo( item.getCodigoDepartamentoLectura() );
        this.validationService.validateCatalogue( departamento, "Departamento", item.getCodigoDepartamentoLectura(), this.fileName );
        // Township
        municipio = municipioRepository.getByCodigo( Long.parseLong(item.getCodigoMunicipioLectura()));
        this.validationService.validateCatalogue( municipio, MUNICIPIO, item.getCodigoMunicipioLectura(), this.fileName );
        if( municipio != null && departamento != null ) {
            this.validationService.validateParent( departamento, municipio.getDepartamento(), "Departamento", departamento.getCodigo(), MUNICIPIO, this.fileName );
        }
        // Locality
        localidad = localidadRepository.getByCodigo( item.getCodigoLocalidadLectura() );
        this.validationService.validateCatalogue( localidad, LOCALIDAD, item.getCodigoLocalidadLectura(), this.fileName );
        if( localidad != null && municipio != null ) {
            //this.validationService.validateParent( municipio, localidad.getMunicipio(), MUNICIPIO, municipio.getCodigo(), LOCALIDAD, this.fileName );
        }
        // Neighborhood
        barrio = barrioRepository.getByCodigo( item.getCodigoBarrioLectura() );
        if( barrio == null ) {
            barrio = new Barrio();
            barrio.setCodigo( item.getCodigoBarrioLectura() );
            barrio.setLocalidad( localidad );
        } else {
            if( localidad != null ) {
                if( barrio.getLocalidad() == null ) {
                    barrio.setLocalidad( localidad );
                }
            } 
        }        
        barrio.setNombre( item.getNombreBarrioLectura() );
        return barrio;
    }

    /**
     * Set the repository service history
     * @param servicioRepository is the repository object
    */
    public void setServicioRepository( ServicioRepository servicioRepository ) {
        this.servicioRepository = servicioRepository;
    }

    /**
     * Set the branch repository
     * @param sucursalRepository is the repository object
    */
    public void setSucursalRepository(SucursalRepository sucursalRepository) {
        this.sucursalRepository = sucursalRepository;
    }

    /**
     * Set the client repository
     * @param clienteRepository is the repository object
    */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Set the account repository
     * @param cuentaRepository is the repository object
    */
    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Set the category repository
     * @param categoriaRepository is the repository object
    */
    public void setCategoriaRepository(CategoriaRepository categoriaRepository) {
        this.categoriaRepository = categoriaRepository;
    }

    /**
     * Set the document type repository
     * @param tipoDocumentoRepository is the repository object
    */
    public void setTipoDocumentoRepository(TipoDocumentoRepository tipoDocumentoRepository) {
        this.tipoDocumentoRepository = tipoDocumentoRepository;
    }

    /**
     * Set the neighborhood repository
     * @param barrioRepository is the repository object
    */
    public void setBarrioRepository(BarrioRepository barrioRepository) {
        this.barrioRepository = barrioRepository;
    }

    /**
     * Set the department repository
     * @param departamentoRepository is the repository object
    */
    public void setDepartamentoRepository(DepartamentoRepository departamentoRepository) {
        this.departamentoRepository = departamentoRepository;
    }

    /**
     * Set the township repository
     * @param municipioRepository is the repository object
    */
    public void setMunicipioRepository(MunicipioRepository municipioRepository) {
        this.municipioRepository = municipioRepository;
    }

    /**
     * Set the locality repository
     * @param localidadRepository is the repository object
    */
    public void setLocalidadRepository(LocalidadRepository localidadRepository) {
        this.localidadRepository = localidadRepository;
    }    

    /**
     * Set the service state repository
     * @param estadoRepository is the repository object
    */
    public void setEstadoRepository(EstadoRepository estadoRepository) {
        this.estadoRepository = estadoRepository;
    }
    
}
