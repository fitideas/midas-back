package com.colpatria.midas.processors;

import com.colpatria.midas.batch.DeudaCargoConfig;
import com.colpatria.midas.constants.CacheConstants;
import com.colpatria.midas.dto.DeudaCargoDto;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;

import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import org.springframework.cache.CacheManager;

import java.math.BigInteger;
import java.time.LocalDate;

public class DeudaCargoItemProcessor implements ItemProcessor<DeudaCargoDto, DeudaCargo> {

    /**
     * Add logger to deudas config
     */
    private static final Logger logger = LoggerFactory.getLogger(DeudaCargoItemProcessor.class);


    /**
     * Validation service to use
     */
    private final ValidationService validationService;

    /**
     * add 'sucursal' repository
     */
    private final SucursalRepository sucursalRepository;

    /**
     * add 'producto' repository
     */
    private final ProductoRepository productoRepository;

    /**
     * add cargo repository
     */
    private final CargoRepository cargoRepository;

    /**
     * file name to process
     */
    private String fileName;

    /**
     * Tipo Documento repository
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Tipo producto repository
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Cliente repository
     */
    private ClienteRepository clienteRepository;

    /**
     * cuenta repository
     */
    private CuentaRepository cuentaRepository;

    /**
     * contrato repository
     */
    private ContratoRepository contratoRepository;

    /**
     * add servicio repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * cache manager in application
     */
    private final CacheManager cacheManager;

    /**
     * Constructor
     *
     * @param validationService       DI of validation service
     * @param sucursalRepository      DI sucursal repository
     * @param productoRepository      DI producto repository
     * @param cargoRepository         DI cargo repository
     * @param fileName                DI of file name to process
     * @param tipoDocumentoRepository DI of tipo documento repository
     * @param tipoProductoRepository  DI of tipo producto
     * @param clienteRepository       DI of client repository
     * @param cuentaRepository        DI of cuenta repository
     * @param contratoRepository      DI contrato repository
     * @param servicioRepository      DI of servicio repository
     * @param cacheManager            DI of cache manager
     */
    public DeudaCargoItemProcessor(ValidationService validationService, SucursalRepository sucursalRepository, ProductoRepository productoRepository, CargoRepository cargoRepository, String fileName, TipoDocumentoRepository tipoDocumentoRepository, TipoProductoRepository tipoProductoRepository, ClienteRepository clienteRepository, CuentaRepository cuentaRepository, ContratoRepository contratoRepository, ServicioRepository servicioRepository, CacheManager cacheManager) {
        this.validationService = validationService;
        this.sucursalRepository = sucursalRepository;
        this.productoRepository = productoRepository;
        this.cargoRepository = cargoRepository;
        this.fileName = fileName;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.clienteRepository = clienteRepository;
        this.cuentaRepository = cuentaRepository;
        this.contratoRepository = contratoRepository;
        this.servicioRepository = servicioRepository;
        this.cacheManager = cacheManager;
    }

    /**
     * process items
     *
     * @param item item to process
     * @return object transformated
     * @throws Exception in case of fail
     */
    @Override
    public DeudaCargo process(DeudaCargoDto item) throws Exception {

        this.validationService.increaseRegisterCounter();
        this.validationService.cleanLineErrors();

        if (ValidateStructure(item)) {
            return null;
        }

        Producto subproducto = productoRepository.getByCodigo(item.getCodigoSubProducto());
        Sucursal sucursal = sucursalRepository.getSucursalByNumero(Integer.parseInt(item.getSucursal()));
        Producto producto = this.productoRepository.getProductoByCodigo(item.getCodigoProducto());
        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(item.getCodigoTipoProducto());
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(item.getTipoIdentificacion());
        Cargo cargo = this.cargoRepository.getCargoById(item.getCodCargo());

        this.validationService.verifyCatalog(subproducto, "Subproducto", this.fileName);
        this.validationService.verifyCatalog(producto, "Producto", this.fileName);
        this.validationService.verifyCatalog(sucursal, "Sucursal", this.fileName);
        this.validationService.verifyCatalog(tipoProducto, "tipo producto", this.fileName);
        this.validationService.verifyCatalog(tipoDocumento, "tipo documento", this.fileName);
        this.validationService.verifyCatalog(cargo, "cargo", this.fileName);

        String codigo = subproducto.getProductoPadre() != null ? subproducto.getProductoPadre().getCodigo() : "";
        this.validationService.verifyParent(producto.getCodigo(),
                codigo,
                "Producto",
                "SubProducto",
                this.fileName);

        this.validationService.verifyParent(
                producto.getTipo().getCodigo(),
                tipoProducto.getCodigo(),
                "Producto",
                "Tipo producto",
                this.fileName);

        if (this.validationService.isLineError()) {
            return null;
        }

        Cuenta cuenta = this.cuentaRepository.getByNumero(Double.parseDouble(item.getNroCuenta()));
        if (cuenta == null) {
            cuenta = new Cuenta();
            cuenta.setNumero(Double.parseDouble(item.getNroCuenta()));
            cuenta.setSucursal(sucursal);
        }

        Cliente cliente = this.clienteRepository.getByNumeroIdentificacion(Double.parseDouble(item.getNroIdentificacion()));
        if (cliente == null) {
            cliente = new Cliente();
            cliente.setTipoDocumento(tipoDocumento);
            cliente.setNumeroIdentificacion(Double.parseDouble(item.getNroIdentificacion()));
            cliente.setCuenta(cuenta);
        }

        cliente = cuenta.getCliente() == null ? cliente : cuenta.getCliente();

        String contratoNumero = item.getNroContrato();
        Contrato contrato = this.contratoRepository.getByNumero(contratoNumero);
        if (contrato == null) {
            contrato = new Contrato();
            contrato.setNumero(contratoNumero);
            contrato.setCliente(cliente);
            contrato.setSubProducto(subproducto);
        }

        contrato = cliente.getContrato() == null ? contrato : cliente.getContrato();

        cliente.setContrato(contrato);
        cuenta.setCliente(cliente);

        Servicio servicio = this.servicioRepository.getByNumero(item.getNroServicio());
        if (servicio == null) {
            servicio = new Servicio();
            servicio.setNumero(item.getNroServicio());
            servicio.setContrato(contrato);
        }

        cacheManager.getCache(CacheConstants.cliente).put(item.getNroIdentificacion(), cliente);
        cacheManager.getCache(CacheConstants.cuenta).put(item.getNroCuenta(), cuenta);
        cacheManager.getCache(CacheConstants.contrato).put(item.getNroContrato(), contrato);
        cacheManager.getCache(CacheConstants.service).put(item.getNroServicio(), servicio);

        DeudaCargo deudaCargo = new DeudaCargo();
        deudaCargo.setCuenta(cuenta);
        deudaCargo.setServicio(servicio);
        deudaCargo.setSubProducto(subproducto);
        deudaCargo.setSucursal(sucursal);
        deudaCargo.setCargo(cargo);
        deudaCargo.setNroDocumentoFacturacion(item.getNroDocumentoFacturacion());
        deudaCargo.setFechaDocumento(LocalDate.parse(item.getFechaDocumento()));
        deudaCargo.setFechaPrimerVencimiento(LocalDate.parse(item.getFechaPrimerVencimiento()));
        deudaCargo.setFechaSegundoVencimiento(LocalDate.parse(item.getFechaSegundoVencimiento()));
        deudaCargo.setFechaReporte(FileUtils.extraerFecha(this.fileName));
        deudaCargo.setSaldo(Double.parseDouble(item.getSaldo()));
        deudaCargo.setAntiguedad(item.getAntiguedad() == null ? null : Integer.parseInt(item.getAntiguedad()));
        deudaCargo.setDescripcion(item.getDescripcion());

        return deudaCargo;
    }

    /**
     * Validate data types
     *
     * @param item to process
     * @return result of process
     */
    private Boolean ValidateStructure(DeudaCargoDto item) {

        this.validationService.verifyEmpty(item.getNroIdentificacion(), "Número de documento", fileName);
        this.validationService.verifyEmpty(item.getNroContrato(), "Número de contrato", fileName);
        this.validationService.verifyEmpty(item.getFechaPrimerVencimiento(), "Primer vencimiento", fileName);
        this.validationService.verifyEmpty(item.getFechaSegundoVencimiento(), "Segundo vencimiento", fileName);
        this.validationService.verifyEmpty(item.getNroServicio(), "Servicio", fileName);
        this.validationService.verifyEmpty(item.getNroCuenta(), "Cuenta", fileName);
        this.validationService.verifyEmpty(item.getCodigoProducto(), "Codigo de producto", fileName);
        this.validationService.verifyEmpty(item.getCodigoSubProducto(), "Codigo de subproducto", fileName);
        this.validationService.verifyEmpty(item.getCodigoTipoProducto(), "Codigo tipo de producto", fileName);
        this.validationService.verifyEmpty(item.getTipoIdentificacion(), "Tipo identificacion", fileName);
        this.validationService.verifyEmpty(item.getSucursal(), "Sucursal", fileName);
        this.validationService.verifyEmpty(item.getCodCargo(), "Codigo de cargo", fileName);

        if (this.validationService.isLineError())
            return this.validationService.isLineError();

        this.validationService.verifyDate(item.getFechaDocumento(), "Fecha documento", this.fileName);
        this.validationService.verifyDate(item.getFechaPrimerVencimiento(), "Fecha de primer vencimiento", this.fileName);
        this.validationService.verifyDate(item.getFechaSegundoVencimiento(), "Fecha de segundo vencimiento", this.fileName);
        this.validationService.verifyNumber(item.getSaldo(), "Saldo", this.fileName);
        this.validationService.verifyNumber(item.getCodigoProducto(), "Codigo producto", this.fileName);
        this.validationService.verifyNumber(item.getCodigoSubProducto(), "Codigo subproducto", this.fileName);
        this.validationService.verifyNumber(item.getCodigoTipoProducto(), "Codigo tipo de producto", this.fileName);
        this.validationService.verifyNumber(item.getSucursal(), "sucursal", this.fileName);
        this.validationService.verifyNumber(item.getTipoIdentificacion(), "Tipo identificacion", this.fileName);
        this.validationService.verifyNumber(item.getNroIdentificacion(), "Número de documento", this.fileName);
        this.validationService.verifyNumber(item.getNroContrato(), "Contrato", this.fileName);
        this.validationService.verifyNumber(item.getNroServicio(), "Servicio", this.fileName);

        if (this.validationService.isLineError())
            return this.validationService.isLineError();

        this.validationService.verifyZero(item.getNroContrato(), "Contrato", this.fileName);
        this.validationService.verifyZero(item.getNroIdentificacion(), "Número de documento", this.fileName);
        this.validationService.verifyZero(item.getNroServicio(), "Servicio", this.fileName);


        return this.validationService.isLineError();
    }


}
