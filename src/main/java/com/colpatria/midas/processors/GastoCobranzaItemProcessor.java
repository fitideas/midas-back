package com.colpatria.midas.processors;

import java.math.BigInteger;
import java.time.LocalDate;
import com.colpatria.midas.dto.GastoCobranzaDto;
import com.colpatria.midas.model.Attributes;
import com.colpatria.midas.model.GastoCobranza;
import com.colpatria.midas.repositories.AttributesRepository;
import com.colpatria.midas.repositories.GastoCobranzaRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

/**
 * Class that is use to transform, filter and verify the items that are being read in a specific batch process
 */
@Component
public class GastoCobranzaItemProcessor implements ItemProcessor<GastoCobranza, GastoCobranzaDto> {

    /**
	 * add 'attributes' repository
	 */
    private final AttributesRepository attributesRepository;

    /**
	 * add 'gasto cobranza' repository
	 */
    private final GastoCobranzaRepository gastoCobranzaRepository;

    /**
	 * Attrubute 'diasMoraLimiteInferior' to determine causals
	 */
    private final Integer diasMoraLimiteInferior;

    /**
	 * Attrubute 'diasMoraLimiteSuperior' to determine causals
	 */
    private final Integer diasMoraLimiteSuperior;

    /**
	 * Attrubute 'saldoCartera' to determine causals
	 */
    private final Double saldoCartera;

    /**
	 * Attrubute 'producto' to determine causals
	 */
    private final String producto;

    /**
	 * Attrubute 'producto' to determine causals
	 */
    private final String subproducto;

    /**
	 * Attrubute 'facturacion' to determine causals
	 */
    private final boolean facturacion;

    /**
	 * Attrubute 'consecutivo' to determine causals
	 */
    private BigInteger consecutivo;

    /**
     * Constructor of the processor class
     * @param attributesRepository dependency injection of attributesRepository
     * @param gastoCobranzaRepository dependency injection of gastoCobranzaRepository
     */
    public GastoCobranzaItemProcessor(AttributesRepository attributesRepository, GastoCobranzaRepository gastoCobranzaRepository){
        this.attributesRepository = attributesRepository;
        this.gastoCobranzaRepository = gastoCobranzaRepository;
        this.diasMoraLimiteInferior = Integer.parseInt(attributesRepository.getAttributesValueByKey("DIAS_MORA_LIMITE_INFERIOR_GASTO_COBRANZA"));
        this.diasMoraLimiteSuperior = Integer.parseInt(attributesRepository.getAttributesValueByKey("DIAS_MORA_LIMITE_SUPERIOR_GASTO_COBRANZA"));
        this.saldoCartera = Double.parseDouble(attributesRepository.getAttributesValueByKey("SALDO_CARTERA_GASTO_COBRANZA"));
        this.producto = attributesRepository.getAttributesValueByKey("PRODUCTO_GASTO_COBRANZA");
        this.subproducto = attributesRepository.getAttributesValueByKey("SUBPRODUCTO_GASTO_COBRANZA");
        this.facturacion = attributesRepository.getAttributesValueByKey("FACTURACION_GASTO_COBRANZA").equals("SI");
        this.consecutivo = new BigInteger(attributesRepository.getAttributesValueByKey("GASTO_COBRANZA_CONSECUTIVO"));
    }

    /**
     * Constructor of the processor class
     * @param item to be processed
     */
    @Override
    public GastoCobranzaDto process(GastoCobranza item) throws Exception {
        GastoCobranzaDto dto = new GastoCobranzaDto();
        dto.setCedula(String.format("%1$,.0f", item.getHistorialCarteraResumen().getContrato().getCliente().getNumeroIdentificacion()));
        dto.setCuentaNumero(String.format("%1$.0f", item.getHistorialCarteraResumen().getContrato().getCliente().getCuenta().getNumero()));
        dto.setSubproducto(item.getHistorialCarteraResumen().getContrato().getSubProducto().getNombre());
        dto.setCiclo(item.getHistorialCarteraResumen().getContrato().getCliente().getCuenta().getCicloFacturacion().toString());
        dto.setDiasMora(item.getHistorialCarteraResumen().getDiasAtrasoMaximo().toString());
        dto.setCapital(String.format("%1$.0f", item.getHistorialCarteraResumen().getSumaCapitalImpago()));
        dto.setCapitalSinFacturar(String.format("%1$.0f", item.getHistorialCarteraResumen().getSumaInteresFacturadoNoAfecto()));
        dto.setIntereses(String.format("%1$.0f", item.getHistorialCarteraResumen().getSumaInteresImpago() + item.getHistorialCarteraResumen().getSumaInteresOrdenCorrienteImpago() + item.getHistorialCarteraResumen().getSumaInteresFacturadoNoAfecto()));
        dto.setInteresMora(String.format("%1$.0f", item.getHistorialCarteraResumen().getSumaInteresMora() + item.getHistorialCarteraResumen().getSumaInteresOrdenMoraImpago()));
        dto.setSaldo(String.format("%1$.0f", item.getHistorialCarteraResumen().getSumaSaldoCapitalPorFacturar()));
        dto.setSaldoNoFacturado(String.format("%1$.0f", item.getHistorialCarteraResumen().getSumaInteresNoAfectoPorFacturar()));
        dto.setCuotaManejo("");
        dto.setGastoCobranza("");
        dto.setFechaReporte(item.getHistorialCarteraResumen().getFechaReporte().toString());
        dto.setEdad("");
        dto.setFechaGestion("");
        dto.setUserGestion("");
        dto.setObservacionesGestion("");
        dto.setEfectividadGestion("");
        dto.setFechaEnvioCarta("");
        dto.setFechaLiquidacionGasto(LocalDate.now().toString());
        dto.setConsecutivo(consecutivo.toString());
        attributesRepository.save(new Attributes("GASTO_COBRANZA_CONSECUTIVO", consecutivo.add(BigInteger.ONE).toString()));
        consecutivo = consecutivo.add(BigInteger.ONE);
        dto.setFechaConsecutivo(LocalDate.now().toString());
        dto.setServicioAsignado("");
        item = setCausales(item);
        dto.setCausal(item.getCausal());
        if(dto.getCausal()==null || dto.getCausal().isEmpty()){
            dto.setFileToBeWriten("gastos_cobranza_afectos_");
        }else{
            dto.setFileToBeWriten("gastos_cobranza_eliminados_");
        }
        return dto;
    }

    /**
     * Constructor of the processor class
     * @param entity to be validated for the generation of causals
     */
    public GastoCobranza setCausales(GastoCobranza entity){
        StringBuilder causales = new StringBuilder();
        if(entity.getHistorialCarteraResumen().getDiasAtrasoMaximo()<=diasMoraLimiteInferior && entity.getHistorialCarteraResumen().getDiasAtrasoMaximo()>diasMoraLimiteSuperior){
            causales.append("Dias Mora, ");
        }
        if(entity.getHistorialCarteraResumen().getSumaCapitalImpago()<saldoCartera){
            causales.append("Saldo Cartera, ");
        }
        if(entity.getHistorialCarteraResumen().getContrato().getSubProducto().getNombre().equals(subproducto) && entity.getHistorialCarteraResumen().getContrato().getSubProducto().getProductoPadre().getNombre().equals(producto)){
            causales.append("Producto y Subprodcuto, ");
        }
        if(entity.getHistorialCarteraResumen().getContrato().getFacturacion().equals(facturacion)){
            causales.append("Marca Facturacion, ");
        }
        if(entity.getHistorialCarteraResumen().getContrato().getFacturacion().equals(facturacion)){
            causales.append("Marca Facturacion, ");
        }
        if(causales.toString().isEmpty()){
            entity.setCausal(causales.toString());
        }else{
            entity.setCausal(causales.toString().substring(0, causales.toString().length() - 2));  
        }

        gastoCobranzaRepository.save(entity);

        return entity;
    }
    
}
