package com.colpatria.midas.processors;

import com.colpatria.midas.dto.RecaudoDto;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.DateUtils;
import com.colpatria.midas.utils.FileUtils;
import com.colpatria.midas.utils.NumberUtils;
import org.springframework.batch.item.ItemProcessor;

/**
 * Recaudo Item Processor
 * Converts objects from RecaudoDto to RecaudoProcessed
 */
public class RecaudoItemProcessor implements ItemProcessor<RecaudoDto, HistorialRecaudo> {

    /**
     * File
     */
    private final String file;

    /**
     * Producto Repository
     */
    private ProductoRepository productoRepository;

    /**
     * Tipo Producto Repository
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Tipo Documento Repository
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Tipo Consumo Repository
     */
    private TipoConsumoRepository tipoConsumoRepository;

    /**
     * Clase Servicio Repository
     */
    private ClaseServicioRepository claseServicioRepository;

    /**
     * Sucursal Repository
     */
    private SucursalRepository sucursalRepository;

    /**
     * Categoria Repository
     */
    private CategoriaRepository categoriaRepository;

    /**
     * Cargo Repository
     */
    private CargoRepository cargoRepository;

    /**
     * Forma Pago Repository
     */
    private FormaPagoRepository formaPagoRepository;

    /**
     * Municipio Repository
     */
    private MunicipioRepository municipioRepository;

    /**
     * Cliente Repository
     */
    private ClienteRepository clienteRepository;

    /**
     * Contrato Repository
     */
    private ContratoRepository contratoRepository;

    /**
     * Cuenta Repository
     */
    private CuentaRepository cuentaRepository;

    /**
     * Servicio Repository
     */
    private ServicioRepository servicioRepository;

    /**
     * Validation Service
     */
    private ValidationService validationService;

    /**
     * Constructor
     *
     * @param file File name of the process
     */
    public RecaudoItemProcessor(String file) {
        this.file = file;
    }

    /**
     * Process an item
     *
     * @param recaudo item to process
     * @return item processed
     */
    @Override
    public HistorialRecaudo process(RecaudoDto recaudo) throws Exception {

        // Initial Cleaning
        this.validationService.cleanLineErrors();
        this.validationService.increaseRegisterCounter();

        // CODIGO PRODUCTO
        this.validationService.verifyEmpty(recaudo.getCodigoProducto(), "Código de producto", this.file);
        Producto producto = this.productoRepository.getProductoByCodigo(recaudo.getCodigoProducto());
        if(!recaudo.getCodigoProducto().equals(""))
            this.validationService.verifyCatalog(producto, "Código de producto", this.file);

        // NOMBRE PRODUCTO
        // sin validaciones

        // CODIGO TIPO PRODUCTO
        this.validationService.verifyEmpty(recaudo.getCodigoTipoProducto(), "Código de tipo de producto", this.file);
        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(recaudo.getCodigoTipoProducto());
        if(!recaudo.getCodigoTipoProducto().equals(""))
            this.validationService.verifyCatalog(tipoProducto, "Código de tipo de producto", this.file);

        // NOMBRE TIPO PRODUCTO
        // sin validaciones

        // CODIGO SUBPRODUCTO
        this.validationService.verifyEmpty(recaudo.getCodigoSubproducto(), "Código de subproducto", this.file);
        Producto subproducto = this.productoRepository.getProductoByCodigo(recaudo.getCodigoSubproducto());
        if(!recaudo.getCodigoSubproducto().equals(""))
            this.validationService.verifyCatalog(subproducto, "Código de subproducto", this.file);
        if (subproducto != null && producto != null) {
            this.validationService.validateChild(producto, subproducto.getProductoPadre(), "subproducto", subproducto.getCodigo(), this.file);
        }

        // NOMBRE SUBPRODUCTO
        // sin validaciones

        // TIPO DOCUMENTO IDENTIDAD
        this.validationService.verifyEmpty(recaudo.getTipoDocumentoIdentidad(), "Tipo de documento de identidad", this.file);
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(recaudo.getTipoDocumentoIdentidad());
        if(!recaudo.getTipoDocumentoIdentidad().equals(""))
            this.validationService.verifyCatalog(tipoDocumento, "Tipo de documento de identidad", this.file);

        // NUMERO DOCUMENTO TITULAR
        this.validationService.validateIntegerWithZero(recaudo.getNroDocumentoTitular(), "Número de documento de titular", this.file);

        // NUMERO CONTRATO
        this.validationService.validateIntegerWithZero(recaudo.getNumeroContrato(), "Número de contrato", this.file);

        // NUMERO CUENTA
        this.validationService.validateIntegerWithZero(recaudo.getNroCuenta(), "Número de cuenta", this.file);

        // SUCURSAL
        this.validationService.verifyEmpty(recaudo.getSucursal(), "Sucursal", this.file);
        int sucursalNumero = NumberUtils.isInteger(recaudo.getSucursal()) ? Integer.parseInt(recaudo.getSucursal()) : -1;
        Sucursal sucursal = this.sucursalRepository.getSucursalByNumero(sucursalNumero);
        if (sucursalNumero != -1 && !recaudo.getSucursal().equals("")) {
            this.validationService.verifyCatalog(sucursal, "Sucursal", this.file);
        }

        // CICLO
        this.validationService.validateIntegerWithZero(recaudo.getCiclo(), "Ciclo", this.file);

        //MUNICIPIO
        this.validationService.validateIntegerWithZero(recaudo.getMunicipio(), "Municipio", this.file);
        long municipioCodigo = NumberUtils.isInteger(recaudo.getMunicipio()) ? Long.parseLong(recaudo.getMunicipio()) : -1L;
        Municipio municipio = this.municipioRepository.getByCodigo(municipioCodigo);
        if(municipioCodigo != -1L && !recaudo.getMunicipio().equals("") && !recaudo.getMunicipio().equals("0")) {
            this.validationService.verifyCatalog(municipio, "Municipio", this.file);
        }

        // CATEGORIA
        this.validationService.verifyEmpty(recaudo.getCategoria(), "Categoría de cuenta", this.file);
        CategoriaCuenta categoriaCuenta = this.categoriaRepository.getCategoriaById(recaudo.getCategoria());
        if(!recaudo.getCategoria().equals(""))
            this.validationService.verifyCatalog(categoriaCuenta, "Categoría de cuenta", this.file);

        // ESTRATO
        this.validationService.verifyNumber(recaudo.getEstrato(), "Estrato", this.file);

        // TIPO PAGO
        // sin validaciones

        // VALOR PAGADO
        this.validationService.verifyNumber(recaudo.getValorPagado(), "Valor pagado", this.file);

        //MONTO
        this.validationService.verifyNumber(recaudo.getMonto(), "Monto", this.file);

        // CODIGO CARGO
        this.validationService.verifyEmpty(recaudo.getCodCargo(), "Código de cargo", this.file);
        Cargo cargo = this.cargoRepository.getCargoById(recaudo.getCodCargo());
        if(!recaudo.getCodCargo().equals(""))
            this.validationService.verifyCatalog(cargo, "Código de cargo", this.file);

        // NUMERO SERVICIO
        this.validationService.validateIntegerWithZero(recaudo.getNroServicio(), "Número de servicio", this.file);

        // DESCRIPCIÓN
        // sin validaciones

        //FECHA PAGO
        this.validationService.validateDate(recaudo.getFechaPago(), "Fecha de pago", this.file);

        //FECHA PROCESO PAGO
        this.validationService.validateDate(recaudo.getFechaProcesoPago(), "Fecha de proceso de pago", this.file);

        // FORMA PAGO
        this.validationService.verifyEmpty(recaudo.getFormaPago(), "Forma de pago", this.file);
        FormaPago formaPago = this.formaPagoRepository.getFormaPagoById(recaudo.getFormaPago());
        if(!recaudo.getFormaPago().equals(""))
            this.validationService.verifyCatalog(formaPago, "Forma de pago", this.file);

        // CODIGO ENTIDAD RECAUDADORA
        this.validationService.validateIntegerWithZero(recaudo.getCodEntidadRecaudadora(), "Código de entidad recaudadora", this.file);

        // SUCURSAL RECAUDO
        this.validationService.validateIntegerWithZero(recaudo.getSucursalRecaudo(), "Sucursal de recaudo", this.file);

        // FECHA FACTURACION
        this.validationService.validateDate(recaudo.getFechaFacturacion(), "Fecha de facturación", this.file);

        // TIPO CONSUMO
        this.validationService.validateInteger(recaudo.getTipoConsumo(), "Tipo de consumo", this.file);

        // FECHA DOCUMENTO
        this.validationService.validateDate(recaudo.getFechaDocumento(), "Fecha de documento", this.file);

        // NUMERO DOCUMENTO
        // sin validaciones

        // OFICINA
        // sin validaciones

        // Verification Validation
        if (this.validationService.isLineError()) {
            return null;
        }

        // Non-Catalogue Validation
        Cuenta cuenta = this.cuentaRepository.getByNumero(Double.parseDouble(recaudo.getNroCuenta()));
        if (cuenta == null) {
            cuenta = new Cuenta();
            cuenta.setNumero(Double.parseDouble(recaudo.getNroCuenta()));
            cuenta.setEstrato(Integer.parseInt(recaudo.getEstrato()));
            cuenta.setMunicipio(Long.parseLong(recaudo.getMunicipio()));
            cuenta.setSucursal(sucursal);
            cuenta.setCicloFacturacion(Integer.parseInt(recaudo.getCiclo()));
            cuenta.setCategoria(categoriaCuenta);
        }

        Cliente cliente = this.clienteRepository.getByNumeroIdentificacion(Double.parseDouble(recaudo.getNroDocumentoTitular()));
        if (cliente == null) {
            cliente = new Cliente();
            cliente.setNumeroIdentificacion(Double.parseDouble(recaudo.getNroDocumentoTitular()));
            cliente.setTipoDocumento(tipoDocumento);
            cliente.setCuenta(cuenta);
        }

        Contrato contrato = this.contratoRepository.getByNumero(recaudo.getNumeroContrato());
        if (contrato == null) {
            contrato = new Contrato();
            contrato.setNumero(recaudo.getNumeroContrato());
            contrato.setCliente(cliente);
            contrato.setSubProducto(subproducto);
            contrato.setCliente(cliente);
        }

        Servicio servicio = this.servicioRepository.getByNumero(recaudo.getNroServicio() + "");
        if (servicio == null) {
            servicio = new Servicio();
            servicio.setNumero(recaudo.getNroServicio() + "");
            servicio.setTipoConsumo(Integer.parseInt(recaudo.getTipoConsumo()));
            servicio.setContrato(contrato);
        }

        RecaudoCargo recaudoCargo = new RecaudoCargo();
        recaudoCargo.setValorPago(Double.parseDouble(recaudo.getValorPagado()));
        recaudoCargo.setMonto(Double.parseDouble(recaudo.getMonto()));
        recaudoCargo.setOficina(Long.parseLong(recaudo.getOficina()));
        recaudoCargo.setFechaDocumento(DateUtils.stringToLocalDate(recaudo.getFechaDocumento()));
        recaudoCargo.setMedioPago(formaPago);
        recaudoCargo.setTipoPago(Integer.parseInt(recaudo.getTipoPago()));
        recaudoCargo.setNroDocumento(recaudo.getNroDocumento());
        recaudoCargo.setServicio(servicio);
        recaudoCargo.setCliente(cliente);

        HistorialRecaudo historialRecaudo = new HistorialRecaudo();
        historialRecaudo.setFechaReporte(FileUtils.extractDateFromFilename(this.file, "yyyMMddHHmmss"));
        historialRecaudo.setFechaPago(DateUtils.stringToLocalDate(recaudo.getFechaPago()));
        historialRecaudo.setFechaProcesoPago(DateUtils.stringToLocalDate(recaudo.getFechaProcesoPago()));
        historialRecaudo.setRecaudoCargo(recaudoCargo);
        historialRecaudo.setCargo(cargo);
        historialRecaudo.setFormaPago(formaPago);
        historialRecaudo.setDescripcion(recaudo.getDescripcion());
        historialRecaudo.setTipoConsumo(recaudo.getTipoConsumo());
        historialRecaudo.setFechaFacturacion(DateUtils.stringToLocalDate(recaudo.getFechaFacturacion()));
        historialRecaudo.setSucursalRecaudo(Integer.parseInt(recaudo.getSucursalRecaudo()));
        historialRecaudo.setCodEntidadRecaudadora(Integer.parseInt(recaudo.getCodEntidadRecaudadora()));

        return historialRecaudo;
    }

    /**
     * Producto Repository Setter
     *
     * @param productoRepository Producto Repository
     */
    public void setProductoRepository(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    /**
     * Tipo producto Repository Setter
     *
     * @param tipoProductoRepository Tipo producto Repository
     */
    public void setTipoProductoRepository(TipoProductoRepository tipoProductoRepository) {
        this.tipoProductoRepository = tipoProductoRepository;
    }

    /**
     * Tipo Documento Repository Setter
     *
     * @param tipoDocumentoRepository Tipo documento Repository
     */
    public void setTipoDocumentoRepository(TipoDocumentoRepository tipoDocumentoRepository) {
        this.tipoDocumentoRepository = tipoDocumentoRepository;
    }

    /**
     * Tipo Consumo Repository Setter
     *
     * @param tipoConsumoRepository Tipo Consumo Repository
     */
    public void setTipoConsumoRepository(TipoConsumoRepository tipoConsumoRepository) {
        this.tipoConsumoRepository = tipoConsumoRepository;
    }

    /**
     * Clase Servicio Repository Setter
     *
     * @param claseServicioRepository Clase Servicio Repository
     */
    public void setClaseServicioRepository(ClaseServicioRepository claseServicioRepository) {
        this.claseServicioRepository = claseServicioRepository;
    }

    /**
     * Sucursal Repository Setter
     *
     * @param sucursalRepository Sucursal Repository
     */
    public void setSucursalRepository(SucursalRepository sucursalRepository) {
        this.sucursalRepository = sucursalRepository;
    }

    /**
     * Cateogria Repository Setter
     *
     * @param categoriaRepository Categoria Repository
     */
    public void setCategoriaRepository(CategoriaRepository categoriaRepository) {
        this.categoriaRepository = categoriaRepository;
    }

    /**
     * Cargo Repository Setter
     *
     * @param cargoRepository Cargo Repository
     */
    public void setCargoRepository(CargoRepository cargoRepository) {
        this.cargoRepository = cargoRepository;
    }

    /**
     * Forma Pago Repository Setter
     *
     * @param formaPagoRepository Forma Pago Repository
     */
    public void setFormaPagoRepository(FormaPagoRepository formaPagoRepository) {
        this.formaPagoRepository = formaPagoRepository;
    }

    /**
     * Municipio Repository Setter
     *
     * @param municipioRepository Municipio Repository
     */
    public void setMunicipioRepository(MunicipioRepository municipioRepository) {
        this.municipioRepository = municipioRepository;
    }

    /**
     * Cliente Repository Setter
     *
     * @param clienteRepository Cliente Repository
     */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Contrato Repository Setter
     *
     * @param contratoRepository Contrato Repository
     */
    public void setContratoRepository(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    /**
     * Cuenta Repository Setter
     *
     * @param cuentaRepository Cuenta Repository
     */
    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Servicio Repository Setter
     *
     * @param servicioRepository Servicio Repository
     */
    public void setServicioRepository(ServicioRepository servicioRepository) {
        this.servicioRepository = servicioRepository;
    }

    /**
     * Validation Service Setter
     *
     * @param validationService Validarion Service
     */
    public void setValidationService(ValidationService validationService) {
        this.validationService = validationService;
    }
}
