package com.colpatria.midas.processors;

import com.colpatria.midas.dto.FinalizacionesDto;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.FinalizacionesItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import java.time.LocalDate;

public class FinalizacionesItemProcessor implements ItemProcessor<FinalizacionesDto, InfoFinalizacion> {

    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(FinalizacionesItemWriter.class);

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * add 'cliente' repository
     */
    private final ClienteRepository clienteRepository;

    /**
     * add 'contrato' repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * add 'servicio' repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * add 'cuenta' repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * add InfoFinalizacionRepository
     */
    private final InfoFinalizacionRepository infoFinalizacionRepository;

    /**
     * add 'usuario' repository
     */
    private final UsuarioRepository usuarioRepository;

    /**
     * file name to process
     */
    private String fileName;

    /**
     * Tipo Documento repository
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Tipo producto repository
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * add 'producto' repository
     */
    private final ProductoRepository productoRepository;

    /**
     * @param validationService          DI of validation service
     * @param clienteRepository          DI of cliente repository
     * @param contratoRepository         DI of contratoRepository
     * @param servicioRepository         DI of servicioRepository
     * @param cuentaRepository           DI of cuentaRepository
     * @param infoFinalizacionRepository DI of infoFinalizacionRepository
     * @param usuarioRepository          DI of usuarioRepository
     * @param fileName                   file name to process
     * @param tipoDocumentoRepository
     * @param tipoProductoRepository
     * @param productoRepository
     */
    public FinalizacionesItemProcessor(ValidationService validationService, ClienteRepository clienteRepository, ContratoRepository contratoRepository, ServicioRepository servicioRepository, CuentaRepository cuentaRepository, InfoFinalizacionRepository infoFinalizacionRepository, UsuarioRepository usuarioRepository, String fileName, TipoDocumentoRepository tipoDocumentoRepository, TipoProductoRepository tipoProductoRepository, ProductoRepository productoRepository) {
        this.validationService = validationService;
        this.clienteRepository = clienteRepository;
        this.contratoRepository = contratoRepository;
        this.servicioRepository = servicioRepository;
        this.cuentaRepository = cuentaRepository;
        this.infoFinalizacionRepository = infoFinalizacionRepository;
        this.usuarioRepository = usuarioRepository;
        this.fileName = fileName;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.productoRepository = productoRepository;
    }


    @Override
    public InfoFinalizacion process(FinalizacionesDto item) throws Exception {

        this.validationService.increaseRegisterCounter();
        this.validationService.cleanLineErrors();

        if (ValidateStructure(item)) {
            return null;
        }

        Producto subproducto = productoRepository.getByCodigo(item.getCodigoSubProducto());
        Producto producto = this.productoRepository.getProductoByCodigo(item.getCodigoProducto());
        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(item.getCodigoTipoProducto());
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(item.getTipoIdentificacion());

        this.validationService.verifyCatalog(subproducto, "Subproducto", this.fileName);
        this.validationService.verifyCatalog(producto, "Producto", this.fileName);
        this.validationService.verifyCatalog(tipoProducto, "tipo producto", this.fileName);
        this.validationService.verifyCatalog(tipoDocumento, "tipo documento", this.fileName);

        String codigo = subproducto.getProductoPadre() != null ? subproducto.getProductoPadre().getCodigo() : "";
        this.validationService.verifyParent(producto.getCodigo(),
                codigo,
                "Producto",
                "SubProducto",
                this.fileName);

        this.validationService.verifyParent(
                producto.getTipo().getCodigo(),
                tipoProducto.getCodigo(),
                "Producto",
                "Tipo producto",
                this.fileName);

        if (this.validationService.isLineError()) {
            return null;
        }

        Cuenta cuenta = this.cuentaRepository.getByNumero(Double.parseDouble(item.getNroCuenta()));
        if (cuenta == null) {
            cuenta = new Cuenta();
            cuenta.setNumero(Double.parseDouble(item.getNroCuenta()));
        }

        Cliente cliente = this.clienteRepository.getByNumeroIdentificacion(Double.parseDouble(item.getNroIdentificacion()));
        if (cliente == null) {
            cliente = new Cliente();
            cliente.setTipoDocumento(tipoDocumento);
            cliente.setNumeroIdentificacion(Double.parseDouble(item.getNroIdentificacion()));
            cliente.setCuenta(cuenta);
        }

        cliente.setCuenta(cliente.getCuenta() == null ? cuenta : cliente.getCuenta());

        String contratoNumero = item.getNroContrato();
        Contrato contrato = this.contratoRepository.getByNumero(contratoNumero);
        if (contrato == null) {
            contrato = new Contrato();
            contrato.setNumero(contratoNumero);
            contrato.setCliente(cliente);
            contrato.setSubProducto(subproducto);
        }

        contrato.setCliente(contrato.getCliente() == null ? cliente : contrato.getCliente());
        Servicio servicio = this.servicioRepository.getByNumero(item.getNroServicio());
        if (servicio == null) {
            servicio = new Servicio();
            servicio.setNumero(item.getNroServicio());
            servicio.setContrato(contrato);
        }

        servicio.setContrato(servicio.getContrato() == null ? contrato : servicio.getContrato());

        Usuario usuario = this.usuarioRepository.getUsuarioById(item.getUsuario());
        if (usuario == null) {
            usuario = new Usuario();
            usuario.setArea(item.getArea());
            usuario.setId(item.getUsuario());
            usuario.setNombres(item.getNombre());
        }

        InfoFinalizacion data = new InfoFinalizacion();
        data.setObservaciones(item.getObservaciones());
        data.setEstadoAnterior(item.getEstadoAnterior());
        data.setEstadoNuevo(item.getEstadoNuevo());
        data.setEstadoNuevo(item.getEstadoNuevo());
        data.setUsuario(usuario);
        data.setServicio(servicio);
        data.setFechaCambio(LocalDate.parse(item.getFechaCambio()));
        data.setCargoConcepto(Double.parseDouble(item.getCargoConcepto()));
        data.setCargoCapital(Double.parseDouble(item.getCargoCapital()));
        data.setCargoInteresCorriente(Double.parseDouble(item.getCargoInteresCorriente()));
        data.setCargoInteresMora(Double.parseDouble(item.getCargoInteresMora()));
        data.setDeudaPendienteFacturar(Double.parseDouble(item.getCargoPendienteFacturar()));

        return data;
    }

    /**
     * Validate data types
     *
     * @param item to process
     * @return result of process
     */
    private Boolean ValidateStructure(FinalizacionesDto item) {

        this.validationService.verifyEmpty(item.getCodigoProducto(), "Codigo producto", fileName);
        this.validationService.verifyEmpty(item.getCodigoTipoProducto(), "Codigo tipo producto", fileName);
        this.validationService.verifyEmpty(item.getCodigoSubProducto(), "Codigo sub producto", fileName);
        this.validationService.verifyEmpty(item.getTipoIdentificacion(), "Tipo identificacion", fileName);
        this.validationService.verifyEmpty(item.getNroIdentificacion(), "Numero identificacion", fileName);
        this.validationService.verifyEmpty(item.getNroServicio(), "Servicio", fileName);
        this.validationService.verifyEmpty(item.getFechaCambio(), "Fecha de cambio", fileName);
        this.validationService.verifyEmpty(item.getNroCuenta(), "Cuenta", fileName);

        if (this.validationService.isLineError())
            return this.validationService.isLineError();

        this.validationService.verifyDate(item.getFechaCambio(), "Fecha cambio", this.fileName);
        this.validationService.verifyNumber(item.getNroContrato(), "Contrato", this.fileName);
        this.validationService.verifyNumber(item.getNroServicio(), "Servicio", this.fileName);
        this.validationService.verifyNumber(item.getNroIdentificacion(), "Numero identificacion", this.fileName);
        this.validationService.verifyNumber(item.getNroCuenta(), "Numero cuenta", this.fileName);
        this.validationService.verifyNumber(item.getCargoConcepto(), "Cargo concepto", this.fileName);
        this.validationService.verifyNumber(item.getCargoCapital(), "Cargo capital", this.fileName);
        this.validationService.verifyNumber(item.getCargoInteresCorriente(), "Cargo interes corriente", this.fileName);
        this.validationService.verifyNumber(item.getCargoInteresMora(), "Cargo interes mora", this.fileName);
        this.validationService.verifyNumber(item.getCargoPendienteFacturar(), "Deuda pendiente por facturar", this.fileName);

        if (this.validationService.isLineError())
            return this.validationService.isLineError();

        this.validationService.verifyZero(item.getNroContrato(), "Contrato", this.fileName);
        this.validationService.verifyZero(item.getNroCuenta(), "Cuenta", this.fileName);
        this.validationService.verifyZero(item.getNroIdentificacion(), "Número de documento", this.fileName);
        this.validationService.verifyZero(item.getNroServicio(), "Número de servicio", this.fileName);

        return this.validationService.isLineError();
    }
}
