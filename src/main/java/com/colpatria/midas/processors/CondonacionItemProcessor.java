package com.colpatria.midas.processors;

import com.colpatria.midas.dto.CondonacionDto;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.DateUtils;
import com.colpatria.midas.utils.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class CondonacionItemProcessor implements ItemProcessor<CondonacionDto, HistorialCondonacion> {

    /**
     * Add logger to pagos config
     */
    private static final Logger logger = LoggerFactory.getLogger(PagoItemProcessor.class);

    /**
     * add contrato repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * add servicio repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * Add file name
     */
    private String fileName;
    /**
     * Add email service
     */
    private EmailService emailService;

    /**
     * add product repository
     */
    private ProductoRepository productoRepository;

    /**
     * add product repository
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Tipo Documento repository
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Add client repository
     */
    private ClienteRepository clienteRepository;

    /**
     * Add Condonacion repository
     */
    private HistorialCondonacionRepository historialCondonacionRepository;

    /**
     * Add Negociacion repository
     */
    private NegociacionRepository negociacionRepository;

    /**
     * Add Tipo Negociacion repository
     */
    private TipoNegociacionRepository tipoNegociacionRepository;

    /**
     * Add Usuario repository
     */
    private UsuarioRepository usuarioRepository;


    /**
     * add cuenta repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * validation service
     */
    private ValidationService validationService;

    /**
     *
     * @param fileName The name of the file to process
     * @param emailService The object that allows tu use Email services
     * @param productoRepository The object that refers to the Entity Producto Repository
     * @param tipoProductoRepository The object that refers to the Entity TipoProducto Repository
     * @param tipoDocumentoRepository The object that refers to the Entity TipoDocumentoRepositorio Repository
     * @param clienteRepository The object that refers to the Entity Cliente Repository
     * @param historialCondonacionRepository The object that refers to the Entity Condonacion Repository
     * @param negociacionRepository The object that refers to the Entity Negociacion Repository
     * @param tipoNegociacionRepository The object that refers to the Entity TipoNegociacion Repository
     * @param usuarioRepository The object that refers to the Entity Usuario Repository
     * @param validationService The object that allows tu use validation services
     */
    public CondonacionItemProcessor(String fileName,
                             EmailService emailService,
                             ProductoRepository productoRepository,
                             TipoProductoRepository tipoProductoRepository,
                             TipoDocumentoRepository tipoDocumentoRepository,
                             ClienteRepository clienteRepository,
                             HistorialCondonacionRepository historialCondonacionRepository,
                             NegociacionRepository negociacionRepository,
                             TipoNegociacionRepository tipoNegociacionRepository,
                             UsuarioRepository usuarioRepository,
                             CuentaRepository cuentaRepository,
                             ContratoRepository contratoRepository,
                             ServicioRepository servicioRepository,
                             ValidationService validationService) {
        this.fileName = fileName;
        this.productoRepository = productoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.clienteRepository = clienteRepository;
        this.historialCondonacionRepository = historialCondonacionRepository;this.negociacionRepository = negociacionRepository;
        this.tipoNegociacionRepository = tipoNegociacionRepository;
        this.usuarioRepository = usuarioRepository;
        this.emailService = emailService;
        this.cuentaRepository = cuentaRepository;
        this.contratoRepository = contratoRepository;
        this.servicioRepository = servicioRepository;
        this.validationService = validationService;
    }

    @Override
    public HistorialCondonacion process(CondonacionDto condonacionDto) throws Exception {
        this.validationService.cleanLineErrors();
        this.validationService.increaseRegisterCounter();

        //Date fields validation
        if (!this.validationService.isEmptyField(condonacionDto.getFechaNegociacion(), "FechaNegociacion", this.fileName))
            this.validationService.validateDate(condonacionDto.getFechaNegociacion(), "Fecha de pago", this.fileName);
        this.validationService.validateDate(condonacionDto.getFechaAplicacion(), "Fecha de aplicación", this.fileName);
        this.validationService.validateDate(condonacionDto.getFechaRechazo(), "Fecha de Rechazo", this.fileName);
        this.validationService.validateDate(condonacionDto.getFechaVencimiento(), "Fecha de vencimiento", this.fileName);
        this.validationService.validateDate(condonacionDto.getFechaPago(), "Fecha de negociacion", this.fileName);

        //Integer Fields Validation
        if (!this.validationService.isEmptyField(condonacionDto.getProductoTipoCodigo(), "TipoProducto", this.fileName))
            this.validationService.validateInteger(condonacionDto.getProductoTipoCodigo(), "TipoProducto", this.fileName);
        if (!this.validationService.isEmptyField(condonacionDto.getProductoCodigo(), "CodigoProducto", this.fileName))
            this.validationService.validateInteger(condonacionDto.getProductoCodigo(), "CodigoProducto", this.fileName);
        if (!this.validationService.isEmptyField(condonacionDto.getSubproductoCodigo(), "CodigoSubproducto", this.fileName))
            this.validationService.validateInteger(condonacionDto.getSubproductoCodigo(), "CodigoSubproducto", this.fileName);
        if (!this.validationService.isEmptyField(condonacionDto.getCiclo(), "CicloFacturacion", this.fileName))
            this.validationService.validateInteger(condonacionDto.getCiclo(), "CicloFacturacion", this.fileName);
        if (!this.validationService.isEmptyField(condonacionDto.getTipoNegociacion(), "TipoNegociacion", this.fileName))
            this.validationService.validateInteger(condonacionDto.getTipoNegociacion(), "TipoNegociacion", this.fileName);
        if (!this.validationService.isEmptyField(condonacionDto.getTipoIdentificacion(), "TipoDocumento", this.fileName))
            this.validationService.validateInteger(condonacionDto.getTipoIdentificacion(), "TipoDocumento", this.fileName);
        this.validationService.verifyEmpty(condonacionDto.getEstadoNegociacion(),"EstadoNegociacion",this.fileName);
        //Long Fields Validation
        if (!this.validationService.isEmptyField(condonacionDto.getNumeroIdentificacion(), "NumeroIdentificacion", this.fileName))
            this.validationService.validateLong(condonacionDto.getNumeroIdentificacion(), "NumeroIdentificacion", this.fileName);
        if (!this.validationService.isEmptyField(condonacionDto.getIdNegociacion(), "IdNegociacion", this.fileName))
            this.validationService.validateLong(condonacionDto.getIdNegociacion(), "IdNegociacion", this.fileName);
        if (!this.validationService.isEmptyField(condonacionDto.getContratoNumero(), "NumeroContrato", this.fileName))
            this.validationService.validateLong(condonacionDto.getContratoNumero(), "NumeroContrato", this.fileName);
        if (!this.validationService.isEmptyField(condonacionDto.getNumeroCuenta(), "NumeroCuenta", this.fileName))
            this.validationService.validateLong(condonacionDto.getNumeroCuenta(), "NumeroCuenta", this.fileName);
        if (!this.validationService.isEmptyField(condonacionDto.getNumeroServicio(), "NumeroServicio", this.fileName))
            this.validationService.validateLong(condonacionDto.getNumeroServicio(), "NumeroServicio", this.fileName);

        //Double Fields validation
        this.validationService.validateDouble(condonacionDto.getValorPago(),"ValorPago",this.fileName);
        this.validationService.validateDouble(condonacionDto.getCapitalCondonado(),"CapitalCondonacion",this.fileName);
        this.validationService.validateDouble(condonacionDto.getSaldoCapitalCondonado(),"SaldoCapitalCondonacion",this.fileName);

        if (this.validationService.isLineError()) {
            return null;
        }

        //Catalogue Entities
        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(condonacionDto.getProductoTipoCodigo());
        this.validationService.verifyCatalog(tipoProducto,"TipoProducto",this.fileName);
        Producto producto = productoRepository.getByCodigo(condonacionDto.getProductoCodigo());
        this.validationService.verifyCatalog(producto, "CodigoProducto", this.fileName);
        Producto subproducto = productoRepository.getByCodigo(condonacionDto.getSubproductoCodigo());
        this.validationService.verifyCatalog(subproducto, "CodigoSubproducto", this.fileName);
        this.validationService.validateIntegerWithZero(condonacionDto.getCiclo(), "Ciclo", this.fileName);
        TipoNegociacion tipoNegociacion = this.tipoNegociacionRepository.getTipoNegociacionById(Integer.parseInt(condonacionDto.getTipoNegociacion()));
        this.validationService.verifyCatalog(tipoNegociacion, "TipoNegociacion", this.fileName);
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(condonacionDto.getTipoIdentificacion());
        this.validationService.verifyCatalog(tipoDocumento, "TipoDocumento", this.fileName);

        // Verification Validation
        if (this.validationService.isLineError()) {
            return null;
        }

        // Subproducto - producto relationship Validation
        String codigo = subproducto!=null && subproducto.getProductoPadre() != null ? subproducto.getProductoPadre().getCodigo() : "";
        if (producto!=null)
            this.validationService.verifyParent(producto.getCodigo(), codigo, "Producto", "SubProducto", this.fileName);

        // Verification Validation
        if (this.validationService.isLineError()) {
            return null;
        }

        //Getting Related Entities
        Cliente cliente = this.clienteRepository.getByNumeroIdentificacion(Double.parseDouble(condonacionDto.getNumeroIdentificacion()));
        Negociacion negociacion = this.negociacionRepository.getNegociacionById(condonacionDto.getIdNegociacion());
        Contrato contrato = this.contratoRepository.getByNumero(condonacionDto.getContratoNumero());
        Cuenta cuenta = this.cuentaRepository.getByNumero(Double.parseDouble(condonacionDto.getNumeroCuenta()));
        Servicio servicio = this.servicioRepository.getByNumero(condonacionDto.getNumeroServicio());
        Usuario usuario = this.usuarioRepository.getUsuarioById(condonacionDto.getCodUsuario());

        //Validate
        if (cliente == null) {
            cliente = new Cliente();
            cliente.setTipoDocumento(tipoDocumento);
            cliente.setNumeroIdentificacion(Double.parseDouble(condonacionDto.getNumeroIdentificacion()));
            cliente = clienteRepository.saveAndFlush(cliente);
        }
        if (cuenta==null) {
            cuenta = new Cuenta();
            cuenta.setNumero(Double.parseDouble(condonacionDto.getNumeroCuenta()));
            cuenta.setCliente(cliente);
            cuenta.setCicloFacturacion(Integer.parseInt(condonacionDto.getCiclo()));
            cuenta = cuentaRepository.saveAndFlush(cuenta);
        }
        if (cliente.getCuenta()==null)
            cliente.setCuenta(cuenta);
        if (negociacion == null) {
            negociacion = new Negociacion();
            negociacion.setId(condonacionDto.getIdNegociacion());
            negociacion.setEstado(condonacionDto.getEstadoNegociacion());
            negociacion.setFecha(DateUtils.stringToLocalDate(condonacionDto.getFechaNegociacion()));
            negociacion = negociacionRepository.saveAndFlush(negociacion);
        }
        if (contrato==null){
            contrato=new Contrato();
            contrato.setNumero(condonacionDto.getContratoNumero());
            contrato.setSubProducto(subproducto);
            contrato = contratoRepository.saveAndFlush(contrato);
        }
        if (contrato.getCliente()==null)
            contrato.setCliente(cliente);
        /* No se actualiza el subproducto de acuerdo con las reglas de negocio
        if (contrato.getSubProducto()==null)
            contrato.setSubProducto(subproducto);*/
        if (servicio==null){
            servicio = new Servicio();
            servicio.setNumero(condonacionDto.getNumeroServicio());
            servicio = servicioRepository.saveAndFlush(servicio);
        }
        if (servicio.getContrato()==null)
            servicio.setContrato(contrato);
        if (contrato.getServicios()==null || contrato.getServicios().size()==0){
            List<Servicio> servicios = new ArrayList<Servicio>();
            servicios.add(servicio);
            contrato.setServicios(servicios);
        }else{
            contrato.getServicios().add(servicio);
        }
        if (usuario == null && condonacionDto.getCodUsuario() != null) {
            usuario = new Usuario();
            usuario.setId(condonacionDto.getCodUsuario());
            if (condonacionDto.getUser() != null)
                usuario.setNombres(condonacionDto.getUser());
            if (condonacionDto.getArea() != null)
                usuario.setArea(condonacionDto.getArea());
            usuario=usuarioRepository.saveAndFlush(usuario);
        }

        // Historial Facturacion
        HistorialCondonacion historialCondonacion = new HistorialCondonacion();
        historialCondonacion.setNegociacion(negociacion);
        historialCondonacion.setServicio(servicio);
        historialCondonacion.setFechaPago(DateUtils.stringToLocalDate(condonacionDto.getFechaPago()));
        historialCondonacion.setFechaAplica(DateUtils.stringToLocalDate(condonacionDto.getFechaAplicacion()));
        historialCondonacion.setFechaRechazo(DateUtils.stringToLocalDate(condonacionDto.getFechaRechazo()));
        historialCondonacion.setFechaVencimiento(DateUtils.stringToLocalDate(condonacionDto.getFechaVencimiento()));
        historialCondonacion.setMotivoRechazo(condonacionDto.getMotivoRechazo());
        historialCondonacion.setTipoNegociacion(tipoNegociacion);
        historialCondonacion.setValorPago(new BigDecimal(condonacionDto.getValorPago()));
        historialCondonacion.setDescripcion(condonacionDto.getDescripcion());
        historialCondonacion.setCapitalCondona(new BigDecimal(condonacionDto.getCapitalCondonado()));
        historialCondonacion.setSaldoCapitalCondona(new BigDecimal(condonacionDto.getSaldoCapitalCondonado()));
        historialCondonacion.setUsuario(usuario);
        return historialCondonacion;
    }

}
