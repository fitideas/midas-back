package com.colpatria.midas.processors;

/*
 *
 * Librerías
 *
 */

import com.colpatria.midas.dto.CarteraDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.model.ClaseServicio;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.Cuenta;
import com.colpatria.midas.model.Estado;
import com.colpatria.midas.model.HistorialCartera;
import com.colpatria.midas.model.Producto;
import com.colpatria.midas.model.Servicio;
import com.colpatria.midas.model.Sucursal;
import com.colpatria.midas.model.TipoConsumo;
import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.model.TipoProducto;
import com.colpatria.midas.model.TipoTarjeta;
import com.colpatria.midas.utils.DateUtils;
import com.colpatria.midas.utils.FileUtils;
import org.springframework.batch.item.ItemProcessor;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;

import java.math.BigInteger;

/**
 * Class that is use to transform, filter and verify the items that are being read in a specific batch process
 */
public class CarteraItemProcessor implements ItemProcessor<CarteraDto, HistorialCartera> {

    /**
     * Constant to represent the product
     */
    private static final String PRODUCTO = "Producto";

    /**
     * Constant to represent the by-product
     */
    private static final String SUBPRODUCTO = "Subproducto";

    /**
     * Constant to represent the client
     */
    private static final String CLIENTE = "Cliente";

    /**
     * Constant to represent the contract
     */
    private static final String CONTRATO = "Contrato";

    /**
     * Constant to represent the account
     */
    private static final String CUENTA = "Cuenta";

    /**
     * Constant to represent the service
     */
    private static final String SERVICIO = "Servicio";

    /**
     * Service repository to read and write to the database
     */
    private ServicioRepository servicioRepository;

    /**
     * Contract repository to read and write to the database
     */
    private ContratoRepository contratoRepository;

    /**
     * Product repository to read and write to the database
     */
    private ProductoRepository productoRepository;

    /**
     * Branch repository to read and write to the database
     */
    private SucursalRepository sucursalRepository;

    /**
     * Banck account repository to read and write to the database
     */
    private CuentaRepository cuentaRepository;

    /**
     * Client repository to read and write to the database
     */
    private ClienteRepository clienteRepository;

    /**
     * Consumption type repository to read and write to the database
     */
    private TipoConsumoRepository tipoConsumoRepository;

    /**
     * Service class repository to read and write to the database
     */
    private ClaseServicioRepository claseServicioRepository;

    /**
     * Product type repository to read and write to the database
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Document type repository to read and write to the database
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Service state repository to read and write to the database
     */
    private EstadoRepository estadoRepository;

    /**
     * File name
     */
    private final String fileName;

    /**
     * validation service
     */
    private final ValidationService validationService;

    /*
     *
     * Métodos
     *
     */

    /**
     * Constructor of the processor class
     *
     * @param fileName          identifies the file that is being read in the batch process
     * @param validationService is the validation service
     */
    public CarteraItemProcessor(String fileName, ValidationService validationService) {
        this.fileName = fileName;
        this.validationService = validationService;
    }

    /**
     * Method that process the items that are being read from the file in the batch process
     *
     * @param item to be process
     * @return the item that has been process
     * @throws VerificationException in case that a mandatory field in the item is null or empty
     */
    @Override
    public HistorialCartera process(CarteraDto item) throws Exception {
        // Variables
        HistorialCartera historialCartera;
        // Code
        validationService.cleanLineErrors();
        validationService.increaseRegisterCounter();
        valdateFileFields(item);
        if (this.validationService.isLineError()) {
            return null;
        }
        historialCartera = processHistorialCartera(item);
        if (this.validationService.isLineError()) {
            return null;
        }
        return historialCartera;
    }

    /**
     * Method that verifies the data
     *
     * @param item is the list of items to be written in the database
     */
    private void valdateFileFields(CarteraDto item) {
        // Contract
        this.validationService.verifyEmpty(item.getCodigoProducto(), "código producto", this.fileName);
        this.validationService.verifyEmpty(item.getCodigoTipoProducto(), "código tipo producto", this.fileName);
        this.validationService.verifyEmpty(item.getCodigoSubproducto(), "código subproducto", this.fileName);
        this.validationService.validateIntegerWithZero(item.getNumeroContrato(), "número de contrato", this.fileName);
        this.validationService.validateIntegerWithZero(item.getTipoIdentificacion(), "tipo de identificación", this.fileName);
        this.validationService.validateIntegerWithZero(item.getNumeroIdentificacion(), "número de identificación", fileName);
        this.validationService.validateInteger(item.getNumeroCuenta(), "número de cuenta", fileName);
        this.validationService.validateInteger(item.getSucursal(), "sucursal", this.fileName);
        this.validationService.validateIntegerWithZero(item.getCiclo(), "ciclo", this.fileName);

        this.validationService.validateInteger(item.getEstrato(), "estrato", this.fileName);
        // Service
        this.validationService.verifyEmpty(item.getClaseServicio(), "clase de servicio", fileName);
        this.validationService.validateIntegerWithZero(item.getNumeroServicio(), "número de servicio", fileName);
        this.validationService.validateDate(item.getFechaCreacion(), "fecha de creación", this.fileName);
        this.validationService.validateDate(item.getFechaCompra(), "fecha de compra", this.fileName);
        this.validationService.validateInteger(item.getTipoConsumo(), "tipo de consumo", fileName);

        this.validationService.validateInteger(item.getCuotasPactadas(), "cuotas pactadas", this.fileName);
        this.validationService.validateInteger(item.getCuotasPorFacturar(), "cuotas por facturar", this.fileName);
        this.validationService.validateDouble(item.getValorCuota(), "valor cuota", this.fileName);
        this.validationService.validateDouble(item.getValorCompra(), "valor compra", this.fileName);
        this.validationService.validateDouble(item.getTasa(), "tasa", this.fileName);
        this.validationService.validateDouble(item.getSaldoDeuda(), "saldo deuda", this.fileName);
        this.validationService.validateDouble(item.getSaldoCapitalPorFacturar(), "saldo capital por facturar", this.fileName);
        this.validationService.verifyEmpty(item.getEstadoServicio(), "estado de servicio", fileName);
        // History

        this.validationService.validateInteger(item.getDiasAtraso(), "días atrado", this.fileName);
        this.validationService.validateInteger(item.getMoraPrimerVencimiento(), "mora primer vencimiento", this.fileName);
        this.validationService.validateDouble(item.getCapitalImpago(), "capital impago", this.fileName);
        this.validationService.validateDouble(item.getInteresImpago(), "interés impago", this.fileName);
        this.validationService.validateDouble(item.getInteresMora(), "interés mora", this.fileName);
        this.validationService.validateDouble(item.getInteresFacturadoNoAfecto(), "interés facturado no afecto", this.fileName);
        this.validationService.validateDate(item.getFechaPrimerVencimiento(), "fecha primer vencimiento", this.fileName);
        this.validationService.validateDate(item.getFechaSegundoVencimiento(), "fecha segundo vencimiento", this.fileName);
        this.validationService.validateDouble(item.getInteresOrdenCorrienteImpago(), "interés orden corriente impago", this.fileName);
        this.validationService.validateDouble(item.getInteresOrdenMoraImpago(), "interés orden mora impago", this.fileName);
        this.validationService.validateDouble(item.getInteresCausadoNoFacturado(), "interés causado no facturado", this.fileName);
        this.validationService.validateDouble(item.getInteresCorrienteOrdenCausadoNoFacturado(), "interés corriente orden causado no facturado", this.fileName);
        this.validationService.validateDouble(item.getInteresMoraCausadoNoFacturado(), "interés mora causado no facturado", this.fileName);
        this.validationService.validateDouble(item.getInteresMoraOrdenCausadoNoFacturado(), "interés mora orden causado no facturado", this.fileName);
        this.validationService.validateDouble(item.getTotalInteresCorrienteFacturadoImpago(), "total interés corriente facturado impago", this.fileName);
        this.validationService.validateDouble(item.getTotalInteresCorrienteCausado(), "total interés corriente causado", this.fileName);
        this.validationService.validateDouble(item.getTotalInteresMoraFacturadoImpago(), "total interés mora facturado impago", this.fileName);
        this.validationService.validateDouble(item.getTotalInteresMoraCausado(), "total interés mora causado", this.fileName);
        this.validationService.validateDouble(item.getInteresNoAfectoPorFacturar(), "interés no afecto por facturar", this.fileName);
    }

    /**
     * Method that writes the history to the database
     *
     * @param item is the list of items to be written in the database
     * @return the history object
     */
    private HistorialCartera processHistorialCartera(CarteraDto item) {
        // Variables
        Servicio servicio;
        HistorialCartera historialCartera;
        // Código
        servicio = processService(item);
        // History
        historialCartera = new HistorialCartera();
        historialCartera.setDiasAtraso(Integer.parseInt(item.getDiasAtraso()));
        historialCartera.setMoraPrimerVencimiento(Integer.parseInt(item.getMoraPrimerVencimiento()));
        historialCartera.setCapitalImpago(Double.parseDouble(item.getCapitalImpago()));
        historialCartera.setInteresImpago(Double.parseDouble(item.getInteresImpago()));
        historialCartera.setInteresMora(Double.parseDouble(item.getInteresMora()));
        historialCartera.setInteresFacturadoNoAfecto(Double.parseDouble(item.getInteresFacturadoNoAfecto()));
        historialCartera.setMotivoFinalizacion(item.getMotivoFinalizacion());
        historialCartera.setFechaPrimerVencimiento(DateUtils.stringToLocalDate(item.getFechaPrimerVencimiento()));
        historialCartera.setFechaSegundoVencimiento(DateUtils.stringToLocalDate(item.getFechaSegundoVencimiento()));
        historialCartera.setInteresOrdenCorrienteImpago(Double.parseDouble(item.getInteresOrdenCorrienteImpago()));
        historialCartera.setInteresOrdenMoraImpago(Double.parseDouble(item.getInteresOrdenMoraImpago()));
        historialCartera.setInteresCausadoNoFacturado(Double.parseDouble(item.getInteresCausadoNoFacturado()));
        historialCartera.setInteresCorrienteOrdenCausadoNoFacturado(Double.parseDouble(item.getInteresCorrienteOrdenCausadoNoFacturado()));
        historialCartera.setInteresMoraCausadoNoFacturado(Double.parseDouble(item.getInteresMoraCausadoNoFacturado()));
        historialCartera.setInteresMoraOrdenCausadoNoFacturado(Double.parseDouble(item.getInteresMoraOrdenCausadoNoFacturado()));
        historialCartera.setTotalInteresCorrienteFacturadoImpago(Double.parseDouble(item.getTotalInteresCorrienteFacturadoImpago()));
        historialCartera.setTotalInteresCorrienteCausado(Double.parseDouble(item.getTotalInteresCorrienteCausado()));
        historialCartera.setTotalInteresMoraFacturadoImpago(Double.parseDouble(item.getTotalInteresMoraFacturadoImpago()));
        historialCartera.setTotalInteresMoraCausado(Double.parseDouble(item.getTotalInteresMoraCausado()));
        historialCartera.setInteresNoAfectoPorFacturar(Double.parseDouble(item.getInteresNoAfectoPorFacturar()));
        historialCartera.setFechaReporte(FileUtils.extraerFecha(this.fileName));
        historialCartera.setDescripcion(item.getDescripcion());
        historialCartera.setServicio(servicio);
        return historialCartera;
    }

    /**
     * Method that gets the service from the database
     *
     * @param item is an object that contains the data of the file
     * @return a model entity
     */
    private Servicio processService(CarteraDto item) {
        // Variables
        Contrato contrato;
        Servicio servicio;
        TipoConsumo tipoConsumo;
        Estado estado;
        // Consumption type
        tipoConsumo = tipoConsumoRepository.getTipoConsumoById(Integer.parseInt(item.getTipoConsumo()));
        this.validationService.validateCatalogue(tipoConsumo, "Tipo consumo", item.getTipoConsumo(), this.fileName);
        // Service state
        estado = estadoRepository.getEstadoById( item.getEstadoServicio() );
        this.validationService.validateCatalogue(estado, "Estado de servicio", item.getEstadoServicio(), this.fileName);
        // Contract
        contrato = processContract(item);
        // Service
        servicio = servicioRepository.getByNumero(item.getNumeroServicio());
        if (servicio == null) {
            servicio = new Servicio();
            servicio.setNumero(item.getNumeroServicio());
            servicio.setContrato(contrato);
        } else {
            if(servicio.getContrato() == null) {
                servicio.setContrato( contrato );
            }
        }
        servicio.setCuotasPactadas(Integer.parseInt(item.getCuotasPactadas()));
        servicio.setCuotasPorFacturar(Integer.parseInt(item.getCuotasPorFacturar()));
        servicio.setValorCuota(Double.parseDouble(item.getValorCuota()));
        servicio.setValorCompra(Double.parseDouble(item.getValorCompra()));
        servicio.setFechaCreacion(DateUtils.stringToLocalDate(item.getFechaCreacion()));
        servicio.setFechaCompra(DateUtils.stringToLocalDate(item.getFechaCompra()));
        servicio.setTasa(Double.parseDouble(item.getTasa()));
        servicio.setEstado(estado);
        servicio.setSaldoDeuda(Double.parseDouble(item.getSaldoDeuda()));
        servicio.setSaldoCapitalPorFacturar(Double.parseDouble(item.getSaldoCapitalPorFacturar()));
        servicio.setSocioDeNegocio(item.getSocioDeNegocio());
        servicio.setClaseServicio(item.getClaseServicio());
        servicio.setTipoConsumo(Integer.parseInt(item.getTipoConsumo()));
        return servicio;
    }

    /**
     * This method creates the contract
     *
     * @param item is an object that contains the data of the file
     * @return Contrato
     */
    private Contrato processContract(CarteraDto item) {
        // Variables
        Producto producto;
        Producto subproducto;
        TipoProducto tipoProducto;
        Cliente cliente;
        Cuenta cuenta;
        Contrato contrato;
        Sucursal sucursal;
        TipoDocumento tipoDocumento;
        // Code
        tipoProducto = tipoProductoRepository.getTipoProductoByCodigo(item.getCodigoTipoProducto());
        this.validationService.validateCatalogue(tipoProducto, "Tipo producto", item.getCodigoTipoProducto(), this.fileName);
        // Product
        producto = productoRepository.getByCodigo(item.getCodigoProducto());
        this.validationService.validateCatalogue(producto, PRODUCTO, item.getCodigoProducto(), this.fileName);
        if (producto != null && tipoProducto != null) {
            this.validationService.validateParent(tipoProducto, producto.getTipo(), "Tipo producto", tipoProducto.getCodigo().toString(), PRODUCTO, this.fileName);
        }
        // Subproduct
        subproducto = productoRepository.getByCodigo(item.getCodigoSubproducto());
        this.validationService.validateCatalogue(subproducto, SUBPRODUCTO, item.getCodigoSubproducto(), this.fileName);
        if (subproducto != null && producto != null) {
            this.validationService.validateChild(producto, subproducto.getProductoPadre(), SUBPRODUCTO, subproducto.getCodigo().toString(), this.fileName);
        }
        // Billing cycle
        this.validationService.validateIntegerWithZero(item.getCiclo(), "Ciclo de facturación", this.fileName);

        // Headquarters
        sucursal = sucursalRepository.getSucursalByNumero(Integer.parseInt(item.getSucursal()));
        this.validationService.validateCatalogue(sucursal, "Sucursal", item.getSucursal(), this.fileName);
        // Account
        cuenta = cuentaRepository.getByNumero(Double.parseDouble(item.getNumeroCuenta()));
        if (cuenta == null) {
            cuenta = new Cuenta();
            cuenta.setNumero(Double.parseDouble(item.getNumeroCuenta()));
            cuenta.setEstrato(Integer.parseInt(item.getEstrato()));
            cuenta.setCicloFacturacion(Integer.parseInt(item.getCiclo()));
            cuenta.setSucursal(sucursal);
        }
        // Document type
        tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(item.getTipoIdentificacion());
        this.validationService.validateCatalogue(tipoDocumento, "Tipo Documento", item.getTipoIdentificacion(), this.fileName);
        // Client
        cliente = clienteRepository.getByNumeroIdentificacion(Double.parseDouble(item.getNumeroIdentificacion()));
        if (cliente == null) {
            cliente = new Cliente();
            cliente.setNumeroIdentificacion(Double.parseDouble(item.getNumeroIdentificacion()));
            cliente.setTipoDocumento(tipoDocumento);
            cliente.setNombre(item.getNombreCliente());
            cliente.setCuenta(cuenta);
        } else {
            if (cliente.getCuenta() == null) {
                cliente.setCuenta( cuenta );
            }
        }
        // Contract
        contrato = contratoRepository.getByNumero(item.getNumeroContrato());
        if (contrato == null) {
            contrato = new Contrato();
            contrato.setNumero(item.getNumeroContrato());
            contrato.setEstado(item.getEstado());
            contrato.setFacturacion(Boolean.parseBoolean(item.getFacturacion()));
            contrato.setSubProducto(subproducto);
            contrato.setCliente(cliente);
        } else {
            if (contrato.getCliente() == null) {
                contrato.setCliente( cliente );
            }
        }
        return contrato;
    }

    /**
     * Set the repository service history
     *
     * @param servicioRepository is the repository object
     */
    public void setServicioRepository(ServicioRepository servicioRepository) {
        this.servicioRepository = servicioRepository;
    }

    /**
     * Set the contract repository
     *
     * @param contratoRepository is the repository object
     */
    public void setContratoRepository(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    /**
     * Set the product repository
     *
     * @param productoRepository is the repository object
     */
    public void setProductoRepository(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    /**
     * Set the branch repository
     *
     * @param sucursalRepository is the repository object
     */
    public void setSucursalRepository(SucursalRepository sucursalRepository) {
        this.sucursalRepository = sucursalRepository;
    }

    /**
     * Set the account repository
     *
     * @param cuentaRepository is the repository object
     */
    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Set the client repository
     *
     * @param clienteRepository is the repository object
     */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Set the consumption type repository
     *
     * @param tipoConsumoRepository is the repository object
     */
    public void setTipoConsumoRepository(TipoConsumoRepository tipoConsumoRepository) {
        this.tipoConsumoRepository = tipoConsumoRepository;
    }

    /**
     * Set the service class repository
     *
     * @param claseServicioRepository is the repository object
     */
    public void setClaseServicioRepository(ClaseServicioRepository claseServicioRepository) {
        this.claseServicioRepository = claseServicioRepository;
    }

    /**
     * Set the product type repository
     *
     * @param tipoProductoRepository is the repository object
     */
    public void setTipoProductoRepository(TipoProductoRepository tipoProductoRepository) {
        this.tipoProductoRepository = tipoProductoRepository;
    }

    /**
     * Set the document type repository
     *
     * @param tipoDocumentoRepository is the repository object
     */
    public void setTipoDocumentoRepository(TipoDocumentoRepository tipoDocumentoRepository) {
        this.tipoDocumentoRepository = tipoDocumentoRepository;
    }

    /**
     * Set the service state repository
     *
     * @param estadoRepository is the repository object
    */
    public void setEstadoRepository(EstadoRepository estadoRepository) {
        this.estadoRepository = estadoRepository;
    }

}
