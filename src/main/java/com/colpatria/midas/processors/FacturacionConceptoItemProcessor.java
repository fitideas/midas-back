package com.colpatria.midas.processors;

import java.time.LocalDate;
import java.util.Optional;
import com.colpatria.midas.dto.FacturacionConceptoDto;
import com.colpatria.midas.model.Cargo;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.Cuenta;
import com.colpatria.midas.model.HistorialFacturacionConcepto;
import com.colpatria.midas.model.Producto;
import com.colpatria.midas.model.Servicio;
import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.model.TipoProducto;
import com.colpatria.midas.model.Sucursal;
import com.colpatria.midas.repositories.CargoRepository;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.CuentaRepository;
import com.colpatria.midas.repositories.ProductoRepository;
import com.colpatria.midas.repositories.ServicioRepository;
import com.colpatria.midas.repositories.SucursalRepository;
import com.colpatria.midas.repositories.TipoDocumentoRepository;
import com.colpatria.midas.repositories.TipoProductoRepository;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.NumberUtils;
import org.springframework.batch.item.ItemProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that is use to transform, filter and verify the items that are being read in a specific batch process
 */
public class FacturacionConceptoItemProcessor implements  ItemProcessor<FacturacionConceptoDto, HistorialFacturacionConcepto> {

    /**
     * Logger in processor
     */
    private static final Logger logger = LoggerFactory.getLogger(FacturacionConceptoItemProcessor.class);

    /**
     * Attribute that identifies the file that is being read in the batch process
     */
    private String fileName;

    /**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

	/**
	 * add 'cargo' repository
	 */
    private final CargoRepository cargoRepository;

	/**
	 * add 'tipoProducto' repository
	 */
    private final TipoProductoRepository tipoProductoRepository;

	/**
	 * add 'producto' repository
	 */
    private final ProductoRepository productoRepository;

	/**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

	/**
	 * add 'contrato' repository
	 */
	private final ContratoRepository contratoRepository;

	/**
	 * add 'cuenta' repository
	 */
	private final CuentaRepository cuentaRepository;

	/**
	 * add 'sucursal' repository
	 */
	private final SucursalRepository sucursalRepository;

	/**
	 * add 'Tipo Documento' repository
	 */
	private final TipoDocumentoRepository tipoDocumentoRepository;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;

    /**
     * Constant for String 'Producto'
     */
    private static final String PRODUCTO = "Producto";

    /**
     * Constant for String 'Subproducto'
     */
    private static final String SUBPRODUCTO = "Subproducto";

    /**
     * Constant for String 'Cargo'
     */
    private static final String CARGO = "Cargo";

    /**
     * Constant for String 'Sucursal'
     */
    private static final String SUCURSAL = "Sucursal";

    /**
     * Constant for String 'TipoProducto'
     */
    private static final String TIPO_PRODUCTO = "TipoProducto";

    /**
     * Constant for String 'TipoDocumento'
     */
    private static final String TIPO_DOCUMENTO = "TipoDocumento";

    /**
     * Constant for String 'FechaDocumento'
     */
    private static final String FECHA_DOCUMENTO = "FechaDocumento";

    /**
     * Constant for String 'FechaProceso'
     */
    private static final String FECHA_PROCESO = "FechaProceso";

    /**
     * Constant for String 'Saldo'
     */
    private static final String SALDO = "Saldo";

    /**
     * Constant for String 'valorFactura'
     */
    private static final String VALOR_FACTURA = "ValorFactura";

    /**
     * Constant for String 'totalDocumento'
     */
    private static final String TOTAL_DOCUMENTO = "TotalDocumento";

    /**
     * Constant for String 'cicloFacturacion'
     */
    private static final String CICLO_FACTURACION = "CicloFacturacion";

    /**
     * Constant for String 'estrato'
     */
    private static final String ESTRATO = "Estrato";

    /**
     * Constant for String 'Contrato'
     */
    private static final String CONTRATO = "Contrato";

    /**
     * Constant for String 'NumeroDocumento'
     */
    private static final String NUMERO_DOCUMENTO = "NumeroDocumento";

    /**
     * Constant for String 'NumeroCuenta'
     */
    private static final String CUENTA_NUMERO = "NumeroCuenta";

    /**
     * Constant for String 'NumeroServicio'
     */
    private static final String SERVICIO_NUMERO = "NumeroServicio";

    /**
	 * @param fileName identifies the file that is being read in the batch process
	 * @param servicioRepository dependency injection of servicioRepository
	 * @param cargoRepository dependency injection of cargoRepository
	 * @param tipoProductoRepository dependency injection of tipoProductoRepository
	 * @param productoRepository dependency injection of productoRepository
	 * @param clienteRepository dependency injection of clienteRepository
	 * @param sucursalRepository dependency injection of sucursalRepository
	 * @param contratoRepository dependency injection of contratoRepository
	 * @param cuentaRepository dependency injection of cuentaRepository
	 * @param tipoDocumentoRepository dependency injection of tipoDocumentoRepository
	 * @param validationService dependency injection of validationService
	 */
	public FacturacionConceptoItemProcessor(
        String fileName,
		ServicioRepository servicioRepository,
		CargoRepository cargoRepository,
		TipoProductoRepository tipoProductoRepository,
		ProductoRepository productoRepository,
		ClienteRepository clienteRepository,
		SucursalRepository sucursalRepository,
		ContratoRepository contratoRepository,
		CuentaRepository cuentaRepository,
		TipoDocumentoRepository tipoDocumentoRepository,
		ValidationService validationService
	) {
        this.fileName = fileName;
		this.servicioRepository = servicioRepository;
        this.cargoRepository = cargoRepository;
		this.tipoProductoRepository = tipoProductoRepository;
		this.productoRepository = productoRepository;
		this.clienteRepository = clienteRepository;
		this.contratoRepository = contratoRepository;
		this.cuentaRepository = cuentaRepository;
		this.sucursalRepository = sucursalRepository;
		this.tipoDocumentoRepository = tipoDocumentoRepository;
		this.validationService = validationService;
	}

    /**
     * Method that process the items that are being read from the file in the batch process
     * @param item to be process
     * @return the item that has been process
     * @throws Exception
     */
    @Override
    public HistorialFacturacionConcepto process(FacturacionConceptoDto item) throws Exception {

        this.validationService.increaseRegisterCounter();
        this.validationService.cleanLineErrors();

        this.validationService.verifyEmpty(item.getProductoCodigo(), PRODUCTO, this.fileName);
        this.validationService.verifyEmpty(item.getProductoTipoCodigo(), TIPO_PRODUCTO, this.fileName);
        this.validationService.verifyEmpty(item.getSubproductoCodigo(), SUBPRODUCTO, this.fileName);
        this.validationService.verifyEmpty(item.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyEmpty(item.getNumeroIdentificacion(), NUMERO_DOCUMENTO, this.fileName);
        this.validationService.verifyEmpty(item.getCuentaNumero(), CUENTA_NUMERO, this.fileName);
        this.validationService.verifyEmpty(item.getCiclo(), CICLO_FACTURACION, this.fileName);
        this.validationService.verifyEmpty(item.getServicioNumero(), SERVICIO_NUMERO, this.fileName);
        this.validationService.verifyEmpty(item.getSucursal(), SUCURSAL, this.fileName);
        this.validationService.verifyEmpty(item.getCargoCodigo(), CARGO, this.fileName);
        this.validationService.verifyEmpty(item.getFechaDocumento(), FECHA_DOCUMENTO, this.fileName);
        this.validationService.verifyEmpty(item.getFechaProceso(), FECHA_PROCESO, this.fileName);

        if(this.validationService.isLineError()){
            return null;
        }

        this.validationService.validateInteger(item.getProductoCodigo(), PRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getProductoTipoCodigo(), TIPO_PRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getSubproductoCodigo(), SUBPRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName);
        this.validationService.validateInteger(item.getCiclo(), CICLO_FACTURACION, this.fileName);
        this.validationService.validateInteger(item.getContratoNumero(), CONTRATO, this.fileName);
        this.validationService.validateInteger(item.getEstrato(), ESTRATO, this.fileName);
        this.validationService.validateInteger(item.getSucursal(), SUCURSAL, this.fileName);
        this.validationService.validateInteger(item.getNumeroIdentificacion(), NUMERO_DOCUMENTO, this.fileName);
        this.validationService.validateInteger(item.getCuentaNumero(), CUENTA_NUMERO, this.fileName);
        this.validationService.validateInteger(item.getServicioNumero(), SERVICIO_NUMERO, this.fileName);

        this.validationService.validateDouble(item.getSaldo(), SALDO, this.fileName);
        this.validationService.validateDouble(item.getTotalDocumento(), TOTAL_DOCUMENTO, this.fileName);
        this.validationService.validateDouble(item.getValorFactura(), VALOR_FACTURA, this.fileName);

        this.validationService.validateDate(item.getFechaDocumento(), FECHA_DOCUMENTO, this.fileName);
        this.validationService.validateDate(item.getFechaProceso(), FECHA_PROCESO, this.fileName);

        this.validationService.verifyZero(item.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyZero(item.getNumeroIdentificacion(), NUMERO_DOCUMENTO, this.fileName);
        this.validationService.verifyZero(item.getCuentaNumero(), CUENTA_NUMERO, this.fileName);
        this.validationService.verifyZero(item.getCiclo(), CICLO_FACTURACION, this.fileName);
        this.validationService.verifyZero(item.getServicioNumero(), SERVICIO_NUMERO, this.fileName);
        this.validationService.verifyZero(item.getSucursal(), SUCURSAL, this.fileName);
        this.validationService.verifyZero(item.getCargoCodigo(), CARGO, this.fileName);

        if(this.validationService.isLineError()){
            return null;
        }

        String productoTipoCodigo = item.getProductoTipoCodigo();
        String productoCodigo = item.getProductoCodigo();
        String subproductoCodigo = item.getSubproductoCodigo();
        String tipoIdentificacion = item.getTipoIdentificacion();
        Integer sucursalNumero = item.getSucursal() != null ? Integer.parseInt(item.getSucursal()) : null;
        Integer ciclo = NumberUtils.isInteger(item.getCiclo()) ? Integer.parseInt(item.getCiclo()) : -1;

        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(productoTipoCodigo);
        Producto subproducto = this.productoRepository.getByCodigo(subproductoCodigo);
        Producto producto = this.productoRepository.getProductoByCodigo(productoCodigo);
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(tipoIdentificacion);
        Cargo cargo = this.cargoRepository.getCargoById(item.getCargoCodigo());
        Sucursal sucursal = this.sucursalRepository.getSucursalByNumero(sucursalNumero);

        this.validationService.verifyCatalog(tipoProducto, TIPO_PRODUCTO, this.fileName);
        this.validationService.verifyCatalog(subproducto, SUBPRODUCTO, this.fileName);
        this.validationService.verifyCatalog(producto, PRODUCTO, this.fileName);
        this.validationService.verifyCatalog(tipoDocumento, TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyCatalog(cargo, CARGO, this.fileName);
        this.validationService.verifyCatalog(sucursal, SUCURSAL, this.fileName);

        if(subproducto != null && producto != null){
            String codigoProductoPadre = subproducto.getProductoPadre() != null ? subproducto.getProductoPadre().getCodigo() : "";
            this.validationService.verifyParent(producto.getCodigo(), codigoProductoPadre, PRODUCTO, SUBPRODUCTO, this.fileName);
        }
        if( producto != null && tipoProducto != null ) {
            this.validationService.verifyParent(producto.getTipo().getCodigo(), tipoProducto.getCodigo(), PRODUCTO, TIPO_PRODUCTO, this.fileName);
        }
        
        if(this.validationService.isLineError()){
            return null;
        }
        
        return createHistorialFacturacionConcepto(item, ciclo, sucursal, subproducto, tipoDocumento, cargo);
    }

    HistorialFacturacionConcepto createHistorialFacturacionConcepto (FacturacionConceptoDto item, Integer cicloFacturacion, Sucursal sucursal, Producto subproducto, TipoDocumento tipoDocumento, Cargo cargo){

        HistorialFacturacionConcepto historialFacturacionConcepto = new HistorialFacturacionConcepto();

        Optional<Cuenta> optionalCuenta = cuentaRepository.findById(Double.parseDouble(item.getCuentaNumero()));
        Cuenta cuenta = new Cuenta();
        if(!optionalCuenta.isPresent()){
            cuenta.setNumero(Double.parseDouble(item.getCuentaNumero()));
            cuenta.setEstrato(Integer.parseInt(item.getEstrato()));
            cuenta.setSucursal(sucursal);
            cuenta.setCicloFacturacion(Integer.parseInt(item.getCiclo()));
        }else{
            cuenta = optionalCuenta.get();
        }

        Optional<Cliente> optionalCliente = clienteRepository.findById(Double.parseDouble(item.getNumeroIdentificacion()));
        Cliente cliente = new Cliente();
        if(!optionalCliente.isPresent()){
            cliente.setNumeroIdentificacion(Double.parseDouble(item.getNumeroIdentificacion()));
            cliente.setTipoDocumento(tipoDocumento);
        }else{
            cliente = optionalCliente.get();
        }
        if(cliente.getCuenta() == null){
            cliente.setCuenta(cuenta);
        }
        
        Optional<Contrato> optionalContrato = contratoRepository.findById(item.getContratoNumero());
        Contrato contrato = new Contrato();
        if(!optionalContrato.isPresent()){
            contrato.setNumero(item.getContratoNumero());
            contrato.setSubProducto(subproducto);
            contrato.setFacturacion(Boolean.parseBoolean(item.getFacturacion()));
        }else{
            contrato = optionalContrato.get();
        }
        if(contrato.getCliente() == null){
            contrato.setCliente(cliente);
        }
        
        Optional<Servicio> optionalServicio = servicioRepository.findById(item.getServicioNumero());
        Servicio servicio = new Servicio();
        if(!optionalServicio.isPresent()){
            servicio.setNumero(item.getServicioNumero());
        }else{
            servicio = optionalServicio.get();
        }
        if(servicio.getContrato() == null){
            servicio.setContrato(contrato);
        }

        historialFacturacionConcepto.setCargo(cargo);
        historialFacturacionConcepto.setServicio(servicio);
        historialFacturacionConcepto.setFechaDocumento(LocalDate.parse(item.getFechaDocumento()));
        historialFacturacionConcepto.setFechaProceso(LocalDate.parse(item.getFechaProceso()));
        historialFacturacionConcepto.setDescripsion(item.getDescripsion());
        historialFacturacionConcepto.setSaldo(Double.parseDouble(item.getSaldo()));
        historialFacturacionConcepto.setTotalDocumento(Double.parseDouble(item.getTotalDocumento()));
        historialFacturacionConcepto.setValorFactura(Double.parseDouble(item.getValorFactura()));

        return historialFacturacionConcepto;
    }
    
}
