package com.colpatria.midas.processors;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.colpatria.midas.dto.CambioEstadoDto;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.CuentaRepository;
import com.colpatria.midas.repositories.ProductoRepository;
import com.colpatria.midas.repositories.ServicioRepository;
import com.colpatria.midas.repositories.TipoDocumentoRepository;
import com.colpatria.midas.repositories.TipoProductoRepository;
import com.colpatria.midas.repositories.UsuarioRepository;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.utils.NumberUtils;
import org.springframework.batch.item.ItemProcessor;

/**
 * Class that is use to transform, filter and verify the items that are being read in a specific batch process
 */
public class CambioEstadoItemProcessor implements  ItemProcessor<CambioEstadoDto, HistorialCambioEstado> {

    /**
     * Attribute that identifies the file that is being read in the batch process
     */
    private final String fileName;

	/**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

	/**
	 * add 'usuario' repository
	 */
	private final UsuarioRepository usuarioRepository;

	/**
	 * add 'TipoDocumento' repository
	 */
	private final TipoDocumentoRepository tipoDocumentoRepository;

	/**
	 * add 'producto' repository
	 */
    private final ProductoRepository productoRepository;

	/**
	 * add 'tipoProducto' repository
	 */
    private final TipoProductoRepository tipoProductoRepository;

	/**
	 * add 'contrato' repository
	 */
    private final ContratoRepository contratoRepository;

    /**
	 * add 'contrato' repository
	 */
    private final ClienteRepository clienteRepository;

    /**
	 * add 'cuenta' repository
	 */
    private final CuentaRepository cuentaRepository;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;

    /**
     * Constant for String 'Producto'
     */
    private static final String PRODUCTO = "Producto";

    /**
     * Constant for String 'Subproducto'
     */
    private static final String SUBPRODUCTO = "Subproducto";

    /**
     * Constant for String 'TipoProducto'
     */
    private static final String TIPO_PRODUCTO = "TipoProducto";

    /**
     * Constant for String 'TipoDocumento'
     */
    private static final String TIPO_DOCUMENTO = "TipoDocumento";

    /**
     * Constant for String 'FechaTransaccion'
     */
    private static final String FECHA_TRANSACCION = "FechaTransaccion";

    /**
     * Constant for String 'Contrato'
     */
    private static final String CONTRATO = "Contrato";

    /**
     * Constant for String 'NumeroDocumento'
     */
    private static final String NUMERO_DOCUMENTO = "NumeroDocumento";

    /**
     * Constant for String 'NumeroServicio'
     */
    private static final String SERVICIO_NUMERO = "NumeroServicio";

    /**
     * Constant for String 'FechaCambio'
     */
    private static final String FECHA_CAMBIO = "FechaCambio";

    /**
     * Constant for String 'EstadoAnterior'
     */
    private static final String ESTADO_ANTERIOR = "EstadoAnterior";

    /**
     * Constant for String 'EstadoNuevo'
     */
    private static final String ESTADO_NUEVO = "EstadoNuevo";

    /**
     * Constructor of the processor class
     * @param fileName identifies the file that is being read in the batch process
	 * @param servicioRepository dependency injection of servicioRepository
	 * @param usuarioRepository dependency injection of usuarioRepository
	 * @param productoRepository dependency injection of productoRepository
	 * @param tipoProductoRepository dependency injection of tipoProductoRepository
	 * @param contratoRepository dependency injection of contratoRepository
     * @param clienteRepository dependency injection of clienteRepository
     * @param cuentaRepository dependency injection of cuentaRepository
	 * @param validationService dependency injection of validationService
     */
    public CambioEstadoItemProcessor(
        String fileName,
        ValidationService validationService,
		ServicioRepository servicioRepository,
        TipoDocumentoRepository tipoDocumentoRepository,
        TipoProductoRepository tipoProductoRepository,
        ProductoRepository productoRepository,
        ContratoRepository contratoRepository,
		UsuarioRepository usuarioRepository,
        ClienteRepository clienteRepository,
        CuentaRepository cuentaRepository
    ) {
        this.fileName = fileName;
		this.servicioRepository = servicioRepository;
		this.usuarioRepository = usuarioRepository;
		this.tipoDocumentoRepository = tipoDocumentoRepository;
		this.productoRepository = productoRepository;
		this.tipoProductoRepository = tipoProductoRepository;
		this.contratoRepository = contratoRepository;
		this.validationService = validationService;
        this.clienteRepository = clienteRepository;
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Method that process the items that are being read from the file in the batch process
     * @param item to be process
     * @return the item that has been process
     * @throws Exception in case that a mandatory field in the item is null or empty
     */
    @Override
    public HistorialCambioEstado process(CambioEstadoDto item) throws Exception {

        this.validationService.increaseRegisterCounter();
        this.validationService.cleanLineErrors();

        this.validationService.verifyEmpty(item.getProductoCodigo(), PRODUCTO, this.fileName);
        this.validationService.verifyEmpty(item.getProductoTipoCodigo(), TIPO_PRODUCTO, this.fileName);
        this.validationService.verifyEmpty(item.getSubproductoCodigo(), SUBPRODUCTO, this.fileName);
        this.validationService.verifyEmpty(item.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyEmpty(item.getNumeroIdentificacion(), NUMERO_DOCUMENTO, this.fileName);
        this.validationService.verifyEmpty(item.getServicioNumero(), SERVICIO_NUMERO, this.fileName);
        this.validationService.verifyEmpty(item.getFechaTransaccion(), FECHA_CAMBIO, this.fileName);
        this.validationService.verifyEmpty(item.getServicioEstadoAnterior(), ESTADO_ANTERIOR, this.fileName);
        this.validationService.verifyEmpty(item.getServicioEstadoNuevo(), ESTADO_NUEVO, this.fileName);

        if(this.validationService.isLineError()){
            return null;
        }

        this.validationService.validateInteger(item.getProductoTipoCodigo(), TIPO_PRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getSubproductoCodigo(), SUBPRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getProductoCodigo(), PRODUCTO, this.fileName);
        this.validationService.validateInteger(item.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName);
        this.validationService.validateInteger(item.getNumeroIdentificacion(), NUMERO_DOCUMENTO, this.fileName);
        this.validationService.validateInteger(item.getServicioNumero(), SERVICIO_NUMERO, this.fileName);

        this.validationService.verifyZero(item.getTipoIdentificacion(), TIPO_DOCUMENTO, this.fileName);
        this.validationService.verifyZero(item.getNumeroIdentificacion(), NUMERO_DOCUMENTO, this.fileName);
        this.validationService.verifyZero(item.getServicioNumero(), SERVICIO_NUMERO, this.fileName);

        if(this.validationService.isLineError()){
            return null;
        }

        String productoTipoCodigo = item.getProductoTipoCodigo();
        String productoCodigo = item.getProductoCodigo();
        String subproductoCodigo = item.getSubproductoCodigo();
        String tipoIdentificacion = item.getTipoIdentificacion();

        TipoProducto tipoProducto = this.tipoProductoRepository.getTipoProductoByCodigo(productoTipoCodigo);
        Producto subproducto = productoRepository.getByCodigo(subproductoCodigo);
        Producto producto = this.productoRepository.getProductoByCodigo(productoCodigo);
        TipoDocumento tipoDocumento = this.tipoDocumentoRepository.getTipoDocumentoByCodigo(tipoIdentificacion);

        this.validationService.verifyCatalog(tipoProducto, TIPO_PRODUCTO, this.fileName);
        this.validationService.verifyCatalog(subproducto, SUBPRODUCTO, this.fileName);
        this.validationService.verifyCatalog(producto, PRODUCTO, this.fileName);
        this.validationService.verifyCatalog(tipoDocumento, TIPO_DOCUMENTO, this.fileName);
        this.validationService.validateDate(item.getFechaTransaccion(), FECHA_TRANSACCION, this.fileName);
        this.validationService.validateInteger(item.getContratoNumero(), CONTRATO, this.fileName);

        if(subproducto != null && producto != null){
            String codigoProductoPadre = subproducto.getProductoPadre() != null ? subproducto.getProductoPadre().getCodigo() : "";
            this.validationService.verifyParent(producto.getCodigo(), codigoProductoPadre, PRODUCTO, SUBPRODUCTO, this.fileName);
        }
        if( producto != null && tipoProducto != null ) {
            this.validationService.verifyParent(producto.getTipo().getCodigo(), tipoProducto.getCodigo(), PRODUCTO, TIPO_PRODUCTO, this.fileName);
        }

        if(this.validationService.isLineError()){
            return null;
        }

        return createHistorialCambioEstado(item, subproducto, tipoDocumento);
    }

    HistorialCambioEstado createHistorialCambioEstado (CambioEstadoDto item, Producto subproducto, TipoDocumento tipoDocumento){

        HistorialCambioEstado historialCambioEstado = new HistorialCambioEstado();

        Optional<Cuenta> optionalCuenta = cuentaRepository.findById(Double.parseDouble(item.getCuentaNumero()));
        Cuenta cuenta = new Cuenta();
        if(!optionalCuenta.isPresent()){
            cuenta.setNumero(Double.parseDouble(item.getCuentaNumero()));
        }else{
            cuenta = optionalCuenta.get();
        }

        Optional<Cliente> optionalCliente = clienteRepository.findById(Double.parseDouble(item.getNumeroIdentificacion()));
        Cliente cliente = new Cliente();
        if(!optionalCliente.isPresent()){
            cliente.setNumeroIdentificacion(Double.parseDouble(item.getNumeroIdentificacion()));
            cliente.setTipoDocumento(tipoDocumento);
            cliente.setCuenta(cuenta);
        }else{
            cliente = optionalCliente.get();
        }
        if(cliente.getCuenta() == null){
            cliente.setCuenta(cuenta);
        }

        String contratoNumero = item.getContratoNumero();
        Optional<Contrato> optionalContrato = contratoRepository.findById(contratoNumero);
        Contrato contrato = new Contrato();
        if(!optionalContrato.isPresent()){
            contrato.setNumero(contratoNumero);
            contrato.setSubProducto(subproducto);
        }else{
            contrato = optionalContrato.get();
        }
        if(contrato.getCliente() == null){
            contrato.setCliente(cliente);
        }

        Optional<Servicio> optionalServicio = servicioRepository.findById(item.getServicioNumero());
        Servicio servicio = new Servicio();
        if(!optionalServicio.isPresent()){
            servicio.setNumero(item.getServicioNumero());
        }else{
            servicio = optionalServicio.get();
        }
        if(servicio.getContrato() == null){
            servicio.setContrato(contrato);
        }

        Optional<Usuario> optionalUsuario = usuarioRepository.findById(item.getUsuarioID());
        Usuario usuario = new Usuario();
        if(!optionalUsuario.isPresent()){
            usuario.setNombres(item.getUsuarioNombre());
            usuario.setId(item.getUsuarioID());
            usuario.setArea(item.getUsuarioArea());
        }else{
            usuario = optionalUsuario.get();
        }

        historialCambioEstado.setUsuario(usuario);
        historialCambioEstado.setServicio(servicio);
        historialCambioEstado.setObservacion(item.getObservacion());
        historialCambioEstado.setServicioEstadoAnterior(item.getServicioEstadoAnterior());
        historialCambioEstado.setServicioEstadoNuevo(item.getServicioEstadoNuevo());
        historialCambioEstado.setFecha(LocalDate.parse(item.getFechaTransaccion()));

        return historialCambioEstado;
    }
    
}
