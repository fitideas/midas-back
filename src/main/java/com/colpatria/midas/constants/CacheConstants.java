package com.colpatria.midas.constants;

/**
 * cache constants with name of storage.
 */
public final class CacheConstants {

    /**
     * cache storage name producto.
     */
    public static final String producto = "Producto";

    /**
     * cache storage name subproducto.
     */
    public static final String subProducto = "SubProducto";

    /**
     * cache storage name tipo identificacion.
     */
    public static final String tipoIdentificacion = "TipoIdentificacion";

    /**
     * cache storage name sucursal.
     */
    public static final String sucursal = "Sucursal";

    /**
     * cache storage name ciclo.
     */
    public static final String ciclo = "Ciclo";

    /**
     * cache storage name categoria.
     */
    public static final String categoria = "Categoria";

    /**
     * cache storage tipo consumo.
     */
    public static final String tipoConsumo = "TipoConsumo";

    /**
     * cache storage name clase servicio.
     */
    public static final String claseServicio = "ClaseServicio";

    /**
     * cache storage name tipo tarjeta.
     */
    public static final String tipoTarjeta = "TipoTarjeta";

    /**
     * cache storage name forma pago.
     */
    public static final String formaPago = "FormaPago";

    /**
     * cache storage name tipo negociacion.
     */
    public static final String tipoNegociacion = "TipoNegociacion";

    /**
     * cache storage name estado negociacion
     */
    public static final String estadoNegociacion = "estadoNegociacion";

    /**
     * cache storage name tipo producto.
     */
    public  static final String tipoProducto = "TipoProducto";

    /**
     * cache storage name departamento.
     */
    public  static final String departamento = "Departamento";

    /**
     * cache storage name municipio.
     */
    public  static final String municipio = "Municipio";

    /**
     * cache storage name localidad.
     */
    public  static final String localidad = "Localidad";

    /**
     * cache storage name cargo.
     */
    public  static final String cargo = "Cargo";

    /**
     * cache storage name cliente
     */
    public  static final String cliente = "Cliente";

    /**
     * cache storage name contrato
     */
    public  static final String contrato = "Contrato";

    /**
     * cache storage name contrato
     */
    public  static final String cuenta = "Cuenta";

    /**
     * cache storage name servicio
     */
    public static final String service = "Servicio";

    /**
     * cache storage name configuration
     */
    public static final String configuration = "Configuration";
}