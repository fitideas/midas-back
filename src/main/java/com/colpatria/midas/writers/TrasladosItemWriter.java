package com.colpatria.midas.writers;

import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class TrasladosItemWriter implements ItemWriter<HistorialTraslado> {


    private final ValidationService validationService;

    private final HistorialTrasladoRepository historialTrasladoRepository;

    private CuentaRepository cuentaRepository;

    private ContratoRepository contratoRepository;

    private ClienteRepository clienteRepository;

    private UsuarioRepository usuarioRepository;

    public TrasladosItemWriter(ValidationService validationService, HistorialTrasladoRepository historialTrasladoRepository) {
        this.validationService = validationService;
        this.historialTrasladoRepository = historialTrasladoRepository;
    }

    @Override
    public void write(List<? extends HistorialTraslado> list) throws Exception {
        if (!this.validationService.getHasError()) {
            for(HistorialTraslado item : list){
                item.setCuentaAnterior(this.cuentaRepository.save(item.getCuentaAnterior()));
                item.setCuentaActual(this.cuentaRepository.save(item.getCuentaActual()));
                item.getContrato().setCliente(this.clienteRepository.save(item.getContrato().getCliente()));
                item.setContrato(this.contratoRepository.save(item.getContrato()));
                item.setUsuario(this.usuarioRepository.save(item.getUsuario()));
                this.historialTrasladoRepository.saveAndFlush(item);
            }
        }

    }

    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    public void setContratoRepository(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    public void setUsuarioRepository(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }
}
