package com.colpatria.midas.writers;

import java.util.List;

import com.colpatria.midas.model.HistorialPeriodoGracia;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

public class PeriodoGraciaItemWriter implements ItemWriter<HistorialPeriodoGracia>{

    /**
     * Logger in processor
     */
    private static final Logger logger = LoggerFactory.getLogger(PeriodoGraciaItemWriter.class);

    /**
     * Repository interface for 'historialCambioEstadoRepository'
     */
    private final HistorialPeriodoGraciaRepository historialPeriodoGraciaRepository;

    /**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

	/**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

	/**
	 * add 'contrato' repository
	 */
	private final ContratoRepository contratoRepository;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;

    /**
	 * add 'Usuario' repository
	 */
    private final UsuarioRepository usuarioRepository;

    /**
	 * add 'Usuario' repository
	 */
    private final NegociacionRepository negociacionRepository;

    /**
	 * add 'Ceunta' repository
	 */
    private final CuentaRepository cuentaRepository;

    /**
     * Constructor of the item writer
     * @param historialPeriodoGraciaRepository interface that saves 'historialPeriodoGraciaRepository' to data base
     * @param validationService is the service that validates fields in the 'validationService'
     * @param clienteRepository is the service that validates fields in the 'clienteRepository'
     * @param contratoRepository is the service that validates fields in the 'contratoRepository'
     * @param servicioRepository is the service that validates fields in the 'servicioRepository'
     * @param usuarioRepository is the service that validates fields in the 'usuarioRepository'
     * @param cuentaRepository is the service that validates fields in the 'cuentaRepository'
     * @param negociacionRepository is the service that validates fields in the 'negociacionRepository'
     */
    public PeriodoGraciaItemWriter(HistorialPeriodoGraciaRepository historialPeriodoGraciaRepository, ClienteRepository clienteRepository, ContratoRepository contratoRepository, ServicioRepository servicioRepository, UsuarioRepository usuarioRepository,  CuentaRepository cuentaRepository, NegociacionRepository negociacionRepository, ValidationService validationService) {
        this.historialPeriodoGraciaRepository = historialPeriodoGraciaRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository =  contratoRepository;
        this.servicioRepository = servicioRepository;
        this.usuarioRepository = usuarioRepository;
        this.cuentaRepository = cuentaRepository;
        this.negociacionRepository = negociacionRepository;
        this.validationService = validationService;
    }

    /**
     * Method that writes to the database
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
     */
    @Override
    public void write(List<? extends HistorialPeriodoGracia> items) throws Exception {
        logger.info("Start write process");
        if (!this.validationService.getHasError()){
            for(HistorialPeriodoGracia item : items){
                item.getServicio().getContrato().getCliente().setCuenta(this.cuentaRepository.save(item.getServicio().getContrato().getCliente().getCuenta()));
                item.getServicio().getContrato().setCliente(this.clienteRepository.save(item.getServicio().getContrato().getCliente()));
                item.getServicio().setContrato(this.contratoRepository.save(item.getServicio().getContrato()));
                item.setServicio(this.servicioRepository.save(item.getServicio()));
                item.setUsuario(this.usuarioRepository.save(item.getUsuario()));
                item.setNegociacion(this.negociacionRepository.save(item.getNegociacion()));
                this.historialPeriodoGraciaRepository.saveAndFlush(item);
            }
        }  
    }
    
}
