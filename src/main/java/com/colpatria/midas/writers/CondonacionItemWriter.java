package com.colpatria.midas.writers;

import com.colpatria.midas.model.HistorialCondonacion;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;


public class CondonacionItemWriter implements ItemWriter<HistorialCondonacion> {

    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(CondonacionItemWriter.class);

    /**
     * Repository para Condonacion
     */
    private HistorialCondonacionRepository historialCondonacionRepository;

    /**
     * Add client repository
     */
    private ClienteRepository clienteRepository;

    /**
     * Add Negociacion repository
     */
    private NegociacionRepository negociacionRepository;

    /**
     * add contrato repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * add cuenta repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * add servicio repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * add servicio repository
     */
    private final TipoNegociacionRepository tipoNegociacionRepository;

    /**
     * Validation service
     */
    private ValidationService validationService;

    /**
     * @param historialCondonacionRepository
     * @param clienteRepository The object that refers to the Entity Cliente Repository
     * @param historialCondonacionRepository The object that refers to the Entity Condonacion Repository
     * @param cuentaRepository The object that refers to the Entity cuenta Repository
     * @param contratoRepository The object that refers to the Entity contrato Repository
     * @param servicioRepository  The object that refers to the Entity Servicio Repository
     * @param negociacionRepository The object that refers to the Entity Negociacion Repository
     * @param tipoNegociacionRepository The object that refers to the Entity TipoNegociacion Repository
     * @param validationService validation service
     */
    public CondonacionItemWriter(HistorialCondonacionRepository historialCondonacionRepository,
                                 CuentaRepository cuentaRepository,
                                 ClienteRepository clienteRepository,
                                 ContratoRepository contratoRepository,
                                 ServicioRepository servicioRepository,
                                 NegociacionRepository negociacionRepository,
                                 TipoNegociacionRepository tipoNegociacionRepository,
                                 ValidationService validationService) {
        this.historialCondonacionRepository = historialCondonacionRepository;
        this.clienteRepository = clienteRepository;
        this.cuentaRepository = cuentaRepository;
        this.contratoRepository = contratoRepository;
        this.servicioRepository = servicioRepository;
        this.negociacionRepository = negociacionRepository;
        this.tipoNegociacionRepository = tipoNegociacionRepository;
        this.validationService = validationService;
    }

    /**
     * @param items with data charged in file
     * @throws Exception exceptions
     */
    @Override
    public void write(List<? extends HistorialCondonacion> items) throws Exception {

        if (!this.validationService.getHasError()) {
            this.historialCondonacionRepository.saveAllAndFlush(items);
        }
    }
}
