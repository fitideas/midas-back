package com.colpatria.midas.writers;

import com.colpatria.midas.model.DeudaCargo;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.CuentaRepository;
import com.colpatria.midas.repositories.DeudaCargoRepository;
import com.colpatria.midas.repositories.ServicioRepository;
import com.colpatria.midas.services.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class DeudaCargoItemWriter implements ItemWriter<DeudaCargo> {

    private static final Logger logger = LoggerFactory.getLogger(DeudaCargoItemWriter.class);

    /**
     * add 'servicio' repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * add 'deudacargo' repository
     */
    private final DeudaCargoRepository deudaCargoRepository;

    /**
     * add 'cuenta' repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * add validation service
     */
    private final ValidationService validationService;

    private final ContratoRepository contratoRepository;

    public DeudaCargoItemWriter(ServicioRepository servicioRepository, DeudaCargoRepository deudaCargoRepository, CuentaRepository cuentaRepository, ValidationService validationService, ContratoRepository contratoRepository) {
        this.servicioRepository = servicioRepository;
        this.deudaCargoRepository = deudaCargoRepository;
        this.cuentaRepository = cuentaRepository;
        this.validationService = validationService;
        this.contratoRepository = contratoRepository;
    }

    @Override
    public void write(List<? extends DeudaCargo> items) throws Exception {
        logger.info("Start write process");
        if (!this.validationService.getHasError()) {
            this.deudaCargoRepository.saveAll(items);
        }
    }
}
