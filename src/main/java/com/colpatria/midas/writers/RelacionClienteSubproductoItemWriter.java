package com.colpatria.midas.writers;

/*
*
* Libraries
*
*/

import java.util.List;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.Cuenta;
import com.colpatria.midas.model.HistorialRelacionClienteSubproducto;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.CuentaRepository;
import com.colpatria.midas.repositories.HistorialRelacionClienteSubproductoRepository;
import com.colpatria.midas.services.ValidationService;
import org.springframework.batch.item.ItemWriter;

/**
 * Class that writes the items that were read and process into the database
*/
public class RelacionClienteSubproductoItemWriter implements ItemWriter<HistorialRelacionClienteSubproducto> {

    /**
     * validation service
    */
    private final ValidationService validationService;

    /**
     * Contract repository to read and write to the database
     */
    private ContratoRepository contratoRepository;

    /**
     * Client repository to read and write to the database
     */
    private ClienteRepository clienteRepository;

    /**
     * Banck account repository to read and write to the database
    */
    private CuentaRepository cuentaRepository;

    /**
     * History repository to read and write to the database
    */
    private HistorialRelacionClienteSubproductoRepository historialRelacionClienteSubproductoRepository;

    /**
     * Constructor of the item writer
     * @param validationService is the validation service
    */
    public RelacionClienteSubproductoItemWriter( ValidationService validationService ) {
        this.validationService = validationService;
    }

    /**
     * Method that writes to the database
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
    */
    @Override
    public void write( List<? extends HistorialRelacionClienteSubproducto> items ) throws Exception {
        // Variables
        Cuenta   cuenta;
        Cliente  cliente;
        Contrato contrato;
        // Code 
        if( !this.validationService.getHasError() ) {
            for( HistorialRelacionClienteSubproducto item : items ) {
                // History
                cuenta  = this.cuentaRepository.save( item.getContrato().getCliente().getCuenta() );
                cliente = item.getContrato().getCliente();
                cliente.setCuenta( cuenta );
                cliente  = this.clienteRepository.save( item.getContrato().getCliente() );
                contrato = item.getContrato();
                contrato.setCliente( cliente );
                contrato = this.contratoRepository.save( contrato );
                item.setContrato( contrato );
                this.historialRelacionClienteSubproductoRepository.save( item );
            }            
        }        
    }

    /**
     * Set the account repository
     * @param cuentaRepository is the repository object
    */
    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Set the client repository
     *
     * @param clienteRepository is the repository object
     */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Set the contract repository
     *
     * @param contratoRepository is the repository object
     */
    public void setContratoRepository(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    /**
     * Set the repository history movement balance
     * @param historialRelacionClienteSubproductoRepository is the repository object
    */
    public void setHistorialRelacionClienteSubproductoRepository(
            HistorialRelacionClienteSubproductoRepository historialRelacionClienteSubproductoRepository) {
        this.historialRelacionClienteSubproductoRepository = historialRelacionClienteSubproductoRepository;
    }    
    
}
