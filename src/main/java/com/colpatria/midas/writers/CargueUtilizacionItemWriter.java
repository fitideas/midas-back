package com.colpatria.midas.writers;

import java.util.List;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

/**
 * Class that writes the items that were read and process into the database
 */
public class CargueUtilizacionItemWriter implements ItemWriter<HistorialCargueUtilizacion> {

    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(CargueUtilizacionItemWriter.class);

    /**
     * Repository interface for 'historialCargueUtilizacionRepository'
     */
    private final HistorialCargueUtilizacionRepository historialCargueUtilizacionRepository;

    /**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

    /**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * Constructor of the item writer
     * @param historialCargueUtilizacionRepository interface that saves 'historialCargueUtilizacionRepository' to data base
     * @param validationService is the service that validates fields in the 'historialCargueUtilizacionRepository'
     * @param clienteRepository is the service that validates fields in the 'clienteRepository'
     * @param servicioRepository is the service that validates fields in the 'servicioRepository'
     */
    public CargueUtilizacionItemWriter(HistorialCargueUtilizacionRepository historialCargueUtilizacionRepository, ClienteRepository clienteRepository, ServicioRepository servicioRepository, ValidationService validationService) {
        this.historialCargueUtilizacionRepository = historialCargueUtilizacionRepository;
        this.clienteRepository = clienteRepository;
        this.servicioRepository = servicioRepository;
        this.validationService = validationService;
    }

    /**
     * Method that writes to the database
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
     */
    @Override
    public void write(List<? extends HistorialCargueUtilizacion> items) throws Exception {
        logger.info("Start write process");
        if (!this.validationService.getHasError()){
            for(HistorialCargueUtilizacion item : items){
                item.setCliente(this.clienteRepository.save(item.getCliente()));
                item.setServicio(this.servicioRepository.save(item.getServicio()));
                this.historialCargueUtilizacionRepository.saveAndFlush(item);
            }
        } 
    }
}