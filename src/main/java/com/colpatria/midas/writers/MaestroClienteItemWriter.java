package com.colpatria.midas.writers;

/*
 *
 * Libraries
 *
*/

import java.util.List;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import org.springframework.batch.item.ItemWriter;

/*
 *
 * Class
 *
*/

public class MaestroClienteItemWriter implements ItemWriter<HistorialMaestroCliente> {
    
    /**
     * History repository to read and write to the database
    */
    private HistorialMaestroClienteRepository historialMaestroClienteRepository;

    /**
     * Client repository to read and write to the database
    */
    private ClienteRepository clienteRepository;

    /**
     * Banck account repository to read and write to the database
    */
    private CuentaRepository cuentaRepository;

    /**
     * Service repository to read and write to the database
    */
    private ServicioRepository servicioRepository;

    /**
     * Neighborhood repository to read and write to the database
    */
    private BarrioRepository barrioRepository;

    /**
     *  Reading repository to read and write to the database
    */
    private LecturaRepository lecturaRepository;

    /**
     *  Delivery repository to read and write to the database
    */
    private RepartoRepository repartoRepository;

    /**
     * validation service
    */
    private final ValidationService validationService;

    /*
     *
     * Métodos
     *
    */

    /**
     * Constructor of the item writer
     * @param validationService is the validation service
    */
    public MaestroClienteItemWriter( ValidationService validationService ) {
        this.validationService = validationService;
    }

    /**
     * Method that writes to the database
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
    */
    @Override
    public void write(List<? extends HistorialMaestroCliente> items ) throws Exception {
        // Variables
        Barrio   barrio;
        Lectura  lectura;
        Cuenta   cuenta;
        Cliente  cliente;
        Servicio servicio;
        Reparto  reparto;
        // Code 
        if( !this.validationService.getHasError() ) {
            for( HistorialMaestroCliente historialMaestroCliente : items ) {
                // History
                cuenta  = this.cuentaRepository.save( historialMaestroCliente.getCliente().getCuenta() );
                cliente = historialMaestroCliente.getCliente();
                cliente.setCuenta( cuenta );
                cliente  = this.clienteRepository.save( cliente );
                servicio = this.servicioRepository.save( historialMaestroCliente.getServicio() );
                barrio   = this.barrioRepository.save( historialMaestroCliente.getLectura().getBarrio() );
                lectura  = historialMaestroCliente.getLectura();
                lectura.setBarrio( barrio );
                lectura = this.lecturaRepository.save( lectura );
                reparto = this.repartoRepository.save( historialMaestroCliente.getReparto() );
                historialMaestroCliente.setCliente( cliente );
                historialMaestroCliente.setServicio( servicio );
                historialMaestroCliente.setLectura( lectura );
                historialMaestroCliente.setReparto( reparto );
                this.historialMaestroClienteRepository.save( historialMaestroCliente );                
            }            
        }        
    }

    /**
     * Set the repository service history
     * @param servicioRepository is the repository object
    */
    public void setServicioRepository( ServicioRepository servicioRepository ) {
        this.servicioRepository = servicioRepository;
    }

     /**
     * Set the account repository
     * @param cuentaRepository is the repository object
    */
    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Set the client repository
     * @param clienteRepository is the repository object
    */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Set the history repository
     * @param historialMaestroClienteRepository is the repository object
    */
    public void setHistorialMaestroClienteRepository(HistorialMaestroClienteRepository historialMaestroClienteRepository) {
        this.historialMaestroClienteRepository = historialMaestroClienteRepository;
    }

    /**
     * Set the neighborhood repository
     * @param barrioRepository is the repository object
    */
    public void setBarrioRepository(BarrioRepository barrioRepository) {
        this.barrioRepository = barrioRepository;
    }

    /**
     * Set the reading repository
     * @param lecturaRepository is the repository object
    */
    public void setLecturaRepository(LecturaRepository lecturaRepository) {
        this.lecturaRepository = lecturaRepository;
    }

    /**
     * Set the delivery repository
     * @param repartoRepository is the repository object
    */
    public void setRepartoRepository(RepartoRepository repartoRepository) {
        this.repartoRepository = repartoRepository;
    }
    
}
