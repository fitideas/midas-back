package com.colpatria.midas.writers;

import java.util.List;

import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import org.springframework.batch.item.ItemWriter;


public class CarteraTotalItemWriter implements ItemWriter<HistorialCartera> {

    /**
     * Repository objects to read and write to the database
     */
    private HistorialCarteraTotalRepository historialCarteraTotalRepository;
    private CuentaRepository cuentaRepository;
    private ClienteRepository clienteRepository;
    private ContratoRepository contratoRepository;
    private ServicioRepository servicioRepository;

    /**
     * validation service
     */
    private final ValidationService validationService;

    /*
     *
     * Métodos
     *
     */

    /**
     * Method that writes to the database
     *
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
     */
    @Override
    public void write(List<? extends HistorialCartera> items) throws Exception {
        // Variables
        Contrato contrato;
        Cuenta cuenta;
        Cliente cliente;
        Servicio servicio;
        // Code
        if (!this.validationService.getHasError()) {
            for (HistorialCartera historialCartera : items) {
                // History
                cliente = historialCartera.getServicio().getContrato().getCliente();
                cuenta = this.cuentaRepository.save(historialCartera.getServicio().getContrato().getCliente().getCuenta());
                cliente.setCuenta( cuenta );
                cliente = this.clienteRepository.save(cliente);
                contrato = historialCartera.getServicio().getContrato();
                contrato.setCliente(cliente);
                contrato = this.contratoRepository.save(contrato);
                servicio = historialCartera.getServicio();
                servicio.setContrato(contrato);
                servicio = this.servicioRepository.save(servicio);
                historialCartera.setServicio(servicio);

                HistorialCarteraTotal total = new HistorialCarteraTotal(historialCartera);
                this.historialCarteraTotalRepository.save(total);

            }
        }
    }

    /**
     * Constructor of the item writer
     *
     * @param validationService is the validation service
     */
    public CarteraTotalItemWriter(ValidationService validationService) {
        this.validationService = validationService;
    }

    /**
     * Set the repository history
     *
     * @param historialCarteraTotalRepository is the repository object
     */
    public void setHistorialCarteraRepository(HistorialCarteraTotalRepository historialCarteraTotalRepository) {
        this.historialCarteraTotalRepository = historialCarteraTotalRepository;
    }

    /**
     * Set the repository service history
     *
     * @param servicioRepository is the repository object
     */
    public void setServicioRepository(ServicioRepository servicioRepository) {
        this.servicioRepository = servicioRepository;
    }

    /**
     * Set the contract repository
     *
     * @param contratoRepository is the repository object
     */
    public void setContratoRepository(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    /**
     * Set the account repository
     *
     * @param cuentaRepository is the repository object
     */
    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Set the client repository
     *
     * @param clienteRepository is the repository object
     */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

}
