package com.colpatria.midas.writers;

/*
 *
 * Librerías
 *
*/
import com.colpatria.midas.model.HistorialRefinanciacion;
import com.colpatria.midas.model.Negociacion;
import com.colpatria.midas.model.Servicio;
import com.colpatria.midas.model.Usuario;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import java.util.List;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.Cuenta;

/*
 *
 * Clase
 *
*/

public class RefinanciacionItemWriter implements ItemWriter<HistorialRefinanciacion> {

    /**
     * Message constants
    */
    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(RefinanciacionItemWriter.class);


    /**
     * Repository para HistorialRefinanciacion
    */
    private HistorialRefinanciacionRepository historialRefinanciacionRepository;

    /**
     * Cuenta Repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * Cliente Repository
     */
    private final ClienteRepository clienteRepository;

    /**
     * Contrato Repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * Servicio Repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * Negociacion Repository
     */
    private final NegociacionRepository negociacionRepository;

    /**
     * Add Usuario repository
     */
    private final UsuarioRepository usuarioRepository;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /*
     *
     * Métodos
     *
    */

    /**
     * Constructor of the item writer
     * @param historialRefinanciacionRepository references the historialRefinanciacionRepository
     * @param clienteRepository references the clienteRepository
     * @param contratoRepository references the contratoRepository
     * @param cuentaRepository references the cuentaRepository
     * @param negociacionRepository references the negociacionRepository
     * @param servicioRepository references the servicioRepository
     * @param validationService references the validationService
     * @param usuarioRepository identifies de usuarioRepository
    */
    public RefinanciacionItemWriter( HistorialRefinanciacionRepository historialRefinanciacionRepository,
                                     ValidationService validationService,
                                     CuentaRepository cuentaRepository,
                                     ClienteRepository clienteRepository,
                                     ContratoRepository contratoRepository,
                                     ServicioRepository servicioRepository,
                                     NegociacionRepository negociacionRepository,
                                     UsuarioRepository usuarioRepository
                                     ) {
        this.historialRefinanciacionRepository = historialRefinanciacionRepository;
        this.cuentaRepository = cuentaRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository = contratoRepository;
        this.servicioRepository = servicioRepository;
        this.negociacionRepository = negociacionRepository;
        this.validationService = validationService;
        this.usuarioRepository = usuarioRepository;
    }

    /**
     * Method that writes to the database
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
    */
    @Override
    public void write( List<? extends HistorialRefinanciacion> items ) throws Exception {
        // Variables
        Contrato    contrato;
        Cuenta      cuenta;
        Cliente     cliente;
        Servicio    servicio;
        Negociacion negociacion;
        Usuario     usuario;
        // Code 
        if( !this.validationService.getHasError() ){
            for( HistorialRefinanciacion item : items ){
                cuenta  = this.cuentaRepository.save( item.getServicio().getContrato().getCliente().getCuenta() );
                cliente = item.getServicio().getContrato().getCliente();
                cliente.setCuenta( cuenta );
                cliente  = this.clienteRepository.save( cliente );
                contrato = item.getServicio().getContrato();
                contrato.setCliente( cliente );
                contrato = this.contratoRepository.save( contrato );
                servicio = item.getServicio();
                servicio.setContrato( contrato );
                servicio    = this.servicioRepository.save( servicio );
                negociacion = this.negociacionRepository.save( item.getNegociacion() );
                usuario     = this.usuarioRepository.save( item.getUsuario() );
                item.setServicio( servicio );
                item.setNegociacion( negociacion );
                item.setUsuario( usuario) ;
                this.historialRefinanciacionRepository.save( item );
            }
        }
    }
}