package com.colpatria.midas.writers;

import java.util.List;
import com.colpatria.midas.model.HistorialAnulacionPago;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.HistorialAnulacionPagoRepository;
import com.colpatria.midas.repositories.ServicioRepository;
import com.colpatria.midas.services.ValidationService;
import org.springframework.batch.item.ItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that writes the items that were read and process into the database
 */
public class AnulacionPagoItemWriter implements ItemWriter<HistorialAnulacionPago>{

    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(AnulacionPagoItemWriter.class);

    /**
     * Repository interface for 'historialAnulacionPagoRepository'
     */
    private final HistorialAnulacionPagoRepository historialAnulacionPagoRepository;

    /**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

    /**
	 * add 'contrato' repository
	 */
	private final ContratoRepository contratoRepository;

    /**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * Constructor of the item writer
     * @param historialAnulacionPagoRepository interface that saves 'historialAnulacionPagoRepository' to data base
     * @param validationService is the service that validates fields in the 'historialAnulacionPago'
     * @param clienteRepository is the service that validates fields in the 'clienteRepository'
     * @param contratoRepository is the service that validates fields in the 'contratoRepository'
     * @param servicioRepository is the service that validates fields in the 'servicioRepository'
     */
    public AnulacionPagoItemWriter(HistorialAnulacionPagoRepository historialAnulacionPagoRepository, ClienteRepository clienteRepository, ContratoRepository contratoRepository, ServicioRepository servicioRepository, ValidationService validationService) {
        this.historialAnulacionPagoRepository = historialAnulacionPagoRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository =  contratoRepository;
        this.servicioRepository = servicioRepository;
        this.validationService = validationService;
    }

    /**
     * Method that writes to the database
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
     */
    @Override
    public void write(List<? extends HistorialAnulacionPago> items) throws Exception {
        logger.info("Start write process");
        if (!this.validationService.getHasError()){
            for(HistorialAnulacionPago item : items){
                item.getServicio().getContrato().setCliente(this.clienteRepository.save(item.getServicio().getContrato().getCliente()));
                item.getServicio().setContrato(this.contratoRepository.save(item.getServicio().getContrato()));
                item.setServicio(this.servicioRepository.save(item.getServicio()));
                this.historialAnulacionPagoRepository.saveAndFlush(item);
            }
        } 
    }

}
