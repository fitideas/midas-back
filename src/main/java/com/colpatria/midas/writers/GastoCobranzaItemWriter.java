package com.colpatria.midas.writers;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.colpatria.midas.dto.GastoCobranzaDto;

import org.apache.poi.ss.formula.functions.Count;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

@Component
public class GastoCobranzaItemWriter implements ItemStream, ItemWriter<GastoCobranzaDto> {

    /**
	 * Constant that contains the file writers
	 */
    private final Map<String, FlatFileItemWriter<GastoCobranzaDto>> writers = new HashMap<>();

    /**
	 * ExecutionContext of the writing process
	 */
    private ExecutionContext executionContext;

    /**
	 * Path where the files will be save
	 */
    private String reportPath;
    
    /**
     * Constructor of the item writer
     * @param reportPath where the files will be save
     */
    public GastoCobranzaItemWriter(String reportPath){
        this.reportPath = reportPath;
    }

    /**
     * @param executionContext of the writing process
     */
    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        this.executionContext = executionContext;
    }

    /**
     * @param executionContext of the writing process
     */
    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
    }

    /**
     * If any resources are needed for the stream to operate they need to be destroyed here
     */
    @Override
    public void close() throws ItemStreamException {
        for(FlatFileItemWriter<GastoCobranzaDto> w : writers.values()){
            w.close();
        }
    }

    /**
     * Creates and saves the writers dependending on the item to be write
     * @param item to be writen
     * @return the writer instance that will be use to wirte to the file
     */
    public FlatFileItemWriter<GastoCobranzaDto> getFlatFileItemWriter(GastoCobranzaDto item) {
        String fileNameKey = item.getFileToBeWriten();
        String generationDay = LocalDate.now().toString();
        FlatFileItemWriter<GastoCobranzaDto> itemWriter = writers.get(fileNameKey);

        if (itemWriter == null) {

            itemWriter = new FlatFileItemWriter<>();

            try {

                BeanWrapperFieldExtractor<GastoCobranzaDto> fieldExtractor = new BeanWrapperFieldExtractor<>();
                fieldExtractor.setNames(new String[] {"cedula", "cuentaNumero", "subproducto", "ciclo", "diasMora", "capital", "capitalSinFacturar", "intereses", "interesMora", "saldo", "saldoNoFacturado", "cuotaManejo", "gastoCobranza", "fechaReporte", "edad", "fechaGestion", "userGestion", "observacionesGestion", "efectividadGestion", "fechaEnvioCarta", "fechaLiquidacionGasto", "consecutivo", "fechaConsecutivo", "servicioAsignado", "causal"});
                fieldExtractor.afterPropertiesSet();

                DelimitedLineAggregator<GastoCobranzaDto> lineAggregator = new DelimitedLineAggregator<>();
                lineAggregator.setDelimiter("|");
                lineAggregator.setFieldExtractor(fieldExtractor);

                FileSystemResource resource = new FileSystemResource(this.reportPath+fileNameKey+generationDay+".txt");

                itemWriter.setName("gastoCobranzaItemWriter_"+fileNameKey);
                itemWriter.setResource(resource);
                itemWriter.setLineAggregator(lineAggregator);
                itemWriter.setHeaderCallback(writer -> writer.write("cedula|numero_cli|SUBPRODUCTO|ciclo|dias_mo|capital|cap_no_afe|intereses|interes_mo|saldo|sal_no_afe|cmanejo|gcobran|fec_repo|edad|fecha_gest|user_gest|resul_gest|contac_ges|carta_env|fec_envliq|consecutiv|fec_consec|serv_carga|causa_dele"));
                itemWriter.afterPropertiesSet();
                itemWriter.open(executionContext);

            } catch (Exception e) {
                e.printStackTrace();
            }
            writers.put(fileNameKey, itemWriter);
        }
        return itemWriter;
    }

    /**
     * Writes to the files
     * @param item to be writen
     */
    @Override
    public void write(List<? extends GastoCobranzaDto> items) throws Exception {
        for (GastoCobranzaDto item : items) {
            FlatFileItemWriter<GastoCobranzaDto> writer = getFlatFileItemWriter(item);
            writer.write(Arrays.asList(item));
        }
    }

    

}
