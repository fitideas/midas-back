package com.colpatria.midas.writers;

/*
 *
 * Librerías
 *
*/

import java.util.List;
import java.util.Optional;

import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import java.time.LocalDate;

/*
 *
 * Clase
 *
*/

public class CarteraLightItemWriter implements ItemWriter<HistorialCartera> {

    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(CarteraLightItemWriter.class);

    /**
     * Repository objects to read and write to the database
    */
    private HistorialCarteraLightRepository   historialCarteraLightRepository;
    private HistorialCarteraResumenRepository historialCarteraResumenRepository;
    private CuentaRepository                  cuentaRepository;
    private ClienteRepository                 clienteRepository;
    private ContratoRepository                contratoRepository;
    private ServicioRepository                servicioRepository;
    private GastoCobranzaRepository           gastoCobranzaRepository;

    /**
     * validation service
     */
    private final ValidationService validationService;

    /*
     *
     * Métodos
     *
    */

    /**
     * Constructor of the item writer
     * @param validationService is the validation service
    */
    public CarteraLightItemWriter(ValidationService validationService, GastoCobranzaRepository gastoCobranzaRepository) {
        this.validationService = validationService;
        this.gastoCobranzaRepository = gastoCobranzaRepository;
    }

    /**
     * Method that writes to the database
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
    */
    @Override
    public void write(List<? extends HistorialCartera> items ) throws Exception {
        // Variables
        Contrato contrato;
        Cuenta   cuenta;
        Cliente  cliente;
        Servicio servicio;
        // Code 
        if( !this.validationService.getHasError() ) {
            for( HistorialCartera historialCartera : items ) {
                // History
                cliente = historialCartera.getServicio().getContrato().getCliente();
                cuenta = this.cuentaRepository.save(historialCartera.getServicio().getContrato().getCliente().getCuenta());
                cliente.setCuenta( cuenta );
                cliente  = this.clienteRepository.save( cliente );
                contrato = historialCartera.getServicio().getContrato();
                contrato.setCliente( cliente );
                contrato = this.contratoRepository.save( contrato );
                servicio = historialCartera.getServicio();
                servicio.setContrato( contrato );
                servicio = this.servicioRepository.save( servicio );
                historialCartera.setServicio( servicio );

                HistorialCarteraLight light = new HistorialCarteraLight(historialCartera);
                this.historialCarteraLightRepository.save( light );
                // History summary
                this.createHistorialCarteraResumen( historialCartera );
            }            
        }        
    }

    /**
     * Method that writes the story summary to the database
     * @param historialCartera is the list history object
     * @return the history summary object
    */
    private void createHistorialCarteraResumen( HistorialCartera historialCartera ) {
        // Variables
        HistorialCarteraResumen historialCarteraResumen;
        LocalDate               fechaReporte;
        Contrato                contrato;
        Servicio                servicio;
        // Código
        historialCarteraResumen = new HistorialCarteraResumen();
        fechaReporte            = historialCartera.getFechaReporte();
        servicio                = historialCartera.getServicio();
        contrato                = servicio.getContrato();
        historialCarteraResumen.setContrato( contrato );
        historialCarteraResumen.setFechaReporte( fechaReporte );

        if( historialCarteraResumenRepository.getResumenByContratoAndFechaReporte( contrato, fechaReporte ) == null ) {
            crearNuevoResumen(historialCartera, fechaReporte, contrato, servicio);
        }else{
            actualizarResumen(historialCartera, fechaReporte, contrato,  servicio);
        }
    }

    public void crearNuevoResumen(HistorialCartera historialCartera, LocalDate fechaReporte, Contrato contrato, Servicio servicio){
        HistorialCarteraResumen historialCarteraResumen = new HistorialCarteraResumen();
        historialCarteraResumen.setContrato( contrato );
        historialCarteraResumen.setFechaReporte( fechaReporte );
        double tasaPonderada = 0;
        historialCarteraResumen.setCantidadServicios( 1 );
        historialCarteraResumen.setCuotasPactadasMaximo( servicio.getCuotasPactadas() );
        historialCarteraResumen.setCuotasPactadasMinimo( servicio.getCuotasPactadas() );
        historialCarteraResumen.setCuotasPorFacturarMaximo( servicio.getCuotasPorFacturar() );
        historialCarteraResumen.setCuotasPorFacturarMinimo( servicio.getCuotasPorFacturar() );
        historialCarteraResumen.setDiasAtrasoMaximo( historialCartera.getDiasAtraso() );
        historialCarteraResumen.setFechaPrimerVencimientoMinimo( historialCartera.getFechaPrimerVencimiento() );
        historialCarteraResumen.setFechaSegundoVencimientoMinimo( historialCartera.getFechaSegundoVencimiento() );
        historialCarteraResumen.setMoraPrimerVencimientoMaximo( historialCartera.getMoraPrimerVencimiento() );
        historialCarteraResumen.setSumaCapitalImpago( historialCartera.getCapitalImpago() );
        historialCarteraResumen.setSumaInteresCausadoNoFacturado( historialCartera.getInteresCausadoNoFacturado() );
        historialCarteraResumen.setSumaInteresCorrienteOrdenCausadoNoFacturado( historialCartera.getInteresCorrienteOrdenCausadoNoFacturado() );
        historialCarteraResumen.setSumaInteresFacturadoNoAfecto( historialCartera.getInteresFacturadoNoAfecto() );
        historialCarteraResumen.setSumaInteresImpago( historialCartera.getInteresImpago() );
        historialCarteraResumen.setSumaInteresMora( historialCartera.getInteresMora() );
        historialCarteraResumen.setSumaInteresMoraOrdenCausadoNoFacturado( historialCartera.getInteresMoraOrdenCausadoNoFacturado() );
        historialCarteraResumen.setSumaInteresNoAfectoPorFacturar( historialCartera.getInteresNoAfectoPorFacturar() );
        historialCarteraResumen.setSumaInteresOrdenCorrienteImpago( historialCartera.getInteresOrdenCorrienteImpago() );
        historialCarteraResumen.setSumaInteresOrdenMoraImpago( historialCartera.getInteresOrdenMoraImpago() );
        historialCarteraResumen.setSumaInteresMoraCausadoNoFacturado( historialCartera.getInteresMoraCausadoNoFacturado() );
        historialCarteraResumen.setSumaSaldoCapitalPorFacturar( servicio.getSaldoCapitalPorFacturar() );
        historialCarteraResumen.setSumaSaldoDeuda( servicio.getSaldoDeuda() );
        historialCarteraResumen.setSumaTotalInteresCorrienteCausado( historialCartera.getTotalInteresCorrienteCausado() );
        historialCarteraResumen.setSumaTotalInteresCorrienteFacturadoImpago( historialCartera.getTotalInteresCorrienteFacturadoImpago() );
        historialCarteraResumen.setSumaTotalInteresMoraCausado( historialCartera.getTotalInteresMoraCausado() );
        historialCarteraResumen.setSumaTotalInteresMoraFacturadoImpago( historialCartera.getTotalInteresMoraFacturadoImpago() );
        historialCarteraResumen.setSumaValorCompra( servicio.getValorCompra() );
        historialCarteraResumen.setSumaValorCuota( servicio.getValorCuota() );
        historialCarteraResumen.setTasaMaximaPactada( servicio.getTasa() );
        historialCarteraResumen.setTasaMinimaPactada( servicio.getTasa() );
        tasaPonderada = (historialCartera.getCapitalImpago() + servicio.getSaldoCapitalPorFacturar()) * servicio.getTasa();
        tasaPonderada = tasaPonderada / historialCarteraResumen.getSumaSaldoCapitalPorFacturar();
        historialCarteraResumen.setTasaPonderada( tasaPonderada );
        historialCarteraResumenRepository.save( historialCarteraResumen ); 
        crearGastoCobranza(historialCarteraResumen, contrato);
    }

    public void actualizarResumen(HistorialCartera historialCartera, LocalDate fechaReporte, Contrato contrato, Servicio servicio){
        HistorialCarteraResumen historialCarteraResumen;
        double tasaPonderada = 0;
        historialCarteraResumen = historialCarteraResumenRepository.getResumenByContratoAndFechaReporte( contrato, fechaReporte );
        historialCarteraResumen.setCantidadServicios( historialCarteraResumen.getCantidadServicios() + 1 );
        historialCarteraResumen.setCuotasPactadasMaximo( ( historialCarteraResumen.getCuotasPactadasMaximo() > servicio.getCuotasPactadas() ) ? historialCarteraResumen.getCuotasPactadasMaximo():servicio.getCuotasPactadas() );
        historialCarteraResumen.setCuotasPactadasMinimo( ( historialCarteraResumen.getCuotasPactadasMinimo() < servicio.getCuotasPactadas() ) ? historialCarteraResumen.getCuotasPactadasMinimo():servicio.getCuotasPactadas() );
        historialCarteraResumen.setCuotasPorFacturarMaximo( ( historialCarteraResumen.getCuotasPorFacturarMaximo() > servicio.getCuotasPorFacturar() ) ? historialCarteraResumen.getCuotasPorFacturarMaximo():servicio.getCuotasPorFacturar() );
        historialCarteraResumen.setCuotasPorFacturarMinimo( ( historialCarteraResumen.getCuotasPorFacturarMinimo() < servicio.getCuotasPorFacturar() ) ? historialCarteraResumen.getCuotasPorFacturarMinimo():servicio.getCuotasPorFacturar() );
        historialCarteraResumen.setDiasAtrasoMaximo( ( historialCarteraResumen.getDiasAtrasoMaximo() > historialCartera.getDiasAtraso() ) ? historialCarteraResumen.getDiasAtrasoMaximo():historialCartera.getDiasAtraso() );
        historialCarteraResumen.setFechaPrimerVencimientoMinimo( ( historialCarteraResumen.getFechaPrimerVencimientoMinimo().isBefore( historialCartera.getFechaPrimerVencimiento() ) ) ? historialCarteraResumen.getFechaPrimerVencimientoMinimo():historialCartera.getFechaPrimerVencimiento() );
        historialCarteraResumen.setFechaSegundoVencimientoMinimo( ( historialCarteraResumen.getFechaSegundoVencimientoMinimo().isBefore( historialCartera.getFechaSegundoVencimiento() ) ) ? historialCarteraResumen.getFechaSegundoVencimientoMinimo():historialCartera.getFechaSegundoVencimiento() );
        historialCarteraResumen.setMoraPrimerVencimientoMaximo( ( historialCarteraResumen.getMoraPrimerVencimientoMaximo() > historialCartera.getMoraPrimerVencimiento() ) ? historialCarteraResumen.getMoraPrimerVencimientoMaximo():historialCartera.getMoraPrimerVencimiento() );
        historialCarteraResumen.setSumaCapitalImpago( historialCarteraResumen.getSumaCapitalImpago() + historialCartera.getCapitalImpago() );
        historialCarteraResumen.setSumaInteresCausadoNoFacturado( historialCarteraResumen.getSumaInteresCausadoNoFacturado() + historialCartera.getInteresCausadoNoFacturado() );
        historialCarteraResumen.setSumaInteresCorrienteOrdenCausadoNoFacturado( historialCarteraResumen.getSumaInteresCorrienteOrdenCausadoNoFacturado() + historialCartera.getInteresCorrienteOrdenCausadoNoFacturado() );
        historialCarteraResumen.setSumaInteresFacturadoNoAfecto( historialCarteraResumen.getSumaInteresFacturadoNoAfecto() + historialCartera.getInteresFacturadoNoAfecto() );
        historialCarteraResumen.setSumaInteresImpago( historialCarteraResumen.getSumaInteresImpago() + historialCartera.getInteresImpago() );
        historialCarteraResumen.setSumaInteresMora( historialCarteraResumen.getSumaInteresMora() + historialCartera.getInteresMora() );
        historialCarteraResumen.setSumaInteresMoraOrdenCausadoNoFacturado( historialCarteraResumen.getSumaInteresMoraOrdenCausadoNoFacturado() + historialCartera.getInteresMoraOrdenCausadoNoFacturado() );
        historialCarteraResumen.setSumaInteresMoraCausadoNoFacturado( historialCarteraResumen.getSumaInteresMoraCausadoNoFacturado() + historialCartera.getInteresMoraCausadoNoFacturado() );
        historialCarteraResumen.setSumaInteresNoAfectoPorFacturar( historialCarteraResumen.getSumaInteresNoAfectoPorFacturar() + historialCartera.getInteresNoAfectoPorFacturar() );
        historialCarteraResumen.setSumaInteresOrdenCorrienteImpago( historialCarteraResumen.getSumaInteresOrdenCorrienteImpago() + historialCartera.getInteresOrdenCorrienteImpago() );
        historialCarteraResumen.setSumaInteresOrdenMoraImpago( historialCarteraResumen.getSumaInteresOrdenMoraImpago() + historialCartera.getInteresOrdenMoraImpago() );
        historialCarteraResumen.setSumaSaldoDeuda( historialCarteraResumen.getSumaSaldoDeuda() + servicio.getSaldoDeuda() );
        historialCarteraResumen.setSumaTotalInteresCorrienteCausado( historialCarteraResumen.getSumaTotalInteresCorrienteCausado() + historialCartera.getTotalInteresCorrienteCausado() );
        historialCarteraResumen.setSumaTotalInteresCorrienteFacturadoImpago( historialCarteraResumen.getSumaTotalInteresCorrienteFacturadoImpago() + historialCartera.getTotalInteresCorrienteFacturadoImpago() );
        historialCarteraResumen.setSumaTotalInteresMoraCausado( historialCarteraResumen.getSumaTotalInteresMoraCausado() + historialCartera.getTotalInteresMoraCausado() );
        historialCarteraResumen.setSumaTotalInteresMoraFacturadoImpago( historialCarteraResumen.getSumaTotalInteresMoraFacturadoImpago() + historialCartera.getTotalInteresMoraFacturadoImpago() );
        historialCarteraResumen.setSumaValorCompra( historialCarteraResumen.getSumaValorCompra() + servicio.getValorCompra() );
        historialCarteraResumen.setSumaValorCuota( historialCarteraResumen.getSumaValorCuota() + servicio.getValorCuota() );
        historialCarteraResumen.setTasaMaximaPactada( ( historialCarteraResumen.getTasaMaximaPactada() >servicio.getTasa() ) ? historialCarteraResumen.getTasaMaximaPactada():servicio.getTasa() );
        historialCarteraResumen.setTasaMinimaPactada( ( historialCarteraResumen.getTasaMinimaPactada() <servicio.getTasa() ) ? historialCarteraResumen.getTasaMinimaPactada():servicio.getTasa() );
        tasaPonderada = historialCarteraResumen.getTasaPonderada() * historialCarteraResumen.getSumaSaldoCapitalPorFacturar();
        tasaPonderada = ((historialCartera.getCapitalImpago() + servicio.getSaldoCapitalPorFacturar()) * servicio.getTasa()) + tasaPonderada;
        historialCarteraResumen.setSumaSaldoCapitalPorFacturar( historialCarteraResumen.getSumaSaldoCapitalPorFacturar() + servicio.getSaldoCapitalPorFacturar() );
        tasaPonderada = tasaPonderada / historialCarteraResumen.getSumaSaldoCapitalPorFacturar();
        historialCarteraResumen.setTasaPonderada( tasaPonderada );
        historialCarteraResumenRepository.save( historialCarteraResumen );
        crearGastoCobranza(historialCarteraResumen, contrato);
    }

    public void crearGastoCobranza(HistorialCarteraResumen historialCarteraResumen, Contrato contrato){
        GastoCobranza gastoCobranza = gastoCobranzaRepository.getGastoCobranzaByContrato(contrato);
        if(gastoCobranza == null){
            gastoCobranza = new GastoCobranza();
            gastoCobranza.setFechaAnterior(historialCarteraResumen.getFechaReporte());
        }else if(!gastoCobranza.getFechaAnterior().isEqual(historialCarteraResumen.getFechaReporte())){
            gastoCobranza.setFechaAnterior(gastoCobranza.getHistorialCarteraResumen().getFechaReporte());
        }
        gastoCobranza.setHistorialCarteraResumen(historialCarteraResumen);
        gastoCobranzaRepository.save(gastoCobranza);
    }

    /**
     * Set the repository history
     * @param historialCarteraLightRepository is the repository object
    */
    public void setHistorialCarteraRepository( HistorialCarteraLightRepository historialCarteraLightRepository ) {
        this.historialCarteraLightRepository = historialCarteraLightRepository;
    }

    /**
     * Set the story summary repository
     * @param historialCarteraResumenRepository is the repository object
    */
    public void setHistorialCarteraResumenRepository(HistorialCarteraResumenRepository historialCarteraResumenRepository) {
        this.historialCarteraResumenRepository = historialCarteraResumenRepository;
    }

    /**
     * Set the repository service history
     * @param servicioRepository is the repository object
    */
    public void setServicioRepository( ServicioRepository servicioRepository ) {
        this.servicioRepository = servicioRepository;
    }

    /**
     * Set the contract repository
     * @param contratoRepository is the repository object
    */
    public void setContratoRepository( ContratoRepository contratoRepository ) {
        this.contratoRepository = contratoRepository;
    }

     /**
     * Set the account repository
     * @param cuentaRepository is the repository object
    */
    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Set the client repository
     * @param clienteRepository is the repository object
    */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }
    
}
