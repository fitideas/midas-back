package com.colpatria.midas.writers;

/*
 *
 * Librerías
 *
*/

import java.util.List;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.transaction.annotation.Transactional;


/**
 * Class that writes the items that were read and process into the database
*/
public class MovimientoSaldoItemWriter implements ItemWriter<HistorialMovimientoSaldo> {

    /**
     * History repository to read and write to the database
    */
    private HistorialMovimientoSaldoRepository historialMovimientoSaldoRepository;
	
    /**
     * User repository to read and write to the database
    */
	private UsuarioRepository usuarioRepository;
    
    /**
     * Contract repository to read and write to the database
    */
	private ContratoRepository contratoRepository;
    
    /**
     * Client repository to read and write to the database
    */
	private ClienteRepository clienteRepository;
    
    /**
     * Negociation repository to read and write to the database
    */
	private NegociacionRepository negociacionRepository;

    /**
     * validation service
     */
    private final ValidationService validationService;

    /*
     *
     * Métodos
     *
    */

    /**
     * Constructor of the item writer
     * @param validationService is the validation service
    */
    public MovimientoSaldoItemWriter( ValidationService validationService ) {
        this.validationService = validationService;
    }

    /**
     * Method that writes to the database
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
    */
    @Override
    public void write( List<? extends HistorialMovimientoSaldo> items ) throws Exception {
        // Variables
        Usuario     usuarioAprobador;
        Usuario     usuarioCreador;
        Negociacion negociacion;
        Cliente     cliente;
        Contrato    contrato;
        // Code 
        if( !this.validationService.getHasError() ) {
            for( HistorialMovimientoSaldo historialMovimientoSaldo : items ) {
                // History
                usuarioAprobador = this.usuarioRepository.save( historialMovimientoSaldo.getUsuarioAprobador() );
                usuarioCreador   = this.usuarioRepository.save( historialMovimientoSaldo.getUsuarioCreador() );
                negociacion      = this.negociacionRepository.save( historialMovimientoSaldo.getNegociacion() );
                cliente          = this.clienteRepository.save( historialMovimientoSaldo.getContrato().getCliente() );
                contrato         = historialMovimientoSaldo.getContrato();
                contrato.setCliente( cliente );
                contrato = this.contratoRepository.save( contrato );
                historialMovimientoSaldo.setContrato( contrato );
                historialMovimientoSaldo.setUsuarioAprobador( usuarioAprobador );
                historialMovimientoSaldo.setNegociacion( negociacion );
                historialMovimientoSaldo.setUsuarioCreador( usuarioCreador );
                this.historialMovimientoSaldoRepository.save( historialMovimientoSaldo );
            }            
        }            
    }

    /**
     * Set the repository history movement balance
     * @param historialMovimientoSaldoRepository is the repository object
    */
    public void setHistorialMovimientoSaldoRepository( HistorialMovimientoSaldoRepository historialMovimientoSaldoRepository ) {
        this.historialMovimientoSaldoRepository = historialMovimientoSaldoRepository;
    }    

    /**
     * Set the contract repository
     * @param contratoRepository is the repository object
    */
    public void setContratoRepository( ContratoRepository contratoRepository ) {
        this.contratoRepository = contratoRepository;
    }

    /**
     * Set the user repository
     * @param usuarioRepository is the repository object
    */
    public void setUsuarioRepository(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    /**
     * Set the client repository
     * @param clienteRepository is the repository object
    */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Set the repository negotiation history
     * @param negociacionRepository is the repository object
    */
    public void setNegociacionRepository(NegociacionRepository negociacionRepository) {
        this.negociacionRepository = negociacionRepository;
    }
    
}
