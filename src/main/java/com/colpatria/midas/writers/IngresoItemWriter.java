package com.colpatria.midas.writers;

import com.colpatria.midas.model.Ingreso;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.IngresoRepository;
import com.colpatria.midas.repositories.ServicioRepository;
import com.colpatria.midas.services.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class IngresoItemWriter implements ItemWriter<Ingreso> {

    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(FinalizacionesItemWriter.class);

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * add 'Ingreso' repository
     */
    private final IngresoRepository ingresoRepository;

    /**
     * add 'Servicio' repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * add 'Contrato' repository
     */
    private final ContratoRepository contratoRepository;
    /**
     * Add 'Cliente' repository
     */
    private final ClienteRepository clienteRepository;


    /**
     Constructor for the Object IngresoItemWriter
     */
    public IngresoItemWriter(ValidationService validationService, IngresoRepository ingresoRepository, ServicioRepository servicioRepository, ContratoRepository contratoRepository,ClienteRepository clienteRepository){
        this.validationService=validationService;
        this.servicioRepository=servicioRepository;
        this.contratoRepository=contratoRepository;
        this.clienteRepository=clienteRepository;
        this.ingresoRepository=ingresoRepository;
    }

    @Override
    public void write(List<? extends Ingreso> items) throws Exception {
        if (!this.validationService.getHasError()) {
            for (Ingreso item : items){
                item.getServicioNumero().getContrato().setCliente(clienteRepository.save(item.getServicioNumero().getContrato().getCliente()));
                item.getServicioNumero().setContrato(contratoRepository.save(item.getServicioNumero().getContrato()));
                //item.setServicioNumero(servicioRepository.save(item.getServicioNumero()));
                ingresoRepository.saveAndFlush(item);
            }
        }
    }
}
