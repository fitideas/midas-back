package com.colpatria.midas.writers;

import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import org.springframework.batch.item.ItemWriter;

import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

public class RecaudoItemWriter implements ItemWriter<HistorialRecaudo> {

    /**
     * Historial Recaudo repository
     */
    private final HistorialRecaudoRepository historialRecaudoRepository;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * Cuenta Repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * Cliente Repository
     */
    private final ClienteRepository clienteRepository;

    /**
     * Contrato Repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * Servicio Repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * Recaudo Cargo Repository
     */
    private final RecaudoCargoRepository recaudoCargoRepository;

    /**
     * Historial Recaudo Resumen Repository
     */
    private final HistorialRecaudoResumenRepository historialRecaudoResumenRepository;

    /**
     * Tipo Cargo Repository
     */
    private final TipoCargoRepository tipoCargoRepository;

    /**
     * @param historialRecaudoRepository Historial Recaudo Repository
     * @param validationService          Validation Service Bean Reference
     * @param cuentaRepository           Cuenta Repository Bean Reference
     * @param clienteRepository          Cliente Repository Bean Reference
     * @param contratoRepository         Contrato Repository Bean Reference
     * @param servicioRepository         Servicio Repository Bean Reference
     * @param recaudoCargoRepository     Recaudo Cargo Bean Reference
     */
    public RecaudoItemWriter(HistorialRecaudoRepository historialRecaudoRepository, ValidationService validationService, CuentaRepository cuentaRepository, ClienteRepository clienteRepository, ContratoRepository contratoRepository, ServicioRepository servicioRepository, RecaudoCargoRepository recaudoCargoRepository, HistorialRecaudoResumenRepository historialRecaudoResumenRepository, TipoCargoRepository tipoCargoRepository) {
        this.historialRecaudoRepository = historialRecaudoRepository;
        this.validationService = validationService;
        this.cuentaRepository = cuentaRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository = contratoRepository;
        this.servicioRepository = servicioRepository;
        this.recaudoCargoRepository = recaudoCargoRepository;
        this.historialRecaudoResumenRepository = historialRecaudoResumenRepository;
        this.tipoCargoRepository = tipoCargoRepository;
    }

    /**
     * @param items with data charged in file
     */
    @Override
    public void write(List<? extends HistorialRecaudo> items) {
        if (!this.validationService.getHasError()) {
            for (HistorialRecaudo item : items) {
                item.getRecaudoCargo().getCliente().setCuenta(this.cuentaRepository.save(item.getRecaudoCargo().getCliente().getCuenta()));
                item.getRecaudoCargo().setCliente(this.clienteRepository.save(item.getRecaudoCargo().getCliente()));
                item.getRecaudoCargo().getServicio().setContrato(this.contratoRepository.save(item.getRecaudoCargo().getServicio().getContrato()));
                item.getRecaudoCargo().setServicio(this.servicioRepository.save(item.getRecaudoCargo().getServicio()));
                item.setRecaudoCargo(this.recaudoCargoRepository.save(item.getRecaudoCargo()));
                item.setResumen(this.createHistorialRecaudoResumen(item));
                this.historialRecaudoRepository.save(item);
            }
        }
    }

    /**
     * Write the Historial Recaudo Resumen corresponding to a given Historial Recaudo
     *
     * @param historialRecaudo Historial Recaudo
     */
    private HistorialRecaudoResumen createHistorialRecaudoResumen(HistorialRecaudo historialRecaudo) {
        HistorialRecaudoResumen resumen = this.historialRecaudoResumenRepository.getHistorialRecaudoResumenByContratoAndFechaPagoAndFechaProcesoPago(historialRecaudo.getRecaudoCargo().getServicio().getContrato(), historialRecaudo.getFechaPago(), historialRecaudo.getFechaProcesoPago());
        if(resumen == null){
            resumen = new HistorialRecaudoResumen();
            resumen.setContrato(historialRecaudo.getRecaudoCargo().getServicio().getContrato());
            resumen.setFechaPago(historialRecaudo.getFechaPago());
            resumen.setFechaProcesoPago(historialRecaudo.getFechaProcesoPago());
            resumen.setValorPagado(0.0);
            resumen.setCapital(0.0);
            resumen.setInteresCorriente(0.0);
            resumen.setInteresMora(0.0);
            resumen.setCuotaManejo(0.0);
            resumen.setHonorarioCobranza(0.0);
            resumen.setSeguroObligatorio(0.0);
            resumen.setSeguroVoluntario(0.0);
            resumen.setComisiones(0.0);
        }
        resumen.setValorPagado(resumen.getValorPagado() + historialRecaudo.getRecaudoCargo().getValorPago());
        if (Objects.equals(historialRecaudo.getCargo().getTipoCargo().getNombre(), "CAPITAL")) {
            resumen.setCapital(resumen.getCapital() + historialRecaudo.getRecaudoCargo().getMonto());
        } else if (Objects.equals(historialRecaudo.getCargo().getTipoCargo().getNombre(), "INTCTE")) {
            resumen.setInteresCorriente(resumen.getInteresCorriente() + historialRecaudo.getRecaudoCargo().getMonto());
        } else if (Objects.equals(historialRecaudo.getCargo().getTipoCargo().getNombre(), "INTMOR")) {
            resumen.setInteresMora(resumen.getInteresMora() + historialRecaudo.getRecaudoCargo().getMonto());
        } else if (Objects.equals(historialRecaudo.getCargo().getTipoCargo().getNombre(), "CMANEJO")) {
            resumen.setCuotaManejo(resumen.getCuotaManejo() + historialRecaudo.getRecaudoCargo().getMonto());
        } else if (Objects.equals(historialRecaudo.getCargo().getTipoCargo().getNombre(), "HONORARIO")) {
            resumen.setHonorarioCobranza(resumen.getHonorarioCobranza() + historialRecaudo.getRecaudoCargo().getMonto());
        } else if (Objects.equals(historialRecaudo.getCargo().getTipoCargo().getNombre(), "SEGURO")) {
            resumen.setSeguroObligatorio(resumen.getSeguroObligatorio() + historialRecaudo.getRecaudoCargo().getMonto());
        } else if (Objects.equals(historialRecaudo.getCargo().getTipoCargo().getNombre(), "SEGUROVOL")) {
            resumen.setSeguroVoluntario(resumen.getSeguroVoluntario() + historialRecaudo.getRecaudoCargo().getMonto());
        } else if (Objects.equals(historialRecaudo.getCargo().getTipoCargo().getNombre(), "COMISIONES")) {
            resumen.setComisiones(resumen.getComisiones() + historialRecaudo.getRecaudoCargo().getMonto());
        }
        return this.historialRecaudoResumenRepository.save(resumen);
    }

}
