package com.colpatria.midas.writers;

import java.util.List;
import com.colpatria.midas.model.HistorialCambioEstado;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.CuentaRepository;
import com.colpatria.midas.repositories.HistorialCambioEstadoRepository;
import com.colpatria.midas.repositories.ServicioRepository;
import com.colpatria.midas.repositories.UsuarioRepository;
import com.colpatria.midas.services.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

/**
 * Class that writes the items that were read and process into the database
 */
public class CambioEstadoItemWriter implements ItemWriter<HistorialCambioEstado> {

    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(CambioEstadoItemWriter.class);

    /**
     * Repository interface for 'historialCambioEstadoRepository'
     */
    private final HistorialCambioEstadoRepository historialCambioEstadoRepository;

    /**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

    /**
	 * add 'contrato' repository
	 */
	private final ContratoRepository contratoRepository;

    /**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

    /**
	 * add 'usuario' repository
	 */
	private final UsuarioRepository usuarioRepository;

    /**
	 * add 'cuenta' repository
	 */
	private final CuentaRepository cuentaRepository;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * Constructor of the item writer
     * @param historialCambioEstadoRepository interface that saves 'historialCambioEstadoRepository' to data base
     * @param validationService is the service that validates fields in the 'historialCambioEstadoRepository'
     * @param clienteRepository is the service that validates fields in the 'clienteRepository'
     * @param contratoRepository is the service that validates fields in the 'contratoRepository'
     * @param servicioRepository is the service that validates fields in the 'servicioRepository'
     * @param usuarioRepository is the service that validates fields in the 'usuarioRepository'
     */
    public CambioEstadoItemWriter(HistorialCambioEstadoRepository historialCambioEstadoRepository, ClienteRepository clienteRepository, ContratoRepository contratoRepository, ServicioRepository servicioRepository, UsuarioRepository usuarioRepository,  CuentaRepository cuentaRepository, ValidationService validationService) {
        this.historialCambioEstadoRepository = historialCambioEstadoRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository =  contratoRepository;
        this.servicioRepository = servicioRepository;
        this.usuarioRepository = usuarioRepository;
        this.cuentaRepository = cuentaRepository;
        this.validationService = validationService;
    }

    /**
     * Method that writes to the database
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
     */
    @Override
    public void write(List<? extends HistorialCambioEstado> items) throws Exception {
        logger.info("Start write process");
        if (!this.validationService.getHasError()){
            for(HistorialCambioEstado item : items){
                item.getServicio().getContrato().getCliente().setCuenta(this.cuentaRepository.save(item.getServicio().getContrato().getCliente().getCuenta()));
                item.getServicio().getContrato().setCliente(this.clienteRepository.save(item.getServicio().getContrato().getCliente()));
                item.getServicio().setContrato(this.contratoRepository.save(item.getServicio().getContrato()));
                item.setServicio(this.servicioRepository.save(item.getServicio()));
                item.setUsuario(this.usuarioRepository.save(item.getUsuario()));
                this.historialCambioEstadoRepository.saveAndFlush(item);
            }
        }  
    }

}
