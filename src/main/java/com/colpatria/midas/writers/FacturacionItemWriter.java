package com.colpatria.midas.writers;

import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import org.springframework.batch.item.ItemWriter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Facturacion Item Writer
 */
public class FacturacionItemWriter implements ItemWriter<HistorialFacturacion> {

    /**
     * Historial Facturacion Repository
     */
    private HistorialFacturacionRepository historialFacturacionRepository;
    private CuentaRepository cuentaRepository;
    private ClienteRepository clienteRepository;
    private ContratoRepository contratoRepository;
    private ServicioRepository servicioRepository;

    @Override
    public void write(List<? extends HistorialFacturacion> list) throws Exception {
        for(HistorialFacturacion historialFacturacion : list){
            historialFacturacion.getServicio().getContrato().getCliente().setCuenta(this.cuentaRepository.save(historialFacturacion.getServicio().getContrato().getCliente().getCuenta()));
            historialFacturacion.getServicio().getContrato().setCliente(this.clienteRepository.save(historialFacturacion.getServicio().getContrato().getCliente()));
            historialFacturacion.getServicio().setContrato(this.contratoRepository.save(historialFacturacion.getServicio().getContrato()));
            historialFacturacion.setServicio(this.servicioRepository.save(historialFacturacion.getServicio()));
            this.historialFacturacionRepository.saveAndFlush(historialFacturacion);
        }
    }

    /**
     * Historial Facturacion Repository Setter
     * @param historialFacturacionRepository Historial Facturacion Repository
     */
    public void setHistorialFacturacionRepository(HistorialFacturacionRepository historialFacturacionRepository) {
        this.historialFacturacionRepository = historialFacturacionRepository;
    }

    /**
     * Cuenta Repository setter
     * @param cuentaRepository Cuenta Repository
     */
    public void setCuentaRepository(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Cliente Repository setter
     * @param clienteRepository Cliente Repository
     */
    public void setClienteRepository(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Contrato repository setter
     * @param contratoRepository Contrato repository
     */
    public void setContratoRepository(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    /**
     * Servicio Repository setter
     * @param servicioRepository Servicio Repository
     */
    public void setServicioRepository(ServicioRepository servicioRepository) {
        this.servicioRepository = servicioRepository;
    }
}
