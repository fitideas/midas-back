package com.colpatria.midas.writers;

import java.time.LocalDate;
import java.util.List;

import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.HistorialFacturacionConcepto;
import com.colpatria.midas.model.HistorialFacturacionConceptoResumen;
import com.colpatria.midas.model.TipoCargo;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.CuentaRepository;
import com.colpatria.midas.repositories.HistorialFacturacionConceptoRepository;
import com.colpatria.midas.repositories.HistorialFacturacionConceptoResumenRepository;
import com.colpatria.midas.repositories.ServicioRepository;
import com.colpatria.midas.services.ValidationService;
import org.springframework.batch.item.ItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that writes the items that were read and process into the database
 */
public class FacturacionConceptoItemWriter implements ItemWriter<HistorialFacturacionConcepto>{

    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(FacturacionConceptoItemWriter.class);

    /**
     * Repository interface for 'historialFacturacionConceptoRepository'
     */
    private final HistorialFacturacionConceptoRepository historialFacturacionConceptoRepository;

    /**
     * Repository interface for 'historialFacturacionConceptoRepository'
     */
    private final HistorialFacturacionConceptoResumenRepository historialFacturacionConceptoResumenRepository;

    /**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

    /**
	 * add 'contrato' repository
	 */
	private final ContratoRepository contratoRepository;

    /**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

    /**
	 * add 'cuenta' repository
	 */
	private final CuentaRepository cuentaRepository;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * Constructor of the item writer
     * @param historialFacturacionConceptoRepository interface that saves 'historialFacturacionConceptoRepository' to data base
     * @param validationService is the service that validates fields in the 'historialFacturacionConcepto'
     * @param clienteRepository is the service that validates fields in the 'clienteRepository'
     * @param contratoRepository is the service that validates fields in the 'contratoRepository'
     * @param servicioRepository is the service that validates fields in the 'servicioRepository'
     */
    public FacturacionConceptoItemWriter(HistorialFacturacionConceptoRepository historialFacturacionConceptoRepository, HistorialFacturacionConceptoResumenRepository historialFacturacionConceptoResumenRepository, ClienteRepository clienteRepository,  CuentaRepository cuentaRepository, ContratoRepository contratoRepository, ServicioRepository servicioRepository, ValidationService validationService) {
        this.historialFacturacionConceptoRepository = historialFacturacionConceptoRepository;
        this.historialFacturacionConceptoResumenRepository = historialFacturacionConceptoResumenRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository =  contratoRepository;
        this.servicioRepository = servicioRepository;
        this.cuentaRepository = cuentaRepository;
        this.validationService = validationService;
    }

    /**
     * Method that writes to the database
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
     */
    @Override
    public void write(List<? extends HistorialFacturacionConcepto> items) throws Exception {
        logger.info("Start write process");
        if (!this.validationService.getHasError()){
            for(HistorialFacturacionConcepto item : items){
                item.getServicio().getContrato().getCliente().setCuenta(this.cuentaRepository.save(item.getServicio().getContrato().getCliente().getCuenta()));
                item.getServicio().getContrato().setCliente(this.clienteRepository.save(item.getServicio().getContrato().getCliente()));
                item.getServicio().setContrato(this.contratoRepository.save(item.getServicio().getContrato()));
                item.setServicio(this.servicioRepository.save(item.getServicio()));
                this.historialFacturacionConceptoRepository.saveAndFlush(item);
                createHistorialFacturacionConceptoResumen(item);
            }
        } 
    }

    /**
     * Method that creates the 'historial facturacion concepto resumen'
     * @param historial is the object hisotry of wich the 'historial facturacion concepto resumen' will be created from
     */
    public void createHistorialFacturacionConceptoResumen(HistorialFacturacionConcepto historial){
        HistorialFacturacionConceptoResumen resumen = new HistorialFacturacionConceptoResumen();
        LocalDate fechaDocumento = historial.getFechaDocumento();
        LocalDate fechaProceso = historial.getFechaProceso();
        Contrato contrato = historial.getServicio().getContrato();
        TipoCargo tipoCargo = historial.getCargo().getTipoCargo();

        resumen.setContrato(contrato);
        resumen.setFechaDocumento(fechaDocumento);
        resumen.setFechaProceso(fechaProceso);

        this.initializeValues(resumen);

        if(historialFacturacionConceptoResumenRepository.getResumenByFilters(contrato, fechaProceso, fechaDocumento) == null ) {
            resumen.setSumaTotalDocumento(historial.getTotalDocumento());
            this.setSumaValorFactura(historial.getValorFactura(), tipoCargo.getCodigo(), resumen);
        }else{
            resumen = historialFacturacionConceptoResumenRepository.getResumenByFilters(contrato, fechaProceso, fechaDocumento);
            resumen.setSumaTotalDocumento(resumen.getSumaTotalDocumento() + historial.getTotalDocumento());
            this.setSumaValorFactura(historial.getValorFactura(), tipoCargo.getCodigo(), resumen);
        }

        historialFacturacionConceptoResumenRepository.save(resumen);
    }

    /**
     * Method that sets the initial values of the fields that correspond with 'tipoCargo'
     * @param resumen is the instance of 'HistorialFacturacionConceptoResumen' that will be save to the database
     */
    private void initializeValues(HistorialFacturacionConceptoResumen resumen){
        resumen.setCapital(0d);
        resumen.setInteresCorriente(0d);
        resumen.setInteresMora(0d);
        resumen.setCuotaManejo(0d);
        resumen.setHonorarioCobranza(0d);
        resumen.setSeguroObligatorio(0d);
        resumen.setSeguroVoluntario(0d);
        resumen.setComisiones(0d);
        resumen.setNuevosCobros(0d);
    }

    /**
     * Method that adds the value of 'valor factura' to the value initialy set depending on the 'tipoCargo'
     * @param valorFactura is the value to be add
     * @param tipoCargo defines the field in wich to add the 'valorFactura'
     * @param resumen is the instance of 'HistorialFacturacionConceptoResumen' that will be save to the database
     */
    private void setSumaValorFactura(Double valorFactura, Integer tipoCargo, HistorialFacturacionConceptoResumen resumen){
        switch(tipoCargo){
            case 0:
                resumen.setCapital(resumen.getCapital() + valorFactura);
                break;
            case 1:
                resumen.setInteresCorriente(resumen.getInteresCorriente() + valorFactura);
                break;
            case 2:
                resumen.setInteresMora(resumen.getInteresMora() + valorFactura);
                break;
            case 3:
                resumen.setCuotaManejo(resumen.getCuotaManejo() + valorFactura);
                break;
            case 4:
                resumen.setHonorarioCobranza(resumen.getHonorarioCobranza() + valorFactura);
                break;
            case 5:
                resumen.setSeguroObligatorio(resumen.getSeguroObligatorio() + valorFactura);
                break;
            case 6:
                resumen.setSeguroVoluntario(resumen.getSeguroVoluntario() + valorFactura);
                break;
            case 7:
                resumen.setComisiones(resumen.getComisiones() + valorFactura);
                break;
            default:
                resumen.setNuevosCobros(resumen.getNuevosCobros() + valorFactura);
                break;
        }
    }

}