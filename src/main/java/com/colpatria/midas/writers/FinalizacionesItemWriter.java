package com.colpatria.midas.writers;

import com.colpatria.midas.model.InfoFinalizacion;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

/**
 * Finalizaciones writer
 */
public class FinalizacionesItemWriter implements ItemWriter<InfoFinalizacion> {

    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(FinalizacionesItemWriter.class);

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * add 'cliente' repository
     */
    private final ClienteRepository clienteRepository;

    /**
     * add 'contrato' repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * add 'servicio' repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * add 'cuenta' repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * add InfoFinalizacionRepository
     */
    private final InfoFinalizacionRepository infoFinalizacionRepository;

    /**
     * add 'usuario' repository
     */
    private final UsuarioRepository usuarioRepository;

    /**
     * @param validationService DI of validation service
     * @param clienteRepository DI of cliente repository
     * @param contratoRepository DI of contratoRepository
     * @param servicioRepository DI of servicioRepository
     * @param cuentaRepository DI of cuentaRepository
     * @param infoFinalizacionRepository DI of infoFinalizacionRepository
     * @param usuarioRepository DI of usuarioRepository
     */
    public FinalizacionesItemWriter(ValidationService validationService, ClienteRepository clienteRepository, ContratoRepository contratoRepository, ServicioRepository servicioRepository, CuentaRepository cuentaRepository, InfoFinalizacionRepository infoFinalizacionRepository, UsuarioRepository usuarioRepository) {
        this.validationService = validationService;
        this.clienteRepository = clienteRepository;
        this.contratoRepository = contratoRepository;
        this.servicioRepository = servicioRepository;
        this.cuentaRepository = cuentaRepository;
        this.infoFinalizacionRepository = infoFinalizacionRepository;
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public void write(List<? extends InfoFinalizacion> items) throws Exception {
        if (!this.validationService.getHasError()) {
            for (InfoFinalizacion item : items) {
                item.getServicio().getContrato().getCliente().setCuenta(this.cuentaRepository.save(item.getServicio().getContrato().getCliente().getCuenta()));
                item.getServicio().getContrato().setCliente(this.clienteRepository.save(item.getServicio().getContrato().getCliente()));
                item.getServicio().setContrato(this.contratoRepository.save(item.getServicio().getContrato()));
                item.setServicio(this.servicioRepository.save(item.getServicio()));
                item.setUsuario(this.usuarioRepository.save(item.getUsuario()));
                infoFinalizacionRepository.saveAndFlush(item);
            }
        }
    }

}