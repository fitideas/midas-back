package com.colpatria.midas.writers;

import java.util.List;
import com.colpatria.midas.model.*;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.ValidationService;
import org.springframework.batch.item.ItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that writes the items that were read and process into the database
 */
public class AutoAmortizadoItemWriter implements ItemWriter<HistorialAutoAmortizado> {

    /**
     * Logger in writer
     */
    private static final Logger logger = LoggerFactory.getLogger(AutoAmortizadoItemWriter.class);

    /**
     * Repository interface for 'historialAutoAmortizadoRepository'
     */
    private final HistorialAutoAmortizadoRepository historialAutoAmortizadoRepository;

    /**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

    /**
	 * add 'contrato' repository
	 */
	private final ContratoRepository contratoRepository;

    /**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * Constructor of the item writer
     * @param historialAutoAmortizadoRepository interface that saves 'historialAutoAmortizadoRepository' to data base
     * @param validationService is the service that validates fields in the 'historialAutoAmortizadoRepository'
     * @param clienteRepository is the service that validates fields in the 'clienteRepository'
     * @param contratoRepository is the service that validates fields in the 'contratoRepository'
     * @param servicioRepository is the service that validates fields in the 'servicioRepository'
     */
    public AutoAmortizadoItemWriter(HistorialAutoAmortizadoRepository historialAutoAmortizadoRepository, ClienteRepository clienteRepository, ContratoRepository contratoRepository, ServicioRepository servicioRepository, ValidationService validationService) {
        this.historialAutoAmortizadoRepository = historialAutoAmortizadoRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository =  contratoRepository;
        this.servicioRepository = servicioRepository;
        this.validationService = validationService;
    }

    /**
     * Method that writes to the database
     * @param items is the list of items to be written in the database
     * @throws Exception when there is an error during the writing process
     */
    @Override
    public void write(List<? extends HistorialAutoAmortizado> items) throws Exception {
        logger.info("Start write process");
        if (!this.validationService.getHasError()){
            for(HistorialAutoAmortizado item : items){
                item.getServicio().getContrato().setCliente(this.clienteRepository.save(item.getServicio().getContrato().getCliente()));
                item.getServicio().setContrato(this.contratoRepository.save(item.getServicio().getContrato()));
                item.setServicio(this.servicioRepository.save(item.getServicio()));
                this.historialAutoAmortizadoRepository.saveAndFlush(item);
            }
        } 
    }

}
