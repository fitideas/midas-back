package com.colpatria.midas.repositories;

import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.HistorialRecaudo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


@Repository
public interface HistorialRecaudoRepository extends JpaRepository<HistorialRecaudo, BigInteger> {

    Page<HistorialRecaudo> findAll(Specification<HistorialRecaudo> specification, Pageable pageable);

    List<HistorialRecaudo> findAll(Specification<HistorialRecaudo> specification);

    HistorialRecaudo findTopByRecaudoCargoServicioContrato(Contrato contrato);

    Integer countByFechaReporteAfterAndRecaudoCargoServicioContrato(LocalDateTime date, Contrato contrato);

}
