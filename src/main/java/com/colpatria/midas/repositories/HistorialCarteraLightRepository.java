package com.colpatria.midas.repositories;

import com.colpatria.midas.model.HistorialCarteraLight;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface HistorialCarteraLightRepository  extends PagingAndSortingRepository<HistorialCarteraLight, BigInteger>, JpaRepository<HistorialCarteraLight, BigInteger> {

    @Modifying
    @Query(value = "UPDATE HISTORIAL_CARTERA_LIGHT SET SERVICIO_NUMERO = ?1 WHERE ID = ?2", nativeQuery = true)
    int updateHistorialCarteraServicio(String servicioNumero, BigInteger historialCarteraId);

    Page<HistorialCarteraLight> findAll(Specification<HistorialCarteraLight> specification, Pageable pageable );

    Page<HistorialCarteraLight> getHistorialCarteraLightByFechaReporte(LocalDate fechaReporte, Pageable pageable);

    @Query(value = "SELECT DISTINCT TOP 2 FECHA_REPORTE from HISTORIAL_CARTERA_LIGHT order by FECHA_REPORTE DESC", nativeQuery = true)
    List<String> getLastTwoLoads();

    List<HistorialCarteraLight> getHistorialCarteraLightsByFechaReporte(LocalDate date);

    Integer countHistorialCarteraLightByFechaReporteAfter(LocalDate date);

    Page<HistorialCarteraLight> getHistorialCarteraLightsByFechaReporte(LocalDate date, Pageable pageable);

    @Query(value = "SELECT TOP 1 FECHA_REPORTE from HISTORIAL_CARTERA_LIGHT order by FECHA_REPORTE DESC", nativeQuery = true)
    String getLastLoad();

}
