package com.colpatria.midas.repositories;

import com.colpatria.midas.model.HistorialArchivos;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface HistorialArchivosRepository extends JpaRepository<HistorialArchivos, String> {
    HistorialArchivos getHistorialArchivosByNombreArchivo(String nombreArchivo);
    Set<HistorialArchivos> getHistorialArchivosByNombreArchivoStartingWith(String prefix);
    Set<HistorialArchivos> getHistorialArchivosByEstado(HistorialArchivos.Status status);
}
