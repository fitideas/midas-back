package com.colpatria.midas.repositories;

import com.colpatria.midas.model.Attributes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Attributes Repository
 */
@Repository
public interface AttributesRepository extends JpaRepository<Attributes, String> {

    /**
     * Map an attribute given a key
     * @param key key value
     * @return mapped value
     */
    @Query("SELECT a.value FROM Attributes a WHERE a.key = ?1")
    String getAttributesValueByKey(String key);

}
