package com.colpatria.midas.repositories;

import com.colpatria.midas.model.EmailSettings;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Repositorio que implementa Jpa para conectar
 * la tabla EMAIL_SETTINGS en la db.
 */
@Repository
public interface EmailSettingsRepository extends JpaRepository<EmailSettings, String> {
/**
 * Metodos
 */
}