package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.constants.CacheConstants;
import com.colpatria.midas.model.Cuenta;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
 *
 * Interfaz
 *
*/

@Repository
public interface CuentaRepository extends JpaRepository<Cuenta, Double> {

    /*
     *
     * Métodos
     *
    */

    @Cacheable(cacheNames= CacheConstants.cuenta, key = "#numero")
    Cuenta getByNumero( Double numero );

}