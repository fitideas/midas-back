package com.colpatria.midas.repositories;

import com.colpatria.midas.model.HistorialCarteraLight;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PagedHistorialCarteraLigthRepository extends PagingAndSortingRepository<HistorialCarteraLight,Integer> {

    @Query(value= "SELECT C.* FROM HISTORIAL_CARTERA_LIGHT C \n " +
            "WHERE C.SERVICIO_NUMERO IN (SELECT NUMERO FROM SERVICIO WHERE CONTRATO_NUMERO = ?1)"
            /*"\n" +
            "INNER JOIN SERVICIO S ON C.SERVICIO_NUMERO = S.NUMERO\n" +
            "INNER JOIN CONTRATO CON ON S.CONTRATO_NUMERO = CON.NUMERO\n" +
            "INNER JOIN CLIENTE CL ON CON.CLIENTE_NUMERO_ID =  CL.NUMERO_IDENTIFICACION\n"+
            "INNER JOIN CUENTA CTA ON CL.CUENTA_NUMERO=CTA.NUMERO\n"+
            "WHERE CON.NUMERO = ?1 "*/, nativeQuery = true)
    Page<HistorialCarteraLight> findAllCarteraByContrato(Long contratoNumero, Pageable pageable);

    @Query(value= "SELECT C.* FROM HISTORIAL_CARTERA_LIGHT C \n " +
            "INNER JOIN SERVICIO S ON C.SERVICIO_NUMERO = S.NUMERO\n" +
            "INNER JOIN CONTRATO CON ON S.CONTRATO_NUMERO = CON.NUMERO\n" +
            "INNER JOIN CLIENTE CL ON CON.CLIENTE_NUMERO_ID =  CL.NUMERO_IDENTIFICACION\n"+
            "INNER JOIN CUENTA CTA ON CL.CUENTA_NUMERO=CTA.NUMERO\n"+
            "WHERE CTA.NUMERO = ?1 ", nativeQuery = true)
    Page<HistorialCarteraLight> findAllCarteraByCuenta(String cuentaNumero, Pageable pageable);

    @Query(value= "SELECT C.* FROM HISTORIAL_CARTERA_LIGHT C \n " +
            "INNER JOIN SERVICIO S ON C.SERVICIO_NUMERO = S.NUMERO\n" +
            "INNER JOIN CONTRATO CON ON S.CONTRATO_NUMERO = CON.NUMERO\n" +
            "INNER JOIN CLIENTE CL ON CON.CLIENTE_NUMERO_ID =  CL.NUMERO_IDENTIFICACION\n"+
            "INNER JOIN CUENTA CTA ON CL.CUENTA_NUMERO=CTA.NUMERO\n"+
            "WHERE CL.TIPO_IDENTIFICACION = ?1 AND CL.NUMERO_IDENTIFICACION=?2 ", nativeQuery = true)
    Page<HistorialCarteraLight> findAllCarteraByCliente(Integer tipoIdentificacion, String Identificacion, Pageable pageable);
}
