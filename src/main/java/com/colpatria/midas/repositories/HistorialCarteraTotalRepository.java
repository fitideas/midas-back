package com.colpatria.midas.repositories;

import com.colpatria.midas.model.HistorialCarteraTotal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;


@Repository
public interface HistorialCarteraTotalRepository extends JpaRepository<HistorialCarteraTotal, BigInteger> {

    List<HistorialCarteraTotal> getHistorialCarteraTotalByFechaReporteBetweenOrderByFechaReporteDesc(LocalDate start, LocalDate fin);

}
