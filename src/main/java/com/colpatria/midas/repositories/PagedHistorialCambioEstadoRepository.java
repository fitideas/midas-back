package com.colpatria.midas.repositories;

import com.colpatria.midas.model.HistorialCambioEstado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigInteger;
import java.util.List;

/**
 * Paged Historial Cambio Estado Repository
 */
public interface PagedHistorialCambioEstadoRepository extends PagingAndSortingRepository<HistorialCambioEstado, BigInteger> {
    /**
     * FindAll registers
     * @param specification query specification to filter
     * @param pageable pagination configuration
     * @return filtered paginated registers
     */
    Page<HistorialCambioEstado> findAll(Specification<HistorialCambioEstado> specification, Pageable pageable);
    /**
     * FindAll registers
     * @param specification query specification to filter
     * @return filtered registers
     */
    List<HistorialCambioEstado> findAll(Specification<HistorialCambioEstado> specification);
}
