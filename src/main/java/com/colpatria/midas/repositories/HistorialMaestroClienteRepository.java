package com.colpatria.midas.repositories;

/*
 *
 * Libraries
 *
*/

import com.colpatria.midas.model.HistorialMaestroCliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.math.BigInteger;


/**
 * History repository to read and write to the database
*/
@Repository
public interface HistorialMaestroClienteRepository extends JpaRepository<HistorialMaestroCliente, BigInteger> {

}