package com.colpatria.midas.repositories;

import java.math.BigInteger;
import java.util.List;

import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.HistorialCarteraResumen;
import com.colpatria.midas.model.HistorialRefinanciacion;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistorialRefinanciacionRepository extends JpaRepository<HistorialRefinanciacion, BigInteger> {
    Page<HistorialRefinanciacion> findAll(Specification<HistorialRefinanciacion> specification, Pageable pageable );

    List<HistorialRefinanciacion> findAll(Specification<HistorialRefinanciacion> specification);

    Integer countByServicioContratoCliente(Cliente cliente);

}
