package com.colpatria.midas.repositories;

/*
 *
 * Libraries
 *
*/

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.colpatria.midas.model.Estado;
import org.springframework.cache.annotation.Cacheable;
import com.colpatria.midas.constants.CacheConstants;

/**
 * History repository to read and write to the database
*/
@Repository
public interface EstadoRepository extends JpaRepository<Estado, String> {

    /**
     * Method that gets the entity from the database
     * @param codigo is the entity code
     * @return the entity
    */
    @Cacheable( cacheNames= CacheConstants.departamento, key = "#id" )
    Estado getEstadoById( String id );

}