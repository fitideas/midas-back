package com.colpatria.midas.repositories;

/*
 *
 * Libraries
 *
*/

import com.colpatria.midas.model.HistorialCalendarioFacturacion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

/**
 * Billing Schedule History Repository
 */
public interface HistorialCalendarioFacturacionRepository extends JpaRepository<HistorialCalendarioFacturacion, BigInteger> {
    HistorialCalendarioFacturacion getFirstByOrderByFechaDesc();
    boolean existsByVersion(String version);
}