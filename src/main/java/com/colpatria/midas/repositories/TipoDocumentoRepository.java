package com.colpatria.midas.repositories;

import com.colpatria.midas.constants.CacheConstants;
import com.colpatria.midas.model.TipoDocumento;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoDocumentoRepository extends JpaRepository<TipoDocumento, String> {
    @Cacheable(cacheNames= CacheConstants.tipoIdentificacion, key = "#codigo")
    TipoDocumento getTipoDocumentoByCodigo(String codigo);
}
