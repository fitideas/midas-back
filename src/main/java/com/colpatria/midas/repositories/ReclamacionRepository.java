package com.colpatria.midas.repositories;

import com.colpatria.midas.model.Reclamacion;
import com.colpatria.midas.model.TipoDocumento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 *
 */
public interface ReclamacionRepository extends JpaRepository<Reclamacion, Integer> {
    Optional<Reclamacion> findByTipoDocumentoAndNumeroDocumentoAndVigenteTrue(TipoDocumento tipoDocumento,
                                                                              Double numeroDocumento);
    List<Reclamacion> getAllByVigenteTrue();
}