package com.colpatria.midas.repositories;

import com.colpatria.midas.model.HistorialCondonacion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Interface to manage the Condonacion entity in the database
 */
@Repository
public interface HistorialCondonacionRepository extends JpaRepository<HistorialCondonacion, BigInteger>  {
        HistorialCondonacion getCondonacionById(BigInteger id);
        public Page<HistorialCondonacion> findAll(Specification<HistorialCondonacion> specification, Pageable pageable);
        public List<HistorialCondonacion> findAll(Specification<HistorialCondonacion> specification);
}
