package com.colpatria.midas.repositories;

import com.colpatria.midas.constants.CacheConstants;
import com.colpatria.midas.model.Cargo;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CargoRepository extends JpaRepository<Cargo, String> {

    @Cacheable(cacheNames= CacheConstants.cargo, key = "#Id")
    Cargo getCargoById(String Id);

    Cargo getCargoByIdAndCargoColpatria(String Id, Boolean CargoColpatria);
}
