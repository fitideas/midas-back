package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.constants.CacheConstants;
import com.colpatria.midas.model.Sucursal;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
 *
 * Interfaz
 *
*/

/**
 * Interface with all methods to use table sucursal in DB
 */
@Repository
public interface SucursalRepository extends JpaRepository<Sucursal, Integer>{

    /*
     *
     * Métodos
     *
    */

    /**
     * @param numero find sucursal by id
     * @return sucursal find
     */
    @Cacheable(cacheNames= CacheConstants.sucursal, key = "#numero")
    Sucursal getSucursalByNumero(Integer numero);

}
