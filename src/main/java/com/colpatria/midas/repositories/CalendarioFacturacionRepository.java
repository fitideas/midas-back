package com.colpatria.midas.repositories;

import com.colpatria.midas.model.CalendarioFacturacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigInteger;
import java.time.LocalDate;

/**
 * Billing Schedule Repository
 */
public interface CalendarioFacturacionRepository extends JpaRepository<CalendarioFacturacion, BigInteger> {
    boolean existsByVersionAndPeriodo(String version, LocalDate periodo);

    /**
     * find the entity by the state and cycle
     * @param estado associated with 'historial calendario facturacion'
     * @param fechaVencimiento associated with 'historial calendario facturacion'
     * @return the 'historial calendario facturacion' that match the filters
    */
    @Query(value = "SELECT * FROM CALENDARIO_FACTURACION r " +
        "WHERE r.CICLO = ?1 AND r.PRIMER_VENCIMIENTO > ?2 AND r.SUCURSAL = ?3 AND r.VERSION = ?4", nativeQuery = true)
        CalendarioFacturacion getByCicloAndFechaVencimientoAndSucursalAndVersion( Integer ciclo, LocalDate fechaVencimiento, Integer sucursal, String version );

    @Query(value = "SELECT MAX(VERSION) from CALENDARIO_FACTURACION r " +
        "WHERE r.CICLO = ?1 AND r.PRIMER_VENCIMIENTO > ?2 AND r.SUCURSAL = ?3", nativeQuery = true)
    String getLastVersionByCicloAndFechaVencimientoAndSucursal( Integer ciclo, LocalDate fechaVencimiento, Integer sucursal );

    CalendarioFacturacion getCalendarioFacturacionByCicloAndFechaFacturacionAfterAndVersion(Integer ciclo, LocalDate fechaFacturacion, String version);
}