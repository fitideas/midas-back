package com.colpatria.midas.repositories;

import com.colpatria.midas.model.RecaudoCargo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecaudoCargoRepository extends JpaRepository<RecaudoCargo, String> {
	
}