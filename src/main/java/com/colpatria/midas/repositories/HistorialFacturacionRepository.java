package com.colpatria.midas.repositories;

import com.colpatria.midas.model.HistorialFacturacion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistorialFacturacionRepository extends JpaRepository<HistorialFacturacion, Integer> {
}
