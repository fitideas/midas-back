package com.colpatria.midas.repositories;

import com.colpatria.midas.model.HistorialTraslado;
import com.colpatria.midas.model.InfoFinalizacion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

/**
 * Repository to InfoFinalizacion
 */
public interface InfoFinalizacionRepository extends JpaRepository<InfoFinalizacion, BigInteger> {
}
