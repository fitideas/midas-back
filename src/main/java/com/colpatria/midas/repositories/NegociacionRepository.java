package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.model.Negociacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
 *
 * Interfaz
 *
*/

@Repository
public interface NegociacionRepository extends JpaRepository<Negociacion, String> {

    /*
     *
     * Métodos
     *
    */

    Negociacion getNegociacionById( String id );

}