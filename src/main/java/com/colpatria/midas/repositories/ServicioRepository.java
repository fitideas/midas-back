package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.constants.CacheConstants;
import com.colpatria.midas.model.Servicio;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
 *
 * Interfaz
 *
*/

@Repository
public interface ServicioRepository extends JpaRepository<Servicio, String> {

    /*
     *
     * Métodos
     *
    */
    @Cacheable(cacheNames= CacheConstants.service, key = "#numero")
    Servicio getByNumero( String numero );
	
}