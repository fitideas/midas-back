package com.colpatria.midas.repositories;

import com.colpatria.midas.model.CategoriaCuenta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaRepository extends JpaRepository<CategoriaCuenta, String> {
    CategoriaCuenta getCategoriaById(String id);
}
