package com.colpatria.midas.repositories;


import com.colpatria.midas.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface HistorialRecaudoResumenRepository extends JpaRepository<HistorialRecaudoResumen, BigInteger> {
    HistorialRecaudoResumen getHistorialRecaudoResumenByContratoAndFechaPagoAndFechaProcesoPago(Contrato contrato, LocalDate fechaPago, LocalDate fechaProcesoPago);

    HistorialRecaudoResumen getTopByContrato(Contrato contrato);

    List<HistorialRecaudoResumen> findAll(Specification<HistorialRecaudoResumen> specification);

    Page<HistorialRecaudoResumen> findAll(Specification<HistorialRecaudoResumen> specification, Pageable pageable);
}
