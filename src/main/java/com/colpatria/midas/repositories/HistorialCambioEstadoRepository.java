package com.colpatria.midas.repositories;

import java.math.BigInteger;

import com.colpatria.midas.model.HistorialCambioEstado;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HistorialCambioEstadoRepository extends JpaRepository<HistorialCambioEstado, BigInteger> {
    
}
