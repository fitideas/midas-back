package com.colpatria.midas.repositories;

/*
 *
 * Libraries
 *
*/

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.math.BigInteger;
import com.colpatria.midas.model.Reparto;

/**
 * History repository to read and write to the database
*/
@Repository
public interface RepartoRepository extends JpaRepository<Reparto, BigInteger> {

}