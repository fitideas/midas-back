package com.colpatria.midas.repositories;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.HistorialFacturacionConceptoResumen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Interface to save 'historial facturacion concepto resumen' into database
 */
@Repository
public interface HistorialFacturacionConceptoResumenRepository extends JpaRepository<HistorialFacturacionConceptoResumen, BigInteger> {

    /**
     * find the entity by the filters provided
     * @param contrato associated with 'historial facturacion concepto resumen'
     * @param fechaProceso associated with 'historial facturacion concepto resumen'
     * @param fechaDocumento associated with 'historial facturacion concepto resumen'
     * @param descripcion associated with 'historial facturacion concepto resumen'
     * @return the 'historial facturacion concepto resumen' that match the filters
     */
    @Query("SELECT r FROM HistorialFacturacionConceptoResumen r WHERE r.contrato = ?1 and r.fechaProceso = ?2 and r.fechaDocumento = ?3")
    HistorialFacturacionConceptoResumen getResumenByFilters(Contrato contrato, LocalDate fechaProceso, LocalDate fechaDocumento);

    /**
     * find the entity by the filters provided
     * @param contrato associated with 'historial facturacion concepto resumen'
     * @return the 'historial facturacion concepto resumen' that match the filters
     */
    @Query("SELECT r FROM HistorialFacturacionConceptoResumen r WHERE r.contrato.cliente = ?1")
    List<HistorialFacturacionConceptoResumen> getResumenByFilters(Cliente cliente);

    /**
     * find the entity by the filters provided
     * @param specification that contains the filters to search the respective entities
     * @param pageable object for pagination
     * @return a page of entities
     */
    Page<HistorialFacturacionConceptoResumen> findAll(Specification<HistorialFacturacionConceptoResumen> specification, Pageable pageable );

    /**
     * find the entity by the filters provided
     * @param specification that contains the filters to search the respective entities
     * @param pageable object for paginatio
     * @return a list of entities
     */
    List<HistorialFacturacionConceptoResumen> findAll(Specification<HistorialFacturacionConceptoResumen> specification);
}
