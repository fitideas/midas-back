package com.colpatria.midas.repositories;

/*
*
* Libraries
*
*/

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import com.colpatria.midas.model.HistorialRelacionClienteSubproducto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * History repository to read and write to the database
*/
@Repository
public interface HistorialRelacionClienteSubproductoRepository extends JpaRepository<HistorialRelacionClienteSubproducto, BigInteger> {

    @Query(value = "SELECT * from HISTORIAL_RELACION_CLIENTE_SUBPRODUCTO r " +
        "INNER JOIN CONTRATO con ON r.CONTRATO_NUMERO = con.NUMERO " +
        "WHERE r.FECHA_INICIAL_CASTIGO IS NOT NULL AND r.FECHA_FIN_CASTIGO IS NULL AND " +
        "con.CLIENTE_NUMERO_ID = ?1 AND r.FECHA_GENERACION = ?2", nativeQuery = true)
    List<HistorialRelacionClienteSubproducto> getByCastigoAndClienteAndFechaGeneracion( Double clienteId, LocalDate fechaGeneracion );

    @Query(value = "SELECT TOP 1 FECHA_GENERACION from HISTORIAL_RELACION_CLIENTE_SUBPRODUCTO order by FECHA_GENERACION DESC", nativeQuery = true)
    String getLastLoad();

    HistorialRelacionClienteSubproducto getByFechaInicialCastigoNotNullAndFechaFinCastigoNullAndContratoNumeroAndFechaGeneracion( String numeroContrato, LocalDate fechaGeneracion );

}