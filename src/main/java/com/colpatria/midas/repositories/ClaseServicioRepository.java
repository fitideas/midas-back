package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.model.ClaseServicio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
 *
 * Interfaz
 *
*/

@Repository
public interface ClaseServicioRepository extends JpaRepository<ClaseServicio, String> {

    /*
     *
     * Métodos
     *
    */

    ClaseServicio getClaseServicioById( String id );
	
}