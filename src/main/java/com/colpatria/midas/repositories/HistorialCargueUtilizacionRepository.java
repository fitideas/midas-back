package com.colpatria.midas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import java.math.BigInteger;
import com.colpatria.midas.model.HistorialCargueUtilizacion;
/**
 * Interface to save 'historial cargue utilizacion' into database
 */

public interface HistorialCargueUtilizacionRepository extends JpaRepository<HistorialCargueUtilizacion, BigInteger> {
    
}
