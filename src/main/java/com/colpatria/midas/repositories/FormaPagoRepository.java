package com.colpatria.midas.repositories;

import com.colpatria.midas.model.FormaPago;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormaPagoRepository extends JpaRepository<FormaPago, String> {
    FormaPago getFormaPagoById(String Id);
}
