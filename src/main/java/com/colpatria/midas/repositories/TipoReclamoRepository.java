package com.colpatria.midas.repositories;

import com.colpatria.midas.model.TipoReclamo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoReclamoRepository extends JpaRepository<TipoReclamo, String> {
}