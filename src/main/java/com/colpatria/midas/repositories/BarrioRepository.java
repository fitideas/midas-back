package com.colpatria.midas.repositories;

/*
 *
 * Libraries
 *
*/

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.colpatria.midas.model.Barrio;

/**
 * History repository to read and write to the database
*/
@Repository
public interface BarrioRepository extends JpaRepository<Barrio, String> {

    /**
     * Method that gets the entity from the database
     * @param codigo is the entity code
     * @return the entity
    */
    Barrio getByCodigo( String codigo );

}