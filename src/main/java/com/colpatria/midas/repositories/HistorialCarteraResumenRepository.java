package com.colpatria.midas.repositories;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.HistorialCarteraResumen;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface HistorialCarteraResumenRepository extends PagingAndSortingRepository<HistorialCarteraResumen, BigInteger>, JpaSpecificationExecutor<HistorialCarteraResumen> {

    HistorialCarteraResumen getResumenByContratoAndFechaReporte(Contrato contrato, LocalDate fechaReporte);

    @Query(value = "SELECT TOP 1 * FROM HISTORIAL_CARTERA_RESUMEN WHERE CONTRATO_NUMERO = :contrato ORDER by FECHA_REPORTE DESC", nativeQuery = true)
    HistorialCarteraResumen getLastResumenByContrato( @Param( "contrato" ) String contrato);

    @Query(value = "select distinct top 2 cartera.FECHA_REPORTE from HISTORIAL_CARTERA_RESUMEN cartera order by cartera.FECHA_REPORTE desc", nativeQuery = true)
    List<String> getLastTwoLoads();

    List<HistorialCarteraResumen> getByFechaReporte(LocalDate date);

    Page<HistorialCarteraResumen> findAll( Specification<HistorialCarteraResumen> specification, Pageable pageable );

    List<HistorialCarteraResumen> findAll( Specification<HistorialCarteraResumen> specification );

    List<HistorialCarteraResumen> findHistorialCarteraResumenBySumaSaldoDeudaLessThanAndMoraPrimerVencimientoMaximoBetween(Double saldo, Integer minDate, Integer maxDate);

    /**
     * find the entity by the state and cycle
     * @param estado associated with 'historial cartera'
     * @param ciclo associated with 'historial cartera'
     * @return the 'historial cartera' that match the filters
    */
    @Query(value = "SELECT * FROM HISTORIAL_CARTERA_RESUMEN r " +
        "INNER JOIN CONTRATO con ON r.CONTRATO_NUMERO = con.NUMERO " +
        "INNER JOIN CLIENTE cli ON con.CLIENTE_NUMERO_ID = cli.NUMERO_IDENTIFICACION " +
        "INNER JOIN CUENTA cue ON cli.CUENTA_NUMERO = cue.NUMERO " +
        "WHERE cue.CICLO_FACTURACION = :ciclo AND con.ESTADO = 'ACTIVO' AND r.MORA_PRIMER_VENCIMIENTO_MAXIMO > 0", nativeQuery = true)
    public List<HistorialCarteraResumen> getByCicloAndEstadoActivo( @Param( "ciclo" ) Integer ciclo );

    /**
     * find the entity by the client id
     * @param clienteId associated with 'historial cartera'
     * @return the 'historial cartera' that match the filters
    */
    @Query(value = "SELECT * FROM HISTORIAL_CARTERA_RESUMEN r " +
        "INNER JOIN CONTRATO con ON r.CONTRATO_NUMERO = con.NUMERO " +
        "WHERE con.CLIENTE_NUMERO_ID = ?1", nativeQuery = true)
    public List<HistorialCarteraResumen> getByCliente( Double clienteId );

    /**
     * find the entity by the client id
     * @param numeroContrato associated with 'historial cartera'
     * @return the 'historial cartera' that match the filters
    */
    public List<HistorialCarteraResumen> getByContratoNumeroAndFechaReporte( String numeroContrato, LocalDate fechaReporte );

    /**
     * find the entity by the state and cycle
     * @param clienteId associated with 'historial cartera'
     * @return the 'historial cartera' that match the filters
    */
    @Query(value = "SELECT * FROM HISTORIAL_CARTERA_RESUMEN r " +
        "WHERE ( SELECT MAX( t.MORA_PRIMER_VENCIMIENTO_MAXIMO ) FROM HISTORIAL_CARTERA_RESUMEN t " +
        "INNER JOIN CONTRATO con ON t.CONTRATO_NUMERO = con.NUMERO " +
        "WHERE con.CLIENTE_NUMERO_ID = :clienteId ) > 0", nativeQuery = true)
    public List<HistorialCarteraResumen> getByClienteAndDiasMoraMayorAUno( @Param("clienteId") Double clienteId );

    /**
     * find the entity by the state and cycle
     * @param clienteId associated with 'historial cartera'
     * @return the 'historial cartera' that match the filters
    */
    @Query(value = "SELECT MAX( r.MORA_PRIMER_VENCIMIENTO_MAXIMO ) FROM HISTORIAL_CARTERA_RESUMEN r " +
        "INNER JOIN CONTRATO con ON r.CONTRATO_NUMERO = con.NUMERO " +
        "WHERE con.CLIENTE_NUMERO_ID = :clienteId", nativeQuery = true)
    public Long getMaxDiasMoraByCliente( @Param("clienteId") Double clienteId );

     /** find the entity by the contratonumber
     * @param numero associated with 'historial cartera'
     * @return the 'historial cartera' that match the filters
     */
    @Query(value = "SELECT * FROM HISTORIAL_CARTERA_RESUMEN r " +
            "INNER JOIN CONTRATO con ON r.CONTRATO_NUMERO = con.NUMERO " +
            "WHERE r.CONTRATO_NUMERO = ?1 and r.FECHA_REPORTE < ?2 order by r.FECHA_REPORTE desc", nativeQuery = true)
    public List<HistorialCarteraResumen> getByContrato(String numero,
                                                       LocalDate fechaAplicacion);

    @Query(value = "SELECT TOP 1 FECHA_REPORTE from HISTORIAL_CARTERA_RESUMEN order by FECHA_REPORTE DESC", nativeQuery = true)
    String getLastLoad();
                                                       
}
