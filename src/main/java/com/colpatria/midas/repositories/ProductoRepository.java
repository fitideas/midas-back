package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.constants.CacheConstants;
import com.colpatria.midas.model.Producto;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/*
 *
 * Interfaz
 *
*/

/**
 * Product repository
 */
@Repository
public interface ProductoRepository extends JpaRepository<Producto, String> {

    /*
     *
     * Métodos
     *
     */

    /**
     * @param codigo to find product
     * @return product found
     */
    @Cacheable(cacheNames= CacheConstants.producto, key = "#codigo")
    Producto getProductoByCodigo(String codigo);

    /**
     * @param codigo to find product
     * @return product found
     */
    @Cacheable(cacheNames= CacheConstants.producto, key = "#codigo")
    Producto getByCodigo( String codigo );

    @Query("SELECT p FROM Producto p WHERE p.productoPadre IS NOT NULL")
    Collection<Producto> getSubProductList();

}