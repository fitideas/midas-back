package com.colpatria.midas.repositories;

import com.colpatria.midas.constants.CacheConstants;
import com.colpatria.midas.model.TipoProducto;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface with all methods to use table tipoProducto in DB
 */
@Repository
public interface TipoProductoRepository extends JpaRepository<TipoProducto, String> {
    /**
     * @param codigo to find tipo producto
     * @return Tipo producto found
     */
    @Cacheable(cacheNames= CacheConstants.tipoProducto, key = "#codigo")
    TipoProducto getTipoProductoByCodigo(String codigo);
}
