package com.colpatria.midas.repositories;

import java.math.BigInteger;
import com.colpatria.midas.model.HistorialTraslado;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

public interface HistorialTrasladoRepository extends JpaRepository<HistorialTraslado, BigInteger> {
    
    /**
     * find the entity by the filters provided
     * @param specification that contains the filters to search the respective entities
     * @param pageable object for pagination
     * @return a page of entities
     */
    Page<HistorialTraslado> findAll(Specification<HistorialTraslado> specification, Pageable pageable );

    /**
     * find the entity by the filters provided
     * @param specification that contains the filters to search the respective entities
     * @param pageable object for paginatio
     * @return a list of entities
     */
    List<HistorialTraslado> findAll(Specification<HistorialTraslado> specification);
}
