package com.colpatria.midas.repositories;

import com.colpatria.midas.model.GestionCobroAction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GestionCobroActionRepository extends JpaRepository<GestionCobroAction, String> {
}