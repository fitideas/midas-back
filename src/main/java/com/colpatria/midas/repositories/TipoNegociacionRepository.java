package com.colpatria.midas.repositories;

import com.colpatria.midas.model.TipoDocumento;
import com.colpatria.midas.model.TipoNegociacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface to manage the TipoNegociacion entity in the database
 */
@Repository
public interface TipoNegociacionRepository extends JpaRepository<TipoNegociacion, Integer> {
    TipoNegociacion getTipoNegociacionById(Integer id);
}
