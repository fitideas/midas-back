package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.model.TipoConsumo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
 *
 * Interfaz
 *
*/

@Repository
public interface TipoConsumoRepository extends JpaRepository<TipoConsumo, Integer> {

    /*
     *
     * Métodos
     *
    */

    TipoConsumo getTipoConsumoById( Integer id );
	
}