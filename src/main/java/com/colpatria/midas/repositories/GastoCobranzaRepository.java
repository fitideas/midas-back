package com.colpatria.midas.repositories;

import java.math.BigInteger;
import com.colpatria.midas.model.Contrato;
import com.colpatria.midas.model.GastoCobranza;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GastoCobranzaRepository extends JpaRepository<GastoCobranza, BigInteger> {

    @Query(value = "SELECT * FROM GASTOS_COBRANZA WHERE CAUSAL != '' or CAUSAL IS NOT NULL", nativeQuery = true)
    Page<GastoCobranza> getGastosCausalNoVacio(Pageable pageable);

    @Query(value = "SELECT * FROM GASTOS_COBRANZA WHERE CAUSAL = '' or CAUSAL IS NULL", nativeQuery = true)
    Page<GastoCobranza> getGastosCausalVacio(Pageable pageable);

    @Query(value = "SELECT COUNT(CAUSAL) FROM GASTOS_COBRANZA WHERE CAUSAL = '' or CAUSAL IS NULL", nativeQuery = true)
    Integer getCountRegistrosAfecto();

    @Query("SELECT r FROM GastoCobranza r WHERE r.historialCarteraResumen.contrato = ?1")
    GastoCobranza getGastoCobranzaByContrato(Contrato contrato);
}