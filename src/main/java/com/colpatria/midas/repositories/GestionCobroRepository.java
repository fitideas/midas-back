package com.colpatria.midas.repositories;

import com.colpatria.midas.model.GestionCobro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GestionCobroRepository extends JpaRepository<GestionCobro, Long> {
}