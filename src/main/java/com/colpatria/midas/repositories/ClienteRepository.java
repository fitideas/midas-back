package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.constants.CacheConstants;
import com.colpatria.midas.model.Cliente;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/*
 *
 * Interfaz
 *
*/

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Double> {

    /*
     *
     * Métodos
     *
    */
    @Cacheable(cacheNames= CacheConstants.cliente, key = "#numeroIdentificacion")
    Cliente getByNumeroIdentificacion( Double numeroIdentificacion );
    @Cacheable(cacheNames= CacheConstants.cliente)
    Optional<Cliente> getByNumeroIdentificacionAndTipoDocumentoCodigo(Double numeroIdentificacion, String tipoDocumento);
    @Cacheable(cacheNames= CacheConstants.cliente)
    Optional<Cliente> getByNumeroIdentificacionAndTipoDocumentoHomologacionCyber(Double numeroIdentificacion, String tipoDocumento);

    /**
     * find the entity by the client id
     * @param ciclo associated with 'cliente'
     * @return the 'cliente' that match the filters
    */
    @Query(value = "SELECT * FROM CLIENTE r " +
        "INNER JOIN CUENTA cue ON r.CUENTA_NUMERO = cue.NUMERO " +
        "WHERE cue.CICLO_FACTURACION = :ciclo", nativeQuery = true)
    List<Cliente> getByCiclo( @Param("ciclo") Integer ciclo );

}