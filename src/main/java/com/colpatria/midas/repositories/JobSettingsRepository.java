package com.colpatria.midas.repositories;

import com.colpatria.midas.model.JobSettings;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobSettingsRepository extends JpaRepository<JobSettings, String> {
    JobSettings getJobSettingsByJobName(String jobName);
}
