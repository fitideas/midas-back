package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.constants.CacheConstants;
import com.colpatria.midas.model.Contrato;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/*
 *
 * Interfaz
 *
*/

@Repository
public interface ContratoRepository extends JpaRepository<Contrato, String> {

    /*
     *
     * Métodos
     *
    */

    @Cacheable(cacheNames= CacheConstants.contrato, key = "#numero")
    Contrato getByNumero( String numero );

    /**
     * find the entity by the client id
     * @param clienteId associated with 'historial contrato'
     * @return the 'historial contrato' that match the filters
    */
    @Query(value = "SELECT * FROM CONTRATO r WHERE r.CLIENTE_NUMERO_ID = ?1", nativeQuery = true)
    List<Contrato> getByCliente( Double clienteId );

    /**
     * find the entity by the client id
     * @param clienteId associated with 'historial contrato'
     * @return the 'historial contrato' that match the filters
    */
    @Query(value = "SELECT * FROM CONTRATO r WHERE r.CLIENTE_NUMERO_ID = :clienteId AND r.ESTADO IN (:estados)", nativeQuery = true)
    List<Contrato> getByClienteAndEstados( @Param("clienteId") Double clienteId, @Param("estados") List<String> estados );

    /**
     * find the entity by the client id
     * @param clienteId associated with 'historial contrato'
     * @return the 'historial contrato' that match the filters
    */
    @Query(value = "SELECT * FROM CONTRATO r WHERE r.CLIENTE_NUMERO_ID = :clienteId AND r.FACTURACION IN (:estadosFacturacion)", nativeQuery = true)
    List<Contrato> getByClienteAndEstadosFacturacion( @Param("clienteId") Double clienteId, @Param("estadosFacturacion") List<Integer> estadosFacturacion );

}
