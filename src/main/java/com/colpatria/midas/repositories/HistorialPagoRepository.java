package com.colpatria.midas.repositories;

import java.math.BigInteger;

import com.colpatria.midas.model.HistorialPago;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository with all methods to historial de pago
 */
@Repository
public interface HistorialPagoRepository extends JpaRepository<HistorialPago, BigInteger> {

    /**
     * @param servicio service to fiend
     * @return Entity with historial de pago
     */
    HistorialPago getByServicio(String servicio);
}
