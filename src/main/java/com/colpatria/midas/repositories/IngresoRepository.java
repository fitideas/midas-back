package com.colpatria.midas.repositories;

import com.colpatria.midas.model.Ingreso;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * Ingreso repository to read and write to the database
 */
public interface IngresoRepository extends JpaRepository<Ingreso, String> {
}
