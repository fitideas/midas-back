package com.colpatria.midas.repositories;

import java.math.BigInteger;
import com.colpatria.midas.model.HistorialPeriodoGracia;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistorialPeriodoGraciaRepository extends JpaRepository<HistorialPeriodoGracia, BigInteger> {
    
}
