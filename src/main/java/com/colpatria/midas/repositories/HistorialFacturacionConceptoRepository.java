package com.colpatria.midas.repositories;

import java.math.BigInteger;
import java.util.List;

import com.colpatria.midas.model.HistorialFacturacionConcepto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

/**
 * Interface to save 'historial facturacion concepto' into database
 */
@Repository
public interface HistorialFacturacionConceptoRepository extends JpaRepository<HistorialFacturacionConcepto, BigInteger> {

    /**
     * find the entity by the filters provided
     * @param specification that contains the filters to search the respective entities
     * @param pageable object for paginatio
     * @return a list of entities
     */
    List<HistorialFacturacionConcepto> findAll(Specification<HistorialFacturacionConcepto> specification);
    
}
