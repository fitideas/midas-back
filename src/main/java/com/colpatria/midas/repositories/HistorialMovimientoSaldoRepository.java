package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.model.HistorialMovimientoSaldo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.math.BigInteger;

/*
 *
 * Interfaz
 *
*/

@Repository
public interface HistorialMovimientoSaldoRepository extends JpaRepository<HistorialMovimientoSaldo, BigInteger> {

}