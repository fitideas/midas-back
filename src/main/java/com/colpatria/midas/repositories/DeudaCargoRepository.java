package com.colpatria.midas.repositories;

import com.colpatria.midas.model.DeudaCargo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.math.BigInteger;

/**
 * Repository with deuda cargo data
 */
@Repository
public interface DeudaCargoRepository extends JpaRepository<DeudaCargo, BigInteger> {
}
