package com.colpatria.midas.repositories;

import java.math.BigInteger;

import com.colpatria.midas.model.HistorialAnulacionPago;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface to save 'historial anulacion pagos' into database
 */
public interface HistorialAnulacionPagoRepository extends JpaRepository<HistorialAnulacionPago, BigInteger> {
    
}
