package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.model.TipoTarjeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
 *
 * Interfaz
 *
*/

@Repository
public interface TipoTarjetaRepository extends JpaRepository<TipoTarjeta, String> {

    /*
     *
     * Métodos
     *
    */

    TipoTarjeta getTipoTarjetaById( String id );
	
}