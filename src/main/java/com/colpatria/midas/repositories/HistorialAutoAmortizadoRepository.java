package com.colpatria.midas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

import com.colpatria.midas.model.HistorialAutoAmortizado;
/**
 * Interface to save 'historial auto amortizados' into database
 */
public interface HistorialAutoAmortizadoRepository extends JpaRepository<HistorialAutoAmortizado, BigInteger>{
    
}
