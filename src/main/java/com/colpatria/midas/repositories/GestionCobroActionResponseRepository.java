package com.colpatria.midas.repositories;

import com.colpatria.midas.model.GestionCobroActionResponse;
import com.colpatria.midas.model.GestionCobroActionResponseId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GestionCobroActionResponseRepository extends JpaRepository<GestionCobroActionResponse, GestionCobroActionResponseId> {
}