package com.colpatria.midas.repositories;

/*
 *
 * Libraries
 *
*/

import com.colpatria.midas.model.Cliente;
import com.colpatria.midas.model.HistorialCartaMorosa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * History repository to read and write to the database
*/
public interface HistorialCartaMorosaRepository extends JpaRepository<HistorialCartaMorosa, BigInteger> {

    /**
     * find the entity by the state and cycle
     * @param estado associated with 'historial carta morosa'
     * @param ciclo associated with 'historial carta morosa'
     * @return the 'historial carta morosa' that match the filters
    */
    @Query(value = "SELECT count(r.ID) FROM HISTORIAL_CARTA_MOROSA r " +
        "INNER JOIN CONTRATO con ON r.CONTRATO_NUMERO = con.NUMERO " +
        "INNER JOIN CLIENTE cli ON con.CLIENTE_NUMERO_ID = cli.NUMERO_IDENTIFICACION " +
        "INNER JOIN CUENTA cue ON cli.CUENTA_NUMERO = cue.NUMERO " +
        "WHERE cue.CICLO_FACTURACION = ?1 AND r.FECHA_ELIMINADO IS NULL AND " +
        "YEAR(r.FECHA_GENERACION) = ?2 AND MONTH(r.FECHA_GENERACION) = ?3", nativeQuery = true)
    public long countByCicloAndPeriodo( Integer ciclo, Integer year, Integer month );

    /**
     * find the entity by the state and cycle
     * @param estado associated with 'historial carta morosa'
     * @param ciclo associated with 'historial carta morosa'
     * @return the 'historial carta morosa' that match the filters
    */
    @Query(value = "SELECT * FROM HISTORIAL_CARTA_MOROSA r " +
        "INNER JOIN CONTRATO con ON r.CONTRATO_NUMERO = con.NUMERO " +
        "INNER JOIN CLIENTE cli ON con.CLIENTE_NUMERO_ID = cli.NUMERO_IDENTIFICACION " +
        "INNER JOIN CUENTA cue ON cli.CUENTA_NUMERO = cue.NUMERO " +
        "WHERE cue.CICLO_FACTURACION = :ciclo AND r.FECHA_ELIMINADO IS NULL AND " +
        "YEAR(r.FECHA_GENERACION) = :year AND MONTH(r.FECHA_GENERACION) = :month AND "+
        "r.CLIENTE_IDENTIFICACION_NUMERO = :clienteId", nativeQuery = true)
    public List<HistorialCartaMorosa> getByCicloAndPeriodoAndCliente( Integer ciclo, Integer year, Integer month, Double clienteId );

    Optional<HistorialCartaMorosa> findByClienteAndFechaGeneracionAndFechaEliminadoNull(Cliente cliente, LocalDate fechaGeneracion);
}