package com.colpatria.midas.repositories;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
 *
 * Interfaz
 *
*/

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, String> {

    /*
     *
     * Métodos
     *
    */

    Usuario getById( String id );

    Usuario getUsuarioById(String id);

}
