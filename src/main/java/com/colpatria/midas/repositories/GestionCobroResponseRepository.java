package com.colpatria.midas.repositories;

import com.colpatria.midas.model.GestionCobroResponse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GestionCobroResponseRepository extends JpaRepository<GestionCobroResponse, String> {
}