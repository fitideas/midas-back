package com.colpatria.midas.repositories;

import com.colpatria.midas.model.TipoCargo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoCargoRepository extends JpaRepository<TipoCargo, Integer> {
}
