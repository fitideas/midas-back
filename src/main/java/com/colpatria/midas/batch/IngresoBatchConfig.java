package com.colpatria.midas.batch;

import com.colpatria.midas.dto.AnulacionPagoDto;
import com.colpatria.midas.dto.IngresoDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialAnulacionPago;
import com.colpatria.midas.model.Ingreso;
import com.colpatria.midas.processors.AnulacionPagoItemProcessor;
import com.colpatria.midas.processors.IngresoItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.AnulacionPagoItemWriter;
import com.colpatria.midas.writers.IngresoItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

/**
 * Configuration class for 'anulacion pagos' batch process
 */
@Configuration
public class IngresoBatchConfig {
    /**
     * Add logger to config
     */
    private static final Logger logger = LoggerFactory.getLogger(AnulacionPagosBatchConfig.class);

    /**
     * Add job builderFactory to config
     */
    public final JobBuilderFactory jobBuilderFactory;

    /**
     * Add step builder factory to config
     */
    public final StepBuilderFactory stepBuilderFactory;

    /**
     * add 'servicio' repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * add 'Ingreso' repository
     */
    private final IngresoRepository ingresoRepository;


    /**
     * add 'tipoProducto' repository
     */
    private final TipoProductoRepository tipoProductoRepository;

    /**
     * add 'producto' repository
     */
    private final ProductoRepository productoRepository;

    /**
     * add 'cliente' repository
     */
    private final ClienteRepository clienteRepository;

    /**
     * add 'contrato' repository
     */
    private final ContratoRepository contratoRepository;


    /**
     * add 'Tipo Documento' repository
     */
    private final TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Add email service
     */
    private final EmailService emailService;

    /**
     * Add file history service
     */
    private final FileHistoryService fileHistoryService;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * References the municipioRepository
     */
    private final MunicipioRepository municipioRepository;

    /**
     * References the municipioRepository
     */
    private final CargoRepository cargoRepository;

    /**
     * Cache manager to use
     */
    private final CacheManager cacheManager;

    /**
     * @param jobBuilderFactory dependency injection of jobBuilderFactory
     * @param stepBuilderFactory dependency injection of stepBuilderFactory
     * @param servicioRepository dependency injection of servicioRepository
     * @param ingresoRepository dependency injection of ingresoRepository
     * @param tipoProductoRepository dependency injection of tipoProductoRepository
     * @param productoRepository dependency injection of productoRepository
     * @param clienteRepository dependency injection of clienteRepository
     * @param contratoRepository dependency injection of contratoRepository
     * @param emailService dependency injection of emailService
     * @param fileHistoryService dependency injection of fileHistoryService
     * @param tipoDocumentoRepository dependency injection of tipoDocumentoRepository
     * @param validationService dependency injection of validationService
     * @param cacheManager DI of cache manager
     */
    @Autowired
    public IngresoBatchConfig(JobBuilderFactory jobBuilderFactory,
                              StepBuilderFactory stepBuilderFactory,
                              ServicioRepository servicioRepository,
                              IngresoRepository ingresoRepository,
                              TipoProductoRepository tipoProductoRepository,
                              ProductoRepository productoRepository,
                              ClienteRepository clienteRepository,
                              ContratoRepository contratoRepository,
                              MunicipioRepository municipioRepository,
                              EmailService emailService,
                              FileHistoryService fileHistoryService,
                              TipoDocumentoRepository tipoDocumentoRepository,
                              CargoRepository cargoRepository,
                              ValidationService validationService,
                              CacheManager cacheManager){
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.servicioRepository = servicioRepository;
        this.ingresoRepository = ingresoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.productoRepository = productoRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository = contratoRepository;
        this.municipioRepository = municipioRepository;
        this.emailService = emailService;
        this.fileHistoryService = fileHistoryService;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.cargoRepository = cargoRepository;
        this.validationService = validationService;
        this.cacheManager = cacheManager;
    }

    /**
     * Method that configures the file reading process
     * @param file to be read
     * @return item reader object that contains the configuration for reading the file in the batch process
     */
    @StepScope
    @Bean
    public FlatFileItemReader<IngresoDto> ingresoReader(@Value("#{jobParameters['file']}") String file) {
        DelimitedLineTokenizer tokenizer;
        DefaultLineMapper<IngresoDto> lineMapper;
        String[] columns = new String[]{
                "TIPO DOCUMENTO",
                "NUMERO DOCUMENTO",
                "FECHA DOCUMENTO",
                "NUMERO SERVICIO",
                "TASA INTERES CORRIENTE",
                "FECHA CAUSACION",
                "TASA INTERES MORA",
                "CODIGO CARGO",
                "MONTO",
                "SIGNO",
                "CUENTA CONTABLE",
                "CODIGO PRODUCTO",
                "NUMERO CONTRATO",
                "MUNICIPIO",
                "CODIGO SUBPRODUCTO"
        };
        tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter("|");
        tokenizer.setNames(
                "tipoDocumento",
                "numeroDocumento",
                "fechaDocumento",
                "numeroServicio",
                "tasaInteresCorriente",
                "fechaCausacion",
                "tasaInteresMora",
                "codigoCargo",
                "monto",
                "signo",
                "cuentaContable",
                "codigoProducto",
                "numeroContrato",
                "municipio",
                "codigoSubproducto"
        );
        lineMapper = new DefaultLineMapper<>();
        lineMapper.setFieldSetMapper(new BeanWrapperFieldSetMapper<IngresoDto>() {{setTargetType(IngresoDto.class);}});
        lineMapper.setLineTokenizer(tokenizer);
        logger.info(file);
        Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));
        CustomItemReader<IngresoDto> reader = new CustomItemReader<>(columns, "ingreso", resource, this.emailService, this.cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

    /**
     * Method that configures the writing process
     * @return item writer object that contains the configuration for writing in the database during the batch process
     */
    @StepScope
    @Bean
    public IngresoItemWriter ingresoWriter(){
        return new IngresoItemWriter(this.validationService, this.ingresoRepository, this.servicioRepository, this.contratoRepository, this.clienteRepository);
    }

    /**
     * Method that configures the processor for the batch process
     * @param fileName Name of the file to be read read
     * @return object of the porcessor class
     */
    @StepScope
    @Bean
    public IngresoItemProcessor itemProcessor(@Value("#{jobParameters['file']}") String fileName) {
        return new IngresoItemProcessor(
                fileName,
                this.servicioRepository,
                this.productoRepository,
                this.tipoProductoRepository,
                this.tipoDocumentoRepository,
                this.clienteRepository,
                this.municipioRepository,
                this.contratoRepository,
                this.cargoRepository,
                this.validationService,
                this.cacheManager
        );
    }

    /**
     * Method that configures the listener to be executed after the batch process
     * @return object of the listener class
     */
    @Bean
    public LoadJobListener ingresoJobListener(){
        return new LoadJobListener(this.emailService, this.fileHistoryService, validationService, cacheManager);
    }

    /**
     * Method that configures the job to be executed in the batch process
     * @param ingresoJobListener is the listener to be executed after the job
     * @param ingresoJobStep1 is the first step to be execute during the job
     * @return job object that contains the configuration for the job
     */
    @Bean(name = "ingresoJob")
    public Job IngresoJob(LoadJobListener ingresoJobListener, Step ingresoJobStep1) {
        return jobBuilderFactory.get("ingresoJob")
                .incrementer( new RunIdIncrementer() )
                .listener( ingresoJobListener )
                .flow( ingresoJobStep1 )
                .build()
                .build();
    }


    /**
     * Method that configures the first step to be executed in the batch process
     * @param ingresoReader is the reader to be used in the step
     * @param ingresoWriter is the writer to be used in the step
     * @param ingresoProcessor is the processor to be used in the step
     * @return object that contains the configuration of the first step in the job
     */
    @Bean
    public Step ingresoJobStep1(
            FlatFileItemReader<IngresoDto> ingresoReader,
            IngresoItemWriter ingresoWriter,
            IngresoItemProcessor ingresoProcessor
    ) {
        return stepBuilderFactory.get( "ingresoJobStep1" )
                .<IngresoDto, Ingreso> chunk( 1000 )
                .reader(ingresoReader)
                .writer(ingresoWriter)
                .processor(ingresoProcessor)
                .faultTolerant()
                .skip(VerificationException.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }


}
