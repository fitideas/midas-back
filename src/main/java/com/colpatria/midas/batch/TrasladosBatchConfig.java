package com.colpatria.midas.batch;

import com.colpatria.midas.dto.TrasladosDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialTraslado;
import com.colpatria.midas.processors.TrasladosItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.TrasladosItemWriter;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;


/**
 * Traslados Batch Configuration
 */
@Configuration
public class TrasladosBatchConfig {

    /**
     * Job Buiolder Factory
     */
    private final JobBuilderFactory jobBuilderFactory;

    /**
     * Step Builder Factory
     */
    private final StepBuilderFactory stepBuilderFactory;

    /**
     * Email Service
     */
    private final EmailService emailService;

    /**
     * File History Service
     */
    private final FileHistoryService fileHistoryService;

    /**
     * Validation service
     */
    private final ValidationService validationService;


    /**
     * Producto Repository
     */
    private final ProductoRepository productoRepository;

    /**
     * Tipo Producto Repository
     */
    private final TipoProductoRepository tipoProductoRepository;

    /**
     * Contrato Repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * Cliente Repository
     */
    private final ClienteRepository clienteRepository;

    /**
     * Cuenta Repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * Usuario Repository
     */
    private final UsuarioRepository usuarioRepository;

    /**
     * Historial Traslado Repository
     */
    private final HistorialTrasladoRepository historialTrasladoRepository;

    /**
     * Tipo Documento Repository
     */
    private final TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Cache manager to use
     */
    private final CacheManager cacheManager;

    /**
     * Contructor
     *  @param jobBuilderFactory           Job Builder Factory bean Reference
     * @param stepBuilderFactory          Step Builder Factory bean Reference
     * @param emailService                Email Service bean Reference
     * @param fileHistoryService          Fille History Service bean Reference
     * @param productoRepository          Producto Repository Bean Reference
     * @param tipoProductoRepository      Tipo Producto Repository Bean Reference
     * @param contratoRepository          Contrato Repository Bean Reference
     * @param clienteRepository           Cliente Repository Bean Reference
     * @param cuentaRepository            Cuenta Repository Bean Reference
     * @param usuarioRepository           Usuario Repository Bean Reference
     * @param historialTrasladoRepository Historial Traslado Bean Reference
     * @param tipoDocumentoRepository     Tipo Documento Repository Bean Reference
     * @param validationService           Validation Service Bean Reference
     * @param cacheManager DI of cache manager
     */
    @Autowired
    public TrasladosBatchConfig(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, EmailService emailService, FileHistoryService fileHistoryService, ProductoRepository productoRepository, TipoProductoRepository tipoProductoRepository, ContratoRepository contratoRepository, ClienteRepository clienteRepository, CuentaRepository cuentaRepository, UsuarioRepository usuarioRepository, HistorialTrasladoRepository historialTrasladoRepository, TipoDocumentoRepository tipoDocumentoRepository, ValidationService validationService, CacheManager cacheManager) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.emailService = emailService;
        this.fileHistoryService = fileHistoryService;
        this.productoRepository = productoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.contratoRepository = contratoRepository;
        this.clienteRepository = clienteRepository;
        this.cuentaRepository = cuentaRepository;
        this.usuarioRepository = usuarioRepository;
        this.historialTrasladoRepository = historialTrasladoRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.validationService = validationService;
        this.cacheManager = cacheManager;
    }

    /***
     * Returns a File item reader according to specifications of Traslados file structure
     * @param file file to read
     * @return The File Item Reader
     */
    @StepScope
    @Bean
    public CustomItemReader<TrasladosDto> trasladosReader(@Value("#{jobParameters['file']}") String file) {
        String[] columns = new String[]{"CODIGO PRODUCTO", "NOMBRE PRODUCTO", "CODIGO TIPO PRODUCTO", "NOMBRE TIPO PRODUCTO", "CODIGO SUBPRODUCTO", "NOMBRE SUBPRODUCTO", "NUMERO_CONTRATO (Relacion Cliente Sub)", "TIPO_IDENTIFICACION", "NRO_DOCTO_IDENT", "CUENTAACTUAL", "CUENTAANTERIOR", "FECHATRA", "USERNAME (usuario genera traslado)", "NOMBRE", "AREA", "CICLO_ANTERIOR", "CICLO_NUEVO", "OBSERVACION"};
        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter("|");
        tokenizer.setNames("codigoProducto", "nombreProducto", "codigoTipoProducto", "nombreTipoProducto", "codigoSubproducto", "nombreSubproducto", "numeroContrato", "tipoIdentificacion", "numeroDocumento", "cuentaActual", "cuentaAnterior", "FechaTraslado", "username", "nombre", "area", "cicloAnterior", "cicloNuevo", "observacion");
        DefaultLineMapper<TrasladosDto> lineMapper = new DefaultLineMapper<>();
        BeanWrapperFieldSetMapper<TrasladosDto> wrapper = new BeanWrapperFieldSetMapper<>();
        wrapper.setTargetType(TrasladosDto.class);
        wrapper.setDistanceLimit(0);
        lineMapper.setFieldSetMapper(wrapper);
        lineMapper.setLineTokenizer(tokenizer);
        Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));
        CustomItemReader<TrasladosDto> reader = new CustomItemReader<>(columns, "traslados", resource, this.emailService, cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

    /**
     * Returns an item writer according to specifications of Traslados's Fields
     *
     * @return The traslados item writer
     */
    @StepScope
    @Bean
    public TrasladosItemWriter trasladosWriter() {
        TrasladosItemWriter writer = new TrasladosItemWriter(this.validationService, this.historialTrasladoRepository);
        writer.setCuentaRepository(this.cuentaRepository);
        writer.setUsuarioRepository(this.usuarioRepository);
        writer.setContratoRepository(this.contratoRepository);
        writer.setClienteRepository(this.clienteRepository);
        return writer;
    }

    /***
     * Returns an object processor of TrasladosDto to TrasladosProcessed
     * @return The Traslados item processor
     */
    @StepScope
    @Bean
    public TrasladosItemProcessor trasladosItemProcessor(@Value("#{jobParameters['file']}") String file) {
        TrasladosItemProcessor processor = new TrasladosItemProcessor(file);
        processor.setValidationService(this.validationService);
        processor.setProductoRepository(this.productoRepository);
        processor.setTipoProductoRepository(this.tipoProductoRepository);
        processor.setTipoDocumentoRepository(this.tipoDocumentoRepository);
        processor.setCuentaRepository(this.cuentaRepository);
        processor.setClienteRepository(this.clienteRepository);
        processor.setContratoRepository(this.contratoRepository);
        processor.setUsuarioRepository(this.usuarioRepository);
        return processor;
    }

    /**
     * Returns a Job Listener for Traslados Job
     *
     * @return The Traslados Job Listener
     */
    @Bean
    public LoadJobListener trasladosListener() {
        return new LoadJobListener(this.emailService, this.fileHistoryService, this.validationService, cacheManager);
    }

    /**
     * Traslados Job
     *
     * @param trasladosJobStep1 Reference to first step of the Job (Reading, processing and writing)
     * @return The Traslados Job
     */
    @Bean(name = "trasladosJob")
    public Job trasladosJob(Step trasladosJobStep1, LoadJobListener trasladosListener) {
        return this.jobBuilderFactory.get("trasladosJob")
                .incrementer(new RunIdIncrementer())
                .listener(trasladosListener)
                .flow(trasladosJobStep1)
                .build()
                .build();
    }

    /**
     * Traslados Chunk Step
     *
     * @param trasladosReader        Reference to Traslados Item Reader
     * @param trasladosWriter        Reference to Traslados Item Writer
     * @param trasladosItemProcessor Reference to Traslados Item Processor
     * @return The Chunk Step
     */
    @Bean
    public Step trasladosJobStep1(CustomItemReader<TrasladosDto> trasladosReader, TrasladosItemWriter trasladosWriter, TrasladosItemProcessor trasladosItemProcessor) {
        return this.stepBuilderFactory.get("trasladosJobStep")
                .<TrasladosDto, HistorialTraslado>chunk(100)
                .reader(trasladosReader)
                .writer(trasladosWriter)
                .processor(trasladosItemProcessor)
                .faultTolerant()
                .skip(VerificationException.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }

}
