package com.colpatria.midas.batch;

import com.colpatria.midas.dto.CondonacionDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialCondonacion;
import com.colpatria.midas.processors.CondonacionItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.CondonacionItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

@Configuration
public class CondonacionDiarioBatchConfig {

    /**
     * Add logger to pagos config
     */
    private static final Logger logger = LoggerFactory.getLogger(CondonacionDiarioBatchConfig.class);

    /**
     * Add job builderFactory to config
     */
    public final JobBuilderFactory jobBuilderFactory;

    /**
     * Add step builder factory to config
     */
    public final StepBuilderFactory stepBuilderFactory;

    /**
     * add product repository
     */
    private final ProductoRepository productoRepository;

    /**
     * add product repository
     */
    private final TipoProductoRepository tipoProductoRepository;

    /**
     * add tipo documento repository
     */
    private final TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Add client repository
     */
    private final ClienteRepository clienteRepository;

    /**
     * Add Condonacion repository
     */
    private final HistorialCondonacionRepository historialCondonacionRepository;

    /**
     * Add Negociacion repository
     */
    private final NegociacionRepository negociacionRepository;

    /**
     * Add Tipo Negociacion repository
     */
    private final TipoNegociacionRepository tipoNegociacionRepository;

    /**
     * Add Usuario repository
     */
    private final UsuarioRepository usuarioRepository;

    /**
     * Add Contrato repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * Add Cuenta repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * Add Servicio repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * Cache manager to use
     */
    private final CacheManager cacheManager;

    /**
     * Add email service
     */
    private final EmailService emailService;

    /**
     * Add file history service
     */
    private final FileHistoryService fileHistoryService;

    /**
     * Validation service
     */
    private final ValidationService validationService;


    /**
     * @param jobBuilderFactory references the Job Factory
     * @param stepBuilderFactory references the Step Factory
     * @param productoRepository references the productoRepository
     * @param tipoProductoRepository references the tipoProductoRepository
     * @param tipoDocumentoRepository references the tipoDopcumentoRepository
     * @param clienteRepository references the clienteRepository
     * @param negociacionRepository references the negociacionRepository
     * @param tipoNegociacionRepository references the tipoNegociacionRepository
     * @param historialCondonacionRepository references the historialCondonacionRepository
     * @param usuarioRepository references the usuarioRepository
     * @param contratoRepository  references the contratoRepository
     * @param servicioRepository references the servicioRepository
     * @param cuentaRepository references the cuentaRepository
     * @param cacheManager DI of cache manager
     * @param emailService references the emailService
     * @param fileHistoryService references the fileHistoryService
     * @param validationService references the validationService
     */
    @Autowired
    public CondonacionDiarioBatchConfig(
            JobBuilderFactory jobBuilderFactory,
            StepBuilderFactory stepBuilderFactory,
            ProductoRepository productoRepository,
            TipoProductoRepository tipoProductoRepository,
            TipoDocumentoRepository tipoDocumentoRepository,
            ClienteRepository clienteRepository,
            NegociacionRepository negociacionRepository,
            TipoNegociacionRepository tipoNegociacionRepository,
            HistorialCondonacionRepository historialCondonacionRepository,
            UsuarioRepository usuarioRepository,
            ContratoRepository contratoRepository,
            ServicioRepository servicioRepository,
            CuentaRepository cuentaRepository,
            CacheManager cacheManager, EmailService emailService,
            FileHistoryService fileHistoryService,
            ValidationService validationService
    ) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.productoRepository = productoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.clienteRepository = clienteRepository;
        this.negociacionRepository = negociacionRepository;
        this.tipoNegociacionRepository = tipoNegociacionRepository;
        this.historialCondonacionRepository = historialCondonacionRepository;
        this.usuarioRepository = usuarioRepository;
        this.contratoRepository=contratoRepository;
        this.cuentaRepository=cuentaRepository;
        this.servicioRepository=servicioRepository;
        this.cacheManager = cacheManager;
        this.emailService = emailService;
        this.fileHistoryService = fileHistoryService;
        this.validationService = validationService;
    }

    /**
     * Method that configures the file reading process
     * @param file to be read
     * @return item reader object that contains the configuration for reading the file in the batch process
     */
    @StepScope
    @Bean
    public FlatFileItemReader<CondonacionDto> condonacionReader(@Value( "#{jobParameters['file']}" ) String file ){

        DelimitedLineTokenizer tokenizer;
        DefaultLineMapper<CondonacionDto> lineMapper;
        String[] columns = new String[]{"CODIGO PRODUCTO",
                "NOMBRE PRODUCTO",
                "CODIGO TIPO PRODUCTO",
                "NOMBRE TIPO PRODUCTO",
                "CODIGO",
                "NOMBRE SUBPRODUCTO",
                "TIPO_IDENTIFICACION",
                "NRO_DOCTO_IDENT",
                "NUMERO_CONTRATO",
                "NUMERO_CUENTA",
                "CICLO",
                "ID_NEGOCIACION",
                "FECHANEG",
                "FECHAPAGO",
                "FECHAAPLICA",
                "FECHA RECHAZO",
                "MOTIVO_RECHAZO",
                "FECHA_VENCIMIENTO",
                "TIPO_NEGOCIACION",
                "ESTADO_NEGOCIACION",
                "VALOR_PAGO",
                "NRO_SERVICIO",
                "DESCRIPCION",
                "CAPITAL_CONDONA",
                "SALDO_CAPITAL_CONDONA",
                "CODUSUARIO",
                "USUARIO",
                "AREA"};
        tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter("|");
        tokenizer.setNames(
                "productoCodigo",
                "productoNombre",
                "productoTipoCodigo",
                "productoTipo",
                "subproductoCodigo",
                "subproductoNombre",
                "tipoIdentificacion",
                "numeroIdentificacion",
                "contratoNumero",
                "numeroCuenta",
                "ciclo",
                "idNegociacion",
                "fechaNegociacion",
                "fechaPago",
                "fechaAplicacion",
                "fechaRechazo",
                "motivoRechazo",
                "fechaVencimiento",
                "tipoNegociacion",
                "estadoNegociacion",
                "valorPago",
                "numeroServicio",
                "descripcion",
                "capitalCondonado",
                "saldoCapitalCondonado",
                "codUsuario",
                "user",
                "area"
        );
        lineMapper = new DefaultLineMapper<>();
        lineMapper.setFieldSetMapper(new BeanWrapperFieldSetMapper<CondonacionDto>() {
            {
                setTargetType(CondonacionDto.class);
            }
        });
        lineMapper.setLineTokenizer(tokenizer);
        logger.info(file);
        Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));

        CustomItemReader<CondonacionDto> reader = new CustomItemReader<>(columns,
                "condonaciones", resource,
                this.emailService,
                cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

    /**
     * Method that configures the writing process
     * @return item writer object that contains the configuration for writing in the database during the batch process
     */
    @StepScope
    @Bean
    public CondonacionItemWriter condonacionItemWriter(){
        return new CondonacionItemWriter(this.historialCondonacionRepository,
                this.cuentaRepository,
                this.clienteRepository,
                this.contratoRepository,
                this.servicioRepository,
                this.negociacionRepository,
                this.tipoNegociacionRepository,
                this.validationService);

    }

    /**
     * Method that configures the processor for the batch process
     * @return object of the porcessor class
     */
    @StepScope
    @Bean
    public CondonacionItemProcessor condonacionProcessor(@Value("#{jobParameters['file']}") String fileName) {
        return new CondonacionItemProcessor(
                fileName,
                this.emailService,
                this.productoRepository,
                this.tipoProductoRepository,
                this.tipoDocumentoRepository,
                this.clienteRepository,
                this.historialCondonacionRepository,
                this.negociacionRepository,
                this.tipoNegociacionRepository,
                this.usuarioRepository,
                this.cuentaRepository,
                this.contratoRepository,
                this.servicioRepository,
                this.validationService
        );
    }

    /**
     * Method that configures the listener to be executed after the batch process
     * @return object of the listener class
     */
    @Bean
    public LoadJobListener condonacionJobListener(){
        return new LoadJobListener(this.emailService, this.fileHistoryService, validationService, cacheManager);
    }

    /**
     * Method that configures the job to be executed in the batch process
     * @param condonacionJobListener is the listener to be executed after the job
     * @param condonacionJobStep1 is the first step to be execute during the job
     * @return job object that contains the configuration for the job
     */
    @Bean(name = "condonacionJob")
    public Job condonacionJob(LoadJobListener condonacionJobListener, Step condonacionJobStep1) {
        return jobBuilderFactory.get("condonacionJob")
                .incrementer( new RunIdIncrementer() )
                .listener( condonacionJobListener )
                .flow( condonacionJobStep1 )
                .build()
                .build();
    }

    /**
     * Method that configures the first step to be executed in the batch process
     * @param condonacionReader is the reader to be used in the step
     * @param condonacionWriter is the writer to be used in the step
     * @param condonacionProcessor is the processor to be used in the step
     * @return object that contains the configuration of the first step in the job
     */
    @Bean
    public Step condonacionJobStep1(
            FlatFileItemReader<CondonacionDto> condonacionReader,
            CondonacionItemWriter condonacionWriter,
            CondonacionItemProcessor condonacionProcessor
    ) {
        return stepBuilderFactory.get( "condonacionJobStep1" )
                .<CondonacionDto, HistorialCondonacion> chunk( 1000)
                .reader(condonacionReader)
                .writer(condonacionWriter)
                .processor(condonacionProcessor)
                .faultTolerant()
                .skip(VerificationException.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }

}
