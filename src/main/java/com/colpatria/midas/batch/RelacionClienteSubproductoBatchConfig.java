package com.colpatria.midas.batch;

/*
*
* Libraries
*
*/

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import com.colpatria.midas.dto.RelacionClienteSubproductoDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialRelacionClienteSubproducto;
import com.colpatria.midas.processors.RelacionClienteSubproductoItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.ClienteRepository;
import com.colpatria.midas.repositories.ContratoRepository;
import com.colpatria.midas.repositories.CuentaRepository;
import com.colpatria.midas.repositories.HistorialRelacionClienteSubproductoRepository;
import com.colpatria.midas.repositories.ProductoRepository;
import com.colpatria.midas.repositories.TipoDocumentoRepository;
import com.colpatria.midas.repositories.TipoProductoRepository;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.RelacionClienteSubproductoItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.core.io.Resource;

/**
 * Configuration class for 'relacion cliente subproducto' batch process
*/
@Configuration
public class RelacionClienteSubproductoBatchConfig {
    
    /**
	 * String the specifies the job name
	*/
	private static final String JOB_NAME = "relacionClienteSubproductoJob";

	/**
	 * String the specifies the file separator symbol
	*/
	private static final String SEPARATOR_SYMBOL = "|";

	/**
	 * String[] specifying the file column names
	*/
	private final String[] fileHeader = new String[]{
		"CODIGO PRODUCTO",
		"NOMBRE PRODUCTO",
		"CODIGO TIPO PRODUCTO",
		"NOMBRE TIPO PRODUCTO",
		"CODIGO SUBPRODUCTO",
		"NOMBRE SUBPRODUCTO",
		"TIPO_IDENTIFICACION",
		"NUMERO_IDENTIFICACION",
		"NOMBRE_CLIENTE",
		"NUMERO_CONTRATO",
		"NUMERO_SUMINISTRO",
		"FACTURACION",
		"ESTADO",
		"TIPO_CLIENTE",
		"VALOR_CUOTA_UTILIZACION",
		"COD_SEG_OBLIGATORIO",
		"FECHA_ORIGEN_RELACION",
		"FECHA_CANCELACION_RELACION",
		"UPGRADE",
		"FECHA_UPGRADE",
		"FECHA_INICIAL_CASTIGO",
		"FECHA_FIN_CASTIGO",
		"FECHA_INI_ENVIO_FACTURA",
		"FECHA_FIN_ENVIO_FACTURA",
		"MOTIVO_NO_ENVIO_FAC"
	};

    /**
     * Batch factory job object
    */
	private final JobBuilderFactory  jobBuilderFactory;

	/**
     * Batch factory step object
    */
	private final StepBuilderFactory stepBuilderFactory;

    /**
	 * Cache manager to use
	 */
	private final CacheManager cacheManager;

    /**
     * Service to move the files
    */
	private final FileHistoryService fileHistoryService;

    /**
	 * Validation service
	 */
	private final ValidationService validationService;

    /**
     * Service to send emails
    */
	private final EmailService emailService;

	/**
     * Product repository to read and write to the database
     */
    private final ProductoRepository productoRepository;

	 /**
     * Product type repository to read and write to the database
     */
    private final TipoProductoRepository tipoProductoRepository;

    /**
     * Contract repository to read and write to the database
     */
    private final ContratoRepository contratoRepository;

    /**
     * Client repository to read and write to the database
     */
    private final ClienteRepository clienteRepository;

    /**
     * Document type repository to read and write to the database
     */
    private final TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Banck account repository to read and write to the database
    */
    private final CuentaRepository cuentaRepository;

	/**
     * History repository to read and write to the database
    */
    private final HistorialRelacionClienteSubproductoRepository historialRelacionClienteSubproductoRepository;

    /**
	 * Class constructor
	 * @param jobBuilderFactory is batch job builder factory
	 * @param stepBuilderFactory is batch step builder factory
	 * @param cacheManager DI of cache manager
     * @param fileHistoryService is the file history service
     * @param validationService is the validationService
     * @param emailService is the service to send emails
	 * @param productoRepository is the product repository
	 * @param tipoProductoRepository is the product type repository
	 * @param contratoRepository is the contract repository
	 * @param clienteRepository is the client repository
	 * @param tipoDocumentoRepository is the document type repository
	 * @param cuentaRepository is the account repository
	 * @param historialRelacionClienteSubproductoRepository is the history repository
	 */
	@Autowired
	public RelacionClienteSubproductoBatchConfig(
        JobBuilderFactory  							  jobBuilderFactory,
        StepBuilderFactory							  stepBuilderFactory,
        CacheManager      							  cacheManager,
        FileHistoryService 							  fileHistoryService,
        ValidationService 							  validationService,
        EmailService 								  emailService,
		ProductoRepository 							  productoRepository,
		TipoProductoRepository 						  tipoProductoRepository,
		ContratoRepository							  contratoRepository,
		ClienteRepository							  clienteRepository,
		TipoDocumentoRepository						  tipoDocumentoRepository,
		CuentaRepository 							  cuentaRepository,
		HistorialRelacionClienteSubproductoRepository historialRelacionClienteSubproductoRepository
    ) {
		this.jobBuilderFactory                			   = jobBuilderFactory;
		this.stepBuilderFactory                			   = stepBuilderFactory;
		this.cacheManager								   = cacheManager;
        this.fileHistoryService							   = fileHistoryService;
        this.validationService                 			   = validationService;
        this.emailService                      			   = emailService;
		this.productoRepository                			   = productoRepository;
		this.tipoProductoRepository            			   = tipoProductoRepository;
		this.contratoRepository                			   = contratoRepository;
		this.clienteRepository                 			   = clienteRepository;
		this.tipoDocumentoRepository		   			   = tipoDocumentoRepository;
		this.cuentaRepository                  			   = cuentaRepository;
		this.historialRelacionClienteSubproductoRepository = historialRelacionClienteSubproductoRepository;
	}

    /**
	 * Method that configures the file reading process
	 * @param file to be read
	 * @return item reader object that contains the configuration for reading the file in the batch process
	*/
	@StepScope
	@Bean
	public FlatFileItemReader<RelacionClienteSubproductoDto> relacionClienteSubproductoReader( @Value("#{jobParameters['file']}") String file ){
		// Variables
		DelimitedLineTokenizer                                   tokenizer;
		DefaultLineMapper<RelacionClienteSubproductoDto>         lineMapper;
		Resource                                                 resource;
		CustomItemReader<RelacionClienteSubproductoDto>          reader;
		BeanWrapperFieldSetMapper<RelacionClienteSubproductoDto> fieldSetMapper;
		// Código
		tokenizer = new DelimitedLineTokenizer();
		tokenizer.setDelimiter( SEPARATOR_SYMBOL );
		tokenizer.setNames(
			"codigoProducto",
			"nombreProducto",
			"codigoTipoProducto",
			"nombreTipoProducto",
			"codigoSubproducto",
			"nombreSubproducto",
			"tipoIdentificacion",
			"numeroIdentificacion",
			"nombreCliente",
			"numeroContrato",
			"numeroCuenta",
			"facturacion",
			"estado",
			"tipoCliente",
			"valorCuotaUtilizacion",
			"codigoSeguroObligatorio",
			"fechaOrigenRelacion",
			"fechaCancelacionRelacion",
			"upgrade",
			"fechaUpgrade",
			"fechaInicialCastigo",
			"fechaFinCastigo",
			"fechaInicialEnvioFactura",
			"fechaFinEnvioFactura",
			"motivoNoEnvioFactura"
		);
		lineMapper     = new DefaultLineMapper<>();
		fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType( RelacionClienteSubproductoDto.class );
		lineMapper.setFieldSetMapper( fieldSetMapper );
		lineMapper.setLineTokenizer( tokenizer );
		resource = new PathResource( this.fileHistoryService.getPathByFileName( file ) );
        reader   = new CustomItemReader<>(
			fileHeader,
            "relacion cliente subproducto",
			resource,
            this.emailService,
                cacheManager, this.fileHistoryService.getHistorialArchivosByFileName( file )
		);
        reader.setLinesToSkip( 1 );
        reader.setLineMapper( lineMapper );		
		return reader;
	}

    /**
	 * Method that configures the writing process
	 * @return item writer object that contains the configuration for writing in the database during the batch process
	*/
	@Bean
	public RelacionClienteSubproductoItemWriter relacionClienteSubproductoWriter(){
		// Variables
		RelacionClienteSubproductoItemWriter writer;
		// Código
		writer = new RelacionClienteSubproductoItemWriter( this.validationService );
		writer.setContratoRepository( this.contratoRepository );
		writer.setClienteRepository( this.clienteRepository );
		writer.setCuentaRepository( this.cuentaRepository );
		writer.setHistorialRelacionClienteSubproductoRepository( this.historialRelacionClienteSubproductoRepository );
		return writer;
	}

    /**
	 * Method that configures the processor for the batch process
	 * @param fileName to be read
	 * @return object of the porcessor class
	*/
	@StepScope
	@Bean
	public RelacionClienteSubproductoItemProcessor relacionClienteSubproductoProcessor( @Value("#{jobParameters['file']}") String fileName ) {
		// Variables
		RelacionClienteSubproductoItemProcessor processor;
		// Code
		processor = new RelacionClienteSubproductoItemProcessor( fileName, this.validationService );	
		processor.setProductoRepository( this.productoRepository );
		processor.setTipoProductoRepository( this.tipoProductoRepository );
		processor.setContratoRepository( this.contratoRepository );
		processor.setClienteRepository( this.clienteRepository );
		processor.setTipoDocumentoRepository( this.tipoDocumentoRepository );
		processor.setCuentaRepository( this.cuentaRepository );
		return processor;
	}

    /**
	 * Method that configures the listener to be executed after the batch process
	 * @return object of the listener class
	*/
	@Bean
	public LoadJobListener relacionClienteSubproductoJobListener(){
		return new LoadJobListener( this.emailService, this.fileHistoryService, this.validationService, cacheManager);
	}

	/**
	 * Method that configures the job to be executed in the batch process
	 * @param relacionClienteSubproductoJobListener is the listener to be executed after the job
	 * @param relacionClienteSubproductoJobStep1 is the first step to be execute during the job
	 * @return a job object that contains the configuration for the job execution
	*/
	@Bean( name = JOB_NAME )
	public Job relacionClienteSubproductoJob( LoadJobListener relacionClienteSubproductoJobListener, Step relacionClienteSubproductoJobStep1 ) {
		return jobBuilderFactory.get( JOB_NAME )
			.incrementer( new RunIdIncrementer() )
			.listener( relacionClienteSubproductoJobListener )
			.flow( relacionClienteSubproductoJobStep1 ).build().build();
	}

	/**
	 * Method that configures the first step to be executed in the batch process
	 * @param relacionClienteSubproductoReader is the reader to be used in the step
	 * @param relacionClienteSubproductoWriter is the writer to be used in the step
	 * @param relacionClienteSubproductoProcessor is the processor to be used in the step
	 * @return object that contains the configuration of the first step in the job
	*/
	@Bean
	public Step relacionClienteSubproductoJobStep1(
		FlatFileItemReader<RelacionClienteSubproductoDto> relacionClienteSubproductoReader,
		RelacionClienteSubproductoItemWriter              relacionClienteSubproductoWriter,
		RelacionClienteSubproductoItemProcessor           relacionClienteSubproductoProcessor
	) {
		return stepBuilderFactory.get( "relacionClienteSubproductoJobStep1" )
			.<RelacionClienteSubproductoDto, HistorialRelacionClienteSubproducto> chunk( 100 )
			.reader( relacionClienteSubproductoReader )
			.writer( relacionClienteSubproductoWriter )
			.processor( relacionClienteSubproductoProcessor )
			.faultTolerant()
			.skip( VerificationException.class )
			.skipLimit( Integer.MAX_VALUE )
			.build();
	}

}
