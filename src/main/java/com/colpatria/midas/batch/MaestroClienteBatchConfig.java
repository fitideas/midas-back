package com.colpatria.midas.batch;

/*
 *
 * Libraries
 *
*/

import com.colpatria.midas.dto.MaestroClienteDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialMaestroCliente;
import com.colpatria.midas.processors.MaestroClienteItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.MaestroClienteItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

/**
 * Configuration class for 'movimiento saldo' batch process
*/
@Configuration
public class MaestroClienteBatchConfig {

	/**
	 * String the specifies the job name
	*/
	private static final String JOB_NAME = "maestroClienteJob";

	/**
	 * String the specifies the file separator symbol
	*/
	private static final String SEPARATOR_SYMBOL = "|";

	/**
	 * String[] specifying the file column names
	*/
	private final String[] fileHeader = new String[]{
		"NRO_CUENTA",
		"NRO_SERVICIO",
		"TIPO_IDENTIFICACION",
		"NRO_DOCUMENTO",
		"NOMBRE",
		"APELLIDOS",
		"DIRRECCION",
		"manzana_lec",
		"Sucursal_lec",
		"Zona_lec",
		"Ciclo_lec",
		"Grupo_lec",
		"COD_DPTO_lec",
		"DPTO_lec",
		"COD_MUNICIPIO_lec",
		"MUNICIPIO_lec",
		"COD_LOCALIDAD_lec",
		"LOCALIDAD_lec",
		"COD_BARRIO_lec",
		"BARRIO_lec",
		"DIRECCION",
		"localizacion_lec",
		"manzana_rep",
		"Sucursal_rep",
		"Zona_rep",
		"Ciclo_rep",
		"Grupo_rep",
		"estrato socio economico",
		"Estado Servicio",
		"estado_srv_electrico",
		"COD_CATEGORIA",
		"DES_CATEGORIA",
		"COD_ACT_ECONOMICA",
		"COD_INTERNO",
		"DES_ACT_ECONOMICA"
	};

	/**
     * Batch factory job object
    */
	private final JobBuilderFactory  jobBuilderFactory;

	/**
     * Batch factory step object
    */
	private final StepBuilderFactory stepBuilderFactory;

	/**
     * History repository to read and write to the database
    */
	private final HistorialMaestroClienteRepository historialMaestroClienteRepository;

	/**
     * Branch repository to read and write to the database
    */
    private final SucursalRepository sucursalRepository;

	/**
     * Client repository to read and write to the database
    */
	private final ClienteRepository clienteRepository;

	/**
     * Document type repository to read and write to the database
    */
	private final TipoDocumentoRepository tipoDocumentoRepository;

	/**
     * Account type repository to read and write to the database
    */
	private final CuentaRepository cuentaRepository;

	/**
     * Service repository to read and write to the database
    */
	private final ServicioRepository servicioRepository;

	/**
     * Neighborhood repository to read and write to the database
    */
    private final BarrioRepository barrioRepository;

    /**
     * ​​Department repository to read and write to the database
    */
    private final DepartamentoRepository departamentoRepository;

    /**
     * Township repository to read and write to the database
    */
    private final MunicipioRepository municipioRepository;

    /**
     * ​​Locality repository to read and write to the database
    */
    private final LocalidadRepository localidadRepository;

	/**
     * Banck account repository to read and write to the database
    */
    private final CategoriaRepository categoriaRepository;

	/**
     *  Reading repository to read and write to the database
    */
    private final LecturaRepository lecturaRepository;

    /**
     *  Delivery repository to read and write to the database
    */
    private final RepartoRepository repartoRepository;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;

	/**
     * Service to send emails
    */
	private final EmailService emailService;

	/**
     * Service to move the files
    */
	private final FileHistoryService fileHistoryService;

	/**
	 * Cache manager to use
	 */
	private final CacheManager cacheManager;

	/*
	 *
	 * Métodos
	 *
	*/

	/**
	 * Class constructor
	 * @param jobBuilderFactory is batch job builder factory
	 * @param stepBuilderFactory is batch step builder factory
	 * @param servicioRepository is the service repository
	 * @param historialMaestroClienteRepository is the history repository
	 * @param emailService is the service to send emails
	 * @param fileHistoryService
	 * @param clienteRepository is the client repository
	 * @param cuentaRepository is the account repository
	 * @param tipoDocumentoRepository is the document type repository
	 * @param categoriaRepository is the category repository
	 * @param barrioRepository is the neighborhood repository
	 * @param departamentoRepository is the ​​department repository
	 * @param municipioRepository is the township repository
	 * @param localidadRepository is the ​​locality repository
	 * @param sucursalRepository the branch repository
	 * @param lecturaRepository the reading repository
	 * @param repartoRepository the delivery repository
	 * @param validationService is the validationService
	 * @param cacheManager DI of cache manager
	 */
	@Autowired
	public MaestroClienteBatchConfig(
			JobBuilderFactory jobBuilderFactory,
			StepBuilderFactory stepBuilderFactory,
			ServicioRepository servicioRepository,
			HistorialMaestroClienteRepository historialMaestroClienteRepository,
			EmailService emailService,
			FileHistoryService fileHistoryService,
			ClienteRepository clienteRepository,
			CuentaRepository cuentaRepository,
			TipoDocumentoRepository tipoDocumentoRepository,
			CategoriaRepository categoriaRepository,
			BarrioRepository barrioRepository,
			DepartamentoRepository departamentoRepository,
			MunicipioRepository municipioRepository,
			LocalidadRepository localidadRepository,
			SucursalRepository sucursalRepository,
			LecturaRepository lecturaRepository,
			RepartoRepository repartoRepository,
			ValidationService validationService,
			CacheManager cacheManager) {
		this.jobBuilderFactory                 = jobBuilderFactory;
		this.stepBuilderFactory                = stepBuilderFactory;
		this.servicioRepository                = servicioRepository;
		this.historialMaestroClienteRepository = historialMaestroClienteRepository;
		this.emailService                      = emailService;
		this.fileHistoryService                = fileHistoryService;
		this.clienteRepository                 = clienteRepository;
		this.cuentaRepository                  = cuentaRepository;
		this.tipoDocumentoRepository		   = tipoDocumentoRepository;
		this.categoriaRepository               = categoriaRepository;
		this.barrioRepository                  = barrioRepository;
		this.departamentoRepository            = departamentoRepository;
		this.municipioRepository               = municipioRepository;
		this.localidadRepository               = localidadRepository;
		this.sucursalRepository                = sucursalRepository;
		this.lecturaRepository                 = lecturaRepository;
		this.repartoRepository                 = repartoRepository;
		this.validationService                 = validationService;
		this.cacheManager = cacheManager;
	}

	/**
	 * Method that configures the file reading process
	 * @param file to be read
	 * @return item reader object that contains the configuration for reading the file in the batch process
	*/
	@StepScope
	@Bean
	public FlatFileItemReader<MaestroClienteDto> maestroClienteReader( @Value("#{jobParameters['file']}") String file ){
		// Variables
		DelimitedLineTokenizer                       tokenizer;
		DefaultLineMapper<MaestroClienteDto>         lineMapper;
		Resource                                     resource;
		CustomItemReader<MaestroClienteDto>          reader;
		BeanWrapperFieldSetMapper<MaestroClienteDto> fieldSetMapper;
		// Código
		tokenizer = new DelimitedLineTokenizer();
		tokenizer.setDelimiter( SEPARATOR_SYMBOL );
		tokenizer.setNames(
			"numeroCuenta",
			"numeroServicio",
			"tipoIdentificacion",
			"numeroIdentificacion",
			"nombreCliente",
			"apellidosCliente",
			"direccionPredio",
			"manzanaLectura",
			"sucursalLectura",
			"zonaLectura",
			"cicloLectura",
			"grupoLectura",
			"codigoDepartamentoLectura",
			"nombreDepartamentoLectura",
			"codigoMunicipioLectura",
			"nombreMunicipioLectura",
			"codigoLocalidadLectura",
			"nombreLocalidadLectura",
			"codigoBarrioLectura",
			"nombreBarrioLectura",
			"direccionReparto",
			"localizacionLectura",
			"manzanaReparto",
			"sucursalReparto",
			"zonaReparto",
			"cicloReparto",
			"grupoReparto",
			"estratoSocioeconomico",
			"estadoServicio",
			"estadoServicioElectrico",
			"codigoCategoria",
			"descripcionCategoria",
			"codigoActividadEconomica",
			"codigoInterno",
			"descripcionActividadEconomica"
		);
		lineMapper     = new DefaultLineMapper<>();
		fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType( MaestroClienteDto.class );
		lineMapper.setFieldSetMapper( fieldSetMapper );
		lineMapper.setLineTokenizer( tokenizer );
		resource = new PathResource( this.fileHistoryService.getPathByFileName( file ) );
        reader   = new CustomItemReader<>(
			fileHeader,
            "maestro cliente",
			resource,
            this.emailService,
                cacheManager, this.fileHistoryService.getHistorialArchivosByFileName( file )
		);
        reader.setLinesToSkip( 1 );
        reader.setLineMapper( lineMapper );		
		return reader;
	}

	/**
	 * Method that configures the writing process
	 * @return item writer object that contains the configuration for writing in the database during the batch process
	*/
	@Bean
	public MaestroClienteItemWriter maestroClienteWriter(){
		// Variables
		MaestroClienteItemWriter writer;
		// Código
		writer = new MaestroClienteItemWriter( this.validationService );
		writer.setHistorialMaestroClienteRepository( this.historialMaestroClienteRepository );
		writer.setClienteRepository( this.clienteRepository );
		writer.setCuentaRepository( this.cuentaRepository );
		writer.setServicioRepository( this.servicioRepository );
		writer.setBarrioRepository( this.barrioRepository );
		writer.setLecturaRepository( this.lecturaRepository );
		writer.setRepartoRepository( this.repartoRepository );
		return writer;
	}

	/**
	 * Method that configures the processor for the batch process
	 * @param fileName to be read
	 * @return object of the porcessor class
	*/
	@StepScope
	@Bean
	public MaestroClienteItemProcessor maestroClienteProcessor( @Value("#{jobParameters['file']}") String fileName ) {
		// Variables
		MaestroClienteItemProcessor processor;
		// Code
		processor = new MaestroClienteItemProcessor( fileName, this.validationService );
		processor.setSucursalRepository( this.sucursalRepository );
		processor.setServicioRepository( this.servicioRepository );
		processor.setClienteRepository( this.clienteRepository );
		processor.setCuentaRepository( this.cuentaRepository );
		processor.setCategoriaRepository( this.categoriaRepository );
		processor.setTipoDocumentoRepository( this.tipoDocumentoRepository );
		processor.setBarrioRepository( this.barrioRepository );
		processor.setDepartamentoRepository( this.departamentoRepository );
		processor.setMunicipioRepository( this.municipioRepository );
		processor.setLocalidadRepository( this.localidadRepository );		
		return processor;
	}

	/**
	 * Method that configures the listener to be executed after the batch process
	 * @return object of the listener class
	*/
	@Bean
	public LoadJobListener maestroClienteJobListener(){
		return new LoadJobListener( this.emailService, this.fileHistoryService, this.validationService, cacheManager);
	}

	/**
	 * Method that configures the job to be executed in the batch process
	 * @param maestroClienteJobListener is the listener to be executed after the job
	 * @param maestroClienteJobStep1 is the first step to be execute during the job
	 * @return a job object that contains the configuration for the job execution
	*/
	@Bean( name = JOB_NAME )
	public Job maestroClienteJob( LoadJobListener maestroClienteJobListener, Step maestroClienteJobStep1 ) {
		return jobBuilderFactory.get( JOB_NAME )
			.incrementer( new RunIdIncrementer() )
			.listener( maestroClienteJobListener )
			.flow( maestroClienteJobStep1 ).build().build();
	}

	/**
	 * Method that configures the first step to be executed in the batch process
	 * @param maestroClienteReader is the reader to be used in the step
	 * @param maestroClienteWriter is the writer to be used in the step
	 * @param maestroClienteProcessor is the processor to be used in the step
	 * @return object that contains the configuration of the first step in the job
	*/
	@Bean
	public Step maestroClienteJobStep1(
		FlatFileItemReader<MaestroClienteDto> maestroClienteReader,
		MaestroClienteItemWriter              maestroClienteWriter,
		MaestroClienteItemProcessor           maestroClienteProcessor
	) {
		return stepBuilderFactory.get( "maestroClienteJobStep1" )
			.<MaestroClienteDto, HistorialMaestroCliente> chunk( 100 )
			.reader( maestroClienteReader )
			.writer( maestroClienteWriter )
			.processor( maestroClienteProcessor )
			.faultTolerant()
			.skip( VerificationException.class )
			.skipLimit( Integer.MAX_VALUE )
			.build();
	}

}
