package com.colpatria.midas.batch;

import com.colpatria.midas.dto.FacturacionDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialFacturacion;
import com.colpatria.midas.processors.FacturacionItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.FacturacionItemWriter;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

/**
 * Facturacion Batch Configuration
 */
@Configuration
public class FacturacionBatchConfig {

    /**
     * Job Buiolder Factory
     */
    private final JobBuilderFactory jobBuilderFactory;

    /**
     * Step Builder Factory
     */
    private final StepBuilderFactory stepBuilderFactory;

    /**
     * Email Service
     */
    private final EmailService emailService;

    /**
     * File History Service
     */
    private final FileHistoryService fileHistoryService;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * Cache manager to use
     */
    private final CacheManager cacheManager;

    // Repositories
    private final TipoProductoRepository tipoProductoRepository;
    private final TipoDocumentoRepository tipoDocumentoRepository;
    private final ProductoRepository productoRepository;
    private final SucursalRepository sucursalRepository;
    private final ClienteRepository clienteRepository;
    private final CuentaRepository cuentaRepository;
    private final ServicioRepository servicioRepository;
    private final ContratoRepository contratoRepository;
    private final CargoRepository cargoRepository;
    private final HistorialFacturacionRepository historialFacturacionRepository;

    @Autowired
    public FacturacionBatchConfig(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, EmailService emailService, FileHistoryService fileHistoryService, TipoProductoRepository tipoProductoRepository, TipoDocumentoRepository tipoDocumentoRepository, ProductoRepository productoRepository, SucursalRepository sucursalRepository, ClienteRepository clienteRepository, CuentaRepository cuentaRepository, ServicioRepository servicioRepository, ContratoRepository contratoRepository, CargoRepository cargoRepository, HistorialFacturacionRepository historialFacturacionRepository, ValidationService validationService, CacheManager cacheManager) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.emailService = emailService;
        this.fileHistoryService = fileHistoryService;
        this.tipoProductoRepository = tipoProductoRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.productoRepository = productoRepository;
        this.sucursalRepository = sucursalRepository;
        this.clienteRepository = clienteRepository;
        this.cuentaRepository = cuentaRepository;
        this.servicioRepository = servicioRepository;
        this.contratoRepository = contratoRepository;
        this.cargoRepository = cargoRepository;
        this.historialFacturacionRepository = historialFacturacionRepository;
        this.validationService = validationService;
        this.cacheManager = cacheManager;
    }

    /***
     * Returns a File item reader according to specifications of Facturacion file structure
     * @param file file to read
     * @return The File Item Reader
     */
    @StepScope
    @Bean
    public FlatFileItemReader<FacturacionDto> facturacionReader(@Value("#{jobParameters['file']}") String file) {
        String[] columns = new String[]{"CODIGO PRODUCTO", "NOMBRE PRODUCTO", "CODIGO TIPO PRODUCTO", "NOMBRE TIPO PRODUCTO", "CODIGO SUBPRODUCTO", "NOMBRE SUBPRODUCTO", "NUMERO_CONTRATO", "TIPO_IDENTIFICACION", "NRO_DOCUMENTO", "NRO_CUENTA", "SECTOR", "NRO_SERVICIO", "SUCURSAL", "ESTRAT", "FACTURACION", "DESCRIPCION", "CARGO", "VALOR_FACT", "FEC_DOC", "FECHA_PR", "TOT_DOCUMENTO", "SALDO"};
        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter("|");
        tokenizer.setNames("codigoProducto", "nombreProducto", "codigoTipoProducto", "nombreTipoProducto", "codigoSubproducto", "nombresubproducto", "numeroContrato", "tipoIdentificacion", "numeroDocumento", "numeroCuenta", "sector", "numeroServicio", "sucursal", "estrato", "facturacion", "descripcion", "cargo", "valorFact", "fechaDoc", "fechaPr", "totDocumento", "saldo");
        DefaultLineMapper<FacturacionDto> lineMapper = new DefaultLineMapper<>();
        BeanWrapperFieldSetMapper<FacturacionDto> wrapper = new BeanWrapperFieldSetMapper<>();
        wrapper.setTargetType(FacturacionDto.class);
        wrapper.setDistanceLimit(0);
        lineMapper.setFieldSetMapper(wrapper);
        lineMapper.setLineTokenizer(tokenizer);
        lineMapper.setLineTokenizer(tokenizer);
        Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));
        CustomItemReader<FacturacionDto> reader = new CustomItemReader<>(columns, "facturacion", resource, this.emailService, cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

    /**
     * Returns an item writer according to specifications of Facturacion's Fields
     *
     * @return The facturacion item writer
     */
    @StepScope
    @Bean
    public FacturacionItemWriter facturacionWriter() {
        FacturacionItemWriter writer = new FacturacionItemWriter();
        writer.setClienteRepository(this.clienteRepository);
        writer.setContratoRepository(this.contratoRepository);
        writer.setCuentaRepository(this.cuentaRepository);
        writer.setServicioRepository(this.servicioRepository);
        writer.setHistorialFacturacionRepository(this.historialFacturacionRepository);
        return writer;
    }

    /***
     * Returns an object processor of FacturacionDto to FacturacionProcessed
     * @return The Facturacion item processor
     */
    @StepScope
    @Bean
    public FacturacionItemProcessor facturacionProcessor(@Value("#{jobParameters['file']}") String file) {
        FacturacionItemProcessor processor = new FacturacionItemProcessor(file);
        processor.setValidationService(this.validationService);
        processor.setProductoRepository(this.productoRepository);
        processor.setTipoProductoRepository(this.tipoProductoRepository);
        processor.setTipoDocumentoRepository(this.tipoDocumentoRepository);
        processor.setSucursalRepository(this.sucursalRepository);
        processor.setCargoRepository(this.cargoRepository);
        processor.setCuentaRepository(this.cuentaRepository);
        processor.setClienteRepository(this.clienteRepository);
        processor.setContratoRepository(this.contratoRepository);
        processor.setServicioRepository(this.servicioRepository);
        processor.setHistorialFacturacionRepository(this.historialFacturacionRepository);
        return processor;
    }

    /**
     * Returns a Job Listener for Facturacion Job
     *
     * @return The Facturacion Job Listener
     */
    @Bean
    public LoadJobListener facturacionjobListener() {
        return new LoadJobListener(this.emailService, this.fileHistoryService, validationService, cacheManager);
    }

    /**
     * Facturacion Job
     *
     * @param facturacionStep Reference to first step of the Job (Reading, processing and writing)
     * @return The Facturacion Job
     */
    @Bean(name = "facturacionJob")
    public Job job(LoadJobListener facturacionjobListener, Step facturacionStep) {
        return this.jobBuilderFactory.get("facturacionJob")
                .incrementer(new RunIdIncrementer())
                .listener(facturacionjobListener)
                .flow(facturacionStep)
                .build()
                .build();
    }

    /**
     * Facturacion Chunk Step
     *
     * @param facturacionReader    Reference to Facturacion Item Reader
     * @param facturacionWriter    Reference to Facturacion Item Writer
     * @param facturacionProcessor Reference to Facturacion Item Processor
     * @return The Chunk Step
     */
    @Bean
    public Step facturacionStep(ItemReader<FacturacionDto> facturacionReader, FacturacionItemWriter facturacionWriter, FacturacionItemProcessor facturacionProcessor) {
        return this.stepBuilderFactory.get("facturacionJobStep")
                .<FacturacionDto, HistorialFacturacion>chunk(100)
                .reader(facturacionReader)
                .writer(facturacionWriter)
                .processor(facturacionProcessor)
                .faultTolerant()
                .skip(VerificationException.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }

}
