package com.colpatria.midas.batch;

import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialRecaudo;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.RecaudoItemWriter;
import com.colpatria.midas.dto.RecaudoDto;
import com.colpatria.midas.processors.RecaudoItemProcessor;
import com.colpatria.midas.repositories.*;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

/**
 * Recaudo Batch Configuration
 */
@Configuration
public class RecaudoBatchConfig {

    /**
     * Job Buiolder Factory
     */
    private final JobBuilderFactory jobBuilderFactory;

    /**
     * Step Builder Factory
     */
    private final StepBuilderFactory stepBuilderFactory;

    /**
     * Email Service
     */
    private final EmailService emailService;

    /**
     * File History Service
     */
    private final FileHistoryService fileHistoryService;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * Producto Repository
     */
    private final ProductoRepository productoRepository;

    /**
     * Tipo Producto Repository
     */
    private final TipoProductoRepository tipoProductoRepository;

    /**
     * Tipo Documento Repository
     */
    private final TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Cliente Repository
     */
    private final ClienteRepository clienteRepository;

    /**
     * Contrato Repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * Cuenta Repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * Servicio Repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * Historial Recaudo Repository
     */
    private final HistorialRecaudoRepository historialRecaudoRepository;

    /**
     * Sucursal Repository
     */
    private final SucursalRepository sucursalRepository;

    /**
     * Categoria Repository
     */
    private final CategoriaRepository categoriaRepository;

    /**
     * Cargo Repository
     */
    private final CargoRepository cargoRepository;

    /**
     * Forma Pago Repository
     */
    private final FormaPagoRepository formaPagoRepository;

    /**
     * Tipo Consumo Repository
     */
    private final TipoConsumoRepository tipoConsumoRepository;

    /**
     * Municipio Repository
     */
    private final MunicipioRepository municipioRepository;

    /**
     * Clase Servicio Repository
     */
    private final ClaseServicioRepository claseServicioRepository;

    /**
     * Recaudo Cargo Repository
     */
    private final RecaudoCargoRepository recaudoCargoRepository;

    /**
     * Historial Recaudo Resumen Repository
     */
    private final HistorialRecaudoResumenRepository historialRecaudoResumenRepository;

    /**
     * Cache manager to use
     */
    private final CacheManager cacheManager;

    /*
     * Tipo Cargo Repository
     */
    private final TipoCargoRepository tipoCargoRepository;

    /**
     * Constructor
     *  @param jobBuilderFactory          Job Builder Factory Bean Reference
     * @param stepBuilderFactory         Step Builder Factory Bean Reference
     * @param emailService               Email Service Bean Reference
     * @param fileHistoryService         File History Service Bean Reference
     * @param productoRepository         Producto Repository Bean Reference
     * @param tipoProductoRepository     Tipo Producto Repository Bean Reference
     *
     * @param tipoDocumentoRepository    Tipo Documento Repository Bean Reference
     * @param clienteRepository          Cliente Repository Bean Reference
     * @param contratoRepository         Contrato Repository Bean Reference
     * @param cuentaRepository           Cuenta Repository Bean Reference
     * @param servicioRepository         Servicio Repository Bean Reference
     * @param historialRecaudoRepository Historial Recaudo bean Reference
     * @param sucursalRepository         Sucursal Repository Bean reference
     * @param categoriaRepository        Categoria Repository Bean reference
     * @param cargoRepository            Cargo Repository Bean Reference
     * @param formaPagoRepository        Forma Pago Repository Bean Reference
     * @param tipoConsumoRepository      Tipo Consumo Repository Bean Reference
     * @param claseServicioRepository    Clase Servicio Repository Bean Reference
     * @param validationService          Validation Service Bean Reference
     * @param cacheManager DI of cache manager
     */
    @Autowired
    public RecaudoBatchConfig(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, EmailService emailService, FileHistoryService fileHistoryService, ProductoRepository productoRepository, TipoProductoRepository tipoProductoRepository, TipoDocumentoRepository tipoDocumentoRepository, ClienteRepository clienteRepository, ContratoRepository contratoRepository, CuentaRepository cuentaRepository, ServicioRepository servicioRepository, HistorialRecaudoRepository historialRecaudoRepository, SucursalRepository sucursalRepository, CategoriaRepository categoriaRepository, CargoRepository cargoRepository, FormaPagoRepository formaPagoRepository, TipoConsumoRepository tipoConsumoRepository, MunicipioRepository municipioRepository ,ClaseServicioRepository claseServicioRepository, ValidationService validationService, RecaudoCargoRepository recaudoCargoRepository, HistorialRecaudoResumenRepository historialRecaudoResumenRepository, CacheManager cacheManager, TipoCargoRepository tipoCargoRepository) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.emailService = emailService;
        this.fileHistoryService = fileHistoryService;
        this.productoRepository = productoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository = contratoRepository;
        this.cuentaRepository = cuentaRepository;
        this.servicioRepository = servicioRepository;
        this.historialRecaudoRepository = historialRecaudoRepository;
        this.sucursalRepository = sucursalRepository;
        this.categoriaRepository = categoriaRepository;
        this.cargoRepository = cargoRepository;
        this.formaPagoRepository = formaPagoRepository;
        this.tipoConsumoRepository = tipoConsumoRepository;
        this.municipioRepository = municipioRepository;
        this.claseServicioRepository = claseServicioRepository;
        this.validationService = validationService;
        this.recaudoCargoRepository = recaudoCargoRepository;
        this.historialRecaudoResumenRepository = historialRecaudoResumenRepository;
        this.cacheManager = cacheManager;
        this.tipoCargoRepository = tipoCargoRepository;
    }

    /***
     * Returns a File item reader according to specifications of Recaudo file structure
     * @param file file to read
     * @return The File Item Reader
     */
    @StepScope
    @Bean
    public FlatFileItemReader<RecaudoDto> reader(@Value("#{jobParameters['file']}") String file) {
        String[] columns = new String[]{"CODIGO PRODUCTO", "NOMBRE PRODUCTO", "CODIGO TIPO PRODUCTO", "NOMBRE TIPO PRODUCTO", "CODIGO SUBPRODUCTO", "NOMBRE SUBPRODUCTO", "TIPO_DOCUMENTO_IDENTIDAD", "NRO_DOCUMENTO_TITULAR", "NUMERO_CONTRATO", "NRO_CUENTA", "SUCURSAL", "CICLO", "MUNICIPIO", "CATEGORIA", "ESTRATO", "TIPO_PAGO", "VALOR_PAGADO", "MONTO", "COD_CARGO", "NRO_SERVICIO", "DESCRIPCION", "FECHA_PAGO", "FECHA_PROCESO_PAGO", "FORMA_PAGO", "COD_ENTIDAD_RECAUDADORA", "SUCURSAL_RECAUDO", "FECHA_FACTURACION", "TIPO_CONSUMO", "FECHA_DOCUMENTO", "NRO_DOCUMENTO", "OFICINA"};
        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter("|");
        tokenizer.setNames("codigoProducto", "nombreProducto", "codigoTipoProducto", "nombreTipoProducto", "codigoSubproducto", "nombreSubproducto", "tipoDocumentoIdentidad", "nroDocumentoTitular", "numeroContrato", "nroCuenta", "sucursal", "ciclo", "municipio", "categoria", "estrato", "tipoPago", "valorPagado", "monto", "codCargo", "nroServicio", "descripcion", "fechaPago", "fechaProcesoPago", "formaPago", "codEntidadRecaudadora", "sucursalRecaudo", "fechaFacturacion", "tipoConsumo", "fechaDocumento", "nroDocumento", "oficina");
        DefaultLineMapper<RecaudoDto> lineMapper = new DefaultLineMapper<>();
        BeanWrapperFieldSetMapper<RecaudoDto> wrapper = new BeanWrapperFieldSetMapper<>();
        wrapper.setTargetType(RecaudoDto.class);
        lineMapper.setFieldSetMapper(wrapper);
        lineMapper.setLineTokenizer(tokenizer);
        Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));
        CustomItemReader<RecaudoDto> reader = new CustomItemReader<>(columns, "recaudo", resource, this.emailService, cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

    /**
     * Returns an item writer according to specifications of Recaudo's Fields
     *
     * @return The recaudo item writer
     */
    @StepScope
    @Bean
    public RecaudoItemWriter writer() {
        return new RecaudoItemWriter(this.historialRecaudoRepository, this.validationService, this.cuentaRepository, this.clienteRepository, this.contratoRepository, this.servicioRepository, this.recaudoCargoRepository, this.historialRecaudoResumenRepository, this.tipoCargoRepository);
    }

    /***
     * Returns an object processor of RecaudoDto to RecaudoProcessed
     * @return The Recaudo item processor
     */
    @StepScope
    @Bean
    public RecaudoItemProcessor recaudoItemProcessor(@Value("#{jobParameters['file']}") String file) {
        RecaudoItemProcessor processor = new RecaudoItemProcessor(file);
        processor.setProductoRepository(this.productoRepository);
        processor.setTipoProductoRepository(this.tipoProductoRepository);
        processor.setTipoDocumentoRepository(this.tipoDocumentoRepository);
        processor.setTipoConsumoRepository(this.tipoConsumoRepository);
        processor.setClaseServicioRepository(this.claseServicioRepository);
        processor.setSucursalRepository(this.sucursalRepository);
        processor.setCategoriaRepository(this.categoriaRepository);
        processor.setCargoRepository(this.cargoRepository);
        processor.setMunicipioRepository(this.municipioRepository);
        processor.setFormaPagoRepository(this.formaPagoRepository);
        processor.setClienteRepository(this.clienteRepository);
        processor.setContratoRepository(this.contratoRepository);
        processor.setCuentaRepository(this.cuentaRepository);
        processor.setServicioRepository(this.servicioRepository);
        processor.setValidationService(this.validationService);
        return processor;
    }

    /**
     * Returns a Job Listener for Recaudo Job
     *
     * @return The Recaudo Job Listener
     */
    @Bean
    public LoadJobListener recaudoJobListener() {
        return new LoadJobListener(this.emailService, this.fileHistoryService, this.validationService, cacheManager);
    }


    /**
     * Recaudo Job
     *
     * @param step Reference to first step of the Job (Reading, processing and writing)
     * @return The Recaudo Job
     */
    @Bean(name = "recaudoJob")
    public Job job(LoadJobListener recaudoJobListener, Step step) {
        return this.jobBuilderFactory.get("recaudoJob")
                .incrementer(new RunIdIncrementer())
                .listener(recaudoJobListener)
                .flow(step).build()
                .build();
    }

    /**
     * Recuado Chunk Step
     *
     * @param reader    Reference to Recaudo Item Reader
     * @param writer    Reference to Recaudo Item Writer
     * @param recaudoItemProcessor Reference to Recaudo Item Processor
     * @return The Chunk Step
     */
    @Bean
    public Step step(ItemReader<RecaudoDto> reader, RecaudoItemWriter writer, RecaudoItemProcessor recaudoItemProcessor) {
        return this.stepBuilderFactory.get("recaudoJobStep")
                .<RecaudoDto, HistorialRecaudo>chunk(100)
                .reader(reader)
                .writer(writer)
                .processor(recaudoItemProcessor)
                .faultTolerant()
                .skip(VerificationException.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }

}
