package com.colpatria.midas.batch;

import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.StepLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

/**
 * Async Job Launcher
 */
@Configuration
public class AsyncJobLauncherConfig {

    /**
     * Job Repository bean reference
     */
    private final JobRepository jobRepository;

    /**
     * Class Constructor
     * @param jobRepository Job Repository Bean Reference
     */
    @Autowired
    public AsyncJobLauncherConfig(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }

    /**
     * Async Job Launcher Bean configuration
     * @return Async Job Launcher
     * @throws Exception
     */
    @Bean(name = "asyncJobLauncher")
    public JobLauncher asyncJobLauncher() throws Exception {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(this.jobRepository);
        jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
        jobLauncher.afterPropertiesSet();
        return jobLauncher;
    }
}
