package com.colpatria.midas.batch;

import com.colpatria.midas.model.Usuario;
import com.colpatria.midas.processors.TestItemProcessor;
import com.colpatria.midas.repositories.UsuarioRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.data.builder.RepositoryItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class TestConfiguration {

	private final JobBuilderFactory jobBuilderFactory;
	private final StepBuilderFactory stepBuilderFactory;
	private final UsuarioRepository usuarioRepository;

   	@Autowired
    public TestConfiguration(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, UsuarioRepository usuarioRepository){
        this.jobBuilderFactory = jobBuilderFactory;
		this.stepBuilderFactory = stepBuilderFactory;
		this.usuarioRepository = usuarioRepository;
    }

	@Bean
	public FlatFileItemReader<Usuario> usuarioReader(){
		DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
		tokenizer.setDelimiter("\t");
		tokenizer.setNames("nombres", "apellidos", "area");
		DefaultLineMapper<Usuario> lineMapper = new DefaultLineMapper<>();
		lineMapper.setLineTokenizer(tokenizer);
		return new FlatFileItemReaderBuilder<Usuario>()
				.name("testItemReader")
				.resource(new ClassPathResource("data.txt"))
				.lineTokenizer(tokenizer)
				.targetType(Usuario.class)
				.build();
	}

	@Bean
	public RepositoryItemWriter<Usuario> usuarioWriter(){
		return new RepositoryItemWriterBuilder<Usuario>()
		.repository(usuarioRepository)
		.methodName("save")
		.build();
	}
	
	@Bean
	public TestItemProcessor testProcessor() {
		return new TestItemProcessor();
	}
	
	@Bean(name="testJob")
	public Job testJob(Step testStep1) {
		return jobBuilderFactory.get("testJob")
				.incrementer(new RunIdIncrementer())
				.flow(testStep1)
				.build()
				.build();
	}

	@Bean
	public Step testStep1(RepositoryItemWriter<Usuario> usuarioWriter) {
		return stepBuilderFactory.get("testStep1")
				.<Usuario, Usuario> chunk(10)
				.reader(usuarioReader())
				.writer(usuarioWriter)
				.processor(testProcessor())
				.build();
	}

}
