package com.colpatria.midas.batch;

import com.colpatria.midas.dto.CargueUtilizacionDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialCargueUtilizacion;
import com.colpatria.midas.processors.CargueUtilizacionItemProcessor;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.CargueUtilizacionItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import com.colpatria.midas.readers.CustomItemReader;
import org.springframework.core.io.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Configuration class for 'cargue utilizacion' batch process
 */
@Configuration
public class CargueUtilizacionBatchConfig {

	/**
     * Add logger to config
     */
    private static final Logger logger = LoggerFactory.getLogger(CargueUtilizacionBatchConfig.class);

	/**
	 * Add job builderFactory to config
	 */
    public final JobBuilderFactory	jobBuilderFactory;

	/**
	 * Add step builder factory to config
	 */
	public final StepBuilderFactory stepBuilderFactory;

	/**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

	/**
	 * add 'historial auto amortizacion' repository
	 */
	private final HistorialCargueUtilizacionRepository historialCargueUtilizacionRepository;

	/**
	 * add 'Tipo Documento' repository
	 */
	private final TipoDocumentoRepository tipoDocumentoRepository;
	
	/*
	 * add 'tipoProducto' repository
	 */
    private final TipoProductoRepository tipoProductoRepository;

	/**
	 * add 'producto' repository
	 */
    private final ProductoRepository productoRepository;

	/**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

    /**
	 * add 'tipo consumo' repository
	 */
	private final TipoConsumoRepository tipoConsumoRepository;

	/**
	 * Add email service
	 */
	private final EmailService emailService;

	/**
	 * Add file history service
	 */
	private final FileHistoryService fileHistoryService;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;

	/**
	 * Cache manager to use
	 */
	private final CacheManager cacheManager;

	/**
	 * @param jobBuilderFactory dependency injection of jobBuilderFactory
	 * @param stepBuilderFactory dependency injection of stepBuilderFactory
	 * @param servicioRepository dependency injection of servicioRepository
	 * @param historialCargueUtilizacionRepository dependency injection of historialCargueUtilizacionRepository
	 * @param tipoProductoRepository dependency injection of tipoProductoRepository
	 * @param productoRepository dependency injection of productoRepository
	 * @param clienteRepository dependency injection of clienteRepository
	 * @param tipoConsumoRepository dependency injection of tipoConsumoRepository
	 * @param emailService dependency injection of emailService
	 * @param fileHistoryService dependency injection of fileHistoryService
	 * @param tipoDocumentoRepository dependency injection of tipoDocumentoRepository
	 * @param validationService dependency injection of validationService
	 * @param cacheManager DI of cache manager
	 */
	@Autowired
	public CargueUtilizacionBatchConfig(
			JobBuilderFactory jobBuilderFactory,
			StepBuilderFactory stepBuilderFactory,
			ServicioRepository servicioRepository,
			HistorialCargueUtilizacionRepository historialCargueUtilizacionRepository,
			TipoProductoRepository tipoProductoRepository,
			ProductoRepository productoRepository,
			ClienteRepository clienteRepository,
			TipoConsumoRepository tipoConsumoRepository,
			EmailService emailService,
			FileHistoryService fileHistoryService,
			TipoDocumentoRepository tipoDocumentoRepository,
			ValidationService validationService,
			CacheManager cacheManager) {
		this.jobBuilderFactory = jobBuilderFactory;
		this.stepBuilderFactory = stepBuilderFactory;
		this.servicioRepository = servicioRepository;
		this.historialCargueUtilizacionRepository = historialCargueUtilizacionRepository;
		this.tipoProductoRepository = tipoProductoRepository;
		this.productoRepository = productoRepository;
		this.clienteRepository = clienteRepository;
		this.emailService = emailService;
		this.fileHistoryService = fileHistoryService;
		this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.tipoConsumoRepository = tipoConsumoRepository;
		this.validationService = validationService;
		this.cacheManager = cacheManager;
	}

	/**
	 * Method that configures the file reading process
     * @param file to be read
	 * @return item reader object that contains the configuration for reading the file in the batch process
     */
    @StepScope
    @Bean
    public FlatFileItemReader<CargueUtilizacionDto> cargoUtilizacionReader(@Value("#{jobParameters['file']}") String file) {
        DelimitedLineTokenizer tokenizer;
        DefaultLineMapper<CargueUtilizacionDto> lineMapper;
        String[] columns = new String[]{
			"CODIGO PRODUCTO",
			"NOMBRE PRODUCTO",
			"CODIGO TIPO PRODUCTO",
			"NOMBRE TIPO PRODUCTO",
			"CODIGO SUBPRODUCTO",
			"NOMBRE SUBPRODUCTO",
			"TIPO_DOCUMENTO",
			"NUMERO_DOCUMENTO",
			"NOMBRE_CLIENTE",
			"NRO_SERVICIO",
			"CIUDAD_ORIGEN",
			"FECHA_POSTEO",
			"FECHA_EFECTIVA",
			"VALOR",
			"VALOR DESCUENTO",
			"VALOR FINANCIADO",
			"TASA",
			"NRO_AUTORIZACION",
			"TIPO_CONSUMO"
		};
        tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter("|");
        tokenizer.setNames(
			"productoCodigo",
			"productoNombre",
			"productoTipoCodigo",
    		"productoTipo",
			"subproductoCodigo",
    		"subproductoNombre",
            "tipoIdentificacion",
			"numeroIdentificacion",
            "clienteNombre",
            "servicioNumero",
            "ciudadOrigen",
            "fechaPosteo",
            "fechaEfectiva",
            "valor",
            "valorDescuento",
            "valorFinanciado",
            "tasa",
            "autorizacionNumero",
            "tipoConsumo"
		);	
        lineMapper = new DefaultLineMapper<>();
        lineMapper.setFieldSetMapper(new BeanWrapperFieldSetMapper<CargueUtilizacionDto>() {{setTargetType(CargueUtilizacionDto.class);}});
        lineMapper.setLineTokenizer(tokenizer);
        logger.info(file);
        Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));
        CustomItemReader<CargueUtilizacionDto> reader = new CustomItemReader<>(columns, "cargueUtilizacion", resource, this.emailService, cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

	/**
	 * Method that configures the writing process
	 * @return item writer object that contains the configuration for writing in the database during the batch process
	 */
	@StepScope
	@Bean
	public CargueUtilizacionItemWriter cargueUtilizacionWriter(){
		return new CargueUtilizacionItemWriter(this.historialCargueUtilizacionRepository, this.clienteRepository, this.servicioRepository, this.validationService);
	}

	/**
	 * Method that configures the processor for the batch process
	 * @param file being read
	 * @return object of the porcessor class
	 */
	@StepScope
	@Bean
	public CargueUtilizacionItemProcessor cargueUtilizacionProcessor(@Value("#{jobParameters['file']}") String fileName) {
		return new CargueUtilizacionItemProcessor(
			fileName,
			this.validationService,
			this.servicioRepository,
			this.tipoDocumentoRepository,
			this.tipoProductoRepository,
			this.productoRepository,
			this.clienteRepository,
			this.tipoConsumoRepository
		);
	}

	/**
	 * Method that configures the listener to be executed after the batch process
	 * @return object of the listener class
	 */
	@Bean
	public LoadJobListener cargueUtilizacionJobListener(){
		 return new LoadJobListener(this.emailService, this.fileHistoryService, validationService, cacheManager);
	}

	/**
	 * Method that configures the job to be executed in the batch process
	 * @param cargueUtilizacionJobListener is the listener to be executed after the job
	 * @param cargueUtilizacionJobStep1 is the first step to be execute during the job
	 * @return job object that contains the configuration for the job
	 */
	@Bean(name = "cargueUtilizacionJob")
	public Job cargueUtilizacionJob(LoadJobListener cargueUtilizacionJobListener, Step cargueUtilizacionJobStep1) {
		return jobBuilderFactory.get("cargueUtilizacionJob")
			.incrementer(new RunIdIncrementer())
			.listener(cargueUtilizacionJobListener)
			.flow(cargueUtilizacionJobStep1)
			.build()
			.build();
	}


	/**
	 * Method that configures the first step to be executed in the batch process
	 * @param cargueUtilizacionReader is the reader to be used in the step
	 * @param cargueUtilizacionWriter is the writer to be used in the step
	 * @param cargueUtilizacionProcessor is the processor to be used in the step
	 * @return object that contains the configuration of the first step in the job
	 */
	@Bean
	public Step cargueUtilizacionJobStep1(
		FlatFileItemReader<CargueUtilizacionDto> cargueUtilizacionReader,
		CargueUtilizacionItemWriter cargueUtilizacionWriter,
		CargueUtilizacionItemProcessor cargueUtilizacionProcessor
	) {
		return stepBuilderFactory.get("CargueUtilizacionJobStep1")
			.<CargueUtilizacionDto, HistorialCargueUtilizacion> chunk(1000)
			.reader(cargueUtilizacionReader)
			.writer(cargueUtilizacionWriter)
			.processor(cargueUtilizacionProcessor)
			.faultTolerant()
			.skip(VerificationException.class)
   			.skipLimit(Integer.MAX_VALUE)
			.build();
	}
    
}
