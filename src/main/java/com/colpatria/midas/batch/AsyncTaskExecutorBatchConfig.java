package com.colpatria.midas.batch;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.scope.context.JobSynchronizationManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

/**
 * Async Task Executor Batch Config
 */
@Configuration
public class AsyncTaskExecutorBatchConfig {
    /**
     * Async Task Executor Config
     *
     * @return Async Task Executor object
     */
    @Bean
    @JobScope
    public TaskExecutor asyncTaskExecutor() {
        return new SimpleAsyncTaskExecutor("AsyncJob_"){
            @Override
            protected void doExecute(Runnable task) {
                JobExecution jobExecution = JobSynchronizationManager.getContext().getJobExecution();
                super.doExecute(() -> {
                    JobSynchronizationManager.register(jobExecution);
                    try {
                        task.run();
                    } finally {
                        JobSynchronizationManager.release();
                    }
                });
            }
        };
    }
}
