package com.colpatria.midas.batch;

import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.JobSettingsService;
import com.colpatria.midas.services.JobSchedulerService;
import com.colpatria.midas.tasklets.MasterTasklet;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Master Job Batch Configuration
 */
@Configuration
@EnableScheduling
public class MasterBatchConfig {

    /**
     * Job Builder Factory Bean reference
     */
    private final JobBuilderFactory jobBuilderFactory;

    /**
     * Step Builder Factory Bean reference
     */
    private final StepBuilderFactory stepBuilderFactory;

    /**
     * Job Launcher Bean reference
     */
    private final JobLauncher jobLauncher;

    /**
     * Job Scheduler service Bean reference
     */
    private final JobSchedulerService jobSchedulerService;

    /**
     * File History service bean reference
     */
    private final FileHistoryService fileHistoryService;

    /**
     * Job Cron service bean reference
     */
    private final JobSettingsService jobSettingsService;

    /**
     * Constructor. Autowire beans to attributes
     *
     * @param jobBuilderFactory   Job Builder Factory Bean
     * @param stepBuilderFactory  Step Builder Factory Bean
     * @param jobLauncher         Job Launcher Bean
     * @param jobSchedulerService Job Scheduler Service Bean
     * @param fileHistoryService  File History Service Bean
     * @param jobSettingsService  Job Cron Service Bean
     */
    @Autowired
    public MasterBatchConfig(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,
                             JobLauncher jobLauncher, JobSchedulerService jobSchedulerService,
                             FileHistoryService fileHistoryService, JobSettingsService jobSettingsService) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.jobLauncher = jobLauncher;
        this.jobSchedulerService = jobSchedulerService;
        this.fileHistoryService = fileHistoryService;
        this.jobSettingsService = jobSettingsService;
    }

    /**
     * MasterTasklet Bean
     *
     * @return Instance of a MasterTasklet Class
     */
    @Bean
    public MasterTasklet masterTasklet() {
        return new MasterTasklet(this.jobSchedulerService, this.fileHistoryService, this.jobSettingsService);
    }

    /**
     * MasterJobStep Bean
     *
     * @param masterTasklet MasterTasklet Bean reference
     * @return Master Job Step
     */
    @Bean
    public Step masterJobStep(MasterTasklet masterTasklet) {
        return this.stepBuilderFactory.get("masterJobStep").tasklet(masterTasklet).build();
    }

    /**
     * MasterJob Bean
     *
     * @param masterJobStep MasterJobStep Bean reference
     * @return Master Job
     */
    @Bean(name = "masterJob")
    public Job masterJob(Step masterJobStep) {
        return this.jobBuilderFactory.get("masterJob").incrementer(new RunIdIncrementer()).flow(masterJobStep).build().build();
    }

}
