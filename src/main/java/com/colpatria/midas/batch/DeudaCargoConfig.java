package com.colpatria.midas.batch;

import com.colpatria.midas.dto.DeudaCargoDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.DeudaCargo;
import com.colpatria.midas.processors.DeudaCargoItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.DeudaCargoItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

/**
 * Class with configuration DeudaCargoConfig
 */
@Configuration
public class DeudaCargoConfig {

    /**
     * Add logger to deudas config
     */
    private static final Logger logger = LoggerFactory.getLogger(DeudaCargoConfig.class);

    /**
     * Add historial pago repository
     */
    private final HistorialPagoRepository historialPagoRepository;

    /**
     * Cache manager to use in methods
     */
    private final CacheManager cacheManager;

    /**
     * Add email service
     */
    private final EmailService emailService;

    /**
     * Add File history repository
     */
    private final FileHistoryService fileHistoryService;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * Add job builderFactory to config
     */
    private final JobBuilderFactory jobBuilderFactory;

    /**
     * Add step builder factory to config
     */
    private final StepBuilderFactory stepBuilderFactory;

    /**
     * add 'servicio' repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * add 'deudacargo' repository
     */
    private final DeudaCargoRepository deudaCargoRepository;

    /**
     * add 'cuenta' repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * add 'producto' repository
     */
    private final ProductoRepository productoRepository;

    /**
     * add 'sucursal' repository
     */
    private final SucursalRepository sucursalRepository;

    /**
     * add 'cargo' repository
     */
    private final CargoRepository cargoRepository;

    /**
     * Tipo producto repository
     */
    private final TipoProductoRepository tipoProductoRepository;

    /**
     * Add Tipo Documento repository
     */
    private final TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * add cliente repository.
     */
    private final ClienteRepository clienteRepository;

    /**
     * add contrato repository
     */
    private ContratoRepository contratoRepository;

    @Autowired
    public DeudaCargoConfig(HistorialPagoRepository historialPagoRepository, CacheManager cacheManager, EmailService emailService, FileHistoryService fileHistoryService, ValidationService validationService, JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, ServicioRepository servicioRepository, DeudaCargoRepository deudaCargoRepository, CuentaRepository cuentaRepository, ProductoRepository productoRepository, SucursalRepository sucursalRepository, CargoRepository cargoRepository, TipoProductoRepository tipoProductoRepository, TipoDocumentoRepository tipoDocumentoRepository, ClienteRepository clienteRepository, ContratoRepository contratoRepository) {
        this.historialPagoRepository = historialPagoRepository;
        this.cacheManager = cacheManager;
        this.emailService = emailService;
        this.fileHistoryService = fileHistoryService;
        this.validationService = validationService;
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.servicioRepository = servicioRepository;
        this.deudaCargoRepository = deudaCargoRepository;
        this.cuentaRepository = cuentaRepository;
        this.productoRepository = productoRepository;
        this.sucursalRepository = sucursalRepository;
        this.cargoRepository = cargoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.clienteRepository = clienteRepository;
        this.contratoRepository = contratoRepository;
    }

    /**
     * @param file path of file to load
     * @return object with reader result
     */
    @StepScope
    @Bean
    public FlatFileItemReader<DeudaCargoDto> deudaReader(@Value("#{jobParameters['file']}") String file) {

        DelimitedLineTokenizer tokenizer;
        DefaultLineMapper<DeudaCargoDto> lineMapper;

        String[] columns = new String[]{
                "CODIGO_PRODUCTO",
                "NOMBRE_PRODUCTO",
                "CODIGO",
                "NOMBRE_TIPO_PRODUCTO",
                "CODIGO_SUBPRODUCTO",
                "NOMBRE_SUBPRODUCTO",
                "TIPO_DOCUMENTO",
                "NUMERO_DOCUMENTO",
                "NUMERO_CONTRATO",
                "NUMERO_CUENTA",
                "SUCURSAL",
                "NRO_DOCUMENTO",
                "FECHA_DOCUMENTO",
                "FECHA_PRIMER_VENCIMIENTO",
                "FECHA_SEGUNDO_VENCIMIENTO",
                "NRO_SERVICIO",
                "DESCRIPCION",
                "COD_CARGO",
                "SALDO",
                "ANTIGUEDAD"
        };
        tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter("|");
        tokenizer.setNames("codigoProducto",
                "nombreProducto",
                "codigoTipoProducto",
                "nombreTipoProducto",
                "codigoSubProducto",
                "nombreSubProducto",
                "tipoIdentificacion",
                "nroIdentificacion",
                "nroContrato",
                "nroCuenta",
                "sucursal",
                "nroDocumentoFacturacion",
                "fechaDocumento",
                "fechaPrimerVencimiento",
                "fechaSegundoVencimiento",
                "nroServicio",
                "descripcion",
                "codCargo",
                "saldo",
                "antiguedad");

        lineMapper = new DefaultLineMapper<>();
        BeanWrapperFieldSetMapper<DeudaCargoDto> wrapper = new BeanWrapperFieldSetMapper<>();
        wrapper.setTargetType(DeudaCargoDto.class);
        lineMapper.setFieldSetMapper(wrapper);
        lineMapper.setLineTokenizer(tokenizer);
        logger.info(file);
        Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));

        CustomItemReader<DeudaCargoDto> reader = new CustomItemReader<>(columns,
                "DeudaCargo", resource,
                this.emailService,
                cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

    /**
     * @return return repository's to step
     */
    @StepScope
    @Bean
    public DeudaCargoItemWriter deudaWriter() {
        return new DeudaCargoItemWriter(this.servicioRepository, this.deudaCargoRepository, this.cuentaRepository, this.validationService, contratoRepository);
    }

    /**
     * @return return processor to steps
     */
    @StepScope
    @Bean
    public DeudaCargoItemProcessor deudaItemProcessor(@Value("#{jobParameters['file']}") String fileName) {
        return new DeudaCargoItemProcessor(this.validationService,
                                           this.sucursalRepository,
                                           this.productoRepository,
                                           this.cargoRepository,
                                           fileName,
                                           this.tipoDocumentoRepository,
                                           this.tipoProductoRepository,
                                           this.clienteRepository,
                                           this.cuentaRepository,
                                           this.contratoRepository,
                                           this.servicioRepository,
                                           this.cacheManager);
    }

    /**
     * @return return listener to steps
     */
    @Bean
    public LoadJobListener deudaListener() {
        return new LoadJobListener(this.emailService, this.fileHistoryService, this.validationService, cacheManager);
    }

    /**
     * @param deudaListener deuda listener of job
     * @param deudaJobStep1 step of job
     * @return return job with configuration
     */
    @Bean(name = "deudaJob")
    public Job deudaJob(LoadJobListener deudaListener, Step deudaJobStep1) {
        return jobBuilderFactory.get("deudaJob")
                .incrementer(new RunIdIncrementer())
                .listener(deudaListener)
                .flow(deudaJobStep1).build()
                .build();
    }

    /**
     * @param deudaReader Pago reader of step
     * @param deudaWriter DI pago writer of step
     * @param processor   processor
     * @return return step configuration
     */
    @Bean
    public Step deudaJobStep1(FlatFileItemReader<DeudaCargoDto> deudaReader,
                              DeudaCargoItemWriter deudaWriter,
                              DeudaCargoItemProcessor processor) {
        return stepBuilderFactory.get("deudaJobStep1")
                .<DeudaCargoDto, DeudaCargo>chunk(100)
                .reader(deudaReader)
                .writer(deudaWriter)
                .processor(processor)
                .faultTolerant()
                .skip(VerificationException.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }
}
