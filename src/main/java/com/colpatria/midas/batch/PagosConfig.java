package com.colpatria.midas.batch;

import com.colpatria.midas.dto.PagosDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialPago;
import com.colpatria.midas.processors.PagoItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.PagoItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;


/**
 * Class with pagos job configuration
 */
@Configuration
public class PagosConfig {

    /**
     * Add logger to pagos config
     */
    private static final Logger logger = LoggerFactory.getLogger(PagosConfig.class);

    /**
     * Add job builderFactory to config
     */
    private final JobBuilderFactory jobBuilderFactory;

    /**
     * Add step builder factory to config
     */
    private final StepBuilderFactory stepBuilderFactory;

    /**
     * add product repository
     */
    private final ProductoRepository productoRepository;

    /**
     * Add client repository
     */
    private final ClienteRepository clienteRepository;

    /**
     * Add Tipo Documento repository
     */
    private final TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Add sucursal repository
     */
    private final SucursalRepository sucursalRepository;

    /**
     * Add historial pago repository
     */
    private final HistorialPagoRepository historialPagoRepository;

    /**
     * Add email service
     */
    private final EmailService emailService;

    /**
     * Tipo producto repository
     */
    private final TipoProductoRepository tipoProductoRepository;

    /**
     * Validation service
     */
    private final ValidationService validationService;


    /**
     * Add File history repository
     */
    private final FileHistoryService fileHistoryService;

    /**
     * Cargo repository
     */
    private CargoRepository cargoRepository;

    /**
     * Cache manager to use
     */
    private final CacheManager cacheManager;

    /**
     * @param jobBuilderFactory       DI of job builder factory
     * @param stepBuilderFactory      DI of step Builder Factory
     * @param productoRepository      DI of productoRepository
     * @param clienteRepository       DI of clienteRepository
     * @param sucursalRepository      DI of sucursalRepository
     * @param historialPagoRepository DI of historialPagoRepository
     * @param emailService            DI of emailService
     * @param tipoProductoRepository  di of tipoProductoRepository
     * @param fileHistoryService      DI of fileHistoryService
     * @param validationService DI of validation service
     * @param cargoRepository DI of cargo repository
     * @param cacheManager DI of cache manager
     */
    @Autowired
    public PagosConfig(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, ProductoRepository productoRepository, ClienteRepository clienteRepository, SucursalRepository sucursalRepository, HistorialPagoRepository historialPagoRepository, EmailService emailService, TipoProductoRepository tipoProductoRepository, FileHistoryService fileHistoryService, TipoDocumentoRepository tipoDocumentoRepository, ValidationService validationService, CargoRepository cargoRepository, CacheManager cacheManager) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.productoRepository = productoRepository;
        this.clienteRepository = clienteRepository;
        this.sucursalRepository = sucursalRepository;
        this.historialPagoRepository = historialPagoRepository;
        this.emailService = emailService;
        this.tipoProductoRepository = tipoProductoRepository;
        this.fileHistoryService = fileHistoryService;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.validationService = validationService;
        this.cargoRepository = cargoRepository;
        this.cacheManager = cacheManager;
    }

    /**
     * @param file path of file to load
     * @return object with reader result
     */
    @StepScope
    @Bean
    public FlatFileItemReader<PagosDto> pagosReader(@Value("#{jobParameters['file']}") String file) {
        // Variables
        DelimitedLineTokenizer tokenizer;
        DefaultLineMapper<PagosDto> lineMapper;
        // Código
        String[] columns = new String[]{
                "CODIGO PRODUCTO",
                "NOMBRE PRODUCTO",
                "CODIGO",
                "NOMBRE TIPO PRODUCTO",
                "CODIGO SUBPRODUCTO",
                "NOMBRE SUBPRODUCTO",
                "NUMERO_SERVICIO",
                "TIPO_DOCUMENTO",
                "NUMERO_DOCUMENTO",
                "NOMBRE",
                "APELLIDO_PAT",
                "APELLIDO_MAT",
                "SUCURSAL",
                "NRO_DOCUMENTO_PAGADO",
                "FECHA_INGRESO",
                "FECHA_PAGO",
                "FECHA_VENCIMIENTO",
                "NRO SERVICIO",
                "COD_CARGO_COLPATRIA",
                "COD_CARGO",
                "DESCRIPCION_CARGO",
                "MONTO",
                "SIGNO",
                "CUENTA",
                "MONTO_PARTICIPACION"};
        tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter("|");
        tokenizer.setNames("codigoProducto",
                "nombreProducto",
                "codigoTipoProducto",
                "nombreTipoProducto",
                "codigoSubproducto",
                "nombreSubproducto",
                "nroServicio",
                "tipoDocumentoIdentidad",
                "nroDocumentoTitular",
                "nombreCliente",
                "apellidoPaterno",
                "apellidoMaterno",
                "sucursal",
                "nroDocPago",
                "fechaIngreso",
                "fechaPago",
                "fechaVencimiento",
                "numeroServicio",
                "codCargoColpatria",
                "codCargo",
                "descCargo",
                "monto",
                "signo",
                "cuentaContable",
                "montoParticipacion");

        lineMapper = new DefaultLineMapper<>();
        BeanWrapperFieldSetMapper<PagosDto> wrapper = new BeanWrapperFieldSetMapper<>();
        wrapper.setTargetType(PagosDto.class);
        lineMapper.setFieldSetMapper(wrapper);
        lineMapper.setLineTokenizer(tokenizer);
        logger.info(file);
        Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));

        CustomItemReader<PagosDto> reader = new CustomItemReader<>(columns,
                "pagos", resource,
                this.emailService,
                cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

    /**
     * @return return repository's to step
     */
    @StepScope
    @Bean
    public PagoItemWriter pagoWriter() {
        return new PagoItemWriter(this.historialPagoRepository,this.clienteRepository,this.validationService );
    }

    /**
     * @return return processor to steps
     */
    @StepScope
    @Bean
    public PagoItemProcessor pagoItemProcessor(@Value("#{jobParameters['file']}") String fileName) {
        return new PagoItemProcessor(
                fileName,
                this.emailService,
                this.productoRepository,
                this.tipoProductoRepository,
                this.clienteRepository,
                this.sucursalRepository,
                this.historialPagoRepository,
                this.tipoDocumentoRepository,
                this.validationService,
                this.cargoRepository
        );
    }

    /**
     * @return return listener to steps
     */
    @Bean
    public LoadJobListener pagoListener() {
        return new LoadJobListener(this.emailService, this.fileHistoryService, this.validationService, cacheManager);
    }

    /**
     * @param pagoListener pago listener of job
     * @param pagoJobStep1 step of job
     * @return return job with configuration
     */
    @Bean(name = "pagoJob")
    public Job pagoJob(LoadJobListener pagoListener, Step pagoJobStep1) {
        return jobBuilderFactory.get("pagoJob")
                .incrementer(new RunIdIncrementer())
                .listener(pagoListener)
                .flow(pagoJobStep1).build()
                .build();
    }

    /**
     * @param pagoReader Pago reader of step
     * @param pagoWriter DI pago writer of step
     * @param processor  processor
     * @return return step configuration
     */
    @Bean
    public Step pagoJobStep1(FlatFileItemReader<PagosDto> pagoReader,
                             PagoItemWriter pagoWriter,
                             PagoItemProcessor processor) {
        return stepBuilderFactory.get("pagoJobStep1")
                .<PagosDto, HistorialPago>chunk(100)
                .reader(pagoReader)
                .writer(pagoWriter)
                .processor(processor)
                .faultTolerant()
                .skip(VerificationException.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }
}
