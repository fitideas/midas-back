package com.colpatria.midas.batch.outfile;

import com.colpatria.midas.model.HistorialCarteraLight;
import com.colpatria.midas.repositories.HistorialCarteraLightRepository;
import com.colpatria.midas.utils.DateUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.builder.RepositoryItemReaderBuilder;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class ProbeCasasDeCobranzaBatchConfig {

    private final HistorialCarteraLightRepository historialCarteraLightRepository;

    @Autowired
    public ProbeCasasDeCobranzaBatchConfig(HistorialCarteraLightRepository historialCarteraLightRepository) {
        this.historialCarteraLightRepository = historialCarteraLightRepository;
    }

    @StepScope
    @Bean
    public RepositoryItemReader<HistorialCarteraLight> pagoCobranzaReader(){
        Map<String, Sort.Direction> sorts = new HashMap<>();
        sorts.put("id", Sort.Direction.ASC);

        List<Object> queryMethodArguments = new ArrayList<>();
        queryMethodArguments.add(DateUtils.stringToLocalDate("2022-01-01"));
        queryMethodArguments.add(DateUtils.stringToLocalDate("2022-12-31"));

        return new RepositoryItemReaderBuilder<HistorialCarteraLight>()
                .repository(this.historialCarteraLightRepository)
                //.sorts(sorts)
                .methodName("getHistorialCarteraLightByFechaReporte")
                //.arguments(queryMethodArguments)
                .pageSize(100)
                .build();
    }

    @StepScope
    @Bean
    public FlatFileItemWriter<Object> pagoCobranzaWriter(){
        return new FlatFileItemWriterBuilder<>()
                .append(false)
                .delimited().delimiter("|").names("")
                .resource(new PathResource(""))
                .build();
    }
}
