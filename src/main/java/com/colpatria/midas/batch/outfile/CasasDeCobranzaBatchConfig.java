package com.colpatria.midas.batch.outfile;

import com.colpatria.midas.dto.CasasDeCobranzaProbeDto;
import com.colpatria.midas.dto.CasasDeCobranzaProbeQueryDto;
import com.colpatria.midas.listeners.ReportGeneratorJobListener;
import com.colpatria.midas.processors.CasasDeCobranzaProbeItemProcessor;
import com.colpatria.midas.readers.CasasDeCobranzaProbeItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.CalendarioFacturacionService;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.utils.DateUtils;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.job.flow.support.SimpleFlow;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Casas de cobranza Batch Config
 */
@Configuration
public class CasasDeCobranzaBatchConfig {

    /**
     * Job Builder Factory bean reference
     */
    private final JobBuilderFactory jobBuilderFactory;

    /**
     * Step Builder Factory bean reference
     */
    private final StepBuilderFactory stepBuilderFactory;

    /**
     * Historial Cartera Light Repository bean reference
     */
    private final HistorialCarteraLightRepository historialCarteraLightRepository;

    /**
     * Historial Recaudo Repository bean reference
     */
    private final HistorialRecaudoRepository historialRecaudoRepository;

    private final HistorialCarteraResumenRepository historialCarteraResumenRepository;

    private final ReclamacionRepository reclamacionRepository;

    private final HistorialRecaudoResumenRepository historialRecaudoResumenRepository;

    private final HistorialRefinanciacionRepository historialRefinanciacionRepository;

    private final CalendarioFacturacionRepository calendarioFacturacionRepository;

    private final CalendarioFacturacionService calendarioFacturacionService;

    /**
     * Email Service
     */
    private final EmailService emailService;

    /**
     * Output Path
     */
    @Value("${midas.jobs.path.output}")
    private String outputPath;

    /**
     * Class Constructor.
     *
     * @param jobBuilderFactory Job Builder Factory bean reference
     */
    @Autowired
    public CasasDeCobranzaBatchConfig(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, HistorialCarteraLightRepository historialCarteraLightRepository, HistorialRecaudoRepository historialRecaudoRepository, HistorialCarteraResumenRepository historialCarteraResumenRepository, EmailService emailService, ReclamacionRepository reclamacionRepository, HistorialRecaudoResumenRepository recaudoResumenRepository, HistorialRefinanciacionRepository historialRefinanciacionRepository, CalendarioFacturacionRepository calendarioFacturacionRepository, CalendarioFacturacionService calendarioFacturacionService) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.historialCarteraLightRepository = historialCarteraLightRepository;
        this.historialRecaudoRepository = historialRecaudoRepository;
        this.historialCarteraResumenRepository = historialCarteraResumenRepository;
        this.reclamacionRepository = reclamacionRepository;
        this.historialRecaudoResumenRepository = recaudoResumenRepository;
        this.historialRefinanciacionRepository = historialRefinanciacionRepository;
        this.calendarioFacturacionRepository = calendarioFacturacionRepository;
        this.calendarioFacturacionService = calendarioFacturacionService;
        this.emailService = emailService;
    }


    /**
     * Report Generator Job Listener
     *
     * @return Report Generator Job Listener Object
     */
    @JobScope
    @Bean
    public ReportGeneratorJobListener reportGeneratorJobListener() {
        return new ReportGeneratorJobListener(this.emailService);
    }


    /**
     * Casas De Cobranza Job Config
     *
     * @param casasDeCobranzaFlow        Casas de cobranza Flow Bean refernece
     * @param reportGeneratorJobListener Reporte Generator Job Listener Object
     * @return Casas de cobranza Job
     */
    @Bean(name = "casasDeCobranza")
    public Job parallelJobs(Flow casasDeCobranzaFlow, ReportGeneratorJobListener reportGeneratorJobListener) {
        return this.jobBuilderFactory.get("casasDeCobranza")
                .incrementer(new RunIdIncrementer())
                .listener(reportGeneratorJobListener)
                .start(casasDeCobranzaFlow)
                .build()
                .build();
    }

    /**
     * Casas de Cobranza Flow definition
     *
     * @param asyncTaskExecutor Async Task Executor Bean reference
     * @param probeFlow         Probe Flow bean reference
     * @return Casas De Cobranza Flow
     */
    @Bean
    public Flow casasDeCobranzaFlow(TaskExecutor asyncTaskExecutor, Flow probeFlow/*, Flow demoFLow, Flow telefonosDesactivadosFlow*/) {
        return new FlowBuilder<SimpleFlow>("parallelFlow")
                .split(asyncTaskExecutor)
                .add(probeFlow/*, demoFlow, telefonosDesactivadosFlow*/)
                .build();
    }

    /**
     * Probe Flow Config
     *
     * @param probeStep Casas de cobranza bean reference
     * @return Flow object
     */
    @Bean
    public Flow probeFLow(Step probeStep) {
        return new FlowBuilder<SimpleFlow>("flowByStep")
                .start(probeStep)
                .build();
    }

    /**
     * Probe Step Config
     *
     * @param casasDeCobranzaProbeItemProcessor Casas de Cobrannza Probe Item Processor
     * @return Probe Step object
     */
    @Bean
    public Step probeStep(CasasDeCobranzaProbeItemReader casasDeCobranzaProbeItemReader, CasasDeCobranzaProbeItemProcessor casasDeCobranzaProbeItemProcessor, FlatFileItemWriter<CasasDeCobranzaProbeDto> casasDeCobranzaProbeItemWriter, ReportGeneratorJobListener reportGeneratorJobListener) {
        return this.stepBuilderFactory.get("casasDeCobranzaProbeStep")
                .<CasasDeCobranzaProbeQueryDto, CasasDeCobranzaProbeDto>chunk(50)
                .reader(casasDeCobranzaProbeItemReader)
                .processor(casasDeCobranzaProbeItemProcessor)
                .writer(casasDeCobranzaProbeItemWriter)
                .listener(reportGeneratorJobListener)
                .build();
    }

    /**
     * Casas de Cobranza Probe Item Reader
     *
     * @return Item Reader
     */
    @Bean
    @StepScope
    public CasasDeCobranzaProbeItemReader casasDeCobranzaProbeItemReader() {
        Map<String, Sort.Direction> sorts = new HashMap<>();
        sorts.put("id", Sort.Direction.ASC);
        CasasDeCobranzaProbeItemReader reader = new CasasDeCobranzaProbeItemReader();
        reader.setLastLoad(DateUtils.stringToLocalDate(this.historialCarteraLightRepository.getLastLoad()));
        reader.setPageSize(50);
        reader.setSort(sorts);
        reader.setName("casasDeCobranzaProbeItemReader");
        reader.setHistorialCarteraLightRepository(this.historialCarteraLightRepository);
        reader.setHistorialCarteraResumenRepository(this.historialCarteraResumenRepository);
        reader.setCalendarioFacturacionRepository(this.calendarioFacturacionRepository);
        reader.setCalendarioFacturacionService(this.calendarioFacturacionService);
        reader.setHistorialRecaudoRepository(this.historialRecaudoRepository);
        reader.setReclamacionRepository(this.reclamacionRepository);
        reader.setHistorialRecaudoResumenRepository(this.historialRecaudoResumenRepository);
        reader.setHistorialRefinanciacionRepository(this.historialRefinanciacionRepository);
        return reader;
    }

    /**
     * Casas De Cobranza Probe Item Processor
     *
     * @return Casas de Cobrazna Probe Item Processor object
     */
    @Bean
    public CasasDeCobranzaProbeItemProcessor casasDeCobranzaProbeItemProcessor() {
        return new CasasDeCobranzaProbeItemProcessor();
    }

    /**
     * Casas de cobranza ProbeItemWriter Config
     *
     * @return Item Writer
     */
    @Bean
    @StepScope
    public FlatFileItemWriter<CasasDeCobranzaProbeDto> casasDeCobranzaProbeItemWriter() {
        return new FlatFileItemWriterBuilder<CasasDeCobranzaProbeDto>()
                .append(false)
                .delimited().delimiter("|").names("numide", "numobl", "saldoK", "diasmo1", "diasmo2", "edad1", "edad2", "cuomor", "saldoVenc", "fecRedif", "valPago", "valorCuot", "cantPagos", "fecVence", "tipoTarj", "proend", "diasmoPro", "pagMinimo", "mLibros", "esp", "tipide", "numide1", "famparada", "doctoAmp", "tipoTarje", "fecUltRe", "saldoTot", "cantRefi", "tipModi1", "fecModif", "fecSegu", "numeroCli", "fechaRepo", "indapert", "cantApert", "asiignado", "fecutil", "indIloc", "cantComp", "valComp", "fechaCuotaMasVencida", "valorDesembolso", "intCte", "intMot", "intTot", "cManejo", "gCobranz", "cTasain", "ciclo", "valPag", "fecPag", "sucursal", "fecha", "plazo", "numeroObligacionCobranzas", "flagReclamo")
                .resource(new PathResource(this.outputPath + "casas_de_cobranza_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern( "dd-MM-yyyy_hh-mm-ss")) + ".txt"))
                .headerCallback(writer -> writer.write("numide|numobl|saldo_k|diasmo1|diasmo2|edad1|edad2|cuomor|saldo_venc|fec_redif|val_pago|valor_cuot|cant_pagos|fec_vence|tipo_tarj|proend|diasmo_pro|pag_minimo|m_libros|esp|tipide|numide1|famparada|docto_amp|tipo_tarje|fec_ult_re|saldo_tot|cant_refi|tip_modi1|fec_modif|fec_segu|numero_cli|fecha_repo|indapert|cant_apert|asiignado|fecutil|ind_iloc|cant_comp|val_comp |CFECACMV|cvlrdes|INTCTE|INTMOR|INTTOT|CMANEJO|GCOBRAN|CTASAIN|CICLO|VALPAG|FECPAG|Sucursal|Fecha|plazo|Número Obligación Cobranzas|Flag Reclamo"))
                .saveState(false)
                .name("casasDeCobranzaProbeItemWriter")
                .build();
    }

    // TODO: Step definition for Demo and Tel

}
