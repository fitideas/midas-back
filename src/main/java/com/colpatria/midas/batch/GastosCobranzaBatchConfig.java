package com.colpatria.midas.batch;

import java.util.HashMap;
import com.colpatria.midas.dto.GastoCobranzaDto;
import com.colpatria.midas.model.GastoCobranza;
import com.colpatria.midas.processors.GastoCobranzaItemProcessor;
import com.colpatria.midas.repositories.AttributesRepository;
import com.colpatria.midas.repositories.GastoCobranzaRepository;
import com.colpatria.midas.writers.GastoCobranzaItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.beans.factory.annotation.Value;

/**
 * Configuration class for 'gastos cobranza' batch process
 */
@Configuration
public class GastosCobranzaBatchConfig {

    /**
     * Add logger to config
     */
    private static final Logger logger = LoggerFactory.getLogger(GastosCobranzaBatchConfig.class);

	/**
     * Path to save the generated reports
     */
    @Value("${midas.jobs.path.reports}")
    private String reportPath;

    /**
	 * Add job builderFactory to config
	 */
    public final JobBuilderFactory	jobBuilderFactory;

	/**
	 * Add step builder factory to config
	 */
	public final StepBuilderFactory stepBuilderFactory;

	/**
	 * add 'gastoCobranza' repository
	 */
	private final GastoCobranzaRepository gastoCobranzaRepository;

	/**
	 * add 'attributes' repository
	 */
	private final AttributesRepository attributesRepository;

    /**
	 * @param jobBuilderFactory dependency injection of jobBuilderFactory
	 * @param stepBuilderFactory dependency injection of stepBuilderFactory
	 * @param gastoCobranzaRepository dependency injection of gastoCobranzaRepository
	 * @param attributesRepository dependency injection of attributesRepository
	 */
	@Autowired
	public GastosCobranzaBatchConfig(
		JobBuilderFactory jobBuilderFactory,
		StepBuilderFactory stepBuilderFactory,
		GastoCobranzaRepository gastoCobranzaRepository,
		AttributesRepository attributesRepository
	) {
		this.jobBuilderFactory = jobBuilderFactory;
		this.stepBuilderFactory = stepBuilderFactory;
		this.gastoCobranzaRepository = gastoCobranzaRepository;
		this.attributesRepository = attributesRepository;
	}

	/**
	 * Method that configures the file reading process
	 * @return item reader object that contains the configuration for reading the file in the batch process
     */
    @StepScope
    @Bean
    public RepositoryItemReader<GastoCobranza> gastoCobranzaItemReader() {
        RepositoryItemReader<GastoCobranza> repositoryItemReader = new RepositoryItemReader<>();
		repositoryItemReader.setRepository(gastoCobranzaRepository);
		repositoryItemReader.setMethodName("findAll");
		final HashMap<String, Sort.Direction> sorts = new HashMap<>();
		sorts.put("id", Sort.Direction.ASC);
		repositoryItemReader.setSort(sorts);
		return repositoryItemReader;
    }

	/**
	 * Method that configures the file writing process
	 * @return item reader object that contains the configuration for writing the file in the batch process
     */
	@StepScope
	@Bean
	public GastoCobranzaItemWriter gastoCobranzaItemWriter(){
		return new GastoCobranzaItemWriter(this.reportPath);
	}
	

	/**
	 * Method that configures the processor for the batch process
	 * @return object of the porcessor class
	 */
	@StepScope
	@Bean
	public GastoCobranzaItemProcessor gastoCobranzaProcessor() {
		return new GastoCobranzaItemProcessor(this.attributesRepository, this.gastoCobranzaRepository);
	}

	/**
	 * Method that configures the job to be executed in the batch process
	 * @param gastoCobranzaStep1 is the first step to be execute during the job
	 * @return job object that contains the configuration for the job
	 */
	@Bean(name = "gastoCobranzaJob")
	public Job gastoCobranzaJob(Step gastoCobranzaStep1) {
		return jobBuilderFactory.get("gastoCobranzaJob")
			.incrementer(new RunIdIncrementer())
			.start(gastoCobranzaStep1)
			.build();
	}


	/**
	 * Method that configures the first step to be executed in the batch process
	 * @param gastoCobranzaItemReader is the reader to be used in the step
	 * @param gastoCobranzaItemWriter is the writer to be used in the step
	 * @param gastoCobranzaProcessor is the processor to be used in the step
	 * @return object that contains the configuration of the first step in the job
	 */
	@Bean
	public Step gastoCobranzaStep1(
		RepositoryItemReader<GastoCobranza> gastoCobranzaItemReader,
		GastoCobranzaItemWriter gastoCobranzaItemWriter,
		GastoCobranzaItemProcessor gastoCobranzaProcessor
	) {
		return stepBuilderFactory.get( "gastoCobranzaStep1" )
			.<GastoCobranza, GastoCobranzaDto> chunk( 1000 )
			.reader(gastoCobranzaItemReader)
			.writer(gastoCobranzaItemWriter)
			.processor(gastoCobranzaProcessor)
			.build();
	}

	
    
}
