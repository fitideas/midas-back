package com.colpatria.midas.batch;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.dto.RefinanciacionDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialRefinanciacion;
import com.colpatria.midas.processors.RefinanciacionItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.RefinanciacionItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

/*
 *
 * Clase
 *
*/

@Configuration
public class RefinanciacionBatchConfig {

	/*
	 *
	 * Constantes
	 *
	*/

	/**
	 * Add logger to pagos config
	 */
	private static final Logger logger = LoggerFactory.getLogger(RefinanciacionBatchConfig.class);

	/**
	 * String the specifies the job name
	*/
	private static final String JOB_NAME = "refinanciacionJob";

	/**
	 * String the specifies the file separator symbol
	*/
	private static final String SEPARATOR_SYMBOL = "|";

	/*
	 *
	 * Atríbutos
	 *
	*/

	/**
	 * Add job builderFactory to config
	 */
	public final JobBuilderFactory  jobBuilderFactory;
	/**
	 * Add step builder factory to config
	 */
	public final StepBuilderFactory stepBuilderFactory;

	/**
     * Repository objects to read and write to the database
    */
	private final ServicioRepository                servicioRepository;
	private final EmailService                      emailService;
	private final FileHistoryService                fileHistoryService;
	private final UsuarioRepository                 usuarioRepository;
	private final HistorialRefinanciacionRepository historialRefinanciacionRepository;
	private final NegociacionRepository             negociacionRepository;
	private final ProductoRepository                productoRepository;
    private final TipoProductoRepository            tipoProductoRepository;
    private final ContratoRepository                contratoRepository;
    private final ClienteRepository                 clienteRepository;
    private final CuentaRepository                  cuentaRepository;
	private final TipoDocumentoRepository			tipoDocumentoRepository;

	/**
	 * Cache manager to use
	 */
	private final CacheManager cacheManager;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;
	/*
	 *
	 * Métodos
	 *
	*/

	/**
	 * Class constructor
	 * @param jobBuilderFactory is batch job builder factory
	 * @param stepBuilderFactory is batch step builder factory
	 * @param servicioRepository is the service repository
	 * @param historialRefinanciacionRepository is the history repository
	 * @param emailService is the service to send emails
	 * @param fileHistoryService is the service to move the files
	 * @param usuarioRepository is the user repository
	 * @param negociacionRepository is the negotiation repository
	 * @param productoRepository is the product repository
	 * @param tipoProductoRepository is the product type repository
	 * @param contratoRepository is the contract repository
	 * @param clienteRepository is the client repository
	 * @param cuentaRepository is the account repository
	 * @param tipoDocumentoRepository is the document type repository
	 * @param cacheManager DI of cache manager
	 * @param validationService
	 */
	@Autowired
	public RefinanciacionBatchConfig(
			JobBuilderFactory jobBuilderFactory,
			StepBuilderFactory stepBuilderFactory,
			ServicioRepository servicioRepository,
			HistorialRefinanciacionRepository historialRefinanciacionRepository,
			EmailService emailService,
			FileHistoryService fileHistoryService,
			UsuarioRepository usuarioRepository,
			NegociacionRepository negociacionRepository,
			ProductoRepository productoRepository,
			TipoProductoRepository tipoProductoRepository,
			ContratoRepository contratoRepository,
			ClienteRepository clienteRepository,
			CuentaRepository cuentaRepository,
			TipoDocumentoRepository tipoDocumentoRepository,
			CacheManager cacheManager, ValidationService validationService) {
		this.jobBuilderFactory                 = jobBuilderFactory;
		this.stepBuilderFactory                = stepBuilderFactory;
		this.servicioRepository                = servicioRepository;
		this.historialRefinanciacionRepository = historialRefinanciacionRepository;
		this.emailService                      = emailService;
		this.fileHistoryService                = fileHistoryService;
		this.usuarioRepository                 = usuarioRepository;
		this.negociacionRepository             = negociacionRepository;
		this.productoRepository                = productoRepository;
		this.tipoProductoRepository            = tipoProductoRepository;
		this.contratoRepository                = contratoRepository;
		this.clienteRepository                 = clienteRepository;
		this.cuentaRepository                  = cuentaRepository;
		this.tipoDocumentoRepository		   = tipoDocumentoRepository;
		this.cacheManager = cacheManager;
		this.validationService = validationService;
	}

	/**
	 * Method that configures the file reading process
	 * @param file to be read
	 * @return item reader object that contains the configuration for reading the file in the batch process
	*/
	@StepScope
	@Bean
	public FlatFileItemReader<RefinanciacionDto> refinanciacionReader( @Value("#{jobParameters['file']}") String file ){
		// Variables
		DelimitedLineTokenizer                     tokenizer;
		DefaultLineMapper<RefinanciacionDto> lineMapper;
		String[] columns = new String[]{"CODIGO PRODUCTO",
				"NOMBRE PRODUCTO",
				"CODIGO TIPO PRODUCTO",
				"NOMBRE TIPO PRODUCTO",
				"CODIGO",
				"SUBPRODUCTO",
				"TIPO_IDENTIFICACION",
				"NRO_DOCTO_IDENT",
				"NUMERO_CONTRATO",
				"NUMERO_CUENTA",
				"CICLO",
				"ID_NEGOCIACION",
				"FECHANEG",
				"VALOR_PAGO",
				"CUOTA_INICIAL",
				"DESCUENTO_CUOTA_INICIAL",
				"FECHAACT",
				"FECHAAPLI",
				"FECHA RECHAZO",
				"MOTIVO_RECHAZO",
				"ESTADO_NEGOCIACION",
				"NRO_SERVICIO",
				"CANTIDAD_CUOTAS_ANTERIORES",
				"CUOTA_MENSUAL_ANTERIOR",
				"CANTIDAD_CUOTAS_DESPUES_DE_REFINANCIACION",
				"CUOTA_MENSUAL_DESPUES",
				"TASA_ANTERIOR",
				"TASA_NUEVA",
				"TASA_CAMBIO_PLAZO",
				"COD_USUARIO",
				"USUARIO",
				"AREA"};
		// Código
		tokenizer = new DelimitedLineTokenizer();
		tokenizer.setDelimiter( SEPARATOR_SYMBOL );
		tokenizer.setNames(
			"codigoProducto",
			"nombreProducto",
			"codigoTipoProducto",
			"nombreTipoProducto",
			"codigoSubproducto",
			"nombreSubproducto",
			"tipoIdentificacion",
			"numeroIdentificacion",
			"numeroContrato",
			"numeroCuenta",
			"ciclo",
			"idNegociacion",
			"fechaNegociacion",
			"valorPago",
			"cuotaInicial",
			"descuentoCuotaInicial",
			"fechaAct",
			"fechaApli",
			"fechaRechazo",
			"motivoRechazo",
			"estadoNegociacion",
			"numeroServicio",
			"cantidadCuotasAnteriores",
			"cuotaMensualAnterior",
			"cantidadCuotasDespuesRefinanciacion",
			"cuotaMensualDespues",
			"tasaAnterior",
			"tasaNueva",
			"tasaCambioPlaza",
			"codigoUsuario",
			"nombreUsuario",
			"area"
		);
		lineMapper = new DefaultLineMapper<>();
		lineMapper.setFieldSetMapper(new BeanWrapperFieldSetMapper<RefinanciacionDto>() {
			{
				setTargetType(RefinanciacionDto.class);
			}
		});
		lineMapper.setLineTokenizer(tokenizer);
		logger.info(file);
		Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));

		CustomItemReader<RefinanciacionDto> reader = new CustomItemReader<>(columns,
				"refinanciaciones", resource,
				this.emailService,
                cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
		reader.setLinesToSkip(1);
		reader.setLineMapper(lineMapper);
		return reader;
	}

	/**
	 * Method that configures the writing process
	 * @param file to be read
	 * @return item writer object that contains the configuration for writing in the database during the batch process
	*/
	@StepScope
	@Bean
	public RefinanciacionItemWriter refinanciacionWriter( @Value("#{jobParameters['file']}") String file ){
		return new RefinanciacionItemWriter(this.historialRefinanciacionRepository,
				this.validationService,
				this.cuentaRepository,
				this.clienteRepository,
				this.contratoRepository,
				this.servicioRepository,
				this.negociacionRepository,
				this.usuarioRepository);
	}

	/**
	 * Method that configures the processor for the batch process
	 * @param fileName to be read
	 * @return object of the porcessor class
	*/
	@StepScope
	@Bean
	public RefinanciacionItemProcessor refinanciacionProcessor( @Value("#{jobParameters['file']}") String fileName ) {
		return new RefinanciacionItemProcessor( fileName,
				this.productoRepository,
				this.tipoProductoRepository,
				this.tipoDocumentoRepository,
				this.clienteRepository,
				this.negociacionRepository,
				this.usuarioRepository,
				this.cuentaRepository,
				this.contratoRepository,
				this.servicioRepository,
				this.validationService);
	}

	/**
	 * Method that configures the listener to be executed after the batch process
	 * @return object of the listener class
	*/
	@Bean
	public LoadJobListener refinanciacionJobListener(){
		return new LoadJobListener( this.emailService, this.fileHistoryService, validationService, cacheManager);
	}

	/**
	 * Method that configures the job to be executed in the batch process
	 * @param refinanciacionJobListener is the listener to be executed after the job
	 * @param refinanciacionJobStep1 is the first step to be execute during the job
	 * @return a job object that contains the configuration for the job execution
	*/
	@Bean( name = JOB_NAME )
	public Job refinanciacionJob( LoadJobListener refinanciacionJobListener, Step refinanciacionJobStep1 ) {
		return jobBuilderFactory.get( JOB_NAME )
			.incrementer( new RunIdIncrementer() )
			.listener( refinanciacionJobListener )
			.flow( refinanciacionJobStep1 ).build().build();
	}

	/**
	 * Method that configures the first step to be executed in the batch process
	 * @param reader is the reader to be used in the step
	 * @param writer is the writer to be used in the step
	 * @param processor is the processor to be used in the step
	 * @return object that contains the configuration of the first step in the job
	*/
	@Bean
	public Step refinanciacionJobStep1(
		FlatFileItemReader<RefinanciacionDto> reader,
		RefinanciacionItemWriter              writer,
		RefinanciacionItemProcessor           processor
	) {
		return stepBuilderFactory.get( "refinanciacionJobStep1" )
			.<RefinanciacionDto, HistorialRefinanciacion> chunk( 1000 )
			.reader( reader )
			.writer( writer )
			.processor( processor )
			.faultTolerant()
			.skip( VerificationException.class )
			.skipLimit( Integer.MAX_VALUE )
			.build();
	}

}