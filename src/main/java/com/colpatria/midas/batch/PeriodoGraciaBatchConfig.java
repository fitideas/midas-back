package com.colpatria.midas.batch;

import com.colpatria.midas.dto.PeriodoGraciaDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialPeriodoGracia;
import com.colpatria.midas.processors.PeriodoGraciaItemProcessor;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.PeriodoGraciaItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import com.colpatria.midas.readers.CustomItemReader;
import org.springframework.core.io.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* Configuration class for 'periodoGracia' batch process
*/
@Configuration
public class PeriodoGraciaBatchConfig {

	/**
     * Add logger to config
     */
    private static final Logger logger = LoggerFactory.getLogger(PeriodoGraciaBatchConfig.class);

	/**
	 * Add job builderFactory to config
	 */
    public final JobBuilderFactory	jobBuilderFactory;

	/**
	 * Add step builder factory to config
	 */
	public final StepBuilderFactory stepBuilderFactory;

	/**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

	/**
	 * add 'historial auto amortizacion' repository
	 */
	private final HistorialPeriodoGraciaRepository historialPeriodoGraciaRepository;

	/**
	 * add 'usuario' repository
	 */
    private final UsuarioRepository usuarioRepository;

	/**
	 * add 'Tipo Documento' repository
	 */
	private final TipoDocumentoRepository tipoDocumentoRepository;
	
	/*
	 * add 'tipoProducto' repository
	 */
    private final TipoProductoRepository tipoProductoRepository;

	/**
	 * add 'producto' repository
	 */
    private final ProductoRepository productoRepository;

	/**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

	/**
	 * add 'contrato' repository
	 */
	private final ContratoRepository contratoRepository;

    /**
	 * add 'negociacion' repository
	 */
    private final NegociacionRepository negociacionRepository;

    /**
	 * add 'estado' repository
	 */
    private final EstadoRepository estadoRepository;

    /**
	 * add 'cuenta' repository
	 */
    private final CuentaRepository cuentaRepository;

	/**
	 * Add email service
	 */
	private final EmailService emailService;

	/**
	 * Add file history service
	 */
	private final FileHistoryService fileHistoryService;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;

	/**
	 * Cache manager to use
	 */
	private final CacheManager cacheManager;

	/**
	 * @param jobBuilderFactory dependency injection of jobBuilderFactory
	 * @param stepBuilderFactory dependency injection of stepBuilderFactory
	 * @param servicioRepository dependency injection of servicioRepository
	 * @param historialPeriodoGraciaRepository dependency injection of historialPeriodoGraciaRepository
	 * @param usuarioRepository dependency injection of usuarioRepository
	 * @param tipoProductoRepository dependency injection of tipoProductoRepository
	 * @param productoRepository dependency injection of productoRepository
	 * @param clienteRepository dependency injection of clienteRepository
	 * @param contratoRepository dependency injection of contratoRepository
     * @param negociacionRepository dependency injection of negociacionRepository
     * @param cuentaRepository dependency injection of cuentaRepository
     * @param estadoRepository dependency injection of estadoRepository
	 * @param emailService dependency injection of emailService
	 * @param fileHistoryService dependency injection of fileHistoryService
	 * @param tipoDocumentoRepository dependency injection of tipoDocumentoRepository
	 * @param validationService dependency injection of validationService
	 * @param cacheManager DI of cache manager
	 */
	@Autowired
	public PeriodoGraciaBatchConfig(
			JobBuilderFactory jobBuilderFactory,
			StepBuilderFactory stepBuilderFactory,
			ServicioRepository servicioRepository,
			HistorialPeriodoGraciaRepository historialPeriodoGraciaRepository,
			UsuarioRepository usuarioRepository,
			TipoProductoRepository tipoProductoRepository,
			ProductoRepository productoRepository,
			ClienteRepository clienteRepository,
			ContratoRepository contratoRepository,
            NegociacionRepository negociacionRepository,
            EstadoRepository estadoRepository,
			EmailService emailService,
			FileHistoryService fileHistoryService,
			TipoDocumentoRepository tipoDocumentoRepository,
			ValidationService validationService,
            CuentaRepository cuentaRepository,
			CacheManager cacheManager) {
		this.jobBuilderFactory = jobBuilderFactory;
		this.stepBuilderFactory = stepBuilderFactory;
		this.servicioRepository = servicioRepository;
		this.historialPeriodoGraciaRepository = historialPeriodoGraciaRepository;
        this.usuarioRepository = usuarioRepository;
		this.tipoProductoRepository = tipoProductoRepository;
		this.productoRepository = productoRepository;
		this.clienteRepository = clienteRepository;
		this.contratoRepository = contratoRepository;
        this.negociacionRepository = negociacionRepository;
        this.estadoRepository = estadoRepository;
		this.emailService = emailService;
		this.fileHistoryService = fileHistoryService;
		this.tipoDocumentoRepository = tipoDocumentoRepository;
		this.validationService = validationService;
        this.cuentaRepository = cuentaRepository;
		this.cacheManager = cacheManager;
	}

	/**
	 * Method that configures the file reading process
     * @param file to be read
	 * @return item reader object that contains the configuration for reading the file in the batch process
     */
    @StepScope
    @Bean
    public FlatFileItemReader<PeriodoGraciaDto> periodoGraciaReader(@Value("#{jobParameters['file']}") String file) {
        DelimitedLineTokenizer tokenizer;
        DefaultLineMapper<PeriodoGraciaDto> lineMapper;
        String[] columns = new String[]{
			"CODIGO PRODUCTO",
			"NOMBRE PRODUCTO",
			"CODIGO TIPO PRODUCTO",
			"NOMBRE TIPO PRODUCTO",
			"CODIGO SUBPRODUCTO",
			"NOMBRE SUBPRODUCTO",
            "TIPO_IDENTIFICACION",
            "NRO_IDENTIFICACION",
            "NUMERO_CONTRATO",
            "NUMERO_CUENTA",
            "CICLO",
            "ID_NEGOCIACION",
            "FECHANEG",
            "FECHAAPLI",
            "ESTADO_NEGOCIACION",
            "CANTIDAD_CUOTAS_ANTERIORES",
            "DIAS_MORA",
            "FECHA_INICIO_GRACIA",
            "FECHA_FIN_GRACIA",
            "FECHA_DESISTE_GRACIA",
            "NRO_SERVICIO",
            "DESCRIPCION",
            "VALOR FACTURADO",
            "CODUSUARIO",
            "USUARIO",
            "AREA"
		};
        tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter("|");
        tokenizer.setNames(
			"productoCodigo",
			"productoNombre",
			"productoTipoCodigo",
    		"productoTipo",
			"subproductoCodigo",
    		"subproductoNombre",
    		"tipoIdentificacion",
    		"numeroIdentificacion",
            "contratoNumero",
            "cuentaNumero",
            "ciclo",
            "negociacionId",
            "fechaNegociacion",
            "fechaAplicacion",
            "negociacionEstado",
            "cantidadCuotasAnteriores",
            "diasMora",
            "fechaInicioGracia",
            "fechaFinGracia",
            "fechaDesistegracia",
            "servicioNumero",
            "descripcion",
            "valorFacturado",
            "usuarioCodigo",
            "usuarioNombre",
            "usuarioArea"
		);
        lineMapper = new DefaultLineMapper<>();
        lineMapper.setFieldSetMapper(new BeanWrapperFieldSetMapper<PeriodoGraciaDto>() {{setTargetType(PeriodoGraciaDto.class);}});
        lineMapper.setLineTokenizer(tokenizer);
        logger.info(file);
        Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));
        CustomItemReader<PeriodoGraciaDto> reader = new CustomItemReader<>(columns, "periodoGracia", resource, this.emailService, this.cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

	/**
	 * Method that configures the writing process
	 * @return item writer object that contains the configuration for writing in the database during the batch process
	 */
	@StepScope
	@Bean
	public PeriodoGraciaItemWriter periodoGraciaWriter(){
		return new PeriodoGraciaItemWriter(this.historialPeriodoGraciaRepository, this.clienteRepository, this.contratoRepository, this.servicioRepository, this.usuarioRepository,  this.cuentaRepository, this.negociacionRepository, this.validationService);
	}

	/**
	 * Method that configures the processor for the batch process
	 * @param file being read
	 * @return object of the porcessor class
	 */
	@StepScope
	@Bean
	public PeriodoGraciaItemProcessor periodoGraciaProcessor(@Value("#{jobParameters['file']}") String fileName) {
		return new PeriodoGraciaItemProcessor(
			fileName,
			this.validationService,
			this.servicioRepository,
			this.tipoDocumentoRepository,
			this.tipoProductoRepository,
			this.productoRepository,
			this.contratoRepository,
			this.usuarioRepository,
			this.clienteRepository,
            this.negociacionRepository,
            this.estadoRepository,
            this.cuentaRepository
		);
	}

	/**
	 * Method that configures the listener to be executed after the batch process
	 * @return object of the listener class
	 */
	@Bean
	public LoadJobListener periodoGraciaJobListener(){
		 return new LoadJobListener(this.emailService, this.fileHistoryService, validationService, cacheManager);
	}

	/**
	 * Method that configures the job to be executed in the batch process
	 * @param periodoGraciaJobListener is the listener to be executed after the job
	 * @param periodoGraciaJobStep1 is the first step to be execute during the job
	 * @return job object that contains the configuration for the job
	 */
	@Bean(name = "periodoGraciaJob")
	public Job periodoGraciaJob(LoadJobListener periodoGraciaJobListener, Step periodoGraciaJobStep1) {
		return jobBuilderFactory.get("periodoGraciaJob")
			.incrementer( new RunIdIncrementer() )
			.listener( periodoGraciaJobListener )
			.flow( periodoGraciaJobStep1 )
			.build()
			.build();
	}


	/**
	 * Method that configures the first step to be executed in the batch process
	 * @param periodoGraciaReader is the reader to be used in the step
	 * @param periodoGraciaWriter is the writer to be used in the step
	 * @param periodoGraciaProcessor is the processor to be used in the step
	 * @return object that contains the configuration of the first step in the job
	 */
	@Bean
	public Step periodoGraciaJobStep1(
		FlatFileItemReader<PeriodoGraciaDto> periodoGraciaReader,
		PeriodoGraciaItemWriter periodoGraciaWriter,
		PeriodoGraciaItemProcessor periodoGraciaProcessor
	) {
		return stepBuilderFactory.get( "periodoGraciaJobStep1" )
			.<PeriodoGraciaDto, HistorialPeriodoGracia> chunk( 1000 )
			.reader(periodoGraciaReader)
			.writer(periodoGraciaWriter)
			.processor(periodoGraciaProcessor)
			.faultTolerant()
			.skip(VerificationException.class)
   			.skipLimit(Integer.MAX_VALUE)
			.build();
	}
    
}
