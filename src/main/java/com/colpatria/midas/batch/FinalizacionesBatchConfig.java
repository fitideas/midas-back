package com.colpatria.midas.batch;

import com.colpatria.midas.dto.FinalizacionesDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.InfoFinalizacion;
import com.colpatria.midas.processors.FinalizacionesItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.FinalizacionesItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

/**
 * FinalizacionesBatchConfig
 */
@Configuration
public class FinalizacionesBatchConfig {

    /**
     * Add logger to deudas config
     */
    private static final Logger logger = LoggerFactory.getLogger(FinalizacionesBatchConfig.class);

    /**
     * Add File history repository
     */
    private final FileHistoryService fileHistoryService;

    /**
     * Add email service
     */
    private final EmailService emailService;

    /**
     * Cache manager to use in methods
     */
    private final CacheManager cacheManager;

    /**
     * Tipo Documento repository
     */
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * Tipo producto repository
     */
    private TipoProductoRepository tipoProductoRepository;

    /**
     * Validation service
     */
    private final ValidationService validationService;

    /**
     * add 'cliente' repository
     */
    private final ClienteRepository clienteRepository;

    /**
     * add 'contrato' repository
     */
    private final ContratoRepository contratoRepository;

    /**
     * add 'servicio' repository
     */
    private final ServicioRepository servicioRepository;

    /**
     * add 'cuenta' repository
     */
    private final CuentaRepository cuentaRepository;

    /**
     * add InfoFinalizacionRepository
     */
    private final InfoFinalizacionRepository infoFinalizacionRepository;

    /**
     * add 'usuario' repository
     */
    private final UsuarioRepository usuarioRepository;

    /**
     * Add step builder factory to config
     */
    private final StepBuilderFactory stepBuilderFactory;

    /**
     * Add job builderFactory to config
     */
    private final JobBuilderFactory jobBuilderFactory;

    /**
     * add 'producto' repository
     */
    private final ProductoRepository productoRepository;

    /**
     * @param file path of file to load
     * @return object with reader result
     */
    @StepScope
    @Bean
    public FlatFileItemReader<FinalizacionesDto> finalizacionesReader(@Value("#{jobParameters['file']}") String file) {
        DelimitedLineTokenizer tokenizer;
        DefaultLineMapper<FinalizacionesDto> lineMapper;

        String[] columns = new String[]{
                "CODIGO PRODUCTO",
                "NOMBRE PRODUCTO",
                "CODIGO TIPO PRODUCTO",
                "NOMBRE TIPO PRODUCTO",
                "CODIGO",
                "SUBPRODUCTO",
                "NUMERO_CONTRATO",
                "TIPO_IDENTIFICACION",
                "NRO_DOCTO_IDENT",
                "NRO_SERVICIO",
                "FECHACAM",
                "NUMERO_CUENTA",
                "ESTADO_ANTERIOR",
                "ESTADO_NUEVO",
                "USUARIO",
                "NOMBRE",
                "AREA",
                "OBSERVACIONES",
                "CARGO_CONCEPTO",
                "CARGO_CAPITAL",
                "CARGO INTERES CORRIENTE",
                "CARGO INTERES MORA",
                "DEUDA PENDIENTE POR FACTURAR"
        };
        tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter("|");
        tokenizer.setNames("codigoProducto",
                "nombreProducto",
                "codigoTipoProducto",
                "nombreTipoProducto",
                "codigoSubProducto",
                "nombreSubProducto",
                "nroContrato",
                "tipoIdentificacion",
                "nroIdentificacion",
                "nroServicio",
                "fechaCambio",
                "nroCuenta",
                "estadoAnterior",
                "estadoNuevo",
                "usuario",
                "nombre",
                "area",
                "observaciones",
                "cargoConcepto",
                "cargoCapital",
                "cargoInteresCorriente",
                "cargoInteresMora",
                "cargoPendienteFacturar");

        lineMapper = new DefaultLineMapper<>();
        BeanWrapperFieldSetMapper<FinalizacionesDto> wrapper = new BeanWrapperFieldSetMapper<>();
        wrapper.setTargetType(FinalizacionesDto.class);
        lineMapper.setFieldSetMapper(wrapper);
        lineMapper.setLineTokenizer(tokenizer);
        logger.info(file);
        Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));

        CustomItemReader<FinalizacionesDto> reader = new CustomItemReader<>(columns,
                "Finalizacion", resource,
                this.emailService,
                cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

    /**
     * @return return repository's to step
     */
    @StepScope
    @Bean
    public FinalizacionesItemWriter finalizacionesWriter() {
        return new FinalizacionesItemWriter(this.validationService, this.clienteRepository, this.contratoRepository, this.servicioRepository, this.cuentaRepository, this.infoFinalizacionRepository, this.usuarioRepository);
    }

    /**
     * @return return processor to steps
     */
    @StepScope
    @Bean
    public FinalizacionesItemProcessor finalizacionItemProcessor(@Value("#{jobParameters['file']}") String fileName) {
        return new FinalizacionesItemProcessor(
                this.validationService,
                this.clienteRepository,
                this.contratoRepository,
                this.servicioRepository,
                this.cuentaRepository,
                this.infoFinalizacionRepository,
                this.usuarioRepository,
                fileName,
                this.tipoDocumentoRepository,
                this.tipoProductoRepository,
                this.productoRepository);
    }

    /**
     * @return return listener to steps
     */
    @Bean
    public LoadJobListener finalizacionesListener() {
        return new LoadJobListener(this.emailService, this.fileHistoryService, this.validationService, cacheManager);
    }

    /**
     * FinalizacionesBatchConfig contructor
     * @param fileHistoryService
     * @param emailService
     * @param cacheManager
     * @param tipoDocumentoRepository
     * @param tipoProductoRepository
     * @param validationService
     * @param clienteRepository
     * @param contratoRepository
     * @param servicioRepository
     * @param cuentaRepository
     * @param infoFinalizacionRepository
     * @param usuarioRepository
     * @param stepBuilderFactory
     * @param jobBuilderFactory
     * @param productoRepository
     */
    @Autowired
    public FinalizacionesBatchConfig(FileHistoryService fileHistoryService, EmailService emailService, CacheManager cacheManager, TipoDocumentoRepository tipoDocumentoRepository, TipoProductoRepository tipoProductoRepository, ValidationService validationService, ClienteRepository clienteRepository, ContratoRepository contratoRepository, ServicioRepository servicioRepository, CuentaRepository cuentaRepository, InfoFinalizacionRepository infoFinalizacionRepository, UsuarioRepository usuarioRepository, StepBuilderFactory stepBuilderFactory, JobBuilderFactory jobBuilderFactory, ProductoRepository productoRepository) {
        this.fileHistoryService = fileHistoryService;
        this.emailService = emailService;
        this.cacheManager = cacheManager;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.tipoProductoRepository = tipoProductoRepository;
        this.validationService = validationService;
        this.clienteRepository = clienteRepository;
        this.contratoRepository = contratoRepository;
        this.servicioRepository = servicioRepository;
        this.cuentaRepository = cuentaRepository;
        this.infoFinalizacionRepository = infoFinalizacionRepository;
        this.usuarioRepository = usuarioRepository;
        this.stepBuilderFactory = stepBuilderFactory;
        this.jobBuilderFactory = jobBuilderFactory;
        this.productoRepository = productoRepository;
    }

    /**
     * @param finalizacionesReader reader of step
     * @param finalizacionesWriter DI pago writer of step
     * @param processor   processor
     * @return return step configuration
     */
    @Bean
    public Step finalizacionesJobStep1(FlatFileItemReader<FinalizacionesDto> finalizacionesReader,
                              FinalizacionesItemWriter finalizacionesWriter,
                              FinalizacionesItemProcessor processor) {
        return stepBuilderFactory.get("finalizacionesJobStep1")
                .<FinalizacionesDto, InfoFinalizacion>chunk(100)
                .reader(finalizacionesReader)
                .writer(finalizacionesWriter)
                .processor(processor)
                .faultTolerant()
                .skip(VerificationException.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }

    /**
     * @param finalizacionesListener deuda listener of job
     * @param finalizacionesJobStep1 step of job
     * @return return job with configuration
     */
    @Bean(name = "finalizacionesJob")
    public Job finalizacionesJob(LoadJobListener finalizacionesListener, Step finalizacionesJobStep1) {
        return jobBuilderFactory.get("finalizacionesJob")
                .incrementer(new RunIdIncrementer())
                .listener(finalizacionesListener)
                .flow(finalizacionesJobStep1).build()
                .build();
    }
}
