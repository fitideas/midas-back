package com.colpatria.midas.batch;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.dto.CarteraDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialCartera;
import com.colpatria.midas.processors.CarteraItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.CarteraLightItemWriter;
import com.colpatria.midas.writers.CarteraTotalItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

/*
 *
 * Clase
 *
*/

@Configuration
public class CarteraBatchConfig {

	/*
	 *
	 * Constantes
	 *
	*/

	/**
	 * String specifying the job name
	*/
	private static final String JOB_NAME = "carteraJob";

	/**
	 * String specifying the job name
	 */
	private static final String JOB_NAME_T = "carteraJobT";

	/**
	 * String specifying the file separator symbol
	*/
	private static final String SEPARATOR_SYMBOL = "|";

	/**
	 * String[] specifying the file column names
	*/
	private final String[] fileHeader = new String[]{
		"CODIGO PRODUCTO",
		"NOMBRE PRODUCTO",
		"CODIGO TIPO PRODUCTO",
		"NOMBRE TIPO PRODUCTO",
		"CODIGO SUBPRODUCTO",
		"NOMBRE SUBPRODUCTO",
		"NUMERO_CONTRATO",
		"TIPO_IDENTIFICACION",
		"NRO_IDENTIFICACION",
		"NOMBRE_CLIENTE",
		"NRO_CUENTA",
		"SUCURSAL",
		"CICLO",
		"CLASE_SERVICIO",
		"ESTRATO",
		"NRO_SERVICIO",
		"DESCRIPCION",
		"ESTADO_SERVICIO",
		"MOTIVO_FINALIZACION",
		"DIAS_ATRASO",
		"FECHA PRIMER VENCIMIENTO",
		"FECHA SEGUNDO VENCIMIENTO",
		"MORA_PRI_VENC",
		"CAPITAL_IMPAGO",
		"INTERES_IMPAGO",
		"INTERES_ORDEN_CORRIENTE_IMPAGO",
		"INTERES_MORA",
		"INTERES_ORDEN_MORA_IMPAGO",
		"INTERES_CAUSADO_NO_FACTURADO",
		"INTERES_CTE_ORDEN_CAUSADO_NO_FACT",
		"INTERES_MORA_CAUSADO_NO_FACT",
		"INTERES_MORA_ORDEN_CAUSADO_NO_FACT",
		"TOTAL_INTERES_CTE_FACT_IMPAGO",
		"TOTAL_INTERES_CTE_CAUSADO",
		"TOTAL_INTERES_MORA_FACT_IMPAGO",
		"TOTAL_INTERES_MORA_CAUSADO",
		"INTERES_FAC_NO_AFECTO",
		"INTERES_NO_AFECTO_POR_FACTURAR",
		"SALDO_DEUDA",
		"SALDO_CAPITAL_POR_FACTURAR",
		"CUOTAS_PACTADAS",
		"CUOTAS_POR_FACTURAR",
		"VALOR_CUOTA",
		"VALOR_COMPRA",
		"FECHA_CREACION",
		"FECHA_COMPRA",
		"TIPO_CONSUMO",
		"TASA",
		"COD_DISTRIBUIDOR",
		"FACTURACION",
		"ESTADO"
	};

	/*
	 *
	 * Atríbutos
	 *
	*/

	/**
     * Batch factory objects
    */
	private final JobBuilderFactory  jobBuilderFactory;
	private final StepBuilderFactory stepBuilderFactory;

	/**
	 * Cache manager to use
	 */
	private final CacheManager cacheManager;

	/**
     * Repository objects to read and write to the database
    */
	private final ServicioRepository                servicioRepository;
	private final HistorialCarteraLightRepository   historialCarteraLightRepository;
	private final HistorialCarteraTotalRepository   historialCarteraTotalRepository;
	private final ContratoRepository                contratoRepository;
	private final HistorialCarteraResumenRepository historialCarteraResumenRepository;
	private final SucursalRepository                sucursalRepository;
	private final ProductoRepository                productoRepository;
	private final CuentaRepository                  cuentaRepository;
	private final ClienteRepository                 clienteRepository;
    private final TipoConsumoRepository             tipoConsumoRepository;
    private final ClaseServicioRepository           claseServicioRepository;
	private final TipoProductoRepository            tipoProductoRepository;
	private final TipoDocumentoRepository			tipoDocumentoRepository;
	private final GastoCobranzaRepository			gastoCobranzaRepository;

	/**
     * Service state repository to read and write to the database
     */
    private final EstadoRepository estadoRepository;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;

	/**
     * Service to send emails
    */
	private final EmailService emailService;

	/**
     * Service to move the files
    */
	private final FileHistoryService fileHistoryService;

	/*
	 *
	 * Métodos
	 *
	*/

	/**
	 * Class constructor
	 * @param jobBuilderFactory is batch job builder factory
	 * @param stepBuilderFactory is batch step builder factory
	 * @param cacheManager DI of cache manager
	 * @param servicioRepository is the service repository
	 * @param historialCarteraLightRepository is the history light repository
     * @param historialCarteraTotalRepository is the history total repository
	 * @param contratoRepository is the contract repository
	 * @param emailService is the service to send emails
	 * @param historialCarteraResumenRepository is the repository of historical summary
	 * @param sucursalRepository is the branch repository
	 * @param fileHistoryService is the service to move the files
	 * @param clienteRepository is the client repository
	 * @param cuentaRepository is the account repository
	 * @param cicloFacturacionRepository is the billing cycle repository
	 * @param productoRepository is the product repository
	 * @param tipoConsumoRepository is the consumption type repository
	 * @param claseServicioRepository is the service class repository
	 * @param tipoProductoRepository is the product type repository
	 * @param tipoDocumentoRepository is the document type repository
	 * @param gastoCobranzaRepository is the gastoCobranza type repository
	 * @param attributesRepository is the attributes type repository
	 * @param validationService is the validationService
	 * @param estadoRepository is the service state repository
	 */
	@Autowired
	public CarteraBatchConfig(
			JobBuilderFactory jobBuilderFactory,
			StepBuilderFactory stepBuilderFactory,
			CacheManager cacheManager, ServicioRepository servicioRepository,
			HistorialCarteraLightRepository historialCarteraLightRepository,
            HistorialCarteraTotalRepository historialCarteraTotalRepository,
			ContratoRepository contratoRepository,
			EmailService emailService,
			HistorialCarteraResumenRepository historialCarteraResumenRepository,
			SucursalRepository sucursalRepository,
			FileHistoryService fileHistoryService,
			ClienteRepository clienteRepository,
			CuentaRepository cuentaRepository,
			ProductoRepository productoRepository,
			TipoConsumoRepository tipoConsumoRepository,
			ClaseServicioRepository claseServicioRepository,
			TipoProductoRepository tipoProductoRepository,
			TipoDocumentoRepository tipoDocumentoRepository,
			GastoCobranzaRepository gastoCobranzaRepository,
			ValidationService validationService,
			EstadoRepository estadoRepository
	) {
		this.jobBuilderFactory                 = jobBuilderFactory;
		this.stepBuilderFactory                = stepBuilderFactory;
		this.cacheManager                      = cacheManager;
		this.servicioRepository                = servicioRepository;
		this.historialCarteraLightRepository   = historialCarteraLightRepository;
        this.historialCarteraTotalRepository   = historialCarteraTotalRepository;
		this.contratoRepository                = contratoRepository;
		this.emailService                      = emailService;
		this.historialCarteraResumenRepository = historialCarteraResumenRepository;
		this.sucursalRepository                = sucursalRepository;
		this.fileHistoryService                = fileHistoryService;
		this.clienteRepository                 = clienteRepository;
		this.cuentaRepository                  = cuentaRepository;
		this.productoRepository                = productoRepository;
		this.tipoConsumoRepository             = tipoConsumoRepository;
		this.claseServicioRepository           = claseServicioRepository;
		this.tipoProductoRepository            = tipoProductoRepository;
		this.tipoDocumentoRepository		   = tipoDocumentoRepository;
		this.validationService                 = validationService;
		this.gastoCobranzaRepository		   = gastoCobranzaRepository;
		this.estadoRepository          		   = estadoRepository;
	}

	/**
	 * Method that configures the file reading process
	 * @param file to be read
	 * @return item reader object that contains the configuration for reading the file in the batch process
	*/
	@StepScope
	@Bean
	public FlatFileItemReader<CarteraDto> carteraReader( @Value( "#{jobParameters['file']}" ) String file ){
		// Variables
		DelimitedLineTokenizer                tokenizer;
		DefaultLineMapper<CarteraDto>         lineMapper;
		Resource                              resource;
		CustomItemReader<CarteraDto>          reader;
		BeanWrapperFieldSetMapper<CarteraDto> fieldSetMapper;
		// Código
		tokenizer = new DelimitedLineTokenizer();
		tokenizer.setDelimiter( SEPARATOR_SYMBOL );
		tokenizer.setNames(
			"codigoProducto",
			"nombreProducto",
			"codigoTipoProducto",
			"nombreTipoProducto",
			"codigoSubproducto",
			"nombreSubproducto",
			"numeroContrato",
			"tipoIdentificacion",
			"numeroIdentificacion",
			"nombreCliente",
			"numeroCuenta",
			"sucursal",
			"ciclo",
			"claseServicio",
			"estrato",
			"numeroServicio",
			"descripcion",
			"estadoServicio",
			"motivoFinalizacion",
			"diasAtraso",
			"fechaPrimerVencimiento",
			"fechaSegundoVencimiento",
			"moraPrimerVencimiento",
			"capitalImpago",
			"interesImpago",
			"interesOrdenCorrienteImpago",
			"interesMora",
			"interesOrdenMoraImpago",
			"interesCausadoNoFacturado",
			"interesCorrienteOrdenCausadoNoFacturado",
			"interesMoraCausadoNoFacturado",
			"interesMoraOrdenCausadoNoFacturado",
			"totalInteresCorrienteFacturadoImpago",
			"totalInteresCorrienteCausado",
			"totalInteresMoraFacturadoImpago",
			"totalInteresMoraCausado",
			"interesFacturadoNoAfecto",
			"interesNoAfectoPorFacturar",
			"saldoDeuda",
			"saldoCapitalPorFacturar",
			"cuotasPactadas",
			"cuotasPorFacturar",
			"valorCuota",
			"valorCompra",
			"fechaCreacion",
			"fechaCompra",
			"tipoConsumo",
			"tasa",
			"socioDeNegocio",
			"facturacion",
			"estado"
		);
		lineMapper     = new DefaultLineMapper<>();
		fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType( CarteraDto.class );
		lineMapper.setFieldSetMapper( fieldSetMapper );
		lineMapper.setLineTokenizer( tokenizer );
		resource = new PathResource( this.fileHistoryService.getPathByFileName( file ) );
        reader   = new CustomItemReader<>(
			fileHeader,
            "cartera",
			resource,
            this.emailService,
                cacheManager, this.fileHistoryService.getHistorialArchivosByFileName( file )
		);
        reader.setLinesToSkip( 1 );
        reader.setLineMapper( lineMapper );
		return reader;
	}

	/**
	 * Method that configures the writing process
	 * @param file to be read
	 * @return item writer object that contains the configuration for writing in the database during the batch process
	*/
	@StepScope
	@Bean
	public CarteraLightItemWriter carteraLightWriter(@Value( "#{jobParameters['file']}" ) String file ){
		// Variables
		CarteraLightItemWriter writer;
		// Código
		writer = new CarteraLightItemWriter( this.validationService, this.gastoCobranzaRepository );
		writer.setHistorialCarteraRepository( this.historialCarteraLightRepository );
		writer.setHistorialCarteraResumenRepository( this.historialCarteraResumenRepository );
		writer.setClienteRepository( this.clienteRepository );
		writer.setContratoRepository( this.contratoRepository );
		writer.setServicioRepository( this.servicioRepository );
		writer.setCuentaRepository( this.cuentaRepository );
		return writer;
	}

	/**
	 * Method that configures the writing process
	 * @param file to be read
	 * @return item writer object that contains the configuration for writing in the database during the batch process
	 */
	@StepScope
	@Bean
	public CarteraTotalItemWriter carteraTotalWriter(@Value( "#{jobParameters['file']}" ) String file ){
		// Variables
		CarteraTotalItemWriter writer;
		// Código
		writer = new CarteraTotalItemWriter( this.validationService );
		writer.setHistorialCarteraRepository( this.historialCarteraTotalRepository );
		writer.setClienteRepository( this.clienteRepository );
		writer.setContratoRepository( this.contratoRepository );
		writer.setServicioRepository( this.servicioRepository );
		writer.setCuentaRepository( this.cuentaRepository );
		return writer;
	}

	/**
	 * Method that configures the processor for the batch process
	 * @param fileName to be read
	 * @return object of the porcessor class
	*/
	@StepScope
	@Bean
	public CarteraItemProcessor carteraProcessor( @Value("#{jobParameters['file']}") String fileName ) {
		// Variables
		CarteraItemProcessor processor;
		// Code
		processor = new CarteraItemProcessor( fileName, this.validationService );
		processor.setServicioRepository( this.servicioRepository );
		processor.setContratoRepository( this.contratoRepository );
		processor.setClienteRepository( this.clienteRepository );
		processor.setSucursalRepository( this.sucursalRepository );
		processor.setCuentaRepository( this.cuentaRepository );
		processor.setProductoRepository( this.productoRepository );
		processor.setTipoConsumoRepository( this.tipoConsumoRepository );
		processor.setClaseServicioRepository( this.claseServicioRepository );
		processor.setTipoProductoRepository( this.tipoProductoRepository );
		processor.setTipoDocumentoRepository( this.tipoDocumentoRepository );
		processor.setEstadoRepository( this.estadoRepository );
		return processor;
	}

	/**
	 * Method that configures the listener to be executed after the batch process
	 * @return object of the listener class
	*/
	@Bean
	public LoadJobListener carteraJobListener(){
		return new LoadJobListener( this.emailService, this.fileHistoryService, this.validationService, cacheManager);
	}

	/**
	 * Method that configures the job to be executed in the batch process
	 * @param carteraJobListener is the listener to be executed after the job
	 * @param carteraJobStep1 is the first step to be execute during the job
	 * @return a job object that contains the configuration for the job execution
	*/
	@Bean( name = JOB_NAME )
	public Job carteraJob( LoadJobListener carteraJobListener, Step carteraJobStep1 ) {
		return jobBuilderFactory.get( JOB_NAME )
			.incrementer( new RunIdIncrementer() )
			.listener( carteraJobListener )
			.flow( carteraJobStep1 ).build().build();
	}


	/**
	 * Method that configures the job to be executed in the batch process
	 * @param carteraJobListener is the listener to be executed after the job
	 * @param carteraJobStep2 is the first step to be execute during the job
	 * @return a job object that contains the configuration for the job execution
	 */
	@Bean( name = JOB_NAME_T )
	public Job carteraJobT( LoadJobListener carteraJobListener, Step carteraJobStep2 ) {
		return jobBuilderFactory.get( JOB_NAME_T )
				.incrementer( new RunIdIncrementer() )
				.listener( carteraJobListener )
				.flow( carteraJobStep2 ).build().build();
	}

	/**
	 * Method that configures the first step to be executed in the batch process
	 * @param carteraReader is the reader to be used in the step
	 * @param carteraLightWriter is the writer to be used in the step
	 * @param carteraProcessor is the processor to be used in the step
	 * @return object that contains the configuration of the first step in the job
	*/
	@Bean
	public Step carteraJobStep1(
		FlatFileItemReader<CarteraDto> carteraReader,
		CarteraLightItemWriter carteraLightWriter,
		CarteraItemProcessor           carteraProcessor
	) {
		return stepBuilderFactory.get( "carteraJobStep1" )
			.<CarteraDto, HistorialCartera> chunk( 5000 )
			.reader( carteraReader )
			.writer( carteraLightWriter )
			.processor( carteraProcessor )
			.faultTolerant()
			.skip( VerificationException.class )
			.skipLimit( Integer.MAX_VALUE )
			.build();
	}

	/**
	 * Method that configures the first step to be executed in the batch process
	 * @param carteraReader is the reader to be used in the step
	 * @param carteraTotalWriter is the writer to be used in the step
	 * @param carteraProcessor is the processor to be used in the step
	 * @return object that contains the configuration of the first step in the job
	 */
	@Bean
	public Step carteraJobStep2(
			FlatFileItemReader<CarteraDto> carteraReader,
			CarteraTotalItemWriter carteraTotalWriter,
			CarteraItemProcessor           carteraProcessor
	) {
		return stepBuilderFactory.get( "carteraJobStep2" )
				.<CarteraDto, HistorialCartera> chunk( 5000 )
				.reader( carteraReader )
				.writer( carteraTotalWriter )
				.processor( carteraProcessor )
				.faultTolerant()
				.skip( VerificationException.class )
				.skipLimit( Integer.MAX_VALUE )
				.build();
	}

}