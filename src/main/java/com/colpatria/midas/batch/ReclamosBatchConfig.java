package com.colpatria.midas.batch;

import com.colpatria.midas.services.JobSchedulerService;
import com.colpatria.midas.tasklets.ReclamosTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Reclamos Batch Config
 */
@Configuration
public class ReclamosBatchConfig {
    /**
     * Job Builder Factory Bean reference
     */
    private final JobBuilderFactory jobBuilderFactory;

    /**
     * Step Builder Factory Bean reference
     */
    private final StepBuilderFactory stepBuilderFactory;

    /**
     * Job Launcher Bean reference
     */
    private final JobLauncher jobLauncher;

    /**
     * Job Scheduler service Bean reference
     */
    private final JobSchedulerService jobSchedulerService;


    /**
     * Constructor. Autowire beans to attributes
     *
     * @param jobBuilderFactory   Job Builder Factory Bean
     * @param stepBuilderFactory  Step Builder Factory Bean
     * @param jobLauncher         Job Launcher Bean
     * @param jobSchedulerService Job Scheduler Service Bean
     */
    @Autowired
    public ReclamosBatchConfig(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,
                               JobLauncher jobLauncher, JobSchedulerService jobSchedulerService) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.jobLauncher = jobLauncher;
        this.jobSchedulerService = jobSchedulerService;
    }

    /**
     * ReclamosTasklet Bean
     *
     * @return Instance of a ReclamosTasklet Class
     */
    @Bean
    public ReclamosTasklet reclamosTasklet() {
        return new ReclamosTasklet();
    }

    /**
     * ReclamosJobStep Bean
     *
     * @param reclamosTasklet ReclamosJobStep Bean reference
     * @return Reclamos Job Step
     */
    @Bean
    public Step reclamosJobStep(ReclamosTasklet reclamosTasklet) {
        return this.stepBuilderFactory.get("reclamosJobStep").tasklet(reclamosTasklet).build();
    }

    /**
     * ReclamosJob Bean
     *
     * @param reclamosJobStep ReclamosJobStep Bean reference
     * @return Reclamos Job
     */
    @Bean(name = "reclamosJob")
    public Job reclamosJob(Step reclamosJobStep) {
        return this.jobBuilderFactory.get("reclamosJob").incrementer(new RunIdIncrementer()).flow(reclamosJobStep).build().build();
    }
}
