package com.colpatria.midas.batch;

/*
 *
 * Librerías
 *
*/

import com.colpatria.midas.dto.MovimientoSaldoDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialMovimientoSaldo;
import com.colpatria.midas.processors.MovimientoSaldoItemProcessor;
import com.colpatria.midas.readers.CustomItemReader;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.MovimientoSaldoItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

/**
 * Configuration class for 'movimiento saldo' batch process
*/
@Configuration
public class MovimientoSaldoBatchConfig {

	/**
	 * String the specifies the job name
	*/
	private static final String JOB_NAME = "movimientoSaldoJob";

	/**
	 * String the specifies the file separator symbol
	*/
	private static final String SEPARATOR_SYMBOL = "|";

	/**
	 * String[] specifying the file column names
	*/
	private final String[] fileHeader = new String[]{
		"CODIGO PRODUCTO",
		"NOMBRE PRODUCTO",
		"CODIGO TIPO PRODUCTO",
		"NOMBRE TIPO PRODUCTO",
		"CODIGO SUBPRODUCTO",
		"NOMBRE SUBPRODUCTO",
		"TIPO_IDENTIFICACION",
		"NRO_DOCUMENTO",
		"NUMERO_CONTRATO",
		"VALOR",
		"ESTADO",
		"NUMERO_DE_TRANSACCION",
		"TIPO_MOVIMIENTO",
		"CONCEPTO_CONTABLE",
		"NUMERO_DE_COMPRA",
		"FECHA_REALIZACION_DEL_AJUSTE",
		"FECHA_APROBACION_DEL_AJUSTE",
		"USUARIO_CREADOR",
		"USUARIO_APROBADOR",
		"OBSERVACIONES"
	};

	/**
     * Batch factory job object
    */
	private final JobBuilderFactory  jobBuilderFactory;

	/**
     * Batch factory step object
    */
	private final StepBuilderFactory stepBuilderFactory;

	/**
     * History repository to read and write to the database
    */
	private final HistorialMovimientoSaldoRepository historialMovimientoSaldoRepository;

	/**
     * Contract repository to read and write to the database
    */
	private final ContratoRepository contratoRepository;

	/**
     * User repository to read and write to the database
    */
	private final UsuarioRepository usuarioRepository;

	/**
     * Position repository to read and write to the database
    */
	private final CargoRepository cargoRepository;

	/**
     * Product repository to read and write to the database
    */
	private final ProductoRepository productoRepository;

	/**
     * Product type repository to read and write to the database
    */
	private final TipoProductoRepository tipoProductoRepository;

	/**
     * Client repository to read and write to the database
    */
	private final ClienteRepository clienteRepository;

	/**
     * Document type repository to read and write to the database
    */
	private final TipoDocumentoRepository tipoDocumentoRepository;

	/**
     * Negociation repository to read and write to the database
    */
	private final NegociacionRepository negociacionRepository;

	/**
     * Service to send emails
    */
	private final EmailService emailService;

	/**
     * Service to move the files
    */
	private final FileHistoryService fileHistoryService;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;

	/**
     * Service state repository to read and write to the database
     */
    private final EstadoRepository estadoRepository;

	/**
	 * Cache manager to use
	 */
	private final CacheManager cacheManager;

	/*
	 *
	 * Métodos
	 *
	*/

	/**
	 * Class constructor
	 * @param jobBuilderFactory is batch job builder factory
	 * @param stepBuilderFactory is batch step builder factory
	 * @param historialCasoMovimientoSaldoRepository is the history repository
	 * @param contratoRepository is the contract repository
	 * @param emailService is the service to send emails
	 * @param fileHistoryService is the service to move the files
	 * @param usuarioRepository is the user repository
	 * @param cargoRepository is the position repository
	 * @param productoRepository is the product repository
	 * @param tipoProductoRepository is the product type repository
	 * @param clienteRepository is the client repository
	 * @param tipoDocumentoRepository is the document type repository
	 * @param negociacionRepository is the negotiation repository
	 * @param validationService is the validationService
	 * @param cacheManager DI of cache manager
	 * @param estadoRepository is the state repository
	 */
	@Autowired
	public MovimientoSaldoBatchConfig(
			JobBuilderFactory jobBuilderFactory,
			StepBuilderFactory stepBuilderFactory,
			HistorialMovimientoSaldoRepository historialCasoMovimientoSaldoRepository,
			ContratoRepository contratoRepository,
			EmailService emailService,
			FileHistoryService fileHistoryService,
			UsuarioRepository usuarioRepository,
			CargoRepository cargoRepository,
			ProductoRepository productoRepository,
			TipoProductoRepository tipoProductoRepository,
			ClienteRepository clienteRepository,
			TipoDocumentoRepository tipoDocumentoRepository,
			NegociacionRepository negociacionRepository,
			ValidationService validationService,
			CacheManager cacheManager,
    		EstadoRepository estadoRepository
	) {
		this.jobBuilderFactory 					= jobBuilderFactory;
		this.stepBuilderFactory 				= stepBuilderFactory;
		this.historialMovimientoSaldoRepository = historialCasoMovimientoSaldoRepository;
		this.contratoRepository 				= contratoRepository;
		this.emailService 						= emailService;
		this.fileHistoryService 				= fileHistoryService;
		this.usuarioRepository 					= usuarioRepository;
		this.cargoRepository 					= cargoRepository;
		this.productoRepository                 = productoRepository;
		this.clienteRepository                  = clienteRepository;
		this.tipoProductoRepository             = tipoProductoRepository;
		this.tipoDocumentoRepository		    = tipoDocumentoRepository;
		this.negociacionRepository              = negociacionRepository;
		this.validationService                  = validationService;
		this.cacheManager 						= cacheManager;
		this.estadoRepository                   = estadoRepository;
	}

	/**
	 * Method that configures the file reading process
	 * @param file to be read
	 * @return item reader object that contains the configuration for reading the file in the batch process
	*/
	@StepScope
	@Bean
	public FlatFileItemReader<MovimientoSaldoDto> movimientoSaldoReader( @Value( "#{jobParameters['file']}" ) String file ){
		// Variables
		DelimitedLineTokenizer              		  tokenizer;
		DefaultLineMapper<MovimientoSaldoDto>         lineMapper;
		Resource                                      resource;
		CustomItemReader<MovimientoSaldoDto>          reader;
		BeanWrapperFieldSetMapper<MovimientoSaldoDto> fieldSetMapper;
		// Código
		tokenizer = new DelimitedLineTokenizer();
		tokenizer.setDelimiter( SEPARATOR_SYMBOL );
		tokenizer.setNames(
			"codigoProducto",
			"nombreProducto",
			"codigoTipoProducto",
			"nombreTipoProducto",
			"codigoSubproducto",
			"nombreSubproducto",
			"tipoIdentificacion",
			"numeroIdentificacion",
			"numeroContrato",
			"valor",
			"estado",
			"numeroTransaccion",
			"tipoMovimiento",
			"codigoCargo",
			"numeroCompra",
			"fechaRealizacionAjuste",
			"fechaAprobacionAjuste",
			"codigoUsuarioCreador",
			"codigoUsuarioAprobador",
			"observacion"
		);
		lineMapper     = new DefaultLineMapper<>();
		fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType( MovimientoSaldoDto.class );
		lineMapper.setFieldSetMapper( fieldSetMapper );
		lineMapper.setLineTokenizer( tokenizer );
		resource = new PathResource( this.fileHistoryService.getPathByFileName( file ) );
        reader   = new CustomItemReader<>(
			fileHeader,
            "movimiento saldo",
			resource,
            this.emailService,
                cacheManager, this.fileHistoryService.getHistorialArchivosByFileName( file )
		);
        reader.setLinesToSkip( 1 );
        reader.setLineMapper( lineMapper );		
		return reader;
	}

	/**
	 * Method that configures the writing process
	 * @return item writer object that contains the configuration for writing in the database during the batch process
	*/
	@Bean
	public MovimientoSaldoItemWriter movimientoSaldoWriter(){
		// Variables
		MovimientoSaldoItemWriter writer;
		// Código
		writer = new MovimientoSaldoItemWriter( this.validationService );
		writer.setHistorialMovimientoSaldoRepository( this.historialMovimientoSaldoRepository );
		writer.setContratoRepository( this.contratoRepository );
		writer.setUsuarioRepository( this.usuarioRepository );	
		writer.setClienteRepository( this.clienteRepository );
		writer.setNegociacionRepository( this.negociacionRepository );
		return writer;
	}

	/**
	 * Method that configures the processor for the batch process
	 * @param fileName to be read
	 * @return object of the porcessor class
	*/
	@StepScope
	@Bean
	public MovimientoSaldoItemProcessor movimientoSaldoProcessor( @Value("#{jobParameters['file']}") String fileName ) {
		// Variables
		MovimientoSaldoItemProcessor processor;
		// Code
		processor = new MovimientoSaldoItemProcessor( fileName, this.validationService );
		processor.setContratoRepository( this.contratoRepository );
		processor.setUsuarioRepository( this.usuarioRepository );
		processor.setCargoRepository( this.cargoRepository );		
		processor.setClienteRepository( this.clienteRepository );
		processor.setProductoRepository( this.productoRepository );
		processor.setTipoProductoRepository( this.tipoProductoRepository );
		processor.setTipoDocumentoRepository( this.tipoDocumentoRepository );
		processor.setNegociacionRepository( this.negociacionRepository );
		processor.setEstadoRepository( this.estadoRepository );
		return processor;
	}

	/**
	 * Method that configures the listener to be executed after the batch process
	 * @return object of the listener class
	*/
	@Bean
	public LoadJobListener movimientoSaldoJobListener(){
		return new LoadJobListener( this.emailService, this.fileHistoryService, this.validationService, cacheManager);
	}

	/**
	 * Method that configures the job to be executed in the batch process
	 * @param movimientoSaldoJobListener is the listener to be executed after the job
	 * @param movimientoSaldoJobStep1 is the first step to be execute during the job
	 * @return a job object that contains the configuration for the job execution
	*/
	@Bean( name = JOB_NAME )
	public Job movimientoSaldoJob( LoadJobListener movimientoSaldoJobListener, Step movimientoSaldoJobStep1 ) {
		return jobBuilderFactory.get( JOB_NAME )
			.incrementer( new RunIdIncrementer() )
			.listener( movimientoSaldoJobListener )
			.flow( movimientoSaldoJobStep1 ).build().build();
	}

	/**
	 * Method that configures the first step to be executed in the batch process
	 * @param movimientoSaldoreader is the reader to be used in the step
	 * @param movimientoSaldoWriter is the writer to be used in the step
	 * @param movimientoSaldoProcessor is the processor to be used in the step
	 * @return object that contains the configuration of the first step in the job
	*/
	@Bean
	public Step movimientoSaldoJobStep1(
		FlatFileItemReader<MovimientoSaldoDto> movimientoSaldoreader,
		MovimientoSaldoItemWriter              movimientoSaldoWriter,
		MovimientoSaldoItemProcessor           movimientoSaldoProcessor
	) {
		return stepBuilderFactory.get( "movimientoSaldoJobStep1" )
			.<MovimientoSaldoDto, HistorialMovimientoSaldo> chunk( 100 )
			.reader( movimientoSaldoreader )
			.writer( movimientoSaldoWriter )
			.processor( movimientoSaldoProcessor )
			.faultTolerant()
			.skip( VerificationException.class )
			.skipLimit( Integer.MAX_VALUE )
			.build();
	}

}