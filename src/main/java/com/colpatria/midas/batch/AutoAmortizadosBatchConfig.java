package com.colpatria.midas.batch;

import com.colpatria.midas.dto.AutoAmortizadoDto;
import com.colpatria.midas.exceptions.VerificationException;
import com.colpatria.midas.listeners.LoadJobListener;
import com.colpatria.midas.model.HistorialAutoAmortizado;
import com.colpatria.midas.processors.AutoAmortizadoItemProcessor;
import com.colpatria.midas.repositories.*;
import com.colpatria.midas.services.EmailService;
import com.colpatria.midas.services.FileHistoryService;
import com.colpatria.midas.services.ValidationService;
import com.colpatria.midas.writers.AutoAmortizadoItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import com.colpatria.midas.readers.CustomItemReader;
import org.springframework.core.io.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Configuration class for 'autoAmortizados' batch process
 */
@Configuration
public class AutoAmortizadosBatchConfig {

	/**
     * Add logger to config
     */
    private static final Logger logger = LoggerFactory.getLogger(AutoAmortizadosBatchConfig.class);

	/**
	 * Add job builderFactory to config
	 */
    public final JobBuilderFactory	jobBuilderFactory;

	/**
	 * Add step builder factory to config
	 */
	public final StepBuilderFactory stepBuilderFactory;

	/**
	 * add 'servicio' repository
	 */
	private final ServicioRepository servicioRepository;

	/**
	 * add 'historial auto amortizacion' repository
	 */
	private final HistorialAutoAmortizadoRepository historialAutoAmortizadoRepository;

	/**
	 * add 'cargo' repository
	 */
    private final CargoRepository cargoRepository;

	/**
	 * add 'Tipo Documento' repository
	 */
	private final TipoDocumentoRepository tipoDocumentoRepository;
	
	/*
	 * add 'tipoProducto' repository
	 */
    private final TipoProductoRepository tipoProductoRepository;

	/**
	 * add 'producto' repository
	 */
    private final ProductoRepository productoRepository;

	/**
	 * add 'cliente' repository
	 */
	private final ClienteRepository clienteRepository;

	/**
	 * add 'contrato' repository
	 */
	private final ContratoRepository contratoRepository;

	/**
	 * Add email service
	 */
	private final EmailService emailService;

	/**
	 * Add file history service
	 */
	private final FileHistoryService fileHistoryService;

	/**
	 * Validation service
	 */
	private final ValidationService validationService;

	/**
	 * Cache manager to use
	 */
	private final CacheManager cacheManager;

	/**
	 * @param jobBuilderFactory dependency injection of jobBuilderFactory
	 * @param stepBuilderFactory dependency injection of stepBuilderFactory
	 * @param servicioRepository dependency injection of servicioRepository
	 * @param historialAutoAmortizadoRepository dependency injection of historialAutoAmortizadoRepository
	 * @param cargoRepository dependency injection of cargoRepository
	 * @param tipoProductoRepository dependency injection of tipoProductoRepository
	 * @param productoRepository dependency injection of productoRepository
	 * @param clienteRepository dependency injection of clienteRepository
	 * @param contratoRepository dependency injection of contratoRepository
	 * @param emailService dependency injection of emailService
	 * @param fileHistoryService dependency injection of fileHistoryService
	 * @param tipoDocumentoRepository dependency injection of tipoDocumentoRepository
	 * @param validationService dependency injection of validationService
	 * @param cacheManager DI of cache manager
	 */
	@Autowired
	public AutoAmortizadosBatchConfig(
			JobBuilderFactory jobBuilderFactory,
			StepBuilderFactory stepBuilderFactory,
			ServicioRepository servicioRepository,
			HistorialAutoAmortizadoRepository historialAutoAmortizadoRepository,
			CargoRepository cargoRepository,
			TipoProductoRepository tipoProductoRepository,
			ProductoRepository productoRepository,
			ClienteRepository clienteRepository,
			ContratoRepository contratoRepository,
			EmailService emailService,
			FileHistoryService fileHistoryService,
			TipoDocumentoRepository tipoDocumentoRepository,
			ValidationService validationService,
			CacheManager cacheManager) {
		this.jobBuilderFactory = jobBuilderFactory;
		this.stepBuilderFactory = stepBuilderFactory;
		this.servicioRepository = servicioRepository;
		this.historialAutoAmortizadoRepository = historialAutoAmortizadoRepository;
        this.cargoRepository = cargoRepository;
		this.tipoProductoRepository = tipoProductoRepository;
		this.productoRepository = productoRepository;
		this.clienteRepository = clienteRepository;
		this.contratoRepository = contratoRepository;
		this.emailService = emailService;
		this.fileHistoryService = fileHistoryService;
		this.tipoDocumentoRepository = tipoDocumentoRepository;
		this.validationService = validationService;
		this.cacheManager = cacheManager;
	}

	/**
	 * Method that configures the file reading process
     * @param file to be read
	 * @return item reader object that contains the configuration for reading the file in the batch process
     */
    @StepScope
    @Bean
    public FlatFileItemReader<AutoAmortizadoDto> autoAmortizadoReader(@Value("#{jobParameters['file']}") String file) {
        DelimitedLineTokenizer tokenizer;
        DefaultLineMapper<AutoAmortizadoDto> lineMapper;
        String[] columns = new String[]{
			"CODIGO PRODUCTO",
			"NOMBRE PRODUCTO",
			"CODIGO TIPO PRODUCTO",
			"NOMBRE TIPO PRODUCTO",
			"CODIGO SUBPRODUCTO",
			"NOMBRE SUBPRODUCTO",
			"NUMERO_CONTRATO",
			"TIPO_IDENTIFICACION",
			"NRO_IDENTIFICACION",
			"NOMBRE_CLIENTE",
			"NRO_SERVICIO",
			"NUM_DOC_PAGADO",
			"FECHA_INGRESO",
			"FECHA_PAGO",
			"FECHA_VENCIMIENTO",
			"FECHA_POSTEO",
			"FECHA_EFECTIVA",
			"CODIGO_CARGO",
			"NOMBRE_CARGO",
			"MONTO",
			"MONTO_PARTICIPACION"
		};
        tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter("|");
        tokenizer.setNames(
			"productoCodigo",
			"productoNombre",
			"productoTipoCodigo",
    		"productoTipo",
			"subproductoCodigo",
    		"subproductoNombre",
    		"contratoNumero",
    		"tipoIdentificacion",
    		"numeroIdentificacion",
            "clienteNombre",
            "servicioNumero",
            "numeroDocumentoPagado",
            "fechaIngreso",
            "fechaPago",
            "fechaVencimiento",
            "fechaPosteo",
            "fechaEfectiva",
            "cargoCodigo",
            "cargoNombre",
            "monto",
            "montoParticipacion"
		);
        lineMapper = new DefaultLineMapper<>();
        lineMapper.setFieldSetMapper(new BeanWrapperFieldSetMapper<AutoAmortizadoDto>() {{setTargetType(AutoAmortizadoDto.class);}});
        lineMapper.setLineTokenizer(tokenizer);
        logger.info(file);
        Resource resource = new PathResource(this.fileHistoryService.getPathByFileName(file));
        CustomItemReader<AutoAmortizadoDto> reader = new CustomItemReader<>(columns, "autoAmortizado", resource, this.emailService, this.cacheManager, this.fileHistoryService.getHistorialArchivosByFileName(file));
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

	/**
	 * Method that configures the writing process
	 * @return item writer object that contains the configuration for writing in the database during the batch process
	 */
	@StepScope
	@Bean
	public AutoAmortizadoItemWriter autoAmortizadoWriter(){
		return new AutoAmortizadoItemWriter(this.historialAutoAmortizadoRepository, this.clienteRepository, this.contratoRepository, this.servicioRepository, this.validationService);
	}

	/**
	 * Method that configures the processor for the batch process
	 * @param file being read
	 * @return object of the porcessor class
	 */
	@StepScope
	@Bean
	public AutoAmortizadoItemProcessor autoAmortizadoProcessor(@Value("#{jobParameters['file']}") String fileName) {
		return new AutoAmortizadoItemProcessor(
			fileName,
			this.validationService,
			this.servicioRepository,
			this.tipoDocumentoRepository,
			this.tipoProductoRepository,
			this.productoRepository,
			this.contratoRepository,
			this.cargoRepository,
			this.clienteRepository
		);
	}

	/**
	 * Method that configures the listener to be executed after the batch process
	 * @return object of the listener class
	 */
	@Bean
	public LoadJobListener autoAmortizadoJobListener(){
		 return new LoadJobListener(this.emailService, this.fileHistoryService, validationService, cacheManager);
	}

	/**
	 * Method that configures the job to be executed in the batch process
	 * @param autoAmortizadoJobListener is the listener to be executed after the job
	 * @param autoAmortizadoJobStep1 is the first step to be execute during the job
	 * @return job object that contains the configuration for the job
	 */
	@Bean(name = "autoAmortizadoJob")
	public Job autoAmortizadoJob(LoadJobListener autoAmortizadoJobListener, Step autoAmortizadoJobStep1) {
		return jobBuilderFactory.get("autoAmortizadoJob")
			.incrementer( new RunIdIncrementer() )
			.listener( autoAmortizadoJobListener )
			.flow( autoAmortizadoJobStep1 )
			.build()
			.build();
	}


	/**
	 * Method that configures the first step to be executed in the batch process
	 * @param autoAmortizadoReader is the reader to be used in the step
	 * @param autoAmortizadoWriter is the writer to be used in the step
	 * @param autoAmortizadoProcessor is the processor to be used in the step
	 * @return object that contains the configuration of the first step in the job
	 */
	@Bean
	public Step autoAmortizadoJobStep1(
		FlatFileItemReader<AutoAmortizadoDto> autoAmortizadoReader,
		AutoAmortizadoItemWriter autoAmortizadoWriter,
		AutoAmortizadoItemProcessor autoAmortizadoProcessor
	) {
		return stepBuilderFactory.get( "autoAmortizadoJobStep1" )
			.<AutoAmortizadoDto, HistorialAutoAmortizado> chunk( 1000 )
			.reader(autoAmortizadoReader)
			.writer(autoAmortizadoWriter)
			.processor(autoAmortizadoProcessor)
			.faultTolerant()
			.skip(VerificationException.class)
   			.skipLimit(Integer.MAX_VALUE)
			.build();
	}
    
}
