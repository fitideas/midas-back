package com.colpatria.midas.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Date Utilities
 */
public class DateUtils {

    /**
     * Private Constructor to simulate static class
     */
    private DateUtils() {
    }

    /**
     * Given a String containing a date, returns the corresponding LocalDate
     *
     * @param dateString String that contains the date
     * @return LocalDate reference to the date given in the string
     */
    public static LocalDate stringToLocalDate(String dateString) {
        if(dateString == null)
            return null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(dateString, formatter);
    }

    /**
     * Method that validates a String containing a date
     *
     * @param dateString String that contains the date
     * @return flag indicating if the date is valid
     */
    public static boolean isValidDate( String dateString ) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern( "yyyy-MM-dd" );
        try {
            LocalDate.parse( dateString, formatter );
            return true;
        } catch( Exception e ) {
            return false;
        }
    }

    /**
     * Method that gets the date now
     * @return LocalDate reference to the date now
     */
    public static LocalDateTime getDatetimeNow() {
        try {
            return LocalDateTime.now();
        } catch( Exception e ) {
            return null;
        }
    }

    /**
     * @param date Date converted to localdate
     * @return localdate
     */
    public static LocalDate ConvertDateToLocalDate(Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate;
    }

}
