package com.colpatria.midas.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * File Utilities
 */
public class FileUtils {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    /**
     * Private Constructor. To simulate Static Class
     */
    private FileUtils() {
    }

    /**
     * Move File from an initial path to a final path
     *
     * @param initialPath Initial Path
     * @param finalPath   Final Path
     */
    public static void moveFile(String initialPath, String finalPath) {
        File initialFile = new File(initialPath);
        File finalFile = new File(finalPath);
        if (!initialFile.renameTo(finalFile)) {
            logger.error("Error moving a file");
        }
    }

    /**
     * Remove a file given a path
     *
     * @param path Path of the file to remove
     */
    public static void removeFile(String path) {
        try {
            Files.delete(Paths.get(path));
        } catch (IOException exception) {
            logger.error("Error removing a file");
        }
    }

    /**
     * Returns a Set of files in a given directory
     * @param directoryPath path of the directory
     * @return a Set of file names
     */
    public static Set<String> listFilesInDirectory(String directoryPath) {
        return Stream.of(Objects.requireNonNull(new File(directoryPath).listFiles())).filter(file -> !file.isDirectory()).map(File::getName).collect(Collectors.toSet());
    }

    public static String obtenerNombre(String rutaArchivo) {
        // Variables
        String nombreArchivo;
        String[] ruta;
        // Código
        ruta = rutaArchivo.split("/");
        nombreArchivo = ruta[ruta.length - 1];
        return nombreArchivo;
    }

    public static LocalDate extraerFecha(String nombreArchivo) {
        // Variables
        DateTimeFormatter formatter;
        String dateString;
        Matcher buscadorNumeros;
        // Código
        dateString = "";
        buscadorNumeros = Pattern.compile("\\d+").matcher(nombreArchivo);
        while (buscadorNumeros.find()) {
            dateString += buscadorNumeros.group();
        }
        formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        return LocalDate.parse(dateString, formatter);
    }

    public static LocalDateTime extractDateFromFilename(String nombreArchivo, String pattern) {
        // Variables
        DateTimeFormatter formatter;
        String dateString;
        Matcher buscadorNumeros;
        // Código
        dateString = "";
        buscadorNumeros = Pattern.compile("\\d+").matcher(nombreArchivo);
        while (buscadorNumeros.find()) {
            dateString += buscadorNumeros.group();
        }
        formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDateTime.parse(dateString, formatter);
    }

}
