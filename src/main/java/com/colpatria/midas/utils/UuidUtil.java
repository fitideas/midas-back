package com.colpatria.midas.utils;

/*
 *
 * Libraries
 *
*/

import java.util.UUID;

/**
 * Uuid Utilities
*/
public class UuidUtil {

    /**
     * Method that generates a random string
     * @return String reference to a random text
    */
    public static String generate() {
        // Variables
        String result;
        // Code
        result = UUID.randomUUID().toString();
        result = result.replace( "-", "" );
        result = result.substring( 0, 16 );
        return result;
    }
    
}
