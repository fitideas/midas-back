package com.colpatria.midas.utils;

/**
 * Utility class that contains the functionality to verify if a field in a file is null or empty and generate a report.
 */
public class VerificationUtils {

    /**
     * Constants field.
     * Saves process name for 'cambio estado job'
     */
    public static final String CAMBIO_ESTADO = "cambioEstado";

    /**
     * Constants field.
     * Saves process name for 'cartera job'
     */
    public static final String CARTERA = "cartera";

    /**
     * Constants field.
     * Saves process name for 'auto amortizado job'
     */
    public static final String AUTO_AMORTIZADO = "autoAmortizado";

    /**
     * Constants field.
     * Saves process name for 'recaudo job'
     */
    public static final String RECAUDO = "recaudo";

    /**
     * Constants field.
     * Saves process name for 'refinanciacion job'
     */
    public static final String REFINANCIACION = "refinanciacion";

    /**
     * Constants field.
     * Saves process name for 'anulacion pago job'
     */
    public static final String ANULACION_PAGO = "anulacionPago";

    /**
     * Constants field.
     * Saves process name for 'cargue utilizacion job'
     */
    public static final String CARGUE_UTILIZACION = "cargueUtilizacion";

    /**
     * Static field.
     * Counters that allows you to know the line of the file that is being read in the 'cambio estado' batch process.
     */
    private static Long cambioEstadoRegisterCounter = 1L;

    /**
     * Static field.
     * Counters that allows you to know the line of the file that is being read in the 'cartera' batch process.
     */
    private static Long carteraRegisterCounter = 1L;

    /**
     * Static field.
     * Counters that allows you to know the line of the file that is being read in the 'auto amortizado' batch process.
     */
    private static Long autoAmortizadoRegisterCounter = 1L;

    /**
     * Static field.
     * Counters that allows you to know the line of the file that is being read in the 'recaudo' batch process.
     */
    private static Long recaudoRegisterCounter = 1L;

    /**
     * Static field.
     * Counters that allows you to know the line of the file that is being read in the 'refinanciacion' batch process.
     */
    private static Long refinanciacionRegisterCounter = 1L;

    /**
     * Static field.
     * Counters that allows you to know the line of the file that is being read in the 'anulacion pago' batch process.
     */
    private static Long anulacionPagoRegisterCounter = 1L;

    /**
     * Static field.
     * Counters that allows you to know the line of the file that is being read in the 'cargue utilizacion' batch process.
     */
    private static Long cargueUtilizacionRegisterCounter = 1L;

    /**
     * Static field.
     * Strings that saves the report of errors found in a file for 'cambio estado' batch process.
     */
    private static StringBuilder cambioEstadoRegisterReport = new StringBuilder();

    /**
     * Static field.
     * Strings that saves the report of errors found in a file for 'cartera' batch process.
     */
    private static StringBuilder carteraRegisterReport = new StringBuilder();

    /**
     * Static field.
     * Strings that saves the report of errors found in a file for 'auto amortizado' batch process.
     */
    private static StringBuilder autoAmortizadoRegisterReport = new StringBuilder();

    /**
     * Static field.
     * Strings that saves the report of errors found in a file for 'recaudo' batch process.
     */
    private static StringBuilder recaudoRegisterReport = new StringBuilder();

    /**
     * Static field.
     * Strings that saves the report of errors found in a file for 'refinanciacion' batch process.
     */
    private static StringBuilder refinanciacionRegisterReport = new StringBuilder();

    /**
     * Static field.
     * Strings that saves the report of errors found in a file for 'anulacion pago' batch process.
     */
    private static StringBuilder anulacionPagoRegisterReport = new StringBuilder();

    /**
     * Static field.
     * Strings that saves the report of errors found in a file for 'cargue utilizacion' batch process.
     */
    private static StringBuilder cargueUtilizacionRegisterReport = new StringBuilder();

    /**
     * Static fields.
     * Saves the state of error of the files being read in 'cambio estado' batch process.
     */
    private static boolean cambioEstadoHasError = false;

    /**
     * Static fields.
     * Saves the state of error of the files being read in 'cartera' batch process.
     */
    private static boolean carteraHasError = false;

    /**
     * Static fields.
     * Saves the state of error of the files being read in 'auto amortizado' batch process.
     */
    private static boolean autoAmortizadoHasError = false;

    /**
     * Static fields.
     * Saves the state of error of the files being read in 'recaudo' batch process.
     */
    private static boolean recaudoHasError = false;

    /**
     * Static fields.
     * Saves the state of error of the files being read in 'refinanciacion' batch process.
     */
    private static boolean refinanciacionHasError = false;

    /**
     * Static fields.
     * Saves the state of error of the files being read in 'anulacion pago' batch process.
     */
    private static boolean anulacionPagoHasError = false;

    /**
     * Static fields.
     * Saves the state of error of the files being read in 'cargue utilizacion' batch process.
     */
    private static boolean cargueUtilizacionHasError = false;

    /**
     * Method that verifies if a value is empty or null and generates a report for a specific batch process.
     * @param value to be verify for empty or null
     * @param valueName name of the field that is being verified
     * @param job name of the batch process that is being run
     * @param fileName name of the file that is being read in the batch process
     * @param <T> generic for the param value
     */
    public static <T> void verify(T value, String valueName, String job, String fileName){
        if(value == null || value.getClass().isAssignableFrom(String.class) && value.toString().isEmpty()){
            writeInReport(job, valueName, fileName);
            setError(job);
        }
    }

    /**
     * @param process name of the batch process that is being run
     * @return line of the file that is being read in the specified batch process
     */
    public static Long getRegisterCounter(String process) {
        Long registerCounter;
        switch (process){
            case CAMBIO_ESTADO:
                registerCounter = cambioEstadoRegisterCounter;
                break;

            case CARTERA:
                registerCounter = carteraRegisterCounter;
                break;

            case AUTO_AMORTIZADO:
                registerCounter = autoAmortizadoRegisterCounter;
                break;

            case RECAUDO:
                registerCounter = recaudoRegisterCounter;
                break;

            case REFINANCIACION:
                registerCounter = refinanciacionRegisterCounter;
                break;

            case ANULACION_PAGO:
                registerCounter = anulacionPagoRegisterCounter;
                break;

            case CARGUE_UTILIZACION:
                registerCounter = cargueUtilizacionRegisterCounter;
                break;

            default:
                registerCounter = 0L;
                break;
        }
        return registerCounter;
    }

    /**
     * @param process name of the batch process that is being run
     * @return report of errors found in the file that is being read in the specified batch process
     */
    public static StringBuilder getRegisterReport(String process) {
        StringBuilder registerReport;
        switch (process){
            case CAMBIO_ESTADO:
                registerReport = cambioEstadoRegisterReport;
                break;

            case CARTERA:
                registerReport = carteraRegisterReport;
                break;

            case AUTO_AMORTIZADO:
                registerReport = autoAmortizadoRegisterReport;
                break;

            case RECAUDO:
                registerReport = recaudoRegisterReport;
                break;

            case REFINANCIACION:
                registerReport = refinanciacionRegisterReport;
                break;

            case ANULACION_PAGO:
                registerReport = anulacionPagoRegisterReport;
                break;

            case CARGUE_UTILIZACION:
                registerReport = cargueUtilizacionRegisterReport;
                break;

            default:
                registerReport = new StringBuilder();
                break;
        }
        return registerReport;
    }

    /**
     * @param process name of the batch process that is being run
     * @return true if an error has been found in the file being read in the specified batch process
     * @return false if no error has been found in the file being read in the specified batch process
     */
    public static boolean hasError(String process) {
        boolean error = false;
        switch (process){
            case CAMBIO_ESTADO:
                error = cambioEstadoHasError;
                break;

            case CARTERA:
                error = carteraHasError;
                break;

            case AUTO_AMORTIZADO:
                error = autoAmortizadoHasError;
                break;

            case RECAUDO:
                error = recaudoHasError;
                break;

            case REFINANCIACION:
                error = refinanciacionHasError;
                break;

            case ANULACION_PAGO:
                error = anulacionPagoHasError;
                break;

            case CARGUE_UTILIZACION:
                error = cargueUtilizacionHasError;
                break;

            default:
                break;
        }
        return error;
    }

    /**
     * @param process name of the batch process that is being run
     */
    public static void setError(String process) {
    
        switch (process){
            case CAMBIO_ESTADO:
                cambioEstadoHasError = true;
                break;

            case CARTERA:
                carteraHasError = true;
                break;

            case AUTO_AMORTIZADO:
                autoAmortizadoHasError = true;
                break;

            case RECAUDO:
                recaudoHasError = true;
                break;

            case REFINANCIACION:
                refinanciacionHasError = true;
                break;

            case ANULACION_PAGO:
                anulacionPagoHasError = true;
                break;

            case CARGUE_UTILIZACION:
                cargueUtilizacionHasError = true;
                break;

            default:
                break;
        }
    }

    /**
     * @param process name of the batch process that is being run
     */
    public static void increaseRegisterCounter(String process) {
        switch (process){
            case CAMBIO_ESTADO:
                cambioEstadoRegisterCounter++;
                break;

            case CARTERA:
                carteraRegisterCounter++;
                break;

            case AUTO_AMORTIZADO:
                autoAmortizadoRegisterCounter++;
                break;

            case RECAUDO:
                recaudoRegisterCounter++;
                break;

            case REFINANCIACION:
                refinanciacionRegisterCounter++;
                break;

            case ANULACION_PAGO:
                anulacionPagoRegisterCounter++;
                break;

            case CARGUE_UTILIZACION:
                cargueUtilizacionRegisterCounter++;
                break;

            default:
                break;
        }
    }

    /**
     * @param process name of the batch process that is being run
     * @param valueName name of the field where the error was found in the specified batch process
     */
    public static void writeInReport(String process, String valueName, String fileName) {
        String message = "Error en el campo '"+valueName+"' en la linea '"+cambioEstadoRegisterCounter+"' del archivo '"+fileName+"'\n";
        switch (process){
            case CAMBIO_ESTADO:
                cambioEstadoRegisterReport.append(message);
                break;

            case CARTERA:
                carteraRegisterReport.append(message);
                break;

            case AUTO_AMORTIZADO:
                autoAmortizadoRegisterReport.append(message);
                break;

            case RECAUDO:
                recaudoRegisterReport.append(message);
                break;

            case REFINANCIACION:
                refinanciacionRegisterReport.append(message);
                break;

            case ANULACION_PAGO:
                anulacionPagoRegisterReport.append(message);
                break;

            case CARGUE_UTILIZACION:
                cargueUtilizacionRegisterReport.append(message);
                break;

            default:
                break;
        }
    }

    /**
     * @param process name of the batch process that is being run
     */
    public static void resetRegisterFields(String process) {
        switch (process){
            case CAMBIO_ESTADO:
                cambioEstadoRegisterCounter = 1L;
                cambioEstadoHasError = false;
                cambioEstadoRegisterReport.setLength(0);
                break;

            case CARTERA:
                carteraRegisterCounter = 1L;
                carteraHasError = false;
                carteraRegisterReport.setLength(0);
                break;

            case AUTO_AMORTIZADO:
                autoAmortizadoRegisterCounter = 1L;
                autoAmortizadoHasError = false;
                autoAmortizadoRegisterReport.setLength(0);
                break;

            case RECAUDO:
                recaudoRegisterCounter = 1L;
                recaudoHasError = false;
                recaudoRegisterReport.setLength(0);
                break;

            case REFINANCIACION:
                refinanciacionRegisterCounter = 1L;
                refinanciacionHasError = false;
                refinanciacionRegisterReport.setLength(0);
                break;

            case ANULACION_PAGO:
                anulacionPagoRegisterCounter = 1L;
                anulacionPagoHasError = false;
                anulacionPagoRegisterReport.setLength(0);
                break;

            case CARGUE_UTILIZACION:
                cargueUtilizacionRegisterCounter = 1L;
                cargueUtilizacionHasError = false;
                cargueUtilizacionRegisterReport.setLength(0);
                break;

            default:
                break;
        }
    }
    
}
