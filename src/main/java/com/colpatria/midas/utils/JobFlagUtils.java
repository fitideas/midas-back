package com.colpatria.midas.utils;

import com.colpatria.midas.constants.CacheConstants;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;

/**
 * Job flag utils
 */
public class JobFlagUtils {

    /**
     * time to sleep
     */
    @Value("${midas.configuration.wait}")
    private static String waitTime;

    /**
     * @param cacheManager persist cache object
     * @param logger to write process
     * @param job name of process
     */
    public static void ValidateJobExecution(CacheManager cacheManager, Logger logger, String job) throws InterruptedException {
        cacheManager.getCache(CacheConstants.configuration).get("WaitJob");
        while (cacheManager.getCache(CacheConstants.configuration).get("WaitJob") != null) {
            String job_name = cacheManager.getCache(CacheConstants.configuration).get("WaitJob", String.class);
            logger.info("The job {} is running, wait {} seconds.", job_name, 5);
            Thread.sleep(10 * 1000);
        }
        cacheManager.getCache(CacheConstants.configuration).put("WaitJob", job);
    }

}
