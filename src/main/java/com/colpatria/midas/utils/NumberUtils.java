package com.colpatria.midas.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberUtils {

    public static double decimalToDouble(String decimal){
        if(decimal.contains(",")){
            decimal = decimal.replace(",", ".");
        }
        return Double.parseDouble(decimal);
    }

    /**
     * boolean validate if a value is integer type
     * @param value field to validate
    */
    public static boolean isInteger(String value) {
        Matcher matcher;
        matcher = Pattern.compile( "^(0|-?[1-9][0-9]*)$" ).matcher(value);
        return matcher.matches();
    }

    /**
     * boolean validate if a value is decimal type
     * @param value field to validate
    */
    public static boolean isDouble(String value) {
        Matcher matcher;
        matcher = Pattern.compile( "^(-?[0-9]+[.][0-9]+|(0|-?[1-9][0-9]*))$" ).matcher(value);
        return matcher.matches();
    }
    
}
