package com.colpatria.midas.utils;

/*
 *
 * Libraries
 *
*/

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

/**
 * Http parameter Utilities
*/
public class HttpParameterUtil {
    
    /**
     * Method that gets the string from parameters
     * @param parameters are the request parameters 
     * @param parameterName is the parameter name 
     * @return String reference to the text
    */
    public static String getString( Map<String, Serializable> parameters, String parameterName ) {
        // Variables
        Optional<String> parameter;
        String           value;
        // Code
        parameter = Optional.ofNullable( ( String )parameters.get( parameterName ) );
        value     = parameter.isPresent() ? parameter.get() : null;
        return value;
    }

    /**
     * Method that gets the number from parameters
     * @param parameters are the request parameters 
     * @param parameterName is the parameter name 
     * @return Integer reference to the number
    */
    public static Integer getInteger( Map<String, Serializable> parameters, String parameterName ) {
        // Variables
        Integer value;
        String  valueString;
        // Code
        valueString = getString( parameters, parameterName );
        value       = valueString != null ? Integer.parseInt( valueString ) : null;
        return value;
    }

    /**
     * Method that gets the number from parameters
     * @param parameters are the request parameters 
     * @param parameterName is the parameter name 
     * @return Long reference to the number
    */
    public static Long getLong( Map<String, Serializable> parameters, String parameterName ) {
        // Variables
        Long   value;
        String valueString;
        // Code
        valueString = getString( parameters, parameterName );
        value       = valueString != null ? Long.parseLong( valueString ) : null;
        return value;
    }

    /**
     * Method that gets the decimal from parameters
     * @param parameters are the request parameters 
     * @param parameterName is the parameter name 
     * @return Long reference to the decimal
    */
    public static Double getDouble( Map<String, Serializable> parameters, String parameterName ) {
        // Variables
        Double value;
        String valueString;
        // Code
        valueString = getString( parameters, parameterName );
        value       = valueString != null ? Double.parseDouble( valueString ) : null;
        return value;
    }

    /**
     * Method that gets the date from parameters
     * @param parameters are the request parameters 
     * @param parameterName is the parameter name 
     * @return LocalDate reference to the date
    */
    public static LocalDate getDate( Map<String, Serializable> parameters, String parameterName ) {
        // Variables
        LocalDate value;
        String    valueString;
        // Code
        valueString = getString( parameters, parameterName );
        value       = valueString != null ? DateUtils.stringToLocalDate( valueString ) : null;
        return value;
    }

}
