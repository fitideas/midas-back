package com.colpatria.midas.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/*
 * Class that contains a generic data field to create a generic response object
*/
@Getter
@Setter
@AllArgsConstructor
public class GenericResponse <T> {

    /**
     * Generic data field than contains the object or list of objects requested.
    */
    private T data;

    /**
     * String field that contains a message to specify the state of the response.
    */
    private String message;

    /**
     * Constructor onlu for the message field
     * @param message that is return in the response
    */
    public GenericResponse(String message) {
        this.message = message;
    }

}
