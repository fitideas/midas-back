package com.colpatria.midas.configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfig {
    @Value("${midas.http.api_key_header_name}")
    private String apiKeyHeaderName;

    private final BuildProperties buildProperties;

    public OpenAPIConfig(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public OpenAPI openApi() {
        return new OpenAPI()
                .components(new Components()
                        //API Key, see: https://swagger.io/docs/specification/authentication/api-keys/
                        .addSecuritySchemes("apiKeyAuth", new SecurityScheme()
                                .type(SecurityScheme.Type.APIKEY)
                                .in(SecurityScheme.In.HEADER)
                                .name(apiKeyHeaderName)
                        )
                )
                .addSecurityItem(new SecurityRequirement()
                        .addList("apiKeyAuth")
                )
                .info(new Info().title("Midas Backend API").version(buildProperties.getVersion())
                        .license(new License().name("Licencia privada").url("http://springdoc.org")))
                ;
    }
}
