package com.colpatria.midas.configuration;

/*
 *
 * Libraries
 *
*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Configuration class for schedule cache cleaning
*/
@Configuration
@EnableScheduling
public class CachingSchedulerConfig {

    /**
     * Cache manager object
    */
    private final CacheManager cacheManager;

    /**
	 * Class constructor
	 * @param cacheManager is the Cache manager object
	*/
    @Autowired
    public CachingSchedulerConfig( CacheManager cacheManager ) {
        this.cacheManager = cacheManager;        
    }

    /**
	 * method that makes the clean
	*/
    private void evictAllCaches() {
        cacheManager.getCacheNames().stream()
          .forEach( cacheName -> cacheManager.getCache( cacheName ).clear() );
    }

    /**
	 * method that schedules cache cleaning
	*/
    /*@Scheduled( cron = "${midas.caching.cleaning_interval}" )
    public void evictAllcachesAtIntervals( ) {
        evictAllCaches();
    }*/
    
}
