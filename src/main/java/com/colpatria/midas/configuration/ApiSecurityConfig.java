package com.colpatria.midas.configuration;

/*
 *
 * Libraries
 *
 */

import com.colpatria.midas.filters.ApiKeyAuthFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.List;

import static org.springframework.http.HttpMethod.*;

/**
 * Configuration class for the request security
 */
@EnableWebSecurity
@Order(1)
public class ApiSecurityConfig   {

    /**
     * Configuration class for the authentication with api key in /api path
     */
    @Configuration
    @Order(1)
    public static class AuthenticationWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        /**
         * Api key header name
         */
        @Value( "${midas.http.api_key_header_name}" )
        private String apiKeyHeaderName;

        /**
         * Configuration to ignore options
         * */
        @Override
        public void configure(final WebSecurity web) {
            web.ignoring().antMatchers(HttpMethod.OPTIONS);
        }

        /**
         * Api key value
         */
        @Value( "${midas.http.api_key}" )
        private String apiKey;

        /**
         * OpenAPI path
         */
        @Value("${springdoc.api-docs.path}")
        private String apiDocsPath;

        /**
         * OpenAPI path
         */
        @Value("${springdoc.swagger-ui.path}")
        private String swaggerUIPath;

        /**
         * Method that configures authentication
         * @param httpSecurity is the spring security object
         */

        @Override
        protected void configure( HttpSecurity httpSecurity ) throws Exception {
            // Variables
            ApiKeyAuthFilter filter;
            // Code
            filter = new ApiKeyAuthFilter( apiKeyHeaderName );
            filter.setAuthenticationManager(authentication -> {
                // Variables
                String headerValue;
                // Code
                headerValue = ( String )authentication.getPrincipal();
                if( !apiKey.equals( headerValue ) ) {
                    throw new BadCredentialsException( "The API key was not found or not the expected value." );
                }
                authentication.setAuthenticated( true );
                return authentication;
            });
            httpSecurity.csrf().disable()
                    .addFilter( filter )
                    .authorizeRequests()
                    .antMatchers(
                            "/reports/download**",
                            apiDocsPath.concat("**"),
                            apiDocsPath.concat("/**"),
                            swaggerUIPath.concat("/**"),
                            swaggerUIPath.concat("**")).permitAll()
                    .anyRequest()
                    .authenticated();
        }
    }
    /**
     * Configuration class for CORS configuration, CSRF protection disable and stateless policy
     */
    @Configuration
    public static class GlobalWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
        /**
         * List of allowed origins for CORS
         */
        @Value("#{'${midas.configuration.cors.allowed_origins}'.split(',')}")
        private List<String> allowedOrigins;

        /**
         * List of allowed headers
         */
        @Value("#{'${midas.http.api_key_header_name}'.split(',')}")
        private List<String> allowedHeaders;

        /**
         * Method that enables CORS, disables CSRF protection and sets and stateless policy
         * @param httpSecurity is the spring security object
         */
        @Override
        protected void configure(HttpSecurity httpSecurity) throws Exception {
            httpSecurity
                    .cors()
                    .and()
                    .csrf()
                    .disable()
                    .sessionManagement()
                    .sessionCreationPolicy( SessionCreationPolicy.STATELESS );
        }

        /**
         * Method that configures CORS rules
         * @return the CORS configuration
         */
        @Bean
        public CorsFilter corsFilter( ){
            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource( );
            CorsConfiguration config = new CorsConfiguration( );
            for(String origin:allowedOrigins){
                config.addAllowedOrigin(origin);
            }
            config.addAllowedHeader( "*" );
            config.addAllowedMethod( GET );
            config.addAllowedMethod( POST );
            config.addAllowedMethod( PUT );
            config.addAllowedMethod( PATCH );
            config.addAllowedMethod( DELETE );
            config.addAllowedMethod( OPTIONS );
            source.registerCorsConfiguration( "/**", config );
            return new CorsFilter(source);
        }
    }
}
