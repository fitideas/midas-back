package com.colpatria.midas.configuration;

import com.colpatria.midas.constants.CacheConstants;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Enable cache config in app
 */
@EnableCaching
@Configuration
public class CachingConfig {

    /**
     * @return create stage with cache info
     */
    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(CacheConstants.producto,
                                             CacheConstants.tipoProducto,
                                             CacheConstants.tipoIdentificacion,
                                             CacheConstants.sucursal,
                                             CacheConstants.ciclo,
                                             CacheConstants.categoria,
                                             CacheConstants.tipoConsumo,
                                             CacheConstants.claseServicio,
                                             CacheConstants.tipoTarjeta,
                                             CacheConstants.formaPago,
                                             CacheConstants.tipoNegociacion,
                                             CacheConstants.estadoNegociacion,
                                             CacheConstants.departamento,
                                             CacheConstants.municipio,
                                             CacheConstants.localidad,
                                             CacheConstants.cargo,
                                             CacheConstants.cuenta,
                                             CacheConstants.contrato,
                                             CacheConstants.cliente,
                                             CacheConstants.service,
                                             CacheConstants.configuration);
    }
}
