package com.colpatria.midas.dto;

/**
 * Traslados Dto Simple Object
 */
public class TrasladosDto {

    /**
     * Codigo Producto
     */
    private String codigoProducto;

    /**
     * Nombre Producto
     */
    private String nombreProducto;

    /**
     * Codigp Tipo Producto
     */
    private String codigoTipoProducto;

    /**
     * Nombre Tipo Producto
     */
    private String nombreTipoProducto;

    /**
     * Codigo Subproducto
     */
    private String codigoSubproducto;

    /**
     * Nombre Subproducto
     */
    private String nombreSubproducto;

    /**
     * Numero Contrato
     */
    private String numeroContrato;

    /**
     * Tipo Identificacion
     */
    private String tipoIdentificacion;

    /**
     * Numero Documento
     */
    private String numeroDocumento;

    /**
     * Cuenta Actual
     */
    private String cuentaActual;

    /**
     * Cuenta Anterior
     */
    private String cuentaAnterior;

    /**
     * Fecha Traslado
     */
    private String fechaTraslado;

    /**
     * Username
     */
    private String username;

    /**
     * Nombre
     */
    private String nombre;

    /**
     * Area
     */
    private String area;

    /**
     * Ciclo Anterior
     */
    private String cicloAnterior;

    /**
     * Ciclo Nuevo
     */
    private String cicloNuevo;

    /**
     * Observacion
     */
    private String observacion;

    /**
     * CodigoProducto Getter
     * @return Codigo Producto
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Codigo Producto Setter
     * @param codigoProducto Codigo Productoo
     */
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    /**
     * Nombre Producto Getter
     * @return Nombre Producto
     */
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
     * Nombre Producto Setter
     * @param nombreProducto Nombre Producto
     */
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
     * Codigo Tipo Producto Getter
     * @return Codigo Tipo Producto
     */
    public String getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    /**
     * Codigo Tipo Producto Setter
     * @param codigoTipoProducto Codigo Tipo Producto
     */
    public void setCodigoTipoProducto(String codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    /**
     * Nombre tipo producto Getter
     * @return Nombre tipo producto
     */
    public String getNombreTipoProducto() {
        return nombreTipoProducto;
    }

    /**
     * Nombre tipo producto Setter
     * @param nombreTipoProducto Nombre tipo producto
     */
    public void setNombreTipoProducto(String nombreTipoProducto) {
        this.nombreTipoProducto = nombreTipoProducto;
    }

    /**
     * Codigo Subproducto Getter
     * @return Codigo Subproducto
     */
    public String getCodigoSubproducto() {
        return codigoSubproducto;
    }

    /**
     * Codigo Subproducto Setter
     * @param codigoSubproducto Codigo Subproducto
     */
    public void setCodigoSubproducto(String codigoSubproducto) {
        this.codigoSubproducto = codigoSubproducto;
    }

    /**
     * Nombre Subproducto Getter
     * @return Nombre Subproducto
     */
    public String getNombreSubproducto() {
        return nombreSubproducto;
    }

    /**
     * Nombre Subproducto Setter
     * @param nombreSubproducto Nombre Subproducto
     */
    public void setNombreSubproducto(String nombreSubproducto) {
        this.nombreSubproducto = nombreSubproducto;
    }

    /**
     * Numero Contrato Getter
     * @return Numero Contrato
     */
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
     * Numero Contrato Setter
     * @param numeroContrato Numero Contrato
     */
    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    /**
     * Tipo Identificacion Getter
     * @return Tipo Identificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Tipo Identificacion Setter
     * @param tipoIdentificacion Tipo Identificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Numero Docuemento Getter
     * @return Numero Docuemento
     */
    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    /**
     * Numero Docuemento Setter
     * @param numeroDocumento Numero Docuemento
     */
    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    /**
     * Cuenta Actual Getter
     * @return Cuenta Actual
     */
    public String getCuentaActual() {
        return cuentaActual;
    }

    /**
     * Cuenta Actual Setter
     * @param cuentaActual Cuenta Actual
     */
    public void setCuentaActual(String cuentaActual) {
        this.cuentaActual = cuentaActual;
    }

    /**
     * Cuenta Anterior Getter
     * @return Cuenta Anterior
     */
    public String getCuentaAnterior() {
        return cuentaAnterior;
    }

    /**
     * Cuenta Anterior Setter
     * @param cuentaAnterior Cuenta Anterior
     */
    public void setCuentaAnterior(String cuentaAnterior) {
        this.cuentaAnterior = cuentaAnterior;
    }

    /**
     * Fecha Traslado Getter
     * @return Fecha Traslado
     */
    public String getFechaTraslado() {
        return fechaTraslado;
    }

    /**
     * Fecha Traslado Setter
     * @param fechaTraslado Fecha Traslado
     */
    public void setFechaTraslado(String fechaTraslado) {
        this.fechaTraslado = fechaTraslado;
    }

    /**
     * Username Getter
     * @return Username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Username Setter
     * @param username Username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Nombre Getter
     * @return Nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Nombre Setter
     * @param nombre Nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Area Getter
     * @return Area
     */
    public String getArea() {
        return area;
    }

    /**
     * Area Setter
     * @param area Area
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * Ciclo Anterior Getter
     * @return Ciclo Anterior
     */
    public String getCicloAnterior() {
        return cicloAnterior;
    }

    /**
     * Ciclo Anterior Setter
     * @param cicloAnterior Ciclo Anterior
     */
    public void setCicloAnterior(String cicloAnterior) {
        this.cicloAnterior = cicloAnterior;
    }

    /**
     * Ciclo Nuevo Getter
     * @return Ciclo Nuevo
     */
    public String getCicloNuevo() {
        return cicloNuevo;
    }

    /**
     * Ciclo Nuevo Setter
     * @param cicloNuevo Ciclo Nuevo
     */
    public void setCicloNuevo(String cicloNuevo) {
        this.cicloNuevo = cicloNuevo;
    }

    /**
     * Observacion Getter
     * @return Observacion
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * Observacion Setter
     * @param observacion Observacion
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
