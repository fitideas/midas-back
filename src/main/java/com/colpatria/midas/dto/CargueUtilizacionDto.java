package com.colpatria.midas.dto;

/*
 * DTO class thas contains the information of the registers in the file being read by the 'cargue utiliacion' batch process
 * @author Romario Jaimes
 */
public class CargueUtilizacionDto {

     /**
     * String that specifies the code of a product.
     */
    private String productoCodigo;

    /**
     * String that specifies the name of a product.
     */
    private String productoNombre;

    /**
     * String that specifies the code of a type of product.
     */
    private String productoTipoCodigo;

    /**
     * String that specifies the name of a type of product.
     */
    private String productoTipo;

    /**
     * String that specifies the code of a subproduct.
     */
    private String subproductoCodigo;

    /**
     * String that specifies the name of a subproduct.
     */
    private String subproductoNombre;

    /**
     * String that specifies the type of ID.
     */
    private String tipoIdentificacion;

    /**
     * String that specifies the number of an ID.
     */
    private String numeroIdentificacion;

    /**
     * String that specifies the name of the client.
     */
    private String clienteNombre;

    /**
     * String that specifies the number of a service.
     */
    private String servicioNumero;

    /**
     * String that specifies city of origin.
     */
    private String ciudadOrigen;


    /**
     * Date for 'posteo cargue utilizacion'.
     */
    private String fechaPosteo;

    /**
     * Date for 'cargue utilizacion efectiva'.
     */
    private String fechaEfectiva;

    /**
     * String that species the monetary amount
     */
    private String valor;

    /**
     * String that species the monetary amount
     */
    private String valorDescuento;

    /**
     * String that species the monetary amount
     */
    private String valorFinanciado;

    /**
     * String that species the monetary rate
     */
    private String tasa;

    /**
     * String that indentifies the authorization number
     */
    private String autorizacionNumero;

    /**
     * String that indetifies type of 'consumo'
     */
    private String tipoConsumo;


    /**
     * @return the 'productoCodigo' associated with this 'cargue utilizacion'
     */
    public String getProductoCodigo() {
        return this.productoCodigo;
    }

    /**
     * @param productoCodigo to be associated with this 'cargue utilizacio'
     */
    public void setProductoCodigo(String productoCodigo) {
        this.productoCodigo = productoCodigo;
    }

    /**
     * @return the 'productoNombre' associated with this 'cargue utilizacion'
     */
    public String getProductoNombre() {
        return this.productoNombre;
    }

    /**
     * @param productoNombre to be associated with this 'cargue utilizacio'
     */
    public void setProductoNombre(String productoNombre) {
        this.productoNombre = productoNombre;
    }

    /**
     * @return the 'productoTipoCodigo' associated with this 'cargue utilizacion'
     */
    public String getProductoTipoCodigo() {
        return this.productoTipoCodigo;
    }

    /**
     * @param productoTipoCodigo to be associated with this 'cargue utilizacio'
     */
    public void setProductoTipoCodigo(String productoTipoCodigo) {
        this.productoTipoCodigo = productoTipoCodigo;
    }

    /**
     * @return the 'productoTipo' associated with this 'cargue utilizacion'
     */
    public String getProductoTipo() {
        return this.productoTipo;
    }

    /**
     * @param productoTipo to be associated with this 'cargue utilizacio'
     */
    public void setProductoTipo(String productoTipo) {
        this.productoTipo = productoTipo;
    }

    /**
     * @return the 'subproductoCodigo' associated with this 'cargue utilizacion'
     */
    public String getSubproductoCodigo() {
        return this.subproductoCodigo;
    }

    /**
     * @param subproductoCodigo to be associated with this 'cargue utilizacio'
     */
    public void setSubproductoCodigo(String subproductoCodigo) {
        this.subproductoCodigo = subproductoCodigo;
    }

    /**
     * @return the 'subproductoNombre' associated with this 'cargue utilizacion'
     */
    public String getSubproductoNombre() {
        return this.subproductoNombre;
    }

    /**
     * @param subproductoNombre to be associated with this 'cargue utilizacio'
     */
    public void setSubproductoNombre(String subproductoNombre) {
        this.subproductoNombre = subproductoNombre;
    }

    /**
     * @return the 'tipoIdentificacion' associated with this 'cargue utilizacion'
     */
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    /**
     * @param tipoIdentificacion to be associated with this 'cargue utilizacio'
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * @return the 'numeroIdentificacion' associated with this 'cargue utilizacion'
     */
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    /**
     * @param numeroIdentificacion to be associated with this 'cargue utilizacio'
     */
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    /**
     * @return the 'clienteNombre' associated with this 'cargue utilizacion'
     */
    public String getClienteNombre() {
        return this.clienteNombre;
    }

    /**
     * @param clienteNombre to be associated with this 'cargue utilizacio'
     */
    public void setClienteNombre(String clienteNombre) {
        this.clienteNombre = clienteNombre;
    }

    /**
     * @return the 'servicioNumero' associated with this 'cargue utilizacion'
     */
    public String getServicioNumero() {
        return this.servicioNumero;
    }

    /**
     * @param servicioNumero to be associated with this 'cargue utilizacio'
     */
    public void setServicioNumero(String servicioNumero) {
        this.servicioNumero = servicioNumero;
    }

    /**
     * @return the 'ciudadOrigen' associated with this 'cargue utilizacion'
     */
    public String getCiudadOrigen() {
        return this.ciudadOrigen;
    }

    /**
     * @param ciudadOrigen to be associated with this 'cargue utilizacio'
     */
    public void setCiudadOrigen(String ciudadOrigen) {
        this.ciudadOrigen = ciudadOrigen;
    }

    /**
     * @return the 'fechaPosteo' associated with this 'cargue utilizacion'
     */
    public String getFechaPosteo() {
        return this.fechaPosteo;
    }

    /**
     * @param fechaPosteo to be associated with this 'cargue utilizacio'
     */
    public void setFechaPosteo(String fechaPosteo) {
        this.fechaPosteo = fechaPosteo;
    }

    /**
     * @return the 'fechaEfectiva' associated with this 'cargue utilizacion'
     */
    public String getFechaEfectiva() {
        return this.fechaEfectiva;
    }

    /**
     * @param fechaEfectiva to be associated with this 'cargue utilizacio'
     */
    public void setFechaEfectiva(String fechaEfectiva) {
        this.fechaEfectiva = fechaEfectiva;
    }

    /**
     * @return the 'valor' associated with this 'cargue utilizacion'
     */
    public String getValor() {
        return this.valor;
    }

    /**
     * @param valor to be associated with this 'cargue utilizacio'
     */
    public void setValor(String valor) {
        this.valor = valor;
    }

    /**
     * @return the 'valorDescuento' associated with this 'cargue utilizacion'
     */
    public String getValorDescuento() {
        return this.valorDescuento;
    }

    /**
     * @param valorDescuento to be associated with this 'cargue utilizacio'
     */
    public void setValorDescuento(String valorDescuento) {
        this.valorDescuento = valorDescuento;
    }

    /**
     * @return the 'valorFinanciado' associated with this 'cargue utilizacion'
     */
    public String getValorFinanciado() {
        return this.valorFinanciado;
    }

    /**
     * @param valorFinanciado to be associated with this 'cargue utilizacio'
     */
    public void setValorFinanciado(String valorFinanciado) {
        this.valorFinanciado = valorFinanciado;
    }

    /**
     * @return the 'tasa' associated with this 'cargue utilizacion'
     */
    public String getTasa() {
        return this.tasa;
    }

    /**
     * @param tasa to be associated with this 'cargue utilizacio'
     */
    public void setTasa(String tasa) {
        this.tasa = tasa;
    }

    /**
     * @return the 'autorizacionNumero' associated with this 'cargue utilizacion'
     */
    public String getAutorizacionNumero() {
        return this.autorizacionNumero;
    }

    /**
     * @param autorizacionNumero to be associated with this 'cargue utilizacio'
     */
    public void setAutorizacionNumero(String autorizacionNumero) {
        this.autorizacionNumero = autorizacionNumero;
    }

    /**
     * @return the 'tipoConsumo' associated with this 'cargue utilizacion'
     */
    public String getTipoConsumo() {
        return this.tipoConsumo;
    }

    /**
     * @param tipoConsumo to be associated with this 'cargue utilizacio'
     */
    public void setTipoConsumo(String tipoConsumo) {
        this.tipoConsumo = tipoConsumo;
    }
    
}
