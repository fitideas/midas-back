package com.colpatria.midas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductoDto {
    private TipoProductoDto tipoProductoDto;
    private Integer codigo;
    private String nombre;
    private ProductoDto productoPadre;
}
