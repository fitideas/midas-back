package com.colpatria.midas.dto;

import lombok.Data;

/**
 * Class that make the data transfer of objects of the HU01-07-03.
 */
@Data
public class RefinanciacionDto {

    /**
     * String that refers to the code product.
    */
    private String codigoProducto;

    /**
     * String that refers to the name product.
     */
    private String  nombreProducto;

    /**
     * String that refers to the type product code.
     */
    private String codigoTipoProducto;

    /**
     * String that refers to the type product name.
     */
    private String  nombreTipoProducto;

    /**
     * String that refers to the Subproduct code.
     */
    private String codigoSubproducto;

    /**
     * String that refers to the subproduct name
     */
    private String nombreSubproducto;

    /**
     * String that refers to the ID type.
     */
    private String tipoIdentificacion;

    /**
     * String that specifies the ID number.
     */
    private String numeroIdentificacion;

    /**
     * String variable that refers to the contract number.
     */
    private String numeroContrato;

    /**
     * String that refers to the account number.
     */
    private String numeroCuenta;

    /**
     * String that refers to the cicle.
     */
    private String ciclo;

    /**
     * String variable that refers to the negotiation ID.
     */
    private String idNegociacion;

    /**
     * Date of the negotiation.
     */
    private String fechaNegociacion;

    /**
     * Log variable that refers the paid value.
     */
    private String valorPago;

    /**
     * String that refers to the initial fee.
     */
    private String cuotaInicial;

    /**
     * String that refers to the initial fee discount.
     */
    private String descuentoCuotaInicial;

   /**
     * Current date variable.
     */
    private String fechaAct;

    /**
     * Date of application.
     */
    private String fechaApli;

    /**
     * Date of rejection.
     */
    private String fechaRechazo;

    /**
     * String that refers to the reason for rejection.
     */
    private String motivoRechazo;

    /**
     * String that refers to the negotiation state.
     */
    private String estadoNegociacion;

    /**
     * String that refers to the service number.
     */
    private String numeroServicio;

    /**
     * String that refers to the amount of previous installments.
     */
    private String cantidadCuotasAnteriores;

    /**
     * String that refers to the monthly previous fee.
     */
    private String cuotaMensualAnterior;

    /**
     * String that refers to the amount installments after refinancing.
     */
    private String cantidadCuotasDespuesRefinanciacion;

    /**
     * String that refers to the monthly afterward fee.
     */
    private String cuotaMensualDespues;

    /**
     * String that refers to the previous rate.
     */
    private String tasaAnterior;

    /**
     * String that refers to the new rate.
     */
    private String tasaNueva;

    /**
     * String that refers to the change of place rate.
     */
    private String tasaCambioPlaza;

    /**
     * String that specifies the user code.
     */
    private String codigoUsuario;

    /**
     * String that specifies the username.
     */
    private String nombreUsuario;

    /**
     * String that refers to the area where the user beStrings.
    */
    private String area;

    /*
     *
     * Métodos
     *
     */

    /**
     * Empty constructor of the class.
     */
    public RefinanciacionDto(){
    }

    /**
     * Get product code of the class.
     * @return String that refers to the code product.
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Set product code of the class.
     * @param codigoProducto String that refers to the code product.
     */
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    /**
     * Get the name product of the class.
     * @return String that refers to the name product.
     */
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
     * Set the name product of the class.
     * @param nombreProducto String that refers to the name product.
     */
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
     * Get the code type product of a class.
     * @return String that refers to the type product code.
     */
    public String getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    /**
     * Set the code type product of a class.
     * @param codigoTipoProducto String that refers to the type product code.
     */
    public void setCodigoTipoProducto(String codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    /**
     * Get the type product name
     * @return String that refers to the type product name.
     */
    public String getNombreTipoProducto() {
        return nombreTipoProducto;
    }

    /**
     * Set the type product name
     * @param nombreTipoProducto String that refers to the type product name.
     */
    public void setNombreTipoProducto(String nombreTipoProducto) {
        this.nombreTipoProducto = nombreTipoProducto;
    }

    /**
     * Get the subproduct code of a class.
     * @return String that refers to the Subproduct code
     */
    public String getCodigoSubproducto() {
        return codigoSubproducto;
    }

    /**
     * Set the subproduct code of a class.
     * @param codigoSubproducto String that refers to the Subproduct code
     */
    public void setCodigoSubproducto(String codigoSubproducto) {
        this.codigoSubproducto = codigoSubproducto;
    }

    /**
     * Get the subproduct name of a class.
     * @return String that refers to the subproduct name
     */
    public String getNombreSubproducto() {
        return nombreSubproducto;
    }

    /**
     * Set the subproduct name of a class.
     * @param nombreSubproducto String that refers to the subproduct name
     */
    public void setNombreSubproducto(String nombreSubproducto) {
        this.nombreSubproducto = nombreSubproducto;
    }

    /**
     * Get the ID type of class.
     * @return String that refers to the ID type.
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Get the ID type of class.
     * @param tipoIdentificacion String that refers to the ID type.
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the ID number of a class.
     * @return String that specifies the ID number.
     */
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    /**
     * Set the ID number of a class.
     * @param numeroIdentificacion String that specifies the ID number.
     */
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    /**
     * Get the contract number of a class.
     * @return String variable that refers to the contract number.
     */
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
     * Set the contract number of a class.
     * @param numeroContrato String variable that refers to the contract number.
     */
    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    /**
     * Get the account number of a class.
     * @return String that refers to the account number.
     */
    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * Set the account number of a class.
     * @param numeroCuenta String that refers to the account number.
     */
    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    /**
     * Get the cycle of a class.
     * @return String that refers to the cicle.
     */
    public String getCiclo() {
        return ciclo;
    }

    /**
     * Set the cicle of a class.
     * @param ciclo String that refers to the cicle.
     */
    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * Get the ID negotiation of a class.
     * @return String variable that refers to the negotiation ID.
     */
    public String getIdNegociacion() {
        return idNegociacion;
    }

    /**
     * Set the ID negotiation of a class.
     * @param idNegociacion String variable that refers to the negotiation ID.
     */
    public void setIdNegociacion(String idNegociacion) {
        this.idNegociacion = idNegociacion;
    }

    /**
     * Get the negotiation date of a class.
     * @return Date of the negotiation.
     */
    public String getFechaNegociacion() {
        return fechaNegociacion;
    }

    /**
     * Set the negotiation date of a class.
     * @param fechaNegociacion Date of the negotiation.
     */
    public void setFechaNegociacion(String fechaNegociacion) {
        this.fechaNegociacion = fechaNegociacion;
    }

    /**
     * Get the paid value of the class.
     * @return String variable that refers the paid value.
     */
    public String getValorPago() {
        return valorPago;
    }

    /**
     * Set the paid value of the class.
     * @param valorPago String variable that refers the paid value.
     */
    public void setValorPago(String valorPago) {
        this.valorPago = valorPago;
    }

    /**
     * Get the initial fee of a class.
     * @return String that refers to the initial fee.
     */
    public String getCuotaInicial() {
        return cuotaInicial;
    }

    /**
     * Set the initial fee of a class.
     * @param cuotaInicial String that refers to the initial fee.
     */
    public void setCuotaInicial(String cuotaInicial) {
        this.cuotaInicial = cuotaInicial;
    }

    /**
     * Get the initial fee discount of a class.
     * @return String that refers to the initial fee discount.
     */
    public String getDescuentoCuotaInicial() {
        return descuentoCuotaInicial;
    }

    /**
     * Set the initial fee discount of a class.
     * @param descuentoCuotaInicial String that refers to the initial fee discount.
     */
    public void setDescuentoCuotaInicial(String descuentoCuotaInicial) {
        this.descuentoCuotaInicial = descuentoCuotaInicial;
    }

    /**
     * Get the rejection reason of a class.
     * @return String that refers to the reason for rejection.
     */
    public String getMotivoRechazo() {
        return motivoRechazo;
    }

    /**
     * Set the rejection reason of a class.
     * @param motivoRechazo String that refers to the reason for rejection.
     */
    public void setMotivoRechazo(String motivoRechazo) {
        this.motivoRechazo = motivoRechazo;
    }

    /**
     * Get the negotiation state of a class.
     * @return String that refers to the negotiation state.
     */
    public String getEstadoNegociacion() {
        return estadoNegociacion;
    }

    /**
     * Set the negotiation state of a class.
     * @param estadoNegociacion String that refers to the negotiation state.
     */
    public void setEstadoNegociacion(String estadoNegociacion) {
        this.estadoNegociacion = estadoNegociacion;
    }

    /**
     * Get the service number of a class.
     * @return String that refers to the service number.
     */
    public String getNumeroServicio() {
        return numeroServicio;
    }

    /**
     * Set the service number of a class.
     * @param numeroServicio String that refers to the service number.
     */
    public void setNumeroServicio(String numeroServicio) {
        this.numeroServicio = numeroServicio;
    }

    /**
     * Get the amount of previous installments of a class.
     * @return String that refers to the amount of previous installments.
     */
    public String getCantidadCuotasAnteriores() {
        return cantidadCuotasAnteriores;
    }

    /**
     * Set the amount of previous installments of a class.
     * @param cantidadCuotasAnteriores String that refers to the amount of previous installments.
     */
    public void setCantidadCuotasAnteriores(String cantidadCuotasAnteriores) {
        this.cantidadCuotasAnteriores = cantidadCuotasAnteriores;
    }

    /**
     * Get the monthly previous fee of a class.
     * @return String that refers to the monthly previous fee.
     */
    public String getCuotaMensualAnterior() {
        return cuotaMensualAnterior;
    }

    /**
     * Set the monthly previous fee of a class.
     * @param cuotaMensualAnterior String that refers to the monthly previous fee.
     */
    public void setCuotaMensualAnterior(String cuotaMensualAnterior) {
        this.cuotaMensualAnterior = cuotaMensualAnterior;
    }

    /**
     * Get the amount installments after refinancing of a class.
     * @return String that refers to the amount installments after refinancing.
     */
    public String getCantidadCuotasDespuesRefinanciacion() {
        return cantidadCuotasDespuesRefinanciacion;
    }

    /**
     * Set the amount installments after refinancing of a class.
     * @param cantidadCuotasDespuesRefinanciacion String that refers to the amount installments after refinancing.
     */
    public void setCantidadCuotasDespuesRefinanciacion(String cantidadCuotasDespuesRefinanciacion) {
        this.cantidadCuotasDespuesRefinanciacion = cantidadCuotasDespuesRefinanciacion;
    }

    /**
     * Get the monthly afterward fee of a class.
     * @return String that refers to the monthly afterward fee.
     */
    public String getCuotaMensualDespues() {
        return cuotaMensualDespues;
    }

    /**
     * Set the monthly afterward fee of a class.
     * @param cuotaMensualDespues String that refers to the monthly afterward fee.
     */
    public void setCuotaMensualDespues(String cuotaMensualDespues) {
        this.cuotaMensualDespues = cuotaMensualDespues;
    }

    /**
     * Get the previous rate of a class.
     * @return String that refers to the previous rate.
     */
    public String getTasaAnterior() {
        return tasaAnterior;
    }

    /**
     * Set the previous rate of a class.
     * @param tasaAnterior String that refers to the previous rate.
     */
    public void setTasaAnterior(String tasaAnterior) {
        this.tasaAnterior = tasaAnterior;
    }

    /**
     * Get the new rate of a class.
     * @return String that refers to the new rate.
     */
    public String getTasaNueva() {
        return tasaNueva;
    }

    /**
     * Set the new rate of a class.
     * @param tasaNueva String that refers to the new rate.
     */
    public void setTasaNueva(String tasaNueva) {
        this.tasaNueva = tasaNueva;
    }

    /**
     * Get the change of place rate of a class.
     * @return String that refers to the change of place rate.
     */
    public String getTasaCambioPlaza() {
        return tasaCambioPlaza;
    }

    /**
     * Set the change of place rate of a class.
     * @param tasaCambioPlaza String that refers to the change of place rate.
     */
    public void setTasaCambioPlaza(String tasaCambioPlaza) {
        this.tasaCambioPlaza = tasaCambioPlaza;
    }

    /**
     * Get the user code of a class.
     * @return String that specifies the user code.
     */
    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    /**
     * Set the user code of a class.
     * @param codigoUsuario String that specifies the user code.
     */
    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    /**
     * Get the username of a class.
     * @return String that specifies the username.
     */
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    /**
     * Set the username of a class.
     * @param nombreUsuario String that specifies the username.
     */
    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    /**
     * Get the area of a class.
     * @return String that refers to the area where the user beStrings.
     */
    public String getArea() {
        return area;
    }

    /**
     * Set the area of a class.
     * @param area String that refers to the area where the user beStrings.
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * Get the current date.
     * @return Current date variable.
     */
    public String getFechaAct() {
        return fechaAct;
    }

    /**
     * Set the current date.
     * @param fechaAct Current date variable.
     */
    public void setFechaAct(String fechaAct) {
        this.fechaAct = fechaAct;
    }

    /**
     * Get the application date of a class
     * @return Date of application.
     */
    public String getFechaApli() {
        return fechaApli;
    }

    /**
     * Set the application date of a class
     * @param fechaApli Date of application.
     */
    public void setFechaApli(String fechaApli) {
        this.fechaApli = fechaApli;
    }

    /**
     * Get the rejection date of a class.
     * @return Date of rejection.
     */
    public String getFechaRechazo() {
        return fechaRechazo;
    }

    /**
     * Set the rejection date of a class.
     * @param fechaRechazo Date of rejection.
    */
    public void setFechaRechazo(String fechaRechazo) {
        this.fechaRechazo = fechaRechazo;
    }

}