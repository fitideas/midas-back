package com.colpatria.midas.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Dto with data in file
 */
public class DeudaCargoDto {

    /**
     * codigo de producto to process
     */
    @Getter
    @Setter
    private String codigoProducto;

    /**
     * nombre de producto to process
     */
    @Getter
    @Setter
    private String nombreProducto;

    /**
     * codigo tipo de producto to process
     */
    @Getter
    @Setter
    private String codigoTipoProducto;

    /**
     * nombre tipo de producto to process
     */
    @Getter
    @Setter
    private String nombreTipoProducto;

    /**
     * codigo sub producto to process
     */
    @Getter
    @Setter
    private String codigoSubProducto;

    /**
     * nombre sub producto to process
     */
    @Getter
    @Setter
    private String nombreSubProducto;

    /**
     * tipo de identificacion to process
     */
    @Setter
    @Getter
    private String tipoIdentificacion;

    /**
     * numero de identificacion to process
     */
    @Setter
    @Getter
    private String nroIdentificacion;

    /**
     * numero de contrato to process
     */
    @Setter
    @Getter
    private String nroContrato;

    /**
     * numero de cuenta to process
     */
    @Setter
    @Getter
    private String nroCuenta;

    /**
     * Numero de documento to process
     */
    @Setter
    @Getter
    private String nroDocumentoFacturacion;

    /**
     * sucursal to process
     */
    @Setter
    @Getter
    private String sucursal;

    /**
     * fecha documento to process
     */
    @Setter
    @Getter
    private String fechaDocumento;

    /**
     * fecha primer Vencimiento to process
     */
    @Setter
    @Getter
    private String fechaPrimerVencimiento;

    /**
     * fecha segundo Vencimiento to process
     */
    @Setter
    @Getter
    private String fechaSegundoVencimiento;

    /**
     * numero de servicio to process
     */
    @Setter
    @Getter
    private String nroServicio;

    /**
     * descripcion to process
     */
    @Setter
    @Getter
    private String descripcion;

    /**
     * codigo de cargo to process
     */
    @Setter
    @Getter
    private String codCargo;

    /**
     * saldo to process
     */
    @Setter
    @Getter
    private String saldo;

    /**
     * antiguedad to process
     */
    @Setter
    @Getter
    private String antiguedad;
}
