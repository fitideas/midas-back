package com.colpatria.midas.dto;

/**
 * Class that make the data transfer of objects of the HU01-07-08.
 */
public class FacturacionDto {

    /**
     * Codigo Producto
     */
    private String codigoProducto;

    /**
     * Nombre Producto
     */
    private String nombreProducto;

    /**
     * Codigo Tipo Producto
     */
    private String codigoTipoProducto;

    /**
     * Nombre Tipo Producto
     */
    private String nombreTipoProducto;

    /**
     * Codigo Subproducto
     */
    private String codigoSubproducto;

    /**
     * Nombre Subproducto
     */
    private String nombresubproducto;

    /**
     * Numero Contrato
     */
    private String numeroContrato;

    /**
     * Tipo Identificacion
     */
    private String tipoIdentificacion;

    /**
     * Numero Documento
     */
    private String numeroDocumento;

    /**
     * Numero Cuenta
     */
    private String numeroCuenta;

    /**
     * Sector
     */
    private String sector;

    /**
     * Numero Servicio
     */
    private String numeroServicio;

    /**
     * Sucursal
     */
    private String sucursal;

    /**
     * Estrato
     */
    private String estrato;

    /**
     * Facturacion
     */
    private String facturacion;

    /**
     * Descripcion
     */
    private String descripcion;

    /**
     * Cargo
     */
    private String cargo;

    /**
     * Valor Facturacion
     */
    private String valorFact;

    /**
     * Fecha Documento
     */
    private String fechaDoc;

    /**
     * Fecha PR
     */
    private String fechaPr;

    /**
     * Total Documento
     */
    private String totDocumento;

    /**
     * saldo
     */
    private String saldo;


    /**
     * Codigo Producto Getter
     * @return codigoProducto Codigo Producto
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Codigo Producto Setter
     * @param codigoProducto Codigo Producto
     */
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    /**
     * Nombre Producto Getter
     * @return Nombre Producto
     */
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
     * Nombre Producto Setter
     * @param nombreProducto Nombre Producto
     */
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
     * Codigo Tipo Producto Getter
     * @return Codigo Tipo Producto
     */
    public String getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    /**
     * Codigo Tipo Producto Setter
     * @param codigoTipoProducto Codigo Producto
     */
    public void setCodigoTipoProducto(String codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    /**
     * Nombre Tipo Producto Getter
     * @return Nombre Tipo Prodcuto
     */
    public String getNombreTipoProducto() {
        return nombreTipoProducto;
    }

    /**
     * Nombre Tipo Producto Setter
     * @param nombreTipoProducto Nombre Tipo Producto
     */
    public void setNombreTipoProducto(String nombreTipoProducto) {
        this.nombreTipoProducto = nombreTipoProducto;
    }

    /**
     * Codigo Subproducto Getter
     * @return Codigo Subproducto
     */
    public String getCodigoSubproducto() {
        return codigoSubproducto;
    }

    /**
     * Codigo Subproducto Setter
     * @param codigoSubproducto Codigo Subproducto
     */
    public void setCodigoSubproducto(String codigoSubproducto) {
        this.codigoSubproducto = codigoSubproducto;
    }

    /**
     * Nombre Suproducto Getter
     * @return Nombre Suproducto
     */
    public String getNombresubproducto() {
        return nombresubproducto;
    }

    /**
     * Nombre Subproducto Setter
     * @param nombresubproducto Nombre Subproducto Setter
     */
    public void setNombresubproducto(String nombresubproducto) {
        this.nombresubproducto = nombresubproducto;
    }

    /**
     * Numero Contrato Getter
     * @return Numero Contrato
     */
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
     * Numero Contrato Setter
     * @param numeroContrato Numero Contrato
     */
    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    /**
     * Tipo Identificacion Getter
     * @return Tipo Identificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Tipo Identificacion Setter
     * @param tipoIdentificacion Tipo identificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Numero Documento Getter
     * @return Numero Documento
     */
    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    /**
     * Numero documento Setter
     * @param numeroDocumento Numero documento Setter
     */
    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    /**
     * Numero Cuenta Getter
     * @return Numero Cuenta
     */
    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * Numero Cuenta Setter
     * @param numeroCuenta Numero Cuenta
     */
    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    /**
     * Sector Getter
     * @return Sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * Sector Setter
     * @param sector Sector
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * Numero Servicio Getter
     * @return Numero Servicio
     */
    public String getNumeroServicio() {
        return numeroServicio;
    }

    /**
     * Numero Servicio Setter
     * @param numeroServicio Numero Servicio
     */
    public void setNumeroServicio(String numeroServicio) {
        this.numeroServicio = numeroServicio;
    }

    /**
     * Sucursal Getter
     * @return Sucursal
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * Sucursal Setter
     * @param sucursal Sucursal
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * Estrato Getter
     * @return Estrato
     */
    public String getEstrato() {
        return estrato;
    }

    /**
     * Estrato Setter
     * @param estrato Estrato
     */
    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    /**
     * Facturacion Getter
     * @return Facturacion
     */
    public String getFacturacion() {
        return facturacion;
    }

    /**
     * Facturacion Setter
     * @param facturacion Facturacion
     */
    public void setFacturacion(String facturacion) {
        this.facturacion = facturacion;
    }

    /**
     * Descripcion Getter
     * @return Descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Descripcion Setter
     * @param descripcion Descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Cargo Getter
     * @return Cargo
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * Cargo Setter
     * @param cargo Cargo
     */
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    /**
     * Valor Facturacion Getter
     * @return Valor Facturacion
     */
    public String getValorFact() {
        return valorFact;
    }

    /**
     * Valor Facturacion Setter
     * @param valorFact Valor Facturacion
     */
    public void setValorFact(String valorFact) {
        this.valorFact = valorFact;
    }

    /**
     * Fecha Documento Getter
     * @return Fecha Documento
     */
    public String getFechaDoc() {
        return fechaDoc;
    }

    /**
     * Gecha Documento Setter
     * @param fechaDoc Fecha Documento
     */
    public void setFechaDoc(String fechaDoc) {
        this.fechaDoc = fechaDoc;
    }

    /**
     * Fecha PR Getter
     * @return Fecha Pr
     */
    public String getFechaPr() {
        return fechaPr;
    }

    /**
     * Fecha PR Setter
     * @param fechaPr Fecha PR
     */
    public void setFechaPr(String fechaPr) {
        this.fechaPr = fechaPr;
    }

    /**
     * Total Documento Getter
     * @return Total Documento
     */
    public String getTotDocumento() {
        return totDocumento;
    }

    /**
     * Total Documento Setter
     * @param totDocumento Total Documento
     */
    public void setTotDocumento(String totDocumento) {
        this.totDocumento = totDocumento;
    }

    /**
     * Saldo Getter
     * @return Saldo
     */
    public String getSaldo() {
        return saldo;
    }

    /**
     * Saldo Setter
     * @param saldo Saldo
     */
    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }
}
