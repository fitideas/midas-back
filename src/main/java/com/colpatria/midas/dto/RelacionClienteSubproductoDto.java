package com.colpatria.midas.dto;

/**
 * Object that transports the information originated by the 'relacion cliente subproducto' file
*/
public class RelacionClienteSubproductoDto {
    
    /**
     * String that specifies the product code.
     */
    private String codigoProducto;

    /**
     * String that specifies the name product.
     */
    private String nombreProducto;

    /**
     * String that specifies the product type code.
     */
    private String codigoTipoProducto;

    /**
     * String that refers to the product type name.
     */
    private String nombreTipoProducto;

    /**
     * String that specifies the subproduct code.
     */
    private String codigoSubproducto;

    /**
     * String that specifies the subproduct name.
     */
    private String nombreSubproducto;

    /**
     * String that specifies the ID type.
     */
    private String tipoIdentificacion;

    /**
     * String that specifies the ID number.
     */
    private String numeroIdentificacion;

    /**
     * String that specifies the customer.
     */
    private String nombreCliente;

    /**
     * String variable that specifies the contract number.
     */
    private String numeroContrato;

    /**
     * String that specifies the account name.
     */
    private String numeroCuenta;

    /**
     * String that specifies the billing.
     */
    private String facturacion;

    /**
     * String that specifies the status.
     */
    private String estado;

    /**
     * String that specifies the client type.
     */
    private String tipoCliente;

    /**
     * String that specifies the usage quota value.
     */
    private String valorCuotaUtilizacion;

    /**
     * String that specifies the mandatory insurance code.
     */
    private String codigoSeguroObligatorio;

    /**
     * String that specifies the relationship origin date.
     */
    private String fechaOrigenRelacion;

    /**
     * String that specifies the relationship cancellation date.
     */
    private String fechaCancelacionRelacion;

    /**
     * String that specifies the upgrade.
     */
    private String upgrade;

    /**
     * String that specifies the upgrade date.
     */
    private String fechaUpgrade;

    /**
     * String that specifies the punishment start date.
     */
    private String fechaInicialCastigo;

    /**
     * String that specifies the punishment end date.
     */
    private String fechaFinCastigo;

    /**
     * String that specifies the invoice Submission initial date.
     */
    private String fechaInicialEnvioFactura;

    /**
     * String that specifies the end date of sending the invoice.
     */
    private String fechaFinEnvioFactura;

    /**
     * String that specifies the reason for not sending invoice.
     */
    private String motivoNoEnvioFactura;

    /**
	 * Get the product code.
	 * @return the product code.
	*/
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
	 * Set the product code.
	 * @param codigoProducto is the product code.
	*/
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    /**
	 * Get the product name.
	 * @return the product name.
	*/
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
	 * Set the product name.
	 * @param nombreProducto is the product name.
	*/
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
	 * Get the product type code.
	 * @return the product type code.
	*/
    public String getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    /**
	 * Set the product type code.
	 * @param codigoTipoProducto is the product type code.
	*/
    public void setCodigoTipoProducto(String codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    /**
	 * Get the product type name.
	 * @return the product type name.
	*/
    public String getNombreTipoProducto() {
        return nombreTipoProducto;
    }

    /**
	 * Set the product type name.
	 * @param codigoTipoProducto is the product type name.
	*/
    public void setNombreTipoProducto(String nombreTipoProducto) {
        this.nombreTipoProducto = nombreTipoProducto;
    }

    /**
	 * Get the subproduct code.
	 * @return the subproduct code.
	*/
    public String getCodigoSubproducto() {
        return codigoSubproducto;
    }

    /**
	 * Set the subproduct code.
	 * @param codigoSubproducto is the subproduct code.
	*/
    public void setCodigoSubproducto(String codigoSubproducto) {
        this.codigoSubproducto = codigoSubproducto;
    }

    /**
	 * Get the subproduct name.
	 * @return the subproduct name.
	*/
    public String getNombreSubproducto() {
        return nombreSubproducto;
    }

    /**
	 * Set the subproduct name.
	 * @param nombreSubproducto is the subproduct name.
	*/
    public void setNombreSubproducto(String nombreSubproducto) {
        this.nombreSubproducto = nombreSubproducto;
    }

    /**
	 * Get the ID type.
	 * @return the ID type.
	*/
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
	 * Set the ID type.
	 * @param tipoIdentificacion is the ID type.
	*/
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
	 * Get the ID number.
	 * @return the ID number.
	*/
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    /**
	 * Set the ID number.
	 * @param numeroIdentificacion is the ID number.
	*/
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    /**
	 * Get the customer.
	 * @return the customer.
	*/
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
	 * Set the customer.
	 * @param nombreCliente is the customer.
	*/
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    /**
	 * Get the contract number.
	 * @return the contract number.
	*/
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
	 * Set the contract number.
	 * @param numeroContrato is the contract number.
	*/
    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    /**
	 * Get the account name.
	 * @return the account name.
	*/
    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
	 * Set the account name.
	 * @param numeroCuenta is the account name.
	*/
    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    /**
	 * Get the billing.
	 * @return the billing.
	*/
    public String getFacturacion() {
        return facturacion;
    }

    /**
	 * Set the billing.
	 * @param facturacion is the billing.
	*/
    public void setFacturacion(String facturacion) {
        this.facturacion = facturacion;
    }

    /**
	 * Get the state.
	 * @return the state.
	*/
    public String getEstado() {
        return estado;
    }

    /**
	 * Set the state.
	 * @param estado is the state.
	*/
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
	 * Get the client type.
	 * @return the client type.
	*/
    public String getTipoCliente() {
        return tipoCliente;
    }

    /**
	 * Set the client type.
	 * @param tipoCliente is the client type.
	*/
    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    /**
	 * Get the usage quota value.
	 * @return the usage quota value.
	*/
    public String getValorCuotaUtilizacion() {
        return valorCuotaUtilizacion;
    }

    /**
	 * Set the usage quota value.
	 * @param valorCuotaUtilizacion is the usage quota value.
	*/
    public void setValorCuotaUtilizacion(String valorCuotaUtilizacion) {
        this.valorCuotaUtilizacion = valorCuotaUtilizacion;
    }

    /**
	 * Get the mandatory insurance code.
	 * @return the mandatory insurance code.
	*/
    public String getCodigoSeguroObligatorio() {
        return codigoSeguroObligatorio;
    }

    /**
	 * Set the mandatory insurance code.
	 * @param codigoSeguroObligatorio is the mandatory insurance code.
	*/
    public void setCodigoSeguroObligatorio(String codigoSeguroObligatorio) {
        this.codigoSeguroObligatorio = codigoSeguroObligatorio;
    }

    /**
	 * Get the relationship origin date.
	 * @return the relationship origin date.
	*/
    public String getFechaOrigenRelacion() {
        return fechaOrigenRelacion;
    }

    /**
	 * Set the relationship origin date.
	 * @param fechaOrigenRelacion is the relationship origin date.
	*/
    public void setFechaOrigenRelacion(String fechaOrigenRelacion) {
        this.fechaOrigenRelacion = fechaOrigenRelacion;
    }

    /**
	 * Get the relationship cancellation date.
	 * @return the relationship cancellation date.
	*/
    public String getFechaCancelacionRelacion() {
        return fechaCancelacionRelacion;
    }

    /**
	 * Set the relationship cancellation date.
	 * @param fechaCancelacionRelacion is the relationship cancellation date.
	*/
    public void setFechaCancelacionRelacion(String fechaCancelacionRelacion) {
        this.fechaCancelacionRelacion = fechaCancelacionRelacion;
    }

    /**
	 * Get the upgrade.
	 * @return the upgrade.
	*/
    public String getUpgrade() {
        return upgrade;
    }

    /**
	 * Set the upgrade.
	 * @param upgrade is the upgrade.
	*/
    public void setUpgrade(String upgrade) {
        this.upgrade = upgrade;
    }

    /**
	 * Get the upgrade date.
	 * @return the upgrade date.
	*/
    public String getFechaUpgrade() {
        return fechaUpgrade;
    }

    /**
	 * Set the upgrade date.
	 * @param fechaUpgrade is the upgrade date.
	*/
    public void setFechaUpgrade(String fechaUpgrade) {
        this.fechaUpgrade = fechaUpgrade;
    }

    /**
	 * Get the punishment start date.
	 * @return the punishment start date.
	*/
    public String getFechaInicialCastigo() {
        return fechaInicialCastigo;
    }

    /**
	 * Set the punishment start date.
	 * @param fechaInicialCastigo is the punishment start date.
	*/
    public void setFechaInicialCastigo(String fechaInicialCastigo) {
        this.fechaInicialCastigo = fechaInicialCastigo;
    }

    /**
	 * Get the punishment end date.
	 * @return the punishment end date.
	*/
    public String getFechaFinCastigo() {
        return fechaFinCastigo;
    }

    /**
	 * Set the punishment end date.
	 * @param fechaFinCastigo is the punishment end date.
	*/
    public void setFechaFinCastigo(String fechaFinCastigo) {
        this.fechaFinCastigo = fechaFinCastigo;
    }

    /**
	 * Get the invoice Submission initial date.
	 * @return the invoice Submission initial date.
	*/
    public String getFechaInicialEnvioFactura() {
        return fechaInicialEnvioFactura;
    }

    /**
	 * Set the invoice Submission initial date.
	 * @param fechaInicialEnvioFactura is the invoice Submission initial date.
	*/
    public void setFechaInicialEnvioFactura(String fechaInicialEnvioFactura) {
        this.fechaInicialEnvioFactura = fechaInicialEnvioFactura;
    }

    /**
	 * Get the end date of sending the invoice.
	 * @return the end date of sending the invoice.
	*/
    public String getFechaFinEnvioFactura() {
        return fechaFinEnvioFactura;
    }

    /**
	 * Set the end date of sending the invoice.
	 * @param fechaFinEnvioFactura is the end date of sending the invoice.
	*/
    public void setFechaFinEnvioFactura(String fechaFinEnvioFactura) {
        this.fechaFinEnvioFactura = fechaFinEnvioFactura;
    }

    /**
	 * Get the reason for not sending invoice.
	 * @return the reason for not sending invoice.
	*/
    public String getMotivoNoEnvioFactura() {
        return motivoNoEnvioFactura;
    }

    /**
	 * Set the reason for not sending invoice.
	 * @param motivoNoEnvioFactura is the reason for not sending invoice.
	*/
    public void setMotivoNoEnvioFactura(String motivoNoEnvioFactura) {
        this.motivoNoEnvioFactura = motivoNoEnvioFactura;
    }

}
