package com.colpatria.midas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CondonacionDto {

    /**
     * String that specifies the code of a product.
     */
    private String productoCodigo;

    /**
     * String that specifies the name of a product.
     */
    private String productoNombre;

    /**
     * Integer that specifies the code of a type of product.
     */
    private String productoTipoCodigo;

    /**
     * String that specifies the name of a type of product.
     */
    private String productoTipo;

    /**
     * Integer that specifies the code of a subproduct.
     */
    private String subproductoCodigo;

    /**
     * String that specifies the name of a subproduct.
     */
    private String subproductoNombre;

    /**
     * Integer that specifies the type of ID.
     */
    private String tipoIdentificacion;

    /**
     * String that specifies the number of an ID.
     */
    private String numeroIdentificacion;

    /**
     * String that refers the number of a contract.
     */
    private String contratoNumero;

    /**
     * String that refers the number of an account.
     */
    private String numeroCuenta;

    /**
     * String that refers the cicle number.
     */
    private String ciclo;

    /**
     * String that specifies the negotiation identifier.
     */
    private String idNegociacion;

    /**
     * String that specifies the negotiation date.
     */
    private String fechaNegociacion;

    /**
     * String that specifies the payment date.
     */
    private String fechaPago;

    /**
     * String that specifies the Application date.
     */
    private String fechaAplicacion;

    /**
     * String that specifies the rejection date.
     */
    private String fechaRechazo;

    /**
     * String that specifies the rejection cause.
     */
    private String motivoRechazo;

    /**
     * String that specifies the due date.
     */
    private String fechaVencimiento;

    /**
     * String that specifies the negotiation kind.
     */
    private String tipoNegociacion;

    /**
     * String that specifies the negotiation state.
     */
    private String estadoNegociacion;

    /**
     * String that specifies the payment ammount.
     */
    private String valorPago;

    /**
     * String that specifies the service code.
     */
    private String numeroServicio;

    /**
     * String that specifies the condonation description.
     */
    private String descripcion;

    /**
     * String that specifies the condonation Ammount.
     */
    private String capitalCondonado;

    /**
     * String that specifies the balance condonation ammount.
     */
    private String saldoCapitalCondonado;

    /**
     * String that specifies the ??????.
     */
    private String intecteCondona;

    /**
     * String that specifies the ?????.
     */
    private String intemoCondona;

    /**
     * String that specifies the condonation ammount.
     */
    private String valorCondonado;

    /**
     * String that specifies the condonation spending.
     */
    private String gastoCobraCondonacion;

    /**
     * String that specifies the condonation fee.
     */
    private String cuotaMancondona;

    /**
     * String that specifies other charges in the condonation.
     */
    private String otrosCargosCondonacion;

    /**
     * String that specifies the user code.
     */
    private String codUsuario;

    /**
     * String that specifies the user.
     */
    private String user;

    /**
     * String that specifies the area.
     */
    private String area;


}
