package com.colpatria.midas.dto;

/*
 *
 * Clase
 *
*/

import java.time.LocalDate;

/**
 * Class that make the data transfer of objects of the logs
 */
public class LogDto {

    /**
     * Int that refers to the number of records.
     */
    private int       numeroRegistros;

    /**
     * String that refers to the name of the archive.
     */
    private String    nombreArchivo;

    /**
     * Current Date of the archive.
     */
    private LocalDate fecha;

    /**
     * Constructor that give the values of each of the variables un the class.
     * @param numeroRegistros Int that refers to the number of records.
     * @param nombreArchivo String that refers to the name of the archive.
     * @param fecha Actual Date of the archive.
     */
    public LogDto( int numeroRegistros, String nombreArchivo, LocalDate fecha ) {
        this.numeroRegistros = numeroRegistros;
        this.nombreArchivo   = nombreArchivo;
        this.fecha           = fecha;
    }

    /**
     * Get the number of records of the class.
     * @return Int that refers to the number of records.
     */
    public int getNumeroRegistros() {
        return numeroRegistros;
    }

    /**
     * Get the name of the archive of the class.
     * @return String that refers to the name of the archive.
     */
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    /**
     * Get the current date of the class.
     * @return Current Date of the archive.
     */
    public LocalDate getFecha() {
        return fecha;
    }
}
