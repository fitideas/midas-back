package com.colpatria.midas.dto.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UploadResponseDto {
    private int cargados;
    private int fallidos;
    private int duplicados;
    private String archivoLog;
    public void increaseCargados(){
        cargados++;
    }
    public void increaseFallidos(){
        fallidos++;
    }
    public void increaseDuplicados(){
        duplicados++;
    }
}
