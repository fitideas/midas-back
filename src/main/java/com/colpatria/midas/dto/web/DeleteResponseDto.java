package com.colpatria.midas.dto.web;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * DTO response for delete operation
 */
@Data
@AllArgsConstructor
public class DeleteResponseDto {
    /**
     * the amount of deleted entries
     */
    private int deleted;
    /**
     * the amount of failed deletions
     */
    private int failed;
    /**
     * the name of the file with the errors
     */
    private String file;

}
