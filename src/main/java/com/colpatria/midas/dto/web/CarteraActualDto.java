package com.colpatria.midas.dto.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarteraActualDto {
    private Integer identifier;
    private String nombreCliente;
    private Double interesesCorrientes;
    private Double  interesesMora;
    private Double cuotaManejo;
    private Double gastosCobranzas;
    private Double seguros;
    private String numeroCompra;
    private Double nuevosCobros;
    private Integer diasMora;
    private String estadoCompra;
    private Integer totalElements;
    private Integer totalPages;
}
