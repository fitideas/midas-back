package com.colpatria.midas.dto.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericResponseDto {
    private Integer totalElements;
    private Integer totalPages;
    private Serializable content;
}
