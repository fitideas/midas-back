package com.colpatria.midas.dto;

/**
 * Cartera Exigible dto.
 */
public class CarteraExigibleDto {

    /**
     * codigo producto in data.
     */
    private String codigoProducto;

    /**
     * Nombre producto in data.
     */
    private String nombreProducto;

    /**
     * codigo tipo de producto in data.
     */
    private String codigoTipoProducto;

    /**
     * nombre tipo de producto in data.
     */
    private String nombreTipoProducto;

    /**
     * codigo subproducto in data.
     */
    private String codigoSubProducto;

    /**
     * Nombresubproducto in data.
     */
    private String nombreSubProducto;

    /**
     * Tipo de documento de identidad
     */
    private String tipoDocumentoIdentidad;

    /**
     * Numero de documento del titular a procesar
     */
    private String nroDocumentoTitular;

    /**
     * numero de contrato in data.
     */
    private String nroContrato;

    /**
     * numero de cuenta in data.
     */
    private String nroCuenta;

    /**
     * sucursal in data.
     */
    private String sucursal;

    /**
     * fecha documento in data.
     */
    private String fechaDocumento;

    /**
     * fecha de primer vencimiento in data.
     */
    private String fechaPrimerVencimiento;

    /**
     * fecha de segundo vencimiento in data.
     */
    private String fechaSegundoVencimiento;

    /**
     * numero de servicio in data.
     */
    private String nroServicio;

    /**
     * descripcion in data.
     */
    private String descripcion;

    /**
     * Cargo in data.
     */
    private String codCargo;

    /**
     * saldo in da ta.
     */
    private String saldo;

    /**
     * antiguedad in data.
     */
    private String antiguedadItem;
}
