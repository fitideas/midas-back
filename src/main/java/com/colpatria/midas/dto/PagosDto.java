package com.colpatria.midas.dto;

import java.time.LocalDate;

/**
 * Objeto que transporta la informacion originada por el archivo de pagos
 */
public class PagosDto {

    /**
     * Codigo del producto a procesar
     */
    private String codigoProducto;

    /**
     * Nombre del producto a procesar
     */
    private String nombreProducto;

    /**
     * Codigo del tipo producto a procesar
     */
    private String codigoTipoProducto;

    /**
     * Nombre del tipo de producto a procesar
     */
    private String nombreTipoProducto;

    /**
     * Codigo del subproducto a procesar
     */
    private String codigoSubproducto;

    /**
     * Nombre del subproducto a procesar
     */
    private String nombreSubproducto;

    /**
     * Numero de servicio a procesar
     */
    private String nroServicio;

    /**
     * Tipo de documento de identidad
     */
    private String tipoDocumentoIdentidad;

    /**
     * Numero de documento del titular a procesar
     */
    private String nroDocumentoTitular;

    /**
     * Nombre de cliente que procesa la informacion
     */
    private String nombreCliente;

    /**
     * Apellido paterno a procesar
     */
    private String apellidoPaterno;

    /**
     * Apellido materno a procesar
     */
    private String apellidoMaterno;

    /**
     * Sucursal que se procesa en los datos
     */
    private String sucursal;

    /**
     * Numero de documento que se utilizo para pagar
     */
    private String nroDocPago;

    /**
     * Fecha en que ingreso el pago
     */
    private String fechaIngreso;

    /**
     * Fecha en que se realizo el pago
     */
    private String fechaPago;

    /**
     * Fecha de vencimiento del pago
     */
    private String fechaVencimiento;

    /**
     * Numero del servicio
     */
    private String numeroServicio;

    /**
     * Codigo del cargo asignado en colpatria
     */
    private String codCargoColpatria;

    /**
     * Codigo del cargo
     */
    private String codCargo;

    /**
     * Descripcion del codigo del cargo
     */
    private String descCargo;

    /**
     * Monto que se paga
     */
    private String monto;

    /**
     * Signo del monto
     */
    private String signo;

    /**
     * Cuenta contable a la que pertenece
     */
    private String cuentaContable;

    /**
     * Monto de participacion
     */
    private String montoParticipacion;

    /**
     * @return retorna el codigo del producto
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * @param codigoProducto setea el codigo del producto
     */
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    /**
     * @return Obtiene el nombre del producto
     */
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
     * @param nombreProducto Setea el nombre del producto
     */
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
     * @return obtiene el codigo del producto
     */
    public String getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    /**
     * @param codigoTipoProducto setea el codigo del producto
     */
    public void setCodigoTipoProducto(String codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    /**
     * @return obtiene el nombre del producto
     */
    public String getNombreTipoProducto() {
        return nombreTipoProducto;
    }

    /**
     * @param nombreTipoProducto setea el nombre del producto
     */
    public void setNombreTipoProducto(String nombreTipoProducto) {
        this.nombreTipoProducto = nombreTipoProducto;
    }

    /**
     * @return obtiene el codigo del subproducto
     */
    public String getCodigoSubproducto() {
        return codigoSubproducto;
    }

    /**
     * @param codigoSubproducto setea el codigo del subproducto
     */
    public void setCodigoSubproducto(String codigoSubproducto) {
        this.codigoSubproducto = codigoSubproducto;
    }

    /**
     * @return se obtiene el nombre del subproducto
     */
    public String getNombreSubproducto() {
        return nombreSubproducto;
    }

    /**
     * @param nombreSubproducto setea el nombre del subproducto
     */
    public void setNombreSubproducto(String nombreSubproducto) {
        this.nombreSubproducto = nombreSubproducto;
    }

    /**
     * @return get numero de servicio
     */
    public String getNroServicio() {
        return nroServicio;
    }

    /**
     * @param nroServicio se setea el numero del servicio
     */
    public void setNroServicio(String nroServicio) {
        this.nroServicio = nroServicio;
    }

    /**
     * @return se obtiene el tipo de documento
     */
    public String getTipoDocumentoIdentidad() {
        return tipoDocumentoIdentidad;
    }

    /**
     * @param tipoDocumentoIdentidad setea el documento de identidad
     */
    public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
        this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
    }

    /**
     * @return se obtiene numero de documento titular
     */
    public String getNroDocumentoTitular() {
        return nroDocumentoTitular;
    }

    /**
     * @param nroDocumentoTitular numero de documento titular
     */
    public void setNroDocumentoTitular(String nroDocumentoTitular) {
        this.nroDocumentoTitular = nroDocumentoTitular;
    }

    /**
     * @return obtiene el numero del cliente
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * @param nombreCliente setea el del nombreCliente
     */
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    /**
     * @return se obtiene el apellido paterno
     */
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * @param apellidoPaterno setea el apellido paterno
     */
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    /**
     * @return se obtiene el apellido materno
     */
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * @param apellidoMaterno se setea el apellido materno
     */
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    /**
     * @return se obtiene el numero de la sucursal
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * @param sucursal se setea el numero de la sucursal
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * @return obtiene el numero de documento del pago
     */
    public String getNroDocPago() {
        return nroDocPago;
    }

    /**
     * @param nroDocPago se setea el numero de documetno q realizo el pago
     */
    public void setNroDocPago(String nroDocPago) {
        this.nroDocPago = nroDocPago;
    }

    /**
     * @return se obtiene la fecha de ingreso
     */
    public String getFechaIngreso() {
        return fechaIngreso;
    }

    /**
     * @param fechaIngreso se setea la fecha de ingreso
     */
    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    /**
     * @return se obtiene la fecha de pago
     */
    public String getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago se setea la fecha del pago
     */
    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return obtiene la fecha de vencimiento
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * @param fechaVencimiento se setea la fecha de vencimiento
     */
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * @return se obtiene el numero del servicio
     */
    public String getNumeroServicio() {
        return numeroServicio;
    }

    /**
     * @param numeroServicio se setea el numero de servicio
     */
    public void setNumeroServicio(String numeroServicio) {
        this.numeroServicio = numeroServicio;
    }

    /**
     * @return se obtiene el codigo del cargo en colpatria
     */
    public String getCodCargoColpatria() {
        return codCargoColpatria;
    }

    /**
     * @param codCargoColpatria se setea ek valor del codigo de cargo colpatria
     */
    public void setCodCargoColpatria(String codCargoColpatria) {
        this.codCargoColpatria = codCargoColpatria;
    }

    /**
     * @return se obtiene el codigo del cargo
     */
    public String getCodCargo() {
        return codCargo;
    }

    /**
     * @param codCargo se setea el codigo del cargo
     */
    public void setCodCargo(String codCargo) {
        this.codCargo = codCargo;
    }

    /**
     * @return se obtiene el la descripcion del cargo
     */
    public String getDescCargo() {
        return descCargo;
    }

    /**
     * @param descCargo se setea la descripcion del cargo
     */
    public void setDescCargo(String descCargo) {
        this.descCargo = descCargo;
    }

    /**
     * @return obtiene el monto
     */
    public String getMonto() {
        return monto;
    }

    /**
     * @param monto setea el valor del monto
     *
     */
    public void setMonto(String monto) {
        this.monto = monto;
    }

    /**
     * @return obtiene el signo del monto
     */
    public String getSigno() {
        return signo;
    }

    /**
     * @param signo setea el signo del monto
     */
    public void setSigno(String signo) {
        this.signo = signo;
    }

    /**
     * @return obtiene el codigo contable
     */
    public String getCuentaContable() {
        return cuentaContable;
    }

    /**
     * @param cuentaContable setea la cuenta contable
     */
    public void setCuentaContable(String cuentaContable) {
        this.cuentaContable = cuentaContable;
    }

    /**
     * @return obtiene el monto de participacion
     */
    public String getMontoParticipacion() {
        return montoParticipacion;
    }

    /**
     * @param montoParticipacion setea el monto de participacion
     */
    public void setMontoParticipacion(String montoParticipacion) {
        this.montoParticipacion = montoParticipacion;
    }
}
