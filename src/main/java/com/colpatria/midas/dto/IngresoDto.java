package com.colpatria.midas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IngresoDto {
    /**
     * String that specifies the customer Document Type.
     */
    private String tipoDocumento;
    /**
     * String thas specifies the customer document number
     */
    private String numeroDocumento;
    /**
     * String thas specifies the fecha documento
     */
    private String fechaDocumento;
    /**
     * String thas specifies the Servicio Number
     */
    private String numeroServicio;
    /**
     * String thas specifies the tasa Interes corriente
     */
    private String tasaInteresCorriente;
    /**
     * String that refers the fecha causacion
      */
    private String fechaCausacion;
    /**
     * String that refers the tasa interes mora
     */
    private String tasaInteresMora;
    /**
     * String that refers the codigo cargo
     */
    private String codigoCargo;
    /**
     * String that refers the monto
     */
    private String monto;
    /**
     * String that refers the signo
     */
    private String signo;
    /**
     * String that refers the cuenta contable
     */
    private String cuentaContable;
    /**
     * String that refers the codigo producto
     */
    private String codigoProducto;
    /**
     * String that refers the numero contrato
     */
    private String numeroContrato;
    /**
     * String that refers the municipio
     */
    private String municipio;
    /**
     * String that refers the codigo subproducto
     */
    private String codigoSubproducto;

}
