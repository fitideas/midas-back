package com.colpatria.midas.dto;

/*
 * DTO class thas contains the information of the registers in the file being read by the 'auto amortizados' batch process
 * @author Romario Jaimes
 */
public class AutoAmortizadoDto {

    /**
     * String that specifies the code of a product.
     */
    private String productoCodigo;

    /**
     * String that specifies the name of a product.
     */
    private String productoNombre;

    /**
     * String that specifies the code of a type of product.
     */
    private String productoTipoCodigo;

    /**
     * String that specifies the name of a type of product.
     */
    private String productoTipo;

    /**
     * String that specifies the code of a subproduct.
     */
    private String subproductoCodigo;

    /**
     * String that specifies the name of a subproduct.
     */
    private String subproductoNombre;

    /**
     * String that refers the number of a contract.
     */
    private String contratoNumero;

    /**
     * String that specifies the type of ID.
     */
    private String tipoIdentificacion;

    /**
     * String that specifies the number of an ID.
     */
    private String numeroIdentificacion;

    /**
     * String that specifies the name of the client.
     */
    private String clienteNombre;

    /**
     * String that specifies the number of a service.
     */
    private String servicioNumero;

    /**
     * String that specifies the bill number.
     */
    private String numeroDocumentoPagado;

    /**
     * Date for 'ingreso auto amortizacion'.
     */
    private String fechaIngreso;

    /**
     * Date for 'pago auto amortizacion'.
     */
    private String fechaPago;

    /**
     * Date for 'vencimiento auto amortizacion'.
     */
    private String fechaVencimiento;

    /**
     * Date for 'posteo auto amortizacion'.
     */
    private String fechaPosteo;

    /**
     * Date for 'auto amortizacion efectiva'.
     */
    private String fechaEfectiva;

    /**
     * String that indetifies the 'cargo'
     */
    private String cargoCodigo;

    /**
     * String that describes the 'cargo'
     */
    private String cargoNombre;

    /**
     * String that indentifies the type of 'cuenta'
     */
    private String cuentaTipo;

    /**
     * String that species the monetary amount of 'monto auto amortizacion'
     */
    private String monto;

    /**
     * String that species the monetary amount of 'monto participacion auto amortizacion'
     */
    private String montoParticipacion;

    /**
     * @return the 'productoCodigo' associated with this 'auto amortizacion'
     */
    public String getProductoCodigo() {
        return this.productoCodigo;
    }

    /**
     * @param productoCodigo to be associated with this 'auto amortizacion'
     */
    public void setProductoCodigo(String productoCodigo) {
        this.productoCodigo = productoCodigo;
    }

    /**
     * @return the 'productoNombre' associated with this 'auto amortizacion'
     */
    public String getProductoNombre() {
        return this.productoNombre;
    }

    /**
     * @param productoNombre to be associated with this 'auto amortizacion'
     */
    public void setProductoNombre(String productoNombre) {
        this.productoNombre = productoNombre;
    }

    /**
     * @return the 'productoTipoCodigo' associated with this 'auto amortizacion'
     */
    public String getProductoTipoCodigo() {
        return this.productoTipoCodigo;
    }

    /**
     * @param productoTipoCodigo to be associated with this 'auto amortizacion'
     */
    public void setProductoTipoCodigo(String productoTipoCodigo) {
        this.productoTipoCodigo = productoTipoCodigo;
    }

    /**
     * @return the 'productoTipo' associated with this 'auto amortizacion'
     */
    public String getProductoTipo() {
        return this.productoTipo;
    }

    /**
     * @param productoTipo to be associated with this 'auto amortizacion'
     */
    public void setProductoTipo(String productoTipo) {
        this.productoTipo = productoTipo;
    }

    /**
     * @return the 'subproductoCodigo' associated with this 'auto amortizacion'
     */
    public String getSubproductoCodigo() {
        return this.subproductoCodigo;
    }

    /**
     * @param subproductoCodigo to be associated with this 'auto amortizacion'
     */
    public void setSubproductoCodigo(String subproductoCodigo) {
        this.subproductoCodigo = subproductoCodigo;
    }

    /**
     * @return the 'subproductoNombre' associated with this 'auto amortizacion'
     */
    public String getSubproductoNombre() {
        return this.subproductoNombre;
    }

    /**
     * @param subproductoNombre to be associated with this 'auto amortizacion'
     */
    public void setSubproductoNombre(String subproductoNombre) {
        this.subproductoNombre = subproductoNombre;
    }

    /**
     * @return the 'contratoNumero' associated with this 'auto amortizacion'
     */
    public String getContratoNumero() {
        return this.contratoNumero;
    }

    /**
     * @param contratoNumero to be associated with this 'auto amortizacion'
     */
    public void setContratoNumero(String contratoNumero) {
        this.contratoNumero = contratoNumero;
    }

    /**
     * @return the 'tipoIdentificacion' of 'cliente' associated with this 'auto amortizacion'
     */
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    /**
     * @param tipoIdentificacion to be associated with this 'auto amortizacion'
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * @return the 'numeroIdentificacion' of 'cliente' associated with this 'auto amortizacion'
     */
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    /**
     * @param numeroIdentificacion to be associated with this 'auto amortizacion'
     */
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    /**
     * @return the 'clienteNombre' of 'cliente' associated with this 'auto amortizacion'
     */
    public String getClienteNombre() {
        return this.clienteNombre;
    }

    /**
     * @param clienteNombre to be associated with this 'auto amortizacion'
     */
    public void setClienteNombre(String clienteNombre) {
        this.clienteNombre = clienteNombre;
    }

    /**
     * @return the 'clienteNombre' of 'cliente' associated with this 'auto amortizacion'
     */
    public String getServicioNumero() {
        return this.servicioNumero;
    }

    /**
     * @param servicioNumero to be associated with this 'auto amortizacion'
     */
    public void setServicioNumero(String servicioNumero) {
        this.servicioNumero = servicioNumero;
    }

    /**
     * @return the 'numeroDocumentoPagado' associated with this 'auto amortizacion'
     */
    public String getNumeroDocumentoPagado() {
        return this.numeroDocumentoPagado;
    }

    /**
     * @param numeroDocumentoPagado to be associated with this 'auto amortizacion'
     */
    public void setNumeroDocumentoPagado(String numeroDocumentoPagado) {
        this.numeroDocumentoPagado = numeroDocumentoPagado;
    }

    /**
     * @return the 'fechaIngreso' associated with this 'auto amortizacion'
     */
    public String getFechaIngreso() {
        return this.fechaIngreso;
    }

    /**
     * @param fechaIngreso saves 'fechaIngreso' associated with this 'auto amortizacion'
     */
    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    /**
     * @return the 'fechaPago' associated with this 'auto amortizacion'
     */
    public String getFechaPago() {
        return this.fechaPago;
    }

    /**
     * @param fechaPago saves 'fechaPago' associated with this 'auto amortizacion'
     */
    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return the 'fechaVencimiento' associated with this 'auto amortizacion'
     */
    public String getFechaVencimiento() {
        return this.fechaVencimiento;
    }

    /**
     * @param fechaVencimiento saves 'fechaVencimiento' associated with this 'auto amortizacion'
     */
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * @return the 'fechaEfectiva' associated with this 'auto amortizacion'
     */
    public String getFechaEfectiva() {
        return this.fechaEfectiva;
    }

    /**
     * @param fechaEfectiva saves 'fechaEfectiva' associated with this 'auto amortizacion'
     */
    public void setFechaEfectiva(String fechaEfectiva) {
        this.fechaEfectiva = fechaEfectiva;
    }

    /**
     * @return the 'cargoCodigo' of the 'cargo' associated with this 'auto amortizacion'
     */
    public String getCargoCodigo() {
        return this.cargoCodigo;
    }

    /**
     * @param cargoCodigo of the 'cargo' to be associated with this 'auto amortizacion'
     */
    public void setCargoCodigo(String cargoCodigo) {
        this.cargoCodigo = cargoCodigo;
    }

    /**
     * @return the 'cargoNombre' of the 'cargo' associated with this 'auto amortizacion'
     */
    public String getCargoNombre() {
        return this.cargoNombre;
    }

    /**
     * @param cargoNombre of the 'cargo' to be associated with this 'auto amortizacion'
     */
    public void setCargoNombre(String cargoNombre) {
        this.cargoNombre = cargoNombre;
    }

    /**
     * @return the 'cuentaTipo' of the 'cuenta' associated with this 'auto amortizacion'
     */
    public String getCuentaTipo() {
        return this.cuentaTipo;
    }

    /**
     * @param cuentaTipo of the 'cuenta' to be associated with this 'auto amortizacion'
     */
    public void setCuentaTipo(String cuentaTipo) {
        this.cuentaTipo = cuentaTipo;
    }

    /**
     * @return the 'monto' associated with this 'auto amortizacion'
     */
    public String getMonto() {
        return this.monto;
    }

    /**
     * @param monto to be associated with this 'auto amortizacion'
     */
    public void setMonto(String monto) {
        this.monto = monto;
    }

    /**
     * @return the 'montoParticipacion' associated with this 'auto amortizacion'
     */
    public String getMontoParticipacion() {
        return this.montoParticipacion;
    }

    /**
     * @param montoParticipacion to be associated with this 'auto amortizacion'
     */
    public void setMontoParticipacion(String montoParticipacion) {
        this.montoParticipacion = montoParticipacion;
    }

    /**
     * @return the 'fechaPosteo' associated with this 'auto amortizacion'
     */
    public String getFechaPosteo() {
        return this.fechaPosteo;
    }

    /**
     * @param fechaPosteo saves 'fechaPosteo' associated with this 'auto amortizacion'
     */
    public void setFechaPosteo(String fechaPosteo) {
        this.fechaPosteo = fechaPosteo;
    }

}
