package com.colpatria.midas.dto;

import lombok.Data;

@Data
public class CasasDeCobranzaProbeDto {
    private String numide;
    private String numobl;
    private String saldoK;
    private String diasmo1;
    private String diasmo2;
    private String edad1;
    private String edad2;
    private String cuomor;
    private String saldoVenc;
    private String fecRedif;
    private String valPago;
    private String valorCuot;
    private String cantPagos;
    private String fecVence;
    private String tipoTarj;
    private String proend;
    private String diasmoPro;
    private String pagMinimo;
    private String mLibros;
    private String esp;
    private String tipide;
    private String numide1;
    private String famparada;
    private String doctoAmp;
    private String tipoTarje;
    private String fecUltRe;
    private String saldoTot;
    private String cantRefi;
    private String tipModi1;
    private String fecModif;
    private String fecSegu;
    private String numeroCli;
    private String fechaRepo;
    private String indapert;
    private String cantApert;
    private String asiignado;
    private String fecutil;
    private String indIloc;
    private String cantComp;
    private String valComp;
    private String fechaCuotaMasVencida;
    private String valorDesembolso;
    private String intCte;
    private String intMot;
    private String intTot;
    private String cManejo;
    private String gCobranz;
    private String cTasain;
    private String ciclo;
    private String valPag;
    private String fecPag;
    private String sucursal;
    private String fecha;
    private String plazo;
    private String numeroObligacionCobranzas;
    private String flagReclamo;

}
