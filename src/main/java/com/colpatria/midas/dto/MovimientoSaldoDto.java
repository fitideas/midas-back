package com.colpatria.midas.dto;

/**
 * Object that transports the information originated by the movement money file
*/
public class MovimientoSaldoDto {

    /**
     * String that specifies the product code
    */
    private String codigoProducto;

    /**
     * String that specifies the product name
    */
    private String nombreProducto;

    /**
     * String that specifies the product type code
    */
    private String codigoTipoProducto;

    /**
     * String that specifies the product type name
    */
    private String nombreTipoProducto;

    /**
     * String that specifies the by-product code
    */
    private String codigoSubproducto;

    /**
     * String that specifies the by-product name
    */
    private String nombreSubproducto;

    /**
     * String that specifies the identification type
    */
    private String tipoIdentificacion;

    /**
     * String that specifies the identification number
    */
    private String numeroIdentificacion;

    /**
     * String that specifies the contract number
    */
    private String numeroContrato;

    /**
     * String that specifies the value
    */
    private String valor;

    /**
     * String that specifies the transaction number
    */
    private String numeroTransaccion;

    /**
     * String that specifies the movement type
    */
    private String tipoMovimiento;

    /**
     * String that specifies the position code
    */
    private String codigoCargo;

    /**
     * String that specifies the purchase number
    */
    private String numeroCompra;
    
    /**
     * String that specifies the adjustment date
    */
    private String fechaRealizacionAjuste;

    /**
     * String that specifies the adjustment approval date
    */
    private String fechaAprobacionAjuste;

    /**
     * String that specifies the creator user
    */
    private String codigoUsuarioCreador;

    /**
     * String that specifies the approving user
    */
    private String codigoUsuarioAprobador;

    /**
     * String that specifies the observation
    */
    private String observacion;

    /**
     * String that specifies the status
    */
    private String estado;

    /*
     *
     * Métodos
     *
    */

    /**
     * This is the class contructutor
    */
    public MovimientoSaldoDto() {
    }

    /**
	 * Get the product code
	 * @return the product code
	*/
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
	 * Set the product code
	 * @param codigoProducto is the product code
	*/
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    /**
	 * Get the product name
	 * @return the product name
	*/
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
	 * Set the product name
	 * @param nombreProducto is the product name
	*/
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
	 * Get the product type code
	 * @return the product type code
	*/
    public String getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    /**
	 * Set the product type code
	 * @param codigoTipoProducto is the product type code
	*/
    public void setCodigoTipoProducto(String codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    /**
	 * Get the product type name
	 * @return the product type name
	*/
    public String getNombreTipoProducto() {
        return nombreTipoProducto;
    }

    /**
	 * Set the product type name
	 * @param nombreTipoProducto is the product type name
	*/
    public void setNombreTipoProducto(String nombreTipoProducto) {
        this.nombreTipoProducto = nombreTipoProducto;
    }

    /**
	 * Get the by-product code
	 * @return the by-product code
	*/
    public String getCodigoSubproducto() {
        return codigoSubproducto;
    }

    /**
	 * Set the by-product code
	 * @param codigoSubproducto is the by-product code
	*/
    public void setCodigoSubproducto(String codigoSubproducto) {
        this.codigoSubproducto = codigoSubproducto;
    }

    /**
	 * Get the by-product name
	 * @return the by-product name
	*/
    public String getNombreSubproducto() {
        return nombreSubproducto;
    }

    /**
	 * Set the by-product name
	 * @param nombreSubproducto is the by-product name
	*/
    public void setNombreSubproducto(String nombreSubproducto) {
        this.nombreSubproducto = nombreSubproducto;
    }

    /**
	 * Get the identification type
	 * @return the identification type
	*/
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
	 * Set the identification type
	 * @param tipoIdentificacion is the identification type
	*/
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
	 * Get the identification number
	 * @return the identification number
	*/
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    /**
	 * Set the identification number
	 * @param numeroIdentificacion is the identification number
	*/
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    /**
	 * Get the contract number
	 * @return the contract number
	*/
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
	 * Set the contract number
	 * @param numeroContrato is the contract number
	*/
    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    /**
	 * Get the value
	 * @return the value
	*/
    public String getValor() {
        return valor;
    }

    /**
	 * Set the value
	 * @param valor is the value
	*/
    public void setValor(String valor) {
        this.valor = valor;
    }

    /**
	 * Get the transaction number
	 * @return the transaction number
	*/
    public String getNumeroTransaccion() {
        return numeroTransaccion;
    }

    /**
	 * Set the transaction number
	 * @param numeroTransaccion is the transaction number
	*/
    public void setNumeroTransaccion(String numeroTransaccion) {
        this.numeroTransaccion = numeroTransaccion;
    }

    /**
	 * Get the movement type
	 * @return the movement type
	*/
    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    /**
	 * Set the movement type
	 * @param tipoMovimiento is the movement type
	*/
    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    /**
	 * Get the position code
	 * @return the position code
	*/
    public String getCodigoCargo() {
        return codigoCargo;
    }

    /**
	 * Set the position code
	 * @param codigoCargo is the position code
	*/
    public void setCodigoCargo(String codigoCargo) {
        this.codigoCargo = codigoCargo;
    }

    /**
	 * Get the purchase number
	 * @return the purchase number
	*/
    public String getNumeroCompra() {
        return numeroCompra;
    }

    /**
	 * Set the purchase number
	 * @param numeroCompra is the purchase number
	*/
    public void setNumeroCompra(String numeroCompra) {
        this.numeroCompra = numeroCompra;
    }

    /**
	 * Get the adjustment date
	 * @return the adjustment date
	*/
    public String getFechaRealizacionAjuste() {
        return fechaRealizacionAjuste;
    }

    /**
	 * Set the adjustment date
	 * @param fechaRealizacionAjuste is the adjustment date
	*/
    public void setFechaRealizacionAjuste(String fechaRealizacionAjuste) {
        this.fechaRealizacionAjuste = fechaRealizacionAjuste;
    }

    /**
	 * Get the adjustment approval date
	 * @return the adjustment approval date
	*/
    public String getFechaAprobacionAjuste() {
        return fechaAprobacionAjuste;
    }

    /**
	 * Set the adjustment approval date
	 * @param fechaAprobacionAjuste is the adjustment approval date
	*/
    public void setFechaAprobacionAjuste(String fechaAprobacionAjuste) {
        this.fechaAprobacionAjuste = fechaAprobacionAjuste;
    }

    /**
	 * Get the creator user code
	 * @return the creator user code
	*/
    public String getCodigoUsuarioCreador() {
        return codigoUsuarioCreador;
    }

    /**
	 * Set the creator user code
	 * @param codigoUsuarioCreador is the creator user code
	*/
    public void setCodigoUsuarioCreador(String codigoUsuarioCreador) {
        this.codigoUsuarioCreador = codigoUsuarioCreador;
    }

    /**
	 * Get the approving user code
	 * @return the approving user code
	*/
    public String getCodigoUsuarioAprobador() {
        return codigoUsuarioAprobador;
    }

    /**
	 * Set the approving user code
	 * @param codigoUsuarioAprobador is the approving user code
	*/
    public void setCodigoUsuarioAprobador(String codigoUsuarioAprobador) {
        this.codigoUsuarioAprobador = codigoUsuarioAprobador;
    }

    /**
	 * Get the observation
	 * @return the observation
	*/
    public String getObservacion() {
        return observacion;
    }

    /**
	 * Set the observation
	 * @param observacion is the observation
	*/
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
	 * Get the status
	 * @return the status
	*/
    public String getEstado() {
        return estado;
    }

    /**
	 * Set the status
	 * @param estado is the status
	*/
    public void setEstado(String estado) {
        this.estado = estado;
    }    

}