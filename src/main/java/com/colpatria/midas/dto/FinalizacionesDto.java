package com.colpatria.midas.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Dto with data in file
 */
public class FinalizacionesDto {

    /**
     * codigo de producto to process
     */
    @Getter
    @Setter
    private String codigoProducto;

    /**
     * nombre de producto to process
     */
    @Getter
    @Setter
    private String nombreProducto;

    /**
     * codigo tipo de producto to process
     */
    @Getter
    @Setter
    private String codigoTipoProducto;

    /**
     * nombre tipo de producto to process
     */
    @Getter
    @Setter
    private String nombreTipoProducto;

    /**
     * codigo sub producto to process
     */
    @Getter
    @Setter
    private String codigoSubProducto;

    /**
     * nombre sub producto to process
     */
    @Getter
    @Setter
    private String nombreSubProducto;

    /**
     * tipo de identificacion to process
     */
    @Setter
    @Getter
    private String tipoIdentificacion;

    /**
     * numero de identificacion to process
     */
    @Setter
    @Getter
    private String nroIdentificacion;

    /**
     * numero de contrato to process
     */
    @Setter
    @Getter
    private String nroContrato;

    /**
     * numero de servicio to process
     */
    @Setter
    @Getter
    private String nroServicio;

    /**
     * fecha cambio to process
     */
    @Setter
    @Getter
    private String fechaCambio;

    /**
     * numero de cuenta to process
     */
    @Setter
    @Getter
    private String nroCuenta;

    /**
     * estado anterior to process
     */
    @Setter
    @Getter
    private String estadoAnterior;

    /**
     * estado nuevo to process
     */
    @Setter
    @Getter
    private String estadoNuevo;

    /**
     * usuario to process
     */
    @Setter
    @Getter
    private String usuario;

    /**
     * area to process
     */
    @Setter
    @Getter
    private String area;

    /**
     * nombre to process
     */
    @Setter
    @Getter
    private String nombre;

    /**
     * observaciones to process
     */
    @Setter
    @Getter
    private String observaciones;

    /**
     * cargoConcepto to process
     */
    @Setter
    @Getter
    private String cargoConcepto;

    /**
     * cargoCapital to process
     */
    @Setter
    @Getter
    private String cargoCapital;

    /**
     * cargoInteresCorriente to process
     */
    @Setter
    @Getter
    private String cargoInteresCorriente;

    /**
     * cargoInteresMora to process
     */
    @Setter
    @Getter
    private String cargoInteresMora;

    /**
     * cargoPendienteFacturar to process
     */
    @Setter
    @Getter
    private String cargoPendienteFacturar;

}
