package com.colpatria.midas.dto;

import com.colpatria.midas.model.*;
import lombok.Data;

/**
 * Casas de Cobranza Query Dto
 */
@Data
public class CasasDeCobranzaProbeQueryDto {
    /**
     * Historial Cartera Light Reference
     */
    private HistorialCarteraLight historialCarteraLight;

    /**
     * Historial Cartera Resumen
     */
    private HistorialCarteraResumen historialCarteraResumen;

    /**
     * Historial Recaudo Reference
     */
    private HistorialRecaudo historialRecaudo;

    /**
     * Calendario facturacion Reference
     */
    private CalendarioFacturacion calendarioFacturacion;

    /**
     * Historial Recaudo Resummen reference
     */
    private HistorialRecaudoResumen historialRecaudoResumen;

    /**
     * Reclamacion Reference
     */
    private Reclamacion reclamacion;

    /**
     * Negociación Reference
     */
    private Negociacion negociacion;

    /**
     * Numero de Pagos Fecha cierre
     */
    private int numeroPagosFechaCierre;

    /**
     * Numero de refinanciaciones
     */
    private int numeroRefinanciaciones;

    /**
     * Numero Carteras Cierre Mes
     */
    private int numeroCarterasCierreMes;


}
