package com.colpatria.midas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PeriodoGraciaDto {

    /**
    * String that specifies the code of a product.
    */
    private String productoCodigo;

    /**
     * String that specifies the name of a product.
     */
    private String productoNombre;

    /**
     * String that specifies the code of a type of product.
     */
    private String productoTipoCodigo;

    /**
     * String that specifies the name of a type of product.
     */
    private String productoTipo;

    /**
     * String that specifies the code of a subproduct.
     */
    private String subproductoCodigo;

    /**
     * String that specifies the name of a subproduct.
     */
    private String subproductoNombre;

    /**
     * String that specifies the type of ID.
     */
    private String tipoIdentificacion;

    /**
     * String that specifies the number of an ID.
     */
    private String numeroIdentificacion;

    /**
     * String that refers to the number of a contract.
     */
    private String contratoNumero;

    /**
     * String that specifies the number of an account.
     */
    private String cuentaNumero;

    /**
     * String that specifies the number of 'ciclo facturacion'.
     */
    private String ciclo;

    /**
     * String variable that refers to the negotiation ID.
     */
    private String negociacionId;

    /**
     * Date of the negotiation.
     */
    private String fechaNegociacion;

    /**
     * Date of the application.
     */
    private String fechaAplicacion;

    /**
     * String that refers to the negotiation state.
     */
    private String negociacionEstado;

    /**
     * String that refers to the amount of previous installments.
     */
    private String cantidadCuotasAnteriores;

    /**
     * String that refers to the amount 'dias mora'.
     */
    private String diasMora;

    /**
     * Date of the 'inicio gracia'.
     */
    private String fechaInicioGracia;

    /**
     * Date of the 'fin gracia'.
     */
    private String fechaFinGracia;

    /**
     * Date of the 'desiste gracia'.
     */
    private String fechaDesistegracia;

    /**
     * String that specifies the number of a service.
     */
    private String servicioNumero;

    /**
     * String that specifies the description.
     */
    private String descripcion;

    /**
     * String that specifies the 'valor facturado'.
     */
    private String valorFacturado;

    /**
     * String that specifies the user id.
     */
    private String usuarioCodigo;

    /**
     * String that specifies the user name.
     */
    private String usuarioNombre;

    /**
     * String that specifies the user area.
     */
    private String usuarioArea;
    
}
