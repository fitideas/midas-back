package com.colpatria.midas.dto;

import java.time.LocalDate;

/**
 *  Class that make the data transfer of objects of the HU01-03.
 */
public class RecaudoProcessed {

    /**
     * Integer that refers to the code product.
     */
    private Integer   codigoProducto;

    /**
     * String that refers to the name product.
     */
    private String    nombreProducto;

    /**
     * Integer that refers to the type product code.
     */
    private Integer   codigoTipoProducto;

    /**
     * String that refers to the type product name.
     */
    private String    nombreTipoProducto;

    /**
     * Integer that refers to the Subproduct code.
     */
    private Integer   codigoSubproducto;

    /**
     * String that refers to the subproduct name
     */
    private String    nombreSubproducto;

    /**
     * Integer that refers to the ID document type
     */
    private Integer tipoDocumentoIdentidad; // Cliente

    /**
     * String that refers to the ID number of the client
     */
    private String nroDocumentoTitular; // Cliente

    /**
     * Long that refers to the contract number
     */
    private Long numeroContrato; // Contrato

    /**
     * String that refers to the account number
     */
    private String nroCuenta; // Cuenta

    /**
     * Long that refers to the branch office
     */
    private Long sucursal; // Sucursal

    /**
     * Integer that refers to the cicle
     */
    private Integer ciclo; // Cuenta

    /**
     * Long that refers to the town
     */
    private Long municipio; // Cuenta

    /**
     * Integer that refers to the category
     */
    private Integer categoria; // Categoria

    /**
     * Integer that refers to the stratum
     */
    private Integer estrato; // Cuenta

    /**
     * Integer that refers to the pay type
     */
    private Integer tipoPago; // RecaudoCargo

    /**
     * Double that refers to the paid value
     */
    private Double valorPagado; // RecaudoCargo

    /**
     * Double that refers to the amount of money
     */
    private Double monto; // RecaudoCargo

    /**
     * String that refers to the position code
     */
    private String codCargo; // RecaudoCargo

    /**
     * Long that refers to the service number
     */
    private Long nroServicio; // Servicio

    /**
     * String that refers to the description
     */
    private String descripcion;

    /**
     * String that refers to the service type
     */
    private String tipoServicio; // Servicio

    /**
     * Date that refers to the pay moment
     */
    private LocalDate fechaPago; // HistorialRecaudo

    /**
     * Date that refers to the paid entry
     */
    private LocalDate fechaIngresoPago; // HistorialRecaudo

    /**
     * Date that refers to the paid process
     */
    private LocalDate fechaProcesoPago; // HistorialRecaudo

    /**
     * Long that refers to the method of payment
     */
    private Long formaPago;

    /**
     * Long that refers to the entity collector code
     */
    private Integer codEntidadRecaudadora;

    /**
     * Integer that refers to the collector branch office
     */
    private Integer sucursalRecaudo;

    /**
     * Date that refers to the billing
     */
    private LocalDate fechaFacturacion;

    /**
     * String that refers to consume type
     */
    private String tipoConsumo; // Servicio

    /**
     * Date that refers to the ID expedition
     */
    private LocalDate fechaDocumento; // RecaudoCargo

    /**
     * Long that refers to the ID number
     */
    private Long nroDocumento;

    /**
     * Long that refers to the office
     */
    private Long oficina; // RecaudoCargo

    /**
     * Long that refers to the pay ID
     */
    private Long idPago;

    /**
     * Long that refers to the paid document number
     */
    private Long nroDocPago; // RecaudoCargo

    /**
     * Long that refers to the service
     */
    private Long servicio; // No Aplica

    /**
     * Get the cde product of a class.
     * @return Integer that refers to the code product.
     */
    public Integer getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Set the cde product of a class.
     * @param codigoProducto Integer that refers to the code product.
     */
    public void setCodigoProducto(Integer codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    /**
     * Get the name product of a class
     * @return String that refers to the name product.
     */
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
     * Set the name product of a class
     * @param nombreProducto String that refers to the name product.
     */
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
     * Get the product code of a class
     * @return Integer that refers to the type product code.
     */
    public Integer getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    /**
     * Set the product code of a class
     * @param codigoTipoProducto Integer that refers to the type product code.
     */
    public void setCodigoTipoProducto(Integer codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    /**
     * Get the product name of a class
     * @return String that refers to the type product name.
     */
    public String getNombreTipoProducto() {
        return nombreTipoProducto;
    }

    /**
     * Set the product name of a class
     * @param nombreTipoProducto String that refers to the type product name.
     */
    public void setNombreTipoProducto(String nombreTipoProducto) {
        this.nombreTipoProducto = nombreTipoProducto;
    }

    /**
     * Get the subproduct code of a class
     * @return Integer that refers to the Subproduct code.
     */
    public Integer getCodigoSubproducto() {
        return codigoSubproducto;
    }

    /**
     * Set the subproduct code of a class
     * @param codigoSubproducto Integer that refers to the Subproduct code.
     */
    public void setCodigoSubproducto(Integer codigoSubproducto) {
        this.codigoSubproducto = codigoSubproducto;
    }

    /**
     * Get the subproduct name of a class
     * @return String that refers to the subproduct name
     */
    public String getNombreSubproducto() {
        return nombreSubproducto;
    }

    /**
     * Set the subproduct name of a class
     * @param nombreSubproducto String that refers to the subproduct name
     */
    public void setNombreSubproducto(String nombreSubproducto) {
        this.nombreSubproducto = nombreSubproducto;
    }

    /**
     * Get the ID document type of  class
     * @return Integer that refers to the ID document type
     */
    public Integer getTipoDocumentoIdentidad() {
        return tipoDocumentoIdentidad;
    }

    /**
     * Set the ID document type of  class
     * @param tipoDocumentoIdentidad Integer that refers to the ID document type
     */
    public void setTipoDocumentoIdentidad(Integer tipoDocumentoIdentidad) {
        this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
    }

    /**
     * Get the ID number of the client of a class
     * @return String that refers to the Id number of the client
     */
    public String getNroDocumentoTitular() {
        return nroDocumentoTitular;
    }

    /**
     * Set the ID number of the client of a class
     * @param nroDocumentoTitular String that refers to the Id number of the client
     */
    public void setNroDocumentoTitular(String nroDocumentoTitular) {
        this.nroDocumentoTitular = nroDocumentoTitular;
    }

    /**
     * Get the contract number of a class
     * @return Long that refers to the contract number
     */
    public Long getNumeroContrato() {
        return numeroContrato;
    }

    /**
     * Set the contract number of a class
     * @param numeroContrato Long that refers to the contract number
     */
    public void setNumeroContrato(Long numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    /**
     * Get the number account of a class.
     * @return String that refers to the account number
     */
    public String getNroCuenta() {
        return nroCuenta;
    }

    /**
     * Set the number account of a class.
     * @param nroCuenta String that refers to the account number
     */
    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    /**
     * Get the branch office of a class.
     * @return Long that refers to the branch office
     */
    public Long getSucursal() {
        return sucursal;
    }

    /**
     * Set the branch office of a class.
     * @param sucursal Long that refers to the branch office
     */
    public void setSucursal(Long sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * Get the cicle of a class.
     * @return Integer that refers to the cicle
     */
    public Integer getCiclo() {
        return ciclo;
    }

    /**
     * Set the cicle of a class.
     * @param ciclo Integer that refers to the cicle
     */
    public void setCiclo(Integer ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * Get the town of a class
     * @return Long that refers to the town
     */
    public Long getMunicipio() {
        return municipio;
    }

    /**
     * Set the town of a class
     * @param municipio Long that refers to the town
     */
    public void setMunicipio(Long municipio) {
        this.municipio = municipio;
    }

    /**
     * Get the category of a class
     * @return Integer that refers to the category
     */
    public Integer getCategoria() {
        return categoria;
    }

    /**
     * Set the category of a class
     * @param categoria Integer that refers to the category
     */
    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    /**
     * Get the stratum of a class.
     * @return Integer that refers to the stratum
     */
    public Integer getEstrato() {
        return estrato;
    }

    /**
     * Set the stratum of a class.
     * @param estrato Integer that refers to the stratum
     */
    public void setEstrato(Integer estrato) {
        this.estrato = estrato;
    }

    /**
     * Get the pay type of  class.
     * @return Integer that refers to the pay type
     */
    public Integer getTipoPago() {
        return tipoPago;
    }

    /**
     * Set the pay type of  class.
     * @param tipoPago Integer that refers to the pay type
     */
    public void setTipoPago(Integer tipoPago) {
        this.tipoPago = tipoPago;
    }

    /**
     * Get the paid value of a class.
     * @return Double that refers to the paid value
     */
    public Double getValorPagado() {
        return valorPagado;
    }

    /**
     * Set the paid value of a class.
     * @param valorPagado Double that refers to the paid value
     */
    public void setValorPagado(Double valorPagado) {
        this.valorPagado = valorPagado;
    }

    /**
     * Get the amount of money of a class.
     * @return Double that refers to the amount of money
     */
    public Double getMonto() {
        return monto;
    }

    /**
     * Set the amount of money of a class.
     * @param monto Double that refers to the amount of money
     */
    public void setMonto(Double monto) {
        this.monto = monto;
    }

    /**
     * Get the position code of a class
     * @return String that refers to the position code
     */
    public String getCodCargo() {
        return codCargo;
    }

    /**
     * Set the position code of a class
     * @param codCargo String that refers to the position code
     */
    public void setCodCargo(String codCargo) {
        this.codCargo = codCargo;
    }

    /**
     * Get the number of service of a class.
     * @return Long that refers to the service number
     */
    public Long getNroServicio() {
        return nroServicio;
    }

    /**
     * Set the number of service of a class.
     * @param nroServicio Long that refers to the service number
     */
    public void setNroServicio(Long nroServicio) {
        this.nroServicio = nroServicio;
    }

    /**
     * Get the description of a class
     * @return String that refers to the description
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     *  Set the description of a class
     * @param descripcion String that refers to the description
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Get the service type of  class.
     * @return String that refers to the service type
     */
    public String getTipoServicio() {
        return tipoServicio;
    }

    /**
     * Set the service type of  class.
     * @param tipoServicio String that refers to the service type
     */
    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    /**
     * Get the date of the pay moment.
     * @return Date that refers to the pay moment
     */
    public LocalDate getFechaPago() {
        return fechaPago;
    }

    /**
     * Set the date of the pay moment.
     * @param fechaPago Date that refers to the pay moment
     */
    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * Get the paid entry of a class
     * @return Date that refers to the paid entry
     */
    public LocalDate getFechaIngresoPago() {
        return fechaIngresoPago;
    }

    /**
     * Set the paid entry of a class
     * @param fechaIngresoPago Date that refers to the paid entry
     */
    public void setFechaIngresoPago(LocalDate fechaIngresoPago) {
        this.fechaIngresoPago = fechaIngresoPago;
    }

    /**
     * Get the paid process date of a class.
     * @return Date that refers to the paid process
     */
    public LocalDate getFechaProcesoPago() {
        return fechaProcesoPago;
    }

    /**
     * Set the paid process date of a class.
     * @param fechaProcesoPago Date that refers to the paid process
     */
    public void setFechaProcesoPago(LocalDate fechaProcesoPago) {
        this.fechaProcesoPago = fechaProcesoPago;
    }

    /**
     * Get the method of payment of a class.
     * @return Long that refers to the method of payment
     */
    public Long getFormaPago() {
        return formaPago;
    }

    /**
     * Set the method of payment of a class.
     * @param formaPago Long that refers to the method of payment
     */
    public void setFormaPago(Long formaPago) {
        this.formaPago = formaPago;
    }

    /**
     * Get the entity collector code of a class.
     * @return Long that refers to the entity collector code
     */
    public Integer getCodEntidadRecaudadora() {
        return codEntidadRecaudadora;
    }

    /**
     * Set the entity collector code of a class.
     * @param codEntidadRecaudadora Long that refers to the entity collector code
     */
    public void setCodEntidadRecaudadora(Integer codEntidadRecaudadora) {
        this.codEntidadRecaudadora = codEntidadRecaudadora;
    }

    /**
     * Get the branch office of a class
     * @return Long that refers to the collector branch office
     */
    public Integer getSucursalRecaudo() {
        return sucursalRecaudo;
    }

    /**
     * Set the branch office of a class
     * @param sucursalRecaudo Long that refers to the collector branch office
     */
    public void setSucursalRecaudo(Integer sucursalRecaudo) {
        this.sucursalRecaudo = sucursalRecaudo;
    }

    /**
     * Get the billing of a class.
     * @return Date that refers to the billing
     */
    public LocalDate getFechaFacturacion() {
        return fechaFacturacion;
    }

    /**
     * Set the billing of a class.
     * @param fechaFacturacion Date that refers to the billing
     */
    public void setFechaFacturacion(LocalDate fechaFacturacion) {
        this.fechaFacturacion = fechaFacturacion;
    }

    /**
     * Get the consume type of class
     * @return String that refers to consume type
     */
    public String getTipoConsumo() {
        return tipoConsumo;
    }

    /**
     * Set the consume type of class
     * @param tipoConsumo String that refers to consume type
     */
    public void setTipoConsumo(String tipoConsumo) {
        this.tipoConsumo = tipoConsumo;
    }

    /**
     * Get the ID expedition of a class.
     * @return Date that refers to the ID expedition
     */
    public LocalDate getFechaDocumento() {
        return fechaDocumento;
    }

    /**
     * Set the ID expedition of a class.
     * @param fechaDocumento Date that refers to the ID expedition
     */
    public void setFechaDocumento(LocalDate fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    /**
     * Get the ID number of a class
     * @return Long that refers to the ID number
     */
    public Long getNroDocumento() {
        return nroDocumento;
    }

    /**
     * Set the ID number of a class
     * @param nroDocumento Long that refers to the ID number
     */
    public void setNroDocumento(Long nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    /**
     * Get the office of a class
     * @return Long that refers to the office
     */
    public Long getOficina() {
        return oficina;
    }

    /**
     * Set the office of a class
     * @param oficina Long that refers to the office
     */
    public void setOficina(Long oficina) {
        this.oficina = oficina;
    }

    /**
     * Get the payment ID of a class.
     * @return Long that refers to the payment ID
     */
    public Long getIdPago() {
        return idPago;
    }

    /**
     * Set the payment ID of a class.
     * @param idPago Long that refers to the payment ID
     */
    public void setIdPago(Long idPago) {
        this.idPago = idPago;
    }

    /**
     * Get the paid document number of a class
     * @return Long that refers to the paid document number
     */
    public Long getNroDocPago() {
        return nroDocPago;
    }

    /**
     * Set the paid document number of a class
     * @param nroDocPago Long that refers to the paid document number
     */
    public void setNroDocPago(Long nroDocPago) {
        this.nroDocPago = nroDocPago;
    }

    /**
     * Get the service of a class.
     * @return Long that refers to the service
     */
    public Long getServicio() {
        return servicio;
    }

    /**
     * Set the service of a class.
     * @param servicio Long that refers to the service
     */
    public void setServicio(Long servicio) {
        this.servicio = servicio;
    }
}
