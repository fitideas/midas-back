package com.colpatria.midas.dto;

/*
 *
 * Class
 *
*/

public class MaestroClienteDto {
    
    /**
     * String that specifies the account number
    */
    private String numeroCuenta;

    /**
     * String that specifies the service number
    */
    private String numeroServicio;

    /**
     * String that specifies the identification type
    */
    private String tipoIdentificacion;

    /**
     * String that specifies the identification number
    */
    private String numeroIdentificacion;

    /**
     * String that specifies the client name
    */
    private String nombreCliente;

    /**
     * String that specifies the client last name
    */
    private String apellidosCliente;

    /**
     * String that specifies the cliente address
    */
    private String direccionPredio;

    /**
     * String that specifies the reading apple
    */
    private String manzanaLectura;

    /**
     * String that specifies the reading branch
    */
    private String sucursalLectura;

    /**
     * String that specifies the reading zone
    */
    private String zonaLectura;

    /**
     * String that specifies the reading cycle
    */
    private String cicloLectura;

    /**
     * String that specifies the reading group
    */
    private String grupoLectura;

    /**
     * String that specifies the reading state code
    */
    private String codigoDepartamentoLectura;

    /**
     * String that specifies the reading state name
    */
    private String nombreDepartamentoLectura;
    
    /**
     * String that specifies the reading municipality code
    */
    private String codigoMunicipioLectura;    

    /**
     * String that specifies the reading municipality name
    */
    private String nombreMunicipioLectura;

    /**
     * String that specifies the reading locality code
    */
    private String codigoLocalidadLectura;

    /**
     * String that specifies the reading locality name
    */
    private String nombreLocalidadLectura;

    /**
     * String that specifies the reading neighborhood code
    */
    private String codigoBarrioLectura;

    /**
     * String that specifies the reading neighborhood name
    */
    private String nombreBarrioLectura;

    /**
     * String that specifies the delivery address
    */
    private String direccionReparto;

    /**
     * String that specifies the reading location
    */
    private String localizacionLectura;

    /**
     * String that specifies the distribution apple
    */
    private String manzanaReparto;

    /**
     * String that specifies the distribution branch
    */
    private String sucursalReparto;

    /**
     * String that specifies the distribution zone
    */
    private String zonaReparto;

    /**
     * String that specifies the distribution cycle
    */
    private String cicloReparto;

    /**
     * String that specifies the distribution group
    */
    private String grupoReparto;

    /**
     * String that specifies the client status
    */
    private String estratoSocioeconomico;

    /**
     * String that specifies the service state
    */
    private String estadoServicio;

    /**
     * String that specifies the electrical service status
    */
    private String estadoServicioElectrico;

    /**
     * String that specifies the category code
    */
    private String codigoCategoria;

    /**
     * String that specifies the category description
    */
    private String descripcionCategoria;

    /**
     * String that specifies the economic activity code
    */
    private String codigoActividadEconomica;

    /**
     * String that specifies the internal code
    */
    private String codigoInterno;

    /**
     * String that specifies the economic activity description
    */
    private String descripcionActividadEconomica;

    /**
     * Get the account number.
     * @return the account number.
    */
    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * Set the account number.
     * @param numeroCuenta is the account number.
    */
    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    /**
     * Get the service number.
     * @return the service number.
    */
    public String getNumeroServicio() {
        return numeroServicio;
    }

    /**
     * Set the service number.
     * @param numeroServicio is the service number.
    */
    public void setNumeroServicio(String numeroServicio) {
        this.numeroServicio = numeroServicio;
    }

    /**
     * Get the identification type.
     * @return the identification type.
    */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the identification type.
     * @param tipoIdentificacion is the identification type.
    */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the identification number.
     * @return the identification number.
    */
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    /**
     * Set the identification number.
     * @param numeroIdentificacion is the identification number.
    */
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    /**
     * Get the client name.
     * @return the client name.
    */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * Set the client last name.
     * @param nombreCliente is the client name.
    */
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    /**
     * Get the client last name.
     * @return the client last name.
    */
    public String getApellidosCliente() {
        return apellidosCliente;
    }

    /**
     * Set the client last name.
     * @param apellidosCliente is the client last name.
    */
    public void setApellidosCliente(String apellidosCliente) {
        this.apellidosCliente = apellidosCliente;
    }

    /**
     * Get the reading apple.
     * @return the reading apple.
    */
    public String getManzanaLectura() {
        return manzanaLectura;
    }

    /**
     * Set the reading apple.
     * @param manzanaLectura is the reading apple.
    */
    public void setManzanaLectura(String manzanaLectura) {
        this.manzanaLectura = manzanaLectura;
    }

    /**
     * Get the reading branch.
     * @return the reading branch.
    */
    public String getSucursalLectura() {
        return sucursalLectura;
    }

    /**
     * Set the reading branche.
     * @param sucursalLectura is the reading branch.
    */
    public void setSucursalLectura(String sucursalLectura) {
        this.sucursalLectura = sucursalLectura;
    }

    /**
     * Get the reading zone.
     * @return the reading zone.
    */
    public String getZonaLectura() {
        return zonaLectura;
    }

    /**
     * Set the reading zone.
     * @param zonaLectura is the reading zone.
    */
    public void setZonaLectura(String zonaLectura) {
        this.zonaLectura = zonaLectura;
    }

    /**
     * Get the reading cycle.
     * @return the reading cycle.
    */
    public String getCicloLectura() {
        return cicloLectura;
    }

    /**
     * Set the reading cycle.
     * @param cicloLectura is the reading cycle.
    */
    public void setCicloLectura(String cicloLectura) {
        this.cicloLectura = cicloLectura;
    }

    /**
     * Get the reading group.
     * @return the reading group.
    */
    public String getGrupoLectura() {
        return grupoLectura;
    }

    /**
     * Set the reading group.
     * @param grupoLectura is the reading group.
    */
    public void setGrupoLectura(String grupoLectura) {
        this.grupoLectura = grupoLectura;
    }

    /**
     * Get the reading state code.
     * @return the reading state code.
    */
    public String getCodigoDepartamentoLectura() {
        return codigoDepartamentoLectura;
    }

    /**
     * Set the reading state code.
     * @param codigoDepartamentoLectura is the reading state code.
    */
    public void setCodigoDepartamentoLectura(String codigoDepartamentoLectura) {
        this.codigoDepartamentoLectura = codigoDepartamentoLectura;
    }

    /**
     * Get the reading state name.
     * @return the reading state name.
    */
    public String getNombreDepartamentoLectura() {
        return nombreDepartamentoLectura;
    }

    /**
     * Set the reading state name.
     * @param nombreDepartamentoLectura is the reading state name.
    */
    public void setNombreDepartamentoLectura(String nombreDepartamentoLectura) {
        this.nombreDepartamentoLectura = nombreDepartamentoLectura;
    }

    /**
     * Get the reading municipality code.
     * @return the reading municipality code.
    */
    public String getCodigoMunicipioLectura() {
        return codigoMunicipioLectura;
    }

    /**
     * Set the reading municipality code.
     * @param codigoMunicipioLectura is the reading municipality code.
    */
    public void setCodigoMunicipioLectura(String codigoMunicipioLectura) {
        this.codigoMunicipioLectura = codigoMunicipioLectura;
    }

    /**
     * Get the reading municipality name.
     * @return the reading municipality name.
    */
    public String getNombreMunicipioLectura() {
        return nombreMunicipioLectura;
    }

    /**
     * Set the reading municipality code.
     * @param nombreMunicipioLectura is the reading municipality name.
    */
    public void setNombreMunicipioLectura(String nombreMunicipioLectura) {
        this.nombreMunicipioLectura = nombreMunicipioLectura;
    }

    /**
     * Get the reading locality code.
     * @return the reading locality code.
    */
    public String getCodigoLocalidadLectura() {
        return codigoLocalidadLectura;
    }

    /**
     * Set the reading locality code.
     * @param codigoLocalidadLectura is the reading locality code.
    */
    public void setCodigoLocalidadLectura(String codigoLocalidadLectura) {
        this.codigoLocalidadLectura = codigoLocalidadLectura;
    }

    /**
     * Get the reading locality name.
     * @return the reading locality name.
    */
    public String getNombreLocalidadLectura() {
        return nombreLocalidadLectura;
    }

    /**
     * Set the reading locality name.
     * @param nombreLocalidadLectura is the reading locality name.
    */
    public void setNombreLocalidadLectura(String nombreLocalidadLectura) {
        this.nombreLocalidadLectura = nombreLocalidadLectura;
    }

    /**
     * Get the reading neighborhood code.
     * @return the reading neighborhood code.
    */
    public String getCodigoBarrioLectura() {
        return codigoBarrioLectura;
    }

    /**
     * Set the reading neighborhood code.
     * @param codigoBarrioLectura is the reading neighborhood code.
    */
    public void setCodigoBarrioLectura(String codigoBarrioLectura) {
        this.codigoBarrioLectura = codigoBarrioLectura;
    }

    /**
     * Get the reading neighborhood name.
     * @return the reading neighborhood name.
    */
    public String getNombreBarrioLectura() {
        return nombreBarrioLectura;
    }

    /**
     * Set the reading neighborhood name.
     * @param nombreBarrioLectura is the reading neighborhood name.
    */
    public void setNombreBarrioLectura(String nombreBarrioLectura) {
        this.nombreBarrioLectura = nombreBarrioLectura;
    }

    /**
     * Get the distribution apple.
     * @return the distribution apple.
    */
    public String getManzanaReparto() {
        return manzanaReparto;
    }

    /**
     * Set the distribution apple.
     * @param manzanaReparto is the distribution apple.
    */
    public void setManzanaReparto(String manzanaReparto) {
        this.manzanaReparto = manzanaReparto;
    }

    /**
     * Get the distribution branch.
     * @return the distribution branch.
    */
    public String getSucursalReparto() {
        return sucursalReparto;
    }

    /**
     * Set the distribution branch.
     * @param sucursalReparto is the distribution branch.
    */
    public void setSucursalReparto(String sucursalReparto) {
        this.sucursalReparto = sucursalReparto;
    }

    /**
     * Get the distribution zone.
     * @return the distribution zone.
    */
    public String getZonaReparto() {
        return zonaReparto;
    }

    /**
     * Set the distribution zone.
     * @param zonaReparto is the distribution zone.
    */
    public void setZonaReparto(String zonaReparto) {
        this.zonaReparto = zonaReparto;
    }

    /**
     * Get the distribution zone.
     * @return the distribution zone.
    */
    public String getCicloReparto() {
        return cicloReparto;
    }

    /**
     * Set the distribution zone.
     * @param cicloReparto is the distribution zone.
    */
    public void setCicloReparto(String cicloReparto) {
        this.cicloReparto = cicloReparto;
    }

    /**
     * Get the distribution group.
     * @return the distribution group.
    */
    public String getGrupoReparto() {
        return grupoReparto;
    }

    /**
     * Set the distribution group.
     * @param grupoReparto is the distribution group.
    */
    public void setGrupoReparto(String grupoReparto) {
        this.grupoReparto = grupoReparto;
    }

    /**
     * Get the client status.
     * @return the client status.
    */
    public String getEstratoSocioeconomico() {
        return estratoSocioeconomico;
    }

    /**
     * Set the client status.
     * @param estratoSocioeconomico is the client status.
    */
    public void setEstratoSocioeconomico(String estratoSocioeconomico) {
        this.estratoSocioeconomico = estratoSocioeconomico;
    }

    /**
     * Get the service state.
     * @return the service state.
    */
    public String getEstadoServicio() {
        return estadoServicio;
    }

    /**
     * Set the service state.
     * @param estadoServicio is the service state.
    */
    public void setEstadoServicio(String estadoServicio) {
        this.estadoServicio = estadoServicio;
    }

    /**
     * Get the electrical service status.
     * @return the electrical service status.
    */
    public String getEstadoServicioElectrico() {
        return estadoServicioElectrico;
    }

    /**
     * Set the electrical service status.
     * @param estadoServicioElectrico is the electrical service status.
    */
    public void setEstadoServicioElectrico(String estadoServicioElectrico) {
        this.estadoServicioElectrico = estadoServicioElectrico;
    }

    /**
     * Get the category code.
     * @return the category code.
    */
    public String getCodigoCategoria() {
        return codigoCategoria;
    }

    /**
     * Set the category code.
     * @param codigoCategoria is the category code.
    */
    public void setCodigoCategoria(String codigoCategoria) {
        this.codigoCategoria = codigoCategoria;
    }

    /**
     * Get the category description.
     * @return the category description.
    */
    public String getDescripcionCategoria() {
        return descripcionCategoria;
    }

    /**
     * Set the category description.
     * @param descripcionCategoria is the category description.
    */
    public void setDescripcionCategoria(String descripcionCategoria) {
        this.descripcionCategoria = descripcionCategoria;
    }

    /**
     * Get the economic activity code.
     * @return the economic activity code.
    */
    public String getCodigoActividadEconomica() {
        return codigoActividadEconomica;
    }

    /**
     * Set the economic activity code.
     * @param codigoActividadEconomica is the economic activity code.
    */
    public void setCodigoActividadEconomica(String codigoActividadEconomica) {
        this.codigoActividadEconomica = codigoActividadEconomica;
    }

    /**
     * Get the internal code.
     * @return the internal code.
    */
    public String getCodigoInterno() {
        return codigoInterno;
    }

    /**
     * Set the internal code.
     * @param codigoInterno is the internal code.
    */
    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    /**
     * Get the economic activity description.
     * @return the economic activity description.
    */
    public String getDescripcionActividadEconomica() {
        return descripcionActividadEconomica;
    }

    /**
     * Set the economic activity description.
     * @param descripcionActividadEconomica is the economic activity description.
    */
    public void setDescripcionActividadEconomica(String descripcionActividadEconomica) {
        this.descripcionActividadEconomica = descripcionActividadEconomica;
    }

    /**
     * Get the cliente address.
     * @return the cliente address.
    */
    public String getDireccionPredio() {
        return direccionPredio;
    }

    /**
     * Set the cliente address.
     * @param direccionPredio is the cliente address.
    */
    public void setDireccionPredio(String direccionPredio) {
        this.direccionPredio = direccionPredio;
    }

    /**
     * Get the delivery address.
     * @return the delivery address.
    */
    public String getDireccionReparto() {
        return direccionReparto;
    }

    /**
     * Set the delivery address.
     * @param direccionReparto is the delivery address.
    */
    public void setDireccionReparto(String direccionReparto) {
        this.direccionReparto = direccionReparto;
    }

    /**
     * Get the reading location.
     * @return the reading location.
    */
    public String getLocalizacionLectura() {
        return localizacionLectura;
    }

    /**
     * Set the reading location.
     * @param localizacionLectura is the reading location.
    */
    public void setLocalizacionLectura(String localizacionLectura) {
        this.localizacionLectura = localizacionLectura;
    }    

}
