package com.colpatria.midas.dto;

/**
 * Class that make the data transfer of objects of the HU01-03.
 */
public class RecaudoDto {

    /**
     * String that refers to the code product.
     */
    private String codigoProducto;

    /**
     * String that refers to the name product.
     */
    private String nombreProducto;

    /**
     * String that refers to the type product code.
     */
    private String codigoTipoProducto;

    /**
     * String that refers to the type product name.
     */
    private String nombreTipoProducto;

    /**
     * String that refers to the Subproduct code.
     */
    private String codigoSubproducto;

    /**
     * String that refers to the subproduct name
     */
    private String nombreSubproducto;

    /**
     * String that refers to the ID type
     */
    private String tipoDocumentoIdentidad;

    /**
     * String that refers to the ID number of the client
     */
    private String nroDocumentoTitular;

    /**
     * String that refers to the contract number
     */
    private String numeroContrato;

    /**
     * String that refers to the account number
     */
    private String nroCuenta;

    /**
     * String that refers to the branch office
     */
    private String sucursal;

    /**
     * String that refers to the cicle
     */
    private String ciclo;

    /**
     * String that refers to the town
     */
    private String municipio;

    /**
     * String that refers to the category
     */
    private String categoria;

    /**
     * String that refers to the stratum
     */
    private String estrato;

    /**
     * String that refers to the pay type
     */
    private String tipoPago;

    /**
     * String that refers to the paid value
     */
    private String valorPagado;

    /**
     * String that refers to the amount of money
     */
    private String monto;

    /**
     * String that refers to the position code
     */
    private String codCargo;

    /**
     * String that refers to the service number
     */
    private String nroServicio;

    /**
     * String that refers to the description
     */
    private String descripcion;

    /**
     * String that refers to the pay moment
     */
    private String fechaPago;

    /**
     * String that refers to the paid process
     */
    private String fechaProcesoPago;

    /**
     * String that refers to the method of payment
     */
    private String formaPago;

    /**
     * String that refers to the entity collector code
     */
    private String codEntidadRecaudadora;

    /**
     * String that refers to the collector branch office
     */
    private String sucursalRecaudo;

    /**
     * String that refers to the date billing
     */
    private String fechaFacturacion;

    /**
     * String that refers to consume type
     */
    private String tipoConsumo;

    /**
     * String that refers to the ID expedition
     */
    private String fechaDocumento;

    /**
     * String that refers to the ID number
     */
    private String nroDocumento;

    /**
     * String that refers to the office
     */
    private String oficina;

    /**
     * Get the code product of a class.
     * @return String that refers to the code product.
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Set the code product of a class.
     * @param codigoProducto String that refers to the code product.
     */
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    /**
     * Get the name product of a class.
     * @return String that refers to the name product.
     */
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
     * Set the name product of a class.
     * @param nombreProducto String that refers to the name product.
     */
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
     * Get the product code of a class.
     * @return String that refers to the type product code.
     */
    public String getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    /**
     * Set the product code of a class.
     * @param codigoTipoProducto String that refers to the type product code.
     */
    public void setCodigoTipoProducto(String codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    /**
     * Get the type product name of a class.
     * @return String that refers to the type product name
     */
    public String getNombreTipoProducto() {
        return nombreTipoProducto;
    }

    /**
     * Set the type product name of a class.
     * @param nombreTipoProducto String that refers to the type product name
     */
    public void setNombreTipoProducto(String nombreTipoProducto) {
        this.nombreTipoProducto = nombreTipoProducto;
    }

    /**
     * Get the subproduct code of a class.
     * @return String that refers to the Subproduct code.
     */
    public String getCodigoSubproducto() {
        return codigoSubproducto;
    }

    /**
     * Set the subproduct code of a class.
     * @param codigoSubproducto String that refers to the Subproduct code.
     */
    public void setCodigoSubproducto(String codigoSubproducto) {
        this.codigoSubproducto = codigoSubproducto;
    }

    /**
     * Get the subproduct name of a class
     * @return String that refers to the subproduct name
     */
    public String getNombreSubproducto() {
        return nombreSubproducto;
    }

    /**
     * Set the subproduct name of a class
     * @param nombreSubproducto String that refers to the subproduct name
     */
    public void setNombreSubproducto(String nombreSubproducto) {
        this.nombreSubproducto = nombreSubproducto;
    }

    /**
     * Get the ID type of a class.
     * @return String that refers to the ID type
     */
    public String getTipoDocumentoIdentidad() {
        return tipoDocumentoIdentidad;
    }

    /**
     * Set the ID type of a class.
     * @param tipoDocumentoIdentidad String that refers to the ID type
     */
    public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
        this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
    }

    /**
     * Get the ID number of a class.
     * @return String that refers to the ID number of the client
     */
    public String getNroDocumentoTitular() {
        return nroDocumentoTitular;
    }

    /**
     * Set the ID number of a class.
     * @param nroDocumentoTitular String that refers to the ID number of the client
     */
    public void setNroDocumentoTitular(String nroDocumentoTitular) {
        this.nroDocumentoTitular = nroDocumentoTitular;
    }

    /**
     * Get the contract number of a class
     * @return String that refers to the contract number
     */
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
     * Set the contract number of a class
     * @param numeroContrato String that refers to the contract number
     */
    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    /**
     * Get the account number of a class.
     * @return String that refers to the account number
     */
    public String getNroCuenta() {
        return nroCuenta;
    }

    /**
     * Set the account number of a class.
     * @param nroCuenta String that refers to the account number
     */
    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    /**
     * Get the branch office of a class.
     * @return String that refers to the branch office
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * Set the branch office of a class.
     * @param sucursal String that refers to the branch office
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * Get the cicle of a class
     * @return String that refers to the cicle
     */
    public String getCiclo() {
        return ciclo;
    }

    /**
     * Set the cicle of a class
     * @param ciclo String that refers to the cicle
     */
    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * Get the town of a class
     * @return String that refers to the town
     */
    public String getMunicipio() {
        return municipio;
    }

    /**
     * Set the town of a class
     * @param municipio String that refers to the town
     */
    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    /**
     * Get the category of a class
     * @return String that refers to the category
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * Set the category of a class
     * @param categoria String that refers to the category
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * Get the stratum of a class
     * @return String that refers to the stratum
     */
    public String getEstrato() {
        return estrato;
    }

    /**
     * Set the stratum of a class
     * @param estrato String that refers to the stratum
     */
    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    /**
     * Get the payment type of  class
     * @return String that refers to the pay type
     */
    public String getTipoPago() {
        return tipoPago;
    }

    /**
     * Set the payment type of  class
     * @param tipoPago String that refers to the pay type
     */
    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    /**
     * Get the paid value of a class
     * @return String that refers to the paid value
     */
    public String getValorPagado() {
        return valorPagado;
    }

    /**
     * Set the paid value of a class
     * @param valorPagado String that refers to the paid value
     */
    public void setValorPagado(String valorPagado) {
        this.valorPagado = valorPagado;
    }

    /**
     * Get the amount of money of a class.
     * @return String that refers to the amount of money
     */
    public String getMonto() {
        return monto;
    }

    /**
     * Set the amount of money of a class.
     * @param monto String that refers to the amount of money
     */
    public void setMonto(String monto) {
        this.monto = monto;
    }

    /**
     * Get the position code of a class
     * @return String that refers to the position code
     */
    public String getCodCargo() {
        return codCargo;
    }

    /**
     * Set the position code of a class
     * @param codCargo String that refers to the position code
     */
    public void setCodCargo(String codCargo) {
        this.codCargo = codCargo;
    }

    /**
     * Get the service number of a class
     * @return String that refers to the service number
     */
    public String getNroServicio() {
        return nroServicio;
    }

    /**
     * Set the service number of a class
     * @param nroServicio String that refers to the service number
     */
    public void setNroServicio(String nroServicio) {
        this.nroServicio = nroServicio;
    }

    /**
     * Get the description of a class
     * @return String that refers to the description
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Set the description of a class
     * @param descripcion String that refers to the description
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Get the pay moment date of a class
     * @return String that refers to the pay moment
     */
    public String getFechaPago() {
        return fechaPago;
    }

    /**
     * Set the pay moment date of a class
     * @param fechaPago String that refers to the pay moment
     */
    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * Get the paid process date of a class
     * @return String that refers to the paid process
     */
    public String getFechaProcesoPago() {
        return fechaProcesoPago;
    }

    /**
     * Set the paid process date of a class
     * @param fechaProcesoPago String that refers to the paid process
     */
    public void setFechaProcesoPago(String fechaProcesoPago) {
        this.fechaProcesoPago = fechaProcesoPago;
    }

    /**
     * Get the method of payment of a class
     * @return String that refers to the method of payment
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * Set the method of payment of a class
     * @param formaPago String that refers to the method of payment
     */
    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    /**
     * Get the entity collector code of a class
     * @return String that refers to the entity collector code
     */
    public String getCodEntidadRecaudadora() {
        return codEntidadRecaudadora;
    }

    /**
     * Set the entity collector code of a class
     * @param codEntidadRecaudadora String that refers to the entity collector code
     */
    public void setCodEntidadRecaudadora(String codEntidadRecaudadora) {
        this.codEntidadRecaudadora = codEntidadRecaudadora;
    }

    /**
     * Get the collector branch office of a class
     * @return String that refers to the collector branch office
     */
    public String getSucursalRecaudo() {
        return sucursalRecaudo;
    }

    /**
     * Set the collector branch office of a class
     * @param sucursalRecaudo String that refers to the collector branch office
     */
    public void setSucursalRecaudo(String sucursalRecaudo) {
        this.sucursalRecaudo = sucursalRecaudo;
    }

    /**
     * Get the billing date of a class
     * @return String that refers to the date billing
     */
    public String getFechaFacturacion() {
        return fechaFacturacion;
    }

    /**
     * Set the billing date of a class
     * @param fechaFacturacion String that refers to the date billing
     */
    public void setFechaFacturacion(String fechaFacturacion) {
        this.fechaFacturacion = fechaFacturacion;
    }

    /**
     * Get the consume type of class
     * @return String that refers to consume type
     */
    public String getTipoConsumo() {
        return tipoConsumo;
    }

    /**
     * Set the consume type of class
     * @param tipoConsumo String that refers to consume type
     */
    public void setTipoConsumo(String tipoConsumo) {
        this.tipoConsumo = tipoConsumo;
    }

    /**
     * Get the ID expedition of a class
     * @return String that refers to the ID expedition
     */
    public String getFechaDocumento() {
        return fechaDocumento;
    }

    /**
     * Set the ID expedition of a class
     * @param fechaDocumento String that refers to the ID expedition
     */
    public void setFechaDocumento(String fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    /**
     * Get the ID number of a class
     * @return String that refers to the ID number
     */
    public String getNroDocumento() {
        return nroDocumento;
    }

    /**
     * Set the ID number of a class
     * @param nroDocumento String that refers to the ID number
     */
    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    /**
     * Get the office of a class
     * @return String that refers to the office
     */
    public String getOficina() {
        return oficina;
    }

    /**
     * Set the office of a class
     * @param oficina String that refers to the office
     */
    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

}
