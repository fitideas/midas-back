package com.colpatria.midas.dto;

/*
 *
 * Librerías
 *
 */

import lombok.Data;

/**
 * Class that make the data transfer of objects of the HU01-01.
 */
@Data
public class CarteraDto {

    /*
     *
     * Atríbutos
     *
    */

    /**
     * String that specifies the product code.
     */
    private String codigoProducto;

    /**
     * String that specifies the name product.
     */
    private String nombreProducto;

    /**
     * String that specifies the product type code.
     */
    private String codigoTipoProducto;

    /**
     * String that refers to the product type name.
     */
    private String nombreTipoProducto;

    /**
     * String that specifies the subproduct code.
     */
    private String codigoSubproducto;

    /**
     * String that specifies the subproduct name.
     */
    private String nombreSubproducto;

    /**
     * String variable that specifies the contract number.
     */
    private String numeroContrato;

    /**
     * String that specifies the ID type.
     */
    private String tipoIdentificacion;

    /**
     * String that specifies the ID number.
     */
    private String numeroIdentificacion;

    /**
     * String that specifies tha customer.
     */
    private String nombreCliente;

    /**
     * String that specifies the account name.
     */
    private String numeroCuenta;

    /**
     * String variable that specifies the branch office.
     */
    private String sucursal;

    /**
     * String that specifies the cicle.
     */
    private String ciclo;

    /**
     * String that specifies the service class.
     */
    private String claseServicio;

    /**
     * String that specifies the stratum.
     */
    private String estrato;

    /**
     * String that refers to the service number.
     */
    private String numeroServicio;

    /**
     * String that specifies to the description.
     */
    private String descripcion;

    /**
     * String that specifies the service state.
     */
    private String estadoServicio;

    /**
     * String that specifies the finalization reason.
     */
    private String motivoFinalizacion;

    /**
     * String that specifies the number of delay days.
     */
    private String diasAtraso;

    /**
     * Date of the first expiration.
     */
    private String fechaPrimerVencimiento;

    /**
     * Date of the second expiration.
     */
    private String fechaSegundoVencimiento;

    /**
     * String variable that refers to the delay of the first expiration.
     */
    private String moraPrimerVencimiento;

    /**
     * String variable that refers to the unpaid money.
     */
    private String capitalImpago;

    /**
     * String variable that refers to the unpaid interest.
     */
    private String interesImpago;

    /**
     * String variable that refers to the current interest on unpaid billed order.
     */
    private String interesOrdenCorrienteImpago;

    /**
     * String variable that refers to the delayed interests.
     */
    private String interesMora;

    /**
     * String variable that refers to the order unpaid delayed interests.
     */
    private String interesOrdenMoraImpago;

    /**
     * String variable that refers to the unbilled caused interests.
     */
    private String interesCausadoNoFacturado;

    /**
     * String variable that refers to the unbilled caused current order interests.
     */
    private String interesCorrienteOrdenCausadoNoFacturado;

    /**
     * String variable that refers to the delayed caused unbilled interests.
     */
    private String interesMoraCausadoNoFacturado;

    /**
     * String variable that refers to the delayed order caused unbilled interests.
     */
    private String interesMoraOrdenCausadoNoFacturado;

    /**
     * String variable that refers to the total current billed unpaid interest.
     */
    private String totalInteresCorrienteFacturadoImpago;

    /**
     * String variable that refers to the total caused current interest.
     */
    private String totalInteresCorrienteCausado;

    /**
     * String variable that refers to the total delayed billed unpaid interest.
     */
    private String totalInteresMoraFacturadoImpago;

    /**
     * String variable that refers to the total caused delay interest.
     */
    private String totalInteresMoraCausado;

    /**
     * String variable that refers to the unaffected billed interest.
     */
    private String interesFacturadoNoAfecto;

    /**
     * String variable that refers to the unaffected interest for bill.
     */
    private String interesNoAfectoPorFacturar;

    /**
     * String variable that refers to the due money.
     */
    private String saldoDeuda;

    /**
     * String variable that refers to the money for bill.
     */
    private String saldoCapitalPorFacturar;

    /**
     * String that refers to the agreed fees.
     */
    private String cuotasPactadas;

    /**
     * String that refers to the fees for bill.
     */
    private String cuotasPorFacturar;

    /**
     * String that refers to the value of the fee.
     */
    private String valorCuota;

    /**
     * String that refers to the value of the purchase.
     */
    private String valorCompra;

    /**
     * Date that refers to the creation of the service in the system.
     */
    private String fechaCreacion;

    /**
     * Date that refers to the purchase.
     */
    private String fechaCompra;

    /**
     * String that contains the consumption type.
     */
    private String tipoConsumo;

    /**
     * String that contains the rate.
     */
    private String tasa;

    /**
     * String that refers to the partner of the business.
     */
    private String socioDeNegocio;

    /**
     * String that contains the billing.
     */
    private String facturacion;

    /**
     * String that contains the state.
     */
    private String estado;

    /*
     *
     * Métodos
     *
    */

    /**
     * Get the code product of a class.
     * @return String that specifies the product code
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Set the code product of a class.
     * @param codigoProducto String that specifies the product code
     */
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    /**
     * Get the name product of a class.
     * @return String that specifies the name product
     */
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
     * Set the name product of a class.
     * @param nombreProducto String that specifies the name product.
     */
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
     * Get the type product code of a class.
     * @return String that specifies the product type code.
     */
    public String getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    /**
     * Set the type product code of a class.
     * @param codigoTipoProducto String that specifies the product type code.
     */
    public void setCodigoTipoProducto(String codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    /**
     * Get the type product type name of a class.
     * @return String that refers to the product type name.
     */
    public String getNombreTipoProducto() {
        return nombreTipoProducto;
    }

    /**
     * Set the type product type name of a class.
     * @param nombreTipoProducto String that refers to the product type name.
     */
    public void setNombreTipoProducto(String nombreTipoProducto) {
        this.nombreTipoProducto = nombreTipoProducto;
    }

    /**
     * Get the subproduct code of a class.
     * @return String that specifies the subproduct code.
     */
    public String getCodigoSubproducto() {
        return codigoSubproducto;
    }

    /**
     * Set the subproduct code of a class.
     * @param codigoSubproducto String that specifies the subproduct code.
     */
    public void setCodigoSubproducto(String codigoSubproducto) {
        this.codigoSubproducto = codigoSubproducto;
    }

    /**
     * Get the subproduct name of a class.
     * @return String that specifies the subproduct name.
     */
    public String getNombreSubproducto() {
        return nombreSubproducto;
    }

    /**
     * Set the subproduct name of a class.
     * @param nombreSubproducto String that specifies the subproduct name.
     */
    public void setNombreSubproducto(String nombreSubproducto) {
        this.nombreSubproducto = nombreSubproducto;
    }

    /**
     * Get the contract number of a class.
     * @return String variable that specifies the contract number
     */
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
     * Set the contract number of a class.
     * @param numeroContrato String variable that specifies the contract number.
     */
    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    /**
     * Get the ID type of class.
     * @return String that specifies the ID type.
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the ID type of class.
     * @param tipoIdentificacion String that specifies the ID type.
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the ID number of a class.
     * @return String that specifies the ID number.
     */
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    /**
     * Set the ID number of a class.
     * @param numeroIdentificacion String that specifies the ID number.
     */
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    /**
     * Get tha client name of a class.
     * @return String that specifies the customer.
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * Set tha client name of a class.
     * @param nombreCliente String that specifies the customer.
     */
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    /**
     * Get the account number of a class.
     * @return String that specifies the account name.
     */
    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * Set the account number of a class.
     * @param numeroCuenta String that specifies the account name.
     */
    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    /**
     * Get the branch office of a class.
     * @return String variable that specifies the branch office.
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * Set the branch office of a class.
     * @param sucursal String variable that specifies the branch office.
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * Get the cicle of a class.
     * @return String that specifies the cicle.
     */
    public String getCiclo() {
        return ciclo;
    }

    /**
     * Set the cicle of a class.
     * @param ciclo String that specifies the cicle.
     */
    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * Get the service class.
     * @return String that specifies the service class.
     */
    public String getClaseServicio() {
        return claseServicio;
    }

    /**
     * Set the service class.
     * @param claseServicio String that specifies the service class.
     */
    public void setClaseServicio(String claseServicio) {
        this.claseServicio = claseServicio;
    }

    /**
     * Get the stratum of a class.
     * @return String that specifies the stratum.
     */
    public String getEstrato() {
        return estrato;
    }

    /**
     * Set the stratum of a class.
     * @param estrato String that specifies the stratum.
     */
    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    /**
     * Get the service number of a class.
     * @return String that refers to the service number.
     */
    public String getNumeroServicio() {
        return numeroServicio;
    }

    /**
     * Set the service number of a class.
     * @param numeroServicio String that refers to the service number.
     */
    public void setNumeroServicio(String numeroServicio) {
        this.numeroServicio = numeroServicio;
    }

    /**
     * Get the description of a class.
     * @return String that specifies the description.
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Set the description of a class.
     * @param descripcion String that specifies the description
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Get the service state of a class.
     * @return String that specifies the service state.
     */
    public String getEstadoServicio() {
        return estadoServicio;
    }

    /**
     * Set the service state of a class.
     * @param estadoServicio String that specifies the service state.
     */
    public void setEstadoServicio(String estadoServicio) {
        this.estadoServicio = estadoServicio;
    }

    /**
     * Get the finalization reason of a class.
     * @return String that specifies the finalization reason.
     */
    public String getMotivoFinalizacion() {
        return motivoFinalizacion;
    }

    /**
     * Set the finalization reason of a class.
     * @param motivoFinalizacion String that specifies the finalization reason.
     */
    public void setMotivoFinalizacion(String motivoFinalizacion) {
        this.motivoFinalizacion = motivoFinalizacion;
    }

    /**
     * Get the delay days of a class.
     * @return String that specifies the number of delay days.
     */
    public String getDiasAtraso() {
        return diasAtraso;
    }

    /**
     * Set the delay days of a class.
     * @param diasAtraso String that specifies the number of delay days.
     */
    public void setDiasAtraso(String diasAtraso) {
        this.diasAtraso = diasAtraso;
    }

    /**
     * Get the date of the first expiration of a class.
     * @return Date of the first expiration.
     */
    public String getFechaPrimerVencimiento() {
        return fechaPrimerVencimiento;
    }

    /**
     * Set the date of the first expiration of a class.
     * @param fechaPrimerVencimiento Date of the first expiration.
     */
    public void setFechaPrimerVencimiento(String fechaPrimerVencimiento) {
        this.fechaPrimerVencimiento = fechaPrimerVencimiento;
    }

    /**
     * Get the first expiration date of a class.
     * @return Date of the second expiration.
     */
    public String getFechaSegundoVencimiento() {
        return fechaSegundoVencimiento;
    }

    /**
     * Set the first expiration date of a class.
     * @param fechaSegundoVencimiento Date of the second expiration.
     */
    public void setFechaSegundoVencimiento(String fechaSegundoVencimiento) {
        this.fechaSegundoVencimiento = fechaSegundoVencimiento;
    }

    /**
     * Get the delay of the first expiration of a class.
     * @return String variable that refers to the delay of the first expiration.
     */
    public String getMoraPrimerVencimiento() {
        return moraPrimerVencimiento;
    }

    /**
     * Set the delay of the first expiration of a class.
     * @param moraPrimerVencimiento String variable that refers to the delay of the first expiration.
     */
    public void setMoraPrimerVencimiento(String moraPrimerVencimiento) {
        this.moraPrimerVencimiento = moraPrimerVencimiento;
    }

    /**
     * Get the unpaid money of a class.
     * @return String variable that refers to the unpaid money.
     */
    public String getCapitalImpago() {
        return capitalImpago;
    }

    /**
     * Set the unpaid money of a class.
     * @param capitalImpago String variable that refers to the unpaid money.
     */
    public void setCapitalImpago(String capitalImpago) {
        this.capitalImpago = capitalImpago;
    }

    /**
     * Get the unpaid interest of a class
     * @return String variable that refers to the unpaid interest.
     */
    public String getInteresImpago() {
        return interesImpago;
    }

    /**
     * Set the unpaid interest of a class
     * @param interesImpago String variable that refers to the unpaid interest.
     */
    public void setInteresImpago(String interesImpago) {
        this.interesImpago = interesImpago;
    }

    /**
     * Get the current interest on unpaid billed order of a class.
     * @return String variable that refers to the current interest on unpaid billed order.
     */
    public String getInteresOrdenCorrienteImpago() {
        return interesOrdenCorrienteImpago;
    }

    /**
     * Set the current interest on unpaid billed order of a class.
     * @param interesOrdenCorrienteImpago String variable that refers to the current interest on unpaid billed order.
     */
    public void setInteresOrdenCorrienteImpago(String interesOrdenCorrienteImpago) {
        this.interesOrdenCorrienteImpago = interesOrdenCorrienteImpago;
    }

    /**
     * Get the delayed interests of a class.
     * @return String variable that refers to the delayed interests.
     */
    public String getInteresMora() {
        return interesMora;
    }

    /**
     * Set the delayed interests of a class.
     * @param interesMora String variable that refers to the delayed interests.
     */
    public void setInteresMora(String interesMora) {
        this.interesMora = interesMora;
    }

    /**
     * Get the order unpaid delayed interests of a class.
     * @return String variable that refers to the order unpaid delayed interests.
     */
    public String getInteresOrdenMoraImpago() {
        return interesOrdenMoraImpago;
    }

    /**
     * Set the order unpaid delayed interests of a class.
     * @param interesOrdenMoraImpago String variable that refers to the order unpaid delayed interests.
     */
    public void setInteresOrdenMoraImpago(String interesOrdenMoraImpago) {
        this.interesOrdenMoraImpago = interesOrdenMoraImpago;
    }

    /**
     * Get the unbilled caused interests of a class.
     * @return String variable that refers to the unbilled caused interests
     */
    public String getInteresCausadoNoFacturado() {
        return interesCausadoNoFacturado;
    }

    /**
     * Set the unbilled caused interests of a class.
     * @param interesCausadoNoFacturado String variable that refers to the unbilled caused interests
     */
    public void setInteresCausadoNoFacturado(String interesCausadoNoFacturado) {
        this.interesCausadoNoFacturado = interesCausadoNoFacturado;
    }

    /**
     * Get the unbilled caused current order interests of a class.
     * @return String variable that refers to the unbilled caused current order interests.
     */
    public String getInteresCorrienteOrdenCausadoNoFacturado() {
        return interesCorrienteOrdenCausadoNoFacturado;
    }

    /**
     * Set the unbilled caused current order interests of a class
     * @param interesCorrienteOrdenCausadoNoFacturado String variable that refers to the unbilled caused current order interests.
     */
    public void setInteresCorrienteOrdenCausadoNoFacturado(String interesCorrienteOrdenCausadoNoFacturado) {
        this.interesCorrienteOrdenCausadoNoFacturado = interesCorrienteOrdenCausadoNoFacturado;
    }

    /**
     * Get the delayed caused unbilled interests of a class.
     * @return String variable that refers to the delayed caused unbilled interests.
     */
    public String getInteresMoraCausadoNoFacturado() {
        return interesMoraCausadoNoFacturado;
    }

    /**
     * Set the delayed caused unbilled interests of a class.
     * @param interesMoraCausadoNoFacturado String variable that refers to the delayed caused unbilled interests.
     */
    public void setInteresMoraCausadoNoFacturado(String interesMoraCausadoNoFacturado) {
        this.interesMoraCausadoNoFacturado = interesMoraCausadoNoFacturado;
    }

    /**
     * Get the delayed order caused unbilled interests of a class.
     * @return String variable that refers to the delayed order caused unbilled interests.
     */
    public String getInteresMoraOrdenCausadoNoFacturado() {
        return interesMoraOrdenCausadoNoFacturado;
    }

    /**
     * Set the delayed order caused unbilled interests of a class.
     * @param interesMoraOrdenCausadoNoFacturado String variable that refers to the delayed order caused unbilled interests.
     */
    public void setInteresMoraOrdenCausadoNoFacturado(String interesMoraOrdenCausadoNoFacturado) {
        this.interesMoraOrdenCausadoNoFacturado = interesMoraOrdenCausadoNoFacturado;
    }

    /**
     * Get the total current billed unpaid interest of a class.
     * @return String variable that refers to the total current billed unpaid interest.
     */
    public String getTotalInteresCorrienteFacturadoImpago() {
        return totalInteresCorrienteFacturadoImpago;
    }

    /**
     * Set the total current billed unpaid interest of a class.
     * @param totalInteresCorrienteFacturadoImpago String variable that refers to the total current billed unpaid interest.
     */
    public void setTotalInteresCorrienteFacturadoImpago(String totalInteresCorrienteFacturadoImpago) {
        this.totalInteresCorrienteFacturadoImpago = totalInteresCorrienteFacturadoImpago;
    }

    /**
     * Get that refers to the total caused current interest of a class.
     * @return String variable that refers to the total caused current interest.
     */
    public String getTotalInteresCorrienteCausado() {
        return totalInteresCorrienteCausado;
    }

    /**
     * Set that refers to the total caused current interest of a class.
     * @param totalInteresCorrienteCausado String variable that refers to the total caused current interest.
     */
    public void setTotalInteresCorrienteCausado(String totalInteresCorrienteCausado) {
        this.totalInteresCorrienteCausado = totalInteresCorrienteCausado;
    }

    /**
     * Get the total delayed billed unpaid interest of a class.
     * @return String variable that refers to the total delayed billed unpaid interest.
     */
    public String getTotalInteresMoraFacturadoImpago() {
        return totalInteresMoraFacturadoImpago;
    }

    /**
     * Set the total delayed billed unpaid interest of a class.
     * @param totalInteresMoraFacturadoImpago String variable that refers to the total delayed billed unpaid interest.
     */
    public void setTotalInteresMoraFacturadoImpago(String totalInteresMoraFacturadoImpago) {
        this.totalInteresMoraFacturadoImpago = totalInteresMoraFacturadoImpago;
    }

    /**
     * Get the total caused delay interest of a class.
     * @return String variable that refers to the total caused delay interest.
     */
    public String getTotalInteresMoraCausado() {
        return totalInteresMoraCausado;
    }

    /**
     * Set the total caused delay interest of a class.
     * @param totalInteresMoraCausado String variable that refers to the total caused delay interest.
     */
    public void setTotalInteresMoraCausado(String totalInteresMoraCausado) {
        this.totalInteresMoraCausado = totalInteresMoraCausado;
    }

    /**
     * Get the unaffected billed interest of a class.
     * @return String variable that refers to the unaffected billed interest.
     */
    public String getInteresFacturadoNoAfecto() {
        return interesFacturadoNoAfecto;
    }

    /**
     * Set the unaffected billed interest of a class.
     * @param interesFacturadoNoAfecto String variable that refers to the unaffected billed interest.
     */
    public void setInteresFacturadoNoAfecto(String interesFacturadoNoAfecto) {
        this.interesFacturadoNoAfecto = interesFacturadoNoAfecto;
    }

    /**
     * Get the unaffected interest for bill of a class.
     * @return String variable that refers to the unaffected interest for bill.
     */
    public String getInteresNoAfectoPorFacturar() {
        return interesNoAfectoPorFacturar;
    }

    /**
     * Set the unaffected interest for bill of a class.
     * @param interesNoAfectoPorFacturar String variable that refers to the unaffected interest for bill.
     */
    public void setInteresNoAfectoPorFacturar(String interesNoAfectoPorFacturar) {
        this.interesNoAfectoPorFacturar = interesNoAfectoPorFacturar;
    }

    /**
     * Get the due money of a class.
     * @return String variable that refers to the due money.
     */
    public String getSaldoDeuda() {
        return saldoDeuda;
    }

    /**
     * Set the due money of a class.
     * @param saldoDeuda String variable that refers to the due money.
     */
    public void setSaldoDeuda(String saldoDeuda) {
        this.saldoDeuda = saldoDeuda;
    }

    /**
     * Get the money for bill of a class.
     * @return String variable that refers to the money for bill
     */
    public String getSaldoCapitalPorFacturar() {
        return saldoCapitalPorFacturar;
    }

    /**
     * Set the money for bill of a class.
     * @param saldoCapitalPorFacturar String variable that refers to the money for bill
     */
    public void setSaldoCapitalPorFacturar(String saldoCapitalPorFacturar) {
        this.saldoCapitalPorFacturar = saldoCapitalPorFacturar;
    }

    /**
     * Get the agreed fees of a class.
     * @return String that refers to the agreed fees.
     */
    public String getCuotasPactadas() {
        return cuotasPactadas;
    }

    /**
     * Set the agreed fees of a class.
     * @param cuotasPactadas String that refers to the agreed fees.
     */
    public void setCuotasPactadas(String cuotasPactadas) {
        this.cuotasPactadas = cuotasPactadas;
    }

    /**
     * Get the fees for bill of a class.
     * @return String that refers to the fees for bill.
     */
    public String getCuotasPorFacturar() {
        return cuotasPorFacturar;
    }

    /**
     * Set the fees for bill of a class.
     * @param cuotasPorFacturar String that refers to the fees for bill.
     */
    public void setCuotasPorFacturar(String cuotasPorFacturar) {
        this.cuotasPorFacturar = cuotasPorFacturar;
    }

    /**
     * Get the value of fee of a class.
     * @return String that refers to the value of the fee.
     */
    public String getValorCuota() {
        return valorCuota;
    }

    /**
     * Set the value of fee of a class.
     * @param valorCuota String that refers to the value of the fee.
     */
    public void setValorCuota(String valorCuota) {
        this.valorCuota = valorCuota;
    }

    /**
     * Get the value of purchase of a class.
     * @return String that refers to the value of the purchase.
     */
    public String getValorCompra() {
        return valorCompra;
    }

    /**
     * Set the value of purchase of a class.
     * @param valorCompra String that refers to the value of the purchase.
     */
    public void setValorCompra(String valorCompra) {
        this.valorCompra = valorCompra;
    }

    /**
     * Get the creation date of a class.
     * @return Date that refers to the creation of the service in the system.
     */
    public String getFechaCreacion() {
        return fechaCreacion;
    }

    /**
     * Set the creation date of a class.
     * @param fechaCreacion Date that refers to the creation of the service in the system.
     */
    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    /**
     * Get the purchase date of a class.
     * @return Date that refers to the purchase.
     */
    public String getFechaCompra() {
        return fechaCompra;
    }

    /**
     * Set the purchase date of a class.
     * @param fechaCompra Date that refers to the purchase.
     */
    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    /**
     * Get the consumption type of class.
     * @return String that contains the consumption type.
     */
    public String getTipoConsumo() {
        return tipoConsumo;
    }

    /**
     * Set the consumption type of class.
     * @param tipoConsumo String that contains the consumption type.
     */
    public void setTipoConsumo(String tipoConsumo) {
        this.tipoConsumo = tipoConsumo;
    }

    /**
     * Get the rate of a class.
     * @return String that contains the rate.
     */
    public String getTasa() {
        return tasa;
    }

    /**
     * Set the rate of a class.
     * @param tasa String that contains the rate.
     */
    public void setTasa(String tasa) {
        this.tasa = tasa;
    }

    /**
     * Get the partner of business of a class.
     * @return String that refers to the partner of the business.
     */
    public String getSocioDeNegocio() {
        return socioDeNegocio;
    }

    /**
     * Set the partner of business of a class.
     * @param socioDeNegocio String that refers to the partner of the business.
     */
    public void setSocioDeNegocio(String socioDeNegocio) {
        this.socioDeNegocio = socioDeNegocio;
    }

    /**
     * Get the billing of a class.
     * @return String that contains the billing.
     */
    public String getFacturacion() {
        return facturacion;
    }

    /**
     * Set the billing of a class.
     * @param facturacion String that contains the billing.
     */
    public void setFacturacion(String facturacion) {
        this.facturacion = facturacion;
    }

    /**
     * Get the state of a class.
     * @return String that contains the state.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Set the state of a class.
     * @param estado String that contains the state.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

}
