package com.colpatria.midas.dto;

public class FacturacionConceptoDto {

    /**
     * String that specifies the id of a product.
     */
    private String productoCodigo;

    /**
     * String that specifies the name of a product.
     */
    private String productoNombre;

    /**
     * String that specifies the id of a product type.
     */
    private String productoTipoCodigo;

    /**
     * String that specifies the name of a product type.
     */
    private String productoTipoNombre;

    /**
     * String that specifies the id of a subproduct.
     */
    private String subproductoCodigo;

    /**
     * String that specifies the name of a subproduct.
     */
    private String subproductoNombre;

    /**
     * String that specifies the number of a contact.
     */
    private String contratoNumero;

    /**
     * String that specifies the type of a identification.
     */
    private String tipoIdentificacion;

    /**
     * String that specifies the number of a identification.
     */
    private String numeroIdentificacion;

    /**
     * String that specifies the number of an account.
     */
    private String cuentaNumero;

    /**
     * String that specifies the billing cicle.
     */
    private String ciclo;

    /**
     * String that specifies the number of a service.
     */
    private String servicioNumero;

    /**
     * String that specifies the id of a 'sucursal'.
     */
    private String sucursal;

    /**
     * String that specifies the 'estrato'.
     */
    private String estrato;

    /**
     * String that specifies the type of a billing.
     */
    private String facturacion;

    /**
     * String that specifies the description of billing.
     */
    private String descripsion;

    /**
     * String that specifies the id of a 'cargo'.
     */
    private String cargoCodigo;

    /**
     * String that specifies the billing amount.
     */
    private String valorFactura;

    /**
     * String that specifies the document date.
     */
    private String fechaDocumento;

    /**
     * String that specifies the process date.
     */
    private String fechaProceso;

    /**
     * String that specifies the document total amount.
     */
    private String totalDocumento;

    /**
     * String that specifies the facturation balance.
     */
    private String saldo;


    /**
     * @return the 'productoCodigo' associated with this 'facturacion concepto'
     */
    public String getProductoCodigo() {
        return this.productoCodigo;
    }

    /**
     * @param productoCodigo to be associated with this 'facturacion concepto'
     */
    public void setProductoCodigo(String productoCodigo) {
        this.productoCodigo = productoCodigo;
    }

    /**
     * @return the 'productoNombre' associated with this 'facturacion concepto'
     */
    public String getProductoNombre() {
        return this.productoNombre;
    }

    /**
     * @param productoNombre to be associated with this 'facturacion concepto'
     */
    public void setProductoNombre(String productoNombre) {
        this.productoNombre = productoNombre;
    }

    /**
     * @return the 'productoTipoCodigo' associated with this 'facturacion concepto'
     */
    public String getProductoTipoCodigo() {
        return this.productoTipoCodigo;
    }

    /**
     * @param productoTipoCodigo to be associated with this 'facturacion concepto'
     */
    public void setProductoTipoCodigo(String productoTipoCodigo) {
        this.productoTipoCodigo = productoTipoCodigo;
    }

    /**
     * @return the 'productoTipoNombre' associated with this 'facturacion concepto'
     */
    public String getProductoTipoNombre() {
        return this.productoTipoNombre;
    }

    /**
     * @param productoTipoNombre to be associated with this 'facturacion concepto'
     */
    public void setProductoTipoNombre(String productoTipoNombre) {
        this.productoTipoNombre = productoTipoNombre;
    }

    /**
     * @return the 'subproductoCodigo' associated with this 'facturacion concepto'
     */
    public String getSubproductoCodigo() {
        return this.subproductoCodigo;
    }

    /**
     * @param subproductoCodigo to be associated with this 'facturacion concepto'
     */
    public void setSubproductoCodigo(String subproductoCodigo) {
        this.subproductoCodigo = subproductoCodigo;
    }

    /**
     * @return the 'subproductoNombre' associated with this 'facturacion concepto'
     */
    public String getSubproductoNombre() {
        return this.subproductoNombre;
    }

    /**
     * @param subproductoNombre to be associated with this 'facturacion concepto'
     */
    public void setSubproductoNombre(String subproductoNombre) {
        this.subproductoNombre = subproductoNombre;
    }

    /**
     * @return the 'contratoNumero' associated with this 'facturacion concepto'
     */
    public String getContratoNumero() {
        return this.contratoNumero;
    }

    /**
     * @param contratoNumero to be associated with this 'facturacion concepto'
     */
    public void setContratoNumero(String contratoNumero) {
        this.contratoNumero = contratoNumero;
    }

    /**
     * @return the 'tipoIdentificacion' associated with this 'facturacion concepto'
     */
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    /**
     * @param tipoIdentificacion to be associated with this 'facturacion concepto'
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * @return the 'numeroIdentificacion' associated with this 'facturacion concepto'
     */
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    /**
     * @param numeroIdentificacion to be associated with this 'facturacion concepto'
     */
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    /**
     * @return the 'cuentaNumero' associated with this 'facturacion concepto'
     */
    public String getCuentaNumero() {
        return this.cuentaNumero;
    }

    /**
     * @param cuentaNumero to be associated with this 'facturacion concepto'
     */
    public void setCuentaNumero(String cuentaNumero) {
        this.cuentaNumero = cuentaNumero;
    }

    /**
     * @return the 'ciclo' associated with this 'facturacion concepto'
     */
    public String getCiclo() {
        return this.ciclo;
    }

    /**
     * @param ciclo to be associated with this 'facturacion concepto'
     */
    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * @return the 'servicioNumero' associated with this 'facturacion concepto'
     */
    public String getServicioNumero() {
        return this.servicioNumero;
    }

    /**
     * @param servicioNumero to be associated with this 'facturacion concepto'
     */
    public void setServicioNumero(String servicioNumero) {
        this.servicioNumero = servicioNumero;
    }

    /**
     * @return the 'sucursal' associated with this 'facturacion concepto'
     */
    public String getSucursal() {
        return this.sucursal;
    }

    /**
     * @param sucursal to be associated with this 'facturacion concepto'
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * @return the 'estrato' associated with this 'facturacion concepto'
     */
    public String getEstrato() {
        return this.estrato;
    }

    /**
     * @param estrato to be associated with this 'facturacion concepto'
     */
    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    /**
     * @return the 'facturacion' associated with this 'facturacion concepto'
     */
    public String getFacturacion() {
        return this.facturacion;
    }

    /**
     * @param facturacion to be associated with this 'facturacion concepto'
     */
    public void setFacturacion(String facturacion) {
        this.facturacion = facturacion;
    }

    /**
     * @return the 'descripsion' associated with this 'facturacion concepto'
     */
    public String getDescripsion() {
        return this.descripsion;
    }

    /**
     * @param descripsion to be associated with this 'facturacion concepto'
     */
    public void setDescripsion(String descripsion) {
        this.descripsion = descripsion;
    }

    /**
     * @return the 'cargoCodigo' associated with this 'facturacion concepto'
     */
    public String getCargoCodigo() {
        return this.cargoCodigo;
    }

    /**
     * @param cargoCodigo to be associated with this 'facturacion concepto'
     */
    public void setCargoCodigo(String cargoCodigo) {
        this.cargoCodigo = cargoCodigo;
    }

    /**
     * @return the 'valorFactura' associated with this 'facturacion concepto'
     */
    public String getValorFactura() {
        return this.valorFactura;
    }

    /**
     * @param valorFactura to be associated with this 'facturacion concepto'
     */
    public void setValorFactura(String valorFactura) {
        this.valorFactura = valorFactura;
    }

    /**
     * @return the 'fechaDocumento' associated with this 'facturacion concepto'
     */
    public String getFechaDocumento() {
        return this.fechaDocumento;
    }

    /**
     * @param fechaDocumento to be associated with this 'facturacion concepto'
     */
    public void setFechaDocumento(String fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    /**
     * @return the 'fechaProceso' associated with this 'facturacion concepto'
     */
    public String getFechaProceso() {
        return this.fechaProceso;
    }

    /**
     * @param fechaProceso to be associated with this 'facturacion concepto'
     */
    public void setFechaProceso(String fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    /**
     * @return the 'totalDocumento' associated with this 'facturacion concepto'
     */
    public String getTotalDocumento() {
        return this.totalDocumento;
    }

    /**
     * @param totalDocumento to be associated with this 'facturacion concepto'
     */
    public void setTotalDocumento(String totalDocumento) {
        this.totalDocumento = totalDocumento;
    }

    /**
     * @return the 'saldo' associated with this 'facturacion concepto'
     */
    public String getSaldo() {
        return this.saldo;
    }

    /**
     * @param saldo to be associated with this 'facturacion concepto'
     */
    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    
}
