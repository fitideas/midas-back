package com.colpatria.midas.dto;

/**
 * Class that make the data transfer of objects of the HU01-07-02.
 * @author Romario Jaimes
 */
public class CambioEstadoDto {

    /**
     * String that specifies the code of a product.
     */
    private String productoCodigo;

    /**
     * String that specifies the name of a product.
     */
    private String productoNombre;

    /**
     * String that specifies the code of a type of product.
     */
    private String productoTipoCodigo;

    /**
     * String that specifies the name of a type of product.
     */
    private String productoTipo;

    /**
     * String that specifies the code of a subproduct.
     */
    private String subproductoCodigo;

    /**
     * String that specifies the name of a subproduct.
     */
    private String subproductoNombre;

    /**
     * String that refers the number of a contract.
     */
    private String contratoNumero;

    /**
     * String that specifies the type of ID.
     */
    private String tipoIdentificacion;

    /**
     * String that specifies the number of an ID.
     */
    private String numeroIdentificacion;

    /**
     * String that specifies the number of a service.
     */
    private String servicioNumero;

    /**
     * Date variable of the transaction.
     */
    private String fechaTransaccion;

    /**
     * String that specifies the number of an account.
     */
    private String cuentaNumero;

    /**
     * String that specifies the new state of a service.
     */
    private String servicioEstadoNuevo;

    /**
     * String that specifies the old state of a service.
     */
    private String servicioEstadoAnterior;

    /**
     * String that specifies the ID of an user.
     */
    private String usuarioID;

    /**
     * String that specifies the name of an user.
     */
    private String usuarioNombre;


    /**
     * String that refers to the area where the user belong.
     */
    private String usuarioArea;

    /**
     * String that refers to a comment about the user.
     */
    private String observacion;

    /**
     * Empty constructor of the class.
     */
    public CambioEstadoDto() {
    }

    /**
     * Get a product code of a class
     * @return String that specifies the code of a product.
     */
    public String getProductoCodigo() {
        return this.productoCodigo;
    }

    /**
     * Set the product code of a class
     * @param productoCodigo String that specifies the code of a product.
     */
    public void setProductoCodigo(String productoCodigo) {
        this.productoCodigo = productoCodigo;
    }

    /**
     * Get a product name of a class.
     * @return String that specifies the name of a product.
     */
    public String getProductoNombre() {
        return this.productoNombre;
    }

    /**
     * Set a product name of a class
     * @param productoNombre  String that specifies the name of a product.
     */
    public void setProductoNombre(String productoNombre) {
        this.productoNombre = productoNombre;
    }


    /**
     * Get a product type code of a class.
     * @return  String that specifies the code of a type of product
     */
    public String getProductoTipoCodigo() {
        return this.productoTipoCodigo;
    }

    /**
     * Set a product type code of a class.
     * @param productoTipoCodigo String that specifies the code of a type of product
     */
    public void setProductoTipoCodigo(String productoTipoCodigo) {
        this.productoTipoCodigo = productoTipoCodigo;
    }

    /**
     * Get the type product of a class.
     * @return String that contains a type product.
     */
    public String getProductoTipo() {
        return this.productoTipo;
    }

    /**
     * Set the type product of a class.
     * @param productoTipo String that contains a type product.
     */
    public void setProductoTipo(String productoTipo) {
        this.productoTipo = productoTipo;
    }

    /**
     * Get the subproduct code of a class.
     * @return String that specifies the code of a subproduct.
     */
    public String getSubproductoCodigo() {
        return this.subproductoCodigo;
    }

    /**
     * Set the subproduct code of a class.
     * @param subproductoCodigo String that specifies the code of a subproduct.
     */
    public void setSubproductoCodigo(String subproductoCodigo) {
        this.subproductoCodigo = subproductoCodigo;
    }

    /**
     * Get the subproduct name of a class.
     * @return String that specifies the name of a subproduct.
     */
    public String getSubproductoNombre() {
        return this.subproductoNombre;
    }

    /**
     * Set the subproduct name of a class.
     * @param subproductoNombre String that specifies the name of a subproduct.
     */
    public void setSubproductoNombre(String subproductoNombre) {
        this.subproductoNombre = subproductoNombre;
    }

    /**
     * Get the contract number of a class.
     * @return String that refers the number of a contract.
     */
    public String getContratoNumero() {
        return this.contratoNumero;
    }

    /**
     * Set the contract number of a class.
     * @param contratoNumero String that refers the number of a contract.
     */
    public void setContratoNumero(String contratoNumero) {
        this.contratoNumero = contratoNumero;
    }

    /**
     * Get the ID type of class.
     * @return String that specifies the type of ID.
     */
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }


    /**
     * Set the ID type of class.
     * @param tipoIdentificacion String that specifies the type of ID.
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the ID number of a class.
     * @return String that specifies the number of an ID.
     */
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    /**
     * Set the ID number of a class.
     * @param numeroIdentificacion String that specifies the number of an ID.
     */
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    /**
     * Get the service number of a class.
     * @return String that specifies the number of a service.
     */
    public String getServicioNumero() {
        return this.servicioNumero;
    }

    /**
     * Set the service number of a class.
     * @param servicioNumero String that specifies the number of a service.
     */
    public void setServicioNumero(String servicioNumero) {
        this.servicioNumero = servicioNumero;
    }

    /**
     * Get the transaction date of a class.
     * @return Date variable of the transaction.
     */
    public String getFechaTransaccion() {
        return this.fechaTransaccion;
    }

    /**
     * Set the transaction date of a class.
     * @param fechaTransaccion Date variable of the transaction.
     */
    public void setFechaTransaccion(String fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    /**
     * Get the account number of a class.
     * @return String that specifies the number of an account.
     */
    public String getCuentaNumero() {
        return this.cuentaNumero;
    }

    /**
     * Set the account number of a class.
     * @param cuentaNumero String that specifies the number of an account.
     */
    public void setCuentaNumero(String cuentaNumero) {
        this.cuentaNumero = cuentaNumero;
    }

    /**
     * Get the new state service of a class.
     * @return String that specifies the new state of a service.
     */
    public String getServicioEstadoNuevo() {
        return this.servicioEstadoNuevo;
    }

    /**
     * Set the new state service of a class.
     * @param servicioEstadoNuevo String that specifies the new state of a service.
     */
    public void setServicioEstadoNuevo(String servicioEstadoNuevo) {
        this.servicioEstadoNuevo = servicioEstadoNuevo;
    }

    /**
     * Get the old state service of a class.
     * @return String that specifies the old state of a service.
     */
    public String getServicioEstadoAnterior() {
        return this.servicioEstadoAnterior;
    }

    /**
     * Set the old state service of a class.
     * @param servicioEstadoAnterior String that specifies the old state of a service.
     */
    public void setServicioEstadoAnterior(String servicioEstadoAnterior) {
        this.servicioEstadoAnterior = servicioEstadoAnterior;
    }

    /**
     * Get the user ID of a class.
     * @return String that specifies the ID of an user.
     */
    public String getUsuarioID() {
        return this.usuarioID;
    }

    /**
     * Set the user ID of a class.
     * @param usuarioID String that specifies the ID of an user.
     */
    public void setUsuarioID(String usuarioID) {
        this.usuarioID = usuarioID;
    }

    /**
     * Get the name user of a class.
     * @return String that specifies the name of an user.
     */
    public String getUsuarioNombre() {
        return this.usuarioNombre;
    }

    /**
     * Set the name user of a class.
     * @param usuarioNombre String that specifies the name of an user.
     */
    public void setUsuarioNombre(String usuarioNombre) {
        this.usuarioNombre = usuarioNombre;
    }

    /**
     * Get the area of the user.
     * @return String that refers to the area where the user belong.
     */
    public String getUsuarioArea() {
        return this.usuarioArea;
    }

    /**
     * Set the area of the user.
     * @param usuarioArea String that refers to the area where the user belong.
     */
    public void setUsuarioArea(String usuarioArea) {
        this.usuarioArea = usuarioArea;
    }

    /**
     * Get the observation of a class.
     * @return String that refers to a comment about the user.
     */
    public String getObservacion() {
        return this.observacion;
    }

    /**
     * Set the observation of a class.
     * @param observacion String that refers to a comment about the user.
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
        
}
