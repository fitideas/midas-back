package com.colpatria.midas.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GastoCobranzaDto {
    private String cedula;
    private String cuentaNumero;
    private String subproducto;
    private String ciclo;
    private String diasMora;
    private String capital;
    private String capitalSinFacturar;
    private String intereses;
    private String interesMora;
    private String saldo;
    private String saldoNoFacturado;
    private String cuotaManejo;
    private String gastoCobranza;
    private String fechaReporte;
    private String edad;
    private String fechaGestion;
    private String userGestion;
    private String observacionesGestion;
    private String efectividadGestion;
    private String fechaEnvioCarta;
    private String fechaLiquidacionGasto;
    private String consecutivo;
    private String fechaConsecutivo;
    private String servicioAsignado;
    private String causal;
    private String fileToBeWriten;
}
